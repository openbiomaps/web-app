<?php
require(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host)) {
    echo "Unsuccesful connect to UI database.<br><br>";
    $row = array();
    $row['email'] = (defined('CONTACT')) ? constant('CONTACT') : '';
} else {
    $cmd = sprintf("SELECT * FROM projects WHERE project_table='%s'",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
}

$message = sprintf("It seems that this project has not been maintained for a long time, so it can no longer be loaded for compatibility reasons.
<p>
If you are the administrator of the project, please visit the update manager and make the necessary updates,<br>if you are a visitor, please contact the project operators (%s) to access the project or data.</p>",$row['email']);

?>
<!DOCTYPE html>
<html>
<head>
<style>
body {
    font-family: "Lucida Grande", Verdana, Geneva, Lucida, Arial, Helvetica, sans-serif !important;
    font-size:12px;
    color: #3f3f3f;
    background-color: slategray;
    margin:0;
    position:relative;
    line-height:1.5;
    background-color:
}
#bheader {
    margin:0;
    display:table;
    width:100%;
    padding:0;
    line-height:1em;
    vertical-align:bottom;
    position:relative;
    border-bottom:1px solid #7c7c7c;
    box-shadow: 0 3px 8px rgba(0,0,0,.24);
    background-color:#cacaca;
    z-index:1100;
    min-width:300px;
}
#bheader .htitle {
    vertical-align:top;
    padding:15px 0 15px 30px;
    float:left;
}
#bheader .htitle p {
    font-family:georgia;
    letter-spacing:1px;
    color:#4d4d4d;
    font-size:250%;
    padding:1em 1em 1em 1em;
    line-height:50px;
    vertical-align:middle;
    margin:0;
    background: -moz-linear-gradient(left, rgba(250,250,250,1) 0%, rgba(250,250,250,0.35) 65%, rgba(0,0,0,0) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, rgba(250,250,250,1) 0%,rgba(250,250,250,0.35) 65%,rgba(0,0,0,0) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, rgba(250,250,250,1) 0%,rgba(250,250,250,0.65) 35%,rgba(0,0,0,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#00000000',GradientType=1 );
    border-radius:5px;
}
</style>
</head>
<body>
    <div id='bheader'>
        <div class='htitle'>
            <p><?php echo "Retired project warning!"; ?></p>
        </div>
    </div>
    <div style='text-align:center;font-size:2em;margin-top:2em'><?php echo $message; ?></div>
</body>
</html>

