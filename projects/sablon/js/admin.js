/* trigger events for logined users
 * */
//var CM = new Array();
var species_plus = new Array();
var myCodeMirror;
var myCM = {};
var cm_json;

$(document).ready(function() {
    
    // View manager
    $('#tab_basic').on('change','#mainTable',function(e){
        $.post("ajax", {'selectMainTable':$(this).val(),},
            function(data){
                var resp = JSON.parse(data);
                if (resp.status == 'success' || resp.status == 'OK') {
                    $('#mainTableColumns').val(resp.data);
                    $('#mainTableJoinColumn').html('');
                    $('#selectBaseColumn').html('');
                    $.each(resp.data, function (i, item) {
                        $('#mainTableJoinColumn').append($('<option>', { 
                            value: item,
                            text : item 
                        }));
                        $('#selectBaseColumn').append($('<option>', { 
                            value: item,
                            text : item 
                        }));
                    });
                    $("#loadEditorButton").click();
                }
        });
    });
    $('#tab_basic').on('change','#selectJoinedTable',function(e){

        $.post("ajax", {'get_columns_of_table':$(this).val(),},
            function(data){
                var resp = JSON.parse(data);
                
                if (resp.status == 'success' || resp.status == 'OK') {
                    $('#selectJoinedColumn').html('');
                    $('#joined1TableJoinColumn').html('');
                    $.each(resp.data, function (i, item) {
                        $('#selectJoinedColumn').append($('<option>', { 
                            value: item,
                            text : item 
                        }));
                        $('#joined1TableJoinColumn').append($('<option>', { 
                            value: item,
                            text : item 
                        }));
                    });
                    $("#loadEditorButton").click();
                } else {
                    alert(' :( ');
                }
        });
    });

    // Repository manager
    $('body').on('click','.RemoveFromRepository',function(e){
        e.preventDefault();

        var file_id = $(this).data('file_id');
        var obm_uid = $(this).data('obm_uid');
        var _this = $(this);
        $.post("ajax", {'delete-dataset-file':file_id,'obm_uid':obm_uid},
            function(data){
                //"{\"status\":\"OK\",\"message\":{\"message\":\"This file has the same content as datafile that is in the dataset. \"},\"data\":{\"files\":[{\"description\":\"\",\"label\":\"datafile-1\",\"restricted\":false,\"version\":1,\"datasetVersionId\":39,\"categories\":[\"Data\"],\"dataFile\":{\"id\":142,\"persistentId\":\"\",\"pidURL\":\"\",\"filename\":\"datafile-1\",\"contentType\":\"application/json\",\"filesize\":519031,\"description\":\"\",\"storageIdentifier\":\"file://17cf13058b0-f3e45e80bb1f\",\"rootDataFileId\":-1,\"md5\":\"fb7ab9d34356e9661754058e4efc0a40\",\"checksum\":{\"type\":\"MD5\",\"value\":\"fb7ab9d34356e9661754058e4efc0a40\"},\"creationDate\":\"2021-11-05\"}}]}}"
                var resp = JSON.parse(data);
                alert(resp.status);
                
                if (resp.status == 'success' || resp.status == 'OK') {
                    alert('OK');
                    _this.hide();
                } else {
                    alert(' :( ');
                }
                //$('#tab_basic').load(projecturl+'includes/profile.php?options=savedresults');
            });
    });
    $('body').on('click','.RepositoryPublish',function(e){
        e.preventDefault();

        var doi = $(this).data('doi');
        var _this = $(this);
        $.post("ajax", {'dataset-publish':doi},
            function(data){
                var resp = JSON.parse(data);
                alert(resp.status);
                
                if (resp.status == 'success' || resp.status == 'OK') {
                    alert('OK');
                    _this.hide();
                } else {
                    alert(' :( ');
                }
                //$('#tab_basic').load(projecturl+'includes/profile.php?options=savedresults');
            });
    });
    
    $('body').on('submit','#new-dataverse-dataset',function(e) {
        e.preventDefault();

        var data = $(this).serializeArray();

        $.post("ajax", {'new-dataverse-dataset':data},
            function(data){

                //{"status":"success","message":"","data":{"dataset":{"id":164,"identifier":"ADATTAR\/GZBJD9","persistentUrl":"https:\/\/doi.org\/10.48428\/ADATTAR\/GZBJD9","protocol":"doi","authority":"10.48428","publisher":"University of Debrecen","storageIdentifier":"file:\/\/10.48428\/ADATTAR\/GZBJD9","latestVersion":{"id":47,"datasetId":164,"datasetPersistentId":"doi:10.48428\/ADATTAR\/GZBJD9","storageIdentifier":"file:\/\/10.48428\/ADATTAR\/GZBJD9","versionState":"DRAFT","lastUpdateTime":"2021-11-13T07:08:39Z","createTime":"2021-11-09T09:18:43Z","license":"NONE","fileAccessRequest":false,"metadataBlocks":{"citation":{"displayName":"Citation Metadata","fields":[{"typeName":"title","multiple":false,"typeClass":"primitive","value":"Estonia air"},{"typeName":"author","multiple":true,"typeClass":"compound","value":[{"authorName":{"typeName":"authorName","multiple":false,"typeClass":"primitive","value":"B\u00e1n Mikl\u00f3s"}}]},{"typeName":"datasetContact","multiple":true,"typeClass":"compound","value":[{"datasetContactEmail":{"typeName":"datasetContactEmail","multiple":false,"typeClass":"primitive","value":"pobiomaps@gmail.com"}}]},{"typeName":"dsDescription","multiple":true,"typeClass":"compound","value":[{"dsDescriptionValue":{"typeName":"dsDescriptionValue","multiple":false,"typeClass":"primitive","value":"Estonia air"}}]},{"typeName":"subject","multiple":true,"typeClass":"controlledVocabulary","value":["Earth and Environmental Sciences"]}]}},"files":[{"description":"11@8IRMsyZPoaD0SfJL","label":"Estonia air-1.json","restricted":false,"version":1,"datasetVersionId":47,"categories":["Data","obm_unique_id"],"dataFile":{"id":190,"persistentId":"","pidURL":"","filename":"Estonia air-1.json","contentType":"application\/json","filesize":592,"description":"11@8IRMsyZPoaD0SfJL","storageIdentifier":"file:\/\/17d1820714f-b8f02b26fde0","rootDataFileId":-1,"md5":"a07726a81e1aea4e864d41eadcbd561c","checksum":{"type":"MD5","value":"a07726a81e1aea4e864d41eadcbd561c"},"creationDate":"2021-11-13"}},{"description":"11@8IRMsyZPoaD0SfJL","label":"Estonia air.json","restricted":false,"version":1,"datasetVersionId":47,"categories":["Data","obm_unique_id"],"dataFile":{"id":189,"persistentId":"","pidURL":"","filename":"Estonia air.json","contentType":"application\/json","filesize":592,"description":"11@8IRMsyZPoaD0SfJL","storageIdentifier":"file:\/\/17d18178b55-ad5474ebb877","rootDataFileId":-1,"md5":"a07726a81e1aea4e864d41eadcbd561c","checksum":{"type":"MD5","value":"a07726a81e1aea4e864d41eadcbd561c"},"creationDate":"2021-11-13"}},{"description":"13@QgH5EpLP7jnwvGyM","label":"g\u00fcg\u00fcg.json","restricted":false,"version":2,"datasetVersionId":47,"categories":["Data"],"dataFile":{"id":175,"persistentId":"","pidURL":"","filename":"g\u00fcg\u00fcg.json","contentType":"application\/json","filesize":618,"description":"13@QgH5EpLP7jnwvGyM","storageIdentifier":"file:\/\/17d068396e1-0252c78695b8","rootDataFileId":-1,"md5":"13a1939428f0eff789c3dc226851abd6","checksum":{"type":"MD5","value":"13a1939428f0eff789c3dc226851abd6"},"creationDate":"2021-11-09"}}]}},"file":{"files":[{"description":"11@8IRMsyZPoaD0SfJL","label":"Estonia air-2.json","restricted":false,"version":1,"datasetVersionId":47,"categories":["Data","obm_unique_id"],"dataFile":{"id":191,"persistentId":"","pidURL":"","filename":"Estonia air-2.json","contentType":"application\/json","filesize":592,"description":"11@8IRMsyZPoaD0SfJL","storageIdentifier":"file:\/\/17d1820ed90-32d8bad3980b","rootDataFileId":-1,"md5":"a07726a81e1aea4e864d41eadcbd561c","checksum":{"type":"MD5","value":"a07726a81e1aea4e864d41eadcbd561c"},"creationDate":"2021-11-13"}}]}}}

                
                var resp = JSON.parse(data);
                alert(resp.status);
                
                if (resp.status == 'success' || resp.status == 'OK') {

                    var versionState = resp.data.dataset.latestVersion.versionState;
                    var doi = resp.data.dataset.latestVersion.datasetPersistentId;
                    var files = resp.data.dataset.latestVersion.files;
                    id = $("#obm_pid").val();
                    var Id = id.substring(0,id.indexOf('@'));
                    $("#doi-id-" + Id).html(doi);
                    $("#doi-state-" + Id).html(versionState);
                    $("#add-link-" + Id).hide();
                    //console.log(resp.data);
                }
                //$('#tab_basic').load(projecturl+'includes/profile.php?options=savedresults');
            });
    });
    
    // JSON editor
    $('body').on('click','.my_codemirror',function(e){
        let i = $(this).attr('id');
        cm_json = CodeMirror.fromTextArea(document.getElementById(i), {
            matchBrackets: true,
            styleActiveLine: true,
            theme : 'default',
            indentUnit: 4,
            smartIndent: true,
            showFormatButton: true,
            autoCloseTags: true,
            autoCloseBrackets: true,
            enableSearchTools: true,
            enableCodeFolding: true,
            lineWrapping: true,
            mode: 'javascript',
        });
        cm_json.setSize("20em", null);
    });

    // file editor
    $('body').on('click','.editor-open',function(e){
       e.preventDefault();

        let file = $(this).data("file");
        let fileExt = $(this).data("name").split('.').pop();
        
        //show editor
        //$("#editor-div").resizable({alsoResize:".CodeMirror"});
        $('#editor-div').show();
        $('#editor-div').data('path', $(this).data("file"));
        $('#editor-div').data('name', $(this).data("name"));

        // load codemirror
        myCodeMirror = CodeMirror.fromTextArea(document.getElementById("editorArea"),{
            matchBrackets: true,
            styleActiveLine: true,
            theme : 'monokai',
            //theme : 'default',
            lineNumbers: true,
            indentUnit: 4,
            smartIndent: true,
            showFormatButton: true,
            autoCloseTags: true,
            autoCloseBrackets: true,
            enableSearchTools: true,
            enableCodeFolding: true,
            lineWrapping: true,
            //keyMap: "vim",

        });
        myCodeMirror.setSize("100%", "100%");

        const url = projecturl + 'ajax?get_file_content=' + file;
        fetch(url).then(function(response) {
          response.text().then(function(text) {
            myCodeMirror.getDoc().setValue(text);
            myCodeMirror.setOption("mode", fileExt );
            myCodeMirror.setSize("100%","100%" );
            $("#LanguageMode option[value='"+fileExt+"']").prop('selected', true);
          });
        });

    });

    $('body').on('change','#LanguageMode',function(e){
        myCodeMirror.setOption("mode", $(this).val() );
    });

    $('body').on('click','#editor-export',function(e){
       $(this).attr('href','ajax?export-editor-file&file=' +  $("#editor-div").data("path"));
    
    });

    $('body').on('click','#editor-close',function(e){
        $('#editor-div').hide();
        myCodeMirror.toTextArea();
    
    });
    $('body').on('click','#editor-save',function(e){
        let code = myCodeMirror.getValue();
        let name = $("#editor-div").data("name"); 
        let path = $("#editor-div").data("path");

        const url = projecturl + 'ajax';

        let formData = new FormData();
        formData.append("put_file_content", code);
        formData.append("path", path);
        formData.append("name", name);

        var request = new Request(url, {
            method: 'POST',
            body: formData,
        });

        fetch(request)
        .then(function() {
            // Handle response we get from the API
        });
    });

    // ezt miért így csináltam????
    sendInputProfile();
    //$(".customScroll").mCustomScrollbar({theme:'dark',scrollInertia:100,scrollButtons:{ enable: true }});

    // ctrl + s
    var ctrlDown = false,
        ctrlKey = 17,
        cmdKey = 91,
        plusKey = 107,
        minusKey = 109;

    // Messenger - copy addresses to clipboard
    $('#body').on('click', '#copy-addresses-to-clipboard', async function (e) {
        e.preventDefault();
        
        resp = await $.ajax({
            url : "ajax",
            type: "POST",
            data : {
                'get-addresses-of-group': $('#to').val(),
            },
        });
        resp = JSON.parse(resp);
        if (resp.status === 'success') {
            var mails = [];
            for (var i=0; i < resp['data'].length; i++) {
                mails.push(resp['data'][i]['email']);
            }
            const copyToClipboard = str => {
              const el = document.createElement('textarea');
              el.value = str;
              el.setAttribute('readonly', '');
              el.style.position = 'absolute';
              el.style.left = '-9999px';
              document.body.appendChild(el);
              const selected =
                document.getSelection().rangeCount > 0
                  ? document.getSelection().getRangeAt(0)
                  : false;
              el.select();
              document.execCommand('copy');
              document.body.removeChild(el);
              if (selected) {
                document.getSelection().removeAllRanges();
                document.getSelection().addRange(selected);
              }
            };

            copyToClipboard(mails.join("\n"));

        }
        else {
            console.log('fail');
        }

    });

    $(document).keydown(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = true;
    }).keyup(function(e) {
        if (e.keyCode == ctrlKey || e.keyCode == cmdKey) ctrlDown = false;
    });

    /* catch tab press change to 4 whitespaces in private_map */
    $("#tab_basic").on('keydown', 'textarea', function(e) { 
        var keyCode = e.keyCode || e.which; 


        if (ctrlDown && (e.keyCode == plusKey )) {
            e.preventDefault();
            $(this).css({'font-size':(parseInt($(this).css('font-size'))+4)+'px'});
            return false;
        }
        else if (ctrlDown && (e.keyCode == minusKey )) { 
            e.preventDefault();
            $(this).css({'font-size':(parseInt($(this).css('font-size'))-4)+'px'});
            return false;
        }

        if (keyCode == 9) { 
            e.preventDefault(); 
            // call custom function here
            var start = $(this).get(0).selectionStart;
            var end = $(this).get(0).selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            $(this).val($(this).val().substring(0, start)
                + "    "
                + $(this).val().substring(end));

            // put caret at right position again
            $(this).get(0).selectionStart =
            $(this).get(0).selectionEnd = start + 4;
        } 
    }); 
    /* logout button click */
    $('body').on('click','#logout',function(e){
        e.preventDefault();
        $.post(projecturl+"includes/logout.php", {html:1}, function(){
            document.location.href = projecturl;
        }); 
    });

    /* load subnav link from outside of topnav menu */
    $('body').on('click','.admin_direct_link',function (e) {
        e.stopPropagation();
        e.preventDefault();
        var url = $(this).attr("data-url");
        href = this.hash;

        if (typeof url !== "undefined") {
            $(href).load(projecturl+url);
        }
    });
    /* profile links click
     *
     * */
    $('#tab_basic,#nav').on('click','.profilelink',function(e) {
        e.stopPropagation();
        e.preventDefault();
        var href = this.hash;
        var url = $(this).attr("data-url");
        $(href).load(projecturl+url, function(){
            var page_slug = url.match(/options=(\w+)/)[1];
            if (typeof page_slug !== 'undefined') {
                $('body').trigger({
                    type: 'profile_page_loaded', 
                    page: page_slug
                });
            }
        });
    });
    
    $("#tabs").on('click','#sidenav_toggle', function (e) {
        if ($("#tabs").hasClass("toggled")) {
            $("#tabs").removeClass('toggled');
            $("#tabs").width("14em");
            $("#tab_basic").css("padding-left","15em");
            $(this).find('i').removeClass("fa-angle-double-right");
            $(this).find('i').addClass("fa-angle-double-left");
        } else {
            $("#tabs").addClass('toggled');
            $("#tabs").width("4em");
            $("#tab_basic").css("padding-left","5em");
            $(this).find('i').removeClass("fa-angle-double-left");
            $(this).find('i').addClass("fa-angle-double-right");
        }
    });


    /* tabs menu
     * click on tab simple button ( not on submenu )
     * load AJAX pages
     * */
    $('#tabs').on('click','.tablink,#prodTabs a',function (e) {
        if ($(this).attr('id')=='noload') {
            return;
        }
        e.stopPropagation();
        e.preventDefault();
        $(".topnav").find("ul.subnav").slideUp('fast');  
        $(".to").removeClass('to-t');
        var url = $(this).attr("data-url");
        if (typeof url !== "undefined") {
            var pane = $(this), href = this.hash;
            if (pane.closest('ul').hasClass('subnav')) {
                pane.closest('.topnav').find(".button-tab").removeClass("button-tab");
                $('#admin').addClass("button-tab");
            } else {
                pane.parent().closest('ul').find(".button-tab").removeClass("button-tab");
                pane.addClass("button-tab");
            }
            // ajax load from data-url
            $(href).load(projecturl+url, function(){
                var page_slug = url.match(/options=(\w+)/)[1];
                if (typeof page_slug !== 'undefined') {
                    $('body').trigger({
                        type: 'profile_page_loaded', 
                        page: page_slug
                    });
                }
            });
        }
    });
    $('body').on('click','.module_submenu',function (e) {
        var url = $(this).attr("data-url");
        if (typeof url !== "undefined") {
            $("#tab_basic").load(projecturl+url);
        }
    });

    // project_admin page: set columns
    $('body').on('click','#proj_column',function() {
        var template = {
            colname:{},
            name:{},
            type:{},
            order:{},
            table:{},
            comment:{},
            command:{},
            scheme:{},
        };
        carr = new Array();
        narr = new Array();
        tarr = new Array();
        oarr = new Array();
        karr = new Array();
        marr = new Array();
        zarr = new Array();
        $('#cola').find('tr').each(function() {
            $(this).find('td').each(function() {
                var id = $(this).attr('id')
                var sId=id.substring(id.length - 1);
                var val = $(this).find('.element').val();
                
                if (sId==0) carr.push(val);
                else if (sId==1) narr.push(val);
                else if (sId==2) marr.push(val);
                else if (sId==3) zarr.push(val);
                else if (sId==4) tarr.push(val);
                else if (sId==5) oarr.push(val);
            });
       }); 
       template['colname']=carr;
       template['name']=narr;
       template['type']=tarr;
       template['order']=oarr;
       template['comment']=marr;
       template['command']=zarr;
       template['table']=$(".main_table_selector").val();
       template['scheme']=$(".main_table_scheme_selector").val();

       $.post(projecturl+"includes/mod_project.php", {t_post:JSON.stringify(template)},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=dbcols',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols&mtable='+data);
            });
    });
    // project_admin page: set columns
    $("body").on('change','#cola',function(){
        $("#ficon").removeClass('fa-floppy-o'); 
        $("#ficon").addClass('fa-refresh fa-spin'); 
        $("#proj_column").removeClass('button-success'); 
        $("#proj_column").addClass('button-warning'); 
    });
    $("body").on('click','#db-collist-toggle',function(){
        $("#db-collist").toggle(); 
        $("#db-collist-helper").toggle(); 
        $("#db-collist-down").toggle();
        $("#db-collist-up").toggle(); 
    });


    // project_admin page: set columns
    $('body').on('click','#add_new_column',function() {
        $.post(projecturl+"includes/mod_project.php", {new_column:1,nc_name:$('#column_name').val(),nc_type:$('#column_type').val(),nc_length:$('#column_length').val(),nc_default:$('#column_default').val(),nc_array:$('#column_array').val(),nc_comment:$('#column_comment').val(),nc_check:$('#column_check').val(),nc_label:$('#column_label').val(),table:$(".main_table_selector").val()},
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['message']);
                } else if (retval['status']=='success') { 
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols&mtable='+retval['data']);
                }
            });
    });

    /* project admin - mod user's groups
     * */
    $('body').on('click','.mrb_send',function() {
            var id=$(this).attr('id');
            var rn=($(this).closest("tr")[0].rowIndex)-1;
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            var inputId='dc-'+widgetId;
            var selectedOptions = $.map($('#'+inputId+' :selected'),
                function(e) { return $(e).val(); } );
            var str = selectedOptions.join(',');
            var user_status=$('#sdc-'+widgetId).val();
            $.post(projecturl+"includes/mod_project.php", {user_id:widgetId,groups:str,user_status:user_status,html:1},
                function(data){
                    //reload page
                    //$('#tab_basic').load('includes/project_admin.php?options=members',function(result){});
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=members');
                    //$('#tagok-'+rn+'-5').html(data);
            }); 
    });
    // on project_admin page: mod groups' rights
    $('body').on('click','.mrg_send',function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            
            var inputId='ng-'+widgetId;
            var selectedOptions = $.map($('.'+inputId+':checked'),
                function(e) { return $(e).val(); } );
            var nested = selectedOptions.join(',');

            $.post(projecturl+"includes/mod_project.php", {group_id:widgetId,nested:nested},
                function(data){
                    //reload page
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                    if (data!='ok') {
                        // some error message
                        alert(data);
                    }
            }); 

    });
    $('body').on('click','.mrg_drop',function() {
            var id=$(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);

            $.post(projecturl+"includes/mod_project.php", {drop_group_id:widgetId},
                function(data){
                    //reload page
                    //$('#tab_basic').load('includes/project_admin.php?options=groups',function(result){});
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                    if (data!='ok') {
                        // some error message
                        alert(data);
                    }
            }); 

    });
    // on project_admin page: add group
    $('body').on('click','#crgr',function() {
        var text = $("#crgr-name").val();
        $.post(projecturl+"includes/mod_project.php", {new_group:text},
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=groups',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=groups');
                //$("#crgr-name").val(text+' done');
        }); 
    }); 

    // taxon maintenance : search
    $("body").on('input','#taxon_sim',function(){
        var t = $(this);
        t.autocomplete({
            source: function( request, response ) {
                $.post(projecturl+"includes/getList.php", {type:'taxon',term:$("#taxon_sim").val()}, function (data) {
                        response(JSON.parse(data));
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                setTaxon( ui.item.id + "#" + ui.item.meta + "#" + ui.item.value);
                $(this).val('');
                return false;
            }
        });
    });
    /* remove species
     * */
    $('body').on('click','.remove-species',function() {
        for( var i = 0; i < species_plus.length-1; i++){ 
            if ( species_plus[i] === $(this).val()) {
                species_plus.splice(i, 1); 
            }
        }
        $(this).remove();
    });

    //project_admin, taxon search 
    $("body").on('click','#taxonname_load',function(e){
        e.preventDefault();
        var taxon_val = "";
        var trgm_string = new Array();
        var allmatch = 0;
        var onematch = 0;
        var qids = new Array();
        var qval = new Array();
        $("#tsellist").html('');

        //if ($("#taxon_sim").val()!='') {
        if ($('#taxon_trgm > option').length) {
            var stra = new Array();
            if ($("#allmatch").is(":checked")) { allmatch = 1; }
            if ($("#onematch").is(":checked")) { onematch = 1; }
            
            $("#taxon_trgm option:selected").each(function(){ 
                stra.push($(this).val()); 
            });
            if (stra.length==0){
                $("#taxon_trgm option").each(function(){ stra.push($(this).val()); });
            }    
            var trgm_string = "";
            species_plus = species_plus.concat(stra); 
        }
        if (species_plus.length){
           trgm_string = JSON.stringify(species_plus);
           species_plus = new Array();
        }

        var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch,tpost:1 }
        for (i=0;i<qids.length;i++) {
            myVar["qids_" + qids[i]] = qval[i];
        }
        $.post("ajax", myVar,
        function(data){
            $('#taxonlist').html(data);
        });
    });
    /* project admin: map set centre */
    $("body").on('click','#map-centre-set',function(){
        $.post(projecturl+"includes/mod_project.php", {
            'map_centre_set' : $("#map-centre").val(),
            'map_zoom_set' : $("#map-zoom").val()
        },
            function(data) {
        });
    });

    /* project admin: mapserver test
     * */
    $("body").on('click','.maptest',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length); //public,private
        var icon = $(this).find("i");
        $(icon).removeClass('fa-eye');
        $(icon).addClass('fa-spinner fa-spin');
        $.post(projecturl+"includes/afuncs.php", {'maptest':widgetId },
            function(data){
                if (data=='no') {
                    $(icon).removeClass('fa-spinner fa-spin');
                    $(icon).addClass('fa-eye');
                    $("#maptest-"+widgetId).html("an error occured while the request");
                } else if (data=='.') {
                    $("#maptest-"+widgetId).html("processing... click on view button later to refresh");
                } else {
                    $("#maptest-"+widgetId).html(data);
                    $(icon).removeClass('fa-spinner fa-spin');
                    $(icon).addClass('fa-eye');
                }
            });
    });
    /* project admin  - create function */
    $("body").on('click','.createfunction',function(){
        var id=$(this).attr('id');
        var table=id.substring(id.indexOf('-')+1,id.length);
        var schema = $(this).data('schema'); 
        var funcs=id.substring(0,id.indexOf('-'));
        $.post(projecturl+"includes/afuncs.php", {'create-function':funcs,'table':table, 'schema': schema },
            function(data){
                alert(data);
                //$( "#dialog" ).text(data);
               //var isOpen = $( "#dialog" ).dialog( "isOpen" );
               //if(!isOpen) $( "#dialog" ).dialog( "open" );
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions&mtable='+schema +'.'+ table);
            });
    });
    /* project admin  - create function */
    $("body").on('click','.enablefunction',function(){
        var id = $(this).attr('id');
        var table = id.substring(id.indexOf('-')+1,id.length);
        var schema = $(this).data('schema');
        var funcs = id.substring(0,id.indexOf('-'));
        $.post(projecturl+"includes/afuncs.php", {'enable-trigger':funcs,'table':table,'schema':schema },
            function(data){
                alert(data);
                //$( "#dialog" ).text(data);
               //var isOpen = $( "#dialog" ).dialog( "isOpen" );
               //if(!isOpen) $( "#dialog" ).dialog( "open" );
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions&mtable='+schema +'.'+table);
            });
    });

    /* project admin  - create function */
    $("body").on('click','.trigger-onoff',function(){
        var id=$(this).attr('id');
        var table=$(this).data('table');
        var schema=$(this).data('schema');
        $.post(projecturl+"includes/afuncs.php", {'enable-trigger-general':id, 'table': table, 'schema': schema},
            function(data){
                alert(data);
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions&mtable='+schema +'.'+table);
            });
    });

    /* project admin - */
    $("body").on('click','.rule-save',function() {
        var id=$(this).attr('id');
        var table=id.substring(id.indexOf('-')+1,id.length);
        var schema = $(this).data('schema');
        $.post(projecturl+"includes/afuncs.php", {'rule-save':1,'rule-function':$('#rule-function').text(),'table':table, 'schema': schema },
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    console.log(retval['data']);
                    alert(retval['message']);
                } else if (retval['status']=='success') { 
                    alert('ok!');
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=functions&mtable='+schema +'.'+table);
                }
            });
    });

    /* project admin - upload forms
     * delete form
     * */
    $('#tab_basic').on('click','#delform',function(){
        var tag = $(this).attr("data-tag");
        translations = {"str_yes_i_want":"Delete","str_no_thanks":"No!!","str_confirm":"Confirm"};
        if (typeof tag !== "undefined") {
                openConfirmDialog(
                    'Delete??',
                    [{
                        text: translations['str_yes_i_want'],
                        icons: { primary: "ui-icon-flag" },
                        click: function() {
                            $.post(projecturl+"includes/afuncs.php", {'delform':tag },
                            function(data){
                                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
                            });
                            $( this ).dialog( "close" );
                        }
                    },{
                        text: translations['str_no_thanks'],
                        icons: { primary: "ui-icon-cancel" },
                        click: function() {
                            $( this ).dialog( "close" );
                        }

                    }],
                    translations['confirm'],
                    'auto',
                    400
                )

        }
    });
    /* project admin - upload forms
     * edit form
     * */
    $('#tab_basic').on('click','#editform',function(){
        var tag = $(this).attr("data-tag");
        if (typeof tag !== "undefined") {
            //$('#tab_basic').load('includes/project_admin.php?options=upload_forms&edit_form='+tag,function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms&edit_form='+tag+'&simplified_list=0');
        }
    });
    $('#tab_basic').on('click','.list-editor',function() {
        $(".list-editor").prop('disabled',false);
        var id=$(this).attr('id');
        var wid=id.substring(id.indexOf('-')+1,id.length);
        var t = $(this).attr("data-simplified");
        var d = $(this).text();
        $("#list-editor-return").val(id);
        $("#list-editor-json").text(d);
        $("#list-editor-json").val(d);
        $("#list-editor").data('wid',wid);
        $('#list-editor-help-box').show();
        $("#list-editor").show();
        $("#list-editor").draggable();
        $(this).prop('disabled',true);
    });

    $('#tab_basic').on('click','#list-editor-cancel',function() {
        var id = $("#list-editor-return").val();
        $("#list-editor").hide();
        $("#"+id).prop('disabled',false);
        $(".item-container").remove();
        $("body").trigger('list-editor-cancel');
    });
    $('#tab_basic').on('click','#list-editor-save',function() {
        var id = $("#list-editor-return").val();
        
        $.post(projecturl+"includes/afuncs.php",{'list-validator':$("#list-editor-json").val()},function(data){

            if (data) {
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    console.log(retval['data']);
                    alert(retval['message']);
                } else if (retval['status']=='success') { 
                    $("#"+id).val($("#list-editor-json").val());
                    $("#"+id).text($("#list-editor-json").val());
                    $("#list-editor").hide();
                    $("#"+id).prop('disabled',false);
                    $("body").trigger('list-editor-save-success');
                }
            }
        });


    });

    $('#tab_basic').on('click','.add-item',function() {
        var id = $(this).text();
        var editor_string = $("#list-editor-json").val(); 
        var j;
        try {
            j = JSON.parse(editor_string);
        } catch(e) {
            j = {};
        }
        if (id == 'list') { 
            if ( ! j.hasOwnProperty('list') )
                j["list"] =  { item: [ "label" ], };
        } else if (id == 'optionsSchema') {
            if ( ! j.hasOwnProperty('optionsSchema') )
                j["optionsSchema"] = "";
        } else if (id == 'optionsTable') {
            if ( ! j.hasOwnProperty('optionsTable') )
                j["optionsTable"] = "";
        } else if (id == 'valueColumn') {
            if ( ! j.hasOwnProperty('valueColumn') )
                j["valueColumn"] = "";
        } else if (id == 'labelColumn') {
            if ( ! j.hasOwnProperty('labelColumn') )
                j["labelColumn"] = "";
        } else if (id == 'filterColumn') {
            if ( ! j.hasOwnProperty('filterColumn') )
                j["filterColumn"] = "";
        } else if (id == 'pictures') {
            if ( ! j.hasOwnProperty('pictures') )
                j["pictures"] = { item: "url" };
        } else if (id == 'triggerTargetColumn') {
            if ( ! j.hasOwnProperty('triggerTargetColumn') )
                j["triggerTargetColumn"] = [""];
            else
                j["triggerTargetColumn"] = [ j["triggerTargetColumn"] ];
        } else if (id == 'Function') {
            if ( ! j.hasOwnProperty('Function') )
                j["Function"] = "";
        } else if (id == 'disabled') {
            if ( ! j.hasOwnProperty('disabled') )
                j["disabled"] = ["item"];
        } else if (id == 'preFilterColumn') {
            if ( ! j.hasOwnProperty('preFilterColumn') )
                j["preFilterColumn"] = "";
        } else if (id == 'preFilterValue') {
            if ( ! j.hasOwnProperty('preFilterValue') )
                j["preFilterValue"] = "";
        } else if (id == 'multiselect') {
            if ( ! j.hasOwnProperty('multiselect') )
                j["multiselect"] = true;
        } else if (id == 'selected') {
            if ( ! j.hasOwnProperty('selected') )
                j["selected"] = ["item"];
        } else if (id == 'size') {
            if ( ! j.hasOwnProperty('size') )
                j["size"] = 3;
        } else if (id == 'add_empty_line') {
            if ( ! j.hasOwnProperty('add_empty_line') )
                j["add_empty_line"] = true;
        }
        
        $("#list-editor-json").val(JSON.stringify(j,null,2));
        
    });
    // update list JS according to selected item
    $('#tab_basic').on('click','.item-element',function() {
        var t = $(this).html();

        update_list($(this).data('id'), t);

        var tparent = $(this).parent().parent();
        tparent.find('.item-container').remove();
        tparent.html(t);
        return false;
    });

    // append select intem box to current value element
    $('#tab_basic').on('click','.item-content',function() {
        var id = $(this).data('type');
        var value = $(this).data('value');
        var schema = '';
        if (typeof $(this).data('schema') !== 'undefined')
            schema = $(this).data('schema');
        var thise = $(this);

        var editor_string = $("#list-editor-json").val(); 
        var j;
        try {
            j = JSON.parse(editor_string);
            if (schema!='' & j.hasOwnProperty(schema))
                schema = j[schema];
            else
                schema = 'public';
        } catch(e) {
            j = {value:''};
        }

        $.post(projecturl+"includes/afuncs.php",{'list-content-parser':id,'value':j[value],'schema':schema},function(data){
            j = JSON.parse(data);

            var list = "";
            for (var i=0;i<j.length;i++) {
                list += "<li class='item-element' data-id='"+id+"'>"+j[i]+"</li>";
            }
            thise.append("<ul class='item-container'>"+list+"</ul>");
        });
    });
    /* project admin - upload forms
     * activate/deactivate form
     * */
    $('#tab_basic').on('click','#deacform',function(){
        var tag = $(this).attr("data-tag");
        var val = $(this).attr("data-val");
        if (typeof tag !== "undefined") {
            if (typeof val === "undefined") { 
                val = '';
            }
            $.post(projecturl+"includes/afuncs.php", {'deacform':tag,'active':val },
            function(data){
                //$('#tab_basic').load('includes/project_admin.php?options=upload_forms',function(result){});
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
            });
        }
    });
    /* project admin - upload form
     * select a field: change background color
     * */
    $("#tab_basic").on('click','.uplf_ckb',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($(this).is(":checked")) {
            if ($('#uplf_obl_y-'+widgetId).is(":checked")) {
                $('.uplf_color3-'+widgetId).css('background-color', '#FF636E');//set red
            }
            else if ($('#uplf_obl_n-'+widgetId).is(":checked")) {
                $('.uplf_color3-'+widgetId).css('background-color', '#B6B6B6'); //set gray
            }
            else if ($('#uplf_obl_s-'+widgetId).is(":checked")) {
                $('.uplf_color3-'+widgetId).css('background-color', '#FFB6BB'); //set pink
            } 
            else {
                $('.uplf_color3-'+widgetId).css('background-color', '#FF636E');//set red
                $('#uplf_obl_y-'+widgetId).attr('checked',true);
            }
        } else {
            $('.uplf_color1-'+widgetId).css('background-color', '#FFFFFF'); //not selected
            $('.uplf_color2-'+widgetId).css('background-color', '#FFFFFF'); //not selected
            $('.uplf_color3-'+widgetId).css('background-color', '#FFFFFF'); //not selected
        }
    });
    /* project admin - upload form
     * */
    $("#tab_basic").on("change","#form_table",function(){
        var table = $(this).val();
        //$('#tab_basic').load('includes/project_admin.php?options=upload_forms&new_form_table='+table,function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms&editor=new&new_form_table='+table);
    });
    /* project admin - upload form
     * input mode: list/text change
     * */
    $("body").on('change','.uplf_type',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($('#uplf_type-'+widgetId).val()=='boolean') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_length-'+widgetId).val('nocheck');
            $('#uplf_list-'+widgetId).val('false,true,');
        } else if ($('#uplf_type-'+widgetId).val()=='autocomplete') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_length-'+widgetId).val('nocheck');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "SELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='list') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_length-'+widgetId).val('nocheck');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "name:value,name:value OR \nvalue,value,value OR \nSELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='crings') {
            $('#uplf_count-'+widgetId).val('');
            $('#uplf_length-'+widgetId).val('nocheck');
            $('#uplf_list-'+widgetId).val('');
            $('#uplf_list-'+widgetId).attr("placeholder", "name:value,name:value OR \nvalue,value,value OR \nSELECT:table.column");
        } else if ($('#uplf_type-'+widgetId).val()=='text' || $('#uplf_type-'+widgetId).val()=='numeric' || $('#uplf_type-'+widgetId).val()=='time' || $('#uplf_type-'+widgetId).val()=='point' ||  $('#uplf_type-'+widgetId).val()=='line' || $('#uplf_type-'+widgetId).val()=='polygon' || $('#uplf_type-'+widgetId).val()=='wkt') {
            $('#uplf_list-'+widgetId).val('');
        } else {
            $('#uplf_list-'+widgetId).val('');
        }
    });
    /* choose 
     *
     * */
    $("body").on('change','#file_manager_choose_table',function(){
        var t = $(this).val();
        //$('#tab_basic').load('includes/project_admin.php?options=files&choose='+t,function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=files&choose='+t);
    });
    $("body").on('change','#filter_files',function(){
        var t = $(this).val().replace(/ /g,"%20");
        var column = $("#filter_files_columns").val();
        //$('#tab_basic').load('includes/project_admin.php?options=files&choose='+t,function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=files&choose='+$('#file_manager_choose_table').val()+'&filter_file='+t+'&filter_column='+column);
    });
    
    /* project admin - upload form
     * obligatory: change background color
     * */
    $("body").on('change','.uplf_obl',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($(this).is(":checked")) {
            if($(this).val()==1)
                $('.uplf_color3-'+widgetId).css('background-color', '#FF636E') //set red
            else if($(this).val()==3)
                $('.uplf_color3-'+widgetId).css('background-color', '#FFB6BB') //set pink
            else
                $('.uplf_color3-'+widgetId).css('background-color', '#B6B6B6') // set gray
            $('#uplf_ckb-'+widgetId).attr('checked','true');
        }
    });
    $("body").on('change','.uplf_relation',function(){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);

        if ($('#uplf_ckb-'+widgetId).is(":checked")) {
            if ($(this).val()!='') {
                $('.uplf_color2-'+widgetId).css('background-color', '#73CBFE') //set blue
            } else {
                $('.uplf_color2-'+widgetId).css('background-color', '#FFFFFF') //set blue
            }
        }
    });

    $("body").on('change','#form_access',function(){
        if($(this).val()==2) {
            $("#form_group_access").attr('disabled',false);
            $("#form_group_access").removeClass('button-passive');    
        } else {
            $("#form_group_access").attr('disabled',true);
            $("#form_group_access option:selected").removeAttr("selected");
            $("#form_group_access").addClass('button-passive');    
        }
    });
    /* project admin - upload form - upload a list file
     * Save file content through afuncs.php
     * */
    $("body").on('change','.uplf_file',function(event) {
        //ennek a tartalma kimehetne egy paraméterezhető függvénybe...
        //file_auto_upload('4096',ajax,[files=>file,list=>0])
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var files = document.getElementById("uplf_file-"+widgetId).files;
        //file_upload(files,'list');
        var u = new Uploader;
        u.param('list');
        u.file_upload(files);
    });

    //Array.prototype.diff = arr1.filter(x => arr2.includes(x));
    Array.prototype.diff = function(a) {
        return this.filter(function(i) {return a.indexOf(i) < 0;});
    };

    $("body").on('input','.available_item_trigger',function(event) {
        
        var nameArray = new Array();
        var allItems = new Array();
        var o = 1;
        $('.available_item_trigger').each(function() {
            nameArray.push(Number($(this).val()));
            allItems.push(o);
            o++;
        });
        var uA = nameArray.filter(function (el) {
          return el != null;
        });

        var uAdiff = allItems.diff(uA)
	
        var dataList = $("#available_items");
	dataList.empty();

        for (var i = 0; i < uAdiff.length; i++) {
            dataList.append("<option value='" + uAdiff[i] + "'>");
        }
    });

    /* project admin - language files
     * Save file content through afuncs.php
     * */
    $("body").on('change','.language_file',function(event) {
        //ennek a tartalma kimehetne egy paraméterezhető függvénybe...
        //file_auto_upload('4096',ajax,[files=>file,list=>0])
        var id=$(this).attr('id');
        var files = document.getElementById("language_file").files;
        var u = new Uploader;
        u.param('language-files');
        u.file_upload(files);
        //$('#tab_basic').load('includes/project_admin.php?options=languages',function(result){});
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options=languages');
    });
    /* project admin - language files
     * Save file content through afuncs.php
     * */
    $("body").on('change','#module_file-upload',function(event) {
        var id=$(this).attr('id');
        var files = document.getElementById("module_file-upload").files;
        var u = new Uploader;
        u.param('module-files');
        u.param2($(this).attr('name')); // owerwrite file-name
        u.complete('alertresult');
        u.file_upload(files);
        //$('#tab_basic').load('includes/project_admin.php?options=modules',function(result){});
        //$('#tab_basic').load(projecturl+'includes/project_admin.php?options=modules');
  
    });
    $("body").on('change','.job-file-upload',function(event) {
        var id=$(this).attr('id');
        var file = document.getElementById(id).files;
        var u = new Uploader;
        u.param('job-files');
        u.param2($(this).data('type')); // run/lib
        u.param3($(this).attr('name')); // jobname
        u.complete('alertresult');
        u.file_upload(file);
    });

    // Upoad forms
    $("body").on('change','#form_observationlist_mode',function(event){
        if ($(this).val() != 'false') {
            $("#form_observationlist_time").prop('disabled',false);
            $("#form_tracklog_mode").prop('disabled',false);
        } else {
            $("#form_observationlist_time").prop('disabled',true);
            $("#form_tracklog_mode").prop('disabled',true);

        }
    });
    // Upload form update/save
    $("body").on('click','#form-draft-submit',function(event) {
        $("#form_name").val($("#form_name").val() + ':draft_save');
        upload_form_save('draft');
    });
    $("body").on('click','#form-publish',function(event) {
        upload_form_save('publish');
    });
    $("body").on('click','#form-update',function(event) {
        upload_form_save('update');
    });


    //show-hide min-max value input field
    $("body").on('change','.uplf_length',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if($(this).val()=='minmax') {
            $("#uplf_count-"+widgetId).attr('placeholder','0:20');
        } else if ($(this).val()=='regexp') {
            $("#uplf_count-"+widgetId).attr('placeholder','/[a-z]{2}\.[a-z]{2}\|[a-z]{2}\.[a-z]{2}/');
        } else {
            $("#uplf_count-"+widgetId).attr('placeholder','');
        }
    });

    /* project_admin - upload forms */
    $("body").on('change','.fullist_option',function(){
        var this_id = $(this).attr('id');
        var this_widgetId=this_id.substring(this_id.indexOf('-')+1,this_id.length);
        var this_val= $(this).val();
        if (this_val==0) return;
        $(".fullist_option").each(function() {
            var id = $(this).attr('id');
            var widgetId=id.substring(id.indexOf('-')+1,id.length);
            if(this_id!=id && $(this).val()==1){
                    $("#uplf_fullist-"+widgetId).val('0').change();
            }
        });
    });
    /* project_admin, module update */ 
    $("body").on('click','.module_update',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);

        if (widgetId != 'new' && typeof myCM.widgetId != "undefined") {
            myCM.widgetId.save();
            myCM.widgetId.toTextArea();
        }

        var module_type = $(".main_table_refresh").attr('id');
        var name=$("#module-name_"+widgetId).val();
        var file=$("#module-file_" +widgetId).val();
        if (widgetId == 'new') {
            file = name.substring(name.indexOf('/')+1,name.length);
            name = name.substring(0,name.indexOf('/'));
        }
        var func=$("#module-function_" +widgetId).val();
        var pars=$("#module-params_" +widgetId).val();
        var enab_=$("#module-ena_" + widgetId);
        var enab = 'f';
        if (enab_.is(':checked')) {
            enab = enab_.val();
        }
        var acce=$("#module-access_" + widgetId).val();
        var gacce=$("#module-gaccess_" + widgetId).val();
        var mtable=$(".main_table_selector").val();

        $.post(
            projecturl+"includes/mod_project.php", 
            { 
                'module_type':module_type,
                'update_modules':widgetId,
                'file':file,
                'name':name,
                'func':func,
                'params':pars,
                'enab':enab,
                'access':acce,
                'mtable':mtable,
                'gaccess':gacce 
            },
            function(data) {
                var retval = jsendp(data);
                if (retval['status']=='success') {

                    retval['message'] = "OK";
                    
                    if ( /^(DROP|NEW|UPDATE):/.test(retval['data']) ) {
                        var mtable = retval['data'].match(/(DROP|NEW|UPDATE):(\w+)/)[2];
                        alert("OK");
                        $('#tab_basic').load(projecturl+'includes/project_admin.php?options='+module_type+'&mtable='+mtable);
                        return;
                    }
                    //$("#module-params-" +widgetId).val(retval['data']);
                }
                openConfirmDialog(
                    retval['message'],
                    [{
                        text: 'OK',
                        icons: { primary: "ui-icon-flag" },
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }],
                    'Response',
                    'auto',
                    400
                )
            }
        );
    });
    $("body").on('click','.editorArea',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        $("#columnref_"+widgetId).show();

        myCM.widgetId = CodeMirror.fromTextArea(document.getElementById(id),{
            matchBrackets: true,
            lineNumbers: false,
            indentUnit: 4,
            smartIndent: true,
            autoCloseTags: true,
            autoCloseBrackets: true,
            lineWrapping: true,
            mode: {
                name: "javascript",
                json: true,
                statementIndent: 2
            },
        });
        $('.CodeMirror').resizable({
            resize: function() {
            myCM.widgetId.setSize($(this).width(), $(this).height());
            }
        });

        $(".CodeMirror").css( { "border": "1px solid orange", "width": "500px","height": "200px"});
    });
    $('#tab_basic').on('click','.add-column',function() {
        if ($(this).has('ul').length == 1) {
            $(this).find('.item-container').remove();
            return true;
        }
        var table = $(this).data('table'); 
        var schema = $(this).data('schema'); 
        var thise = $(this);
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);

        $.post(projecturl+"includes/afuncs.php",{'list-content-parser':'valueColumn','value':table,'schema':schema},function(data){
            j = JSON.parse(data);

            var list = "";
            for (var i=0;i<j.length;i++) {
                list += "<li class='item-column' data-ref='"+widgetId+"'>"+j[i]+"</li>";
            }
            thise.append("<ul class='item-container'>"+list+"</ul>");
        });
    });
    // update list JS according to selected item
    $('#tab_basic').on('click','.item-column',function() {
        var t = $(this).html(); // column name

        var ref = $(this).data('ref');

        update_json(''+ref+'',t);

        var tparent = $(this).parent().parent();
        tparent.find('.item-container').remove();
        return false;
    });

    /* project_admin, query set update */ 
    $("body").on('click','.q_update',function(){
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var name=$("#qnam-" +widgetId).val();
        var typ=$("#qtype-" +widgetId).val();
        var query=$("#qquery-" + widgetId).val();
        var cname=$("#qcname-" + widgetId).val();
        var rst=$("#qrst-" + widgetId).val();
        var geom_type=$("#qgt-" + widgetId).val();
        var enab=$("#qena-" + widgetId).val();

        $.post(projecturl+"includes/update_mapfile.php", { 
            'qq':widgetId,
            'name':name,
            'type':typ,
            'query':query,
            'enab':enab,
            'rst':rst,
            'cname':cname,
            'geom_type':geom_type 
        },
        function(data){
            //$('#tab_basic').load('includes/project_admin.php?options=query_def',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=query_def');
        });
    });

    /* project_admin, openlayers update */ 
    $("body").on('click','.opl_update',function(){

        cm_json.toTextArea();
        
        var id = $(this).attr('id');
        var widgetId=id.substring(id.indexOf('_')+1,id.length);
        var order=$("#oplorder-"+widgetId).val();
        var name=$("#oplnam-" +widgetId).val();
        var typ=$("#opltype-" +widgetId).val();
        var def=$("#opldef-" + widgetId).val();
        var url=$("#oplurl-" + widgetId).val();
        var map=$("#oplmap-" + widgetId).val();
        var desc=$("#opldesc-" +widgetId).val();
        var enab=$("#oplena-" + widgetId).val();
        var mslayer=$("#oplmslayer-" + widgetId).val();
        var singletile=$("#oplsingletile-" + widgetId).val();
        var legend=$("#opllegend-" + widgetId).val();

        $.post(projecturl+"includes/update_mapfile.php", { 
            'op':widgetId,
            'order':order,
            'name':name,
            'type':typ,
            'def':def,
            'url':url,
            'map':map,
            'desc':desc,
            'enab':enab,
            'mslayer':mslayer,
            'singletile':singletile,
            'legend':legend
        },
        function(data){
            //$('#tab_basic').load('includes/project_admin.php?options=openlayes_def',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=mapserv');
        });
    });
    //project_admin, mapfile update
    $("body").on('click','.map_update',function(){
        var k = $(this).attr('id');
        //$.post(projecturl+"includes/update_mapfile.php", { 'map_post':$("#" + k + "_map").val(),'p':k,'map_layers':$('#mapfile-form').serialize() },
        var values = $('#mapfile-form').serializeArray();
        values.push({
            name: "map_post",
            value: $("#" + k + "_map").val()
        });
        values.push({
            name: "p",
            value: k
        });
        values = jQuery.param(values);
        
        $.post(projecturl+"includes/update_mapfile.php", values,
        function(data){
            //$("#" + k + "_map").html(data);
            //$('#tab_basic').load('includes/project_admin.php?options=mapserv',function(result){});
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=mapserv');
        });
    });
    /* project_admin - mapserver */
    $("body").on('change','#public_map',function(){
        $("#pub-icon").addClass('fa-spin'); 
        $("#public").removeClass('button-success'); 
        $("#public").addClass('button-warning'); 
    });
    /* project_admin - mapserver */
    $("body").on('change','#private_map',function(){
        $("#priv-icon").addClass('fa-spin'); 
        $("#private").removeClass('button-success'); 
        $("#private").addClass('button-warning'); 
    });



/***********************************************\
 
            Custom polygon management

\***********************************************/

    // Change save button's color to green when changing name of the polygon
    $("body").on('change','.polygon_name',function(){
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var save_button = $(this).parent().find('.polygon_name_save');
        save_button.removeClass('button-secondary');
        save_button.removeClass('button-gray');
        save_button.addClass('button-success');
    });

    // Polygon update: Change polygon-access policy
    // Select menu
    $("body").on('change','.polygon_access',function(){
        var polygon_id = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);

        var _location = $("#tab_basic").find('#location');

        $.post("ajax",{'polygon_update':polygon_id,'polygon_access':$(this).val()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                $("#poly-users-force0_"+polygon_id+" option").each(function(){
                    $(this).removeAttr('disabled');
                });
            }
        });
    });

    // Polygon update: update polygon name 
    // Save button
    $("body").on('click','.polygon_name_save',function(){
        var id = $(this).attr('id');
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var this_ = $(this);

        polygon_name = $(this).parent().find('.polygon_name').val();

        $.post("ajax",{'polygon_update':widgetId,'polygon_name':polygon_name},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
                this_.removeClass('button-success');
                this_.addClass('button-fail');
            } else if (retval['status']=='success') {
                this_.removeClass('button-gray');
                this_.removeClass('button-success');
                this_.addClass('button-secondary');
            }
        });
    });


    // select / upload view options for own polygons
    $("body").on('click','.polygon_show_options',function(){
        var id = $(this).attr('id');
        var widgetId = id.substring(id.indexOf('_')+1,id.length);
        var current_view_setting=$(this).val();
        var type = '';
        if ($(this).hasClass('sel'))
            type = 'select';
        if ($(this).hasClass('upl'))
            type = 'upload';

        $.post("ajax",{'showhide_polygon':widgetId,'show':current_view_setting,'type':type},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                $("#"+id).toggleClass('button-success');
                $("#"+id).toggleClass('button-gray');
                
                if (retval['data'] == 'none') {
                    $("#ops_"+widgetId).val('none');
                    $("#opu_"+widgetId).val('none');
                }
                else if (retval['data'] == 'only-select') {
                    $("#ops_"+widgetId).val('only-select');
                    $("#opu_"+widgetId).val('only-select');
                }
                else if (retval['data'] == 'only-upload') {
                    $("#ops_"+widgetId).val('only-upload');
                    $("#opu_"+widgetId).val('only-upload');
                }
                else if (retval['data'] == 'select-upload') {
                    $("#ops_"+widgetId).val('select-upload');
                    $("#opu_"+widgetId).val('select-upload');
                }
            }
        });
    });


    // 
    // Admin page options for polygons
    //

    // select / upload view options for users in polygons admin page
    $("body").on('click','.polygon_showforce_options',function(){

        var id = $(this).attr('id');
        var row_id = id.substring(0,id.indexOf('_')).match(/\d+$/)[0]; // Az első aláhúzásig található számok
        var polygon_id = id.substring(id.indexOf('_')+1,id.indexOf('-')); // Az első aláhúzástól az első kötőjelig
        var user_id = id.substring(id.indexOf('-')+1,id.length); // Az első kötőjeltől a végig
        
        var current_view_setting=$(this).val();
        var type = '';
        if ($(this).hasClass('sel'))
            type = 'select';
        if ($(this).hasClass('upl'))
            type = 'upload';

        $.post("ajax",{'force_showhide_polygon':polygon_id,'show':current_view_setting,'type':type,'force_users':user_id},function(data) {
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
            } else if (retval['status']=='success') {
                
                // set colours
                if ( ($("#i"+id).hasClass('fa-map-o') || $("#i"+id).hasClass('fa-upload')) && $("#"+id).hasClass('button-success')) {
                     $("#"+id).removeClass('button-success');
                     $("#"+id).addClass('button-gray');
                } else {
                     $("#"+id).removeClass('button-gray');
                     $("#"+id).removeClass('button-success');
                     $("#"+id).addClass('button-success');
                }
                
                // set value
                if (retval['data'] == 'none') {
                    $("#ops"+row_id+"_"+polygon_id+"-"+user_id).val('none');
                    $("#opu"+row_id+"_"+polygon_id+"-"+user_id).val('none');
                    $("#"+id).closest("tr").hide();
                }
                else if (retval['data'] == 'only-select') {
                    $("#ops"+row_id+"_"+polygon_id+"-"+user_id).val('only-select');
                    $("#opu"+row_id+"_"+polygon_id+"-"+user_id).val('only-select');
                }
                else if (retval['data'] == 'only-upload') {
                    $("#ops"+row_id+"_"+polygon_id+"-"+user_id).val('only-upload');
                    $("#opu"+row_id+"_"+polygon_id+"-"+user_id).val('only-upload');
                }
                else if (retval['data'] == 'select-upload') {
                    $("#ops"+row_id+"_"+polygon_id+"-"+user_id).val('select-upload');
                    $("#opu"+row_id+"_"+polygon_id+"-"+user_id).val('select-upload');
                }
            }
        });
    });
    // Add users to polygon_users in polygons admin page
    $("body").on('change','.poly_users_force',function(){
        var id = $(this).attr('id');
        var tr_index = $(this).closest('tr').index();
        var row_id = id.substring(0,id.indexOf('_')).match(/\d+$/)[0];
        var polygon_id = id.substring(id.indexOf('_')+1,id.length);
        var user_id = $(this).val();
        var user_name = $(this).find(":selected").text();
        var polygon_name = $("#ope"+row_id+"_"+polygon_id).val();

        //var current_view_setting =  $("#ops"+row_id+"_"+polygon_id).val();

        var _location = $("#tab_basic").find('#location');

        $.post("ajax",{'force_showhide_polygon':polygon_id,'show':'select-upload','type':'','force_users':user_id},function(data) {
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
            } else if (retval['status']=='success') {
                // Add user line instead of reloading
                var row = '<tr class="z_'+polygon_id+'"><td><input type="hidden" value="'+polygon_name+'"></td><td>'+user_name+'</td><td colspan="2"><button class="pure-button button-success button-small polygon_showforce_options sel" id="ops'+row_id+'_'+polygon_id+'-'+user_id+'" value="select-upload" title="Enabled for selection (web map)"><i id="iops'+row_id+'_'+polygon_id+'-'+user_id+'" class="fa fa-map fa-2x"></i></button> <button class="pure-button button-success button-small polygon_showforce_options upl" id="opu'+row_id+'_'+polygon_id+'-'+user_id+'" value="select-upload" title="Enabled for upload (web/mobile)"><i id="iopu'+row_id+'_'+polygon_id+'-'+user_id+'" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button></td></tr>';
                $('.resultstable > tbody > tr').eq(tr_index).after(row);
            }
        });
    });

        

//
//  END of custom polygon management
// 




    /* drop saved import */
    // admin page - drop any import
    $('#tab_basic').on('click','.dropimport',function() {
        var id = $(this).data('id');
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", obj.str_intr_uploads);
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: obj.str_yes_sure,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'drop_1import':id},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=imports');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.str_no_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).html( obj.str_drop_intr_uplds_question );
        $( "#dialog-confirm" ).height('auto');
    });

    /* invitation */
    $( '#body' ).on( "click","#soi", function() {
         sendInvitation();
    });
    /* invitations - drop invites button click */
    $('#body').on('click','#dropInv',function(){
        var d = [];
        $(".delinv:checked").each(function() {
            d.push(this.value);
        });
        $.post(projecturl+"includes/afuncs.php", {delinv:d}, function(data){
            var url = $("#invites").attr("data-url");
            if (typeof url !== "undefined") {
                var pane = $("#invites"), href = $('#invites').attr('href');
                // ajax load from data-url
                //$(href).load(url,function(result){ pane.tab('show'); });
                $(href).load(projecturl+url);
            }
        });
    });
    /* create new project
     * add new database fields row
     * */
    $("body").on('click','#fields-def_add-cell',function(e) {
        e.preventDefault();
        $("#fields_def_table").append("<tr><td><input name='name[]' placeholder='"+str_obligatory+"' require></td><td><input name='desc[]' placeholder='"+str_obligatory+"' require></td><td><textarea name='meta[]' placeholder='"+str_recommended+"'></textarea></td><td><select name='type[]'><option value='numeric'>numeric</option><option value='integer'>integer</option><option value='text'>text</option><option value='date'>datum</option><option value='timestamp without time zone'>datetime</option></select></td></tr>");
    });
    /* create new project */
    $("body").on('dblclick',"#new_proj_messages",function(){
        $(this).hide();
    });
    /* create new project - generate password */ 
    $("body").on('change','#project_dir',function () {
        $("#pgsql_admin").val($(this).val()+'_admin');
        $("#pgsql_passwd").val(Math.random().toString(36).slice(2));
    });
    /* create new project
     * - create new project button
     * */
    $("body").on('click','#send_newproj_req',function () {
        //var d = $(this).closest('form').serialize();
        var form = document.getElementById('new_project');
        var d = new FormData(form);
        $.ajax({
            url: projecturl+"includes/create_new_project.php",
            type: "POST",
            data: d,
            processData: false,
            contentType: false,
            success: function(data) {
                $("#new_proj_messages").html(data);
                $("#new_proj_messages").css({height: '600px'});
                $("#new_proj_messages").show();
                //$('#message').css({top:'50%',left:'50%',margin:'-'+($('#message').height() / 2)+'px 0 0 -'+($('#message').width() / 2)+'px'});
                //$('#message').show();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    });

    /* SQL Console */

    $("#sql-console").draggable({
            cursor: "move",
            zIndex: 10000,
            handle: ".modal-draggable-handle"
    });
    //$("#sql-cmd").resizable({ minHeight: 100, minWidth: 600});

    $( "#sql-console" ).on( "dragstop", function( event, ui ) {
        $(this).css('height','auto');
    });

    $("#sql-console").resizable({minHeight: 570, minWidth: 600, alsoResize: "#sql-answer"});
    //$("#sql-answer").resizable();

    $("body").on("click","#open-sql-console",function(e) {
        e.preventDefault();
        $('#sql-answer').empty();
        $("#sql-console").show();
    });

    $("body").on("click","#send-sql-cmd",function(e) {
        $.post('ajax',{'send-sql-cmd':$('#sql-cmd').text()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                
                $('#sql-answer').css({'grid-template-columns':'auto'});
                $('#sql-answer').html(retval['message']);

            } else if (retval['status']=='fail') {

                $('#sql-answer').css({'grid-template-columns':'auto'});
                $('#sql-answer').html(retval['message']);
            } else if (retval['status']=='success') {

                $('#sql-answer').html('');
                var parsed = retval['data'];
                if (parsed == false) {
                    $('#sql-answer').html(str_no_rows);
                }
                
                // (C) TABLE HEADER
                var theWrap = document.getElementById("sql-answer");
                var theCell = null;
                var i = 0;
                for (let key in parsed[0]) {
                  theCell = document.createElement("div");
                  theCell.innerHTML = key;
                  theCell.classList.add("cell");
                  theCell.classList.add("head");
                  theWrap.appendChild(theCell);
                  i++;
                }
                // pg_affected_rows()
                if (i == 0) {
                    $('#sql-answer').html(str_affected_rows + ': ' + parsed);
                }

                // (D) TABLE CELLS
                var thePerson = null;
                var altRow = false;
                for (let key in parsed) {
                  thePerson = parsed[key];
                  for (let i in thePerson) {
                    theCell = document.createElement("div");
                    theCell.innerHTML = thePerson[i];
                    theCell.classList.add("cell");
                    theCell.classList.add("disable-scrollbars");
                    if (altRow) { 
                      theCell.classList.add("alt"); 
                    }
                    theWrap.appendChild(theCell);
                  }
                  altRow = !altRow;
                }

                $('#sql-answer').css({'grid-template-columns':'repeat('+i+', minmax(100px, 1fr)'});
            }


        });
    });
    $("body").on("focusout blur","#sql-cmd",function(e) {
        $.post('ajax',{'sql-cmd-highlight':$('#sql-cmd').text()},function(data){
            var j = JSON.parse(data);
            $("#sql-cmd").html(j[0]);
        });
    });
    $("body").on("click","#sql-console-close",function(e) {
        $("#sql-console").hide();
        $('#sql-answer').empty();
    });

    /* Switch profile */
    $("body").on("click","#switchprofile",function(e) {
        e.preventDefault();

        $.post('ajax',{'switchprofile':$('#switchprofile').attr("data-switchprofile")},function(data){
            $('#login-box').html(data);
            $('#login-box').show();
        });
    });
    $("body").on("click","#reauth",function(e) {
        e.preventDefault();

        var values = $(this).parent().serializeArray();
        $.post('ajax',{'reauth':values},function(data){
            if (data=='reload_profile') {
                location.reload();
            }
        });
        // sql-console
        $(this).parent().remove();
    });

    // project admin - syslog
    $("body").on('change','#syslog_source',function(){
        var n = $(this).val();
        var filter = btoa($('#syslog_filter').val());
        $('#logconsole').load(projecturl+'includes/project_admin.php?options=server_logs&update&filter='+filter+'&source='+n);
    });
    $("body").on('click','#syslog_refresh',function(){
        var n = $('#syslog_source').val();
        var filter = btoa($('#syslog_filter').val());
        $('#logconsole').load(projecturl+'includes/project_admin.php?options=server_logs&update&filter='+filter+'&source='+n);
    
    });
    
    // project admin - main table choose in several pages
    $("body").on('change','.main_table_selector',function() {
        var id = $(this).attr('id');
        var n = $(this).val();
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options='+id+'&mtable='+n);
    
    });
    $("body").on('click','.main_table_refresh',function(){
        var id = $(this).attr('id');
        var n = $('.main_table_selector').val();
        var mtable = '';
        if (typeof n !== 'undefined') {
            mtable = '&mtable=' +n
        }
        var s = $(this).data('subpage');
        var subpage = '';
        if (typeof s !== 'undefined') {
            subpage = '&subpage=' +s
        }
        $('#tab_basic').load(projecturl+'includes/project_admin.php?options='+id+ '' + mtable + '' + subpage);
    
    });

    // r-shiny server control
    $("body").on('click','#control-R-server',function(e) {
        var control = $(this).val();
        $.post('ajax',{'r_server':control},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                alert('Done!');
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=r_server');
            }
        });
    });

    $("body").on('click','#update_project_description',function(e) {
        e.preventDefault();
        $.post('ajax',{
            'update_project_short_description':$("#short_desc").val(),
            'update_project_long_description':$("#long_desc").val(),
            'update_project_description_lang':$("#language").val()
        },
        function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                alert('Ok');
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=description');
            }
        });
    });

    // create table call
    $("body").on('click','#create_new_table',function(e) {
        e.preventDefault();
        $.post('ajax',{'create_new_table':$("#new_table").val(),'new_table_comment':$("#new_table_comment").val()},function(data){
            var retval = jsendp(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            } else if (retval['status']=='success') {
                alert('Ok');
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=dbcols');
            }
        });
    });
    // create table call
    $("body").on('click','#set_columns',function(e) {
        e.preventDefault();
        $('.proj_cols_set').val('data');
    });
    
    $("body").on("change",".preferred-language-selector", async function() {
        const preflang = $(this).find(':selected').val();
        var queryParams = new URLSearchParams(window.location.search);
        queryParams.set("lang", preflang);
        window.location.search = queryParams.toString();
    });
    
    $("body").on("change",".taxon-lang-selector", async function() {
        try {
            const preflang = $(this).find(':selected').val();
            const data = await $.post('ajax',{
                action: 'set_option',
                option_name: 'taxon_lang',
                option_value: preflang
            });
            
            var retval = jsendp(data);
            if (retval['status']=='error' || retval['status']=='fail') {
                throw new Error(retval['message']);
            }
            $.post('ajax', {action: 'clear_st_col'});
        }
        catch (err) {
            alert(err);
        }
    });

    // project_admin page - language definitions
    $("body").on("submit","#update_translations",async function(ev) {
        ev.preventDefault();
	try {
            const form_data = new FormData(this);
            const results = await $.ajax({
                url: "ajax", 
                type: "POST",
                enctype: 'multipart/form-data',
                data: form_data,
                processData: false,
                contentType: false
            });

            var retval = jsendp(results);
            
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            }
            else if (retval['status'] == 'success') {

                $("#update_translations button[type='submit']").removeClass("button-warning");
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=languages');
            }
        }
	catch (error) {
            console.log(error);
        }
    });

    // project_admin page - language definitions
    $("body").on("change","#update_translations .red-border",function(ev) {
        ev.preventDefault();
        $(this).removeClass('red-border');
    });

    // project_admin page - language definitions
    $("body").on("change","#update_translations input, #update_translations textarea",function(ev) {
        ev.preventDefault();
        $("#update_translations button[type='submit']").removeClass("button-success");
        $("#update_translations button[type='submit']").addClass("button-warning");
        $("#ficon").removeClass('fa-floppy-o');
        $("#ficon").addClass('fa-refresh fa-spin'); 
    });

    // project_admin page - language definitions
    $("body").on("click", "#new_translation", function(ev) {
        ev.preventDefault();
        let id = $(this).data('id') + 1;
        $("#translationsTable").append('<tr><td><input name="new-const-' + id + '" style="width: 300px" value=""></td><td><textarea name="new-translation-' + id + '" style="width:550px;height:40px" placeholder=""></textarea></td></tr>');
        $(this).data('id',id);
    });

    //project_admin page - save privileges
    $('body').on('submit','#privileges-form',async function(ev) {
        ev.preventDefault();
		try {
            const form_data = new FormData(this);
            const results = await $.ajax({
                url: "ajax", 
                type: "POST",
                enctype: 'multipart/form-data',
                data: form_data,
                processData: false,
                contentType: false
            });

            var retval = jsendp(results);
            
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
                console.log(retval['data']);
            }
            else if (retval['status'] == 'success') {

                $("#privileges-form button[type='submit']").removeClass("button-warning");
                $('#tab_basic').load(projecturl+'includes/project_admin.php?options=privileges');
                alert(retval['data']);
            }
		}
		catch (error) {
			console.log(error);
        }
    });
    
    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === "template_editor") {
            let editor = [];
            Object.keys(obj.languages).forEach(function(l) {
                editor[l] = [];
                editor[l]["message"] = conf_quill("#template_editor_" + l);
                editor[l]["subject"] = $("#template_subject_" + l);
            })
            
            const reset_editor = function(template_list = null) {
                $("#template_name").val("");
                Object.values(editor).forEach(function(ed) {
                    ed["message"].root.innerHTML = "";
                    ed["subject"].val("");
                })
                if (template_list) {
                    const html = template_list.map((t) => '<li><a href="" class="template">' + t + '</a></li>').join('');
                    $('.templates ul').html( html );
                    init_template_links();
                }
            }
            
            // load saved template
            const init_template_links = function() {
                $(".template_manager .template").on("click", async function(ev) {
                    ev.preventDefault();
                    
                    const template_name = $(this).html();
                    
                    const parameters = {
                        "action": "get_message_template",
                        "template_name": template_name
                    };
                    
                    template = await $.getJSON("ajax", parameters)
                    
                    $("#template_name").val(template_name);
                    
                    Object.keys(template.message).forEach(function(l){
                        if (template.message[l] !== null) {
                            //editor[l].setText(template.message[l]);
                            editor[l]["message"].root.innerHTML = template.message[l];
                        }
                        //editor[l].setText()
                    });
                    Object.keys(template.subject).forEach(function(l){
                        if (template.subject[l] !== null) {
                            editor[l]["subject"].val(template.subject[l]);
                        }
                        //editor[l].setText()
                    });
                })
            };
            init_template_links();
            //clear editor
            $("[name='clear_editor']").on("click", function() {
                reset_editor();
            });
            
            //delete template
            $("[name='delete_template']").on("click", async function(ev) {
                try {
                    ev.preventDefault();
                    template_name = $("#template_name").val();
                    if (template_name == '') {
                        throw 'Nothing to delete!'
                    }
                    let resp = await $.post( 'ajax', {
                        action: 'delete_template',
                        template_name: template_name
                    });
                    resp = JSON.parse(resp);
                    if (resp.status === "success") {
                        $( "#dialog" ).html("Template deleted!");
                        $("#dialog").dialog("open");
                        reset_editor(resp.data);
                    }
                    else {
                        throw resp.message;
                    }
                } catch (err) {
                    $("#dialog").text(err);
                    $("#dialog").dialog("open");
                } 
            });
            
            //saving template
            $("#template-editor-form").submit(async function(ev){
                try {
                    ev.preventDefault();
                    const formElement = document.getElementById('template-editor-form');
                    const fd = new FormData(formElement);
                    if (!fd.has('template_name') || fd.get('template_name') === '') {
                        throw "Template name must not be empty!";
                    }
                    
                    Object.keys(obj.languages).forEach(function(l) {
                        fd.append(l + "[subject]", editor[l]['subject'].val());
                        fd.append(l + "[message]", editor[l]['message'].root.innerHTML);
                    });
                    
                    
                    resp = await $.ajax({
                        url : "ajax",
                        type: "POST",
                        data : fd,
                        processData: false,
                        contentType: false,
                    });
                    
                    resp = JSON.parse(resp);
                    if (resp.status === "success") {
                        $( "#dialog" ).html('Template saved!');
                        $("#dialog").dialog("open");
                        reset_editor(resp.data);
                        
                    }
                    else {
                        throw resp.message;
                    }
                    
                }
                catch(err) {
                    $("#dialog").text(err);
                    $("#dialog").dialog("open");
                }
                
            });
        }
    });

    $(document).on('click','.new-job',async function(ev) {
        ev.preventDefault();
        const vm = $('#jobname').val();
        const parameters = {
            vm: vm,
            action: 'new-job',
        };

        const result = JSON.parse(await $.post('ajax',{'admin-job':'1','parameters':parameters}));
        const message = $('#message-div');

        if (result.status == 'success') {
            message.html(result.data).addClass('message-success').show();
        }
        else if (result.status == 'error') {
            message.html(result.message).addClass('message-error').show();
        }

        const t = $(this).parent();

        setTimeout( function() {
            message.removeClass('message-error').removeClass('message-success').html('').hide();
            t.find(".module_submenu").click();
        }, 3000);

    });
    $(document).on('click','.save-job',async function(ev) {
        ev.preventDefault();
        const vm = $(this).data('vm');
        const jobfileext = $(this).data('ext');
        const parameters = {
            vm: vm,
            ext: jobfileext,
            action: 'save-job',
            data: $('#'+vm+'-form').find("select, textarea, input").serialize(),
        };

        const result = JSON.parse(await $.post('ajax',{'admin-job':'1','parameters':parameters}));
        const message = $('#message-div');

        if (result.status == 'success') {
            message.html(result.data).addClass('message-success').show();
        }
        else if (result.status == 'error') {
            message.html(result.message).addClass('message-error').show();
        }

        setTimeout( function() {
            message.removeClass('message-error').removeClass('message-success').html('').hide();
        }, 3000);
          
    });
    
    $(document).on('click','.delete-job', async function(ev) {
        ev.preventDefault();
        if (!confirm('Are you sure do you want to delete this job file? Your job file and your settings will be lost. The job module will remain intact.')) {
            return;
        }
        const vm = $(this).data('vm');
        const jobfileext = $(this).data('ext');
        const parameters = {
            vm: vm,
            ext: jobfileext,
            action: 'delete-job',
        };

        const result = JSON.parse(await $.post('ajax',{'admin-job':'1','parameters':parameters}));
        const message = $('#message-div');

        if (result.status == 'success') {
            message.html(result.data).addClass('message-success').show();
        }
        else if (result.status == 'error') {
            message.html(result.message).addClass('message-error').show();
        }

        setTimeout( function() {
            message.removeClass('message-error').removeClass('message-success').html('').hide();
            $('#job_admin_page').find('button.module_submenu').click();
        }, 3000);
          
    });
    
    $(document).on('click','.run-job',async function(ev) {
        ev.preventDefault();
        const vm = $(this).data('vm');
        const jobfileext = $(this).data('ext');
        const parameters = {
            vm: vm,
            ext: jobfileext,
            action: 'run-job',
            data: $('#'+vm+'-form').find("select, textarea, input").serialize(),
        };

        const result = JSON.parse(await $.post('ajax',{'admin-job':'1','parameters':parameters}));
        const message = $('#message-div');

        if (result.status == 'success') {
            message.html(result.data).addClass('message-success').show();
        }
        else if (result.status == 'error') {
            message.html(result.message).addClass('message-error').show();
        }

        
        setTimeout( function() {
            message.removeClass('message-error').removeClass('message-success').html('').hide();
        }, 3000);
          
    });
    $(document).on('click','.read-job',async function(ev) {
        ev.preventDefault();
        const vm = $(this).data('vm');
        const jobfileext = $(this).data('ext');
        const parameters = {
            vm: vm,
            ext: jobfileext,
            action: 'read-job',
            data: $('#'+vm+'-form').find("select, textarea, input").serialize(),
        };

        const result = JSON.parse(await $.post('ajax',{'admin-job':'1','parameters':parameters}));
        const message = $('#jobresults');

        if (result.status == 'success') {
            message.html(result.data);
        }
        else if (result.status == 'error') {
            message.html(result.message);
        }
          
    });

    $(document).on('change','#gitlab-job-file', function(e) {
        
        var file = $(this).val();
        //window.open(projecturl+'ajax?get-job-form-gitlab&file='+file, '_blank');
        $.post('ajax', {'get-job-from-gitlab':file },
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['message']);
                } else if (retval['status']=='success') { 
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=jobs');
                }
        });
    });


    $(document).on('focus', '#gitlab-job-file', async function () {
        const result = JSON.parse(await $.post('ajax',{'get-job-file-list-from-gitlab':'1'}));
        $("#gitlab-job-file").html("");
        for ( var i = 0; i < result.length-1; i++){ 
            var o = new Option(result[i].text, result[i].value);
            $(o).html(result[i].text);
            $("#gitlab-job-file").append(o);
        }

    });
    /*
    $("body").on('click','#export_attachments',function(e) {
        e.preventDefault();
        var table = $(this).attr("data-download_files");
        $.post(projecturl+"includes/afuncs.php", {'download_files':table },
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['message']);
                } else if (retval['status']=='success') { 
                    alert(retval['data']);
                    $('#tab_basic').load(projecturl+'includes/project_admin.php?options=jobs');
                }
            });
    });*/

    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === 'languages') {
            
            Vue.component('language_input_tr', {
                data () {
                    return {
                        
                        // Azért jön létre, hogy a változásokat meg lehessen jeleníteni
                        // Ebben történnek a változások (felhasználói és ezt küldi el az Ajax)
                        tr: JSON.parse(JSON.stringify(this.t))
                    }
                },
                props: {
                    // Ezt tölti fel a v.for itt: new Vue(....
                    t: Object,
                    languages: Object
                },
                template:`
                <tr>
                    <td> <input type="text" v-model="tr.cnst" /></td>
                    <td v-for="(label, l) in languages"> 
                        <div v-if="changed" class="text-warning">{{ t[l] }}</div>
                        <textarea 
                            :style="tr[l] === '__not_translated__' ? 'border-color: red;' : ''" 
                            v-model="tr[l]" 
                        />
                    </td>
                    <td> 
                        <button 
                            type="button" 
                            :class="'pure-button ' + ( !changed ? 'button-secondary' : 'button-warning')" 
                            @click="save"
                        >
                            <i class="fa fa-floppy-o"> </i> 
                        </button> 
                        <button v-if="!isLast" type="button" class="pure-button button-error" @click="dlt"><i class="fa fa-trash-o"> </i> </button> 
                    </td>
                </tr>
                `,
                computed: {
                    changed () {
                        c = Object.keys(this.languages).reduce((c, l) => {
                            return c + Number(this.t[l] === this.tr[l])
                        }, 0)
                        return (c < Object.values(this.languages).length)
                    },
                    isLast () {
                        return this.t.cnst === '';
                    }
                },
                methods: {
                    async save () {
                        try {
                            let resp = await $.post('ajax', {
                                'language-administration': 'save',
                                language_strings: this.tr, // egy object, egy sor minden nyelven, fordítások
                            });
                            resp = JSON.parse(resp);
                            if (resp.status !== 'success') {
                                throw resp.message
                            }
                            //Vajom miért kellet kiüríteni a tr-t mentéskor?
                            //Ez nekünk kell, hogy lássuk, hogy mi van az adott sorban!
                            //this.tr = {}
                            this.$emit('reinit')
                            if (this.isLast) {
                                this.tr = {"cnst":""};
                            }
                        } catch (e) {
                            console.log(new Error(e));
                        }
                    },
                    async dlt () {
                        try {
                            if (confirm('Are you sure?')) {
                                let resp = await $.post('ajax', {
                                    'language-administration': 'delete',
                                    cnst: this.tr.cnst
                                });
                                resp = JSON.parse(resp);
                                if (resp.status !== 'success') {
                                    throw resp.message
                                }
                                this.$emit("reinit");
                            }
                        } catch (e) {
                            console.log(new Error(e));
                        }
                    }
                }
            })
            var l = new Vue({
                el: '#language-definitions',
                data () {
                    return {
                        languages: [],
                        translations: [],
                        filter: "",
                    }
                },
                template: `
                <div id="language-definitions">
                    <form class='pure-form'>
                        <table class='pure-table pure-table-horizontal'>
                            <thead> 
                                <tr> 
                                    <th> 
                                        constant
                                        <input type="text" v-model="filter" placeholder="type to filter values ...">
                                    </th> 
                                    <th v-for="(label, l) in languages"> {{ label }} </th>
                                    <th> save </th> 
                                </tr> 
                            </thead>
                            <tbody v-if='translations_filtered.length'>
                                <language_input_tr 
                                    v-for="t in translations_filtered" :key="t.cnst" 
                                    :t="t" 
                                    :languages="languages" 
                                    @reinit="init"
                                />
                            </tbody>
                        </table>
                    </form>
                </div>
                `,
                mounted () {
                    this.init()
                },
                computed: {
                    translations_filtered () {
                        const t = (this.filter.length > 2) ? this.translations.filter((t) => t.cnst.match(this.filter)) : this.translations
                        return t
                    }
                },
                methods: {
                    async init () {
                        try {
                            let resp = await $.getJSON('ajax', {
                                'language-administration': 'get_translations',
                            });
                            if (resp.status !== 'success') {
                                throw resp.message
                            }
                            this.translations = resp.data.translations
                            // üres sor a táblázat végén, hogy tudjunk új fordítást hozzáadni
                            this.translations.push({
                                cnst: ""
                            })
                            
                            this.languages = resp.data.languages
                        } catch (e) {
                            console.log(new Error(e));
                        }
                    },
                }
            })
        }
    });


});
$(document).ready(function() {
    $(document).click(function(e) {
        $('.topnav').find('ul.subnav').find('.subsubnav').hide();
    });

    $('.topnav').find('ul.subnav').find('.sno').on('mouseover',function(ev) {
        $('.topnav').find('ul.subnav').find('.subsubnav').show();
    });
});

