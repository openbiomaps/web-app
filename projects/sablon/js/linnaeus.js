// var $ = ''
// var Vue = ''
var lt = {}
var lParams = {}
var lModules = false
var lAdminOptions = false
var initial = {}

function Taxon (t = { id: null, terms_table: null, words: [], parentId: null }) { // taxon_id = null, accepted = [], synonym = [], misspelled = [], undef = [], parentId = null) {
  this.id = t.id
  this.terms_table = t.terms_table
  this.accepted = []
  this.synonym = []
  this.misspelled = []
  this.undefined = []
  this.parent = null
  this.children = []

  t.words.forEach((w) => {
    if (['accepted', 'synonym', 'misspelled', 'undefined'].indexOf(w.status) >= 0) {
        w.terms_table = this.terms_table
        this[w.status].push(new Word(w))
    }
  })

  this.parentId = t.parentId
  this.words = t.words.map((w) => w.word)
  
  this.accepted_name = () => {
    return (this.accepted.length) ? this.accepted[0].word : this.words[0]
  }
  if (lModules.taxonMeta && lModules.taxonMeta.indexOf('parent_id') > -1) {
    
    this.saveParent = async function () {
      try {
        let resp = await $.post('ajax', {
          m: 'linnaeus', action: 'update_parent',
          taxon_id: this.taxon_id,
          parentId: this.parentId
        })
        resp = JSON.parse(resp)
        if (resp.status !== 'success') {
          throw new Error(resp.message)
        }
        this.loadParent()
      } catch (e) {
        console.log(e)
      }
    }
    
    this.loadParent = async function () {
      if (this.parentId) {
        try {
          const resp = await $.getJSON('ajax', {
            linnaeus: 'load_list',
            ids: [this.parentId]
          })
          this.parent = new Taxon(resp[0])
        } catch (e) {
          console.log(e)
        }
      }
    }
    
    this.loadChildren = async function () {
      const resp = await $.getJSON('ajax', {
        m: 'linnaeus', action: 'load_children',
        taxon_id: this.taxon_id
      })
      if (resp.status !== 'success') {
        throw new Error(resp.message)
      }
      this.children = Object.values(resp.data).map((t) => new Taxon(t))
    }
  }
}

function Word (w = { id: null, word: '', status: 'undefined', subject: '', taxon_db: 0, taxon_id: null, table: '', terms_table: '', role_id: null }) {
  this.id = w.id
  this.word = w.word
  this.status = w.status
  this.subject = w.subject
  this.taxon_db = w.taxon_db
  this.taxon_id = w.taxon_id
  this.table = w.table
  this.terms_table = w.terms_table
  this.role_id = w.role_id
  
  this.save = async function () {
    try {
      if (!this.subject || !this.word) {
        throw new Error('word and subject can\'t be empty!')
      }
      let resp = await $.post('ajax', {
        linnaeus: 'save_word',
        word: this
      })
      if (resp.status !== 'success') {
        throw new Error(resp.message)
      }
      data = resp.data
      this.id = data.id
      this.taxon_id = data.taxon_id
      return true
    } catch (e) {
      $('#dialog').html(e)
      $('#dialog').dialog('open')
      console.log(e)
      return false
    }
  }
  this.delete = async function () {
    try {
      let resp = await $.post('ajax', {
        linnaeus: 'delete_word',
        word: this
      })
      resp = JSON.parse(resp)
      if (resp.status !== 'success') {
        throw new Error(resp.message)
      }
      return true
    } catch (e) {
      $('#dialog').html(e)
      $('#dialog').dialog('open')
      console.log(e)
      return false
    }
  }
}

Vue.component('tx-card', {
  data () {
    return {
      modules: lModules,
      editable: false,
      showMetanameButtons: true,
      showMetaModal: false,
      remoteInfo: {
          error: "No info available"
      }
    }
  },
  props: {
    word: Object,
    status: String,
    table: String,
    repositories: {
        type: Object,
        default: {}
    }
  },
  template: `
    <div>
        <div class='tx-card'> 
            <div>
                <form class='tx-card-input pure-form'>
                    <strong v-if="!editable">{{ word.word }}</strong>
                    <input v-else class="tx-word editable" v-model="word.word" type="text">
                    <ul>
                        <li v-if="word.terms_table != 'taxon'">data table: 
                            <strong v-if="!editable">{{ word.table }}</strong>
                            <select v-else v-model="word.table">
                                <option value=""></option>
                                <option v-for="tbl in dataTables" :value="tbl" >{{ tbl }}</option>
                            </select>
                        </li>
                        <li>subject: 
                            <strong v-if="!editable">{{ word.subject }}</strong>
                            <select v-else v-model="word.subject">
                                <option value=""></option>
                                <option v-for="sbj in subjects" :value="sbj" >{{ sbj }}</option>
                            </select>
                        </li>
                        role ID:
                        <li v-if="editable && cite_cols.indexOf(word.subject) > -1">
                            <input type='number' v-model="word.role_id">
                        </li>
                    </ul>
                </form>
                <div v-if="showMetanameButtons" class="metaname_buttons">
                    <button 
                        v-if="repositories"
                        v-for="repository in Object.values(repositories)"
                        :key="repository.short"
                        :title='repository.name' 
                        class='pure-button button-xsmall' 
                        @click="showMetaname(repository.short)"
                    >
                        <img :src="'css/img/' + repository.icon" width="24" height="24" alt='fetch data' >
                    </button>
                </div>
            </div>
            <div class='tx-card-buttons'>
                <button type='button' v-if="!editable" class='edit-word pure-button button-warning' @click="edit"><i class="fa fa-pencil"></i></button>
                <button type='button' v-else class='edit-word pure-button button-secondary' @click="edit"><i class="fa fa-floppy-o"></i></button>
                <button type='button' class='delete-word pure-button button-error' @click="$emit('delete-word')" v-if="editable"><i class='fa fa-trash'></i></button>
                <!--<button type='button' class='pure-button button-xsmall' @click="showMetanameButtons = !showMetanameButtons"><i :class="chevron"></i></button>-->
            </div>
        </div>
        <tx-metaname :show="showMetaModal" :word="word.word" :remoteInfo="remoteInfo" @close="showMetaModal = false"/>
    </div>
  `,
  methods: {
    edit () {
      if (this.editable) {
        this.$emit('save-word')
      }
      this.editable = !this.editable
    },
    async showMetaname (repo) {
        try {
            const resp = await $.getJSON('ajax', {
                metaname: 'fetch_remote_data',
                repository: repo,
                term: this.word.word
            })
            if (resp.status != 'success') {
                throw resp.message
            }
            this.remoteInfo = resp.data;
            this.showMetaModal = true
        } catch (e) {
            console.log(e);
        }
    }
  },
  computed: {
      dataTables() {
          if (!this.table || !this.modules) {
              return [];
          }
          return this.modules[this.table].map((c) => c.data_table).filter((v, i, a) => a.indexOf(v) == i)
      },
      subjects () {
          if (!this.table || !this.modules) {
              return [];
          }
          return this.modules[this.table].map((sbj) => sbj.subject)
      },
      chevron () {
          const ud = (this.showMetanameButtons) ? 'up' : 'down'
          return 'fa fa-chevron-' + ud;
      },
      cite_cols () {
          return this.modules.terms.filter(t => t.obm_type === 'cite').map(t => t.subject)
       }
  }
})

Vue.component('tx-metaname', {
    props: {
        word: String,
        remoteInfo: Object,
        show: Boolean
    },
    data () {
        return {
        }
    },
    template: `
        <v-modal v-if="show" :hideSaveBtn="true" cancelBtnLabel="OK" @close="$emit('close')">
            <template v-slot:header>
            {{ word }}
            </template>
            <template v-slot:body>
                <table class='pure-table'>
                    <tr v-for="(value, key) in remoteInfo" :key='key'>
                        <td> {{ key }} </td>
                        <td> <span v-html="value"></span> </td>
                    </tr>
                </table>
            </template>
        </v-modal>
    `
})

Vue.component('tx-editor-row', {
  props: {
    taxon: Object,
    table: String
  },
  data () {
    return {
      showTaxonomy: false,
      showValidation: false,
      modules: lModules,
      states: {
        accepted: lt.str_accepted_name,
        synonym: lt.str_synonym_name,
        misspelled: lt.str_mispelled_name,
        undefined: lt.str_undefined
      },
    }
  },
  computed: {
    taxonMeta () {
        return this.modules.taxonMeta
    },
    wordNumber () {
      return Object.keys(this.states).reduce((sum, st) => {
        return sum + this.taxon[st].length
      }, 0)
    },
    subjects () {
        const subjects = [];
        Object.keys(this.states).forEach((st, i) => {
            this.taxon[st].forEach((wrd, i) => {
                if (subjects.indexOf(wrd.subject) === -1) {
                    subjects.push(wrd.subject)
                }
            });
        });
        return subjects;
    },
    taxon_tools () {
      const tools = {
        remove: {
          title: 'remove from editor',
          fa: 'window-close-o',
          method: this.remove
        },
        add: {
          title: 'add word',
          fa: 'plus-square-o',
          method: this.add
        },
      }
      if (lModules.taxonMeta && lModules.taxonMeta.indexOf('parent_id') > -1) {
        tools['taxonomy'] = {
          title: lt.str_taxonomy,
          fa: 'code-fork',
          method: this.taxonomy
        }
      }
      if (lModules.validation) {
        tools['validation'] = {
          title: 'validation',
          fa: 'balance-scale',
          method: () => this.$emit('validate')
        }
      }
      return tools
    }
  },
  template: `
    <tr>
        <td class="tx-action-buttons" :title='taxon.id'>
            <v-toolbar v-bind:buttons="taxon_tools"></v-toolbar>
        </td>
    
        
        <td v-for="(status_translated, status) in states" v-bind:status="status" class="tx-column" :key="status">
          <draggable class="list-group" :list="taxon[status]" group="taxon_names" @change="save(status, $event)" @end="dropRowIfEmpty">
            <tx-card 
                v-for="word in taxon[status]" 
                :key="word.id"
                :table="table"
                :word="word" 
                :repositories="repositories"
                @save-word="word.save()" 
                @delete-word="deleteWord(status, word)"
            />
          </draggable>
        </td>
    </tr>
  `,
  methods: {
    dropRowIfEmpty () {
      if (this.wordNumber === 0) {
        this.$emit('deleteTaxon', this.taxon.id)
      }
    },
    async save (status, ev) {
      if (ev.added) {
        ev.added.element.status = status
        ev.added.element.taxon_id = this.taxon.id
        ev.added.element.save()
      }
    },
    add () {
      this.taxon.undefined.push(new Word({
          taxon_id: this.taxon.id,
          terms_table: this.table,
          status: 'undefined'
      }))
    },
    remove () {
      this.$emit('removeThisRow', this.taxon.id)
    },
    delete () {
      if (!window.confirm('Do you really want to delete this row?')) {
        return
      }
      this.$emit('deleteTaxon', this.taxon.id)
    },
    async deleteWord (status, word) {
      if (!window.confirm('Do you really want to delete this word?')) {
        return
      }
      const res = await word.delete()
      if (res === true) {
        this.taxon[status] = this.taxon[status].filter((t) => t.id !== word.id)
      }
    },
    taxonomy () {
      this.showTaxonomy = true
    },
    validate() {
        this.$emit('validate')
    }
  },
  asyncComputed: {
    async repositories () {
        try {
            const resp = await $.getJSON('ajax', {
                metaname: 'get_repositories',
            })
            if (resp.status != 'success') {
                throw new Error(resp.message)
            }
            return resp.data
        } catch (e) {
            console.log(e);
        }
    }  
  },
})

Vue.component('tx-editor', {
  data () {
    return {
      states: {
          accepted: lt.str_accepted_name,
          synonym: lt.str_synonym_name,
          misspelled: lt.str_mispelled_name,
          undefined: lt.str_undefined
      },
      bottom_toolbar: {
        new_taxon: {
          title: 'Add new taxon',
          fa: 'plus-square-o',
          method: this.newTaxon
        }
      },
    }
  },
  template: `
    <div>
      <table class="pure-table pure-table-bordered tx-editor-body">
        <thead>
            <tr>
                <th>{{lt.str_actions}}</th>
                <th v-for="(item, index) in states" :key="status"> {{ item }} </th>
            </tr>
        </thead>
        <tbody>
            <tx-editor-row 
                v-for="taxon in taxons" 
                :table="table" 
                :taxon="taxon" 
                :key='taxon.id' 
                @deleteTaxon="deleteTaxon" 
                @removeThisRow="removeThisRow"
                @validate="$emit('validate', taxon.id)"
            />
            <tr v-if="!taxons" class="error"><td :colspan="states.length + 1"> Nothing to show </td></tr>  
        </tbody>
      </table>
      <v-toolbar v-bind:buttons="bottom_toolbar"></v-toolbar>
    </div>
  `,
  props: {
      selectedIds: Array,
      table: String
  },
  asyncComputed: {
    async taxons () {
      const resp = await $.getJSON('ajax', {
        linnaeus: 'load_list',
        table: this.table,
        taxon_id: this.selectedIds
      })
      if (resp.status != 'success') {
        throw new Error(resp.message)
      }
      return Object.values(resp.data).map((t) => {
        t.terms_table = this.table
        return new Taxon(t)
      })
    }  
  },
  methods: {
    async newTaxon () {
      this.taxons.push(new Taxon({
          terms_table: this.table,
          words: []
      }))
    },
    removeThisRow (e) {
      this.$delete(this.taxons, Object.keys(this.taxons).find((k) => this.taxons[k].id === e))
    },
    async deleteTaxon (e) {
      this.$delete(this.taxons, Object.keys(this.taxons).find((k) => this.taxons[k].id === e))
    },
  }
})

Vue.component('tx-table', {
  template: `
      <div id='tx-table'>
        <div class='tx-table-menu pure-form flex'>
            <div>
                <select class="terms_table" v-model="tbl" @change="selectTable(4)">
                    <option value="" disabled selected> {{ lt.str_please_choose }} </option>
                    <option value="taxon">Taxon table</option>
                    <option value="terms" v-if="modules.terms" >Terms table</option>
                </select>
            </div>
        </div>
        <div class='flex' v-if="txSelection.table">
          <div>
            <table class="pure-table pure-table-horizontal unselected">
                <thead class="pure-form">
                    <tr>
                        <th>tid</th> <th>table</th> <th>column</th> <th>word</th> <th>status</th> <th>select</th> 
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <select v-model="dataTable" @change="selectTable(3)">
                                <option value=""></option>
                                <option v-for="table in dataTables" :key="table" :value="table">{{ table }}</option>
                            </select>
                        </th>
                        <th>
                            <select name='subject' v-model="subject" @change="selectTable(1)">
                                <option value=""></option>
                                <option v-for="subject in subjects" :key="subject" :value="subject">{{ subject }}</option>
                            </select>
                        </th>
                        <th>
                            <input name='filter' type='text' v-model='filter' @input="selectTable(1)">
                        </th>
                        <th>
                            <select name='status' v-model="status" @change="selectTable(1)">
                                <option value=""></option>
                                <option v-for="status in states" :key="status" :value="status">{{ status }}</option>
                            </select>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="el in list">
                        <td>{{el.taxon_id}}</td>
                        <td>{{el.table}}</td>
                        <td>{{el.subject}}</td>
                        <td>{{el.word}}</td>
                        <td>{{el.status}}</td>
                        <td><button class='pure-button button-small' @click='select(el.taxon_id)'><i class='fa fa-chevron-right'></i></button></td>
                    </tr>
                </tbody>
            </table>
            <v-pagination 
                v-if="totalPages > 1"
                :total-pages='totalPages' 
                :total='totalRows' 
                :per-page='perPage'
                :currentPage='currentPage' 
                @pagechanged='onPageChange' 
            />
          </div>
          <div v-if="selected.length" >
            <table 
                class="pure-table pure-table-horizontal selected"
                style="max-width: 750px;"
            >
                <colgroup>
                    <col span="1" style="width: 100px;">
                    <col span="1">
                </colgroup>
                <thead>
                    <tr>
                        <th>deselect</th><th>word</th>
                    </tr>
                    <tr>
                        <th colspan="6">
                            <button class='pure-button button-warning' @click="$emit('editSelected', ids)"> <i class='fa fa-pencil'></i> </button>
                        </th> 
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="el in selected">
                        <td>
                            <button class='pure-button button-small' @click='deselect(el.taxon_id)'><i class='fa fa-chevron-left'></i></button>
                            <button v-if="validation_enabled" class='pure-button button-small button-info' @click='validate(el.taxon_id)'><i class='fa fa-balance-scale'></i></button>
                        </td>
                        <td>{{el.words}}</td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
      </div>
  `,
  props: {
    txSelection: Object,
  },
  mounted() {
      if (this.tbl) {
          this.loadData(1)
      }
  },
  data() {
    return {
        currentPage: 1,
        perPage: 15,

        lt: lt,
        
        modules: lModules,
        tbl: this.txSelection.table,
        
        dataTable: this.txSelection.dataTable,
        subject: this.txSelection.subject,
        filter: this.txSelection.filter,
        status: this.txSelection.status,
        
        list: [],
        selected: [],
        
        totalRows: 0
      }
  },
  methods: {
      onPageChange(page) {
          this.currentPage = page;
          this.loadData()
      },
      async loadData (resetLevel = 0) {
          this.reset(resetLevel);
          if (this.filter.length == 1) {
              return
          }
          let resp = await $.getJSON('ajax', { 
              linnaeus: 'get_list',
              table: this.tbl,
              dTable: this.dataTable,
              subject: this.subject,
              status: this.status,
              filter: this.filter,
              limit: this.perPage,
              offset: this.perPage * (this.currentPage - 1),
              orderby: "newestFirst",
              excluded: this.selected.map((el) => el.taxon_id)
          })
          if (resp.status != 'success') {
              throw new Error(resp.message);
          }
          var data = resp.data;
          
          this.list = data.list ? data.list.map((el) => {
              el.selected = false
              return el
          }) : []
          this.totalRows = Number(data.count)
          
      },
      reset(level) {
          if (level >= 1) {
              this.currentPage = 1;
          }
          if (level >= 2) {
              this.status = '';
              this.filter = '';
              this.selected = [];
          }
          if (level >= 3) {
              this.subject = '';
          }
          if (level >= 4) {
              this.dataTable = '';
          }
      },
      async select(id) {
          const resp = await $.getJSON('ajax', {
              linnaeus: 'get_list',
              table: this.txSelection.table,
              taxon_id: id
          })
          if (resp.status != 'success') {
              throw new Error(resp.message);
          }
          const data = resp.data
          // this.selected = this.selected.concat(data.list)
          this.selected.push({
              taxon_id: id,
              words: data.list.map(el => el.word).join(", ")
          })
          this.list = this.list.filter((el) => el.taxon_id != id);
          this.loadData()
      },
      deselect(id) {
          this.selected = this.selected.filter((el) => el.taxon_id != id);
          this.loadData()
      },
      selectTable(resetLevel) {
          this.$emit('tableSelectd', {
              table: this.tbl,
              dataTable: this.dataTable,
              subject: this.subject,
              filter: this.filter
          })
          this.loadData(resetLevel)
      },
      reformPagination(counts) {
          var paginationCounts = {
              total: 0,
              tables: {},
              subjects: {}
          } 
          
          counts.forEach((el) => {
              if (typeof paginationCounts.tables[el.dTable] === 'undefined' || typeof paginationCounts.subjects[el.dTable] === 'undefined') {
                  paginationCounts.tables[el.dTable] = 0;
                  paginationCounts.subjects[el.dTable] = {};
                  
              }
              paginationCounts.total += Number(el.c);
              paginationCounts.tables[el.dTable] += Number(el.c);
              paginationCounts.subjects[el.dTable][el.subject] = Number(el.c)
          })
          return paginationCounts
      },
      validate(taxon_id) {
          this.$emit('validate', taxon_id)
      }
  },
  computed: {
      totalPages () {
          return Math.ceil( this.totalRows / this.perPage)
      },
      dataTables() {
          if (!this.txSelection.table || !this.modules) {
              return [];
          }
          return this.modules[this.txSelection.table].map((c) => c.data_table).filter((v, i, a) => a.indexOf(v) == i)
      },
      subjects () {
          if (!this.txSelection.table || !this.modules) {
              return [];
          }
          return this.modules[this.txSelection.table].filter((sbj) => sbj.data_table === this.dataTable).map((sbj) => sbj.subject)
      },
      states () {
          if (!this.txSelection.table || !this.modules) {
              return [];
          }
          const states_arr = this.modules[this.txSelection.table].filter((sbj) => sbj.dTable === this.dataTable && sbj.subject === this.subject).map((sbj) => sbj.status)
          return states_arr.length ? states_arr[0].split(',') : [] 
      },
      ids () {
          return this.selected.map((s) => s.taxon_id)
      },
      validation_enabled () {
          return (this.txSelection.table === 'taxon' && this.modules.validation)
      }
  }
})

$(document).ready(function () {
    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === "linnaeus" || ev.page === "taxon") {
            const init = (async () => {
              let resp = await $.getJSON('ajax', { linnaeus: 'translations' })
              if (resp.status != 'success') {
                  throw new Error(resp.message);
              }
              lt = resp.data;
              
              resp = await $.getJSON('ajax', { linnaeus: 'enabled_modules'})
              if (resp.status != 'success') {
                  throw new Error(resp.message);
              }
              lModules = resp.data;
            })()

            init.then(() => {
              var l = new Vue({
                el: '#linnaeus',
                data () {
                    return {
                        modules: lModules,
                        
                        txSelection: {
                            table: lModules.terms ? "" : 'taxon',
                            dataTable: "",
                            subject: "",
                            filter: "",
                            status: "",
                        },
                        
                        selectedIds: [],
                        validate_taxon_id: null,
                        top_toolbar: {
                            editFinished: {
                                title: 'Done!',
                                label: 'Done!',
                                fa: 'check',
                                classes: 'button-secondary',
                                method: () => {
                                    this.selectedIds = []
                                    this.validate_taxon_id = null
                                }
                            }
                        },
                    }
                },
                computed: {
                    validation_enabled () {
                        return (this.txSelection.table === 'taxon' && this.modules.validation)
                    }
                },
                template: `
                <div id='linnaeus'>
                    <tx-table 
                        v-if="!selectedIds.length && !validate_taxon_id" 
                        :txSelection = "txSelection"
                        @tableSelectd = "onTableSelected" 
                        @editSelected = "onEditSelected"
                        @validate = "onValidate"
                    />
                    <v-toolbar v-else v-bind:buttons="top_toolbar" />
                    
                    <tx-editor 
                        v-if="selectedIds.length" 
                        :selectedIds="selectedIds" 
                        :table="txSelection.table" 
                        @validate="onValidate" 
                    />
                    <rules_table 
                        v-if="validation_enabled && validate_taxon_id" 
                        :vData="modules.validation" 
                        :taxon_id='validate_taxon_id' 
                    />
                </div>
                `,
                methods: {
                    onEditSelected (ids) {
                        this.selectedIds = ids
                    },
                    onTableSelected (txSelection) {
                        this.txSelection = txSelection
                    },
                    onValidate (taxon_id) {
                        this.selectedIds = []
                        this.validate_taxon_id = Number(taxon_id)
                    }
                }
              })
            }).catch(console.error)
            
            if ($("#linnaeus_admin").length) {
                const admin_init = (async () => {
                    const resp = await $.getJSON('ajax', { linnaeus: 'get_admin_data'})
                    if (resp.status != 'success') {
                        throw new Error(resp.message);
                    }
                    lAdminOptions = resp.data;
                    
                })()
                admin_init.then(() => {
                    var la = new Vue({
                        el: '#linnaeus_admin',
                        data () {
                            return {
                                lAdminOptions,
                                starterOptions: "",
                                hidden: true,
                            }
                        },
                        mounted () {
                            this.starterOptions = JSON.stringify(this.lAdminOptions)
                        },
                        template: `
                        <div id='linnaeus_admin' class="container">
                        <h3>Linnaeus admin</h3>
                            <h4><span @click="hidden = !hidden">{{ pm }}</span> Managed columns </h4>
                            <div id="managed_columns" v-if="!hidden">
                                <div>
                                    <div v-for="(linnOptions, table) in lAdminOptions" :key="table">
                                        <h5>{{ table }}</h5>
                                        <div>
                                            <button class="pure-button button-warning" v-if="!linnOptions.healthcheck.linnaeus_table_exists" @click="activateLinnaeus(table)">Activate</button>
                                            <table class="pure-table" v-else>
                                              <thead>
                                                <tr><th>managed</th><th>column</th><th>multiterm</th></tr>
                                              </thead>
                                              <tbody>
                                                <tr v-for="(value, col) in linnOptions.columns" :key="col">
                                                    <td><input type="checkbox" :checked="value.managed" v-model="lAdminOptions[table].columns[col].managed" /></td>
                                                    <td>{{col}}</td>
                                                    <td><input :disabled="!value.managed" type="checkbox" :checked="value.multiterm == 'true'" v-model="lAdminOptions[table].columns[col].multiterm" /></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="mc_footer">
                                    <button type="button" :class="saveBtnClass" @click="setOptions"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                            <l-healthcheck :lAdminOptions="lAdminOptions" />
                        </div>
                        `,
                        methods: {
                            async setOptions () {
                                try {
                                    const res = await $.post('ajax', {
                                        linnaeus: 'set_linnaeus_options',
                                        managed_columns: this.managed_columns
                                    })
                                    if (res.status !== 'success') {
                                        throw res.message
                                    }
                                    this.starterOptions = JSON.stringify(this.lAdminOptions)
                                } catch (e) {
                                    console.log(e);
                                }
                            },
                            async activateLinnaeus(table) {
                                try {
                                    const res = await $.post('ajax', {
                                        linnaeus: 'activate_linnaeus_table',
                                        table
                                    })
                                    if (res.status !== 'success') {
                                        throw res.message
                                    }
                                    this.lAdminOptions[table].healthcheck.linnaeus_table_exists = true
                                    
                                } catch (e) {
                                    console.log(e);
                                }
                            }
                        },
                        computed: {
                            managed_columns () {
                                const mc = {}
                                Object.keys(this.lAdminOptions).forEach(tbl => {
                                    if (this.lAdminOptions[tbl].healthcheck.linnaeus_table_exists) {
                                        mc[tbl] = []
                                        tblData = this.lAdminOptions[tbl]
                                        Object.keys(tblData.columns).forEach(col => {
                                            if (tblData.columns[col].managed) {
                                                mc[tbl].push({
                                                    name: col, multiterm: tblData.columns[col].multiterm ?? false
                                                })
                                            }
                                        })
                                        if (!mc[tbl].length) {
                                            mc[tbl] = 'none'
                                        }
                                    }
                                })
                                return mc
                            },
                            saveBtnClass () {
                                const color = (this.starterOptions !== JSON.stringify(this.lAdminOptions)) ? 'button-warning' : 'button-success'
                                return `pure-button ${color}`;
                            },
                            pm () {
                                return (this.hidden) ? '+' : '-'
                            }
                        }
                    })
                }).catch(console.error)
            }
        }
    });
});

Vue.component('l-healthcheck', {
    template: `
        <div id="l-healthcheck">
            <h4><span @click="hidden = !hidden">{{ pm }}</span> Healthcheck </h4>
            <table v-if="!hidden">
                <tr v-for="(linnOptions, table) in lAdminOptions" :key="table">
                    <td>{{table}}</td>
                    <td v-for="(v,k) in linnOptions.healthcheck" :key="k">
                        <span :style="v ? 'color: green' : 'color: red'" :title="k">
                            {{ v ? '&#x2714;' : '&#x2718;' }}
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    `,
    data () {
        return {
            hidden: true,
        }
    },
    props: {
        lAdminOptions: Object
    },
    computed: {
        pm () {
            return (this.hidden) ? '+' : '-'
        }
    }
    
})
