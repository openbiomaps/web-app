const vToolbar = Vue.component('v-toolbar', {
  props: {
    buttons: Object
  },
  template: `
    <div class="toolbar">
      <v-button v-for="btn in buttons" :onClick="btn.method" :classes="btn.classes" :fa="btn.fa" :title="btn.title" :type="btn.type">
        {{ btn.label }}
      </v-button>
    </div>
    `
})

export { vToolbar }
