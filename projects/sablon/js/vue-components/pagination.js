const vPagination = Vue.component('v-pagination', {
    template: `
      <ul class='pagination'>
      <li class='pagination-item'>
        <button class='pure-button' type="button" @click="onClickFirstPage" :disabled="isInFirstPage" > First </button>
      </li>

      <li class='pagination-item'>
        <button class='pure-button' type="button" @click="onClickPreviousPage" :disabled="isInFirstPage" > Previous </button>
      </li>

      <li v-for="page in pages" class='pagination-item'>
        <button class='pure-button' type="button" @click="onClickPage(page.name)" :disabled="page.isDisabled" :class="isPageActive(page.name) ? 'button-secondary' : ''">
          {{ page.name }}
        </button>
      </li>

      <li class='pagination-item'>
        <button class='pure-button' type="button" @click="onClickNextPage" :disabled="isInLastPage" > Next </button>
      </li>

      <li class='pagination-item'>
        <button class='pure-button' type="button" @click="onClickLastPage" :disabled="isInLastPage" > Last </button>
      </li>
      </ul>
    `,
    props: {
        maxVisibleButtons: {
            type: Number,
            required: false,
            default: 3
        },
        totalPages: {
            type: Number,
            required: true
        },
        total: {
            type: Number,
            required: true
        },
        currentPage: {
            type: Number,
            required: true
        }
    },
    computed: {
        startPage() {
            // When on the first page
            if (this.currentPage === 1) {
                return 1;
            }
            // When on the last page
            if (this.currentPage === this.totalPages) {
                return this.totalPages - this.maxVisibleButtons + 1;
            }
            // When in between
            return this.currentPage - 1;
        },
        pages() {
            const range = [];
            
            for (let i = this.startPage; i <= Math.min(this.startPage + this.maxVisibleButtons - 1, this.totalPages); i+= 1 ) {
                range.push({ name: i, isDisabled: i === this.currentPage });
            }
            
            return range;
        },
        isInFirstPage() {
            return this.currentPage === 1;
        },
        isInLastPage() {
            return this.currentPage === this.totalPages;
        },
    },
    methods: {
        onClickFirstPage() {
            this.$emit('pagechanged', 1);
        },
        onClickPreviousPage() {
            this.$emit('pagechanged', this.currentPage - 1);
        },
        onClickPage(page) {
            this.$emit('pagechanged', page);
        },
        onClickNextPage() {
            this.$emit('pagechanged', this.currentPage + 1);
        },
        onClickLastPage() {
            this.$emit('pagechanged', this.totalPages);
        },
        isPageActive(page) {
            return this.currentPage === page;
        }
    }
})

export { vPagination }
