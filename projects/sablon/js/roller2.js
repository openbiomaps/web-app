// using this:
// http://blog.buymeasoda.com/creating-a-jquery-like-chaining-api/
// globális változókat kéne visszaírni.....
//     myQuery(".scrollbar").init({rollerSend:40,ajax:'roller.php'});
//    myQuery(".scrollbar").prepare();
//    myQuery(".scrollbar").render();
//    $('.scrollbar').on('scroll', function() {
//        myQuery(".scrollbar").render();
//    });


var myQuery;

(function() {

    myQuery = function(selector) {
        return new MyQuery(selector);
    };

    var MyQuery = function(selector) {
        // Lets make a really simplistic selector implementation for demo purposes
        /*var nodes = document.getElementsByTagName(selector);
        for (var i = 0; i < nodes.length; i++) {
            this[i] = nodes[i];
        }
        this.length = nodes.length;
        return this;*/
        var nodes;
        if ($(selector).length) {
            nodes = $(selector);
        }
        for (var i = 0; i < nodes.length; i++) {
            this[i] = nodes[i];
        }
        this.length = nodes.length;

        this.selector = selector;
        return this;
    };

    // Expose the prototype object via myQuery.fn so methods can be added later
    myQuery.fn = MyQuery.prototype = {
        from: 0,
        skipRead: 1,
        rollerSend: 40,
        target: '#rollertable',
        ajax: 'roller.php',
        pos: 0,
        init: function(e) {
            var t = this;
            t.ajax = e.ajax;
            return t;
        },
        // API Methods
        hide: function() {
            for (var i = 0; i < this.length; i++) {
                this[i].style.display = 'none';
            }
            return this;
        },
        remove: function() {
            for (var i = 0; i < this.length; i++) {
                this[i].parentNode.removeChild(this[i]);
            }
            return this;
        },
        // More methods here, each using 'return this', to enable chaining
        //
        //
        //
        readData: function(pos) {
            var t = this;
            t.skipRead--;
            if (t.skipRead!=0) {
                return false;
            }
        
            $(t.target).css("cursor", "wait");
            $.post(t.ajax, {'from':pos}, function(data){
                t.skipRead=1;
                $('#rollertable').html('');
                var j = JSON.parse(data);
                for (f = 0; f < j.length; f++) {
                    t.from++;
                    var d = "";
                    for (k = 0; k<j[f].length; k++) {
                        d += "<div class='cdiv'>" + j[f][k] + "</div>";
                    }
                    var h = pos*19;
                    $("#rollertable").append('<div class="rdiv" style="top:'+h+'px">'+d+'</div>')
                }
            });
            $('#rollertable').css("cursor", "auto");
        },
        render: function() {
            var t = this;
            var rpos = $("#rollertable").position().top;
    
            /*$('.rdiv').each(function(n) {
                    console.log(n+": "+" "+rpos+" "+$(this).position().top + " " + $(this).height() + " " + $("#rollertable").height() + " " + $('.scrollbar').height());
            });*/
                
            // renderer számláló
            // csak teszt, de egyébként le lehet véle kérdezni a legfelső látszó elemet
            /*$('.rdiv').each(function(n) {
                    //top visible rdiv index of current render
                    if ($(this).position().top>-8) {
                         $('#visible-items').html(  n );
                         return false;  // break once the first displayable value has been returned.
                    }
            });*/

            var l = $('.rdiv').length-1;
            if (typeof l !== typeof undefined && l !== false && l > 0) {
                var averageHeight = $('.rdiv:eq(0)').height();
                if ($('.rdiv:eq(0)').position().top>0) {
                    //ne legyen több a kivont érték mint a roller által visszaadott érték+a tábla maximuma!
                    var sm = t.rollerSend-Math.round($('.scrollbar').height()/averageHeight);
                    t.readData(Math.abs(Math.round(rpos/averageHeight))-sm);
                } else if ($('.rdiv:eq('+l+')').position().top < $('.scrollbar').height()-averageHeight) {
                    //read from rpos/row-height if last element position lower than scrollbar_height-averageHeight
                    //the averageHeight border means: start rendering before show empty area
                    t.readData(Math.abs(Math.round(rpos/averageHeight)));
                }
            }
            return t;
        },
        prepare: function(e) {
            var t = this;
            $(this.selector).html("<div id='rollertable' class='force-overflow'></div>");
            //function prepare() {
            $.post(t.ajax, {'prepare':1}, function(data){
                var d = $("#rollertable").css('line-height').match(/(\d+)/);
                $("#rollertable").height(d[1]*data);
                //if table prepared we read the firs block into from 0 pos
                t.readData(0);
            });
            return t;
        }
    };

}());
