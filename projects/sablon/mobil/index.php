<?php
### ---------------------------------- 
# PWA app for offline data fetch
# Miklós Bán 2022.05.03 - 2024.07.06

$version = '1.3';

$wms_cluster = 'my_cluster';
$min_zoom_for_filter = 12;
$distanceInput = 30;
$minDistanceInput = 1;
#
#
###-----------------------------------

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
require_once('/etc/openbiomaps/system_vars.php.inc');

# wms cluster layer name
# layer_data_MY-CLUSTER-LAYER for cname created automatically
#

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CODELAB: Add link rel manifest -->
  <link rel="manifest" href="manifest.json?v1">
<!-- CODELAB: Add iOS meta tags and icons -->
<!--   <meta name="apple-mobile-web-app-capable" content="yes"> -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="OpenBioMaps">
  <link rel="apple-touch-icon" href="images/icons/Android/Icon-144.png">
  <link rel="icon" href="https://openbiomaps.org/img/favicon.ico" type="image/x-icon" />
  <!-- description -->
  <meta name="description" content="OBM Map Data Query App">
  <!-- meta theme-color -->
  <meta name="theme-color" content="#aad2dd" />
  <link rel="stylesheet" href="https://openlayers.org/en/v5.3.0/css/ol.css" type="text/css">
  <link rel="stylesheet" href="https://unpkg.com/purecss@2.1.0/build/pure-min.css" integrity="sha384-yHIFVG6ClnONEA5yB5DJXfW2/KC173DIQrYoZMEtBvGzmf0PKiGyNEqe9N6BNDBH" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/fontawesome.min.css">
  <link rel="stylesheet" type="text/css" href="styles/fontawesome-free-6.1.1-web/css/solid.min.css">
  <link rel="stylesheet" type="text/css" href="styles/inline.css?2">
  <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
  <!-- Local stroge -->
  <script src="scripts/localforage.js"></script>
  <!-- Keep screen on -->
  <script src="scripts/NoSleep.min.js"></script>

  <script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
  <title><?php echo PROJECTTABLE ?></title>
</head>
<body>
<header class="header">

    <div class='pure-button button-secondary' style='margin-top:-12px;height:fit-content;font-size:90%;margin-right:2px;position:relative' onclick='$("#mainMenu").toggle()'><i class="fa-solid fa-bars"></i>
    <div id='mainMenu' style='display:none;position:absolute;top:36px;left:10px;background-color: rgb(66, 184, 221);border-radius:4px'>
        <ul style='margin:0;padding:4px'>
            <li style='list-style-type:none' onclick='Exit()'>Kilép</li>
        </ul>
    </div>
</div>
    <form method='get' class="pure-form" style='margin-top:-12px;font-size:90%'><span id='projectname'><?php echo ucfirst(PROJECTTABLE) ?>:</span>
        <select name='table' id='table_list' onchange='this.form.submit()'><option></option><option><?php echo PROJECTTABLE ?></option></select>
    </form>
    <h1>
    </h1>
    <button id="butInstall" aria-label="Install" hidden></button>
    <i class='fa-solid fa-eye' id="togglekeep" title="Wake Lock is enabled" style='color:lightskyblue'></i>
</header>
<script src="scripts/install.js"></script>
<div style='min-height:56px'></div>
<div id="map" class="map"></div>
<div id='info'></div>
<div id='geoinfo' style='padding:6px;border-radius:5px;position:fixed;top:60px;right:5px;background-color:white;opacity:0.85;z-index:1001;overflow-y: auto;'><span id='accuracy'></span> <span id='speed'></span></div>
<div style='position:absolute;right:0px;bottom:0px;padding:10px;background-color:white;opacity:0.8'>
    <select id='typeSelect' style='display:none'>
        <option value=''>Viewport</option>
        <option value='Polygon'>Polygon draw/edit</option>
        <option value='Circle'>Circle draw/edit</option>
    </select>
    <button onclick='$("#typeSelect").toggle()' class='pure-button'><i class="fa-solid fa-circle-nodes"></i></button>
    <button onclick='filter()' class='pure-button button-secondary' style='color:#fffb2a'>filter</button>
    <button onclick='clearF()' class='pure-button button-warning'>clear</button>
  </div>
<div id="myAuthModal" class="modal">
<div class="modal-content">
    <div class="close" onclick="Close('myAuthModal')">&times;</div>
    <form method='post' id='authbox' name='authbox'>
    <label for='username'>Username: </label><br><input name='username' id='username' class='' autocomplete="username">
    <br>
    <br>
    <label for='password'>Password: </label><br><input type='password' name='password' id='password' class='' autocomplete="current-password">
    <br><br>
    <button id='sendLogIn' class='pure-button button-xlarge button-success'>Log in</button>
    </form>
</div>
</div>

    <!--
    <div>
      position accuracy : <code id="accuracy"></code>&nbsp;&nbsp;
      altitude : <code id="altitude"></code>&nbsp;&nbsp;
      altitude accuracy : <code id="altitudeAccuracy"></code>&nbsp;&nbsp;
      heading : <code id="heading"></code>&nbsp;&nbsp;
      speed : <code id="speed"></code>
    </div>-->

<script type="text/javascript">
    const oauth_token_url = 'https://<?php echo URL ?>/oauth/token.php';
    const pds_url = 'https://<?php echo URL ?>/v2.4/pds.php';
    const current_table = '<?php echo isset($_GET['table']) ? $_GET['table'] : PROJECTTABLE ?>';
    const proxy_url = 'https://<?php echo URL ?>/private/proxy.php';
    const wms_cluster = '<?php echo $wms_cluster ?>';
    const min_zoom_for_filter = <?php echo $min_zoom_for_filter ?>;
    const oauth_client_id = 'pwa';
    const oauth_client_secret = '<?php echo constant('PWA_CLIENT_SECRET') ?>';

    const distanceInput = <?php echo $distanceInput ?>;
    const minDistanceInput = <?php echo $minDistanceInput ?>;
</script>
<script type="text/javascript" src="pwa.js?v<?php echo $version ?>"></script>

<script>
    // <!-- Register service worker. --->
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('service-worker.js')
            .then((reg) => {
              console.log('Service worker registered.', reg);
            });
      });
    }
</script>    
</body>
</html>
