<?php
// Database connection definitions
define('gisdb_user','sablon_admin');
define('gisdb_pass',$_ENV['SABLON_ADMIN_PASSWORD']);
define('gisdb_name','gisdata');
define('gisdb_host','gisdata'); //instead of localhost using docker's gisdata host

// Project's sql table name 
#define('PROJECTTABLE','your_database_table_name');
define('PROJECTTABLE',basename(__DIR__));

// Default project data restriction level
// 0 data read/modify for everybody
// 1 data read/modify only for logined users
// 2 data read/modify only for group members
// 3 data read/modify only own data
define('ACC_LEVEL',2);
define('MOD_LEVEL',2);

// Default language
define('LANG','en');

// Path settings
#define('OBMS','openbiomaps.org');
#define('OBMS',sprintf("%s",'localhost:9880/biomaps'));
define('PATH','/projects');
#define('URL',sprintf("%s%s%s",$HOST,PATH,'/'.PROJECTTABLE));
define('URL',sprintf("%s%s%s",$_SERVER['SERVER_NAME'],PATH,'/'.PROJECTTABLE));
#define('OAUTHURL',sprintf("http://%s%s%s",'localhost',PATH,'/'.PROJECTTABLE));

// Mapserver's variables
define('PRIVATE_MAPSERV',sprintf("%s/private/proxy.php",URL));
define('PUBLIC_MAPSERV',sprintf("%s/public/proxy.php",URL));
define('PRIVATE_MAPCACHE',sprintf("%s/private/cache.php",URL));
define('PUBLIC_MAPCACHE',sprintf("%s/public/cache.php",URL));
define('MAPSERVER','http://mapserver/cgi-bin/mapserv');
define('MAPCACHE','http://mapserver/mapcache');
define('MAP','PMAP');
define('PRIVATE_MAPFILE','private.map');

// Invitations
// 0 Only admins can invite new members, any grater value the maximum active invitations per member
define('INVITATIONS',10);

// Mail settings
define('SMTP_AUTH',false); # true
define('SMTP_HOST','...');
define('SMTP_USERNAME','...');
define('SMTP_PASSWORD','...');
define('SMTP_SECURE','tls'); # ssl
define('SMTP_PORT','587'); # 465
define('SMTP_SENDER','openbiomaps@...');

// UI Settings
define('SHINYURL',false);
define('RSERVER',false);
define('LOGINPAGE','map');
define('TRAINING',false);

define('MyHASH',$_ENV['SABLON_HASH']);

// Deafult TimeZone if you need
date_default_timezone_set('Europe/Budapest');

// Docker specific constant
define('OB_PROJECT_DOMAIN',OB_DOMAIN);

// Project languages, the first is the default
define('LANGUAGES', array('en'=>'in English','hu'=>'magyarul','ro'=>'română','ru'=>'русский'));

// Max image size of attachments in bytes
define('ALLOWED_FILE_SIZE',4194304);

// A developer variable
// use the debug_upload file to define debug options:
// login-email  return-type project-table form-id
// obmdebugger@bugfixer.org return_error checkitout 473
//define('DEBUG_UPLOAD',false);

// Generate verbose pds logging to openbiomaps.log
//define('DEBUG_PDS',false);

// Don not use temporary_tables.obs_... table
define('USE_TEMPTABLES_FOR_OBSLISTS','false');

// Overwrite default data export limit (1.000.000 rows)
// above the limit, a backround job triggering to handle the download process
//define('DATA_EXPORT_BGPROC_LIMIT','1000');

# MainPage settings
define('MAINPAGE',array(
    'template'=>'gridbox',
    'content1'=>'map',   // map | upload-table | slideshow
    'sidebar1'=>'members|uploads|data|species|species_stat', // members uploads data species species_stat
    'system_footer'=>'on',
    'system_header'=>'off',
));
# App Style
define('STYLE',array(
    'template'=>'evolvulus'
));
define('HEADER',array(
    'links' => 'upload|map|messages|profile|localize',
    'layout' => 'obm'
    )
);
define('FOOTER',array(
    'links'=>'upload|map|about|terms|privacy',
    'languages'=>'languages',
    'partners' => array(
        array('img'=>'obm_logo.png','size'=>'110','url'=>'https://openbiomaps.org'),
        array('img'=>'unideb_logo.png','size'=>'','url'=>'https://unideb.hu')
    )));


define('CACHE_HOST', $_ENV['CACHE_HOST'] ?? 'localhost');
define('CACHE_PORT', $_ENV['CACHE_PORT'] ?? 11211);

define('OPENID_CONNECT', [
    'google' => [
        'client_id' => 'xxxxx.apps.googleusercontent.com',
        'client_secret' => 'xxxxxxx',
        'provider_url' => 'https://accounts.google.com/',
    ],
]);
define('OPENID_CONNECT_CERT_PATH', '/etc/ssl/certs/ca-certificates.crt');

# Show pwa app link on mainpage
define('PWA_LINK','on');

# Above this limit the data export working instead of normal download
define('DATA_EXPORT_BGPROC_LIMIT',1000);
?>
