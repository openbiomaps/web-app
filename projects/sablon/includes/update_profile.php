<?php
/* Ajax functions
 *
 * */
require_once(getenv('OB_LIB_DIR').'db_funcs.php');

session_start();

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

// csak bejelentkezve és csak a saját profilunkat tudjuk szerkeszteni
if(!isset($_SESSION['Tid'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

$user = new User($_SESSION['Tid']);

require_once(getenv('OB_LIB_DIR').'languages.php');

if (!isset($_POST['name'])) exit;
else $post_name=preg_replace("/[^a-z]/","",$_POST['name']);
#if (!isset($_POST['id'])) return;
#else $post_id=preg_replace("/[^0-9]/","",$_POST['id']); 
if (!isset($_POST['text'])) exit;
else $post_text = $_POST['text'];

$text = strip_tags($post_text);
if(strlen($text)>252) exit;
   
if (!in_array($post_name,array('user','password','email','address','institute','username','orcid','familyname','givenname','references','dropmyaccount'))) {
    print "Invalid request";
    exit;
}
$usernameset = "";

if ($post_name == 'password') {
    $passwd_rep = preg_replace("/[^a-zA-Z0-9@.,-_+! ]/i","", $text);
    if (strlen($text)!=strlen($passwd_rep)) { 
        print 'Invalid request:Only a-zA-Z0-9@.,-_+! characters allowed!';
        exit;
    } elseif (strlen($text)<4) {
        print 'Invalid request:More than 4 characters needed!';
        exit;
    }
}
if ($post_name == 'user') {
    // auto generated, no way to manual update
    exit;
}
if ($post_name == 'orcid') {
    if (strlen($text)>20) {
        print 'Invalid request:Too long id!';
        exit;
    }
}
if ($post_name == 'familyname') {
    if (strlen($text)>512) {
        print 'Invalid request:Too long family name!';
        exit;
    }
    $usernameset = sprintf("username=concat_ws(' ',%s,array_to_string(givenname,' '))",quote($text));
    $text = preg_split('/ /',$text);
    $text = "{".implode(",",$text)."}";
}
if ($post_name == 'givenname') {
    if (strlen($text)>512) {
        print 'Invalid request:Too long given name!';
        exit;
    }
    $usernameset = sprintf("username=concat_ws(' ',array_to_string(familyname,' '),%s)",quote($text));
    $text = preg_split('/ /',$text);
    $text = "{".implode(",",$text)."}";
}
if ($post_name == 'username') {
    if (strlen($text)>100) {
        print 'Invalid request:Too long name!';
        exit;
    }
}
if ($post_name == 'references') {
    if (strlen($text)>512) {
        print 'Invalid request:Too long id!';
        exit;
    }
    $text = preg_split('/,/',$text);
    $text = "{".implode(",",$text)."}";
}
//drop account
if ($post_name == 'dropmyaccount') {

    $text = preg_replace('/[^a-zA-Z0-9@._-]/','',$text);
 
    $M = new Messenger();
    
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 32 );
    
    $_SESSION['drop_my_profile'] = [
        'code' => $random_string,
        'profile' => $_SESSION['Tcrypt'],
    ];
    
    $resp = ($M->send_system_message($user, 'dropmyaccount', compact('random_string'), true )) ? 'ok' : 'fail';
    
    echo common_message($resp, 1);
    exit;
}

/* change email address */
if ($post_name == 'email') {

    $text = preg_replace('/[^a-zA-Z0-9@._-]/','',$text);
 
    $M = new Messenger();
    
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 32 );
    
    $_SESSION['emailchange'] = [
        'code' => $random_string,
        'addr' => $text,
    ];

    if ($M->send_system_message($user, 'change_email_address', compact('random_string'), true )) {
        echo 'OK:check_your_mailbox';
    }
    
    exit;
}
if ($post_name != 'password') {
    $text = quote($text);
} else {
    /* change password */
    if (!$text = gen_password_hash($text)) {
        exit;
    }
    $text = quote($text);
    unset($_SESSION['register_upw']);
}

$ret_text = "OK"; #$text;

$cmd = sprintf("UPDATE \"public\".\"users\" SET \"$post_name\"=%s WHERE id='{$_SESSION['Tid']}'",$text);
$res = pg_query($BID,$cmd);
if ($usernameset!='') {
    $cmd = sprintf("UPDATE \"public\".\"users\" SET $usernameset WHERE id='{$_SESSION['Tid']}'");
    $res = pg_query($BID,$cmd);
}

if ($res and pg_affected_rows($res)) {
    print $ret_text;
    obm_cache('delete',"get_profile_data_".$_SESSION['Tid']);
}
else {
    if (preg_match('/duplicate key/',pg_last_error($BID))) {
        $error = "Invalid request:This key already exists!";
    }
    else {
        $error = pg_last_error($BID);
    }
    print "Invalid request:$error";
}
//pg_close($BID);  
?>
