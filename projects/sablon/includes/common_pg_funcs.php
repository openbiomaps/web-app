<?php
/* ***************************************************************************  /

VARIABLES FOR ALL BIOMAPS DATABASES

****************************************************************************** */
#xdebug_start_trace('/tmp/xdebug');

/* ***************************************************************************  /

GENERAL and GLOBAL FUNCTIONS
Do not change them
Use the local_funcs.php if you need specific functions

****************************************************************************** */
/* recursive copy of directory structure
 * create_new_project
 *
 * */


# result table printout class
class createTable {
    public $def=array('tid'=>'','tclass'=>'','theader'=>'','tbody'=>'','tstyle'=>'');

    private $table;
    private $th = array();
    private $tr = array();
    private $td_format = array();
    private $format_cols = array();
    private $format_string = array();
    private $diff = array();
    private $cols = 0;
    private $id = '';
    private $type = 'table';

    public function __construct ($id='',$type='table') {
        $this->id = $id;
        $this->type = $type;
    }

    public function def($arr) {
        foreach ($arr as $key=>$val) {
            if ($key == 'tid') $this->def['tid'] = $val;
            elseif ($key == 'tclass') $this->def['tclass'] = $val;
            elseif ($key == 'class') $this->def['tclass'] = $val;       // simplified tclass name
            elseif ($key == 'tbody') $this->def['tbody'] = $val;
            elseif ($key == 'theader') $this->def['theader'] = $val;
            elseif ($key == 'tstyle') $this->def['tstyle'] = $val;
        }
        if ($this->id == '') $this->id = $this->def['tid'];
    }
    /*  format_col: hányadik oszlopokkal
     *  format_string: mit csinál
     *  kissé bonyolult...
     * */
    public function tformat($func) {
        #'format_col'=>'2';'format_string'=>'<a href=\'?id=COL-1\'>COL-2</a>'
        $this->format_cols = explode(';',$func['format_col']);
        $a = array();
        $this->format_string = explode(';',$func['format_string']);
        for($i=0;$i<count($this->format_cols);$i++) {
            $b = $this->format_cols[$i];
            $a[$b]=$this->format_string[$i];
        }
        $this->format_string = $a;
    }

    /* line: array of elements will be displayed in cells
     * format: style or other control things will in <td ....> element
     * */
    public function addRows($line,$format='') {
        array_push($this->tr,$this->addCells($line));
        array_push($this->td_format,$format);
    }
    public function addHeader($line) {
        $this->th = $this->addCells($line);
    }
    // should be renamed to printTable!!!
    public function printOut() {
        if ($this->type=='table') {
            $this->table .= sprintf("<table id='%s' class='%s' style='%s'>\n",$this->id,$this->def['tclass'],$this->def['tstyle']);
            $this->table .= sprintf("<thead>%s</thead>",$this->formatHeader($this->th));
            $this->table .= sprintf("<tbody>%s</tbody>",$this->formatRow($this->tr));
            $this->table .= "</table><br>";
        } else {
            $this->table .= sprintf("<div id='%s' class='tbl %s' style='%s'>\n",$this->id,$this->def['tclass'],$this->def['tstyle']);
            $this->table .= sprintf("%s",$this->formatHeader($this->th));
            $this->table .= sprintf("%s",$this->formatRow($this->tr));
            $this->table .= "</div>";
        
        }
        return $this->table;
    }
    public function nRows() {
        return count($this->tr);
    }


    private function addCells($line) {
        $tr = array();
        foreach($line as $cell) {
            # content replace function call
            array_push($tr,$cell);
        }
        return $tr;
    }
    private function diffRows() {
        $i=0;
        $diff = array();
        $dr = $this->tr;
        $hrp = array();
        while($row = array_shift($dr)) {
            if (count($hrp))
                $diff[$i] = array_keys(array_diff_assoc($hrp,$row));
            $hrp = $row;
            $i++;
        }
        $this->diff = $diff;
    }
    private function formatHeader($rows) {
        $lines = "";
        $row_id = 0;
        $line = "";
        $cell_id = 0;
        $cpan=0;
        $csprint=0;
        foreach ($rows as $cell) {
            $this->cols++;
            if ($cell == '-1') continue;
            $m=array();
            $csp = '';
            if (preg_match('/^COLSPAN=(\d+)$/',$cell,$m)) {
                $cpan = $m[1];
                $csprint=1;
                continue;
            }
            if ($csprint) {
                $csp = "colspan='$cpan'";
                $csprint--;
                $cpan--;
                $cpan--;
            }
            elseif ($cpan>0) {
                $cpan--;
                continue;
            }

            if ($this->type=='table') 
                $line .= sprintf('<th %s id=\'%sh-%d-%d\'>%s</th>',$csp,$this->id,$row_id,$cell_id,$cell);
            else
                $line .= sprintf('<div class=\'tbl-h\' %s id=\'%sh-%d-%d\'>%s</div>',$csp,$this->id,$row_id,$cell_id,$cell);
            $cell_id++;
        }
        if ($this->type=='table') 
            $lines .= sprintf("<tr>%s</tr>\n",$line);
        else
            $lines .= sprintf("<div class='tbl-row'>%s</div>\n",$line);
        return $lines;
    }
    private function formatRow($rows) {
        $lines = "";
        $row_id = 0;
        foreach ($rows as $row) {
            $line = "";
            $cell_id = 0;
            $cpan='';
            $csprint=0;
            for ($i=0;$i<$this->cols;$i++) {
                if ($this->th[$i] == '-1') continue;
                $cell = $row[$i];
                if (isset($this->format_string[$i])) $f = $this->format_string[$i];
                else $f = 'undef';
                for($k=0;$k<$this->cols;$k++) {
                    if ($f=='undef') continue;
                    if (preg_match("/COL-$k/",$f)) {
                        $f = preg_replace("/COL-$k/",$row[$k],$f);
                        $cell = $f;
                    }
                }
                $m=array();
                $csp = '';
                if (preg_match('/^COLSPAN=(\d+)$/',$cell,$m)) {
                    $cpan = $m[1];
                    $csprint=1;
                    continue;
                }
                if ($csprint) {
                    $csp = "colspan='$cpan'";
                    $csprint--;
                    $cpan--;
                    $cpan--;
                }
                elseif ($cpan>0) {
                    $cpan--;
                    continue;
                }
                if ($this->type=='table') 
                    $line .= sprintf('<td %s %s id=\'%s-%d-%d\'>%s</td>',$this->td_format[$row_id],$csp,$this->id,$row_id,$cell_id,$cell);
                else
                    $line .= sprintf('<div class=\'tbl-cell\' %s %s id=\'%s-%d-%d\'>%s</div>',$this->td_format[$row_id],$csp,$this->id,$row_id,$cell_id,$cell);
                $cell_id++;
            }
            $row_id++;
            if ($this->type=='table') 
                $lines .= sprintf("<tr>%s</tr>\n",$line);
            else
                $lines .= sprintf("<div class='tbl-row'>%s</div>\n",$line);
        }
        return $lines;
    }
}

# WMS WFS Layer definitions
# defLayer('WMS','Dinpi Biotika',$url,$WMS_MAP,'project'=>'dinpi','l'=>'dinpi','b'=>'true','o'=>'1.0','f'=>'image/png','v'=>'true','t'=>'true')
#new OpenLayers.Layer.WMS('Dinpi Biotika','http://$url/dinpi/$auth/mapserv',
#   {map:'$WMS_MAP',layers:'dinpi',isBaseLayer:false,visibility:true,opacity:1.0,format:'image/png',transparent:true});"
class defLayer {

    public $Olayer='';
    private $opts='';
    private $Source='';
    private $Name='';
    private $Url='';
    private $Map='';
    private $Cname='';
    public $singleTile = false;

    function __construct($arr)
    {
        foreach ($arr as $key=>$val) {
            if ($key == 'Source') $this->Source = "$val";
            elseif ($key == 'Name') $this->Name = "$val";
            elseif ($key == 'Url' and $val!='') $this->Url = "$val";
            elseif ($key == 'SingleTile' and $val=='t') $this->singleTile = true;
            elseif ($key == 'Opts' and $val!='') $this->opts = $val;
            elseif ($key == 'Map' and $val!='') $this->Map = "$val";
            elseif ($key == 'Cname' and $val!='') $this->Cname = "$val";
        }
    }
    function printLayerSources() {
        //$options = implode(',',$this->opts);
        //debug(array_filter(array($this->Map,$this->opts,$this->Cname)));
        $source = '';
        $skip_process = false;
        $this->Olayer = '';

        if ($this->Source == 'XYZ') {
            //  Mapbox XYZ, ...
            // $url = '[\'http://a.tile.openstreetmap.org/${z}/${x}/${y}.png\',\'http://b.tile.openstreetmap.org/${z}/${x}/${y}.png\',\'http://c.tile.openstreetmap.org/${z}/${x}/${y}.png\'],';
            
            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = $this->Source;
            $init = $options;
        
        } 
        elseif($this->Source == 'OSM') {
            
            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = 'OSM';
            $init = '';
        } 
        elseif($this->Source == 'Bing') {

            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = 'BingMaps';
            if (defined('BING_API_KEY')) {
                $options = preg_replace('/BING_API_KEY/', BING_API_KEY, $options);
            }
            $init = $options;

        } 
        elseif($this->Source == 'TomTom') {

            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = 'XYZ';
            if (defined('TOMTOM_API_KEY')) {
                $options = preg_replace('/TOMTOM_API_KEY/', TOMTOM_API_KEY, $options);
            }
            $init = $options;

        } 
        // local WMS
        elseif ($this->Source == 'WMS' or $this->Source == 'LocalWMS') {

            // OpenLayer Options for local mapserver proxy
            $options = array_filter(array_merge(array("map"=>$this->Map),$this->opts,array("CNAME"=>$this->Cname)));
            $options = preg_replace('/"/',"'",json_encode($options,JSON_UNESCAPED_SLASHES));
            $source = 'ImageWMS';

                /*new ol.source.ImageWMS({
                    url: 'http://localhost/biomaps/resources/private/proxy.php?map=PMAP',
                    params: {'LAYERS': 'dinpi_points','MAP':'PMAP','CNAME':'layer_data_biotika',
                        'isBaseLayer':'false', 'visibility':'true', 'opacity':'1.0', 'format':'image/png', 'transparent':'true', 'numZoomLevels':'20'},
                    ratio: 1,
                    singleTile: true,
                    serverType: 'mapserver',
                })*/
            if ($this->singleTile)
                $init = "{url: '$this->Url',params: ".$options.", ratio:1, serverType: 'mapserver', singleTile: true}";
            else
                $init = "{url: '$this->Url',params: ".$options.", ratio:1, serverType: 'mapserver'}";
        }
        // remote WMS service
        elseif ($this->Source == 'ImageWMS') {

            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = 'ImageWMS';
            $init = "{url: '$this->Url',params: ".$options.", ratio:1, serverType: 'mapserver'}";
        }
        // remote WMS service
        elseif ($this->Source == 'TileWMS') {
            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = 'TileWMS';
            $init = "{url: '$this->Url',params: ".$options.", ratio:1, serverType: 'mapserver'}";
        }
        else {
            // Empty options  
            // Google ???
            $options = preg_replace('/"/',"'",json_encode($this->opts,JSON_UNESCAPED_SLASHES));
            $source = "TileImage";
            $init = "{".$options."}";
            log_action("Not supported layer definition: $source($options)",__FILE__,__LINE__);
            $skip_process = true;
        }

        $init = preg_replace("/'?layers'?:/","'LAYERS':",$init);

        if (!$skip_process)
            $this->Olayer = "new ol.source.".$source."(".$init.")";

        return $this->Olayer;
    }

}

function print_file_content ($path, $name, $attachment=true) {
    
    $fp = file_get_contents($path);
    $filesize = strlen($fp);
    $filename = basename($path);
    $name = sprintf("%s",basename($name));
    # header definíció!!
    header("Content-Type: text");
    header("Content-Length: $filesize");
    if ($attachment)
        header("Content-Disposition: attachment; filename=\"$name\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    //header("OriginalFileName: \"$filename\"");
    printf("%s",$fp);

    return;
}

## normal functions

function copyr($source, $dest, $make_symlinks = 0){
    $err = 0;
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        while ($file=readdir($dir_handle)){
            if ($file!="." && $file!=".."){
                if (is_dir($source."/".$file)){
                    if (!mkdir($dest."/".$file)) $err++;
                    copyr($source."/".$file, $dest."/".$file, $make_symlinks);
                } else {
                    if ($make_symlinks) {
                        if (!symlink($source."/".$file, $dest."/".$file)) $err++;
                    } else {
                        if (!copy($source."/".$file, $dest."/".$file)) $err++;
                    }
                }
            }
        }
        closedir($dir_handle);
    } else {
        if ($make_symlinks) {
            if (!symlink($source, $dest)) $err++;
        } else {
            if (!copy($source, $dest)) $err++;
        }
    }
    return $err;
}
/* results_builder
 * interface
 * NOT USED !!!
 * */
function lang_label($lab) {
    # return the constans value if its exists
    # specifically used for set the language specific "species name" label
    if (defined($lab)) $label = constant($lab);
    else $label = $lab;

    return $label;
}

// user info
// return: http link with the user's name
function fetch_user($id,$type='userid',$return='link') {

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    global $BID;
    if ($id=='') return false;

    // possible define other types, e.g., fetch by name...
    if ($type=='userid') {
        $id = preg_replace("/[^0-9-]/","", $id);
        $cmd = sprintf("SELECT username,\"user\" FROM users WHERE id=%s",quote($id));
        $res = pg_query($BID,$cmd);
    } elseif ($type=='roleid') {
        $id = preg_replace("/[^0-9-]/","", $id);
        $cmd = sprintf("SELECT username,\"user\" FROM users LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE role_id=%s",PROJECTTABLE,quote($id));
        $res = pg_query($BID,$cmd);
    }

    if ($return=='link') {
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $eurl = sprintf("$protocol://".URL.'/profile/%s/',$row['user']);

            $link = "<a href=$eurl target='_blank'>{$row['username']}</a>";
            return $link;
        }
    }
    return false;
}
// flexible functions to get user's data
class userdata {
    private $type;
    private $id;
    public $hash;

    public function __construct ($id,$type) {
        $this->idtype = $type;
        $this->id = $id;
        if ($type == 'hash')
            $this->hash = $id;
        elseif ($type == 'roleid') {
            $this->hash = $this->hash_from_roleid($id);
        }
        elseif ($type == 'userid') {
            $this->hash = $this->hash_from_userid($id);
        }
    }
    // get username from hash
    function get_username() {
        global $BID;
        $cmd = sprintf("SELECT username FROM \"users\" WHERE \"user\"=%s",quote($this->hash,true));
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return false;
        $row = pg_fetch_assoc($result);
        return $row['username'];
    }
    // get description from role_id
    function get_role_description() {
        global $BID;
        $cmd = sprintf("SELECT description FROM \"project_roles\" WHERE \"role_id\"=%s",quote($this->id,true));
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return false;
        $row = pg_fetch_assoc($result);
        return $row['description'];
    }

    // get role_id from hash
    function get_roleid() {
        global $BID;
        $cmd = sprintf("SELECT role_id FROM project_roles LEFT JOIN \"users\" ON (id=user_id AND project_table='%s') WHERE \"user\"=%s",PROJECTTABLE,quote($this->hash,true));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['role_id'];
        }
        return -1;
    }
    // get user_id from hash
    function get_userid() {
        global $BID;
        $cmd = sprintf("SELECT id FROM \"users\" WHERE \"user\"=%s",quote($this->hash,true));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['id'];
        }
        return -1;
    }
    // get user_id from role_id
    function hash_from_roleid($roleid) {
        global $BID;
        $cmd = sprintf("SELECT \"user\" FROM \"users\" LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE role_id=%s",PROJECTTABLE,quote($roleid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['user'];
        }
        return -1;
    }
    // get user hash from user_id
    function hash_from_userid($userid) {
        global $BID;
        $cmd = sprintf("SELECT \"user\" FROM \"users\" LEFT JOIN project_roles ON (id=user_id AND project_table='%s') WHERE id=%s",PROJECTTABLE,quote($userid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            return $row['user'];
        }
        return -1;
    }
}

// get users' profile data
// called in profile.php
// some as pds's profile()
function get_profile_data($table,$userhash) {
    global $BID,$ID;
    # Ha a userid tömb: (id1,id2,...)...
    # akkor nem ad vissza semmit
    # ha az egyes lekérdezések atomi funkciók volnának, akkor a profile, össze tudná rakni, de lényegében nincs értelme

    $userdata = new userdata($userhash,'hash');
    //$username = $userdata->get_username();
    $role_id = $userdata->get_roleid();
    $userid = $userdata->get_userid();

    if (!$userid) return false;

    $userdata = obm_cache('get',"get_profile_data_$userid");
    if ($userdata!==FALSE) {
        return $userdata;
    }

    // feltöltések száma
    $cmd = sprintf("SELECT id,project_table FROM system.uploadings WHERE uploader_id=%d AND project=%s",$role_id,quote(PROJECTTABLE));
    $res = pg_query($ID,$cmd);
    $uplcount = pg_num_rows($res);
    $upid = array();
    $upid_table = array();
    $upld = array();
    $modcount = '';
    while ($row=pg_fetch_assoc($res)) {
        if (!isset($upid_table[$row['project_table']]))
            $upid_table[$row['project_table']] = array();
        $upid_table[$row['project_table']][] = $row['id'];
        array_push($upid,$row['id']);
    }

    // Count of records
    if ($table) {
        foreach ($upid_table as $project_table=>$id_list) {
            // do not count for temporary tables
            if (preg_match('/^temporary_tables\..+/', $project_table)) {
                continue;
            }
            $cmd = sprintf("SELECT f_project_schema,f_project_table
                            FROM header_names
                            WHERE f_project_name='%s' AND f_project_table=%s",PROJECTTABLE,quote($project_table));
            $res3 = pg_query($BID,$cmd);
            if (pg_num_rows($res3)) {
                $rows3 = pg_fetch_assoc($res3);
                $schema = $rows3['f_project_schema'];
            }

            // feltöltött rekordok száma
            // nagyon nagy hosszú tud lenni!!!
            $cmd = sprintf("SELECT count(*) AS c FROM \"%s\".\"%s\" WHERE obm_uploading_id IN (%s)",$schema,$project_table,implode(',',$id_list));
            $res2 = pg_query($ID,$cmd);
            if (pg_num_rows($res2)) {
                $row2=pg_fetch_assoc($res2);
                $upld[] = $row2['c'];
            }
        }
    }
    $upld = array_sum($upld);


    // módosított sorok száma
    if ($table) {
        $cmd = sprintf("SELECT count(*) AS c FROM \"%s\" WHERE obm_modifier_id=%s",$table,quote($userid));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row=pg_fetch_assoc($res);
            $modcount = $row['c'];
        }
    }

    // Count of observation lists
    $cmd = sprintf("SELECT count(id)
                    FROM system.uploadings 
                    WHERE project='%s' AND uploader_id=%d AND
                        metadata::jsonb ? 'observation_list_id' AND metadata::jsonb ? 'observation_list_start'", 
                        PROJECTTABLE, $role_id);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row=pg_fetch_assoc($res);
        $obslistcount = $row['count'];
    }

    $st = "";
    if ($table) {
        $st = "LEFT JOIN project_users pup ON (pup.user_id=users.id AND pup.project_table='$table')";
    }

    // groups eliminated!!!
    $cmd = sprintf("SELECT %d AS uplcount,'%s' AS upid,%d AS upld,%d AS modcount,pu.user_status,status,
        institute,orcid,address,username,array_to_string(givenname,' ') as givenname,array_to_string(familyname,' ') as familyname,
        array_to_string(\"references\",',') as \"references\",\"user\",email,validation,pu.project_table,pu.receive_mails,pu.visible_mail,
        %d AS obslistcount,inviter,openid_provider
        FROM public.users
        LEFT JOIN project_users pu ON (pu.user_id=users.id )
        $st
        WHERE public.users.id=%s
        GROUP BY users.id,pu.project_table,pu.user_status,pu.receive_mails,pu.visible_mail
        ORDER BY pu.project_table",$uplcount,implode(',',$upid),$upld,$modcount,$obslistcount,quote($userid));

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r1 = pg_fetch_all($res);
        $c = array_combine(array_keys($r1[0]),array_values($r1[0]));

        obm_cache('set',"get_profile_data_$userid",json_encode($c),180);
        return json_encode($c);
    }
    return false;
}

// login window
// The simplified parameter only used in Swith profile action.
function login_box ($simplified=FALSE) {
    global $protocol;
  
    $url = $protocol . "://". URL;
    $callback = $simplified ? "$url/profile" : "$url/"; # I've removed the closing index.php as it is not exists on the routed URL-s, but works on where index.php is the resolver.
    $reauth = $simplified ? "&reauth" : "";
    
    return "<a href='$url/weblogin.php?callback=$callback&scope=webprofile&client_id=web$reauth' class='login_button'><i class='fa fa-sign-in' aria-hidden='true'></i>&nbsp;<span>".t(str_login)."</span></a>";
}

function generate_openid_button($service) {
    global $protocol;

    if (!defined('OPENID_CONNECT') || !isset(OPENID_CONNECT[$service])) {
        return "";
    }
    if (!isset(OPENID_CONNECT[$service]['client_id'])) {
        log_action("OpenId connect configuration error: client ID is missing for service $service");
        return "";
    }
    $buttons = '';
    if ($service == 'google') {
        $buttons .= "<a href='$protocol://" . URL . "/includes/openid.php?provider=google' id='sign-in-with-google'>Sign in with Google</a>"; 
    }

    return $buttons;
}

//request invitation from project admins
function request_invitation() {
    ob_start();
    //by default it is enabled:
    $request_invitation_enabled = !( get_option('request_invitation') === 'disabled' );
?> 
<?php if ($request_invitation_enabled): ?>
    <form class="pure-form pure-form-stacked" id="request-invitation" method="post">
        <fieldset>
            <legend><?= t('str_request_invitation_help') ?></legend>
            
            <label for="name"><?= t('str_name') ?></label>
            <input id="name" name="name" type="text" required placeholder="<?= t('str_name') ?>">

            <label for="email"><?= t('str_email') ?></label>
            <input id="email" name="email" type="email" required placeholder="<?= t('str_email') ?>">

            <label for="your_message"><?= t('str_message') ?></label>
            <textarea id="your_message" name="your_message" rows="8" cols="80" required></textarea>
            
            <label for="captcha"><?= t('str_retype_the_characters_from_the_picture') ?></label>
            <div class="captcha-div">
                <img id="captcha-img" src="<?= "//" . constant('URL') . "/ajax?refresh-captcha&v=". microtime() ?>" >
                <button type="button" id="refresh-captcha" class="pure-button button-small"> <i class="fa fa-refresh"></i> </button>
            </div>
            <input id="captcha" name="captcha" type="text">
            
            <input type="hidden" name="action" value="request_invitation">

            <input type='submit' class='button-success button-xlarge pure-button pure-u-1-3' value='<?= t('str_send'); ?>'>
        </fieldset>
    </form>
<?php endif; ?>
    <hr>
    <?= t('str_invitate'); ?>
<?php
}

function send_invitation_request() {
    if ($_POST['captcha'] !== $_SESSION['phrase']) {
        return false;
    }
    
    $M = new Messenger();
    $admins = new Role('project masters');
    $words = [
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'your_message' => $_POST['your_message'],
        'lang' => LANGUAGES[$_SESSION['LANG']],
    ];
    foreach ($admins->get_members() as $to) {
        $M->send_system_message($to, 'invitation_request', $words, true);
    }
    return true;
}

//data changes
function data_changes($hist_table,$data_table,$data_row_id) {
    global $ID;
    $hm = '';
    $ht = '';
    $hc = 0;
    $ulink = '';
    $cmd = sprintf('SELECT to_char(hist_time,\'YYYY-MM-DD HH24:MI\') as hist_time,obm_modifier_id
        FROM %1$s LEFT JOIN %2$s ON(row_id=obm_id)
        WHERE row_id=%3$s AND %1$s.data_table=\'%2$s\' ORDER BY hist_time DESC',$hist_table,$data_table,quote($data_row_id));
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $hm = $row['obm_modifier_id'];
        $ht = $row['hist_time'];
        $hc = pg_num_rows($res);
    }
    //fetch modifier user's profile link
    $ulink = fetch_user($hm,'userid','link');
    return array('time'=>$ht,'link'=>$ulink,'count'=>$hc);
}

/* tracklog object processing
 * extract times from tracklog, convert tracklog
 * NOT READY!
 * */
function tracklog_process($tracklog) {
    /*
     *  {
	    "trackLogId": "ca9ed464-e542-4839-bd95-8755d99eda4d",
            "trackLogName": "OpenBirdMaps_Alkalmimadármegfigyelések_2021-04-17-16:09:33-16:55:19",
            "startTime": 1618675773746,
	    "endTime": 1618678519337,
optional:        "observation_list_id": "...",
	    "trackLogArray": [{
		"longitude": 24.6335816,
		"latitude": 46.6481496,
		"timeStamp": 1618675783834,
		"accuracy": 19
                }, { .. }
            ]
        }
     */
    $j = json_decode($tracklog, true);

    $time1 = round($j['startTime']/1000);
    $time2 = round($j['endTime']/1000);
    $name = $j['trackLogName'];
    $id = $j['trackLogId'];
    if (isset($j['observation_list_id']))
        $observation_list_id = $j['observation_list_id'];
    else
        $observation_list_id = "";
    
    $geojson = obm_tracklog_to_geojson($j['trackLogArray'],'points');

    return array($time1,$time2,$geojson,$name,$id,$observation_list_id);
}
/* Parameter 1 is a tracklogArray
 *  [{
        "longitude": 24.6335816,
	"latitude": 46.6481496,
	"timeStamp": 1618675783834,
	"accuracy": 19
    }] 
    Parameter 2 is the output type
    points or linestring
 * */
function obm_tracklog_to_geojson ($tracklog,$type='points') {

    if (!count($tracklog)) return false;

    $geojson = array("type"=>"FeatureCollection","features"=>array());
    
    if ($type=='linestring') {
        $geojson["features"] = array(
                "type"=>"Feature",
                "properties"=>array(),
                "geometry"=>array("type"=>"LineString","coordinates"=>array()));

    }

    foreach ($tracklog as $trackpoint) {
        if ($type=='points') {
            $geojson["features"][] = array(
                "type"=>"Feature",
                "properties"=>array("timestamp"=>$trackpoint["timestamp"],"accuracy"=>$trackpoint["accuracy"]),
                "geometry"=>array("type"=>"Point","coordinates"=>array($trackpoint["longitude"],$trackpoint["latitude"])));
        }
        elseif ($type=='linestring') {
             $geojson["features"]["geometry"]["coordinates"][] = array($trackpoint["longitude"],$trackpoint["latitude"]);

        }
    }
    
    return json_encode($geojson);
}

/* shell function
 * called in results_builder class
 * */
function gpsbabel($d) {
    #$res = system("echo '$csv' | R --vanilla --slave \"a<-read.csv(pipe('cat /dev/stdin'),sep='#'); summary(a)\"");
    #

    $gpx_file = sprintf("%s%s_download.gpx",OB_TMP,session_id());
    $tmp_file = sprintf("%s%s_download_gpx.csv",OB_TMP,session_id());
    $handle = fopen($tmp_file, "w");
    $fwrite = fwrite($handle,$d);
    fclose($handle);

    if (!$fwrite)
        log_action("No data written to create GPX, No space left on device?",__FILE__,__LINE__);

    //$res = system("echo '$d' | gpsbabel -t -i unicsv -f - -x transform,wpt=trk,del -o gpx -F $f 2>&1",$r);
    system("cat $tmp_file | gpsbabel -t -i unicsv -f - -x transform,wpt=trk,del -o gpx -F $gpx_file",$ret);
    if ($ret==127) {
        log_action("gpsbabel not found.",__FILE__,__LINE__);
    } else {
        unlink($tmp_file);

        $handle = fopen($gpx_file, "rb");
        $contents = fread($handle, filesize($gpx_file));
        fclose($handle);
        unlink($gpx_file);
        return $contents;
    }
    return false;
}

/**
 * Determines which columns should be exported in the GPX or KML files
 **/
function determineGpxKmlColumns() {
    $st_col = st_col($_SESSION['current_query_table'],'array');
    
    if (defined('GPX_NAME_COLUMN')) {
        if (is_array(GPX_NAME_COLUMN) && isset(GPX_NAME_COLUMN[$_SESSION['current_query_table']])) {
            $SPECIES_C =  GPX_NAME_COLUMN[$_SESSION['current_query_table']];
        }
        elseif (is_string(GPX_NAME_COLUMN)) {
            $SPECIES_C =  GPX_NAME_COLUMN;
        }
    }
    if (!isset($SPECIES_C)) {
        if (!isset($st_col['SPECIES_C']) or $st_col['SPECIES_C']=='') {
            $SPECIES_C = "'SPECIES_C'";
        }
        else {
            $SPECIES_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m)) ? $m[2] : $st_col['SPECIES_C'];
        }
    }
    
    // special case obm_taxon - for multilingual taxon names
    if ($SPECIES_C === 'obm_taxon') {
        $SPECIES_C = $st_col['SPECIES_C'];
    }
    
    if (defined('GPX_DESC_COLUMN')) {
        if (is_array(GPX_DESC_COLUMN) && isset(GPX_DESC_COLUMN[$_SESSION['current_query_table']])) {
            $NUM_IND_C =  GPX_DESC_COLUMN[$_SESSION['current_query_table']];
        }
        elseif (is_string(GPX_DESC_COLUMN)) {
            $NUM_IND_C =  GPX_DESC_COLUMN;
        }
    }
    if (!isset($NUM_IND_C)) {
        if (!isset($st_col['NUM_IND_C']) or  $st_col['NUM_IND_C']=='') {
            $NUM_IND_C = "'NUM_IND_C'";
        }
        else {
            $NUM_IND_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['NUM_IND_C'],$m)) ? $m[2] : $st_col['NUM_IND_C'];
        }
    }
    
    return compact('SPECIES_C', 'NUM_IND_C');
}

/* return with a random hash string
 * */
function genhash($len=16) {
    // return with default(16) byte long random string
    $hash='';
    $base='ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz';
    $max=strlen($base)-1;
    mt_srand((double)microtime()*1000000);
    while (strlen($hash)<$len+1) $hash .= $base[mt_rand(0,$max)];
    return $hash;
}
/* return uuidv4 string */
function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}


/* create a php file with
 * define('',''); structure
 * create_new_project.php
 * */
function create_define_file($target,$defs) {
    if(!$fp=fopen("$target",'w'))
        return false;
    fwrite($fp, sprintf("<?php\n"));
    foreach ($defs as $key=>$var) {
        if (preg_match('/^sprintf/',$var)) $var = $var;
        elseif (preg_match('/^array/',$var)) $var = $var;
        elseif ($var == 'false')
            $var = $var;
        elseif ($var == 'true')
            $var = $var;
        else 
            $var = "'$var'";

        if (preg_match('/^#/',$key)) 
            fwrite($fp, sprintf("#define('%s',%s);\n",preg_replace('/[^a-z0-9_]/i','_',$key),$var));
        else
            fwrite($fp, sprintf("define('%s',%s);\n",preg_replace('/[^a-z0-9_]/i','_',$key),$var));
    }
    fwrite($fp, sprintf("?%s\n",">")); // just for vi syntax highlight
    fclose($fp);
    return true;
}
/* Assemble an SQL query string for mapserver and results_query
 * layer = optional | mapserver's layer name
 * query_type  = base/query or proxy as alias of both
 * bbox  = optional | boundary box for query: four numeric coordinate value of corners
 * */
function assemble_query($layer='',$query_type='',$bbox='',$cname='') {
    global $BID,$ID, $modules;

    $debug_query = 0;

    $grid_filter_query = '';
    $grid_mki = false;

    // proxy lekérdezés hosszú várakozás után.... nincsenek session változók!!!
    if (!isset($_SESSION['current_query_table']))
       $_SESSION['current_query_table'] = PROJECTTABLE;
    
    $cmd = sprintf("SELECT 1 FROM project_queries WHERE layer_cname=%s AND layer_type='base' AND project_table=%s",quote($cname),quote(PROJECTTABLE));
    $res = pg_query($BID,$cmd);

    // skip this for base layers!
    if (!pg_num_rows($res)) {

        if ($grid_mki = $modules->is_enabled('grid_view')) {
            $grid_layer = $modules->_include('grid_view','get_grid_layer');
            #{"kef_5":"dinpi_grid","utm_2.5":"dinpi_grid","utm_10":"dinpi_grid","utm_100":"dinpi_grid","original":"dinpi_points"}

            if (isset($_SESSION['display_grid_for_map'])) {

                // For grid coordinates disable original layer
                foreach ($grid_layer as $gc=>$gl) {

                    if ($_SESSION['display_grid_for_map'] == $gc and $layer!='' and $layer!=$gl) {
                        //log_action("DROP: {$_SESSION['display_grid_for_map']} == $gc & $layer != $gl",__FILE__,__LINE__);
                        return;
                    }
                }
            } else {
                $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                foreach ($grid_layer as $gc=>$gl) {

                    if ($grid_geom_col == $gc and $layer!='' and $layer!=$gl) {
                        //log_action("DROP: $grid_geom_col == $gc & $layer != $gl",__FILE__,__LINE__);
                        return;
                    }
                }
            }
            //$grid_filter_query = $dmodule->grid_view('filter_query',$modules[$grid_mki]['p']);
        }
    }

    $st_col = st_col($_SESSION['current_query_table'],'array');

    // query query
    $layer_type = "";
    $layer_name = "";
    $query = "";
    $rst = " IS NULL";

    if ($layer != '') $layer_name = "=".quote($layer);
    else $layer_name = ' IS NOT NULL';

    if ($cname != '') {
        $layer_cname = "=".quote($cname);
    }
    else $layer_cname = ' IS NOT NULL';

    if ($query_type == 'proxy') $layer_type = "=ANY('{base,query}')";
    elseif ($query_type == 'query') $layer_type = "='query'";
    else $layer_type = " LIKE '%'";

    /* Ezen a szinten a jogosultság kezelés  0-1-2-3 szintek; jelentése: non-login, login, projekt-member, admin
     * Az oauth bevezetése óta az 1-2 szint nem különbözik! Csak a 2-es létezik: 0,2,3
     * */
    $prst = prst();
    if ($query_type == 'proxy')
        $cmd = sprintf("SELECT layer_query,mapserv_layer_name AS layer_name,layer_type,geometry_type as geom_type,rst
                    FROM project_queries pq
                    LEFT JOIN project_mapserver_layers ml ON (pq.project_table=ml.project_table)
                    WHERE layer_query!='' AND pq.project_table='%s' AND layer_type%s AND mapserv_layer_name%s AND layer_cname%s AND rst<=%d AND enabled=TRUE AND main_table=%s %s
                    ORDER BY rst DESC",
                        PROJECTTABLE,$layer_type,$layer_name,$layer_cname,$prst,quote($_SESSION['current_query_table']),$grid_filter_query);
    else
        $cmd = sprintf("SELECT layer_query,'' AS layer_name,layer_type,'' as geom_type,rst
                    FROM project_queries
                    WHERE layer_query!='' AND project_table='%s' AND layer_type%s AND rst<=%d AND enabled=TRUE AND main_table=%s %s
                    ORDER BY rst DESC",
                        PROJECTTABLE,$layer_type,$prst,quote($_SESSION['current_query_table']),$grid_filter_query);

    $res = pg_query($BID,$cmd);
    //log_action($cmd,__FILE__,__LINE__);

    // we expect only one row!
    //while($row = pg_fetch_assoc($res)) {
    if (pg_num_rows($res)) {

        /*  Module actions over subqueries
            module include as query_builder.php */


        /* ok, it is already allowed!
         * if(pg_num_rows($res)>1) {
            log_action('common_pg_funcs - WARNING: more than one assembled query!!!');
            log_action('common_pg_funcs - SQL: '. $cmd);
            //point - line - polygon
        }*/
        $qq = array();
        $rst_counter = 0;
        $id_alias = PROJECTTABLE; # default id_alias for strange cases

        //
        if (!isset($st_col['GEOM_C'])) $geom_col = '';
        else {$geom_col = $st_col['GEOM_C'];

            $geom_noalias_col = $st_col['GEOM_C'];
        }

        if (!isset($st_col['SPECIES_C'])) $species_col = '';
        else $species_col = $st_col['SPECIES_C'];

        if (!isset($st_col['CITE_C'])) $observer_cols = '';
        else $observer_cols = $st_col['CITE_C'];

        /* If not necessary do not query it, bacause proxy sends lots of queriees, every data column send a query....
        $rst = 0;
        if (rst('acc')) {
            $rst = 1;
        } */

        $box = preg_split('/,/',$bbox);
        $ebox = '';
        if (count($box)==4) {
            $bcmd = sprintf("SELECT concat_ws(',',
                    st_X(st_transform(st_geomfromtext('POINT($box[0] $box[1])',3857),4326)), st_Y(st_transform(st_geomfromtext('POINT($box[0] $box[1])',3857),4326)),
                    st_X(st_transform(st_geomfromtext('POINT($box[2] $box[3])',3857),4326)), st_Y(st_transform(st_geomfromtext('POINT($box[2] $box[3])',3857),4326))) AS box");
            $bres = pg_query($BID,$bcmd);
            $brow = pg_fetch_assoc($bres);
            $ebox = $brow['box'];
        }

        // snesitive data handling
        // query only for the allowed users or groups
        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        while ($row = pg_fetch_assoc($res)) {
            //csak a legmagassabb rst értékű rétegeket olvassuk be
            //ez azért van így mert lehet, hogy több azonos színtű rétegünk van és mindegyiket végrehajtjuk egyszerre. Pl.: point, line, polygon
            //sql szinten nem tudom, hogyan lehetne ezt kezelni
            if ($rst_counter==0) {
                $rst_max = $row['rst'];
                $rst_counter++;
            }
            if ($row['rst'] < $rst_max) {
                break;
            }

            $_SESSION['ignore_joined_tables'] = 0;
            $id_col = "obm_id";

            $qtable = $_SESSION['current_query_table'];
            $qtable_alias = $qtable;

            if (preg_match('/%F%(.+)%F%/',$row['layer_query'],$m)) {

                $FROM = sprintf('FROM %s',$m[1]);

                if (preg_match('/([a-z0-9_.]+) (\w+)/i',$m[1],$mt)) {
                    $qtable = $mt[1];
                    $qtable_alias = $mt[2];
                    if (preg_match("/([a-z0-9_]+)\.obm_geometry/",$row['layer_query'],$mg)) {
                        $geom_alias = $mg[1];
                        $geom_col = $geom_alias.".obm_geometry";
                    }
                    /*if (preg_match("/([a-z_]+)\.%transform_geometry%/",$row['layer_query'],$mg)) {
                        $geom_alias = $mg[1];
                        $geom_col = $geom_alias.".obm_geometry";
                    }*/
                    if (preg_match("/([a-z0-9_]+)\.obm_id/",$row['layer_query'],$mg)) {
                        $id_alias = $mg[1];
                        $id_col = $id_alias.".obm_id";
                    }
                }
                if (preg_match_all('/%J%(.+)%J%/',$row['layer_query'],$mj)) {
                    foreach($mj[1] as $mje) {
                        if (isset($_SESSION['ignoreJoins']) and $_SESSION['ignoreJoins'])
                            $_SESSION['ignore_joined_tables']++;
                        $FROM .= " ".$mje;
                    }
                }
            }


            //geom column name should be passed here to module!!!
            /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($modules, 'module_name')));
            $mki = (count($module_name)) ? array_search($module_name[0], array_column($modules, 'module_name')) : false;
            if ($mki!==false) {
                require_once(getenv('OB_LIB_DIR').'default_modules.php');
                $dmodule = new defModules();
                $geom_col = $dmodule->snap_to_grid('geom_column',$modules[$mki]['p'],$geom_col);
            }*/

            $orderby = array();
            $limit = '';
            //if ($query_type == 'proxy')
            //    $limit = 16384;
            $query = $row['layer_query'];
            $layer_type = $row['layer_type'];
            $geom_type = preg_split('/,/',$row['geom_type']);

            /* Example usage of replace-variables in the query string:
             *
                CASE WHEN '%admin%'='admin' THEN 1 ELSE 0
                END AS super,

                CASE WHEN %uid%=ANY(owners) THEN 1 ELSE 0
                END AS mydata,

                CASE WHEN %grp%=ANY(groups) THEN 1 ELSE 0
                END AS groupdata

             * **********************************/
            // default patterns
            $uid = 0;
            $grp = 0;
            $admin = 'user';
            $morefilter = '';

            // access control settings
            // applied in both: base and query layers
            if (isset($_SESSION['Tid'])) $uid = $_SESSION['Tid'];
            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') $grp = $_SESSION['Tgroups'];
            $prst_class = array('public','login','project','admin')[$prst];

            /* Repleace variables for controlling MAPSERVER map styling
             * %rules% String: public,login,project,admin
             * %grp% Comma separated Numeric List of Logined user's group or 0
             * %uid% Numeric logined user's ID or 0
             *
             * */

            //a bit different: access class independently from the project rules: factor 0,1,2,3/public,login,project,admin
            $query = preg_replace('/%rules%/',"$prst_class",$query);
            //login groups
            $query = preg_replace('/%grp%/',"$grp",$query);
            //user's id
            $query = preg_replace('/%uid%/',"$uid",$query);

            //remove join_table marks
            $query = preg_replace('/%F%/','',$query);
            $query = preg_replace('/%J%/','',$query);

            //geometry_column
            //$query = preg_replace('/(\w\.)?%transform_geometry%/',$geom_col,$query); # csak egy karakteres aliasokkal működik így!!!!

            //grid geometry column
            //obm_geometry dupla megjelentését le kell védeni!

            if ($grid_mki) {
                if (isset($_SESSION['display_grid_for_map'])) {
                    $grid_geom_col = $_SESSION['display_grid_for_map'];
                    $geom_col = 'qgrids.'.'"'.$grid_geom_col.'"';
                    $query = preg_replace('/%grid_geometry%/','qgrids.'.'"'.$grid_geom_col.'"',$query);
                } else {
                    $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                    $geom_col = 'qgrids.'.'"'.$grid_geom_col.'"';
                    $query = preg_replace('/%grid_geometry%/','qgrids.'.'"'.$grid_geom_col.'"',$query);
                }
                $geom_noalias_col = $grid_geom_col;

                //$geom_type = array('POLYGON','MULTIPOLYGON','POINT','LINESRTING','MULTILINESTRING');
            } else {
                $query = preg_replace('/%grid_geometry%/',$id_alias.".".'"'.$geom_col.'"',$query);
            }

            if (isset($_SESSION['query_build_string'])) {
                //preserve $ quotes
                $q = preg_replace('/\$/','\\\$',$_SESSION['query_build_string']);
                $q = preg_replace('/qgrids\.[\w.]+/',$geom_col,$q);
                if (!is_array($geom_type))
                    $q = preg_replace('/@@geom_type@@/',"$geom_type",$q);
                else
                    $q = preg_replace('/@@geom_type@@/',"$geom_type[0]",$q); //passz, nem tudom, hogy így jó lesz-e???

                // query set parameters
                if (trim($q)!='')
                    $query = preg_replace('/%qstr%/',"AND $q",$query);
                else
                    $query = preg_replace('/%qstr%/',"",$query);

                $query = preg_replace('/%selected%/',",1 AS selected",$query);

            } else {
                $query = preg_replace('/%selected%/',",0 AS selected",$query);
                $query = preg_replace('/%qstr%/',"",$query);
            }

            // bbox of openlayers request square
            // obm_geometry && ST_MakeEnvelope (%bbox%,4326)
            //if ($ebox!='' and $geom_col != '') {
            //    //$query = preg_replace('/%envelope%/',"AND $geom_col && ST_MakeEnvelope({$ebox},4326)",$query);
            //    $query = preg_replace('/%envelope%/',"AND ST_Intersects($geom_col,ST_MakeEnvelope({$ebox},4326))",$query);
            //} else {
                //text query no bbox
            //    $query = preg_replace('/%envelope%/',"",$query);
            //}

            //
            $qtable = $_SESSION['current_query_table'];
            if ($query_type == 'proxy' and $geom_type != '' and $geom_col != '') {
                if (is_array($geom_type)) {
                    $geom_type = "'".implode("','",$geom_type)."'";
                    $query = preg_replace('/%geometry_type%/',"AND GeometryType($geom_col) IN ($geom_type)",$query);
                }
                else
                    $query = preg_replace('/%geometry_type%/',"AND GeometryType($geom_col)='$geom_type'",$query);
            }
            else
                $query = preg_replace('/%geometry_type%/',"",$query);

            // optional taxon join
            if (isset($_SESSION['taxon_join_filter']) and $_SESSION['taxon_join_filter'] and $species_col != '')
                $query = preg_replace('/%taxon_join%/', join_table('taxon', compact('species_col')),$query);
            else
                $query = preg_replace('/%taxon_join%/','',$query);
            // optional search join
            if (isset($_SESSION['search_join_filter']) and $_SESSION['search_join_filter']) {
                $query = preg_replace('/%search_join%/',join_table('search',compact('id_col','qtable')), $query);
            }
            else
                $query = preg_replace('/%search_join%/','',$query);

            // optional uploading join
            if (isset($_SESSION['uploading_join_filter']) and $_SESSION['uploading_join_filter'])
                $query = preg_replace('/%uploading_join%/', join_table('uploadings', compact('qtable_alias')), $query);
            else
                $query = preg_replace('/%uploading_join%/','',$query);

            // optional filename join
            //if ($modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
            if (get_option('show_attachment_filenames')) {
                $query = preg_replace('/%filename_join%/', join_table('filename',compact('qtable')), $query);
            }
            else {
                $query = preg_replace('/%filename_join%/','',$query);
            }

            // _rules JOIN and sensitive check if not super admin!!
            if ($st_col['USE_RULES']) {
                /*SENSITIVE DATA:
                 * DO NO QUERY sensitive data - only for the owners of them
                 * It is different than the acc check
                 */

                // rules join
                $query = preg_replace('/%rules_join%/', join_table('rules',compact('qtable', 'id_col')), $query);
                if (!has_access('master')) {
                    //No group access checking on SENSITIVE DATA, we DO NOT QUERY sensitive data - only for the owners of these
                    $query .= " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$grp] && read) ";

                    if ($query_type == 'proxy') {
                        $query .= " OR (sensitivity::varchar IN ('1','no-geom') AND ARRAY[$grp] && read) ";
                        $query .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND read && ARRAY[$grp]) ";
                        $query .= " OR sensitivity IS NULL "; // beengedjük a térképre azokat a sorokat aminek nincs rule sora
                        $query .= " OR sensitivity::varchar IN ('0','public') ";

                        $allowed_cols = dbcolist('columns',$_SESSION['current_query_table']);
                        if ($modules->is_enabled('allowed_columns')) {
                            $allowed_cols_module = 1;
                            $allowed_cols = $modules->_include('allowed_columns','return_columns',$allowed_cols,true);

                            $current_query_keys = array('obm_geometry');
                            $intersected_allowed_columns_count = count(array_intersect($current_query_keys,$allowed_cols));
                            $query_column_count = count($current_query_keys);
                            if ($intersected_allowed_columns_count != $query_column_count) {
                                # disable restricted data if the obm_geometry column not allowed by the allowed_columns module
                                $query .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND 1=0 AND (read && ARRAY[$grp])=FALSE) ";
                            } else {
                                ## show data on the map if obm_geometry allowed by the allowed_columns module 
                                # This only works if the read is an array! If read is null it not works!!!
                                $query .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND (read && ARRAY[$grp])=FALSE) ";
                            }
                        }

                    } else {

                        $query .= " OR sensitivity IS NULL OR sensitivity::varchar IN ('0','1','2','public','no-geom','restricted','sensitive') ";
                    
                    }

                    $query .= ")";
                }
            } else {
                $query = preg_replace('/%rules_join%/','',$query);
            }

            // grid join
            $query = preg_replace("/%grid_join%/",join_table('grid', compact('id_col','qtable')), $query);

            /*  subfiltering
                applied in query layers
                point - line - polygon */
            if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']!='clear') {
                $res4 = pg_query($ID,sprintf("SELECT EXISTS (
                   SELECT 1
                   FROM   information_schema.tables
                   WHERE  table_schema = 'temporary_tables'
                   AND    table_name = 'temp_filter_%s_%s');",PROJECTTABLE,session_id()));
                $row = pg_fetch_assoc($res4);
                if ($row['exists']=='t') {

                    //$resm = pg_query($ID,sprintf("SELECT rowid FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
                    //if (pg_num_rows($resm)) {
                    if (isset($_SESSION['morefilter_query']))
                        $query = preg_replace('/%morefilter%/',"{$_SESSION['morefilter_query']}",$query);
                    else
                        $query = preg_replace('/%morefilter%/',"",$query);

                    //} else {
                    //    $query = preg_replace('/%morefilter%/',"",$query);
                    //}
                    //} else {
                } else {
                    $query = preg_replace('/%morefilter%/',"",$query);
                }
            } else {
                    $query = preg_replace('/%morefilter%/',"",$query);
            }

            // order by module call
            //          not implemented
            // limit module call
            //          not implemented
            $orderby = implode(' ',$orderby);
            if (trim($orderby != '')) {
                if (preg_match('/order by/i',$query)) {
                    $query = preg_replace('/ORDER BY(\s)/i',"$orderby",$query);
                } else {
                    $query .= " ORDER BY $orderby";
                }
            }
            if ($limit != '') {
                $query .= " LIMIT $limit";
            }

            if ($ebox!='' and $geom_noalias_col != '') {
                if (preg_match('/%envelope%/',$query)) {
                    $query = preg_replace('/%envelope%/',"",$query);
                    $query = "WITH \"$geom_noalias_col\" (geom) AS (SELECT ST_MakeEnvelope($ebox,4326)) ".$query;
                }
            }

            //text query no bbox
            $query = preg_replace('/%envelope%/',"",$query);

            $query = preg_replace('/WHERE\s+AND/i','WHERE',$query);
            $query = preg_replace('/WHERE\s*$/i','',$query);

            if ($debug_query)
                log_action($query,__FILE__,__LINE__);

            $qq[] = $query;
        }
        return $qq;

    } else {

        if (pg_last_error($BID))
            log_action(pg_last_error($BID),__FILE__,__LINE__);

        //log_action('WARNING: No query for layer in project_queries: '.$layer,__FILE__,__LINE__);
        //log_action('SQL: '. $cmd,__FILE__,__LINE__);
        return;
    }
}
/*
 *
 * */
function update_temp_table_index ($name,$prefix='temp',$interconnect=false) {
    global $ID,$GID;

    //update temp_index
    $res = pg_query($ID,sprintf("SELECT 1 FROM system.temp_index WHERE table_name='%s_%s'",$prefix,$name));
    if (!pg_num_rows($res)) {
        if ($interconnect)
            $interconnect = 't';
        else
            $interconnect = 'f';
        pg_query($ID,sprintf("INSERT INTO system.temp_index (table_name,interconnect) VALUES ('%s_%s','%s')",$prefix,$name,$interconnect));
    } else {
        pg_query($ID,sprintf("UPDATE system.temp_index SET datum=now() WHERE table_name='%s_%s'",$prefix,$name));
    }
    // clear unused (older than 3 days) temp tables - in general -
    // the temp tables dropped by automatically using drop_temporay_table trigger function:
    // BEGIN
    //    IF (TG_OP = 'DELETE') THEN
    //        execute format('DROP TABLE IF EXISTS temporary_tables.%I',OLD.table_name);
    //        RETURN OLD;
    //    END IF;
    // END
    //  temp_query_dinpi_2sa15ucf3nvbhcd0105o93lo56
    //  temp_roll_list_dinpi_2sa15ucf3nvbhcd0105o93lo56
    //  temp_dinpi_2sa15ucf3nvbhcd0105o93lo56
    //  upload_dinpi_njso7rqgmm23qha13vo29vba2j
    //pg_query($GID,"DELETE FROM system.temp_index WHERE datum < NOW() - INTERVAL '3 days' AND table_name LIKE '$prefix%_".PROJECTTABLE."_%'");
    // DO NOT DELETE SAVED IMPORTS!!!
    pg_query($GID,sprintf("DELETE FROM system.temp_index WHERE datum < NOW() - INTERVAL '3 days' AND table_name LIKE '%s%s_%s'",$prefix,'%',PROJECTTABLE));
}
/* Get named XML node
 *
 * */
function &getXMLnode($object, $param) {
        foreach($object as $key => $value) {
            if(isset($object->$key->$param)) {
                return $object->$key->$param;
            }
            if(is_object($object->$key)&&!empty($object->$key)) {
                $new_obj = $object->$key;
                $ret = getXMLnode($new_obj, $param);
            }
        }
        if($ret) return (string) $ret;
        return false;
}

//log_action($_SESSION['Tgroups']);
/* reinitialize session after long sleep
 *
 * */
function init_session() {
    require_once('prepare_vars.php');

    //session változók beállítása
    st_col('default_table');

    return true;
}

/* multidim array search
 *
 *
 * */
function searchForId($name,$id, $array) {
   foreach ($array as $key => $val) {
       if ($val[$name] == $id) {
           return $key;
       }
   }
   return null;
}
/* encode a passed string with the passed key string
 *
 *
 * */
function encode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}
/* decode a passed string with the passed key string
 *
 *
 * */
function decode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}
/* --> queries.php
 * save wfs/wms result gml into files
 * NINCS már használva ez a funkció!!!
 * */
/*function SaveQueryTemp($query) {
    //global $_SESSION;
    if(!isset($_SESSION['Tth'])) return;
    $ses = session_id();
    $query = preg_replace('/ /','%20',$query);
    file_put_contents("/tmp/query_$ses.txt", $query);

    # !!! A resp válasz lementése itt dupla lekérdezést eredményez, mert a
    # result builderben is ott van az openlayers hívás miatt.... !!!
    $ses = session_id();
    $password = decode($_SESSION['Tth'],session_id());
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("{$_SESSION['Tname']}:$password")
    )
    ));
    $content = file_get_contents($query, false, $context);
    file_put_contents("/tmp/conts_$ses.xml", $content);
}*/
/* Ajax-call function
 * its called from functions.js
 * it saves the draw selecions as a geometry for further actions */
function AutoSaveGeometry($wkt) {
    global $GID;

    if (count($wkt) > 1) {
        // polygons can be extracted and creates a MULTIPOLYGON .... GEOMETRYCOLLECTION ....
        // but now, only the last one processed
        $one_wkt = end($wkt);

    } else {
        $one_wkt = $wkt[0];
    }

    $srid = '4326'; // default on the maps..
    //$cmd = sprintf("SELECT ST_GeomFromText('POLYGON(($query))',$srid)");
    if (isset($_SESSION['Tid'])) {
        pg_query($GID,'SET search_path TO system,public');
        $cmd = sprintf("INSERT INTO system.query_buff (\"user_id\",\"geometry\",\"datetime\",\"table\") VALUES ('%d',ST_GeomFromText(%s,4326),now(),'%s') RETURNING id",
            $_SESSION['Tid'],
            quote($one_wkt),
            PROJECTTABLE);
        $res = pg_query($GID,$cmd);
        if (!pg_num_rows($res)) {
            log_action($cmd,__FILE__,__LINE__);
            return 0;
        } else {
            $row = pg_fetch_assoc($res);
            $_SESSION['selection_geometry_id'] = $row['id'];
            $_SESSION['selection_geometry'] = $wkt;
            $_SESSION['selection_geometry_type'] = 'draw';
            return 1;
        }
    } else {
        $_SESSION['selection_geometry'] = $wkt;
        return 2;
    }
}
/* 
 * Save query results as geojson into database
 * */
function ShareQuery() {
    global $ID,$BID,$GID,$_SESSION;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    if (!isset($_SESSION['Tid'])) common_message('error','Should be logged in!');

    // save query or save results
    $ses = session_id();
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 16 );
    $link = '';
    $label = '';

    // query temporary table columns
    $cmd = sprintf("SELECT attrelid::regclass, attnum, attname 
                    FROM pg_attribute 
                    WHERE attrelid = 'temporary_tables.temp_query_%s_%s'::regclass AND attnum > 0 AND NOT attisdropped 
                    ORDER BY attnum",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    $tempcols=array();
    while($row=pg_fetch_assoc($res)) {
        $tempcols[] = $row['attname'];
    }
    $tc = '"'.implode('","',$tempcols).'"';


    if (!isset($_SESSION['st_col']['GEOM_C']) or $_SESSION['st_col']['GEOM_C']=='') {
        //Maybe there is no geometry column of the non-main table
        //So, we need to use the main-table's geom col
        $st_col = st_col(PROJECTTABLE,'array');
        $GEOM_C = $st_col['GEOM_C'];
    
    } else
        $GEOM_C = $_SESSION['st_col']['GEOM_C'];

    // Create GeoJSON from the query results
    $cmd = sprintf("SELECT row_to_json(fc) as geojson
            FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
            FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.%s)::json As geometry, row_to_json((SELECT l FROM (SELECT %s) As l)) As properties
            FROM temporary_tables.temp_query_%s_%s As lg ) As f )  As fc",
                $GEOM_C,$tc,PROJECTTABLE,session_id());

    $res = pg_query($ID,$cmd);

    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $result = $row['geojson'];

        // Get spatial extent of the query
        $geometry=NULL;
        if(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='draw') {
            $cmd = "SELECT geometry FROM system.query_buff WHERE id={$_SESSION['selection_geometry_id']}";
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        } elseif(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='customPolygon') {
            // hogyan????
            $geometry = '';

        } elseif(isset($_SESSION['selection_geometry_id']) and isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='sharedPolygon') {
            $cmd = "SELECT geometry FROM system.shared_polygons WHERE id={$_SESSION['selection_geometry_id']}";
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        } else {
            // text query
            $cmd = sprintf("SELECT ST_Extent(obm_geometry)::Geometry AS geometry FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $r = pg_fetch_assoc($res);
                $geometry = $r['geometry'];
            }
        }
        if ($geometry=='') $geometry = 'NULL';
        else $geometry = quote($geometry);

        #$cmd = sprintf("INSERT INTO public.project_shares (user_id,project,query,result,datetime,share_id,label,spatial_selection)

        $cmd = sprintf("INSERT INTO public.project_repository (user_id,project,query,result,datetime,sessionid,name,selection)
                        VALUES ('%s','%s',%s,%s,now(),'%s',%s,%s) returning id",
                $_SESSION['Tid'],PROJECTTABLE,quote(json_encode($_SESSION['query_build_string'])),quote($result),$random_string,quote($label),$geometry);

        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            $link = t('str_sharing_link')."<br><br>";
            $link .= sprintf('&nbsp; &nbsp; <a href="%4$s://%1$s/?Share=%2$s@%3$s" class="copytoclipboard" id="cp%3$s" target="_blank">%4$s://%1$s/?Share=%2$s@%3$s</a>',
                URL,$r['id'],$random_string,$protocol);
        } else {
                $errorID = uniqid();
                log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                return common_message("failed",t('str_saving_error').". ErrorID#$errorID#");
        }

        return common_message("ok",$link);

    } else {
        $errorID = uniqid();
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
        return common_message("failed",t('str_saving_error').". ErrorID#$errorID#");
    }
}

function BookmarkQuery($label='') {
    global $ID,$BID,$GID,$_SESSION;

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (!isset($_SESSION['Tid'])) common_message('error','Should be logged in!');

    // Save spatial selection to shared polygons
    if (isset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='draw' ) {
        pg_query($GID,'BEGIN');
        pg_query($GID,'SET search_path TO system,public');
        $cmd = sprintf("DELETE FROM system.query_buff WHERE id=%s AND user_id=%d RETURNING geometry",
                        $_SESSION['selection_geometry_id'],$_SESSION['Tid']);
        $res = pg_query($GID,$cmd);
        $row = pg_fetch_assoc($res);

        $cmd = sprintf("INSERT INTO system.shared_polygons (project_table, geometry, name, user_id, access) 
                        VALUES ('%s','%s',%s,%d,'private') RETURNING id",
                        PROJECTTABLE,$row['geometry'],quote($label),$_SESSION['Tid']);
        $res = pg_query($GID,$cmd);

        if (pg_affected_rows($res)) {
            $r = pg_fetch_assoc($res);
        
            $cmd = sprintf("INSERT INTO system.polygon_users (user_id, polygon_id, select_view)
                VALUES(%d, %d, '%s')",
                $_SESSION['Tid'],$r['id'],'only-select');
            $res = pg_query($GID,$cmd);

            if (pg_affected_rows($res)) {
                pg_query($GID,'COMMIT');
            } else {
                $errorID = uniqid();
                log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
                pg_query($GID,'ROLLBACK');

                return common_message("failed",t('str_saving_error').". ErrorID#$errorID#");
            }
        
        } else {
            $errorID = uniqid();
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            pg_query($GID,'ROLLBACK');

            return common_message("failed",t('str_saving_error').". ErrorID#$errorID#");
        }
    }
    // Save text selection
    $query_string = $_SESSION['query_build_string'];
    $cmd = sprintf("INSERT INTO public.bookmarks (query_string,user_id,key,project,project_table)
                VALUES (%s,%d,%s,'".PROJECTTABLE."','".$_SESSION['current_query_table']."') returning id",
                quote($query_string),$_SESSION['Tid'],quote($label));

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);

        $link = t(' str_bookmarking_link'). "<br><br>";
        $link .= sprintf('<a href="%1$s://%2$s/?bookmark=%3$d@%4$s&qtable=%5$s" target="_blank">%1$s://%2$s/?bookmark=%3$d@%4$s&qtable=%5$s</a>',
            $protocol,URL,$r['id'],$label,preg_replace('/^'.PROJECTTABLE.'_/','',$_SESSION['current_query_table']));
        return common_message("ok",$link);

    } else {
        $errorID = uniqid();
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
        return common_message("failed",t('str_saving_error').". ErrorID#$errorID#");
    }
}
/* get user comments from evaluations table
 *
 *
 * */
function comments($id,$table='',$db) {
    global $ID, $BID;
    if ($id=='') return;

    if ($db == 'data') {
        $conn = $ID;
        if ($table=='')
            $table = PROJECTTABLE;
        $schema = 'system';
    }
    elseif ($db == 'upload') {
        $conn = $ID;
        $table = "uploadings";
        $schema = 'system';
    }
    else {
        $conn = $BID;
        $table = $table;
        $schema = 'public';
    }
    $m = array();
    if (preg_match('/([0-9]+)_([0-9]+)/',$id,$m)) $id = $m[1];

    $cmd = "SELECT comments,user_name,to_char(datum,'YYYY-MM-DD HH24:MI') as datum,valuation FROM $schema.evaluations WHERE \"table\"='$table' and row='$id'";
    $res = pg_query($conn,$cmd);
    $e = "<ul style='padding:0;margin:0;list-style-type:none'>";
    if (pg_num_rows($res)) {
        while ($row=pg_fetch_assoc($res)){
            $name = preg_replace("/^NULL$/","-",$row['user_name']);
            if ($row['valuation']>0) $color = "#B2DC53";
            elseif ($row['valuation']<0) $color = "#CA3C3C";
            else $color = "#42B8DD";
            $e .= "<li style='border-left:10px solid $color;line-height:1.5em;padding:3px;color:#222222;text-align:justify'>".$row['comments'];
            $e .= ' <span style="color:#333333;font-size:0.7em">['.$name.' '.$row['datum'].']</span>';
            $e .= '</li>';
        }
    }
    $e .= "</ul>";
    return $e;
}
/* track data view
 * */
function track_data ($id) {
    global $BID;
    $data_id = preg_replace('/[^0-9]/','',$id);

    $cmd = sprintf("SELECT counter FROM track_data_views WHERE project_table='%s' AND id=%s",PROJECTTABLE,quote($data_id));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $cmd = sprintf("UPDATE track_data_views SET counter=counter+1 WHERE project_table='%s' AND id=%s AND session_id!='%s'",PROJECTTABLE,quote($data_id),session_id());
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            if (pg_last_error($BID)) {
                log_action(pg_last_error($BID),__FILE__,__LINE__);
                return "not";
            }
        } else
            return $row['counter']+1;
    } else {
        $cmd = sprintf("INSERT INTO track_data_views (project_table,id,counter,session_id) VALUES('%s',%s,1,'%s')",PROJECTTABLE,quote($data_id),session_id());
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            log_action(pg_last_error($BID),__FILE__,__LINE__);
            return "error";
        } else
            return '1';
    }
    return "error";
}
/* track download
 * */
function track_download ($id,$table='') {
    
    global $BID;

    if (!is_array($id)) {
        $data_id = array($id);
    } else {
        $data_id = $id;
    }
    $rows = array();
    $session_id = session_id();

    if ($table == '') {

        $table = isset($_SESSION['current_query_table']) ? $_SESSION['current_query_table'] : PROJECTTABLE;
    }
    
    /*
    foreach ($data_id as $p) {

        $scmd = sprintf("UPDATE track_downloads SET counter=counter+1 WHERE project_table='%s' AND id=%s AND session_id!='%s'",PROJECTTABLE,quote($p),session_id());
        $res = pg_query($BID,$scmd);
        // Számláló frissítve, egy sessionben csak 1x frissítek!
        // else
        if (!pg_affected_rows($res)) {
            // nem volt frissítés mert már volt ebben a sessionben?
            $scmd = sprintf("SELECT counter FROM track_downloads WHERE project_table='%s' AND id=%s",PROJECTTABLE,quote($p));
            $res = pg_query($BID,$scmd);
            if(!pg_num_rows($res))
                $rows[] = sprintf("%s\t%d\t1\t%s",PROJECTTABLE,$p,session_id());
        }
    }
    //insert executes: all at once
    if (count($rows)) pg_copy_from($BID, "track_downloads", $rows, "\t","\\NULL");
    */
    foreach ($data_id as $p) {
        $rows[] = sprintf("('%s',%d,%d,'%s')",$table,$p,1,$session_id);
    }

    $cmd = sprintf('INSERT INTO track_downloads (project_table, id, counter, session_id)
            VALUES %s
                ON CONFLICT (id,project_table) DO UPDATE 
                    SET "counter" = track_downloads."counter"+1, 
                        session_id = \'%s\' WHERE track_downloads.project_table=%s AND track_downloads.id=%d', 
            implode(',',$rows),$session_id,quote($table),$p);

    $res = pg_query($BID,$cmd);

    return;
}
function metrics ($id,$t) {
    global $BID;
    $cmd = sprintf("SELECT counter FROM track_%s WHERE project_table='%s' AND id=%d",$t,PROJECTTABLE,$id);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return($row['counter']);
    }
}
function cite_metrics ($id,$target,$table) {
    global $BID;
    if ($target == 'citations')
        $cmd = sprintf("SELECT UNNEST(source) AS source FROM track_%s WHERE project_table='%s' AND data_table=%s AND id=%d",
            'citations',PROJECTTABLE,quote($table),$id);
    else
        return json_encode(array());

    $res = pg_query($BID,$cmd);
    $a = array();
    while ($row = pg_fetch_assoc($res)) {
        if (preg_match('/^http/',$row['source']))
            $row['source'] = "<a href='".$row['source']."'>".$row['source']."</a>";
        $a[] = $row['source'];
    }
    return(json_encode($a));
}
// visitor statistics
function track_visitors ($page) {
    global $BID;
    $bot = 0;
    #if (preg_match('/robot/',$_SERVER['HTTP_USER_AGENT']))
    #    $bot = 1;
    #elseif (preg_match('/bot\.html/',$_SERVER['HTTP_USER_AGENT']))
    #    $bot = 1;

    if (!$bot) {
        $request = $_REQUEST;
        if (isset($request['loginpass'])) $request['loginpass'] = '';
        $cmd = sprintf("INSERT INTO project_visitors (date,ip_addr,project_table,page,action) VALUES(now(),'%s','%s','%s',%s)",$_SERVER['REMOTE_ADDR'],PROJECTTABLE,$page,quote(json_encode($request)));
        pg_query($BID,$cmd);
    }
}

/* Data validation score calculation
 * Return the validation score for a data id
 *
 * */
function validation ($id,$table=PROJECTTABLE) {
    global $ID, $BID;
    if ($id=='') return;
    $m = array();
    if (preg_match('/([0-9]+)_([0-9]+)/',$id,$m)) $id = $m[1];

    $valid = obm_cache('get',"mean_validation".$id);
    if ($valid !== false )
        return $valid;

    $id = quote($id);
    $mean = 0;

    // project validation
    $header_val = project_validation(PROJECTTABLE);

    // data validation
    $cmd = "SELECT obm_validation as v,obm_uploading_id FROM ".$table." WHERE obm_id=$id";
    $result = pg_query($ID,$cmd);
    $row=pg_fetch_assoc($result);
    $uploading_id = $row['obm_uploading_id'];
    $cal_val = $row['v'];

    // uploading validation
    $uploading_id = quote($row['obm_uploading_id']);
    //$cmd = "SELECT array_avg(validation) as v,uploader_id FROM system.uploadings WHERE id=$uploading_id";
    $cmd = "SELECT validation as v,uploader_id FROM system.uploadings WHERE id=$uploading_id";
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) {
        $row=pg_fetch_assoc($result);
        $uploader_id = $row['uploader_id'];
        $uploading_val = $row['v'];
    } else {
        // There is no uploading metadata
        $uploader_id = 0; 
        $uploading_val = 0; 
    }    
    
    // user's validation
    $userdata = new userdata($uploader_id,'hash');
    $user_val = user_validation($userdata->get_userid());

    if($header_val!=0 and $header_val!='') $mean++;
    if($cal_val!=0 and $cal_val!='') $mean++;
    if($uploading_val!=0 and $uploading_val!='') $mean++;
    if($user_val!=0 and $user_val!='') $mean++;

    if($mean)
        $valid = sprintf("%.2f",($header_val+$cal_val+$user_val+$uploading_val)/$mean);
    else
        $valid = '';

    obm_cache('set',"mean_validation".$id,$valid,30);

    return $valid;
}
// user's validation
function user_validation ($user_id) {
    global $BID;

    $user_id = preg_replace('/[^0-9]/','',$user_id);
    if(!$user_id) return;

    $cmd = sprintf("SELECT validation as v FROM users WHERE id=%d",$user_id);
    $result = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($result);
    $v = $row['v'];

    return $v;
}
// project validation
function project_validation ($project_table) {
    global $BID;
    $cmd = sprintf('SELECT validation as v FROM projects WHERE project_table=%s',quote($project_table));
    $result = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($result);
    $v = $row['v'];
    return $v;
}
/* general column names from header_names
 * Össze lehetne vonni az other_col függvénnyel is és akkor a hstore
 * típusok tömbbként lennének az st_col-ban tárolva
 * */
function st_col ($table,$output='session',$usecache=1) {
    global $BID,$ID;

    $ACC_LEVEL = ACC_LEVEL;

    if ($table == 'default_table' or $table == '') {
        if (defined('DEFAULT_TABLE'))
            $table = DEFAULT_TABLE;
        else
            $table = PROJECTTABLE;

    }
    
    if ($usecache)
        $st_col = obm_cache('get',"st_col.$table",'','');
    else
        $st_col = false;

    if ($st_col===FALSE) {
        $schema = 'public';     // Default is public!!!
        $sp = preg_split('/\./',$table);
        if (count($sp)==2) {
            $table = $sp[1];
            $schema = $sp[0];
        }
        $cmd = sprintf("SELECT f_geom_column, f_species_column, f_quantity_column, f_id_column,
            coalesce(f_x_column,'') as f_x_column,coalesce(f_y_column,'') as f_y_column,
            f_srid, f_order_columns, f_use_rules, f_utmzone_column,
            ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,
            ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
            ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
            ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
        FROM header_names
        WHERE f_project_schema=%s AND f_project_name='%s' AND f_project_table=%s",quote($schema),PROJECTTABLE,quote($table));
        $result = pg_query($BID,$cmd);
        if (pg_last_error($BID)) {
            log_action(pg_last_error($BID),__FILE__,__LINE__);
            return false;
        } else if (!pg_num_rows($result)) {
            return array('USE_RULES' => false, 'GEOM_C' => '', 'SPECIES_C' => '', 'NUM_IND_C' => '', 'ID_C' => '',  'X_C' => '', 'Y_C' => '',
                'UTMZONE_C' => '', 'SRID_C' => '', 'ORDER' => '', 'CITE_C' => '', 'DATE_C' => '', 'ALTERN_C' => '', 'FILE_ID_C' => '');
        }
        $row = pg_fetch_assoc($result);

        $st_col = array();
        $st_col['GEOM_C'] = trim($row['f_geom_column']);
        $st_col['SPECIES_C'] = trim($row['f_species_column']);
        $st_col['SPECIES_C_SCI'] = trim($row['f_species_column']);
        $st_col['NUM_IND_C'] = trim($row['f_quantity_column']);
        $st_col['ID_C'] = trim($row['f_id_column']); //should be "obm_id" for the normal tables, but can be something else for helper tables
        $st_col['X_C'] = trim($row['f_x_column']);
        $st_col['Y_C'] = trim($row['f_y_column']);
        $st_col['UTMZONE_C'] = trim($row['f_utmzone_column']);
        $st_col['SRID_C'] = trim($row['f_srid']);
        $st_col['ORDER'] = trim($row['f_order_columns']);
        $st_col['CITE_C'] = preg_split("/,/",($row['f_cite_person_columns']));
        $st_col['DATE_C'] = preg_split("/,/",$row['f_date_columns']);
        $st_col['RULES_WORKS'] = false;
        $st_col['ALTERN_C'] = array_map(function($col) {
          return preg_split('/@/',$col)[0];
        }, preg_split("/,/",$row['f_alter_speciesname_columns']));
        
        $national_taxon_names = array_filter( preg_split("/,/",$row['f_alter_speciesname_columns']), function($col) {
          return isset(preg_split('/@/',$col)[1]);
        });
        $st_col['NATIONAL_C'] = [];
        foreach ($national_taxon_names as $col) {
          $m = preg_split('/@/',$col);
          $st_col['NATIONAL_C'][$m[1]] = $m[0];
        }
        
        $use_rules = $row['f_use_rules'];
        $st_col['USE_RULES'] = false;

        /* applying personal species column preferences */
        if (isset($_SESSION['Trole_id'])) {
          $preferred_species_column = get_option('taxon_lang', $_SESSION['Trole_id']);
          if (in_array($preferred_species_column, $st_col['ALTERN_C']) and $preferred_species_column!==false) {
            $st_col['SPECIES_C'] = $preferred_species_column;
            $st_col['ALTERN_C'] = array_unique(array_values(array_filter($st_col['ALTERN_C'], function($c) use ($preferred_species_column) {
              return $c !== $preferred_species_column;
            })));
          }
        }
        
        /* Visszatartott adatsort jelentő oszlop a restriction és a secret modulok használják
         * It is depend on the extince on the rules table and the global ACC_LEVEL
         * */

        $cmd = "SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '".PROJECTTABLE."_rules') AS exists";
        $result = pg_query($ID,$cmd);
        //pg_close($nID);
        $rules_table = pg_fetch_assoc($result);

        if ($rules_table['exists']=='t') {
            if ("$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group') {
                if ($use_rules=='1' or $use_rules=='true' or $use_rules===true or $use_rules=='t') {
                    $st_col['USE_RULES'] = true;
                    $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s'",$table);
                    $res2 = pg_query($ID,$cmd);
                    if (pg_num_rows($res2)) {
                        $row2 = pg_fetch_assoc($res2);
                        if ($row2['tgenabled'] == 'O') {
                            $st_col['RULES_WORKS'] = true;
                        }
                    }
                }
            }
        } else {
            if ("$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'group')
                log_action("The global access level is `group` but ".PROJECTTABLE."_rules table does not exists!",__FILE__,__LINE__);
        }

        //itt gyakorlatilag nem az oszlop nevét használom, hanem a tényt hogy van attachment használat - ez modulként kellene nem így
        $st_col['FILE_ID_C'] = trim($row['f_file_id_columns']);

        obm_cache('set',"st_col.$table",$st_col,300);
    }

    if ($output == 'array')
        return $st_col;
    else {
        stcol_to_session($st_col);
    }
}
/* Return taxon name columns
 * language_label=>column_name
 * NOT USED EVER!
 *
 * */
function lang_col($table,$schema='public') {
    global $BID;

    $columns = obm_cache('get',"lang_col",'','',FALSE);
    if ($columns) {
        return $columns;
    }

    $cmd = sprintf("SELECT (each(f_alter_speciesname_columns)).key as key, (each(f_alter_speciesname_columns)).value as value 
        FROM header_names 
        WHERE f_project_name='%s' AND f_project_schema=%s AND f_project_table=%s",PROJECTTABLE,quote($schema),quote($table));
    $result = pg_query($BID,$cmd);
    $COLUMNS = array();
    while($row = pg_fetch_assoc($result)) {
        $COLUMNS[$row['key']]=$row['value'];
    }

    obm_cache('set',"lang_col",$COLUMNS,300,FALSE);
    return $COLUMNS;
}
/* st_col elements as _SESSION array
 *
 * */
function stcol_to_session($st_col) {

    global $BID;

    $_SESSION['st_col'] = array();

    foreach ($st_col as $k=>$v) {
        $_SESSION['st_col'][$k] = $v;
    }

    /* tábla tábla összekapcsolások
     * amikor automatikusan akarunk ilyet csinálni, nem modulból statikusan akkor lehet használni:
     *
     * */
    /*
    $_SESSION['ic_col'] = array();
     * $cmd = sprintf("SELECT project_table,inter_table,pt_id_col,it_id_col FROM project_interconnects WHERE project_table='%s'",PROJECTTABLE);
    $result = pg_query($BID,$cmd);

    while($row = pg_fetch_assoc($result)) {
        $_SESSION['ic_col'][$row['inter_table']] = $row['it_id_col'];
    }*/
}
// random color
// Creates a random color string
// return a 6byte long hex string like AABBCC
function rand_color() {
    $rc = '';
    for ($i=0;$i<3;$i++) {
        $rnd = dechex(rand(0,255));
        if (strlen($rnd) == 1 ) $rnd = "0$rnd";
        $rc .= $rnd;
    }
    return $rc;
}
/* invitation code processing
 * adduser add user
 *
 * */
function inventer ($code) {
    global $BID;
    require_once(getenv('OB_LIB_DIR').'languages.php');

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    # TRANSLATIONS!!
    $err = '';
    $post_code=preg_replace("/[^a-zA-Z0-9]/","",$code);
    if (strlen($post_code)!=32) $post_code='';

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    $random_pw = substr( str_shuffle( $chars ), 0, 16 );

    if ($post_code!='') {
        $post_code = quote($post_code);
        $cmd = "SELECT u.id as uid,i.id, i.user_id as inviter_id, project_table,mail,email FROM invites i LEFT JOIN users u ON mail=email WHERE code=$post_code AND created + interval '60 days' > now()";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            $inv_id = $r['id'];
            $inv_pt = $r['project_table'];
            $inv_m = $r['mail'];
            $inviter_id = $r['inviter_id'];
            if ($r['uid']!='') {
                //already registered user
                $user_id = $r['uid'];
                $registered_user = 1;
            }
            else {
                $user_id = '';
                $registered_user = 0;
            }
            //$inv_gr = $r['group'];
        } else {
            #pg_last_error($BID);
            log_action("Expired or invalid invitation: ".$cmd,__FILE__,__LINE__);
            return "Expired or invalid invitation code";
        }
        if ($user_id=='') {

            if (!$hash = gen_password_hash($random_pw)) {
                return false;
            }

            pg_query($BID,'BEGIN');
            $cmd = sprintf('INSERT INTO "users" ("user",password,status,username,institute,address,email,inviter,pwstate)
                                SELECT \'\',%s,1,name,\'\',\'\',mail,user_id,true
                                FROM invites
                                WHERE id=\'%d\'
                            RETURNING id',quote($hash),$inv_id);
            $res = pg_query($BID,$cmd);
            if (!pg_affected_rows($res)){
                if (preg_match('/duplicate key value/',pg_last_error($BID))) $err = "The user already exists.";
                else $err = "Registration error.";
                log_action($err,__FILE__,__LINE__);
                pg_query($BID,'ROLLBACK');
                return $err;
            } else {
                $row = pg_fetch_assoc($res);
                $user_id = $row['id'];
            }
        }

        $cmd = "INSERT INTO project_users (project_table,user_id) VALUES ('$inv_pt',$user_id)";
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            if (preg_match('/duplicate key value/',pg_last_error($BID))) $err = "This email already registered in this project.";
            else $err = "Registration error.";
            log_action($err,__FILE__,__LINE__);
            pg_query($BID,'ROLLBACK');
            return $err;
        }

        # inviter's info
        $inviter = new User($inviter_id);
        #new user's info
        $invited = new User($user_id);
        
        
        //drop all invitation
        //$cmd = "DELETE FROM invites WHERE id='$inv_id'";
        $cmd = sprintf("DELETE FROM invites WHERE mail='%s' AND project_table='%s' RETURNING lang",$invited->email,PROJECTTABLE);
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res)) {
            $r = pg_fetch_assoc($res);
            pg_query($BID,"COMMIT");
            $_SESSION['agree_new_terms_lang'] = $r['lang'];
            //mail to the inviter
            if ($inviter->email != '') {
                $M = new Messenger();

                $M->send_system_message($inviter, 'invitation_accomplished', ['inviter' => $inviter->name, 'invited' => $invited->name], true);
                $M->send_system_message($invited, 'welcome_to', [ 'user_profile_id' => $invited->hash ], true);
                
            }
            //processing return code?
            //...
            if ($registered_user==0) {
                $_SESSION['register_upw']=$random_pw; //generated password
                $_SESSION['register_um']=$inv_m; //invited email
                return true;
            } else {
                // already registered user
                return true;
            }
        } else {
            $_SESSION['le'] = "sql error";
            pg_query($BID,'ROLLBACK');
            return "Registration error.";
        }
    } else {
        return "Registration error";
    }
}

/* Update password hash and delete reactivate string
 * it called from prepare_vars.php when isset GET['activate']
 * users clicked on reactivate link
 * */
function reactivate($addr,$code) {
    global $BID;
    $post_code=preg_replace("/[^a-zA-Z0-9]/","",$code);

    if ($post_code!='') {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $random_pw = substr( str_shuffle( $chars ), 0, 16 );

        if (function_exists ( 'mcrypt_create_iv' ))
            $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        elseif (function_exists( 'random_bytes' ))
            $salt = random_bytes(22);
        else {
            log_action("ERROR!! No random salt function available to create password!",__FILE__,__LINE__);
            return "Sorry, there is a system problem.";
        }


        $options = [
                'cost' => 10,
                'salt' => $salt,
        ];
        $hash = password_hash($random_pw, PASSWORD_BCRYPT, $options);

        $cmd = sprintf('UPDATE users SET password=%s,reactivate=NULL,pwstate=TRUE WHERE LOWER(email)=%s AND reactivate=%s RETURNING "email"',quote($hash),quote(strtolower($addr)),quote($post_code));
        $res = pg_query($BID,$cmd);
        if($row = pg_fetch_assoc($res)){
            $_SESSION['register_upw']=$random_pw;
            $_SESSION['register_uuu']=strtolower($row['email']);
            return true;
        } else {
            return "Not valid email or activation code. Probably your activation code expired.";
        }
    }
    return false;
}
/*
 *
 *
 * */
function floatcmp($f1,$f2,$precision = 10)
{
    $e = pow(10,$precision);
    return (intval($f1 * $e) == intval($f2 * $e));
}
/* create html select menu
 * should be generalized??
 *
 */
class select_input_template {
    private $def_style="width:100%";
    public $style="width:100%";
    public $id;
    public $reference;
    public $menu;
    public $size=4;
    public $multi="multiple='multiple'";
    public $autocomplete=false;
    public $colour_rings=false;
    public $colour_rings_options="";
    public $empty_line="";
    public $format_query="";
    public $access_class="";
    public $destination_table = '';
    public $destination_schema = '';
    public $tdata = '';
    public $filter_column = '';
    public $filter_value = '';
    public $all_options = 'off';
    public $onlyoptions = 'off';
    public $placeholder = '';

    function set_style($style) {
        if ($style=='')
            $this->style = $this->def_style;
        else
            $this->style = $style;
    }

    function add_empty_line($e) {
        $this->empty_line = $e;
    }

    function format_query($e) {
        $this->format_query = $e;
    }

    function new_menu($reference='',$filter='') {
        $this->reference = $reference;
        if ($this->destination_table=='')
            $this->destination_table = $_SESSION['current_query_table'];

        if ($this->destination_schema=='')
            $this->destination_schema = isset($_SESSION['current_query_schema']) ? $_SESSION['current_query_schema'] : 'public';

        if ($this->autocomplete)
            $this->push_autocomplete_menu();
        elseif ($this->colour_rings)
            $this->push_colour_rings_menu();
        else
            $this->push_selectmenu($filter);
    }

    // creates an input field for autocomplete input
    function push_autocomplete_menu() {
        $id = $this->reference;

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }
        $placeholder = ($this->placeholder === "") ? "" : "placeholder='{$this->placeholder}'";
        $this->menu = "<input id='$id' class='qfautocomplete $this->access_class' $placeholder $tdata>";
    }

    // colour rings div
    function push_colour_rings_menu() {
        $id = $this->reference;

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }

        $colors = array('red','pink','green','lightgreen','orange','yellow','blue','lightblue','white','black','brown','purple','violet','metal');
        $codes = array('R','P','G','g','O','Y','B','b','W','K','N','U','V','M');
        $textcolors = array('K','K','K','K','K','K','W','K','K','W','W','W','K','K');

        if ($this->colour_rings_options!='') {
            $arc = preg_split('/,/',$this->colour_rings_options);
            $available_ring_colors = array();
            for($i=0;$i<count($arc);$i++) {
                if ($i==0) {
                    if (preg_match("/^\[(.+)\]$/",$arc[0])) {
                        $available_ring_colors[$arc[0]] = array();
                        continue;
                    }
                }
                $cindex = array_search($arc[$i],$codes);
                if ($cindex!==false)
                    $available_ring_colors[$arc[$i]] = array($colors[$cindex]);
            }
        }
        // It is coming from the form settings
        // e.g.
        //$available_ring_colors = "[XX],blue:B,red:B";
        if (isset($available_ring_colors) and count($available_ring_colors)!=0) {
            $arc = array_keys($available_ring_colors);
            $custom_colors = array();
            $custom_codes = array();
            if (preg_match('/^\[(.+)\]$/',$arc[0],$m)) {
                array_shift($available_ring_colors);
                $ring_position_pattern = $m[1];
            }
            foreach ($available_ring_colors as $cc=>$clabel) {
                $label = strtolower($clabel[0]);
                $custom_colors[] = $label;
                if ($cc!='')
                    $custom_codes[] = $cc;
                else {
                    $ci = array_search($label,$colors);
                    $custom_codes[] = $codes[$ci];
                }
            }
        }

        $li = array();
        for($i=0;$i<count($colors);$i++) {

            $dca = array($colors[$i],$codes[$i],$textcolors[$i]);

            if (isset($arc)) {
                $index = array_search($dca[0],$custom_colors);
                if ($index===false) continue;
                if (isset($custom_codes[$index]))
                    $dca[1] = $custom_codes[$index];
            }
            $title = t(constant('str_'.$dca[0]));
            if ($dca[0]=='metal') $dca[0] = 'silver';
            if ($dca[2]=='K') $textcolor = '#000';
            else $textcolor = '#fff';
            $li[] = "<li style='background-color:$dca[0];color:$textcolor' data-color='$dca[1]' data-text='$dca[2]' title='$title'>$dca[1]</li>";
        }

        $out = "
        <div id='colourringdiv'>
            <div style='display:table;border-spacing:2px'>

                <div style='display:table-row;'>
                    <div style='display:block;text-align:right;background-color: #b3cee6;margin-top: -7px;margin-left: -7px;margin-right: -7px;'>
                        <button id='colourringdiv-close' class='pure pure-button button-gray' style='height:30px'><i class='fa fa-close fa-lg'></i></button>
                    </div>
                </div>

                <div style='display:table-row'>
                    <div style='display:table-cell;vertical-align:top;background:repeating-linear-gradient( 45deg, #888, #888 3px, #aaa 3px, #aaa 6px);width:100%'>";
                        //$default_colours = array('red:R:K,','pink:P:K','green:G:K','lightgreen:g:K','orange:O:K','yellow:Y:K','blue:B:W','lightblue:b:K','white:W:K','black:K:W','brown:N:W','purple:U:W','violet:V:K','metal:M:K');
                        if (isset($ring_position_pattern))
                            $out .= "<input type='hidden' value='$ring_position_pattern' id='ring_position_pattern'>";
                        else
                            //two rings on each position
                            $out .= "<input type='hidden' value='XX' id='ring_position_pattern'>";
                        $out .= "
                            <ul class='ring-box ring'>"; $out .= implode('',$li); $out .= "</ul>
                    </div>
                </div>

                <div>
                    <div id='legbox'>
                        <div class='left'>
                            <ul id='leg-left-top' class='ring-position ring'></ul>
                            <ul id='leg-left-bottom' class='ring-position ring'></ul>
                            <span id='leftlabel' class='leftrightlabel'>".str_left."</span>
                        </div>
                        <div class='right'>
                            <ul id='leg-right-top' class='ring-position ring'></ul>
                            <ul id='leg-right-bottom' class='ring-position ring'></ul>
                            <span id='rightlabel' class='leftrightlabel'>".str_right."</span>
                        </div>
                    </div>
                    <div style='float:right'>
                        <button id='rotate' class='pure pure-button button-gray' style='width:40px;'><i class='fa fa-refresh fa-lg fa-rotation'></i></button>
                    </div>
                </div>

                <div>
                    <div><input id='cr_code' style='border:none;background:transparent'></div>
                    <div id='ring-text-div'>".str_ring_label.": <input id='ring-text' class='pure-input pure-u-1' data-text='' data-color=''></div>
                    <div><button class='pure-button button-success pure-u-1' id='addcolourings'>".str_set."</button></div>
                    <input type='hidden' id='colour-ring-referenceid' value='$id'>
                </div>
            </div>
        </div> <!-- colourringdiv -->";

        $this->menu = "$out<input id='$id' class='qfautocomplete show-colour-ring-box $this->access_class' $tdata>";

    }

    // creates a select input fields with options queried from a table
    function push_selectmenu($filter='') {
        global $ID;
        $op = "";
        if ($this->empty_line == 1) {
            $op = "<option value=''></option>";
        }
        # using complex SQL columns naming - ## will be changed the requested column name dinamically
        # I don't know where and how can I use it!!!
        /*if ($format_query != '') {
            $s = preg_replace('/##/',$this->id,$format_query);
        } else*/

        $id = $this->reference;
        $pm_column = $this->reference;
        $table_alias = '';

        $pm_split = preg_split('/\./',$this->reference);
        if (count($pm_split) > 1) {
            $pm_column = $pm_split[1];
            /*if (preg_match('/(.+):(.+)/',$pm_column,$m)) {
                $pm_column = $m[1];
                $pm_column_id = $m[2];
        }*/
            $table_alias = $pm_split[0];
            //html id
            $id = preg_replace('/\./','-',$this->reference);
        }
        if ($this->id!='')
            $id = $this->id;

        $transform_to_boolen = 0;
        if ($filter != '') {
            if (preg_match('/_boolen/',$filter,$m)) {
                $transform_to_boolen = 1;
                $filter = '';
            } elseif (preg_match('/values\((.*)\)/',$filter,$m)) {
                if ($m[1]=='')
                    $filter = 'WHERE 1=0';

            } else {
                $filter = '';
            }
        }

        // imporved filtering
        if ($this->filter_column!='' and $this->filter_value!='') {
            $this->filter_column = preg_replace('/-/','.',$this->filter_column);
            $imp = implode(',',array_map('quote',$this->filter_value));
            $filter = "WHERE $this->filter_column IN ($imp)";
        }

        // data-... elements
        $tdata = '';
        if ($this->tdata != '') {
            foreach ($this->tdata as $target=>$value) {
                $tdata .= "data-$target='".$value."' ";
            }
        }

        if (array_key_exists($pm_column,dbcolist('array',$this->destination_table,$this->destination_schema))) {
            $stcol = st_col($this->destination_table,'array');

            $sp = preg_split('/\./',$this->destination_table);
            if (count($sp) == 2) {
                $this->destination_table = preg_replace("/[^a-z0-9_]/i","",$sp[1]);
                $this->destination_schema = preg_replace("/[^a-z0-9_]/i","",$sp[0]);
            }

            // FUll list, no distinct, but ID
            if ($this->all_options=='on')
                $cmd = "SELECT $pm_column AS n, substring($pm_column::character varying,1,24) AS l, {$stcol['ID_C']} AS id FROM ".$this->destination_schema.".".$this->destination_table." $table_alias $filter";
            else
                // select-options no ID, just distinct!!!
                $cmd = "SELECT DISTINCT $pm_column AS n, substring($pm_column::character varying,1,24) AS l FROM ".$this->destination_schema.".".$this->destination_table." $table_alias $filter";

            $result = pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action("SQL ERROR in selecting menu options",__FILE__,__LINE__);
                log_action($cmd);
            }
            $sort = array();
            while($row=pg_fetch_assoc($result)) {
                if ($this->size>1 and $row['n']=='') {
                    continue;
                }
                $value = $row['n'];
                $label = $row['l'];
                if ($transform_to_boolen and $label == 1) {
                    $label = str_true;
                } elseif ($transform_to_boolen and $label == 0) {
                    $label = str_false;
                }
                if (isset($row['id']))
                    $dataid = $row['id'];
                else
                    $dataid = '';

                if (defined($value))
                    $label = constant($value);

                $sort[$label] = sprintf('<option value="%s" data-id="%s" title="%s">%s</option>',$value,$dataid,$label,$label);
            }
            #$col = new \Collator('hu_HU');
            #$col->asort( $sort );
            ksort($sort);
            if ($this->size>1 and count($sort)<$this->size) {
                $size = count($sort);
            } else {
                $size = $this->size;
            }
            $m = "<select size='$size' style='{$this->style}' {$this->multi} id='$id' class='$this->access_class' $tdata>";
            $mo = implode("",$sort);
            if ($this->onlyoptions=='on')
                $this->menu = $mo;
            else
                $this->menu = $m.$mo."</select>";
        } else {
            if ($pm_column!='' and !preg_match('/^_table_prefix/',$pm_column)) {
                $this->menu = "NA";
                log_action("$pm_column column does not exists in {$this->destination_table} table!",__FILE__,__LINE__);
            }
        }
    }
}
// create html table rows put td cell colspan, etc...
class table_row_template {
    public $tmpl_col = array();
    public $tmpl_row = array();
    public $colspan=0;
    /*     f: cell content
     *    cp: colspan
     * class: css class
     * style: css style
     * */
    function cell($f,$cp=1,$class='',$style='') {
        $colspan = "";
        if ($cp>1) $colspan = "colspan='$cp'";
        $this->tmpl_col[] = "<td $colspan class='$class' style='$style'>$f</td>";
    }
    function row($class='',$style='') {
        $r = implode("",$this->tmpl_col);
        $this->tmpl_col = array();
        $this->tmpl_row[] = "<tr class='$class' style='$style'>$r</tr>";
    }
    function printOut() {
        return implode("",$this->tmpl_row);
    }
}
/* Send mail to...
 *
 *
 * */
function mail_to_ori($To,$Subject,$FromName,$FromEmail,$Title,$Message,$method,$domain,$realemail) {
    $stripped_title = strip_tags($Title);
    $stripped_message = strip_tags($Message,'<a>');

    $HTMLMessage = "<h2>$Title</h2>$Message";
    $TEXTMessage = $stripped_title."\n\n".$stripped_message;
    $subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';
    $mime_boundary = "openbiomaps.org_".md5(time());

    if ($method == 'simple')
    {
        # Mail headers
        $Headers  = "From: $FromName <$FromEmail@$domain>\r\n";
        $Headers .= "Reply-To: $realemail\r\n";
        $Headers .= "MIME-Version: 1.0\r\n";
        $Headers .= "Content-Type: text/plain; charset=utf8\r\n";
        $Headers .= 'X-Mailer: PHP/' . phpversion();
    }
    elseif ($method == 'multipart')
    {
        # Mail headers
        $Headers  = "From: $FromName <$FromEmail@$domain>\r\n";
        $Headers .= "Reply-To: $realemail\r\n";
        $Headers .= "MIME-Version: 1.0\r\n";
        $Headers .= "Content-Type: multipart/alternative;\n\tboundary=--PHP-alt-$mime_boundary\r\n";
        $Headers .= 'X-Mailer: PHP/' . phpversion();

        ob_start(); //Turn on output buffering
?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/plain;charset=utf8
Content-Transfer-Encoding: 8bit

<?php echo $TEXTMessage."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/html;charset=utf8
Content-Transfer-Encoding: 8bit

<?php echo $HTMLMessage."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>--

<?php
        echo "\r\n";
        $message = ob_get_clean();
    }
    #ini_set("SMTP","vocs.unideb.hu");
    # Send mail
    $mail_sent = @mail( $To, $subject, $message, $Headers );
    if (!$mail_sent) return "Failed to sent mail.";
    else return $mail_sent;
}
/* pear/Mail deprecated */
function pear_mail_to($To,$Subject,$From,$ReplyTo,$Title,$Message,$Method) {
    //https://pear.php.net/package/Mail
    //https://pear.php.net/package/Net_SMTP/
    //require_once "Mail.php";
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');

    //base64 subject
    $Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

    //mime boundary
    $mime_boundary = "openbiomaps.org_".md5(time());

    if ($Method == 'simple')
    {
        $content_type = 'text/plain; charset=utf8';
        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $body = $stripped_title."\n\n".$stripped_message;
    }
    elseif ($Method == 'multipart')
    {
        $body = "<h2>$Title</h2>$Message";

        $stripped_title = strip_tags($Title);
        $stripped_message = strip_tags($Message,'<a><br>');
        $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
        $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
        $plain_body = $stripped_title."\n\n".$stripped_message;

        $content_type = "multipart/alternative;\n\tboundary=--PHP-alt-$mime_boundary";
        ob_start(); //Turn on output buffering
    ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/plain;charset=utf8
Content-Transfer-Encoding: 8bit

    <?php echo $plain_body."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>

Content-Type: text/html;charset=utf8
Content-Transfer-Encoding: 8bit

    <?php echo $body."\r\n"; ?>

----PHP-alt-<?php echo $mime_boundary; ?>--

    <?php
        echo "\r\n";
        $body = ob_get_clean();
    }

    if (defined('SMTP_PORT'))
        $port = array('port' => SMTP_PORT);
    else
        $port = array();

    $headers = array (
        'From' => $From,
        'To' => $To,
        'Reply-To' => $ReplyTo,
        'Subject' => $Subject,
        'MIME-Version' => '1.0',
        'Content-Type' => $content_type,
        'X-Mailer' => 'PHP/' . phpversion()
    );

    if (SENDMAIL=='sendmail')
        $smtp = Mail::factory('sendmail');
    elseif(SENDMAIL=='smtp') {
        // overwrite SMTP sender with SMTP_USERNAME from local_vars.php.inc
        $headers["From"] = SMTP_USERNAME;

        $smtp = Mail::factory('smtp',
        array_merge($port,array ('host' => SMTP_HOST,
               'auth' => SMTP_AUTH,
               'username' => SMTP_USERNAME,
               'password' => SMTP_PASSWORD)));
    }

    $mail = $smtp->send($To, $headers, $body);

    # $pear = new PEAR(); $pear->isError($mail);
    //if (PEAR::isError($mail)) {
    if (@PEAR::isError($mail)) {
        log_action('Message sending failed.',__FILE__,__LINE__);
        log_action($mail->getMessage(),__FILE__,__LINE__);
        return 1;
    } else {

        //"Message successfully sent!";
        return 2;
    }
}

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/* php mailer */
if ( !function_exists( 'mail_to' ) ) {
    function mail_to($To,$Subject,$From,$ReplyTo,$Message,$Method,$ReplyToName='No Reply',$FromName=PROJECTTABLE) {

        require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');

        //base64 subject
        //$Subject = '=?UTF-8?B?'.base64_encode($Subject).'?=';

        if ($Method == 'simple')
        {
            $stripped_message = strip_tags($Message,'<a><br>');
            $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
            $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
            $body = $stripped_message;
            $plain_body = $body;
            $isHTML = false;
        }
        elseif ($Method == 'multipart')
        {
            $body = "$Message";
            $stripped_message = strip_tags($Message,'<a><br>');
            $stripped_message = preg_replace("/<br>/","\n",$stripped_message);
            $stripped_message = preg_replace("/<br \/>/","\n",$stripped_message);
            $plain_body = $stripped_message;
            $isHTML = true;
        }

        $mail = new PHPMailer(true);        // Passing `true` enables exceptions
        try {
            //Server settings
            
            $mail->SMTPDebug = defined('SMTP_DEBUG') ? SMTP_DEBUG : 0; // Enable verbose debug output?
            
            if (SENDMAIL=='sendmail') {
                $mail->isSendmail();

            } elseif(SENDMAIL=='smtp') {
                $From = defined('SMTP_SENDER') ? SMTP_SENDER : SMTP_USERNAME;

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = SMTP_HOST;                              // Specify main and backup SMTP servers: 'smtp1.example.com;smtp2.example.com'
                $mail->SMTPAuth = SMTP_AUTH;                          // Enable SMTP authentication     true | false without quote!!
                $mail->Username = SMTP_USERNAME;                      // SMTP username
                $mail->Password = SMTP_PASSWORD;                      // SMTP password
                if (defined('SMTP_SECURE')) {
                    $mail->SMTPSecure = SMTP_SECURE; // Enable TLS encryption, `tls | ssl` accepted
                }
                else {
                    $mail->SMTPAutoTLS = false;
                }
                $mail->Port = defined('SMTP_PORT') ? SMTP_PORT : 25;                                  // TCP port to connect to
            }

            //Recipients
            $mail->setFrom($From, $FromName);
            #$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
            if (is_array($To)) {
                #$mail->addCC('cc@example.com');
                #$mail->addBCC('bcc@example.com');
                $first_rcp = array_pop($To);
                $mail->addAddress($first_rcp);
                foreach ($To as $cc) {
                    $mail->addCC($cc);
                }
            }
            else {
                $mail->addAddress($To);               // Name is optional
            }
            $ReplyTo = preg_replace('/localhost/','openbiomaps.org',$ReplyTo);
            $mail->addReplyTo($ReplyTo, $ReplyToName);

            //Attachments
            #$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML($isHTML);                                  // Set email format to HTML
            $mail->Subject = $Subject;
            $mail->Body    = $body;
            $mail->AltBody = $plain_body;

            $mail->CharSet = 'UTF-8';
            $mail->send();
            //"Message successfully sent!";
            return 2;
        } catch (Exception $e) {
            log_action('Message sending failed.',__FILE__,__LINE__);
            log_action($mail->ErrorInfo,__FILE__,__LINE__);
            return 1;
        }
    }
}
/*
 *
 *
 * */
function get_type($var)
{
    if (is_array($var)) return "array";
    if (is_bool($var)) return "boolean";
    if (is_float($var)) return "float";
    if (is_int($var)) return "integer";
    if (is_null($var)) return "NULL";
    if (is_numeric($var)) return "numeric";
    if (is_object($var)) return "object";
    if (is_resource($var)) return "resource";
    if (preg_match('/([0-9]{1,4}).([0-9]{1,2}).([0-9]{1,4}).?/',$var,$m)) {
        $date = array($m[1],$m[2],$m[3]);
        $y_i = array_keys($date, max($date));
        $year = array_slice($date,$y_i[0],1);
        $year = $year[0];
        if (checkdate($date[0], $date[1], $year))
            return "datum"; #"$year-{$date[0]}-{$date[1]}";
        else
            return "Invalid date";
    }
    if (is_string($var)) return "string";
    return "unknown";
}
/* This function return with information about the target table
 * Arguments
 * Control:
 *   count: return with number of elements
 *   columns: return with an array of column names
 *   array: return with associative array column=>short_name
 * Table:
 * Schema:
 * */
function dbcolist($control='', $table=PROJECTTABLE, $schema = 'public', $usecache = true) {
    global $BID,$GID,$ID;

    if ($usecache)
        $cache = obm_cache('get',"dbcolist.$schema.$table",'','',TRUE);
    else
        $cache = false;

    // Ez egy csúnya hack!!! Ezt nem így kell csinálni
    if (isset($_SESSION['update_lang']))  {
        $cache = false;
        unset($_SESSION['update_lang']);
    }
    // automatic cache clean if page did not updated ???
    //if ($_SESSION['LANG_SET_TIME'] > time()-300)
    //    $cache = false;

    if ($cache) {
        if ($control=='count')
            return count($cache);
        elseif($control=='columns')
            return array_keys($cache);
        else
            return $cache;
    }

    $st_col = st_col($table,'array');

    $H = array();
    $cmd = "SELECT column_name as key,short_name as value FROM project_metaname WHERE project_table='$table' AND project_schema='$schema' ORDER BY column_name";
    $result = pg_query($BID,$cmd);
    //$skip_col = array();
    if (pg_num_rows($result)) {
        while($row=pg_fetch_assoc($result)) {
            //if(in_array($row['key'],$skip_col)) continue;

            if (preg_match('/^str_/',$row['value']))
                if (defined($row['value'])) {
                    $row['value'] = constant($row['value']);
                }
            $H[$row['key']]=$row['value'];
        }
    } else {
        // Ebbe az ág mikor kerülünk? Hol van ilyen hívás?
        // Amikor rosszul kapja meg a dbcolist a tábla nevét és abban már benne van a schema is.
        // Pl custom_box -> text_filter
        $sp = preg_split('/\./',$table);
        if (count($sp) == 2) {
            $table = preg_replace("/[^a-z0-9_]/i","",$sp[1]);
            $schema = preg_replace("/[^a-z0-9_]/i","",$sp[0]);
        }
        // no assigned columns just registered table in header_names
        $cmd = sprintf('SELECT f_project_schema FROM header_names WHERE f_project_name=\'%1$s\' and f_project_table=%2$s',PROJECTTABLE,quote($table));
        $result = pg_query($BID,$cmd);
        if (pg_num_rows($result)) {
            $row = pg_fetch_assoc($result);
            $schema = $row['f_project_schema'];

            $cmd = sprintf('SELECT column_name FROM information_schema.columns WHERE table_name = %1$s AND table_schema=%2$s',quote($table),quote($schema));
            // GID ???
            $result = pg_query($ID,$cmd);
            while($row=pg_fetch_assoc($result)) {
                $H[$row['column_name']]=$row['column_name'];
            }
        } else {
            log_action("$table is not registered for ".PROJECTTABLE." project in header_names",__FILE__,__LINE__);
        }
    }
    /* Species name: default names
     * only in the main table
    foreach($st_col['ALTERN_C'] as $k=>$v){
            if ($k=='') continue;
            if ($v=='sci') {
                if (preg_match('/^'.$table.'\.(\w+)/',$k,$m)) {
                    $k = $m[1];
                    $v = str_sci_name;
                    $H[$k]=$v;
                }
            }
            else {
                if (preg_match('/^'.$table.'\.(\w+)/',$k,$m)) {
                    $k = $m[1];
                    $v = str_alt_name;
                    $H[$k]=$v;
                }
            }
    }
    */

    // ordering
    $order_table = array();
    $order_col = json_decode($st_col['ORDER'],true);
    //{"ddnpi":{"faj":"10","magyar":"20","egyedszam":"30","szamossag":"31","dt_from":"40","dt_to":"50","time":"51","eov_x":"60","eov_y":"70","obm_geometry":"71","gyujto":"80","gyujto2":"90","gyujto3":"100","adatkozlo":"110","megj":"115","hatarozo":"120","kor":"130","him":"140","nosteny":"150","eloford_al":"160","modsz":"170","visszafogas":"180","hely":"190","elohely":"191","veszteny":"192","feszkeles":"218","gyuruszam":"219","szinesgyuru":"220","vedettseg_p":"229","natura_2000_p":"230","rogz_modsz":"240","rogz_dt":"241"}}
    if (is_array($order_col) and isset($order_col[$table]))
        $order_table = $order_col[$table];
    //{"faj":"10","magyar":"20","egyedszam":"30","szamossag":"31","dt_from":"40","dt_to":"50","time":"51","eov_x":"60","eov_y":"70","obm_geometry":"71","gyujto":"80","gyujto2":"90","gyujto3":"100","adatkozlo":"110","megj":"115","hatarozo":"120","kor":"130","him":"140","nosteny":"150","eloford_al":"160","modsz":"170","visszafogas":"180","hely":"190","elohely":"191","veszteny":"192","feszkeles":"218","gyuruszam":"219","szinesgyuru":"220","vedettseg_p":"229","natura_2000_p":"230","rogz_modsz":"240","rogz_dt":"241"}

    $ms = array();
    if (count(array_values($order_table))) {
        $cl = max(array_values($order_table))+1;
        foreach(array_keys($H) as $k) {
            $order = '';
            if (array_key_exists($k,$order_table)) {
                $order = $order_table[$k];
            }
            if($order!=='')
                $ms[] = $order;
            else
                $ms[] = $cl;
        }
        array_multisort($ms,$H);
    }

    obm_cache('set',"dbcolist.$schema.$table",$H,300,TRUE);

    if ($control=='count') return count($H);
    elseif ($control=='columns') return array_keys($H);
    elseif ($control=='array') return $H;
    else return false;
}
/*
 *
 *
 * */
function parse_csv($file, $options = null) {
    $delimiter = empty($options['delimiter']) ? "," : $options['delimiter'];
    $to_object = empty($options['to_object']) ? false : true;
    $str = file_get_contents($file);
    $lines = explode("\n", $str);
    //pr($lines);
    $field_names = explode($delimiter, array_shift($lines));
    foreach ($lines as $line) {
        // Skip the empty line
        if (empty($line)) continue;
        $fields = explode($delimiter, $line);
        $_res = $to_object ? new stdClass : array();
        foreach ($field_names as $key => $f) {
            if ($to_object) {
                $_res->{$f} = $fields[$key];
            } else {
                $_res[$f] = $fields[$key];
            }
        }
        $res[] = $_res;
    }
    return $res;
}

/* XML - GML processing functions
 * not used*/
function GML_coords_from_bounds($child,$namespaces) {
        $GML_Bounds = '';
        if ($child->getName() != 'boundedBy') return;
        //echo $child->getName() . " ";
        foreach($child->children($namespaces['gml']) as $boundedBy){
            //echo $boundedBy->getName() . " ";
            foreach($boundedBy->children($namespaces['gml']) as $coordinates){
                //echo $coordinates->getName() . ": $coordinates";
                $GML_Bounds .= $coordinates;
            }
        }
        return $GML_Bounds;
}

/* XML - GML processing functions
 * coordinates from wfs 1.1.0 xml response*/
function GML_coords_from_points($child,$namespaces) {
        $GML_Bounds = '';
        //if ($child->getName() != 'Point') return;
        //echo $child->getName() . " ";
        foreach($child->children($namespaces['gml']) as $coordinates){
            //echo $coordinates->getName() . ": $coordinates";
            $GML_Bounds .= $coordinates;
        }
        return $GML_Bounds;
}
/* XML - GML processing functions
 * coordinates from wfs 1.1.0 xml response*/
function GML_coords_from_Mpoints($child,$namespaces) {
        $GML_Bounds = array();
        //if ($child->getName() != 'MultiPoint') return;
        foreach($child->children($namespaces['gml']) as $pointMember)
            foreach ($pointMember as $pos)
                $GML_Bounds[] = $pos->{'pos'}->__toString();

        return $GML_Bounds;
}
/* create project specific taxon and history table
 * can be use for custom tables?
 * */
function Create_Project_plus_tables($schema = 'public', $table,$f,$owner) {
    global $ID;
    $send = '';

    $st_col = st_col($table,'array');

    $project_admin = defined('PROJECT_ADMIN') ? constant('PROJECT_ADMIN') : 'project_admin';

    if ($f == 'rules') {
        // Rules table
        $send = sprintf('
    SET statement_timeout = 0;
    SET lock_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_rules (
        "data_table" character varying(128) NOT NULL,
        "row_id" integer NOT NULL,
        "owner" character varying(255),
        "read" integer[],
        "write" integer[],
        "sensitivity" character varying(16) DEFAULT \'public\'
    );
    ALTER TABLE %1$s_rules OWNER TO %2$s;
    ALTER TABLE ONLY %1$s_rules ADD CONSTRAINT %1$s_rules_ukey UNIQUE ("data_table", row_id);
    REVOKE ALL ON TABLE %1$s_rules FROM PUBLIC;
    REVOKE ALL ON TABLE %1$s_rules FROM %2$s;
    GRANT ALL ON TABLE %1$s_rules TO %2$s;',$table,$owner);


    } elseif ( $f == 'history' ) {
        // History Table
        $send = sprintf('
    SET statement_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_history (
        operation character(1) NOT NULL,
        hist_time timestamp without time zone NOT NULL,
        userid text NOT NULL,
        query text,
        row_id integer NOT NULL,
        hist_id integer NOT NULL,
        modifier_id integer,
        data_table character varying(64)
    );
    ALTER TABLE public.%1$s_history OWNER TO %2$s;
    CREATE SEQUENCE %1$s_history_hist_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER TABLE public.%1$s_history_hist_id_seq OWNER TO %2$s;
    ALTER SEQUENCE %1$s_history_hist_id_seq OWNED BY %1$s_history.hist_id;
    ALTER TABLE ONLY %1$s_history ALTER COLUMN hist_id SET DEFAULT nextval(\'%1$s_history_hist_id_seq\'::regclass);
    ALTER TABLE ONLY %1$s_history
        ADD CONSTRAINT %1$s_history_pkey PRIMARY KEY (hist_id);
    CREATE INDEX %1$s_history_row_id_idx ON %1$s_history USING btree (row_id);',$table,$owner);

    } else if ( $f == 'taxon' ) {
        // Taxon table
        $send = sprintf('
    SET statement_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    SET search_path = public, pg_catalog;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE %1$s_taxon (
        meta character varying(254),
        word character varying(254),
        lang character varying(16),
        taxon_id integer NOT NULL,
        status numeric(1,0) DEFAULT 0,
        modifier_id integer DEFAULT 0,
        frequency integer DEFAULT 0,
        taxon_db integer DEFAULT 0,
        wid integer DEFAULT 0
    );
    ALTER TABLE public.%1$s_taxon OWNER TO %2$s;
    CREATE SEQUENCE %1$s_taxon_taxon_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    CREATE SEQUENCE %1$s_taxon_wid_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER TABLE public.%1$s_taxon_taxon_id_seq OWNER TO %2$s;
    ALTER SEQUENCE %1$s_taxon_taxon_id_seq OWNED BY %1$s_taxon.taxon_id;
    ALTER TABLE ONLY %1$s_taxon ALTER COLUMN taxon_id SET DEFAULT nextval(\'%1$s_taxon_taxon_id_seq\'::regclass);
    ALTER TABLE public.%1$s_taxon_wid_seq OWNER TO %2$s;
    ALTER SEQUENCE %1$s_taxon_wid_seq OWNED BY %1$s_taxon.wid;
    ALTER TABLE ONLY %1$s_taxon ALTER COLUMN wid SET DEFAULT nextval(\'%1$s_taxon_wid_seq\'::regclass);
    ALTER TABLE ONLY %1$s_taxon ADD CONSTRAINT %1$s_taxon_metaword_key UNIQUE (meta,word);
    CREATE INDEX %1$s_trgm_idx ON %1$s_taxon USING gist (meta gist_trgm_ops);
    CREATE INDEX %1$s_meta_idx ON %1$s_taxon USING btree (meta);
    CREATE INDEX %1$s_word_idx ON %1$s_taxon USING btree (word);',$table,$owner);

    } else if ( $f == 'exports' ) {
        $send = sprintf('
    SET statement_timeout = 0;
    SET lock_timeout = 0;
    SET idle_in_transaction_session_timeout = 0;
    SET client_encoding = \'UTF8\';
    SET standard_conforming_strings = on;
    SELECT pg_catalog.set_config(\'search_path\', \'\', false);
    SET check_function_bodies = false;
    SET xmloption = content;
    SET client_min_messages = warning;
    SET row_security = off;
    SET default_tablespace = \'\';
    SET default_with_oids = false;
    CREATE TABLE system.%1$s_data_exports (
        id integer NOT NULL,
        filename character varying(128),
        user_id integer NOT NULL,
        status character varying(32) NOT NULL,
        message text,
        downloaded integer DEFAULT 0,
        requested timestamp without time zone,
        valid_until timestamp without time zone,
        colnames json,
        maxlength integer
    );
    ALTER TABLE system.%1$s_data_exports OWNER TO %2$s;
    CREATE SEQUENCE system.%1$s_data_exports_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
    ALTER TABLE system.%1$s_data_exports_id_seq OWNER TO %2$s;
    ALTER SEQUENCE system.%1$s_data_exports_id_seq OWNED BY system.%1$s_data_exports.id;
    ALTER TABLE ONLY system.%1$s_data_exports ALTER COLUMN id SET DEFAULT nextval(\'system.%1$s_data_exports_id_seq\'::regclass);
    ALTER TABLE system.%1$s_data_exports OWNER TO %1$s_admin;
    ALTER TABLE ONLY system.%1$s_data_exports
        ADD CONSTRAINT %1$s_data_exports_filename UNIQUE (filename);
    ALTER TABLE ONLY system.%1$s_data_exports
        ADD CONSTRAINT %1$s_data_exports_id PRIMARY KEY (id);
    GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE system.%1$s_data_exports TO %3$s;',
        $table,$owner,$project_admin);
    }
    return $send;
}
/* create project specific taxon and history trigger functions
 * can be use for custom triggers???
 * */
function Create_Project_SQL_functions($schema = 'public',$table,$f,$owner) {
    global $ID;
    $send = '';

    $st_col = st_col($table,'array');

    if ($f == 'taxonlist_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='taxon_update_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s.%2$s DISABLE TRIGGER taxon_update_%1$s',$schema,$table);

        } else {
            $send = sprintf('DROP TRIGGER IF EXISTS taxon_update_%2$s ON %1$s.%2$s;
            CREATE TRIGGER taxon_update_%2$s BEFORE INSERT OR UPDATE ON %1$s.%2$s FOR EACH ROW EXECUTE PROCEDURE %1$s.update_%2$s_taxonlist();',$schema,$table);
        }
    } elseif ($f == 'taxonname_trigger') {
        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='%s_name_update' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s.%2$s DISABLE TRIGGER %2$s_name_update',$schema,$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS %1$s_name_update ON %1$s.%2$s_taxon;
            CREATE TRIGGER %2$s_name_update AFTER UPDATE ON %1$s.%2$s_taxon FOR EACH ROW EXECUTE PROCEDURE %1$s.update_%2$s_taxonname();',$schema,$table);
        }
    } elseif ($f == 'history_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='history_update_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s.%2$s DISABLE TRIGGER history_update_%2$s',$schema,$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS history_update_%2$s ON %1$s.%2$s;
            CREATE TRIGGER history_update_%2$s AFTER DELETE OR UPDATE ON %1$s.%2$s FOR EACH ROW EXECUTE PROCEDURE %1$s.history_%2$s();',$schema,$table);
        }
    } elseif ($f == 'rules_trigger') {

        $trigger_enabled = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s' AND tgenabled != 'D')",$table);
        $result = pg_query($ID,$trigger_enabled);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $send = sprintf('ALTER TABLE %1$s.%2$s DISABLE TRIGGER rules_%2$s',$schema,$table);

        } else {

            $send = sprintf('DROP TRIGGER IF EXISTS rules_%2$s ON %1$s.%2$s;
            CREATE TRIGGER rules_%2$s AFTER DELETE OR UPDATE OR INSERT ON %1$s.%2$s FOR EACH ROW EXECUTE PROCEDURE %1$s.rules_%2$s();',$schema,$table);

        }
        // Create _rules table if not exists
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE  table_schema = 'public' AND table_name = '%s_rules' )",PROJECTTABLE);
        $result = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($result);
        if ($row['exists']=='f') {
            $cmd = Create_Project_plus_tables($schema,PROJECTTABLE,'rules',gisdb_user);
            $result = pg_query($ID,$cmd);
        }

    } elseif ($f == 'taxonlist') {
        $SPECIES_C = $st_col['SPECIES_C'];
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
            $SPECIES_C = $m[2];

        $send = sprintf('DROP FUNCTION IF EXISTS %3$s.update_%1$s_taxonlist() cascade;
        CREATE OR REPLACE FUNCTION %3$s.update_%1$s_taxonlist()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF tg_op = \'\'INSERT\'\' THEN
        INSERT INTO %3$s.%1$s_taxon (meta,word,lang,modifier_id)
            SELECT replace(new.%2$s,\'\' \'\',\'\'\'\'),new.%2$s,\'\'%2$s\'\',new.obm_modifier_id
            WHERE NOT EXISTS (
                    SELECT taxon_id FROM %3$s.%1$s_taxon WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=meta
            ) AND new.%2$s IS NOT NULL; ',$table,$st_col['SPECIES_C'],$schema);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;

            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
            //    $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send.= sprintf('
        INSERT INTO %4$s.%1$s_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.%3$s,\'\' \'\',\'\'\'\'),new.%3$s,\'\'%3$s\'\',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id
            FROM %4$s.%1$s_taxon s LEFT JOIN %4$s.%1$s_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM %4$s.%1$s_taxon WHERE replace(new.%3$s,\'\' \'\',\'\'\'\')=meta
        ) AND new.%3$s IS NOT NULL;',$table,$SPECIES_C,$column,$schema);
            }
        $send.= sprintf('RETURN new;END IF;');
        $send .= sprintf('
    IF tg_op = \'\'UPDATE\'\' THEN
        INSERT INTO %3$s.%1$s_taxon (meta,word,lang,modifier_id)
            SELECT replace(new.%2$s,\'\' \'\',\'\'\'\'),new.%2$s,\'\'%2$s\'\',new.obm_modifier_id
            WHERE NOT EXISTS (
                    SELECT taxon_id FROM %3$s.%1$s_taxon WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=meta
            ) AND new.%2$s IS NOT NULL; ',$table,$st_col['SPECIES_C'],$schema);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;

            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
            //    $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send.= sprintf('
        INSERT INTO %4$s.%1$s_taxon (taxon_id,meta,word,lang,modifier_id)
        SELECT sq.taxon_id,replace(new.%3$s,\'\' \'\',\'\'\'\'),new.%3$s,\'\'%3$s\'\',new.obm_modifier_id
        FROM (
            SELECT DISTINCT ON (h.taxon_id) h.taxon_id
            FROM %4$s.%1$s_taxon s LEFT JOIN %1$s_taxon h ON s.taxon_id=h.taxon_id WHERE replace(new.%2$s,\'\' \'\',\'\'\'\')=s.meta) as sq
        WHERE NOT EXISTS (
            SELECT 1 FROM %4$s.%1$s_taxon WHERE replace(new.%3$s,\'\' \'\',\'\'\'\')=meta
        ) AND new.%3$s IS NOT NULL;',$table,$SPECIES_C,$column,$schema);
        }
            $send.= sprintf('RETURN new;
    END IF;');

            $send.= sprintf('
END \' LANGUAGE plpgsql;');
            $send .= sprintf('ALTER FUNCTION update_%1$s_taxonlist() OWNER TO %2$s;',PROJECTTABLE,$owner);
    }  elseif($f=='taxonname') {
        $SPECIES_C = $st_col['SPECIES_C'];
        //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
        //    $SPECIES_C = $m[2];

        $send = sprintf('DROP FUNCTION IF EXISTS %4$s.update_%3$s_taxonname() cascade;
        CREATE OR REPLACE FUNCTION %4$s.update_%3$s_taxonname()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF tg_op = \'\'UPDATE\'\' THEN
        IF new.lang=\'\'%2$s\'\' THEN
            UPDATE %1$s SET %2$s=new.word,obm_modifier_id=new.modifier_id WHERE %1$s.%2$s=old.word;',$table,$SPECIES_C,PROJECTTABLE,$schema);
        foreach($st_col['ALTERN_C'] as $column) {
            if (trim($column)=='') continue;

            if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$column,$m))
                $column = $m[2];
            if ($SPECIES_C == $column) continue;

            $send .= sprintf('
        ELSE
            UPDATE %3$s.%1$s SET %2$s=new.word,obm_modifier_id=new.modifier_id WHERE %1$s.%2$s=old.word;',$table,$column,$schema);
        }
$send .= sprintf('
        END IF;
        RETURN new;
    END IF;
END \' LANGUAGE plpgsql;',$table);
    } elseif($f=='history') {
        $send = sprintf('DROP FUNCTION IF EXISTS %3$s.history_%2$s() cascade;
        CREATE FUNCTION %3$s.history_%2$s()
        RETURNS TRIGGER AS
        \'
BEGIN
    IF (TG_OP = \'\'DELETE\'\') THEN
        INSERT INTO %1$s_history (operation,hist_time,userid,query,row_id,modifier_id,data_table)
            SELECT \'\'D\'\', now(), user, CONCAT(OLD.*), OLD.obm_id, OLD.obm_modifier_id, \'\'%2$s\'\';
        RETURN OLD;
    ELSIF (TG_OP = \'\'UPDATE\'\') THEN
        INSERT INTO %1$s_history (operation,hist_time,userid,query,row_id,modifier_id,data_table)
            SELECT \'\'U\'\', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, \'\'%2$s\'\';
        RETURN NEW;
    END IF;
END \' LANGUAGE plpgsql;',PROJECTTABLE,$table,$schema);

    } elseif ($f=='rules') {

        # ERROR: record "old" has no field "obm_access" CONTEXT: SQL statement " SELECT (TG_OP = 'UPDATE' AND OLD.obm_access != NEW.obm_access)" PL/pgSQL function rules_onpi() line 11 at IF
        # Az obm_access oszlop nem alapvetés, hogy létezik!!!!
        # default policy: restricted
        # Is there any more flexible way to set default policy???
        $projecttable = PROJECTTABLE;
        $send = "DROP FUNCTION IF EXISTS $schema.rules_{$table}() cascade;
        CREATE FUNCTION $schema.rules_{$table}()
        RETURNS TRIGGER AS $$
        DECLARE 
            project varchar;
            tbl varchar;
            sens varchar DEFAULT 'public';
            upl RECORD;
        BEGIN
             project := '{$projecttable}';
             tbl := TG_TABLE_NAME;
             
             IF (TG_OP = 'DELETE') THEN
                 EXECUTE format('DELETE FROM %s_rules WHERE data_table = %L AND row_id = %L', project, tbl, old.obm_id);
                 
                 RETURN OLD;

             ELSIF (TG_OP = 'UPDATE') THEN
                 IF to_jsonb(NEW) ? 'obm_access' THEN
                     sens := new.obm_access;
                 ELSE
                     sens := 'restricted';
                 END IF;
                 EXECUTE format('UPDATE %s_rules SET sensitivity = %L WHERE data_table = %L AND row_id = %L', project, sens, tbl, NEW.obm_id);
                 
                 RETURN NEW;
                 
             ELSIF (TG_OP = 'INSERT') THEN
                 IF to_jsonb(NEW) ? 'obm_access' THEN
                     sens := new.obm_access;
                 ELSE
                     sens := 'restricted';
                 END IF;
                 SELECT \"group\" as rd, owner as wr INTO upl FROM system.uploadings WHERE uploadings.id=NEW.obm_uploading_id;
                 EXECUTE format('INSERT INTO %s_rules (data_table,row_id,read,write,sensitivity) VALUES (%L, %L, %L, %L, %L);', project, tbl, NEW.obm_id, upl.rd, upl.wr, sens);
                 
                 RETURN NEW;
             END IF;
         END $$ LANGUAGE plpgsql;";


    } elseif ($f=='main') {
        // kell ez valamire valahol???
        // main triggers
        $cmd = sprintf('SELECT event_object_table,trigger_name,event_manipulation,action_statement,action_timing
                        FROM information_schema.triggers
                        WHERE event_object_table=\'%1$s\'
                        ORDER BY event_object_table,event_manipulation',$table);
        $result = pg_query($ID,$cmd);
        while($row=pg_fetch_assoc($result)) {
            print_r($row);
        }
        $send = sprintf('
        ',$table);
    }

    return $send;
}

function file_upload($files) {
    $log = '';
    $e = array(
        0=>"There is no error, the file uploaded with success",
        1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
        2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
        3=>"The uploaded file was only partially uploaded",
        4=>"No file was uploaded",
        6=>"Missing a temporary folder",
        7=>"Failed to write file to disk",
        8=>"A PHP extension stopped the file upload."
    );
    $i = 0;
    if (is_array($files['error'])) {
        foreach ($files['error'] as $error) {
            if ($error) $log .= "Error: ".$files['name'][$i].": ".$e[$error];
            $i++;
        }
    } else {
        if ($files['error']) $log .= "Error: ".$files['name'].": ".$e[$error];
    }
    if ($log != '')
        log_action("FILE upload error: $log",__FILE__,__LINE__);

    return $log;
}
# create array from array
# args: a multidimensional array
# return: an other m.array
# used: not!
function rearrange( $arr ){
    foreach( $arr as $key => $all ){
        foreach( $all as $i => $val ){
            $new[$i][$key] = $val;
        }
    }
    return $new;
}
/* read KML file
 * */
function readKML($xml,$out,$file='') {
    $data = array();

    /*$dom = new DOMDocument();
    $dom->loadXml($xml);

    $xpath = new DOMXpath($dom);
    $rootNamespace = $dom->lookupNamespaceUri($dom->namespaceURI);
    $xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd');

    $Placemark = $dom->getElementsByTagName('Placemark');
    debug($Placemark);
    return;*/


    //$xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd');
    //$xpath->registerNamespace('kml', 'http://schemas.opengis.net/kml/2.3/ogckml23.xsd');
    //$xpath->registerNamespace('kml', 'http://www.opengis.net/kml/2.2');
    //$places = $xpath->evaluate('//kml:Placemark', NULL, FALSE);

    //debug( $dom->saveXML() );
    //return;

    //foreach ($places as $place) {
    //

    //$kml = new SimpleXMLElement($xml);
    //$namespaces = $kml->getNamespaces(true);
    // debug($namespaces); {"":"http:\/\/earth.google.com\/kml\/2.2"}
    //$namespaces = $kml->getDocNamespaces();
    // debug($namespaces); {"":"http:\/\/earth.google.com\/kml\/2.2"}
    //$result = $kml->xpath('//Placemark');

    //while(list( , $node) = each($result)) {
    //    debug( '/Placemark: '.$node );
    //}
    //return;


    $kml = simplexml_load_string($xml,'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
    $path = array('Document'=>false,'Folder'=>false,'Placemark'=>false);

    // simple path evaluation
    // xpath not works with many kml, however it is strange and I can't understand why...
    foreach ($kml->children() as $child) {
        if ($child->getName() == 'Document') {
            $path['Document'] = true;
            foreach ($child->children() as $subchild) {
                if ($subchild->getName() == 'Folder') {
                    $path['Folder'] = true;

                    foreach ($subchild->children() as $subchild2) {
                        if ($subchild2->getName() == 'Placemark') {
                            $path['Placemark'] = true;
                        }
                    }
                }
                if ($subchild->getName() == 'Placemark') {
                    $path['Placemark'] = true;
                }
            }
        }
    }

    //$kml->registerXPathNamespace('kml', 'http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd');
    //$Placemark = $kml->xpath('//Document/Folder/Placemark');
    if ($path['Document'] and $path['Folder'] and $path['Placemark'])
        $Placemark = $kml->Document->Folder->Placemark;
    elseif ($path['Document'] and !$path['Folder'] and $path['Placemark'])
        $Placemark = $kml->Document->Placemark;


    foreach($Placemark as $place){


        $point = array();
        $lines = array();
        $linearring = array();

        if (isset($place->{'Point'})) {
            $point = preg_split('/,/',$place->{'Point'}->{'coordinates'}[0]);
        }
        elseif (isset($place->{'LineString'})) {
            $lines = preg_split('/\s/',$place->{'LineString'}->{'coordinates'}[0]);
        }
        elseif (isset($place->{'Polygon'})) {
            // it is deeper some path evaluation is necessaryy....
            // 	<outerBoundaryIs> <LinearRing> <coordinates>
            $linearring = preg_split('/\s/',$place->{'Polygon'}->outerBoundaryIs->LinearRing->{'coordinates'}[0]);
        }

        // xpath not works in many cases
        //$point = explode(',', $xpath->evaluate('string(kml:Point/kml:coordinates)', $place, FALSE), 3);
        //$line = array_filter(preg_split('/\s/', $xpath->evaluate('string(kml:LineString/kml:coordinates)', $place, FALSE)));
        //$linearring = array_filter(preg_split('/\s/', $xpath->evaluate('string(kml:Polygon//kml:coordinates)', $place, FALSE)));

        $wkt = "";
        //height not processed
        if (count($point)>1) {
            $wkt = "POINT ({$point[0]} {$point[1]})";
        }
        elseif (count($line)>1) {
            $linea = array();
            foreach ($line as $co) {
                $coi = explode(",",$co);
                $linea[] = $coi[0].' '.$coi[1];
            }
            $l = join(',',$linea);
            $wkt = "LINESTRING ($l)";
        }
        elseif (count($linearring)>1) {
            //multilinear string not processed in this version!!
            $linea = array();
            foreach ($linearring as $co) {
                $coi = explode(",",$co);
                $linea[] = $coi[0].' '.$coi[1];
            }
            $l = join(',',$linea);
            $wkt = "POLYGON (($l))";
        }

        $name = mb_convert_encoding($place->{'name'}[0], 'UTF-8', 'auto');

        if ($out=='obj') {

            $data[] = array(
                //'name' => $xpath->evaluate('string(kml:name)', $place, FALSE),
                'name' => $name,
                'wkt' => $wkt
            );
        }
        elseif ($out=='csv') {
            //$data[] = '"'.$wkt.'","'.$xpath->evaluate('string(kml:name)', $place, FALSE).'"';
            $data[] = '"'.$wkt.'","'.$name.'"';

        }
    }
        //var_dump($result);
        /*foreach ($xml->Document->Placemark as $coord) {
                    var_dump($coord);
                    $coord = (string) $coord->Point->coordinates."<br/>";
                    $args     = explode(",", $coord);
                    $coords[] = array($args[0], $args[1], $args[2]);
        }
        print_r($coords);*/
    return $data;
}
/* read SQLite / spatialite files
 *
 * */
function readSQLITE($sqlite,$out,$file='',$header=false,$selectedLayer) {
    //only one layer supported actually!!!
    if (!command_exist('ogrinfo')) {
        log_action('ogrinfo not found',__FILE__,__LINE__);
        return false;
    }

    $data = array();
    $ret = exec("ogrinfo \"$sqlite\" |head -1|grep -o FAILURE");
#    $prjstring = exec("gdalsrsinfo \"$sqlite\" |grep -hoP 'PROJCS\[\"\w+'|sed -e 's/PROJCS\[\"//'");
#    if (in_array(chop($prjstring),array('HD72','HD72_EOV','HD_1972_Egyseges_Orszagos_Vetuleti','Hungarian_1972_Egyseges_Orszagos_Vetuleti'))) {
#        log_action("HD72 correction applied...",__FILE__,__LINE__);
#        $eov_srs = "-s_srs '+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +bounds=16.1200,45.7800,22.9100,48.6000'";
#    } else
#       $eov_srs = '';

    if ($ret!='') {
        log_action("read spatialite error: $ret",__FILE__,__LINE__);
        echo "<div class='error-message'>".str_shape_error."</div>"; //Ez itt vajon mit csinál?
        return;
    }
    if ($out=='csv') {
        // SQLite layer selection
        if (isset($_SESSION['upload_file']['activeSheet'])) {
            $selectedLayer = $_SESSION['upload_file']['activeSheet'];
            $selectedLayer = preg_replace('/([0-9]{1,5})(:\s)(.+)(\s)(\(.+)/','$3',$selectedLayer);
        }

        if ($selectedLayer=='') {
            $output = array();
            $ret = exec("ogrinfo \"$sqlite\"", $output);
            // we are expecting an array like this:
            // ["INFO: Open of `\/tmp\/ob_phpoxsikk\/adatok.sqlite'","using driver `SQLite' successful.","1: rtm (Multi Point)"....
            // using the first layer

            $list_of_layers = $_SESSION['upload_file']['sheetList'] = array_slice($output,2);
            // 1: rtm (Multi Point)
            $selectedLayer = preg_replace('/([0-9]{1,5})(:\s)(.+)(\s)(\(.+)/','$3',$output[2]);
            // rtm
        } else {
            $output = array();
            $ret = exec("ogrinfo \"$sqlite\"", $output);
            $_SESSION['upload_file']['sheetList'] = array_slice($output,2);
        }

        # eov settings for ogr2ogr
        if ($file=='')
        {
            //-dim 2 backward compatibility to do not process elevation
            //
            $fp = popen("ogr2ogr -f CSV /dev/stdout \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2", "r");
            #log_action("ogr2ogr -f CSV /dev/stdout $shp -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 $eov_srs");
        } else {
            // a sleep  lehet, hogy kell
            exec("ogr2ogr -f CSV $file \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2; sleep 1");
            #debug("ogr2ogr -f CSV $file \"$sqlite\" -t_srs EPSG:4326 -sql \"select * from $selectedLayer\" -lco GEOMETRY=AS_WKT -dim 2; sleep 1");
            $fp = fopen($file,"r");
        }
        if (!is_resource($fp)) {
            log_action("ogr2ogr -f CSV ... execution failed.",__FILE__,__LINE__);
            echo "<div class='error-message'>ogr2ogr -f CSV ... execution failed.</div>";
            return;
        }
        $data = array();
        while (!feof($fp)) {
            $l = fgets($fp);
            //karakter kódolási problémák miatt nem biztos, hogy itt kell kezelni, esetleg ki lehet vinni php-ból...
            /*mb_internal_encoding('utf8');
            mb_regex_encoding('utf8');
            $l = mb_ereg_replace('^("[A-Z]+) \(','\\1(',$l);*/
            $data[] = chop($l);
        }
        //log_action($data,__FILE__,__LINE__);
        //remove header!
        if (!$header)
            array_shift($data);

        if ($file=='')
            pclose($fp);
        else
            fclose($fp);
    }
    return $data;
}

# read a shape file
# args: shp path; output format; [output file]
# return:
# used: afuncs.php, read_upload_file.php
function readSHP($shp,$out,$file='',$header=false,$s_srs='') {
    //only one layer supported actually!!!
    //
    // check .prj exists! if not set -s_srs EPSG:4326
    //

    if (!command_exist('ogrinfo')) {
        log_action('ogrinfo not found',__FILE__,__LINE__);
        echo "<div class='error-message'>gdal-bin is missing!</div>";
        return false;
    }
    $err = exec("ogrinfo \"$shp\" |head -1|grep -o FAILURE");

    if ($err!='') {
        log_action("Read shape error: $ret",__FILE__,__LINE__);
        echo "<div class='error-message'>".str_shape_error."</div>";
        return false;
    }
    $err = exec("gdalsrsinfo \"$shp\" 2>&1");
    if (preg_match('/ERROR 1/',$err)) {

        if (!preg_match('/(\d+)$/',$s_srs,$m)) {
            log_action("Read shape error: $err",__FILE__,__LINE__);
            echo "<div class='error-message'>Can't transform coordinates, source layer has no coordinate system. Please add SRS info in 'Extra arguments' field, like [epsg:23700    ]</div>";
        }
    }

    $_SESSION['upload']['hd72-correction'] = 0;
    $eov_srs = '';
    $prjstring = exec("gdalsrsinfo \"$shp\" |grep -hoP 'PROJCS\[\"\w+'|sed -e 's/PROJCS\[\"//'");
    if (in_array(chop($prjstring),array('HD72','HD72_EOV','HD_1972_Egyseges_Orszagos_Vetuleti','Hungarian_1972_Egyseges_Orszagos_Vetuleti'))) {
        $_SESSION['upload']['hd72-correction'] = 1;
        log_action("HD72 correction applied...",__FILE__,__LINE__);
        $eov_srs = "-s_srs '+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +bounds=16.1200,45.7800,22.9100,48.6000'";
    }

    $data = array();

    // csv output
    if ($out=='csv') {
        if ( $s_srs != '' ) {
            if (preg_match('/(\d+)$/',$s_srs,$m)) {
                $s_srs = "-s_srs EPSG:".$m[1];
            }
        }
        $error = "";

        //-skipfailures
        # eov settings for ogr2ogr
        if ($file=='')
        {
            //-dim 2 backward compatibility to do not process elevation
            //
            $fp = popen("ogr2ogr -f CSV /dev/stdout \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs","r");
             //log_action("ogr2ogr -f CSV /dev/stdout \"$shp\" -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 $eov_srs");
        } else {
            // a sleep  lehet, hogy kell
            exec("ogr2ogr -f CSV $file \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs 2>&1",$retArr, $retVal);
            sleep(1);
            if ($retVal) {
                $error = $retArr[0];
                log_action($retArr[0],__FILE__,__LINE__);
                echo "<div class='error-message'>ogr2ogr failed: $error</div>";
                return false;
            } 
            //log_action("ogr2ogr -f CSV $file \"$shp\" -t_srs EPSG:4326 $s_srs -lco GEOMETRY=AS_WKT -dim 2 $eov_srs",__FILE__,__LINE__);
            $fp = fopen($file,"r");
        }
        if (!is_resource($fp)) {
            #log_action("ogr2ogr -f CSV ... execution failed.",__FILE__,__LINE__);
            echo "<div class='error-message'>ogr2ogr -f CSV ... execution failed.</div>";
            return false;
        }
        $data = array();
        while (!feof($fp)) {
            $l = fgets($fp);
            //karakter kódolási problémák miatt nem biztos, hogy itt kell kezelni, esetleg ki lehet vinni php-ból...
            /*mb_internal_encoding('utf8');
            mb_regex_encoding('utf8');
            $l = mb_ereg_replace('^("[A-Z]+) \(','\\1(',$l);*/
            if (chop($l)!='')
                $data[] = chop($l);
        }
        if (count($data)==0) {
            echo "<div class='error-message'>".str_shape_error.' Please check your shp file with this command: `ogr2ogr -f CSV teszt.csv "something.shp" -t_srs EPSG:4326 -lco GEOMETRY=AS_WKT -dim 2 '.$eov_srs.'`</div>';
            return false;
        }
        //remove header!
        if (!$header)
            array_shift($data);

        if ($file=='')
            pclose($fp);
        else
            fclose($fp);
    }
    // Postgres output
    if ($out=='pgsql') {
        $p = popen("shp2pgsql -I -s 4326 -W LATIN1 \"$shp\" upload_temp","r");
        $data = "";
        while ($l = fgets($p)){ $data[] = $l; }
        pclose($p);
    }

    // Shape data as memory object using a php lib:
    // https://packagist.org/packages/phpmyadmin/shapefile
    // it is an experimental code, not used anywhere!
    // should be installed...
    // composer require phpmyadmin/shapefile
    // To be able to read and write the associated DBF file, you need dbase extension:
    // pecl install dbase
    // echo "extension=dbase.so" > /etc/php5/conf.d/dbase.ini
    //
    if ($out=='obj') {

        require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
        $shp_obj = new ShapeFile(1);
        $shp_obj->loadFromFile($shp);
        $i = 1;
        $data = array();
        foreach ($shp_obj->records as $i => $record) {
            $shp_data = $record->SHPData;
            $dbf_data = $record->DBFData;
            $data[] = array($shp_data,$dbf_data);
        }
        # returning with a multidim.array
        # https://www.phpclasses.org/package/1741-PHP-Read-vectorial-data-from-geographic-shape-files.html
        /*include_once(getenv('OB_LIB_DIR').'ShapeFile.inc.php');
        $shp = new ShapeFile($shp, array('noparts'=>false));
    	$i = 0;
        $data = array();
        while ($record = $shp->getNext()) {
        	$dbf_data = $record->getDbfData();
	    	$shp_data = $record->getShpData();
	        $data[] = array($shp_data,$dbf_data);
        }*/
    }
    return $data;
}
/* read JSON file */
function readJSON ($inputFileName) {
    $file = new SplFileObject($inputFileName);
    $sheetData = array();

    $line = '';
    // Loop until we reach the end of the file.
    while (!$file->eof()) {
        // Echo one line from the file.
        $line .= $file->fgets();
    }
    $foo = (array) json_decode($line);

    $columns = array_keys($foo);
    $rowcount = 0;
    if (is_array($foo[$columns[0]]))
        $rowcount = count($foo[$columns[0]]);

    // transform array to an other array type:
    /*
     * read simple format
    {
        "asd": [1,2],
        "dsfg": [10,11],
        "fgh": [3,6]
    }
    write redundant format
    [
        {"asd":"1","dsgf":"10","fgh":"3"},
        {"asd":"2","dsgf":"11","fgh":"6"}
    ]
     */
    for($i=0;$i<$rowcount;$i++) {
        $put = array();
        foreach($foo as $key=>$value) {
            # {"asd":"1","dsgf":"10","fgh":"3"},
            if (is_array($value)) {
                $put[$key] = $value[$i];
            }
        }
        $sheetData[] = $put;
    }
    // Unset the file to call __destruct(), closing the file handle.
    $file = null;

    return $sheetData;
}

/* read Fasta file */
function readFASTA ($inputFileName,$sep) {
    $finfo = new finfo(FILEINFO_MIME);
    $mimetype = $finfo->file($inputFileName);
    $arr = preg_split('/;/',$mimetype);
    if(isset($arr[0]) and $arr[0]=='text/plain') {

    }
    $file = new SplFileObject($inputFileName);
    $sheetData = array();

    // Loop until we reach the end of the file.
    while (!$file->eof()) {
        // Echo one line from the file.
        $line = $file->fgets();
        // line processing should be coming from spcific modules!

        // Barbara's special put to a module!!!
        # >GWY_1_(reversed)
        if (preg_match('/^>(.+)/',$line,$m)) {
            $id_blocks = preg_split("/$sep/",$m[1]);
            //$triplecode = $individual_id = $other_things = null;
            /*
            if (count($m>=2)) {
                $triplecode = $m[1];
                $individual_id = $m[2];
                if (isset($m[3]))
                    $other_things = $m[3];
            }*/
            //$put = array('triplecode'=>$triplecode,'inidiv_id'=>$individual_id,'other_comm'=>$other_things);
            //
            $gi = count($id_blocks);
            $letter = 'A';
            for($i=0;$i<$gi;$i++) {
                $put[$letter] = $id_blocks[$i];
                ++$letter;
            }

            $sheetData[] = $put;
        }
    }
    // Unset the file to call __destruct(), closing the file handle.
    $file = null;
    return $sheetData;

}

/* readCSV*/
function readCSV ($inputFileName,$bom = null) {

    $finfo = new finfo(FILEINFO_MIME);

    $mimetype = $finfo->file($inputFileName);
    $arr = preg_split('/;/',$mimetype);
    if(isset($arr[0]) and $arr[0]=='text/plain') {
        # text, csv
        $charset = '';
        if(isset($arr[1])) {
            $brr = preg_split('/=/',$arr[1]);
            if (isset($brr[1])) $charset = $brr[1];
        }

        if (strtolower($charset) != 'utf-8' and $charset!='') {
            //$text = file_get_contents($_FILES["file"]["tmp_name"][$i]);
            //setlocale(LC_ALL, 'hu_HU.UTF8');
            //$ttext = iconv($charset, "UTF-8//IGNORE", $text);
            system("iconv -f $charset -t utf-8 $inputFileName > $inputFileName-utf8", $retval);
            if ($retval !== 0) {
                log_action('ICONV failed.',__FILE__,__LINE__);
                unlink("$inputFileName-utf8");
                $out = system("iconv -l | grep -i $charset");
                // unknown character set
                if ($out=="") {
                    // converting any to utf-8 but dropping unknown characters
                    // keep utf8 characters
                    log_action("ICONV: Silently discard characters that cannot be converted instead of terminating when encountering such characters.",__FILE__,__LINE__);
                    system("iconv -f utf8 -t utf-8 $inputFileName -c > $inputFileName-utf8", $retval);
                }
            }

            if ($charset == 'utf-16le') $bom = 'nobom';

            // delete UTF-8 BOM:
            if ($bom == 'nobom') {
                system("awk 'NR==1{sub(/^\xef\xbb\xbf/,\"\")}1' $inputFileName-utf8 > $inputFileName-utf8-nobom ");
                rename("$inputFileName-utf8-nobom","$inputFileName");
            }
            else
                rename("$inputFileName-utf8","$inputFileName");

            #ini_set('mbstring.substitute_character', "none");
            #$ttext= mb_convert_encoding($text, 'UTF-8',$charset);
            #$ttext = mb_convert_encoding($text, "UTF-8", mb_detect_encoding($text, "UTF-8, ISO-8859-1, ISO-8859-15", true));
            #file_put_contents($_FILES["file"]["tmp_name"][$i], $ttext);
        }

    }

    /** Include path **/
    set_include_path(get_include_path() . PATH_SEPARATOR . '../../includes/');

    /** parsecsv https://code.google.com/p/parsecsv-for-php */
    //include_once 'parsecsv.lib.php';
    //require 'parsecsv-for-php-master/parsecsv.lib.php';
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');

    if (file_exists($inputFileName) and filesize($inputFileName)) {
        # handling # comment lines at the beginning of csv files as file comment?

        //$csv = new parseCSV();
        $csv = new ParseCsv\Csv();
        //manual encoding & delimeter:
        //$csv->delimiter = "\t";
        $csv->encoding('UTF-8', 'UTF-8'); // UTF8 to UTF8 encoding can save some stupid files....
        //$csv->parse($inputFileName);

        //change header line feeding
        if(isset($_SESSION['feed_header_line']) and $_SESSION['feed_header_line']=='no')
            $csv->heading=FALSE;
        //useing: $csv->titles
        //auto-detect delimiter character
        $csv->auto($inputFileName);
        
        // Loop over the all rows
        $n = 0;
        foreach ($csv->data as $k) {
            // automatic WKT formatting: removing a space
            if (isset($k['WKT']))
                $csv->data[$n]['WKT'] = preg_replace('/^([A-Z]+) \(/','$1(',$k['WKT']);
            if (isset($_SESSION['na_values']) and count($_SESSION['na_values'])) {
                foreach ( $csv->data[$n] as $nk=>$nv) {
                    if (in_array($nv,$_SESSION['na_values'])) {
                        $csv->data[$n][$nk] = "";
                    }
                }
            }
            $n++;
        }
        $sheetData = $csv->data;

        $loop_update = 0;
        if (isset($_SESSION['character_encoding']) and preg_match('/(cp|windows-)?1252/i',$_SESSION['character_encoding'])) {
            $ascii = array();
            $ok = array();
            // további betvek kellhetnek!!!
            $ascii[251] = 'û'; //ű
            $ascii[245] = 'õ';
            $ascii[244] = 'ô';
            $ok[251] = 'ű'; //ű
            $ok[245] = 'ő'; //ő
            $ok[244] = 'ő'; //ő
            $loop_update = 1;
        }
        if (isset($_SESSION['decimal']) and $_SESSION['decimal']!='.') {
            $loop_update = 2;
        }
        if ($loop_update) {
            //$sheetData[0] = str_replace($ascii, $ok, $sheetData[0]);
            //print_r( $sheetData[0]);
            $cnt = count($sheetData);
            for($i=0;$i<$cnt;$i++) {
                foreach($sheetData[$i] as $key=>$val) {
                    if ($loop_update == 1)
                        $sheetData[$i][$key] = str_replace($ascii, $ok, $val);
                    else {
                        $sheetData[$i][$key] = preg_replace("/(\d+){$_SESSION['decimal']}(\d+)/",'$1.$2', $val);
                    }
                }
            }
        }
        return $sheetData;

        #$sheetData = parse_csv($inputFileName, array("delimiter"=>";"));
    } else {
        return 0;
    }
}
/* Create a temporay directory
 * */
function tempdir($dir=false,$prefix='ob_php') {
    $tempfile=tempnam(sys_get_temp_dir(),$prefix);
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}
//array walk internal function
//in results_builder.php
function prefix(&$item1, $key, $prefix)
{
    if (!preg_match("/^$prefix\./",$item1))
        $item1 = "$prefix.$item1";
}

/* Check given process is runnning
 * session based check would be desired?
 * */
function is_process_running($PID) {
    exec("ps $PID", $ProcessState);
    return(count($ProcessState) >= 2);
}
/* Run shell process in background
 * */
function run_in_background($Command, $Priority=0,$redirect='null') {
    if ($redirect==='null') $null = "2> /dev/null";
    else $null = "";

    if($Priority)
        $PID = shell_exec("nohup nice -n $Priority $Command $null & echo $!");
    else
        $PID = shell_exec("nohup $Command $null & echo $!");

    return($PID);
}
/* return given WKT string as geojson
 * */
function wkt_to_geojson ($text) {
    //set_include_path(get_include_path() . PATH_SEPARATOR . '../../libs/');
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once ('gisconverter.php');
    $decoder = new gisconverter\WKT();
    return $decoder->geomFromText($text)->toGeoJSON();
}
/* return given geojson as WKT string
 * */
function geojson_to_wkt ($text) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once ('gisconverter.php');
    $decoder = new gisconverter\GeoJSON();
    return $decoder->geomFromText($text)->toWKT();
}
function json_to_wkt($json) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    $geom = geoPHP::load($json,'json');
    if ($geom)
        return $geom->out('wkt');
    else
        return '';
}
function wkt_to_json($wkt) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    $geom = geoPHP::load($wkt,'wkt');
    if($geom)
        return $geom->out('json');
    else
        return '';
}
//wkt string validation
function is_wkt($wkt) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    try {
        $geom = geoPHP::load($wkt,'wkt');
        return true; # output geometry in GeoJSON format
    } catch (InvalidText $itex) {
        //"WKT not well formed";
        return false;
    } catch (Exception $ex) {
        //"General exception.";
        return false;
    }
}
//wkt string validation
function is_geojson($wkt) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    //include_once('geoPHP/geoPHP.inc');
    try {
        $geom = geoPHP::load($wkt,'json');
        return true; # output geometry in GeoJSON format
    } catch (InvalidText $itex) {
        //"WKT not well formed";
        return false;
    } catch (Exception $ex) {
        //"General exception.";
        return false;
    }
}
function language_file_process() {
    $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
    if(!isset($_SESSION['skip_local_lang'])) {
        $rr['err'][] = "Turn off the local language files first!";
        return $rr;
    }


    //$dest = sys_get_temp_dir()."/ob_lang_upl_".session_id();
    $r = move_upl_files($_FILES['files']);
    $rj = json_decode($r);

    $files = (array) $rj->{'file_names'};

    #{"file_names":["hu.csv"],"err":[],"sum":["0065e0e713d4e2e230a2fc72da81f21719dc753f"],"tmp":"\/tmp\/ob_phpvMsJM1\/","type":["text\/plain; charset=us-ascii"]}
    for($i=0;$i<count($files);$i++) {
        $name = $files[$i];
        $file = $rj->{'tmp'}.$files[$i];

        if($s = readCSV($file)) {
            // file name
            $m = array();
            if(preg_match('/(\w+)\.csv/',$name,$m)) {
                $filename = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$m[1]);
            } else {
                $rr['err'][] = "File name should be xx.csv, where xx is the languge code!";
                return $rr;
            }

            // check lang_code turned off
            if(!isset($_SESSION['skip_local_lang'][$m[1]])) {
                $rr['err'][] = "Turn off the local $m[1] language defintion first!";
                return $rr;
            }

            // file content
            $out = array();
            foreach ($s as $line) {
                //echo $line['definition'].' '.$line['value'];
                if(!isset($line['definition'])) {
                    $wrongline = array_values($line);
                    $line = array();
                    $line['definition'] = $wrongline[0];
                    $line['value'] = $wrongline[1];
                }
                if(!isset($line['value'])) {
                    $wrongline = array_values($line);
                    $line = array();
                    $line['definition'] = $wrongline[0];
                    $line['value'] = $wrongline[1];
                }
                $d = trim($line['definition'],"'");
                $d = trim($d,'"');
                $v = trim($line['value'],"'");
                $v = trim($v,'"');

                if (trim($d)!='') {
                    if(defined($d)) $rr['err'][] = "$d already defined, skipped!";
                    else $out[trim($d)] = $v;
                }
            }
            if (!count($out)) {
                $rr['err'][] = "Empty processed array, might be wrong csv syntax!";
                return $rr;
            }

            // create language definition file
            if(!create_define_file($filename,$out)) {
                $rr['err'][] = "File creating error!";
                return $rr;
            }
            unset($_SESSION['skip_local_lang']);
            $rr['file_names'][] = sprintf("local %2s language defintions created",$m[1]);
        }
        else $rr['err'][] = "Read csv error!";
    }
    return $rr;
}
/* Move uploaded files to destination
 * Returning: an array: 'temp dir name','file name','errors'
 * should call after: rm tmp dir!!
 * */
function move_upl_files($F,$dest='') {
    //Check disk free space...
    //disk_free_space ( string $directory )
    $r = array('file_names'=>array(),'err'=>array(),'sum'=>array(),'tmp'=>'','type'=>array(),'warn'=>array());
    if ($dest=='') {
        $t = tempdir();
        if (!$t) {
            $r['err'][] = "Could not create temp directory: $t";
            log_action("Could not create temp directory: $t",__FILE__,__LINE__);
            return json_encode($r);
        }
    } else {
        if ( !file_exists($dest) )
            mkdir($dest);

        if ( is_dir($dest) and is_writable($dest) ) {
            $t = $dest;
        } else {
            $r['err'][] = "Destination directory is not exist or not writable: $dest";
            log_action("Destination directory is not exist or not writable: $dest",__FILE__,__LINE__);
            return json_encode($r);
        }
    }
    $t = "$t/";
    $r['tmp'] = $t;

    $ALLOWED_FILE_SIZE = defined('ALLOWED_FILE_SIZE') ? ALLOWED_FILE_SIZE : ini_get('upload_max_filesize');
    if (preg_match('/(\d+)M$/',$ALLOWED_FILE_SIZE,$m)) {
        $ALLOWED_FILE_SIZE = $m[1] * 1024 * 1024;
    }

    if ($ALLOWED_FILE_SIZE < 1024) {
        log_action("ALLOWED_FILE_SIZE less than 1KB which is meaningless and might be cannot be applied!", __FILE__, __LINE__);
    }

    if (isset($F["tmp_name"]) and is_array($F["tmp_name"])) {
        for ($i=0;$i<count($F["tmp_name"]);$i++) {

            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file($F['tmp_name'][$i]);
            $r['type'][] = $mimetype;

            $sha1 = sha1_file($F['tmp_name'][$i]);
            $r['sum'][] = $sha1;
            $cnam = check_file_exist($F['name'][$i],$t,$F['tmp_name'][$i],$sha1);
            if ($cnam) {
                if (is_uploaded_file($F["tmp_name"][$i])) {

                    // Auto resize large photo if its size does not exceeds the php limit but larger than the project limit.
                    if (filesize($F["tmp_name"][$i]) > $ALLOWED_FILE_SIZE) {
                         
                        $arr = preg_split('/;/',$mimetype);
                        if (isset($arr[0]) and preg_match('/image\/.+/',$arr[0])) {
                            list($width, $height, $type, $attr) = getimagesize($F["tmp_name"][$i]);
                            $step = $width/10;
                            $image = imagecreatefromstring(file_get_contents($F['tmp_name'][$i]));
                            if ($image != FALSE) {
                                $err = 1;
                                for ($i=1;$i<10;$i++) {
                                    $resized_width = $width - $step*$i;
                                    $imgResized = imagescale($image , $resized_width);
                                    imagejpeg($imgResized, "$t".$cnam, 100);
                                    imagedestroy($imgResized);
                                    if (filesize("$t".$cnam) < $ALLOWED_FILE_SIZE) {
                                        $r['file_names'][] = $cnam;
                                        $err = 0;
                                        break;
                                    } else {
                                        unlink("$t".$cnam);
                                    }
                                }
                                if ($err) {
                                    $r['err'][] = "Move file to desnitaion error: ".$F['name'][$i].".";
                                }
                            }
                            imagedestroy($image);
                        }
                    } else {
                    
                        if ( move_uploaded_file($F["tmp_name"][$i], "$t".$cnam)) {
                            $r['file_names'][] = $cnam;
                        } else {

                            $r['err'][] = "Move file to desnitaion error: ".$F['name'][$i].".";
                            /*$e = '';
                            switch ($F['error'][$i]) {
                                case UPLOAD_ERR_OK:
                                    $e = 'Ok.';
                                    break;
                                case UPLOAD_ERR_NO_FILE:
                                    $e = 'No file sent.';
                                    break;
                                case UPLOAD_ERR_INI_SIZE:
                                    $e = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                                    break;
                                case UPLOAD_ERR_FORM_SIZE:
                                    $e = 'Exceeded filesize limit.';
                                    break;
                                default:
                                    $e = 'Unknown errors.';
                                    break;
                            }
                            $r['err'][] = "Can't move to desnitaion '".$F['name'][$i]."' - $e";
                            */
                        }
                    }
                } else {
                    // unpacked or other way placed files /e.g. reloaded page/
                    // tmp_dir basename check!!!
                    if (copy($F["tmp_name"][$i], "$t".$cnam)) {
                        unlink($F["tmp_name"][$i]);
                        $r['file_names'][] = $F['name'][$i];
                    } else
                        $r['err'][] = "Can't copied file to desnitaion.";
                }
            } else {
                $r['file_names'][] = $F['name'][$i];
                //$r['err'][] = "File already exists";
                $r['warn'][] = "File already exists";
            }
        }
    } else if (isset($F["tmp_name"]) and $F["tmp_name"]!='') {
        $sha1 = sha1_file($F['tmp_name']);
        $r['sum'][] = $sha1;

        $finfo = new finfo(FILEINFO_MIME);
        $mimetype = $finfo->file($F["tmp_name"]);
        $r['type'][] = $mimetype;
        $cnam = check_file_exist($F['name'],$t,$F['tmp_name'],$sha1);
        if ($cnam) {

            // Auto resize large photo if its size does not exceeds the php limit but larger than the project limit.
            if (filesize($F["tmp_name"]) > $ALLOWED_FILE_SIZE) {
                $arr = preg_split('/;/',$mimetype);

                if (isset($arr[0]) and preg_match('/image\/.+/',$arr[0])) {
                    list($width, $height, $type, $attr) = getimagesize($F["tmp_name"]);
                    $step = $width/10;
                    $image = imagecreatefromstring(file_get_contents($F['tmp_name']));
                    if ($image != FALSE) {
                        $err = 1;
                        for ($i=1;$i<10;$i++) {
                            $resized_width = $width - $step*$i;
                            $imgResized = imagescale($image , $resized_width);
                            imagejpeg($imgResized, "$t".$cnam, 100);
                            imagedestroy($imgResized);
                            if (filesize("$t".$cnam) < $ALLOWED_FILE_SIZE) {
                                $r['file_names'][] = $cnam;
                                $err = 0;
                                break;
                            } else {
                                unlink("$t".$cnam);
                            }
                        }
                        if ($err) {
                            $r['err'][] = "Move file to desnitaion error: ".$F['name'].".";
                        }
                    }
                    imagedestroy($image);
                }
            } else {
                if (move_uploaded_file($F["tmp_name"], "$t".$cnam)) {
                    $r['file_names'][] = $cnam;
                } else {
                    $r['err'][] = "Move file to desnitaion error: ".$F['name'].".";
                }
            }
        } else {
            $r['file_names'][] = $F['name'];
            //$r['err'][] = "File already exists";
            $r['warn'][] = "File already exists";
        }
    } else {
        $r['err'][] = "File upload error";
    }
    return json_encode($r);
}
function check_file_exist($origin, $dest, $tmp_name, $tmp_sum)
{
    $lc = new LocaleManager();
    $lc->doBackup();
    $lc->fixLocale();
    $origin = basename($origin);
    $lc->doRestore();
    $fulldest = $dest.$origin;
    $filename = $origin;

    if (file_exists($fulldest) and sha1_file($fulldest) == $tmp_sum) {
        return 0;
    }
    mb_internal_encoding("UTF-8");
    for ($i=1; file_exists($fulldest); $i++)
    {
        $fileext = (mb_strpos($origin,'.')===false?'':'.'.mb_substr(strrchr($origin, "."), 1));
        $filename = mb_substr($origin, 0, mb_strlen($origin)-mb_strlen($fileext)).'['.$i.']'.$fileext;
        $fulldest = $dest.$filename;
        if (file_exists($fulldest) and sha1_file($fulldest) == $tmp_sum) {
            return $filename;
        }
    }
    return $filename;
}
class LocaleManager
{
    /** @var array */
    private $backup;


    public function doBackup()
    {
        $this->backup = array();
        $localeSettings = setlocale(LC_ALL, 0);
        if (strpos($localeSettings, ";") === false)
        {
            $this->backup["LC_ALL"] = $localeSettings;
        }
        // If any of the locales differs, then setlocale() returns all the locales separated by semicolon
        // Eg: LC_CTYPE=it_IT.UTF-8;LC_NUMERIC=C;LC_TIME=C;...
        else
        {
            $locales = explode(";", $localeSettings);
            foreach ($locales as $locale)
            {
                list ($key, $value) = explode("=", $locale);
                $this->backup[$key] = $value;
            }
        }
    }


    public function doRestore()
    {
        foreach ($this->backup as $key => $value)
        {
            setlocale(constant($key), $value);
        }
    }


    public function fixLocale()
    {
        setlocale(LC_ALL, "C.UTF-8");
    }
}
function check_file_uploaded_name ($filename)
{
    (bool) ((preg_match("`^[-0-9A-Z_\.]+$`i",$filename)) ? true : false);
}
function check_file_uploaded_length ($filename)
{
    return (bool) ((mb_strlen($filename,"UTF-8") > 225) ? true : false);
}
/**
 * return the translated string 
 */
function tr($str) {
    return defined($str) ? constant($str) : $str;
}

/* utf8 ucfirst
 * glue arbitray arguments into a string
 * */
function t() {
    $encoding='UTF-8';
    mb_internal_encoding($encoding);
    $s = func_get_args();
    $b = array_map('t_map',$s);
    $string = implode(' ',$b);
    if ($string=='') {
        return "<i>".str_undefined."</i>";
    }
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    if ($firstChar!='')
        return mb_strtoupper($firstChar, $encoding) . $then;
    else {
        if (is_array($s)) return "";
        else return $s;
    }
}
function t_map($s) {

    if (preg_match('/^str_/',$s)) {
        if (defined($s))
            return constant($s);
        else {
            $s = mb_eregi_replace('^str_','::',$s);
            return $s;
        }
    } else
        return $s;
}
/* Validate JSON
 * used in geomtest.php
 * */

use Seld\JsonLint\JsonParser;
function is_json($str,$sendError=false) {
    
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    $parser = new JsonParser;
    $e = $parser->lint($str);
    if (is_null($e)) return true;
    else {
        if ($sendError) {
            $k = $e->getMessage();
            return $k;
        }
    }
    return false;
    //return json_decode($str) != null;
}
// pretty json format
// https://gist.github.com/GloryFish/1045396
function format_json($json, $html = false, $tabspaces = null) {
    $tabcount = 0;
    $result = '';
    $inquote = false;
    $ignorenext = false;

    if ($html) {
        $tab = str_repeat("&nbsp;", ($tabspaces == null ? 4 : $tabspaces));
        $newline = "<br/>";
    } else {
        $tab = ($tabspaces == null ? "\t" : str_repeat(" ", $tabspaces));
        $newline = "\n";
    }

    for($i = 0; $i < strlen($json); $i++) {
        $char = $json[$i];

        if ($ignorenext) {
            $result .= $char;
            $ignorenext = false;
        } else {
            switch($char) {
                case ':':
                    $result .= $char . (!$inquote ? " " : "");
                    break;
                case '{':
                    if (!$inquote) {
                        $tabcount++;
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case '}':
                    if (!$inquote) {
                        $tabcount--;
                        $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case ',':
                    if (!$inquote) {
                        $result .= $char . $newline . str_repeat($tab, $tabcount);
                    }
                    else {
                        $result .= $char;
                    }
                    break;
                case '"':
                    $inquote = !$inquote;
                    $result .= $char;
                    break;
                case '\\':
                    if ($inquote) $ignorenext = true;
                    $result .= $char;
                    break;
                default:
                    $result .= $char;
            }
        }
    }
    return $result;
}
//validate session with projecttable
function vreq() {
    if (isset($_SESSION['token'])) {
        if (isset($_SESSION['token']['projecttable']) and $_SESSION['token']['projecttable']==PROJECTTABLE)
            if (isset($_SESSION['token']['session_id']) and $_SESSION['token']['session_id']==session_id())
                return true;
    }
    return false;
}
/* http://php.net/manual/en/function.php-check-syntax.php
 *
 * */
function php_check_syntax( $php, &$error_message, $isFile=false )
{
    # Get the string tokens
    $tokens = token_get_all( '<?php '.trim( $php  ));

    # Drop our manually entered opening tag
    array_shift( $tokens );
    token_fix( $tokens );

    # Check to see how we need to proceed
    # prepare the string for parsing
    if( isset( $tokens[0][0] ) && $tokens[0][0] === T_OPEN_TAG )
       $evalStr = $php;
    else
        $evalStr = "<?php\n{$php}?>";

    if( $isFile OR ( $tf = tempnam( NULL, 'parse-' ) AND file_put_contents( $tf, $php ) !== FALSE ) AND $tf = $php )
    {
        # Prevent output
        $phpbin = (defined('PHP_PATH')) ? constant('PHP_PATH') : '/usr/bin/php';
        $error_message = shell_exec($phpbin.' -l "'.$php.'"');

        if( !preg_match('/^No syntax/',$error_message))
        {
            return false;
        }
        return true;
    }
    return false;
}

//fixes related bugs: 29761, 34782 => token_get_all returns <?php NOT as T_OPEN_TAG
function token_fix( &$tokens ) {
    if (!is_array($tokens) || (count($tokens)<2)) {
        return;
    }
   //return of no fixing needed
    if (is_array($tokens[0]) && (($tokens[0][0]==T_OPEN_TAG) || ($tokens[0][0]==T_OPEN_TAG_WITH_ECHO)) ) {
        return;
    }
    //continue
    $p1 = (is_array($tokens[0])?$tokens[0][1]:$tokens[0]);
    $p2 = (is_array($tokens[1])?$tokens[1][1]:$tokens[1]);
    $p3 = '';

    if (($p1.$p2 == '<?') || ($p1.$p2 == '<%')) {
        $type = ($p2=='?')?T_OPEN_TAG:T_OPEN_TAG_WITH_ECHO;
        $del = 2;
        //update token type for 3rd part?
        if (count($tokens)>2) {
            $p3 = is_array($tokens[2])?$tokens[2][1]:$tokens[2];
            $del = (($p3=='php') || ($p3=='='))?3:2;
            $type = ($p3=='=')?T_OPEN_TAG_WITH_ECHO:$type;
        }
        //rebuild erroneous token
        $temp = array($type, $p1.$p2.$p3);
        if (version_compare(phpversion(), '5.2.2', '<' )===false)
            $temp[] = isset($tokens[0][2])?$tokens[0][2]:'unknown';

        //rebuild
        $tokens[1] = '';
        if ($del==3) $tokens[2]='';
        $tokens[0] = $temp;
    }
    return;
}
/* proxy or wfs...
 * not used?
 * */
function getUrlContents($url,$file1)
{
   $url_parsed = parse_url($url);

   $host = $url_parsed["host"];
   if ($url == '' || $host == '') {
       return false;
   }
   $port = 80;
   //$path = (empty($url_parsed["path"]) ? '/' : $url_parsed["path"]);
   //$path.= (!empty($url_parsed["query"]) ? '?'.$url_parsed["query"] : '');
   $out = "GET $path HTTP/1.0\r\nHost: $host\r\nConnection: Close\r\n\r\n";
   $fp = fsockopen($host, $port, $errno, $errstr, 30);
   fwrite($fp, $out);
   $headers = '';
   $content = '';
   $buf = '';
   $isBody = false;
   while (!feof($fp) and !$isBody) {
          $buf = fgets($fp, 1024);
          if ($buf == "\r\n" ) {$isBody = true;}
          else{$headers .= $buf;}
   }
   //$file1 = fopen(basename($url_parsed["path"]), 'w');
   $bytes=stream_copy_to_stream($fp,$file1);
   fclose($fp);
   return $bytes;
}
/* news stream..
 * to be deprecated
 * */
function insertNews($message,$level,$sender=0,$receiver=0) {
    global $BID;
    if ($level=='project') $l = 'project';
    elseif ($level=='public') $l = 'public';
    elseif ($level=='personal') $l = 'personal';
    else return;

    $cmd = sprintf("INSERT INTO project_news_stream (datum,news,uploader,project_table,level,receiver) VALUES(NOW(),%s,%d,'%s','%s',%d) RETURNING id",quote("$message"),$sender,PROJECTTABLE,$l,$receiver);
    pg_query($BID,$cmd);
    // no response
}
function rainbow($start=array(0,0,0),$end=array(0,0,0),$steps=1) {

    $colors=array(hexcolorFromArraycolor($start)); //You want the start color to be part of the result array
    $intervals=$steps-1; //You want 3 steps to mean 2 intervals

    $current=$start;
    if ($intervals>0) $delta=array(
       ($end[0]-$start[0])/$intervals,
       ($end[1]-$start[1])/$intervals,
       ($end[2]-$start[2])/$intervals);
    else $delta = array($end[0]-$start[0],$end[1]-$start[1],$end[2]-$start[2]);

    for ($i=1;$i<$intervals;$i++) {
        $current=array($current[0]+$delta[0],$current[1]+$delta[1],$current[2]+$delta[2]);
        $colors[]=hexcolorFromArraycolor(array(round($current[0]),round($current[1]),round($current[2])));
    }

    $colors[]=hexcolorFromArraycolor($end); //You want the end color to be part of the result array
    return $colors;
    //return hexcolorFromArraycolor($colors);
}
function hexcolorFromArraycolor($arraycolor) {
    return '#'
    .substr('0'.dechex($arraycolor[0]),-2)
    .substr('0'.dechex($arraycolor[1]),-2)
    .substr('0'.dechex($arraycolor[2]),-2);
}
function mkThumb($image,$thumbWidth,$thumbHeight=0) {
    # create or not a thumbnail from an image
    #
    $photo_folder = getenv('PROJECT_DIR').'local/attached_files';
    $thumbnail_folder = getenv('PROJECT_DIR').'local/attached_files/thumbnails';

    $pattern = sprintf("%s/%dx[0-9]+_%s*",$thumbnail_folder,$thumbWidth,$image);
    $list = glob($pattern);
    foreach ($list as $l) {
        #should be localized
        #$lc = new LocaleManager();
        #$lc->doBackup();
        #$lc->fixLocale();
        return basename($l);
        #$lc->doRestore();
    }
    // get extension
    $is_image_by_ext = 0;
    $ext = '';
    if (preg_match('/\.(\w{1,4})$/i',$image,$m)) {
        $images = array('jpg','jpeg','gif','png');
        $ext = strtolower($m[1]);
        if (in_array(strtolower($m[1]),$images)) $is_image_by_ext = 1;
    }

    //check is this processable image?
    if($is_image_by_ext) {
        if(false !== (list($width,$height) = @getimagesize($photo_folder."/".$image))){
            if ($thumbHeight==0) {
                $new_width = $thumbWidth;
                $new_height = floor( $height * ( $thumbWidth / $width ) );
            } else {
                $new_height = $thumbHeight;
                $new_width = floor( $width * ( $thumbHeight / $height ) );
            }

            $output = sprintf("%s/%dx%d_%s",$thumbnail_folder,$new_width,$new_height,$image);
            if (file_exists($output))
                return sprintf("%dx%d_%s",$new_width,$new_height,$image);

            if($ext == 'jpg' or $ext == 'jpeg')
                $source = open_image($photo_folder."/".$image);
            elseif($ext == 'png')
                $source = open_image($photo_folder."/".$image);
            elseif($ext == 'gif')
                $source = open_image($photo_folder."/".$image);

            if ($source!==false) {
                $thumb = imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($thumb,$source,0,0,0,0,$new_width,$new_height,$width,$height);
                imagejpeg($thumb,$output);
                imagedestroy($thumb);
                return sprintf("%dx%d_%s",$new_width,$new_height,$image);
            } else
                return false;
        }
    } else {
        //not an image -- ?pdf
        if ($ext=='pdf') {
            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file($photo_folder."/".$image);
            if (preg_match('/application\/pdf/',$mimetype)) {
                if ($thumbWidth==600) {
                    $new_width = 400;
                    $new_height = 600;
                } else {
                    $new_width = 60;
                    $new_height = 60;
                }
                // php 7.2 fails to use...
                if (class_exists('imagick')) {
                    $im = new imagick();
                    $im->setResolution(100, 100);
                    //$im->setSize($new_width, $new_height);

                    try {
                        if ($im->readImage($photo_folder."/".$image.'[0]')) {
                            $im->thumbnailImage($new_width , $new_height , TRUE);
                            $im->setBackgroundColor(new ImagickPixel('white'));
                            $im->setImageFormat('jpg');
                            $output = sprintf("%s/%dx%d_%s.jpg",$thumbnail_folder,$new_width,$new_height,$image);
                            if (file_exists($output)) return sprintf("%dx%d_%s.jpg",$new_width,$new_height,$image);
                            $im->writeImage($output);
                            return sprintf("%dx%d_%s.jpg",$new_width,$new_height,$image);
                        }
                    } catch (Exception $e) {
                        log_action($e->getMessage());
                    }
                }
            }
        }
    }
    return false;
}
function open_image ($file) {
    $size = getimagesize($file);
    switch($size["mime"]){
        case "image/jpeg":
            $im = imagecreatefromjpeg($file); //jpeg file
            break;
        case "image/gif":
            $im = imagecreatefromgif($file); //gif file
            break;
        case "image/png":
            $im = imagecreatefrompng($file); //png file
            break;
        default:
            $im=false;
            break;
    }
    return $im;
}

function save_image($image, $file) {
    $size = getimagesize($file);
    switch($size["mime"]){
        case "image/jpeg":
            $out_i = imagejpeg($image, $file, 100); //jpeg file
            break;
        case "image/gif":
            $out_i = imagegif($image, $file, 9); //gif file
            break;
        case "image/png":
            $out_i = imagepng($image, $file); //png file
            break;
        default:
            $out_i=false;
            break;
    }
    return $out_i;
}

function mime_icon ($file,$size) {

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $path = '/images/Free-file-icons-master/';
    $icon_ext = "png";

    //default icon: none
    $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,32,'_blank.png');
    $icon = sprintf('%1$s.%2$s','_blank',$icon_ext);
    if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
        $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);

    if (preg_match('/\.(docx)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(doc)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(odt)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(odp)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','ppt',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xls)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(ods)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xlsx)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(txt)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(csv)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','txt',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(pdf)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(zip)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(tar)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','tgz',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(gz)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s','tgz',$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(tgz)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(wav)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(mp4)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(mp3)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(avi)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(sql)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    } elseif (preg_match('/\.(xml)$/i',$file,$m)) {
        $icon = sprintf('%1$s.%2$s',strtolower($m[1]),$icon_ext);
        if (file_exists(sprintf("%s%s%s%spx/%s",OB_ROOT,'projects/'.PROJECTTABLE.'/',$path,$size,$icon)))
            $src = sprintf("$protocol://%s/%s%spx/%s",URL,$path,$size,$icon);
    }

    return $src;
}
/* create database lines for attached files
 * it processing arrays
 *
 * */
function file_upload_process($list,$sum,$mimetype) {
    global $ID,$GID;

    pg_query($GID,'SET search_path TO system,public');

    $root_dir = getenv('PROJECT_DIR').'local/attached_files';

    $done = array();
    $comment = '';
    $sessionid = session_id();
    if(isset($_SESSION['Tid'])) {
        $t = $_SESSION['Tid'];
    }
    else {
        $t = 0;
    }

    $lc = new LocaleManager();
    $lc->doBackup();
    $lc->fixLocale();
    for($i=0;$i<count($list);$i++) {
        $ref = $list[$i];
        $exif = array();

        if (in_array(exif_imagetype("$root_dir/$ref"),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM))) {
            $exif_read = exif_read_data("$root_dir/$ref", 0, false);
            if (json_encode($exif_read)) {
                $exif = $exif_read;
                /* automatic rotation
                 *
                 * if (isset($exif_read['COMPUTED'])) {
                    if (isset($exif_read['COMPUTED']['Orientation'])) {
                        $exif_orientation = $exif_read['COMPUTED']['Orientation'];
                        $eit = exif_imagetype("$root_dir/$ref");

                        switch($exif_orientation) {
                            case 3:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, 180, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                            case 6:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, -90, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                            case 8:
                                if ($eit == IMAGETYPE_JPEG) {
                                    $im = @imagecreatefromjpeg("$root_dir/$ref");
                                    $image_p = imagerotate($im, 90, 0);
                                    imagejpeg($im, "$root_dir/$ref");
                                }
                                break;
                        }
                    }
                }*/
            } else
                $exif = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
        } else {
            $a = exec("mediainfo $root_dir/$ref",$output);
            $n = array();
            foreach ($output as $o) {
                $on = preg_split("/\s+: /",$o);
                if(count($on)>1) {
                    if($on[0]=='Complete name') {
                        $on[1] = basename($on[1]);
                    }
                    $n[$on[0]] = $on[1];
                }
            }
            $exif = $n;
        }

        $cmd = sprintf("SELECT status,id,reference FROM system.files WHERE reference=%s AND sum=%s AND project_table='%s'",quote($ref),quote($sum[$i]),PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        if (!pg_num_rows($res)) {
            if (isset($_SESSION['current_query_table']))
                $data_table = $_SESSION['current_query_table'];
            else
                $data_table = PROJECTTABLE;

            if (isset($_SESSION['current_query_schema']))
                $data_schema = $_SESSION['current_query_schema'];
            else
                $data_schema = 'public';

            $cmd = sprintf("INSERT INTO system.files (project_table,reference,comment,datum,access,user_id,status,sessionid,sum,mimetype,data_table,exif,project_schema) 
                VALUES('%s',%s,%s,NOW(),0,%d,%s,'%s',%s,%s,%s,%s,%s) 
                RETURNING id",PROJECTTABLE,quote($ref),quote($comment),$t,quote('progress'),$sessionid,quote($sum[$i]),quote($mimetype[$i]),quote($data_table),quote(json_encode($exif)),quote($data_schema));
            $res = pg_query($GID,$cmd);
            if (pg_last_error($GID)){
                $lc->doRestore();
                log_action(pg_last_error($GID));
                return false;
            } else {
               $done[] = $ref;
            }
        } else {
            $row = pg_fetch_assoc($res);
            if ($row['status']=='progress') {
                // file already exists and might be uploded recently
                $done[] = $row['reference'];
            }
            elseif ($row['status']=='valid') {
                // file already exists and connected previosuly
                // the file owner should be warned that the same file is used again?
                // something else management stpes?
                $done[] = $row['reference'];
            }
            elseif ($row['status']=='deleted') {
                // file has been deleted and now reuploaded
                // it the same file - sha1_file()
                $cmd = sprintf("UPDATE system.files SET status='progress' WHERE id='%d'",$row['id']);
                $res = pg_query($GID,$cmd);
                $done[] = $ref;
            }
        }
    }

    $lc->doRestore();
    return $done;
}
/* create connections for files
 * returning conid
 * */
function file_connection_process($list,$conid,$table,$sessionid,$rownum) {
    global $ID;

    $connect_list = array_unique($list);
    array_filter($connect_list);

    $first_file = 0;

    $conid_container = array();

    //$sessionid = session_id();

    foreach ($connect_list as $cl) {
        if (!$cl) continue;
        //ha minden felsorolt elem már megvan a kapcsolat táblában és egy azonosítóval van összekötve
        //$cmd = sprintf("SELECT DISTINCT conid,count(*) FROM system.file_connect WHERE conid>0 AND file_id IN (SELECT id FROM files WHERE reference IN (%s) and project_table='%s' %s) GROUP BY conid",implode(',',array_map('quote',$connect_list)),PROJECTTABLE,$filter);

        $file_id = 0;
        $cmd = sprintf('SELECT id FROM system.files WHERE reference=%s AND project_table=%s',quote($cl),quote($table));
        //log_action($cmd,__FILE__,__LINE__);
        $res1 = pg_query($ID,$cmd);
        if (pg_num_rows($res1)) {
            $row1 = pg_fetch_assoc($res1);
            $file_id = $row1['id'];
        }
        if (!$file_id) continue;

        if (!$conid) {
            if (!$sessionid) {
                $sessionid = session_id();
            }
            // upload_funcs.php
            // update_fields.php
            // clean current conid in new upload
            $cmd = sprintf('DELETE FROM system.file_connect WHERE temporal=true AND sessionid=\'%s\' AND rownum=%d',$sessionid,$rownum);
            pg_query($ID,$cmd);

            // ganerate short hash from available strings
            // using it instead of numeric conid
            #$conid = crypt($file_id.$rownum.microtime(),$sessionid);
            $conid = uniqid($_SESSION['Tid']."$",true);

            $cmd = sprintf("INSERT INTO system.file_connect (conid,file_id,sessionid,rownum,temporal) VALUES ('%s',%d,'%s',%d,false) RETURNING conid",$conid,$file_id,$sessionid,$rownum);
            pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action(pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }
        } else {
            if (!$sessionid) {
                $sessionid = session_id();
            }

            // update_fields.php
            $cmd = sprintf("INSERT INTO system.file_connect (conid,file_id,sessionid,rownum,temporal) VALUES ('%s',%d,'%s',%d,false)",$conid,$file_id,$sessionid,$rownum);
            pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action(pg_last_error($ID),__FILE__,__LINE__);
                return false;
            }
        }
    }
    return $conid;
}
//file_connect sub function
// NOT USED !!
function create_new_connectid_with_elements($connect_list,$filter) {
    global $GID;

    $f = array_pop($connect_list);
    $cmd = sprintf("INSERT INTO system.file_connect (file_id) SELECT id FROM files WHERE reference=%s AND project_table='%s' %s RETURNING conid",quote($f),PROJECTTABLE,$filter);
    $res = pg_query($GID,$cmd);
    $row = pg_fetch_assoc($res);
    $conid = $row['conid'];
    if($conid)
        add_elements_to_connectid($conid,$connect_list,$filter);
    return $conid;
}
//file_connect sub function
// NOT USED !!!
function add_elements_to_connectid($conid,$connect_list,$filter) {
    global $GID;
    foreach ($connect_list as $e) {
        $cmd = sprintf("INSERT INTO system.file_connect (file_id,conid) SELECT id,%d FROM files WHERE reference=%s AND project_table='%s' %s RETURNING conid",$conid,quote($e),PROJECTTABLE,$filter);
        $res = pg_query($GID,$cmd);
    }
}
/* Profile function
 * change own email address
 * This called, when user confirm it by follow an url
 * */
function change_email($code) {
    global $BID;
    if (!isset($_SESSION['Tid'])) return false;

    if (isset($_SESSION['emailchange']) and $_SESSION['emailchange']['code']==$code and $code!='') {
        $cmd = sprintf("UPDATE \"public\".\"users\" SET \"email\"=%s WHERE id='{$_SESSION['Tid']}'",quote(strtolower($_SESSION['emailchange']['addr'])));
        unset($_SESSION['emailchange']);
        $res = pg_query($BID,$cmd);
        if(pg_affected_rows($res)) {
            return true;
        }
        else return pg_last_error($BID);
    }
    return false;
}
/* Profile function
 * drop my profile
 * */
function drop_profile($code) {
    global $BID;
    if (!isset($_SESSION['Tid'])) return false;
    if (isset($_SESSION['drop_my_profile']) and $_SESSION['drop_my_profile']['code']==$code and $code!='') {
        $cmd = sprintf('DELETE FROM "users" WHERE id=%d AND "user"=%s',$_SESSION['Tid'],quote($_SESSION['drop_my_profile']['profile']));
        $res = pg_query($BID,$cmd);
        if(pg_affected_rows($res)) {
            return true;
        }
    }
    log_action( pg_last_error($BID),__LINE__,__FILE__ );
    return false;
}
### PHP compatibility before php 5.5
if(!function_exists("array_column"))
{

    function array_column($array,$column_name)
    {

        return array_map(function($element) use($column_name){return $element[$column_name];}, $array);

    }

}

/* upload - file upload, pager
 * perpage : how many rows displayed
 * pages : 
 * counter : 
 * */
function pager($perpage,$pages,$counter){
    if ($perpage == 0) return;

    $m = '';
    if($pages!='') {
        $stepb = (round($counter/$perpage)-1)*$perpage;
        $stepf = (round($counter/$perpage)+1)*$perpage;

        if($counter > 0) {
            $m .= "<li class='pure-button button-small button-transp'><a href='0' class='paging'><i class='fa fa-backward'></i></a></li>";
            $m .= "<li class='pure-button button-small button-transp'><a href='$stepb' class='paging'><i class='fa fa-chevron-left'></i></a></li>";
        } else {
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-backward' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-chevron-left' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
        }

        # mennyi lap van
        $lp = ceil($pages/$perpage);

        # számláló állása
        $c = round($counter/$perpage);

        # honnan kezdődik
        if($c<5) {
            $c = 0;
        }
        elseif($c>4 and $c<($lp-4) and $lp>10) {
            $c = $c-5;
        } elseif ($c>=($lp-4) and $lp>10) {
            $c = $lp-10;
        }

        # meddig megy a számláló
        if ($lp>($c+10)) {
            $n = $c+10;
        } else
            $n = $lp;

        //köztes gombok
        for($i=$c;$i<$n;$i++) {
            $a = '';
            if ($counter/$perpage == $i) {
                $a = "pure-button-active";
            }
            $m .= sprintf("<li class='pure-button button-small button-transp $a'><a href='%d' class='paging'>%d.</a></li>",$i*$perpage,$i+1);
        }

        //egész osztásnál ne adjon egy + üres lapot
        if (fmod($pages,$perpage)) {
            $op = 0;
        } else
            $op = $perpage;

        //záró gombok
        if($stepf < $pages) {
            $m .= sprintf("<li class='pure-button button-small button-transp'><a href='%d' class='paging'><i class='fa fa-chevron-right'></i></a></li>",$stepf);
            $m .= sprintf("<li class='pure-button button-small button-transp'><a href='%d' class='paging'><i class='fa fa-forward'></i></a></li>",(floor($pages/$perpage)*$perpage)-$op);
        } else {
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-chevron-right' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
            $m .= "<li class='pure-button button-small button-transp'><i class='fa fa-forward' style='color:#cccccc;margin:0;width:100%;height:100%;display:inline-block;'></i></li>";
        }

    }
    return $m;
}
/* fetch remote xml xontent
 *
 * */
function fetch_remote_xml($url) {
    $xml = file_get_contents($url);
    
    $data = ($xml) ? new SimpleXMLElement($xml) : "";
    
    return $data;
}

/* create a href button
 * It used in results_builder action buttons */
class button {
    public $url = '?';
    public $title;
    public $icon;
    public $target = '_blank';
    public $class = 'button-href pure-button';
    public $data;
    public $onclick;
    private $type;

    public function __construct(array $args) {
        $this->init($args);
    }
    private function init($args) {
        extract($args);

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        if (isset($url) and $url != '') {
            $this->url = $protocol."://".URL."/".$url;
            $this->type = 'href';
        }

        //awesome class
        if (isset($icon) and $icon != '') $this->icon = "<i class='$icon'></i> ";

        if (isset($data) and $data != '') {
            $n = preg_split('/=/',$data);
            $this->data = "data-$n[0]=$n[1]";
        }

        if (isset($title)) $this->title = $title;
        if (isset($target)) $this->target = $target;
        if (isset($class)) $this->class = $class;
        if (isset($onclick)) $this->onclick = "onclick='".$onclick."'";

    }

    public function print(array $args) {
        if (count($args)) $this->init($args);

        if ($this->type=='href')
            return sprintf("<a href='%s' target='%s' class='%s' %s %s>%s%s</a>",$this->url,$this->target,$this->class,$this->data,$this->onclick,$this->icon,$this->title);

        return sprintf("<button class='%s' %s %s>%s%s</button>",$this->class,$this->data,$this->onclick,$this->icon,$this->title);
    }
}
/* revision as timestamp
 * */
function rev($file) {
    $timestamp = 0;#time();
    if (!file_exists("$file")) {
        $file = getenv('OB_LIB_DIR').$file;
    }
    if (file_exists("$file")) {
        $fp = fopen("$file", "r");
        $fstat = fstat($fp);
        $timestamp = $fstat['mtime'];
    }
    return $timestamp;
}
/* function of upload_table_post ajax request
 * called in upload_funcs.php
 * */
function warning($row,$column_label,$message,$column='') {
    //global $colnames;
    $r = '';
    if ($column=='')
        $column = $column_label;
    if ($column_label!='') {
        if (isset($_SESSION['api_warnings'])) {
            $_SESSION['api_warnings'][$column][$row] = $message;
            return $row;
        } else {
            // normal error warning
            if ($row!=='') {
                $r = "$row. ".str_in_row;
            }
            return "$r <span style='font-style:italic;background-color:#accade'>$column_label</span> ".str_column.": $message!";
        }
    } else {
        if (isset($_SESSION['api_warnings'])) {
            log_action('column should be defined',__FILE__,__LINE__);
        }
        // no column name
        if ($row!='') {
            $r = "$row. ".str_in_row;
        }
        return "$r: $message!";
    }
}
/* edit span button
 *
 * */
function edit ($value,$id,$name,$enabled,$function,$active=0,$action='',$table=PROJECTTABLE,$target='') {
    // return an edbox input field
    $valuec = ($value == '....................') ? "" : preg_replace("/'/","&#39;",$value);
    
    if ($target!='') {
        $target = "data-target='$target'";
    }

    $add_attachment = ($name=='obm_files_id');

    return render_view(
        'components/edit-span-button', 
        compact('enabled', 'active', 'valuec', 'value', 'id', 'name', 'function', 'action', 'table', 'target', 'add_attachment'),
        false
    );
}

/* wiki help
 * */
function wikilink($wiki,$text,$cssclass='faq') {
    //turn off help messages by using define('HELPS','off') in local_vars.php
    if (defined('HELPS') and HELPS=='off')
        return;

    $wikiurl = "https://openbiomaps.org/documents/".$_SESSION['LANG']."/$wiki";
    $link = sprintf("<span class='$cssclass'><i class='fa fa-lg fa-lightbulb-o'></i>&nbsp;<a href='%s' target='_blank' class='$cssclass'>%s</a></span>",$wikiurl,t($text));
    return $link;
}
/**
 * PseudoCrypt by KevBurns (http://blog.kevburnsjr.com/php-unique-hash)
 * Reference/source: http://stackoverflow.com/a/1464155/933782
 *
 * I want a short alphanumeric hash that’s unique and who’s sequence is difficult to deduce.
 * I could run it out to md5 and trim the first n chars but that’s not going to be very unique.
 * Storing a truncated checksum in a unique field means that the frequency of collisions will increase
 * geometrically as the number of unique keys for a base 62 encoded integer approaches 62^n.
 * I’d rather do it right than code myself a timebomb. So I came up with this.
 *
 * Sample Code:
 *
 * echo "<pre>";
 * foreach(range(1, 10) as $n) {
 *     echo $n." - ";
 *     $hash = PseudoCrypt::hash($n, 6);
 *     echo $hash." - ";
 *     echo PseudoCrypt::unhash($hash)."<br/>";
 * }
 *
 * Sample Results:
 * 1 - cJinsP - 1
 * 2 - EdRbko - 2
 * 3 - qxAPdD - 3
 * 4 - TGtDVc - 4
 * 5 - 5ac1O1 - 5
 * 6 - huKpGQ - 6
 * 7 - KE3d8p - 7
 * 8 - wXmR1E - 8
 * 9 - YrVEtd - 9
 * 10 - BBE2m2 - 10
 */
class PseudoCrypt {

    /* Key: Next prime greater than 62 ^ n / 1.618033988749894848 */
    /* Value: modular multiplicative inverse */
    private static $golden_primes = array(
        '1'                  => '1',
        '41'                 => '59',
        '2377'               => '1677',
        '147299'             => '187507',
        '9132313'            => '5952585',
        '566201239'          => '643566407',
        '35104476161'        => '22071637057',
        '2176477521929'      => '294289236153',
        '134941606358731'    => '88879354792675',
        '8366379594239857'   => '7275288500431249',
        '518715534842869223' => '280042546585394647'
    );

    /* Ascii :                    0  9,         A  Z,         a  z     */
    /* $chars = array_merge(range(48,57), range(65,90), range(97,122)) */
    private static $chars62 = array(
        0=>48,1=>49,2=>50,3=>51,4=>52,5=>53,6=>54,7=>55,8=>56,9=>57,10=>65,
        11=>66,12=>67,13=>68,14=>69,15=>70,16=>71,17=>72,18=>73,19=>74,20=>75,
        21=>76,22=>77,23=>78,24=>79,25=>80,26=>81,27=>82,28=>83,29=>84,30=>85,
        31=>86,32=>87,33=>88,34=>89,35=>90,36=>97,37=>98,38=>99,39=>100,40=>101,
        41=>102,42=>103,43=>104,44=>105,45=>106,46=>107,47=>108,48=>109,49=>110,
        50=>111,51=>112,52=>113,53=>114,54=>115,55=>116,56=>117,57=>118,58=>119,
        59=>120,60=>121,61=>122
    );

    public static function base62($int) {
        $key = "";
        while(bccomp($int, 0) > 0) {
            $mod = bcmod($int, 62);
            $key .= chr(self::$chars62[$mod]);
            $int = bcdiv($int, 62);
        }
        return strrev($key);
    }

    public static function hash($num, $len = 5) {
        $ceil = bcpow(62, $len);
        $primes = array_keys(self::$golden_primes);
        $prime = $primes[$len];
        $dec = bcmod(bcmul($num, $prime), $ceil);
        $hash = self::base62($dec);
        return str_pad($hash, $len, "0", STR_PAD_LEFT);
    }

    public static function unbase62($key) {
        $int = 0;
        foreach(str_split(strrev($key)) as $i => $char) {
            $dec = array_search(ord($char), self::$chars62);
            $int = bcadd(bcmul($dec, bcpow(62, $i)), $int);
        }
        return $int;
    }

    public static function unhash($hash) {
        $len = strlen($hash);
        $ceil = bcpow(62, $len);
        $mmiprimes = array_values(self::$golden_primes);
        $mmi = $mmiprimes[$len];
        $num = self::unbase62($hash);
        $dec = bcmod(bcmul($num, $mmi), $ceil);
        return $dec;
    }

}
#http://stackoverflow.com/questions/959957/php-short-hash-like-url-shortening-websites
class BaseIntEncoder {

    //const $codeset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //readable character set excluded (0,O,1,l)
    const codeset = "23456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";

    static function encode($n){
        $base = strlen(self::codeset);
        $converted = '';

        while ($n > 0) {
            $converted = substr(self::codeset, bcmod($n,$base), 1) . $converted;
            $n = self::bcFloor(bcdiv($n, $base));
        }

        return $converted ;
    }

    static function decode($code){
        $base = strlen(self::codeset);
        $c = '0';
        for ($i = strlen($code); $i; $i--) {
            $c = bcadd($c,bcmul(strpos(self::codeset, substr($code, (-1 * ( $i - strlen($code) )),1))
                    ,bcpow($base,$i-1)));
        }

        return bcmul($c, 1, 0);
    }

    static private function bcFloor($x)
    {
        return bcmul($x, '1', 0);
    }

    static private function bcCeil($x)
    {
        $floor = bcFloor($x);
        return bcadd($floor, ceil(bcsub($x, $floor)));
    }

    static private function bcRound($x)
    {
        $floor = bcFloor($x);
        return bcadd($floor, round(bcsub($x, $floor)));
    }
}
function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}
function test_alter(&$item1, $key, $prefix)
{
    $item1 = $prefix;
}
/* `apply function: + click functions in upload table
 * content = content read from cell to apply function
 * if pointer given to an other cell, content will be the pointed cell content
 * and the selected cell content is the fill
 *
 * */
function upload_table_obfun($apply_function,$rowindex,$colindex,$idmatch,$autofill_data,$u_theader) {

    $idx  = sprintf('%d',preg_replace('/[^0-9]/','',$colindex)-2);

    $theader = json_decode($u_theader,true);
    //$upload_column_assign = json_decode($a_theader,true); MÉG NINCS ÁTADVA
    $key = $theader[$idx];

    $row = get_sheet_row($rowindex+1);

    if ($row!='')
        $j = json_decode($row['data'],true);
    else
        $j = array();

    if (!isset($j[$key]))
        $content = "";
    else
        $content = $j[$key];


    if ($idmatch!='' and $idmatch!=-1) {
        // if use function to refer an other column
        // apply:[EGYEDSZAM]
        // idmatch is EGYEDSZAM
        $fill = $content;
        //$content = $j[$idmatch];
    }
    elseif ($idmatch=='-1') {
        if ($rowindex==0) return $content;

        $row = get_sheet_row($rowindex+1);
        $j1 = json_decode($row['data'],true);
        $content = $j1[$key];

        $row = get_sheet_row($rowindex);
        $j0 = json_decode($row['data'],true);
        $fill = $j0[$key];

        $j = $j1;
    }

    if (preg_match('/^`apply:(\[.+?\])?(.+)$/i',$apply_function,$fm)) {
        // referring an other cell in function e.g. apply:[EGYEDSZAM]
        $pointer_cell = preg_replace('/^\[/','',$fm[1]);
        $pointer_cell = preg_replace('/\]$/','',$pointer_cell);

        $function = preg_replace('/^\[\]/','',$fm[2]);

        if (preg_match('/days.since.(\d{4})-(\d{2})-(\d{2})/',$function,$km) and is_numeric($content)) {
            // excel days
            $con_date = strtotime("$km[1]-$km[2]-$km[3]");
            $unix_date = strtotime("1970-01-01");
            $datediff = $unix_date - $con_date;

            $int = (int)$content;
            $float = (float)$content;

            if ($float != $int) {
                $daysdiff = $datediff / (60 * 60 * 24);
                $UNIX_DATE = ($content - $daysdiff) * 86400;
                $content = gmdate("Y-m-d H:i:s", $UNIX_DATE);
            } else {
                $daysdiff = floor($datediff / (60 * 60 * 24));
                $UNIX_DATE = ($content - $daysdiff) * 86400;
                $content = gmdate("Y-m-d", $UNIX_DATE);
            }
        }
        elseif (preg_match('/seconds.since.(\d{2})-(\d{2})-(\d{2})/',$function,$km) and is_numeric($content)) {
            // excel time
            $t = $content * 86400;
            $hours = floor($t / 3600);
            $t = $t % 3600;
            $minutes = floor($t / 60);
            $seconds = $t % 60;
            $content = sprintf("%02d:%02d:%02d",$hours,$minutes,$seconds);
        }
        elseif ($function=='increment' and $content!='') {

            if ($pointer_cell=='') {
                $content++;
            } else {
                $content++;
            }
        }
        elseif ($function=='math.sum') {

            if ($pointer_cell=='') {
                if (preg_match_all('/([0-9.-]+)/',$content,$mm))
                    $content = array_sum($mm[1]);
            } else {
                // reference cells
                $references = preg_split('/,/',$pointer_cell);
                $avg_values = array();
                foreach($references as $ref) {
                    $content = $j[$ref];

                    if (preg_match_all('/([0-9.-]+)/',$j[$ref],$mm2)) {
                        $avg_values = array_merge($avg_values,$mm2[1]);
                    }
                }
                if (count($avg_values))
                    $content = array_sum($avg_values);
                else
                    $content = $fill;

            }
        }
        elseif ($function=='math.avg') {

            if ($pointer_cell=='') {
                if (preg_match_all('/([0-9.-]+)/',$content,$mm))
                    $content = array_sum($mm[1])/count($mm[1]);
            } else {
                // reference cells
                $references = preg_split('/,/',$pointer_cell);
                $avg_values = array();
                foreach($references as $ref) {
                    $content = $j[$ref];

                    if (preg_match_all('/([0-9.-]+)/',$j[$ref],$mm2)) {
                        $avg_values = array_merge($avg_values,$mm2[1]);
                    }
                }
                if (count($avg_values))
                    $content = array_sum($avg_values) / count($avg_values);
                else
                    $content = $fill;
            }
        }
        elseif (preg_match('/^replace.all:\/(.*)\/(.*)\//u',$function,$mm)) {
            // replace cell content to the given string in the cell value matches to the pattern
            // match test can be an other field

            if ($pointer_cell=='') {
                if (preg_match("/$mm[1]/",$content)) {
                    $content = $mm[2];
                }
            } else {
                // special case we read compare content from an other column

                $jvals = array_values($j);
                $references = preg_split('/,/',$pointer_cell);
                // csak egy, az utolsó referencia cella van figyelmbe véve!
                foreach($references as $ref) {
                    if (isset( $upload_column_assign )) {
                        $pkey = array_search($ref,$upload_column_assign);
                        $content = $jvals[$pkey];
                    } else
                        $content = $j[$ref];
                }
                // ha megvan a minta a másik cellában
                if (preg_match("/$mm[1]/",$content,$m3)) {
                    // és van /($1)/ hivatkozás a regexpben
                    if (preg_match('/\$(\d)/',$mm[2],$m4))
                        $mm[2] = $m3[$m4[1]];
                    $content = $mm[2];
                } else {
                    // protect non-mached cells, write back the original content into the content variable
                    $content = $fill;
                }
            }
        }
        elseif (preg_match('/^replace.match:\/(.*)\/(.*)\//u',$function,$mm)) {
            // replace matched characters to the given string in the cell values

            if (preg_match("/$mm[1]/",$content)) {
                $content = preg_replace("/$mm[1]/", $mm[2], $content);
            }
        }
        elseif ($function=='groupped.fill') {
            if ($idmatch=='') {
                if ($rowindex==0) return $content;

                $row = get_sheet_row($rowindex+1);
                $j1 = json_decode($row['data'],true);
                $content = $j1[$key];
                $row = get_sheet_row($rowindex);
                $j0 = json_decode($row['data'],true);
                $fill = $j0[$key];

                $j = $j1;
            }

            if ($content == '') {
                $content = $fill;
            }
        }
    } else {

        // not function based filling
        // just overwrite content with the given string.
        $content = $autofill_data;
    }

    $j[$key] = $content;
    $res = update_sheet_row($rowindex+1,$j);
    return $content;

}
// check system command exists
function command_exist($cmd) {
    //$returnVal = shell_exec(sprintf("which %s", escapeshellarg($cmd)));
    $returnVal = exec(sprintf("which %s", escapeshellarg($cmd)));
    return !empty($returnVal);
}
/* quote an sql column name
 * split by .
 * remove extra characters...
 */
function quote_column($column) {
    $table = '';
    $m = preg_split('/\./',$column);
    if (count($m)>1) {
        $table = preg_replace('/[^a-z0-9_]/i','',$m[0]);
        $column = $m[1];
    }
    $column = preg_replace('/[^a-z0-9_]/i','',$column);

    if ($table != '')
        return sprintf('"%s"."%s"',$table,$column);
    else
        return sprintf('"%s"',$column);
}

/* array [normal|associative], array_element
 * create an "<option></option>" list from the element the array and set the selected element
 * */
function selected_option($options,$selected='',$disabled=array()) {
    $e = array();
    foreach($options as $val) {
        $key = $val;
        $title = '';
        $m = preg_split('/::/',$val);
        if (count($m) > 1) {
            $key = $m[0];
            $val = $m[1];
            if(isset($m[2]))
                $title = $m[2];
        }
        if (!is_array($selected))
            $selected = array($selected);

        $s = "";
        foreach ($selected as $sel) {
            if (trim($val) == trim($sel)) {
                $s = "selected";
            }
        }
        $d = "";
        foreach ($disabled as $sel) {
            if (trim($val) == trim($sel)) {
                $d = "disabled";
            }
        }
        $value = "<option $s $d value='$val' title='$title'>$key</option>";

        $e[$key] = $value;
    }
    // extra empty line
    if ($selected==' ') $e[] = "<option selected></option>";
    return implode($e,'');
}
/* Return a 'checked' string if checkbox checked
 *
 * */
function checkbox_check($option,$checked) {
    if ($option === $checked)
        return 'checked';
}
/* insert assoc key=>elem to specified pos in array */
function insertAt($array = [], $item = [], $position = 0) {
    $previous_items = array_slice($array, 0, $position, true);
    $next_items     = array_slice($array, $position, NULL, true);
    return $previous_items + $item + $next_items;
}

/* A simple web-get function */
function wget($url,$header) {
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
    } else {
        $result = common_message('error','Curl error');
    }
    return $result;
}

/* Retreive and create citation from doi
 * It is an experimental function, not used yet anywhere...
 *
 * doi:
 *      10.1242/jeb.040394
 * url:
 *      https://doi.org/
 *      https://api.crossref.org/works/
 *      ...
 * type:
 *      bibtex,Citeproc.JSON,RDF.XML,text
 * style:
 *      apa,mla,harvard3
 *      list of available style:
 *      https://github.com/citation-style-language/styles
 * accept:
 *      Accept: application/x-bibtex
 *      ...
 *
 * */
function doi_get($doi,$url='https://doi.org/',$type='text',$style='apa',$accept="") {


    // examples:
    // https://citation.crosscite.org/docs.html

    // simple bibtex format
    // dx.doi.org
    // curl -LH "Accept: application/x-bibtex" http://dx.doi.org/10.1242/jeb.040394
    // www.doi2bib.org
    // curl http://www.doi2bib.org/doi2bib?id=10.1242/jeb.040394
    if ($type == 'bibtex') {
        //$url = "http://www.doi2bib.org/doi2bib?id=";
        //$url = "http://dx.doi.org/";
        //$url = "http://doi.org/";

        $result = wget($url.$doi,array('Accept: application/x-bibtex'));
        return $result;
    }

    $curl = curl_init();

    // Citeproc JSON from crossref.org
    // curl https://api.crossref.org/works/10.1242/jeb.040394/transform/application/vnd.citationstyles.csl+json
    // curl -LH "Accept: application/rdf+xml;q=0.5, application/vnd.citationstyles.csl+json;q=1.0" https://doi.org/10.1242/jeb.040394
    // return JSON
    if ($type == 'Citeproc.JSON') {
        if (preg_match("api.crossref.org",$url)) {
            $url = "https://api.crossref.org/works/";
            $doi .= "/transform/application/application/vnd.citationstyles.csl+json";
            $accept = "";
        } elseif (preg_match('doi.org')) {
            $url = "https://doi.org/";
            $accept = "Accept: application/rdf+xml;q=0.5, application/vnd.citationstyles.csl+json;q=1.0";
        }

        $result = wget($url.$doi,array($accept));
        return $result;
    }

    // styled text
    // possible styles: https://github.com/citation-style-language/styles
    // APA styled from doi.org
    // curl -LH "Accept: text/x-bibliography;style=apa" https://doi.org/10.1242/jeb.040394
    // return text
    if ($type == 'text') {
        $accept = "Accept: text/x-bibliography;style=$style; locale=en-GB";
        //$url = "https://doi.org/";
        //$url = "https://dx.doi.org/";

        $result = wget($url.$doi,array($accept));
        return $result;
    }

    // RDF XML
    // get xml
    // curl -LH "Accept: application/vnd.crossref.unixref+xml;q=1, application/rdf+xml;q=0.5" https://doi.org/10.1242/jeb.040394
    //
    // get JSON
    if ($style == "RDF.XML") {
        //$url = "https://doi.org/"
        if (preg_match('doi.org')) {
            $url = "https://doi.org/";
            $accept = "Accept: application/vnd.crossref.unixref+xml;q=1, application/rdf+xml;q=0.5";
        }

        $result = wget($url.$doi,array($accept));
        /* xml processing example
         *
         * $xml = simplexml_load_string($result);
         * if (isset($xml->{'doi_records'})) {
            $title = $xml->{'doi_records'}->{'doi_record'}->{'crossref'}->{'journal'}->{'journal_article'}->{'titles'}->{'title'};
            return;
        } */

        return $result;

    }
    // Search by title or other text fields...
    // curl http://search.crossref.org/dois?q=Discordancy+or+template-based+recognition?+Dissecting+the+cognitive+basis+of+the+rejection+of+foreign+eggs+in+hosts+of+avian+brood+parasites
    // $j = json_decode($result)
    // $doi = $j[0]['doi'];
    // ...
}
function common_multi_message($return) {
    $mstatus = array();
    foreach($return as $marray) {
        $mstatus[] = $marray[0];
        $mexpl[] = $marray[1];
    }
    if (count(array_unique($mstatus))>1) {
        $return = "error";
        $explanation = implode('; ',$mexpl);
    }
    else {
        $return = $mstatus[0];
        $explanation = implode('; ',$mexpl);
    }

    return common_message($return,$explanation);
}
/* return standard JSON status message
 * https://labs.omniti.com/labs/jsend
 * */
function common_message ($return,$explanation='') {


    $status = 'unknown';
    $data = '';
    $message = '';
    $error = '';

    if (preg_match('/ok/i',$return)) {
        $status = "success";
        $message = '';
        $data = $explanation;
    } elseif (preg_match('/fail/i',$return)) {
        $status = "fail";
        $message = $explanation;
        #ErrorID#5e3c06f6592ba#
        if (preg_match('/#([0-9a-f]{13})#$/',$explanation,$hash)) {
            $data = "#$hash[1]";
        } else $data = '#';
    } elseif (preg_match('/error/i',$return)) {
        $status = "error";
        $message = $explanation;
        $data = '';
    } elseif (preg_match('/warning/i',$return)) {
        $status = "warning";
        $message = $explanation;
        $data = '';
    } elseif ($return) {
        $status = "success";
        $message = '';
        $data = $explanation;
    } elseif (!$return) {
        $status = "error";
        $message = $explanation;
        $data = '';
        // due to oauth error compatibility
        $error = $explanation;
    } elseif ($return == 'empty') {
        $status = "fail";
        $message = $explanation;
        if (preg_match('/#([0-9a-f]{13})#$/',$explanation,$hash)) {
            $data = "#$hash[1]";
        } else $data = '#';
    } else {
        log_action('invalid ajax message: '.$return,__FILE__,__LINE__);
    }

    if ($error!='')
        $result = array('status'=>$status,'message'=>$message,'data'=>$data,'error'=>$error);
    else
        $result = array('status'=>$status,'message'=>$message,'data'=>$data);

    return (json_encode($result));
}
//Check if a port is open - run R-shiny server
function service_test($ip='localhost', $port=8001) {

    $fp = @fsockopen($ip, $port, $errno, $errstr, 0.1);
    if (!$fp) {
        return false;
    } else {
        fclose($fp);
        return true;
    }
}
// mgrid-area function
function area($area) {
        $area = preg_replace('/^\s*/m','"',$area);
        $area = preg_replace('/$/m','"',$area);
        $area = preg_replace('/\s+/m',' ',$area);
        return $area;
}

function mb_in_array($_needle,array $_hayStack) {
    foreach ($_hayStack as $value) {
        //log_action(mb_strtolower($value).' '.mb_strtolower($_needle));
        if((mb_strtolower($value)) === (mb_strtolower($_needle))) {
            return true;
        }
    }
    return false;
}
/* sql table aliases FROM clause*/
function sql_aliases($current_table) {
    global $BID;

    if ($current_table == 1) {
        if (isset($_SESSION['current_query_table']))
            $current_table = $_SESSION['current_query_table'];
        else
            $current_table = PROJECTTABLE;
    }

    $st_col = st_col($current_table,'array');
    $qtable = $current_table;
    $id_col = "obm_id";
    $id_alias = $qtable;
    $qtable_alias = $qtable;
    $FROM = sprintf('FROM %1$s',$qtable);
    $jtable_alias = $qtable_alias;

    # very default geometry settings
    $geom_alias = $qtable_alias;
    $GEOMETRY_COLUMN = 'obm_geometry';
    if (isset($st_col['GEOM_C']) and $st_col['GEOM_C']!='') {
        $GEOM_C = $qtable_alias.".".$st_col['GEOM_C'];
        $GEOMETRY_COLUMN = $st_col['GEOM_C'];
    }

    $cmd1 = sprintf("SELECT layer_query
        FROM project_queries
        WHERE project_table='%s' AND layer_type='query' AND enabled=TRUE AND main_table=%s
        ORDER BY rst DESC",
            PROJECTTABLE,quote($current_table));

    // WHICH ROW????
    $res = pg_query($BID,$cmd1);
    $row = pg_fetch_assoc($res);

    $joins = 0;
    // FROM %F%dinpi d%F%
    if (pg_num_rows($res) && preg_match('/%F%(.+)%F%/',$row['layer_query'],$m)) {

        $FROM = sprintf('FROM %s',$m[1]);

        // dinpi d
        if (preg_match('/([a-z0-9_]+) (\w+)/i',$m[1],$mt)) {
            $qtable = $mt[1];
            $qtable_alias = $mt[2];
            $id_alias = $qtable_alias;
            $id_col = $qtable_alias.".obm_id";
            if (isset($st_col['GEOM_C']) and $st_col['GEOM_C']!='') {
                $geom_alias = $qtable_alias;
                $GEOM_C = $qtable_alias.".".$st_col['GEOM_C'];
            } else {
                $geom_alias = '';
                $GEOM_C = '';
            }

            // geometry can come from any joined tables!!
            if (preg_match("/([a-z0-9_]+)\.obm_geometry/",$row['layer_query'],$mg)) {
                $geom_alias = $mg[1];
                $GEOM_C = $geom_alias.".obm_geometry";
                $GEOMETRY_COLUMN = 'obm_geometry';
            }
            /*if (preg_match("/([a-z_]+)\.%transform_geometry%/",$row['layer_query'],$mg)) {
                $geom_alias = $mg[1];
                $geom_col = $geom_alias.".obm_geometry";
            }*/
            if (preg_match("/([a-z0-9_]+)\.obm_id/",$row['layer_query'],$mg)) {
                $id_alias = $mg[1];
                $id_col = $id_alias.".obm_id";
            }

        }
        if (preg_match_all('/%J%(.+)%J%/',$row['layer_query'],$mj)) {
            foreach($mj[1] as $mje) {
                // LEFT JOIN public_nestbox_data n ON (nestbox_id=n.obm_id)
                if (preg_match('/JOIN\s+([a-z0-9_]+)\s+(\w+)/i',$mje,$mt)) {
                    $jtable = $mt[1];
                    $jtable_alias = $mt[2];
                    $joins++;
                }
                $FROM .= " ".$mje;
            }
        }
    }
    return(array("from"=>$FROM,"qtable"=>$qtable,"qtable_alias"=>$qtable_alias,
        "geom_col"=>$GEOM_C,"geom_alias"=>$geom_alias,"id_col"=>$id_col,"id_alias"=>$id_alias,
            "joins"=>$joins,"GEOMETRY_COLUMN"=>$GEOMETRY_COLUMN,"join_alias"=>$jtable_alias));
}

/* unzip file to destination directory
 * destination directory is typically a temp directory, which shlould be deleted!!
 * */
function unpack_zip($file,$dest) {
    $zip = new ZipArchive;
    if ($zip->open($file) === TRUE) {
        if (!mkdir($dest, 0700, true)) {
            log_action("Failed to create UNZIP directory: $dest",__FILE__,__LINE__);
            return 0;
        }
        $zip->extractTo($dest);
        $zip->close();
        return 1;
    } else {
        log_action("Failed to read ZIP archive: $file",__FILE__,__LINE__);
        return 0;
    }
}
function unpack_rar($file,$dest) {
    if ($rar_open($file) === TRUE) {
        if (!mkdir($dest, 0700, true)) {
            log_action("Failed to create UNRAR directory: $dest",__FILE__,__LINE__);
            return 0;
        }

        $list = rar_list($rar_file);
        foreach($list as $file) {
            $entry = rar_entry_get($rar_file, $file);
            $entry->extract($dest); // extract to the current dir
        }
        rar_close($rar_file);
        return 1;
    } else {
        log_action("Failed to read Rar archive: $file",__FILE__,__LINE__);
        return 0;
    }
}

/* zip data line process
 * unzip, attachments processing
 * create sheet_data,theader SESSION variables for upload_processing
 *
 * */
function packed_data_line_process($F,$append=false,$u_theader) {
    switch ($F['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            echo common_message('error','No file sent.');
            return;
        case UPLOAD_ERR_INI_SIZE:
            echo common_message('error','The uploaded file exceeds the upload_max_filesize directive in php.ini.');
            return;
        case UPLOAD_ERR_FORM_SIZE:
            echo common_message('error','Exceeded filesize limit.');
            return;
        default:
            echo common_message('error','Unknown errors.');
            return;
    }

    $theader = json_decode($u_theader,true);

    // unpack zip destination temporary directory
    $dest = sprintf('%s%s/zipdir/%s_dir/',OB_TMP,session_id(),$F['name']);
    if (is_dir($dest)) {
        $errorID = uniqid();
        echo common_message('failed','Failed to upload files, see logs for more details.',$errorID);
        log_action("ErrorID#$errorID# "."Temporary directory unexpectedly exists: $dest",__FILE__,__LINE__);
        return;
    }

    if (!unpack_zip($F['tmp_name'],$dest)) {
        echo common_message('error','Failed to unpacking files');
        if (is_dir($dest)) {
            //might be not empty...
            //foreach($drop_files as $f)
            //    unlink($dest.$f);
            rmdir($dest);
            rmdir(sprintf('%s%s/zipdir/',OB_TMP,session_id()));
            rmdir(sprintf('%s%s/',OB_TMP,session_id()));
        }
        return;
    }

    include_once(getenv('OB_LIB_DIR').'read_uploaded_file.php');
    $ruf = new read_uploaded_file();

    // create $_FILES like array from zip content

    # Old Mobile App export data format
    # Archive:  Mon May 07 11:29:03 CEST 2018.zip
    #  Length      Date    Time    Name
    #---------  ---------- -----   ----
    #       12  2018-05-14 10:38   note.txt
    #       26  2018-05-14 10:38   geometry.wkt
    #  1590977  2018-05-14 10:38   IMG_20180507_112730.jpg
    #  1508545  2018-05-14 10:38   IMG_20180507_112743.jpg
    #  1216705  2018-05-14 10:38   IMG_20180507_112800.jpg
    #---------                     -------
    #  4316265                     5 files

    $zip_contents = array('data'=>'','type'=>'','geometry'=>'','attachments'=>array('tmp_name'=>array(),'name'=>array(),'type'=>array(),'error'=>array()));
    $handle = opendir($dest);
    $drop_files = array();

    while (false !== ($entry = readdir($handle))) {
        if (!is_file($dest.$entry))
            continue;
        $drop_files[] = $entry;

        if ($entry == 'note.txt') {
            // old obm_app compatibility
            //$zip_contents['data'] = $entry;
            $zip_contents['type'] = 'CSV';
            $zip_contents['data'] = 'note.csv';
            $note = file($dest.$entry);

        } elseif ($entry == 'data.json') {
            // theoretical advanced zip upload
            $zip_contents['data'] = $entry;
            $zip_contents['type'] = 'JSON';
            $note = file($dest.$entry);

        } elseif ($entry == 'geometry.wkt') {
            // old obm_app compatibility
            $wkt = file($dest.$entry);

        } else {
            $zip_contents['attachments']["tmp_name"][] = $dest.$entry;
            $zip_contents['attachments']['name'][] = $entry;
            $zip_contents['attachments']['type'][] = '';
            $zip_contents['attachments']['error'][] = UPLOAD_ERR_OK;

        }
    }
    closedir($handle);

    //old style zip export processing
    if ($zip_contents['data']=='note.csv') {

        // flip coordinates due to old app format...
        // POINT(47.171012 19.055104)
        if (preg_match('/\((\d+\.\d+) (\d+\.\d+)\)/',$wkt[0],$m)) {
            $wkt[0] = "POINT($m[2] $m[1])";
        }
        $datum = preg_replace('/\.zip$/','',$F['name']);
        $files_list = sprintf(implode(',',$zip_contents['attachments']['name']));
        $zip_contents['data'] = array_combine($theader,array($wkt[0],$files_list,$note[0],$datum));
    } elseif ($zip_contents['data']=='data.json') {
        $zip_contents['data'] = json_decode($note,true);
    }

    $r = move_upl_files($zip_contents['attachments'],getenv('PROJECT_DIR').'local/attached_files');
    $rr = json_decode($r);
    $rr->{'file_names'} = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});

    if (isset($zip_contents['data']['obm_files_id']))
        $zip_contents['data']['obm_files_id'] = implode(',',$rr->{'file_names'});

    $res = create_upload_temp(array($zip_contents['data']),$append);

    // cleaning zip directory
    // unlink(/mnt/data/tmp/j1n34pc8o55i1u2uagn3312r7t/zipdir/stuff.zip_dir/Sun May 13 08:52:51 CEST 2018.zip)
    if (is_dir($dest)) {
        foreach($drop_files as $f)
            unlink($dest.$f);
        rmdir($dest);
        rmdir(sprintf('%s%s/zipdir/',OB_TMP,session_id()));
        rmdir(sprintf('%s%s/',OB_TMP,session_id()));
    }
    return "done";
}

/* Copy readed CSV, GPX,... content to temporary database table
 * Used in read_uploades_file.php, pds_class.php
 * */
function create_upload_temp($sheetData=array(),$append=false,$interconnect=false) {
    global $ID;

    if ($append) {
        $cmd = sprintf("SELECT EXISTS (
                SELECT 1
                FROM   information_schema.tables
                WHERE  table_schema = 'temporary_tables' AND table_name = 'upload_%s_%s')",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            if ($row[0]=='f')
                $append = false;
        }
    }
    if (!$append) {
        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.upload_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.upload_%s_%s (row_id integer, skip_marked boolean DEFAULT false NOT NULL, data json)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);

        if (pg_last_error($ID)) {
            log_action(pg_last_error($ID),__FILE__,__LINE__);
            return false;
        } else {
            update_temp_table_index (PROJECTTABLE.'_'.session_id(),'upload',$interconnect);
            $row_num = 1;
        }
    } else {
        $cmd = sprintf("SELECT max(row_id) FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            $row_num = $row[0]+1;
        } else
            return false;
    }

    pg_query($ID, sprintf("COPY temporary_tables.upload_%s_%s FROM stdin",PROJECTTABLE,session_id()));

    foreach ($sheetData as $data_line) {
        # skip empty lines
        # lehet, hogy jobb lenne az implode_recursive függvénnyel tesztelni, csak meg kéne nézni, hogy mi is jön be tömbként...
        if (!is_multi($data_line))
            if (implode("",$data_line) == "") continue;

        $line = array();
        $line[] = $row_num;
        $line[] = 0;
        $n_line = json_encode($data_line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT|JSON_HEX_APOS);
        # 
        # invalid input syntax for type json....
        #
        // control variable pass by user??
        $new_line_char = "\\\\n";
        // $new_line_char = " ";
        $n_line = str_replace("\\n", "$new_line_char", $n_line); # newline replace  
        # \t problem
        $n_line = str_replace("\\t", "    ", $n_line); # tabs replaced by four whitespaces
        # backslash problem
        #$n_line = preg_replace('/\\\+/','',$n_line);
        $n_line = pg_escape_string($n_line);
        $n_line = str_replace('\\', '\\\\', $n_line); ## Haha!! pg_escape_string replace every \ to \\ but it is not enough to create a valid JSON we need more \ :D

        $line[] = $n_line;
        $line_string = implode("\t",$line);
        pg_put_line($ID, $line_string."\n");
        $row_num++;
    }

    pg_put_line($ID, "\\.\n");
    pg_end_copy($ID);

    return true;
}

function implode_recursive(string $separator, array $array): string
{
    $string = '';
    foreach ($array as $i => $a) {
        if (is_array($a)) {
            $string .= implode_recursive($separator, $a);
        } else {
            $string .= $a;
            if ($i < count($array) - 1) {
                $string .= $separator;
            }
        }
    }

    return $string;
}

function is_multi($a) {
    $rv = array_filter($a,'is_array');
    if(count($rv)>0) return true;
    return false;
}

/* Read offset - count rows data from sheetData temp table
 *
 * */
function get_sheet_page($from,$count) {
    global $ID;

    if ($from==0 and $count==-1)
        $cmd = sprintf("SELECT row_id,skip_marked,data FROM temporary_tables.upload_%s_%s ORDER BY row_id",PROJECTTABLE,session_id());
    else
        $cmd = sprintf("SELECT row_id,skip_marked,data FROM temporary_tables.upload_%s_%s ORDER BY row_id LIMIT %d OFFSET %d",PROJECTTABLE,session_id(),$count,$from);

    $res = pg_query($ID,$cmd);
    $d = array();
    while ($row = pg_fetch_assoc($res)) {
        $d[] = json_decode($row['data'],true);
    }
    return $d;
}
/*
 *
 * */
function get_upload_count() {
    global $ID;

    $cmd = sprintf("SELECT COUNT(*) AS c FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return $row['c'];
    }
    return 0;
}
/*
 *
 * */
function get_sheet_row($n) {
    global $ID;
    $cmd = sprintf("SELECT * FROM temporary_tables.upload_%s_%s WHERE row_id=%d",PROJECTTABLE,session_id(),$n);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        return pg_fetch_assoc($res);
    }
    return false;
}
/*
 *
 * */
function update_sheet_row($n,$data,$skip='NA') {
    global $ID;
    if ($skip!='NA') {
        $skip_marked = 'false';
        if ($skip) $skip_marked = 'true';
        $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET data=%s,skip_marked=%s WHERE row_id=%d",PROJECTTABLE,session_id(),quote(json_encode($data)),$skip_marked,$n);
    } else
        $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET data=%s WHERE row_id=%d",PROJECTTABLE,session_id(),quote(json_encode($data)),$n);

    $res = pg_query($ID,$cmd);
    if (pg_affected_rows($res)) {
        return true;
    }
    return false;
}
/*
 *
 * */
function update_sheet_skips($n,$skip) {
    global $ID;
    $skip_marked = 'false';
    if ($skip) $skip_marked = 'true';

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked=%s WHERE row_id=%d",PROJECTTABLE,session_id(),$skip_marked,$n);

    $res = pg_query($ID,$cmd);
    if (pg_affected_rows($res)) {
        return true;
    }
    return false;
}
/*
 *
 * */
function add_sheet_row($n,$data,$skip='false') {
    global $ID;

    if ($skip == 1)
        $skip = 'true';
    elseif ($skip == 0)
        $skip = 'false';

    if ($skip!='true')
        $skip_marked = 'false';
    else
        $skip_marked = 'true';

    if ($n == '') {
        $cmd = sprintf("SELECT max(row_id)+1 FROM temporary_tables.upload_%s_%s",PROJECTTABLE,session_id());
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_row($res);
            $n = $row[0];
        }
    }

    $cmd = sprintf("INSERT INTO temporary_tables.upload_%s_%s (row_id,data,skip_marked) VALUES (%d,%s,%s)",PROJECTTABLE,session_id(),$n,quote(json_encode($data)),$skip_marked);
    $res = pg_query($ID,$cmd);
}
/*
 *
 * */
function get_sheet_skips($from,$count) {
    global $ID;

    if ($from==0 and $count==-1)
        $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s ORDER BY row_id",PROJECTTABLE,session_id(),$count,$from);
    else
        $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s ORDER BY row_id  LIMIT %d OFFSET %d",PROJECTTABLE,session_id(),$count,$from);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        return pg_fetch_all($res);
    }
    return false;
}
/*
 *
 * */
function get_row_skip($n) {
    global $ID;

    $cmd = sprintf("SELECT skip_marked FROM temporary_tables.upload_%s_%s WHERE row_id = %d",PROJECTTABLE,session_id(),$n);
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return $row['skip_marked'];
    }
    return false;

}
/*
 *
 * */
function get_sheet_header() {
    global $ID;

    $cmd = sprintf("SELECT data FROM temporary_tables.upload_%s_%s WHERE row_id = 1 LIMIT 1",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return array_keys(json_decode($row['data'],true));
    }
    return false;
}

/* Processes the trigger function from the upload form definition */
function process_trigger($row) {
    $t = ['function' => ''];
    //{"list": {}, "table": "milvus_sampling_units","valueColumn": "obm_geometry", "labelColumn": "", "filterColumn": "obm_id", "pictures": [],"triggerTargetColumn": [],"Function":"get_value"}
    if ($row['list_definition']!='') {
        $j = json_decode($row['list_definition'],TRUE);
        if (isset($j['Function'])) {
            $t['foreign_key'] = isset($j['filterColumn']) ? $j['filterColumn'] : "";
            $t['function'] = isset($j['Function']) ? $j['Function'] : "";
            if (isset($j['triggerTargetColumn']) and is_array($j['triggerTargetColumn'])) $t['target_column'] = $j['triggerTargetColumn'];
            elseif (isset($j['triggerTargetColumn'])) $t['target_column'] = explode(';',$j['triggerTargetColumn']);
            else $t['target_column'] = array();
            $t['table'] = isset($j['optionsTable']) ? $j['optionsTable'] : "";
            $t['schema'] = isset($j['optionsSchema']) ? $j['optionsSchema'] : "public";
            $t['value_col'] = isset($j['valueColumn']) ? $j['valueColumn'] : "";
            $t['labelAsValue'] = isset($j['labelAsValue']) ? $j['labelAsValue'] : "";
            $t['preFilterColumn'] = isset($j['preFilterColumn']) ? $j['preFilterColumn'] : "";
            $t['preFilterValue'] = isset($j['preFilterValue']) ? $j['preFilterValue'] : "";
            $t['preFilterRelation'] = isset($j['preFilterRelation']) ? $j['preFilterRelation'] : "";

            return $t;
        }
        return false;
    }
    /*if ($row['control'] == 'trigger') {
        if (preg_match('/get_value\((\w+)?\.(\w+)\:?(\w+)?\)/',$row['custom_function'],$sl)) {
            $t['function'] = 'get_value';
            $t['table'] = $sl[1];
            $t['id_col'] = $sl[2];
            $t['value_col'] = $sl[3];
        }
        elseif (preg_match('/select_list\((\[?\w+(?:(?:,\w+)+)?\]?)?\,(\^?\w+)?\)/',$row['custom_function'],$sl)) {
            $t['function'] = 'select_list';
            $t['target_column'] = explode(',',trim($sl[1],'[]'));
            $t['foreign_key'] = $sl[2];
        }
        else
            return false;
        return $t;
    }
    else*/
        return false;
}

class upload_select_list {

    const PATTERN = '/SELECT:(\w+)\[?(\w+(?:(?:,\w+)+)?)?\]?\.(\w+):?(\w+)?/'; //pattern with filter (eg. SELECT:table[filter_col,filter_value].value:label)
                                                                               //                         SELECT:milvus_sampling_units.obm_id:alapegyseg
    var $options = [];
    var $data_attr = '';
    var $multiselect = false;
    var $selected = null;
    var $ssiz = '';
    var $add_empty_line = true;

    function __construct ($form_element, $term = '', $add_filter_column_for_pds = false, $startmatch = true) {

        $this->list_definition = $form_element['list_definition'];
        $this->list = $form_element['list'];
        $this->type = $form_element['type'];
        $this->control = $form_element['control'];
        $this->custom_function = $form_element['custom_function'];
        $this->default_value = $form_element['default_value'];
        $this->ajaxCall = (substr((debug_backtrace()[0]['file']),-10) == 'afuncs.php');
        $this->filter_for_pds = $add_filter_column_for_pds;

        // a form szerkesztő list_definition paraméterek feldolgozása
        if ($this->list_definition != '') {
            $j = (is_array($this->list_definition)) ? $this->list_definition : json_decode($this->list_definition,TRUE);
            if (isset($j['add_empty_line']) and $j['add_empty_line'] === false) {
                $this->add_empty_line = false;
            }
            if (isset($j['list']) and count($j['list'])) {
                // simple list
                $this->from_list(0,$j);

            } elseif (isset($j['optionsTable']) and $j['optionsTable'] and isset($j['valueColumn']) ) {
                $optionsTable = $j['optionsTable'];
                $valueColumn = $j['valueColumn'];
                $labelColumn = (isset($j['labelColumn']) and $j['labelColumn']!='' ) ? $j['labelColumn'] : $j['valueColumn'];

                // a következő két feltétel a rendezés paramétereit tömbökké alakítja
                if (isset($j['orderBy'])) {
                    $orderBy = (is_array($j['orderBy'])) ? $j['orderBy'] : [$j['orderBy']];
                }
                else {
                    $orderBy = [$labelColumn];
                }
                if (isset($j['order'])) {
                    $order = (is_array($j['order'])) ? $j['order'] : [$j['order']];
                }
                else {
                    $order = ['ASC'];
                }

                if (isset($j['preFilterColumn']) and $j['preFilterColumn']!='') {
                    if (in_array('obm_taxon', $j['preFilterValue'])) {
                      $j['preFilterValue'] = array_map(function($col) {
                        return ($col === 'obm_taxon') ? $_SESSION['st_col']['SPECIES_C'] : $col;
                      }, $j['preFilterValue']);
                    }
                    if (in_array('obm_login_email', $j['preFilterValue'])) {
                      $j['preFilterValue'] = array_map(function($col) {
                        return ($col === 'obm_login_email') ? $_SESSION['Tmail'] : $col;
                      }, $j['preFilterValue']);
                    }
                    $relation = (isset($j['preFilterRelation'])) ? $j['preFilterRelation'] : null;
                    $preFilter = prepareFilter($j['preFilterColumn'],$j['preFilterValue'],$relation);
                }
                else
                    $preFilter = "1 = 1";

                // ha jól értem a term szűrés csak autocomplete lista előállításból jön be
                // ami így itt van rögzítva a szó eleji ILIKE keresésre
                $search_term = "$term%";
                if (!$startmatch) {
                    $search_term = "%$search_term";
                    $lterm = strtolower($term);
                    array_unshift($orderBy, "CASE WHEN lower($valueColumn) ILIKE '$lterm%' THEN 1 WHEN lower($valueColumn) ILIKE '% $lterm%' THEN 2 ELSE 3 END");
                    array_unshift($order, 'ASC');
                }

                $preFilter .= ($term)
                    ? ' AND ' . prepareFilter($j['valueColumn'], $search_term, 'ILIKE')
                    : '';

                $optionsSchema = (isset($j['optionsSchema']) && $j['optionsSchema'] !== '') ? $j['optionsSchema'] : 'public';
                $filterColumn = ($this->filter_for_pds) ? $j['filterColumn'] : null;
                $this->from_table( compact( 'optionsTable', 'preFilter', 'valueColumn', 'labelColumn', 'filterColumn', 'optionsSchema', 'orderBy', 'order') );
            }
            else {
                log_action('Missing list definition elements',__FILE__,__LINE__);
            }

            if (isset($j['multiselect']) and $j['multiselect']=='true') {
                $this->multiselect = true;
            }

            if (isset($j['selected']) and is_array($j['selected'])) {
                $this->selected = $j['selected'];
            }

            if (isset($j['size']) && is_int($j['size'])) {
                $this->ssiz = "size='{$j['size']}'";
            }

        } else {
            //backward compatibility
            if (preg_match(self::PATTERN,$this->list,$m)) {
                $this->from_table($m);
            } else
                $this->from_list(1);
        }
        /*if (preg_match(self::PATTERN,$this->list,$m)) {
            $this->from_table($m);
        }
        else {
            $this->from_list();
        }*/
    }

    /***************
     * private functions
     * ****************/

    private function from_table($m) {
        global $ID;

        $this->table = $m['optionsTable'];

        if ($this->has_default_value()) {
            $filter = sprintf('%s = %s',$m['valueColumn'],$this->def);
            $this->data_attr = 'data-nested="default"';
        }
        else {
            $filter = $m['preFilter'];
        }

        $orderColumn = implode(', ', array_map(function ($k, $o) {
            return "$o as ord$k";
        }, array_keys($m['orderBy']), $m['orderBy']));

        $order = implode(', ', array_map( function($k, $o) {
            $o = (in_array(strtolower($o),['asc','desc'])) ? strtoupper($o) : "ASC";
            return "ord$k $o";

        },array_keys($m['orderBy']), $m['order'] ) );


        $filter_column = ($this->filter_for_pds) ? ", {$m['filterColumn']} as filter" : "" ;


        $sl = process_trigger(["control" => $this->control,"custom_function" => $this->custom_function,"list_definition" => $this->list_definition]);

        $rules = $this->sensitivity_check();

        if ($sl && $sl['target_column'] != [] && $this->data_attr == '') {
            $this->data_attr = 'data-nested="parent"';
        }
        if ($sl && $sl['foreign_key'] != []) {
            $this->data_attr .= ' data-nested_child="true"';
        }

        if (is_array($sl) && isset($sl['labelAsValue']) && $sl['labelAsValue']) {
            $this->data_attr .= ' data-lav="true"';
        }
        if (!isset($sl['foreign_key']) || $sl['foreign_key'] == 'NULL' || $sl['foreign_key'] == '' || $this->ajaxCall || $this->filter_for_pds) {
            
            // It is good for [any] -> [select]
            //$a = ($this->ajaxCall && $sl['foreign_key']!='' &&  $sl['foreign_key'] != 'NULL' && !in_array($this->type, ['autocomplete','autocompletelist']))
            $a = "";
            if (isset($sl['foreign_key'])) {
                $a = ($this->ajaxCall && $sl['foreign_key']!='' &&  $sl['foreign_key'] != 'NULL' && isset($_POST['condition']))
                    ? sprintf('AND %s = %s',$sl['foreign_key'], quote($_POST['condition']))
                    : "";
            }

            //SELECT DISTINCT  AS foo,  AS bar,  AS ord  FROM public. t  W...
            $cmd = sprintf('SELECT DISTINCT %1$s AS foo, %3$s AS bar, %10$s %11$s FROM %9$s.%2$s t %6$s WHERE %5$s %7$s %8$s ORDER BY %4$s',
                $m['valueColumn'],
                $this->table,
                $m['labelColumn'],
                $order,
                $filter,
                $rules['join'],
                $rules['filter'],
                $a,
                $m['optionsSchema'],
                $orderColumn,
                $filter_column
            );
            $res = pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                log_action("List definition is wrong",__FILE__,__LINE__);
                log_action($cmd,__FILE__,__LINE__);
                log_action($m,__FILE__,__LINE__);
            }

            while ($row = pg_fetch_assoc($res)) {
                $rowbar = (defined($row['bar'])) ? constant($row['bar']) : $row['bar'];

                $data = (substr($this->data_attr,0,11) == 'data-nested') ? ['fkey' => $row['foo']] : [];
                $value = (is_array($sl) && isset($sl['labelAsValue']) && $sl['labelAsValue'] != '') ? $row['bar'] : $row['foo'];
                $selected = '';
                $disabled = '';
                $label = [$rowbar];
                $filter = ($this->filter_for_pds) ? $row['filter'] : null;
                $this->options[] = compact('value', 'selected', 'disabled', 'label', 'data','filter');
            }

        }
    }

    // old_style : backward compatibility bit
    private function from_list($old_style,$j=null) {

        if (!$old_style) {

            foreach ($j['list'] as $value=>$keys) {
                if ($value === '_empty_')
                    $value = '';
                if (!count($keys)) {
                    // if no label defined e.g. true,false, label is created from the value
                    $keys[] = $value;
                }
                $l_translated = array();
                foreach($keys as $label) {
                    if ($label === 'true') $l_translated[] = constant("str_".$label);
                    elseif ($label === 'false') $l_translated[] = constant("str_".$label);
                    elseif( defined($label)) $l_translated[] = constant($label);
                    else $l_translated[] = $label;
                }
                $disabled = '';
                if (isset($j['disabled']) and is_array($j['disabled']) and array_search($value,$j['disabled'])!==false)
                    $disabled = 'disabled';
                $selected = '';
                if (isset($j['selected']) and is_array($j['selected']) and array_search($value,$j['selected'])!==false)
                    $selected = 'selected';

                $bgImage = '';
                if (isset($j['pictures']) and is_array($j['pictures']) and array_key_exists($value,$j['pictures'])) {
                    $bgImage = $j['pictures'][$value];
                }
                $this->options[] = array('value' => $value, 'selected' => $selected, 'disabled' => $disabled, 'label' => $l_translated, 'bgImage' => $bgImage);
            }

            return;
        }
        /* normál lista név:érték párok
         * female#nőstény:f,male#hím:m
         * female#nőstény:f:disabled,male#hím:m
         * */
        foreach( str_getcsv($this->list,",","'") as $l) {
            //foreach(explode(',',$r2['list']) as $l)
            $tl = mb_split(':',$l);
            $disabled = '';
            $selected = '';
            if(count($tl)>1) {
                // multi lables support; in web form - the first will be used
                #if ($table_type=='file') {
                $l_text = preg_split('/#/',$tl[0]);
                #} else {
                #    $labels = preg_split('/#/',$tl[0]);
                #    $l_text = $labels[0];
                #}

                $l_val = $tl[1];
                if(isset($tl[2]))
                    $disabled = 'disabled';
            } else {
                $l_text = array($l);
                $l_val = $l;
            }
            $l_translated = array();
            foreach($l_text as $l_text_p) {
                if ($l_text_p == 'true') $l_translated[] = constant("str_".$l_text_p);
                elseif ($l_text_p == 'false') $l_translated[] = constant("str_".$l_text_p);
                elseif( defined($l_text_p)) $l_translated[] = constant($l_text_p);
                else $l_translated[] = $l_text_p;
            }

            if(trim($l_val)=='') $l_val=$l_text[0];
            $this->options[] = array('value' => $l_val, 'disabled' => $disabled, 'label' => $l_translated, 'selected' => '');
        }
    }

    private function has_default_value() {
        if (preg_match('/^_list:(.+)/',$this->default_value,$def)) {
            $this->def = $def[1];
            return true;
        }
        else {
            if ($this->type == 'list' && $this->add_empty_line) {
                $this->add_empty_line();
            }
            return false;
        }
    }

    private function add_empty_line() {
        $this->options[] = ['value' => '','label' => [''], 'disabled' => '', 'selected' => '','filter' => ''];
    }

    /****************
     * public functions
     * *****************/

    public function get_data_attribute() {
        return $this->data_attr;
    }

    public function get_options($format = 'html') {
        if ($format == 'array') {
            //debug($this->options,__FILE__,__LINE__);
            return $this->options;
        }
        elseif ($format == 'html') {
            $html = [];


            foreach ($this->options as $row) {

                $dataAttr = '';
                if (isset($row['data']) && !empty($row['data'])) {
                    foreach ($row['data'] as $dataKey => $dataValue) {
                        $dataAttr .= "data-$dataKey=\"$dataValue\" ";
                    }
                }
                $html[] = "<option value=\"{$row['value']}\" {$row['disabled']} {$row['selected']} $dataAttr>{$row['label'][0]}</option>";
            }
            return $html;
        }
        else
            log_action('Unknown format',__FILE__,__LINE__);

    }

    /* Sensitivity check and rules table join for dynamic select lists */
    public function sensitivity_check() {
        $tid = (!isset($_SESSION['Tid'])) ? 0 : $_SESSION['Tid'];
        $tgroups = (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='') ? 0 : $_SESSION['Tgroups'];

        $rules['filter'] = '';
        $rules['join'] = '';

        if ($this->table == PROJECTTABLE.'_taxon') return $rules;

        $st_col = st_col($this->table,'array');

        if ($st_col['USE_RULES'] && get_option('upload_lists_access_control')) {
            if (!has_access('master')) {
                $rules['filter'] = " AND ( (sensitivity::varchar IN ('1','no-geom','3','only-owner') AND ARRAY[$tgroups] && read)  OR sensitivity::varchar IN ('0','public','2','restricted','sensitive') ) ";
                $rules['join'] = join_table('rules',['id_col' => 't.obm_id', 'qtable' => $this->table]);
            }
        }
        return $rules;
    }
}
/* terms and conditions modal dialog
 *
 * */
function agree_new_terms() {
    global $BID;

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $languages = array_keys(LANGUAGES);

    if (isset($_SESSION['agree_new_terms_lang'])) {
        # Language setting after accepting invitation    
        if (($key = array_search($_SESSION['agree_new_terms_lang'], $languages)) !== false) {
            unset($languages[$key]);
            array_unshift($languages,  $_SESSION['agree_new_terms_lang']);
        }
    } elseif (defined('LANG')) {
        # Language setting after password reminder
        if (($key = array_search(constant('LANG'), $languages)) !== false) {
            unset($languages[$key]);
            array_unshift($languages,  constant('LANG'));
        }
    }

    $consts = ['str_privacy_policy','str_terms_and_conditions','str_cookie_policy','str_private_policy_info','str_policy_agree_info','str_agree_new_terms','str_agree_and_continue','str_decline'];

    $cmd = sprintf("SELECT * FROM translations WHERE (scope = 'global' OR project = '%s') AND lang IN (%s) AND const IN (%s)", 
        PROJECTTABLE, implode(',',array_map('quote',$languages)), implode(',', array_map('quote',$consts)));

    if ($res = pg_query($BID, $cmd)) {
        while ($row = pg_fetch_assoc($res)) {
            $translations[$row['lang']][$row['scope']][$row['const']] = $row['translation'];
        }
    }

    if (isset($_SESSION['Tid']) and (!isset($_SESSION['Tterms_agree']) or (isset($_SESSION['Tterms_agree']) && !$_SESSION['Tterms_agree'] ) ) ) {
        $out = "<script> $('body').css('overflow-y','hidden'); </script>
            <div id='new-terms-agree-div'>
            <div id='new-terms-container'>";

        // if there is only one languge it should be active by default
        $force_active = (count($languages) == 1) ? 'active' : '';
        foreach ($languages as $L) {
            $active = ($L == 'en') ? 'active' : $force_active;
            $out .= "<span class='terms-langswitch flag-icon flag-icon-$L $active' data-lang='$L' style='margin: 0 5px;'></span>";
        }

        $out .= " <a href='$protocol://".URL."/index.php?logout' class='terms-cancel'>X</a>";

        foreach ($languages as $lang) {
            $tr = [];
            foreach ($consts as $const)
                $tr[$const] = (isset($translations[$lang]['local'][$const])) ? $translations[$lang]['local'][$const] : $translations[$lang]['global'][$const] ;

            $hidden = ($lang == $languages[0]) ? '' : 'hidden';
            $out .= "<div id='terms-$lang' class='terms-lang $hidden'>
            <div id='private-policy-info'>".$tr['str_private_policy_info']."</div>
            ".$tr['str_policy_agree_info']."
            <br>
            ".$tr['str_agree_new_terms']."
            <div style='display: flex; justify-content: space-around; margin: 20px 0;'>
                <a href='' class='termsText pure-button' data-target='privacy'> " . t($tr['str_privacy_policy']) . " </a>
                <a href='' class='termsText pure-button' data-target='terms'> " . t($tr['str_terms_and_conditions']) . " </a>
                <a href='' class='termsText pure-button' data-target='cookies'> " . t($tr['str_cookie_policy']) . " </a>
            </div>
            <div class='terms-text'>ide jön a szöveg</div>
            <div id='new-terms-buttons'>
                <button id='agree-new-terms' class='pure-button button-large button-success'>".$tr['str_agree_and_continue']."</button>
                <button id='drop_my_account' class='pure-button button-large button-warning' style='color:white'>".$tr['str_decline']."</button>
                </div></div>";
        }
        $out .= "
            </div>
            </div>";
        echo $out;
    }
}

/* interconnect_request handling
 * Called in prepare_vars.php when connection request arrived
 * */
function interconnect_request($key,$request_project) {
    global $BID;

    
    // key processing
    $accept_key = '';
    $cmd = sprintf("SELECT accept_key FROM interconnects_slave WHERE accept_key=%s AND remote_project=%s AND pending=false AND local_project=%s",quote($key),quote($request_project),quote(PROJECTTABLE));
    $res = pg_query($BID,$cmd);

    if (pg_num_rows($res)) {
        return true;

    } else {

        $project_masters = new Role('project masters');
        $masters = $project_masters->get_members();
        $M = new Messenger();
        // no connection established yet
        // send alert admin to accept or decline connection request
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.=!+[]$*&@{}#<>";
        $hash = substr( str_shuffle( $chars ), 0, 16 );
        #$accept_key = crypt($hash.microtime(),$request_project);
        $accept_key = uniqid(substr($request_project,0,9),true);

        $cmd = sprintf("INSERT INTO interconnects_slave (local_project,remote_project,accept_key,pending) VALUES(%s,%s,%s,true)",quote(PROJECTTABLE),quote($request_project),quote($accept_key));
        $res = pg_query($BID,$cmd);

        $names = implode(', ', array_map(function($m) { return $m->name; }, $masters));
        $b64_request_project = base64_encode($request_project);

        foreach ($masters as $master) {
            $M->send_system_message($master, 'interconnect_request', compact('request_project', 'b64_request_project', 'names', 'key', 'accept_key'), true);
        }

    }

    return true;
}
/* Interconnect function
 * local admin decision to accept or reject remote OBM server/project connection
 * Called in prepapre_vars.php
 * */
function interconnect_key_accept($decision,$accept_key,$request_key,$request_project) {
    global $BID,$ID;

    if ($decision=='accept') {
        $cmd = sprintf("UPDATE interconnects_slave SET pending=false WHERE accept_key=%s AND remote_project=%s AND local_project=%s",quote($accept_key),quote(base64_decode($request_project)),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);

        if (pg_affected_rows($res)) {

            $answer = file_get_contents("http://".base64_decode($request_project)."/index.php?interconnect_request_decision=accept&accept_key=$accept_key&request_key=$request_key");
            // update temporary_tables post file
            $old_session_id = '';
            $cmd = sprintf("SELECT table_name FROM system.temp_index WHERE interconnect='t' AND table_name ~ 'upload_%s_[A-Za-z0-9]+_nk_%s'",PROJECTTABLE,$request_key);
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if (preg_match('/upload_'.PROJECTTABLE.'_([A-Za-z0-9]+)_nk_'.$request_key.'/',$row['table_name'],$m)) {
                    $old_session_id = $m[1];
                }
            }

            $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_wk_%4$s\' WHERE interconnect=\'t\' AND table_name=\'upload_%1$s_%2$s_nk_%3$s\'',PROJECTTABLE,$old_session_id,$request_key,$accept_key);
            if (pg_query($ID,$cmd)) {
                $cmd = sprintf('ALTER TABLE temporary_tables."upload_%1$s_%2$s_nk_%3$s" RENAME TO "upload_%1$s_%2$s_wk_%4$s"',PROJECTTABLE,$old_session_id,$request_key,$accept_key);
                $res = pg_query($ID,$cmd);

                return $accept_key;
            }
        } else {
            log_action('Error in interconnects update',__FILE__,__LINE__);
        }


    } elseif($decision=='decline') {
        $cmd = sprintf("DELETE FROM interconnects_slave WHERE pending=true AND accept_key=%s AND remote_project=%s AND local_project=%s",quote($accept_key),quote(base64_decode($request_project)),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);

        $answer = file_get_contents("http://".base64_decode($request_project)."/index.php?interconnect_request_decision=decline&accept_key=$accept_key&request_key=$request_key");

        // Delete post data from temporary_tables
        // upload_sandbox_q201ltm400uhnp56hj80p6k740_nk_request_key
        $cmd = sprintf("DELETE FROM system.temp_index WHERE interconnect='t' AND table_name ~ 'upload_%s_[A-Za-z0-9]+_nk_%s'",PROJECTTABLE,$request_key);
        $res = pg_query($ID,$cmd);

        if (pg_affected_rows($res))
            return true;
        else
            log_action('Error in interconnects update',__FILE__,__LINE__);
    }
    return false;
}
/* Interconnect request sent and answered: "request received"
 * Called in afuncs.php as an AJAX action: User initiated a data sharing
 * */
function interconnect_master_connect($remote_project) {
    global $BID;

    $cmd = sprintf("SELECT accept_key FROM interconnects_master WHERE local_project=%s AND remote_project=%s AND accept_key!='' AND pending=false",quote(PROJECTTABLE),quote($remote_project));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return array('accept_key'=>$row['accept_key']);
    }

    // generating a request key
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.=!+[]$*&@{}#<>";
    $hash = substr( str_shuffle( $chars ), 0, 16 );
    #$request_key = crypt($hash.microtime(),$remote_project);
    $request_key = uniqid(substr($remote_project,0,9),true);

    $cmd = sprintf("SELECT 1 FROM interconnects_master WHERE pending=true AND local_project=%s AND remote_project=%s",quote(PROJECTTABLE),quote($remote_project));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        return array('failed','There is an active connection request to this project which not yet accepted by remote admin. Please wait for response and share this data again.');
    }

    $cmd = sprintf("INSERT INTO interconnects_master (local_project,remote_project,request_key,pending) VALUES(%s,%s,%s,true)",quote(PROJECTTABLE),quote($remote_project),quote($request_key));
    $res = pg_query($BID,$cmd);
    if (pg_affected_rows($res))
        return array('request_key'=>$request_key);
    else {
        log_action("failed to insert request_key to interconnects_master",__FILE__,__LINE__);
        return array('failed','An error occured in key processing.');;
    }
}
/* Remote project's admin answered: accept or decline
 * Called: prepare_vars.php
 * when remote admin accepted or declined connection request
 * */
function interconnect_got_decision($decision,$accept_key,$request_key) {
    global $BID;

    if ($decision == 'accept') {
        $cmd = sprintf("UPDATE interconnects_master SET accept_key=%s,pending=false WHERE request_key=%s AND pending=true AND local_project=%s",quote($accept_key),quote($request_key),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res))
            return true;
    } else if ($decision == 'decline') {
        $cmd = sprintf("DELETE FROM interconnects_master WHERE request_key=%s AND pending=true AND local_project=%s",quote($request_key),quote(PROJECTTABLE));
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res))
            return true;
    }
    return false;
}

/**
 * Converts array to the WHERE part of an SQL string
 *
 * @param string|array $col the column or the list of columns used in the filter
 * @param string|array $val the value or list of values used in the filter
 * @param string|array $rel optional parameter if relation is other then '=' or 'IN'
 * @param string $alias optional - adds an alias before the column names
 * complex example
 * $col = ['col1','col2','col3']
 * $val = ['val1','val2',['val3a',val3b']]
 * $rel = ['=','<','IN']
 *
 * @return string $preFilter
 *
 * $preFilter == "col1 = 'val1' AND col2 < 'val2' AND col3 IN ('val3a','val3b') "
 *
 * autocomplete list - results list generation:
 *    col,"$term%",'ILIKE'
 *    $preFilter == "col" ILIKE 'term%'
 *
 **/
function prepareFilter($col, $val, $rel = NULL, $alias = NULL) {
    if (is_null($col) || is_null($val)){
        log_action('ERROR: filter col or val missing');
        return;
    }
    if (!is_array($col) && !is_array($val)) {
        $col = array($col);
        $val = array($val);
    }
    elseif (!is_array($col) && is_array($val)) {
        $col = array($col);
        $val = array($val);
    }
    elseif (is_array($col) && (!is_array($val) || (count($val) !== count($col)))) {
        log_action('ERROR: Pre filter column and value number do not match');
        return;
    }

    $n = count($col);
    if (isset($rel)) {
        if (!is_array($rel))
            $rel = array($rel);
        if ($n !== count($rel))
            log_action('WARNING: pre-filter columns, values and relations number do not match!',__FILE__,__LINE__);
    }
    
    if ($alias) {
        $col = array_map(function ($c) use ($alias) {
            return "$alias.$c";
        }, $col);
    }
    
    $preFilter = [];
    for ($i = 0; $i < $n; $i++) {
        if (is_array($val[$i])) {
            $value = "(".implode(',',array_map('quote',$val[$i])) . ")";
            $relation = (isset($rel[$i])) ? $rel[$i] : 'IN';
        }
        else {
            if ($val[$i] === 'NULL') {
                $value = $val[$i];
                $relation = (isset($rel[$i])) ? $rel[$i] : 'IS';
            }
            else {
                $value = quote($val[$i]);
                $relation = (isset($rel[$i])) ? $rel[$i] : '=';
            }
        }

        $preFilter[] = sprintf( " %s %s %s ", $col[$i], $relation, $value);
    }
    return implode('AND',$preFilter);
}

/**
 * Converts arguments to the ORDER BY part of an SQL string
 *
 * @param string|array $by the column or the array of columns to be ordered by
 * @param string|array $dir the direction or list of directions of ordering
 * complex example
 * $by = ['col1','col2']
 * $dir = ['ASC','DESC']
 *
 * @return string "ORDER BY col1 ASC, col2 DESC"
 **/
function prepareOrder($by, $dir = NULL) {
    if (!is_array($by)) {
        $by = [$by];
    }
    if (is_string($dir)) {
        $dir = [$dir];
    }
    if (is_null($dir) || (is_array($dir) && count($by) != count($dir))) {
        $dir_bak = $dir;
        $dir = [];
        for ($i=0; $i < count($by); $i++) {
            $dir[$i] = $dir_bak[$i] ?? 'ASC';
        }
    }
    
    // checking for correct input
    $matches = array_filter($by, function ($b) {
        return preg_match('/^\w+$/', $b);
    });
    if (count($matches) != count($by)) {
        log_action('order by strings contain unsupported characters!', __FILE__, __LINE__);
        return "";
    }
    
    $orderby = array_map(function ($o, $d) {
        $d = (in_array(strtolower($d), ['asc', 'desc'])) ? strtoupper($d) : 'ASC';
        return "$o $d";
    }, $by, $dir);
    
    return "ORDER BY " . implode(', ', $orderby);
}

class input_field {
    private $defaults = [
        'element' => 'input',
        'type' => 'text',
        'id' => '',
        'class' => '',
        'placeholder' => '',
        'alt' => '',
        'checked' => '',
        'disabled' => '',
        'max' => '',
        'min' => '',
        'maxlength' => '',
        'minlength' => '',
        'size' => '',
        'multiple' => '',
        'name' => '',
        'pattern' => '',
        'required' => '',
        'step' => '',
        'value' => '',
        'empty_line' => true,
        'tdata' => [],
        'data_atts' => '',
        'onlyoptions' => false,
        'data' => [],
        'selected' => '',
        'style' => 'width: 100%',
        'rows' => '',
        'cols' => '',
        'debug' => false,
        'checked' => '',
    ];
    private $args = [];

    function __construct($args) {
        // reading the arguments
        foreach ($this->defaults as $prop => $value) {
            $this->args[$prop] = (!isset($args[$prop])) ? $value : $args[$prop] ;
        }

        if (!empty($this->args['tdata'])) {
            $this->args['data_atts'] = $this->tdata($this->args['tdata']);
        }

        if ($this->args['debug'])
            log_action($this->args,__FILE__,__LINE__);
    }

    public function getElement() {
        $type = 'push_'.$this->args['element'];
        return $this->$type();
    }

    // creates an input field for autocomplete input
    private function push_input() {

        $input_atts = ['id','type','class','placeholder','name','pattern','autofocus','autocomplete','max','min','maxlength','required','size','step','value', 'checked'];
        $atts = [];
        foreach ($input_atts as $attr) {
            if (isset($this->args[$attr]) && $this->args[$attr]!=='')
                $atts[] = "$attr='{$this->args[$attr]}'";
        }
        return sprintf("<input %s %s >",implode(' ',$atts),$this->args['data_atts']);
    }

    // creates a select input fields with options queried from a table
    private function push_select($filter='') {
        $op = ($this->args['empty_line']) ? "<option value=''></option>" : "";

        if (($this->args['placeholder'] != '') && ($this->args['selected'] == ''))
            $op .= "<option hidden selected disabled value=''>{$this->args['placeholder']}</option>";

        //ordering the list by the translated strings
        $this->args['data'] = array_map(function ($row) {
            $value = $row['value'];
            $label = (isset($row['label'])) ? $row['label'] : $row['value'];
            $row['label'] = (defined($label)) ? constant($label) : $label;
            return $row;
        }, $this->args['data']);

        $labels = array_column($this->args['data'],'label');
        array_multisort($labels, SORT_ASC, $this->args['data']);


        foreach ($this->args['data'] as $row) {
            if ($this->args['size'] > 1 and $row['value'] == '') {
                continue;
            }

            $value = $row['value'];
            $label = $row['label'];
            $tdata = (isset($row['tdata'])) ? $this->tdata($row['tdata']) : '';

            if (isset($this->args['boolen']) and $label == 1) {
                $label = str_true;
            } elseif (isset($this->args['boolen']) and $label == 0) {
                $label = str_false;
            }

            $selected = (($this->args['selected']) && $this->args['selected'] === $value) ? 'selected' : '';

            $op .= sprintf('<option value="%1$s" title="%2$s" %4$s %3$s>%2$s</option>',$value,$label,$tdata, $selected);
        }

        $c = count($this->args['data']);
        if ($this->args['size'] > 1 and $c < $this->args['size'])
            $this->args['size'] = $c;

        $select_atts = ['id','class','name','autofocus','required','size','disabled','multiple','style'];
        $atts = [];
        foreach ($select_atts as $attr) {
            if (isset($this->args[$attr]) && ($this->args[$attr]))
                $atts[] = "$attr='{$this->args[$attr]}'";
        }

        $m = sprintf("<select %s %s>", implode(' ',$atts), $this->args['data_atts']);

        if ($this->args['onlyoptions'])
            return $op;
        else
            return $m.$op."</select>";
    }

    //creates a textarea input field
    private function push_textarea() {
        $textarea_atts = ['autocomplete','autofocus','cols','disabled','id','class','placeholder','name','minlength','maxlength','required','rows'];
        $atts = [];
        foreach ($textarea_atts as $attr) {
            if (isset($this->args[$attr]) && $this->args[$attr])
                $atts[] = "$attr='{$this->args[$attr]}'";
        }
        return sprintf("<textarea %s>%s</textarea>", implode(' ',$atts), $this->args['value']);
    }

    public function remove_empty_line() {
        $this->empty_line = false;
    }

    function format_query($e) {
        $this->format_query = $e;
    }

    private function tdata($arr) {
        $tdata = '';
        foreach ($arr as $target=>$value) {
            $tdata .= "data-" . strtolower($target) . "='$value' ";
        }
        return $tdata;
    }
}

if (! function_exists( 'read_jobs' ) ) {
    function read_jobs() {

        $jobs_dir = getenv('PROJECT_DIR') . '/jobs/';
        if (!is_writable($jobs_dir)) {
            return ['error' => 'Jobs directory missing or not writable!'];
        }

        $table = file($jobs_dir.'jobs.table');
        $jobs = array();
        foreach($table as $t) {
            $te = preg_split('/ /',$t);
            $jobs[trim($te[3])] = array($te[0],$te[1],$te[2]);
        }
        return $jobs;
    }
}
if (!function_exists('write_jobs')) {
    function write_jobs($jobs) {
        if (is_null($jobs)) {
            return ['error' => 'write_jobs argument missing'];
        }

        $jobs_dir = getenv('PROJECT_DIR') . '/jobs/';
        if (!is_writable($jobs_dir)) {
            return ['error' => 'Jobs directory missing or not writable!'];
        }
        $content = '';
        foreach ($jobs as $job => $times) {
            $row = implode(' ',$times) . ' ' . $job . "\n";
            if (!preg_match('/^(\d{1,2}|\*) (\d{1,2}|\*) (\d|\*) ([[:alnum:]._-]+)/', $row, $output_array)) {
                return ['error' => "Syntax error: $row"];
            }
            $content .= $row;
        }

        return (file_put_contents($jobs_dir.'jobs.table', $content) !== false);

    }
}
# return list of admin pages belonging to a group
# e.g.
# mapserv: operator, ...
function get_admin_pages_list($group = '%') {
    global $BID;

    $ap = obm_cache('get', 'admin_page_list_' . $group);
    if ($ap !== false) {
        return $ap;
    }
    
    $cmd = sprintf("SELECT * FROM admin_pages WHERE \"group\" LIKE %s;", quote($group));
    if (!$res = pg_query($BID,$cmd))
        return [];

    $ap = [];
    $results = pg_fetch_all($res);
    if ($group=='%')
        foreach ($results as $p) {
            $label = defined($p['label']) ? constant($p['label']) : $p['label'];
            $ap[] = array('label'=>$label,'page'=>$p['name'],'group'=>$p['group'],'filename'=>$p['filename']);
        }
    else {
        foreach ($results as $p) {
            $ap[defined($p['label']) ? constant($p['label']) : $p['label']] = $p['name'];
        }
    }
    
    obm_cache('set', 'admin_page_list_' . $group, $ap, 300);
    return $ap;
}
/* JSON Validator
 * Params:
 *      a JSON string
 *      a JSON schema object -- json_decode(...)
 * */

# SchemaStorage not works!!!
#use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use JsonSchema\Constraints\Factory;
function json_schema_validator($json_string,$jsonSchemaName=null,$jsonSchemaObject=null) {

    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    $jsonToValidateObject = json_decode($json_string);
    if ($jsonToValidateObject===null) {
        if (!function_exists('json_last_error_msg')) {
            function json_last_error_msg() {
                static $ERRORS = array(
                    JSON_ERROR_NONE => 'No error',
                    JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
                    JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
                    JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
                    JSON_ERROR_SYNTAX => 'Syntax error',
                    JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
                );

                $error = json_last_error();
                return isset($ERRORS[$error]) ? $ERRORS[$error] : 'Unknown error';
            }
        }

        return common_message("error",json_last_error_msg());
    }

    // Schema storage method not works...
    if ( $jsonSchemaObject!==null ) {
        #$schemaStorage = new SchemaStorage();
        #$schemaStorage->addSchema('file://mySchema', $jsonSchemaObject);
        #$jsonValidator = new Validator( new Factory($schemaStorage));
        #$jsonValidator->validate($jsonToValidateObject, $jsonSchemaObject);
    } else if ($jsonSchemaName!==null) {
        $jsonValidator = new Validator();
        $jsonValidator->validate($jsonToValidateObject, (object)['$ref' => 'file://' . realpath($jsonSchemaName)]);
    }

    if ($jsonValidator->isValid()) {
        return common_message("ok","The supplied JSON validates against the schema.");
    } else {
        $errors = "";
        foreach ($jsonValidator->getErrors() as $error) {
            $errors .= sprintf("[%s] %s\n", $error['property'], $error['message']);
        }
        return common_message("error", $errors);
    }

}
// List of Schemas in gisdata database excluding special schemas
// returning an array
function getProjectSchemas() {
    global $ID;
    $cmd = sprintf("SELECT nspname FROM pg_catalog.pg_namespace ORDER BY nspname");
    $res = pg_query($ID,$cmd);
    $schemas = array();
    while ($row = pg_fetch_assoc($res)) {
        if (preg_match('/^pg_/',$row['nspname'])) continue;
        if ($row['nspname'] == 'information_schema') continue;
        if ($row['nspname'] == 'temporary_tables') continue;

        $schemas[] = $row['nspname'];
    }
    return $schemas;
}
// List of tables in a gives Schema in gisdata database
// returning an array
function getDataTables($schema='public') {
    global $ID;
    if ($schema == '') $schema = 'public';
    $tables = array();
    $cmd = sprintf("SELECT * FROM information_schema.tables WHERE table_schema = %s ORDER BY table_name",quote($schema));
    $res = pg_query($ID,$cmd);
    while ($row = pg_fetch_assoc($res)) {
        $tables[] = $row['table_name'];
    }
    return $tables;
}
// List of registered tables/views belongs to a given PROJECT
// returning an array
function getProjectTables($width_schema=false) {
    
    $tables = obm_cache('get', 'projectTables', '', '', false);
    if ($tables !== false) {
        return $tables;
    }
    
    global $BID;
    $cmd = sprintf("SELECT f_project_table,f_project_schema FROM header_names WHERE f_project_name='%s' ORDER BY f_project_table",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $tables = array();
    while ($row = pg_fetch_assoc($res)) {
        if ($width_schema)
            $tables[] = $row['f_project_schema'].".".$row['f_project_table'];
        else
            $tables[] = $row['f_project_table'];
    }
    obm_cache('set', 'projectTables', $tables, 60, false);
    return $tables;
}
function getColumnsOfTables($table = null,$schema = 'public') {
    global $GID;
    if ($schema == '') $schema = 'public';
    if (!$table) {
        $table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    } 
    $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_name=%s AND table_schema=%s ORDER BY column_name",quote($table),quote($schema));
    $result = pg_query($GID,$cmd);
    $columns = array();
    while($row=pg_fetch_assoc($result)) {
        $columns[] = $row['column_name'];
    }
    return $columns;
}
function getDistinctElementsOfColumn($column,$table,$schema='public') {
    global $ID;
    if ($schema == '') $schema = 'public';
    $cmd = sprintf('SELECT DISTINCT %1$s AS dvalues FROM %3$s.%2$s ',$column,$table,$schema);
    $result = pg_query($ID,$cmd);
    $values = array();
    while($row=pg_fetch_assoc($result)) {
        $values[] = $row['dvalues'];
    }
    return $values;
}
function getNullFormId($table,$editform=false) {
    global $BID,$ID;
    if ($editform)
        // null edit form
        $cmd = sprintf("SELECT form_id FROM project_forms WHERE project_table='".PROJECTTABLE."' AND destination_table=%s AND form_name='null-editform'",quote($table));
    else
        // null insert form
        $cmd = sprintf("SELECT form_id FROM project_forms WHERE project_table='".PROJECTTABLE."' AND destination_table=%s AND form_name='nullform'",quote($table));
    $res = pg_query($BID,$cmd);
    $nullform_id = 0;
    while($row = pg_fetch_assoc($res)){
        $nullform_id = $row['form_id'];
    }

    if ($nullform_id == 0) {
        // create null-form
        if ($editform)
            $formname = 'null-editform';
        else
            $formname = 'nullform';     // depreceted - normal nullform not created automatically any more!

        pg_query($BID,'BEGIN');
        $cmd = sprintf("INSERT INTO project_forms (user_id,project_table,form_name,form_type,form_access,active,groups,description,destination_table,srid,data_groups)
            VALUES (%d,'%s','%s',%s,%s,1,%s,%s,%s,%s,%s) RETURNING form_id",
            $_SESSION['Tid'],PROJECTTABLE,$formname,quote('{file}'),quote(0),quote('{}'),quote(''),quote($table),quote('{}'),quote('{}'));
        $res = pg_query($BID,$cmd);

        if ($res) {
            $row = pg_fetch_assoc($res);
            $nullform_id = $row['form_id'];
        }
        if ($nullform_id!=0) {
            $cols = dbcolist('array',$table);
            $cols['obm_id'] = 'obm_id';

            $err = 0;
            foreach($cols as $col=>$cv) {
                $obl = 2;
                $type = 'text';

                $cmd = sprintf("SELECT data_type FROM information_schema.columns WHERE table_name = %s AND column_name = '$col'",quote($table));
                $rest = pg_query($ID,$cmd);
                $rowt = pg_fetch_assoc($rest);
                $db_type = $rowt['data_type'];


                if ($editform)
                    if ($col=='obm_id') { $obl = 1;$type = 'numeric';}
                else
                    if ($col=='obm_id') continue; #{ $obl = 1;$type = 'numeric';}
                // automatic type selection
                if ($db_type=='integer') $type = 'numeric';
                elseif ($db_type=='numeric') $type = 'numeric';
                elseif ($db_type=='real') $type = 'numeric';
                elseif ($db_type=='double precision') $type = 'numeric';
                elseif ($db_type=='smallint') $type = 'numeric';
                elseif ($db_type=='date') $type = 'date';
                elseif (preg_match('/^timestamp with/',$db_type)) $type = 'datetime';
                elseif (preg_match('/^time /',$db_type)) $type = 'time';
                elseif (preg_match('/^character varying \((\d+)\)/',$db_type)) $type = 'text';
                elseif ($col=='obm_files_id') $type = 'file_id';
                elseif ($col=='obm_geometry') $type = 'wkt';
                elseif ($db_type == 'boolean') $type = 'boolean';
                elseif ($db_type == 'ARRAY') $type = 'array';

                $cmd = sprintf("INSERT INTO project_forms_data (form_id,\"column\",description,type,control,list_definition,
                                                        count,regexp,obl,fullist,default_value,
                                                        api_params,relation,spatial,pseudo_columns,custom_function,position_order)
                                                VALUES (%d,%s,%s,%s,%s,%s,
                                                        %s,%s,%d,%d,%s,
                                                        %s,%s,%s,%s,%s,%s)",
                $nullform_id,quote($col),quote(''),quote($type),quote('nocheck'),quote(''),
                quote(''),quote(''),$obl,0,quote(''),quote(''),quote(''),quote(''),quote(''),quote(''),quote(''));

                $r = pg_query($BID,$cmd);
                if (!$r) {
                    $err++;
                    log_action($cmd.__FILE__,__LINE__);
                }

            }
            if ($err)
                pg_query($BID,'ROLLBACK');
            else
                pg_query($BID,'COMMIT');
        }
    }

    return $nullform_id;
}

# Set role option for a functions
function set_option($option_name, $option_value, $role_id = NULL) {
    global $BID;

    $cmd = ($role_id)
        ? sprintf('WITH u AS (UPDATE project_options SET option_value = %3$s WHERE project_table = \'%1$s\' AND option_name = %2$s AND role_id = %4$s RETURNING id) INSERT INTO project_options (project_table, option_name, option_value, role_id) SELECT \'%1$s\', %2$s, %3$s, %4$s WHERE NOT EXISTS (SELECT id FROM u);', PROJECTTABLE, quote($option_name), quote($option_value), quote($role_id))
        : sprintf('WITH u AS (UPDATE project_options SET option_value = %3$s WHERE project_table = \'%1$s\' AND option_name = %2$s AND role_id IS NULL RETURNING id) INSERT INTO project_options (project_table, option_name, option_value) SELECT \'%1$s\', %2$s, %3$s WHERE NOT EXISTS (SELECT id FROM u);', PROJECTTABLE, quote($option_name), quote($option_value));

    if (! $res = pg_query($BID,$cmd) ) {
        log_action('get_option query error',__FILE__,__LINE__);
        return false;
    }
    obm_cache('set', $option_name . $role_id, $option_value, 300);
    return true;
}

# Get role option for a functions
function get_option($option, $role_id = false) {
    global $BID;
    $option_value = obm_cache('get',$option . $role_id);
    if ($option_value !== false) {
        return $option_value;
    }
    if ($role_id) {
        $role = new Role($role_id);
        $role_id = ($role->is_user) ? $role->get_member_ids()[0] : false;
    }
    $cmd = ($role_id)
        ? sprintf("SELECT option_value, role_id FROM project_options WHERE project_table = '%s' AND option_name = %s AND (role_id IS NULL OR role_id = '%s') ORDER BY role_id LIMIT 1;", 
            PROJECTTABLE, 
            quote($option), 
            $_SESSION['Trole_id'])
            : sprintf("SELECT option_value, role_id FROM project_options WHERE project_table = '%s' AND option_name = %s AND role_id IS NULL LIMIT 1;", 
                PROJECTTABLE, 
                quote($option));
    if (! $res = pg_query($BID,$cmd) ) {
        log_action('get_option query error',__FILE__,__LINE__);
        return false;
    }
    if (pg_num_rows($res)) {
        $result = pg_fetch_assoc($res);
        $option_value = $result['option_value'];
    } else {
        $option_value = false;
    }

    obm_cache('set', $option . $role_id, $option_value, 300);
    return $option_value;
}

if (file_exists(getenv('PROJECT_DIR').'/local/includes/private/custom_functions.php')) {
    require_once(getenv('PROJECT_DIR').'/local/includes/private/custom_functions.php');
}
function setExpires($expires) {
    header(
    'Expires: '.gmdate('D, d M Y H:i:s', time()+$expires).'GMT');
}

function join_table($table,$params) {
    global $x_modules;

    extract($params);
    
    $req_params = [
        'uploadings' => ['qtable_alias'],
        'rules' => ['id_col','qtable'],
        'taxon' => ['species_col'],
        'search' => ['id_col','qtable'],
        'filename' => ['qtable'],
        'grid' => ['id_col','qtable'],
    ];
    foreach ($req_params[$table] as $param) {
        if (!isset($$param)) {
            log_action("ERROR: $param missing",__FILE__,__LINE__);
            return '';
        }
    }
    switch ($table) {
        case 'uploadings':
            return sprintf(' LEFT JOIN system.uploadings ON ("%s".obm_uploading_id=uploadings.id)',$qtable_alias);
            break;

        case 'rules':
            return sprintf(' LEFT JOIN %1$s_rules data_rules ON (data_rules.data_table=\'%2$s\' AND %3$s=data_rules.row_id)',PROJECTTABLE,$qtable, $id_col);
            break;

        case 'taxon':
            $tm_join = $x_modules->_include('taxon_meta', 'table_join');
            
            return sprintf("LEFT JOIN %1\$s_taxon ON %2\$s=word AND lang = '%2\$s' %3\$s",PROJECTTABLE,$species_col, $tm_join);
            break;

        case 'search':
            return sprintf('LEFT JOIN system.%1$s_linnaeus linn ON (linn.row_id = %2$s)',$qtable, $id_col);
            break;

        case 'filename':
            return sprintf("LEFT JOIN ( SELECT string_agg(reference, ', ') as filename, conid FROM system.file_connect fc LEFT JOIN system.files f ON fc.file_id = f.id WHERE f.project_table = '%s' AND f.data_table = '%s' GROUP BY conid) as fj ON obm_files_id = fj.conid", PROJECTTABLE, $qtable );
            break;

        case 'grid':
            return sprintf('LEFT JOIN %s_qgrids qgrids ON (%s = qgrids.row_id)', $qtable, $id_col);
            break;

        default:
            // code...
            break;
    }
}

function the_project_email() {
    global $BID;
    
    $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
    $row = pg_fetch_assoc($res);
    return $row['email'];
}
function language_selector_widget($options = []) {
    $defaults = [
        'selected' => "",
        'class' => "",
        'id' => "",
        'required' => false
    ];
    
    foreach ($defaults as $key => $value) {
        $$key = $options[$key] ?? $defaults[$key];
    }
    
    $out = sprintf("<select %s %s>", 
        ($id === "") ? "" : "id='$id'",
        ($class === "") ? "" : "class='$class'"
    );
    if ($selected == "") {
        $out .= "<option value='' selected disabled>" . t(str_please_choose) . "</option>";
    }
    foreach (LANGUAGES as $L=>$label) {
        $sel = ($L == $selected) ? "selected" : "" ;
        $out .= "<option value='$L' $sel > $label </option>";
    }
    $out .= "</select>";
    return $out;
}
/* Format bytes to GB,MB,KB,.. */
function formatSizeUnits($bytes) {
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

/**
  * returns the language of the web interface.
  **/
function select_language() {
  if (!defined('LANGUAGES') || !is_array(LANGUAGES) || empty(LANGUAGES)) {
    log_action('LANGUAGES const is not defined or not an array!', __FILE__, __LINE__);
    return 'en';
  }
  if (count(LANGUAGES) > 1) {
    //logined users
    if (isset($_SESSION['Trole_id'])) {
      if (isset($_COOKIE['obm_lang_' . $_SESSION['Tcrypt']])) {
        $browser_lang = $_COOKIE['obm_lang_'. $_SESSION['Tcrypt']];
      }
      elseif ($lang = get_option('language', $_SESSION['Trole_id'])) {
        $browser_lang = $lang;
      }
      else {
        $browser_lang = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2) : array_keys(LANGUAGES)[0];
      }
    }
    else {
      if (isset($_COOKIE['obm_lang'])) {
        $browser_lang = $_COOKIE['obm_lang'];
      }
      else {
        $browser_lang = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2) : array_keys(LANGUAGES)[0];
      }
    }
    return preg_replace('/[^a-z]/i','',substr($browser_lang,0,2));
  } 
  else {
    return array_keys(LANGUAGES)[0];
  }
}

/**
  * Sets the language, and taxon_lang option for the selected role_id
  */
function set_preferred_lang($lang, $role_id) {
  set_option("language", $lang, $role_id);
  $r = new Role($role_id);
  $role_members = $r->get_members();
  $user = $role_members[0];
  if (isset($_SESSION['st_col']['NATIONAL_C']) && in_array($user->options->taxon_lang, $_SESSION['st_col']['NATIONAL_C'])) {
    set_option("taxon_lang", $_SESSION['st_col']['NATIONAL_C'][$lang], $role_id);
  }
  st_col('default_table', 'session', 0);
  return true;
}

function sslEncrypt($TEXT,$KEY) {
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($TEXT, $cipher, $KEY, $options=OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $KEY, $as_binary=true);
    $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    return $ciphertext;
}

function sslDecrypt ($ENCRYPTED_TEXT,$KEY) {
    $c = base64_decode($ENCRYPTED_TEXT);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $KEY, $options=OPENSSL_RAW_DATA, $iv);
    return array($hmac, $original_plaintext, $ciphertext_raw);
}

/**
 * setting the locale is necessary when sorting arrays of strings containing 
 * UTF-8 characters
 */
function obm_set_locale() {
    $locales = [
        'hu' => 'hu_HU',
        'ro' => 'ro_RO',
        'en' => 'en_GB',
        'ru' => 'ru_RU'
    ];
    $l = $locales[$_SESSION['LANG']] ?? 'en-GB';
    setlocale(LC_ALL, $l);
}
/* Returning
 * id, properties and geoms in three arrays
 *
 * */
function gml_processing ($xml) {

    $data = array();
    $geom = array();
    $id = array();

    /*GML processing*/
    if (isset($xml['gml'])) {

        foreach($xml->children($xml['gml']) as $child) {
            
            if (isset($xml['ms'])) {
                
                foreach($child->children($xml['ms']) as $data) {
                    
                    foreach($data->children($xml['ms']) as $i) {
                        
                        if ($i->getName() == 'id') {
                            $id[] = (string) $i;
                        } else {
                            $var = $i->getName();
                            if (!isset($data[$var])) {
                                $data[$var] = array();
                            }
                            $data[$var][] = (string) $i;
                        }
                        foreach($i->children($xml['gml']) as $j){

                            if ($j->getName() == 'MultiPoint') {
                                $tmp1 = array();
                                foreach(GML_coords_from_Mpoints($j,$xml) as $k){
                                    list($x,$y) = explode(' ',$k);
                                    $tmp1[] = array($y,$x);
                                }
                                $geom[] = array('linearRing'=>$tmp1);
                            }
                            if ($j->getName() == 'Point') {
                                list($x,$y) = explode(' ',GML_coords_from_points($j,$xml));
                                $geom[] = array('point'=>array($y,$x));
                            }
                            //echo $j->getName() . ":". GML_coords_from_points($j,$xml);
                            //echo "<br>";
                        }
                    }
                    //foreach($data->children($xml['gml']) as $i){
                        //echo $i->getName() . ":". GML_coords_from_bounds($i,$xml);
                        //echo "<br>";
                    //}
                }
            }
        }
    }
    return (array($data,$geom,$id));
}
/* Used in wfs_processing
 * Arguments: GeoJson
 * Parsing geojson into 3 arrays:
 *      id
 *      geometry
 *      properties
 * */
function geojson_parsing($json) {
    $geom = array();
    $id = array();
    $data = array(); // data not processed here!
 
    foreach($json->{'features'} as $feature) {
        $g = $feature->{'geometry'};
        if ($g != null ) {
            if ($g->{'type'}=='Point') 
                $geom[] = array('point'=>array($g->{'coordinates'}[0],$g->{'coordinates'}[1]));
            elseif ($g->{'type'}=='LineString') {
                $line = array();
                foreach ($g->{'coordinates'} as $xy) {
                    $line[] = array($xy[0],$xy[1]);
                }
                $geom[] = array('line'=>$line);
            }
            elseif ($g->{'type'}=='Polygon') {
                $polygon = array();
                foreach ($g->{'coordinates'}[0] as $xy) {
                    $polygon[] = array($xy[0],$xy[1]);
                }
                $geom[] = array('polygon'=>$polygon);
            }
        } else {
            // 0,0 for no geom data!!!!
            //$geom[] = array('point'=>array(0,0));
        }

        $t = $feature->{'type'};
        $line = array();
        foreach($feature->{'properties'} as $k=>$v) {
            if ($k=='obm_id') {
                $id[] = $v;
            }
            $data[] = $v;
        }
    }
    return (array($data,$geom,$id));

}


function create_query_tmp_table_from_geojson($json,$cols,$col_types) {
    global $ID;
    
    pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));

    $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_query_%s_%s (obm_id integer,%s)",PROJECTTABLE,session_id(),implode(',',$cols));
    $res = pg_query($ID,$cmd);

    if (pg_last_error($ID)) {
        log_action(pg_last_error($ID),__FILE__,__LINE__);
        log_action($cmd,__FILE__,__LINE__);
        return false;

    } else {
        
        update_temp_table_index('aquery_'.PROJECTTABLE.'_'.session_id());

        pg_query($ID, sprintf("COPY temporary_tables.temp_query_%s_%s FROM stdin",PROJECTTABLE,session_id()));

        require_once('vendor/autoload.php');

        // LoadQuery feature collection
        //$num = 0;
        foreach($json->{'features'} as $feature) {
            /*$g = $feature->{'geometry'};
            if ($g->{'type'}=='Point') $post_geom[] = array('point'=>array($g->{'coordinates'}[0],$g->{'coordinates'}[1]));
            elseif ($g->{'type'}=='LineString') {
                foreach ($g->{'coordinates'} as $xy) {
                    $post_geom[] = array('linearRing'=>array($xy[0],$xy[1]));
                }
            }
            elseif ($g->{'type'}=='Polygon') {
                foreach ($g->{'coordinates'} as $xy) {
                    $post_geom[] = array('linearRing'=>array($xy[0],$xy[1]));
                }
            }*/

            $t = $feature->{'type'};
            $line = array();
            foreach($feature->{'properties'} as $k=>$v) {
                /*if ($k=='obm_id') {
                    $post_id[] = $v;
                }*/
                if (isset($col_types[$k])) {
                    if (($col_types[$k]=='integer' or $col_types[$k]=='smallint') and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='numeric' and $v == ''){ 
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='real' and $v == ''){ 
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='geometry' and $v != '') {
                        //$line[$k] = "$v::geometry";
                        $line[$k] = "$v";
                    } elseif ($col_types[$k]=='geometry' and $v == '') {
                        $line[$k] = "\N";
                    } elseif ($col_types[$k]=='boolean' and $v == '') {
                        $line[$k] = "\N";
                    } elseif ($col_types[$k]=='double precision' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='timestamp without time zone' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='date' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='time without time zone' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='time with time zone' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='timestamp with time zone' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='varchar[]' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='varchar[]' and $v != '') {
                        $line[$k] = "{".$v."}";
                    } elseif ($col_types[$k]=='text[]' and $v == '') {
                        $line[$k] = '\N';
                    } elseif ($col_types[$k]=='text[]' and $v != '') {
                        $line[$k] = "{".$v."}";
                    } else {
                        // replace backslashes 
                        $v = preg_replace('/\\\+/','-',$v);
                        $line[$k] = "$v";
                        //if ($num == 972) debug($line);
                    }
                } else {
                    $line[$k] = "$v";
                }
            }
            $line_string = implode("\t",$line);
            pg_put_line($ID, $line_string."\n");
            //$num++;
        }

        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);
        
        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));

        // It can be very slow - ten minutes or more if the query is very big...
        // If I create the table and call parallel inserts??? how?? exec('php parallel.php $id_cmd')
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_%s_%s AS %s",PROJECTTABLE,session_id(),sprintf("SELECT * FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
        $res = pg_query($ID,$cmd);
        if (!pg_last_error($ID)) {

            $cmd = sprintf("GRANT SELECT ON temporary_tables.temp_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
            $res = pg_query($ID,$cmd);
        }

        if (!pg_last_error($ID)) {
            update_temp_table_index(PROJECTTABLE.'_'.session_id());
            return true;
        }

        return false;
    }
}
/* return assoc array of column with postgres types
 *
 * */
function get_type_of_columnslist($c,$schema,$table) {

    global $ID;

    $typecmd = sprintf("SELECT column_name, data_type, udt_name FROM information_schema.columns WHERE table_schema = %s AND table_name = %s",quote($schema),quote($table));
    $res = pg_query($ID,$typecmd);
    $cols = array();
    $col_types = array();
    while ($row = pg_fetch_assoc($res)) {
        $index = array_search($row['column_name'],$c);
        if ($index !== false) {
            if ($row['data_type'] == 'USER-DEFINED') {
                $cols[$row['column_name']] = $row['column_name']." geometry";
                $col_types[$row['column_name']] = 'geometry';
            } elseif ($row['data_type'] == 'ARRAY') {
                if ($row['udt_name'] == '_varchar') {
                    $cols[$row['column_name']] = $row['column_name']." varchar[]";
                    $col_types[$row['column_name']] = 'varchar[]';
                }
                elseif ($row['udt_name'] == '_text') {
                    $cols[$row['column_name']] = $row['column_name']." text[]";
                    $col_types[$row['column_name']] = 'text[]';
                }
            } else {
                $cols[$row['column_name']] = $row['column_name']." ".$row['data_type'];
                $col_types[$row['column_name']] = $row['data_type'];
            }
        } else {
            $cols[$row['column_name']] = $row['column_name']." text";
        }
    }
    // change type USER-DEFINED to geometry
    //$index = array_search('obm_geometry',$c);
    //if ($index !== false)
    //    $cols['obm_geometry'] = "obm_geometry geometry";

    foreach (array_values($c) as $cn) {
        $index = array_search($cn,array_keys($cols));
        if ($index === false)
            $cols[$cn] = "$cn text";
    }
    $arr2ordered = array() ;

    foreach (array_values($c) as $key) {
        $arr2ordered[$key] = $cols[$key];
    }
    $cols = $arr2ordered;
    
    return array($cols,$col_types);

}
/* It is a general shortcat function instead of checking a variable existing before using */
function getval(&$var,$type='string') {
    $rettype = ($type == 'array') ? array() : '';
    $retval = isset($var) ? $var : $rettype;
    return $retval;
}

#xdebug_stop_trace();

function render_view($view, $data, $echo = true) {
    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    if (!$view_exists = view_exists($view)) {
        throw new Exception("view $view does not exist");
    }

    $local = ($view_exists === 'local') ? 'local' : "";
    $m = new Mustache_Engine([
        'loader' => new Mustache_Loader_FilesystemLoader(getenv('PROJECT_DIR') . $local . STYLE_PATH . "/views")
    ]);
    $tpl = $m->loadTemplate($view);
    $v = $tpl->render($data);
    if ($echo) {
        echo $v;
        return;
    }
    return $v;
}

function view_exists($view) {
    if (file_exists(getenv('PROJECT_DIR') . 'local' . STYLE_PATH . "/views/$view.mustache")) {
        return "local";
    }
    elseif (file_exists(getenv('PROJECT_DIR') . STYLE_PATH . "/views/$view.mustache")) {
        return "global";
    }
    else {
        return false;
    }
}

function projectProperties() {
    global $BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
    if (!$res = pg_query($BID,$cmd)) {
        return [];
    }
    $results = pg_fetch_all($res);
    if (pg_num_rows($res) == 0) {
        return [];
    } 
    $lang = $_SESSION['LANG'];
    $props = array_values(array_filter($results, function ($row) use ($lang) {
        return ($row['language'] === $lang);
    }));
    $props = (count($props)) ? $props[0] : [];
    $props['logo'] = ( file_exists(getenv('PROJECT_DIR').'local/styles/app/images/logo.png') ) ? '/local/styles/app/images/logo.png' : STYLE_PATH.'/images/logo.png';
    $props['url'] = "$protocol://" . URL;

    return $props;
}
function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
        }
    });

    return $array;
}
/* Returning 3 arrays and allowed_columns module state
 *      main_cols = array(...
 *      allowed_cols = array(...
 *      allowed_gcols = array(...
 *      allowed_cols_module = false / true
 *
 * */
function allowed_columns ($modules,$current_query_table) {

    $obm_columns = array('obm_id','uploader_name','uploading_date','uploading_id');
    $allowed_cols = $allowed_gcols = $main_cols = array_merge(dbcolist('columns', $current_query_table), $obm_columns);
    $allowed_cols_module = false;

    if ($modules->is_enabled('allowed_columns')) {
        $allowed_cols_module = true;
        if (!isset($_SESSION['load_loadquery']))
            $allowed_cols = array_merge(
                $modules->_include('allowed_columns','return_columns',$main_cols,true),
                $obm_columns
            );
            
        $allowed_gcols = array_merge(
            $modules->_include('allowed_columns','return_gcolumns',$main_cols,true),
            $obm_columns
        );
    } 
    else {
        // drop all column for non logined users if access level is higher than public
        if (!isset($_SESSION['Tid']) and in_array(ACC_LEVEL, ['1', '2', 'login', 'group'])) {
            $allowed_cols = $allowed_gcols = array('obm_id');
        }
    }
    return (compact("main_cols","allowed_cols", "allowed_gcols", "allowed_cols_module"));
}
function data_access_check ($acc = false, $use_rules = false, $allowed_cols = array('obm_id'), $tgroups = 0, $allowed_cols_module = false, $row) {

    $geometry_restriction = (
        $use_rules && !$acc && 
            in_array($row['obm_sensitivity'], ['1', 'no-geom']) && 
                !count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))) );
    $column_restriction = (
        ($use_rules && !$acc 
            && !count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read'])))) || 
                # special cases, rules not configured
                (!$use_rules && !$acc) );

    if (!$acc && in_array(ACC_LEVEL, ['2', 'group']) &&
            !$allowed_cols_module)
                $allowed_cols = array('obm_id');

    return compact("column_restriction", "geometry_restriction", "allowed_cols");
}

function parse_layers_from_msmap() {
    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $grep_layers = array_filter(preg_split('/\n|\r/',$var));
    $var = preg_replace('/NAME/i','',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);
    $map_layers = array_map('trim',array_filter(preg_split('/\n|\r/',$var)));

    $geom_types = array();
    foreach ($grep_layers as $gl) {
        $map = getenv('PROJECT_DIR').'private/private.map';
        ob_start();
        if (preg_match('/"/',$gl)) $quote = "'";
        else $quote = '"';
        passthru("grep $quote$gl$quote $map -A 10|grep -i \" TYPE [pl]\"");
        $var = ob_get_contents();
        ob_end_clean();
        $var = preg_replace('/\s+TYPE\s+/i','',$var);
        $geom_types[] = $var;
    }


    return compact(array("geom_types","map_layers"));
}

function validate_url($url) {
    // Regular expression pattern for URL validation
    $url_pattern = "/^https?:\/\/[^\\s\/$.?#].[^\\s]*$/";

    // Check if the URL matches the pattern
    if (preg_match($url_pattern, $url)) {
        return true;
    } else {
        return false;
    }
}

require_once(getenv('OB_LIB_DIR') . 'healthcheck.php');
?>
