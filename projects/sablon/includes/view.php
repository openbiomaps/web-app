<?php
/* View from the boat
 *
 * Map and data query pages
 *
 *
 * */


/* maintenance scheduler code */

$schedule_day = "2011/12/13";
$from = "23:00";
$length = "2 hours";
$address = "127.0.0.1";
$local_mconf = 0;
$system_mconf = 0;
$mconf = "../maintenance.conf";
$maintenance_message = "";

if (file_exists(getenv('PROJECT_DIR').'maintenance.conf'))
    $local_mconf = filemtime(getenv('PROJECT_DIR').'maintenance.conf');

if (file_exists(getenv('PROJECT_DIR').'../maintenance.conf'))
    $system_mconf = filemtime(getenv('PROJECT_DIR').'../maintenance.conf');

if ($local_mconf>$system_mconf)
    $mconf = getenv('PROJECT_DIR')."maintenance.conf";

if (file_exists($mconf)) {
    $contents = file($mconf); 
    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>"0");

    foreach ($contents as $line) {

        if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $schedule_day = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $from = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $length = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $address = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }

        if (preg_match('/^## start day/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['day'] = 1;
        }
        elseif (preg_match('/^## from/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['from'] = 1;
        }
        elseif (preg_match('/^## length/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['length'] = 1;
        }
        elseif (preg_match('/^## maintenance address/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['address'] = 1;
        }
    }
    $date = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start_format = $date->format('Y-m-d H:i');
    $length = preg_replace("/hour.?/","H",$length);
    $length = preg_replace("/minute.?/","M",$length);
    $date_end = $date->add(new DateInterval('PT'.str_replace(" ","",$length)));
    $date_end_format = $date_end->format('Y-m-d H:i');

    $date_now = new DateTime();
    $interval = $date_now->diff($date_start);
    $diff = $interval->format('%r%d %h %i');

    #var_dump($interval);
    #echo 

    $maintenance_message_span_style = '';
    if ($_SERVER['REMOTE_ADDR']!=$address) {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24) {
            $maintenance_message = "Database maintenance will be scheduled between $date_start_format and $date_end_format!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;';
        }
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "Database maintenance!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;';
            include(getenv('PROJECT_DIR')."maintenance.php");
        }
    } else {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24) {
            $maintenance_message = "Database maintenance will be scheduled between $date_start_format and $date_end_format!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;background-color:pink;width:100%';
        }
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "Database maintenance!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;background-color:pink;width:100%';
        }
    }
}
// **************************************************************************************************** \\


if (!isset($_COOKIE['LoadCookie'])) {
    $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie("LoadCookie", 1, time()+3600,'/',$domain,false,true); 
}

#Header("Cache-Control: no-cache, must-revalidate");
#$offset = 60 * 60 * 24 * 3;
#$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
#Header($ExpStr);
require(getenv('OB_LIB_DIR').'db_funcs.php');

if (session_status() === PHP_SESSION_NONE) {
    session_cache_limiter('nocache');
    session_cache_expire(0);
    session_start();
}

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require(getenv('OB_LIB_DIR').'prepare_vars.php');
require(getenv('OB_LIB_DIR').'languages.php');

$protocol = protocol();

if ($load_mainpage) {
    require(getenv('OB_LIB_DIR').'mainpage.php');
}

if (isset($_SESSION['Tid']))
    require(getenv('OB_LIB_DIR').'languages-admin.php');

if (!defined('str_cookies')) {
    echo "<h2>System updates needed!</h2>
        <h3>No languages/translations, go to <a href='/supervisor.php'>supervisor</a> and use the 'Update global translations from GitLab' function and maybe you need to update your system as well.</h3>";
    exit;
}

if (!isset($_SESSION['private_key']))
    $_SESSION['private_key'] = genhash();

if (!isset($_SESSION['openssl_ivs'])) $_SESSION['openssl_ivs'] = array();

#testing memcached
#$mcache = new Memcached();
#$mcache->addServer('localhost', 11211);
#$mcache->set('key', 'hello openbiomaps');
#log_action($mcache->getAllKeys());

$cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
$res = pg_query($BID,$cmd);
$row = pg_fetch_all($res);
$k = array_search($_SESSION['LANG'], array_column($row, 'language'));
if(!$k) $k = 0;
$OB_project_title = $row[$k]['short'];
//should be changed to markdown process !!!
//https://github.com/michelf/php-markdown/
//https://allinthehead.com/retro/364/dont-parse-markdown-at-runtime
$OB_project_description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/","<br><br>",$row[$k]['long']);
$OB_contact_email = $row[$k]['email'];

$M = new Messenger();

/* Visitor counting
 * 
 * */
track_visitors('index');

if (!isset($_SESSION['st_col'])) {
    st_col('default_table', 'session', 0);
}

/* Customizable MainPage
 * Loaded from mainpage template directory
 * */
if ($load_mainpage) {

    // default is header off
    if (isset($MAINPAGE_VARS['system_header']) and ($MAINPAGE_VARS['system_header']=='on'))
        include(getenv('PROJECT_DIR').STYLE_PATH."/header.php.inc");

    includeMainpage();

    // default is footer on
    if (!isset($MAINPAGE_VARS['system_footer']) or (isset($MAINPAGE_VARS['system_footer']) and ($MAINPAGE_VARS['system_footer']=='on')))
        require_once(getenv('PROJECT_DIR').STYLE_PATH."/footer.php.inc");

    exit;
}

/* Project name and description */

/* Load Shared data as raw JSON
 * saved query results / data as JSON (feature collection)
 * */
if ($load_loaddata) {
    global $BID;
    list($id,$s) = preg_split('/@/',$load_loaddata);
    $cmd = "SELECT result FROM project_repository WHERE id='$id' AND sessionid='$s'"; 
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {
        $row=pg_fetch_assoc($result);
        $j = $row['result'];

        header("Content-type: application/json");
        print $j;
    }

    exit;
}

#debug('load view.php',__FILE__,__LINE__);
?>
<!DOCTYPE html>
<html>
<head>
<?php
if (defined('GOOGLE_ANALYTICS')) { 
    echo "    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=".GOOGLE_ANALYTICS."\"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '".GOOGLE_ANALYTICS."');
    </script>";
}
?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta http-equiv="cache-control" content="private, max-age=604800, must-revalidate" />
<?php
if ($load_map) {
/*    header("Cache-Control: public, max-age=86400");
    $offset = 60 * 60 *24;
    $ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
    header($ExpStr);*/
    #echo '    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />';
    #echo '    <meta http-equiv="Pragma" content="no-cache" />';
    #echo '    <meta http-equiv="Expires" content="0" />';
    #echo '    <meta http-equiv="Cache-Control" content="public, max-age=3600" />';
    #echo '    <meta http-equiv="Expires" content="'.gmdate('D, d M Y H:i:s', time()+3600).' GMT" />';
}
?>

    <title>OBM - <?php echo $OB_project_title ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $protocol ?>://<?php echo URL ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-16x16.png">

    <!-- CSS icons fork-awsome insead of font-awsome!!! -->    
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fork-awesome/css/fork-awesome.min.css?rev=<?php echo rev('css/fork-awesome/css/fork-awesome.min.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />

    <!-- pure -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css?rev=<?php echo rev('css/pure/pure-min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/grids-responsive-min.css?rev=<?php echo rev('css/pure/grids-responsive-min.css'); ?>" />

    <!-- jquery / ui -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.css?rev=<?php echo rev('js/ui/jquery-ui.min.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js?rev=<?php echo rev('js/jquery.cookie.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>

    <!-- obm -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/style.css?rev=<?php echo rev('.'.STYLE_PATH.'/style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/colors.css.php?rev=<?php echo rev('.'.STYLE_PATH.'/colors.css.php'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/page_style.css?rev=<?php echo rev('.'.STYLE_PATH.'/page_style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.css.php?rev=<?php echo rev('includes/modules.css.php'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/vote.css?rev=<?php echo rev('.'.STYLE_PATH.'/vote.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>

    <!-- vue components -->
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/vue-multiselect.min.css?rev=<?php echo rev('css/vue-multiselect.min.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" >
    <?php if (defined('DEV_MODE')) : ?>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue.js?rev=<?php echo rev('js/vue.js'); ?>"></script>
    <?php else: ?>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue.min.js?rev=<?php echo rev('js/vue.min.js'); ?>"></script>
    <?php endif; ?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue-components.js?rev=<?php echo rev('js/vue-components.js'); ?>"></script>
    <script async defer type="text/javascript" src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <script async defer type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue-multiselect@2.1.0"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/linnaeus.js?rev=<?php echo rev('js/linnaeus.js'); ?>"></script>
    

<?php 
// Module hook: external_dependencies
// Include external script dependencies <script></script> tags
echo "<!-- Module js -->\n";
$ed = [];
foreach ($x_modules->which_has_method('external_dependencies') as $m) { 
    $ed = array_merge($ed, $x_modules->_include($m,'external_dependencies'));
}
echo implode("\n", array_unique($ed));
unset($ed);

if (function_exists('custom_script_tags')) {
    custom_script_tags();
}
?>
<?php
//include project's own javascript
//e.g.
//include google api with project level api key

# USING local map.js.php
if ($load_map) {
    if (file_exists(getenv('PROJECT_DIR').'local/mapserver/map.js.php'))
        include(getenv('PROJECT_DIR').'local/mapserver/map.js.php');
}
?>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.js.php?rev=<?php echo rev('includes/modules.js.php'); ?>"></script>
<?php
//Include custom css/js libraries
$custom_css_directory = getenv('PROJECT_DIR').STYLE_PATH.'/custom_css';
$custom_js_directory = getenv('PROJECT_DIR').STYLE_PATH.'/custom_js';

if (is_dir($custom_css_directory)) {
    $files = scandir($custom_css_directory);
    echo "<!-- Custom css -->\n";

    foreach ($files as $file) {
        // Only css files
        if (pathinfo($file, PATHINFO_EXTENSION) === 'css') {
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$protocol://". URL ."". STYLE_PATH ."/{$file}?rev=". rev('.'.STYLE_PATH."/{$file}") ."\"  />\n";
        }
    }
}
if (is_dir($custom_js_directory)) {
    $files = scandir($custom_js_directory);
    echo "<!-- Custom js -->\n";

    foreach ($files as $file) {
        // Only js files
        if (pathinfo($file, PATHINFO_EXTENSION) === 'js') {
            echo "<script type=\"text/javascript\" src=\"$protocol://". URL ."/js/{$file}?rev=". rev("js/$file") ."\"></script>\n";
        }
    }
}
?>
</head>
<?php 

// nyelv váltásnál visszatesz arra az oldalra aol voltunk
// css class nem lehet a page...
$page = (!empty($_SESSION['current_page'])) ? array_keys($_SESSION['current_page'])[0] : 'mainpage'; 

?>
<body id='index' data-page="<?= $page ?>" <?php 

    if ($load_map) {echo "onload='init();";}

    if ($load_getquery) {
        if (!$load_map) echo "onload='";
        echo "loadQueryMap($load_getquery_text);";
        if(!$load_map) echo "'";

    } else if ($load_bookmark) {
        // sql queries stored in the database and called by with their names
        if (!$load_map) echo "onload='";
        echo "loadQueryMap({ \"bookmark\":\"$load_bookmark_label\" });";
        if (!$load_map) echo "'";
    }

    if ($load_map) echo "'"; ?>>

<div id="holder">
<?php
        
    include(getenv('PROJECT_DIR').STYLE_PATH."/header.php.inc");

    // Load accept Terms & Conditions if needed
    agree_new_terms();

   

    // Module hook: modal_dialog
    // Modal dialogs and other hidden elements
    foreach ($x_modules->which_has_method('modal_dialog') as $m) { 
        echo $x_modules->_include($m,'modal_dialog',['index']);
    }
    foreach ($modules->which_has_method('modal_dialog') as $m) { 
        echo $modules->_include($m,'modal_dialog',['index']);
    }

    if (isset($_GET['feedback'])) {
        echo "<script>$(document).ready(function() { $('#bugreport-box').show() });</script>";
    }
    
// Body div
?>
<div id='body'>
<div id='message'><?php echo $load_message; ?></div>
<div id="dialog" title="<?php echo t(str_dialog) ?>"><p id='Dt'></p></div>
<div id="dialog-message" title="<?php echo t(str_message) ?>"><p id='Dm'></p></div>
<div id="dialog-confirm" title="<?php echo t(str_confirm) ?>"><p id='Dtc'></p></div>
<?php

// bugreport box
echo "<div id='bugreport-box'>
    <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:0px'>".str_report_an_error."</h2>
    <div style='padding:5px;text-align:center'>
    <button id='bugreport-cancel' style='position:absolute;top:0;right:0' class='pure-button button-large button-passive'><i class='fa fa-close'></i></button>
    <form class='pure-form pure-u-1'>
        <input type='hidden' id='page' value='profile.php'>

        <fieldset class='pure-group'>
            <input type='text' id='issue-title' maxlength='32' class='pure-u-1' placeholder='".str_subject."'>
            <textarea id='issue-body' style='min-height:10em' class='pure-u-1' placeholder='".str_bug_description."'></textarea>
        </fieldset>
        <button id='bugreport-submit' class='pure-button button-large button-warning pure-u-2-3'>".str_send." <i class='fa fa-lg fa-bug'></i></button>
        
    </form>
    </div>
</div>";

if (!isset($_SESSION['Tterms_agree']) || !$_SESSION['Tterms_agree']) {
    if (defined('OB_PROJECT_DOMAIN')) {
        $domain = OB_PROJECT_DOMAIN;
    } elseif (defined('OB_DOMAIN')) {
        $domain = OB_DOMAIN;
    } else 
        $domain = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];

    if (!isset($_SESSION['cookies_accepted'])) {
        echo "<div id='cookie_div' style='text-align:center;position:fixed;z-index:9999;bottom:0;padding:20px 50px 20px 50px;background-color:black;color:#acacac'>";
        if ($_SESSION['LANG']!='hu') {
            echo "
        This site uses cookies to deliver our services and to offer you a better browsing experience. By using our site, you acknowledge that you have read and understand our <a href='$protocol://$domain/cookies/'>Cookie Policy</a>, <a href='$protocol://$domain/privacy/'>Privacy Policy</a>, and our <a href='$protocol//$domain/terms/'>Terms of Services</a>. Your use of OpenBioMaps’s Products and Services, including the OpenBioMaps Network, is subject to these policies and terms. <button class='pure-button' id='cookie-ok'>Accept</button>";
        } else {
            echo "
        Ez a weboldal Sütiket használ az oldal működésének biztosításához és a jobb felhasználói élmény biztosítása érdekében. A Sütik weboldalon történő használatával kapcsolatos részletes tájékoztatás az <a href='$protocol://$domain/privacy/'>Adatkezelési Tájékoztatóban</a> és a <a href='$protocol://$domain/cookies/'>Süti Tájékoztatóban</a> és a <a href='$protocol://$domain/terms/'>Felhasználói szabályzatban</a> található. <button class='pure-button' id='cookie-ok'>Rendben</button>";
        }
        echo "</div>";
    }
}

// Custom pages includes        
if (isset($_GET['includes']) and file_exists(getenv('PROJECT_DIR') . 'local/includes/custom_pages.php')) {
    $custom_load_page = $_GET['includes'];
    if (isset($_GET['subpage']))
        $clp_subpage = $_GET['subpage'];
    else
        $clp_subpage = '';
    include(getenv('PROJECT_DIR') . 'local/includes/custom_pages.php');
}

// Messages
if (isset($em)) {
    echo $em;    
}

/* DISPLAY MAP div and
 * display the click results in the matrix div  * */
if ($load_mappage) {
    //result slides...
    unset($_SESSION['slide']);
    unset($_SESSION['slides']);
    unset($_SESSION['wfs_array']);
    unset($_SESSION['wfs_full_array']);

?>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/roller.css?rev=<?php echo rev('.'.STYLE_PATH.'/roller.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/mapsdata.css?rev=<?php echo rev('.'.STYLE_PATH.'/mapsdata.css'); ?>" type="text/css" />
    <!--<script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/proj4js-compressed.js"></script>-->

    <!--OpenLayers-->
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/openlayers_v6.14.1_build_ol.js"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/openlayers_v6.14.1_css_ol.css">
    <!--<script src="<?php echo $protocol ?>://<?php echo URL ?>/js/ol.js?rev=(<?php echo rev('js/ol.js'); ?>)"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/ol.css?rev=(<?php echo rev('css/ol.css'); ?>)">-->
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/ol-layerswitcher.css" type="text/css" />
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/ol-layerswitcher.js"></script>
    
    <script>
        obj.load_map_page=1;
    </script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.awesome-cursor.min.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/map_init.js?rev=<?php echo rev('js/map_init.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/map_functions.js?rev=<?php echo rev('js/map_functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/javascript.util.js"></script>
    <!--<script src="https://unpkg.com/jsts@2.3.0/dist/jsts.min.js"></script> -->
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/colour-ring-box.js?rev=<?php echo rev('js/colour-ring-box.js'); ?>"></script>
    
<?php

    //include(getenv('OB_LIB_DIR').'interface.php');
    // map page with filters and map
    //echo load_map();
    require_once(getenv('OB_LIB_DIR').'map-page.php');
    echo $out;
    unset($out);

    // right-side navigator buttons
    //echo load_navigator();
    require_once(getenv('OB_LIB_DIR').'navigator.php');
    echo $out;
    unset($out);

}
/* Data page of a data row => edit attributes * */
elseif ($load_data) {
    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }
    
    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
        $_SESSION['current_query_table'] = $_GET['table'];
    }
    $qtable = $_SESSION['current_query_table'];

    $uri = "/api/data_sheet/prepare_data/load.php";
    $call = iapi::load($uri);
    $r = json_decode($call,true);
    if ($r['status'] == 'success') {
        if (view_exists("data-sheet-page__$qtable")) {
            render_view("data-sheet-page__$qtable", $r['data']);
        }
        else {
            render_view('pages/data-sheet-page', $r['data']);
        }
    }
    else {
        render_view('error-page', ['error_message' => $r['message']]);
    }
    $_SESSION['current_query_table'] = $restore_cqt;
}
/* History pages * */
elseif ($load_datahistory) {

    if (!isset($_SESSION['current_query_table']))
        $_SESSION['current_query_table'] = PROJECTTABLE;

    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
            $_SESSION['current_query_table'] = $_GET['table'];
    }

    include(getenv('OB_LIB_DIR').'results_builder.php');
    echo "<h2>".t(str_datahistorypage).": {$_SESSION['getid']}</h2>"; 
    echo "<div style='max-width:100vw;overflow-x: auto;margin-right:30px'>";

    include(getenv('OB_LIB_DIR').'data-history-page.php');

    echo "</div>";
    $_SESSION['current_query_table'] = $restore_cqt;
}
/* Lost Password */
elseif ($load_lostpw and !isset($_SESSION['Tid'])) { ?>
    <br><h2><?php echo t(str_lostpasswd) ?></h2><br>
    <form method=post  class='form pure-form'>
    <?php echo t(str_newactivation) ?><br><br> <input placeholder='<?php echo str_email ?>' size=40 name='loginmail'><br>
    <br>
    <button type='submit' class='button-success button-xlarge pure-button' name='lostpasswd' value='<?php echo t(str_send) ?>'><?php echo t(str_send) ?> <i class="fa fa-send"></i></button>
    <br>
    <br>
    <br>
    </form>
<?php 
}
// metadata page for query
elseif ($load_loadmeta) {
    include(getenv('OB_LIB_DIR').'interface.php');
    echo load_doimetadata('query',$load_loadmeta, $load_loaddoi);
}
/* metadata page for database * */
elseif ($load_doimetadata) {
    include(getenv('OB_LIB_DIR').'interface.php');
    echo load_doimetadata('database','',''); 
}
// DOI page
elseif ($load_loaddoi) {
    global $BID;
    $cmd = sprintf('SELECT 0 as x FROM projects p 
        WHERE p.doi ILIKE %1$s
        UNION
        SELECT id as x FROM project_repository q 
        WHERE q.doi ILIKE %1$s',
            quote('%'.$load_loaddoi));
    
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) {
        $row = pg_fetch_assoc($result);
        if ($row['x'] === '0') {
            $scope = 'database';
        } else {
            $scope = 'query';
        
        }
        include(getenv('OB_LIB_DIR').'interface.php');
        echo load_doimetadata($scope,$load_loadmeta, $load_loaddoi);
        $load_loadmeta = 0;
        $load_doimetadata = 0;
    } else {
        echo "No such DOI here.";
    }
}
/* Voting history page * */
elseif ($load_validhistory) {
    include(getenv('OB_LIB_DIR').'interface.php');
    echo "<p><h2>".t(str_validhist)."</h2><br>";
    echo valid_history(); 
}
/* Upload history page * */
elseif ($load_uploadhistory) {
    include(getenv('OB_LIB_DIR').'interface.php');
    echo "<p><h2>".str_uphist."</h2><br>";
    echo upload_history(); 
}
/* Show Database Summary page
 * would be better in deck.php */
elseif ($load_database) { 
    //include(getenv('OB_LIB_DIR').'interface.php');
    //echo load_database_summary(); 
    require_once(getenv('OB_LIB_DIR').'database-summary-page.php');
    echo $out;
    unset($out);

}
/* Species list * */
elseif ($load_specieslist) {
    include(getenv('OB_LIB_DIR').'interface.php');
?>
    <h2><?php echo $OB_project_title.' - '.str_specieses; ?></h2>
    <div style='padding-top:30px'><?php echo taxonlist(); ?></div>
<?php
}
// no function calls below
/* Login Box * */
elseif ($load_login and !isset($_SESSION['Tid'])) {
    echo login_box();

}
/* Registration process */
elseif ($load_register) {
    echo request_invitation();
    
}
/* Data usage agreement */
elseif ($load_useagreement) { ?>
    <h2><?php echo t(str_data_usage); ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/datauseandshare.php'))
            include(getenv('PROJECT_DIR').'local/datauseandshare.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'at_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'DataUseAndSharingAgreement_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'DataUseAndSharingAgreement.html');
        ?>    
    </div>
<?php
}
/* Privacy policy */
elseif ($load_privacy) { ?>
    <h2><?php echo t(str_privacy_policy) ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/privacy.php'))
            include(getenv('PROJECT_DIR').'local/privacy.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'privacy_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'privacy_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'privacy.html');
        ?>    
    </div>
<?php 
}
/* Terms and conditions */
elseif ($load_terms) { ?>
    <h2><?php echo str_terms_and_conditions ?></h2>
    <div class='preformatted_text left-padding'>
    <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/terms.php'))
            include(getenv('PROJECT_DIR').'local/terms.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'terms_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'terms_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'terms.html');
    ?>  
    </div>
<?php 
}
/* Server technical info */
elseif ($load_technical) { ?>
    <h2><?php echo str_technical_info ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/technical.php'))
            include(getenv('PROJECT_DIR').'local/technical.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'server_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'server_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'server.html');
        ?>    
    </div>
<?php 
}
/* Using Cookies */
elseif ($load_cookies) { ?>
    <h2><?php echo str_cookies ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/cookies.php'))
            include(getenv('PROJECT_DIR').'local/cookies.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'cookies_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'cookies_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'cookies.html');
        ?>
    </div>
<?php
}
/* What's new? */
elseif ($load_whatsnew) { ?>
    <h2><?php echo str_whatsnew ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/whatsnew.php'))
            include(getenv('PROJECT_DIR').'local/whatsnew.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'whatsnew_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'whatsnew_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'whatsnew.html');
        ?>
    </div>
<?php
}
/* Usage */
elseif ($load_usage) { ?>
    <h2><?php echo str_usage ?></h2>
    <div class='preformatted_text left-padding'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/usage.php'))
            include(getenv('PROJECT_DIR').'local/usage.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'usage_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'usage_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'usage.html');
        ?>
    </div>
<?php

}

echo "</div><!--/body-->";
require_once(getenv('PROJECT_DIR').STYLE_PATH."/footer.php.inc");
echo "</div><!--/holder-->";
echo "</body></html>";


exit;
