<?php
    header("Content-Type: text/css", true);
    require(getenv('OB_LIB_DIR').'db_funcs.php');
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS databases.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database with biomaps.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    
    require(getenv('OB_LIB_DIR').'modules_class.php');
    require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    require(getenv('OB_LIB_DIR').'auth.php');
    require(getenv('OB_LIB_DIR').'prepare_auth.php');
    require(getenv('OB_LIB_DIR').'prepare_session.php');
    require(getenv('OB_LIB_DIR').'languages.php');

    setExpires(300);

    // list of modules which contains print_js functions - should be automatically loaded 
    $mj = new modules($_SESSION['current_query_table']);
    $mjx = new x_modules();

    $m_c = array();
    foreach ($mj->which_has_method('print_css',true) as $m) {
        $m_c[] = $m;
    }
    $m_c = array_unique($m_c); // preventing double include
    foreach ($m_c as $m) {
        echo $mj->_include($m,'print_css');
        # A results_buttons-ra azt mondja, hogy not enabled, amikor az echo megtörténik. Ha csak debug-ba megy a kimenet, nem mondja, hogy nincs engedélyezve... Miért???
    }

    foreach ($mjx->which_has_method('print_css') as $m) { 
        echo $mjx->_include($m,'print_css');
    }
?>
