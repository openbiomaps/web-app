<?php
/* Usage
require_once("iapi.php");
$uri = "/api/species/list/";

This uri will load api/species/list.php
    
    species_list() class will be loaded
    execute({functions[], options[]})

$call = api::load($uri);
$result = json_decode($call,true);


$uri = "/api/species/list.php?order=asc&name_status=accepted";

This uri also will load api/species/list.php
    
    species_list() class will be loaded
    execute({functions[], options{order=asc, name_status=accepted}})

$call = api::load($uri);
$result = json_decode($call,true);


$uri = "/api/species/query/current/list.php?order=asc&name_status=accepted";

This uri also will load api/species/list.php
    
    species_list() class will be loaded
    execute({functions[query,current], options{order=asc, name_status=accepted}})
*/

class iapi {

    public static $species_fields = array();

    public static function load($uri) {
        $parsd = self::uri_parse($uri);
        
        $j = json_decode($parsd);

        if (!isset($j->extension) and $j->filename == $j->basename) {
            $j->basename = $j->basename.".php";
        }
        
        require_once($j->path."/".$j->basename);
        
        $api_class = $j->api."_".$j->filename;
        if (!class_exists($api_class)) {
            #log_action("class $j->filename does not exists on $j->path path!",__FILE__,__LINE__);
            printf("class $api_class does not exists in $j->basename!");
            return;
        }
        return ($api_class::execute(json_encode($j->params)));
    }
    private function uri_parse($uri) {

        $purl = parse_url($uri);

        $pinfo = pathinfo($purl["path"]);

        $path = array_filter(explode("/",$pinfo["dirname"]));
        $base_dir = array_shift($path); // shift api
        $entry_point = array_shift($path); // shift entry point
        $purl["path"] = $base_dir.'/'.$entry_point;
        $purl["api"] = $entry_point;
    
        $pstr = array("params"=>array("functions"=>$path,"options"=>array()));

        if (isset($purl["query"])) {
            parse_str($purl["query"],$output);
            $pstr = array("params"=>array_merge($pstr["params"],array("options"=>$output)));
        }

        return json_encode(array_merge($purl,$pinfo,$pstr));
    }
}
?>
