<?php

class Auth {

    public static function set_token_cookies($tokens, $provider = 'self', $path = '/') {
        $remember = (isset($tokens->remember_me) && $tokens->remember_me);

        if ($remember) {
            $expiry = time() -1 + 14*24*60*60;
            $data = (object) ['remember' => true];
            $cookieData = (object) compact( "data", "expiry", "provider" );
            setcookie( "remember", json_encode( $cookieData ), $expiry , $path , "", true, true);
        }

        // set cookies
        if (isset($tokens->access_token)) {
            $access_token = $tokens->access_token;
            $expiry = time() + ($tokens->expires_in ?? 3600);
            $data = (object) compact( "access_token" );
            $cookieData = (object) compact("data", "expiry", "provider");
            setcookie( "access_token", json_encode( $cookieData ), $expiry , $path);
        }

        if (isset($tokens->refresh_token) and $tokens->refresh_token) {
            $refresh_token = $tokens->refresh_token;
            $expiry = time() -1 + (($remember) ? 14*24*60*60 : 1.5*60*60);
            $data = (object) compact( "refresh_token" );
            $cookieData = (object) compact( "data", "expiry", "provider" );
            // Ez így úgyan nagyon biztosnásgos, de így nem lehet a JS-ből elérni a refresh_token, hanem csak session-ből használható, ami lerövidíti a refresh_token idejét a php session idejére.
            //setcookie( "refresh_token", json_encode( $cookieData ), $expiry , $path, "", true, true);
            setcookie( "refresh_token", json_encode( $cookieData ), $expiry , $path, "", true, false);
        }
        
    }

    public static function logout() {

        if (! self::is_session_started()) {
            session_start();
        }

        self::drop_session_temp_tables();

        self::destroy_session();

        session_start();
        session_regenerate_id(TRUE);
        //$_GET = array();
        //$_POST = array();
        //$_REQUEST = array();

        if ( isset($_COOKIE['access_token']) ) {
            self::delete_cookie('access_token');
        }    
        if ( isset($_COOKIE['refresh_token']) ) {
            self::delete_cookie('refresh_token');
        }
        if ( isset($_COOKIE['remember']) ) {
            self::delete_cookie('remember');
        }
    }

    private static function is_session_started() {
        if ( php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                return ( session_status() === PHP_SESSION_ACTIVE );
            } else {
                return ( session_id() !== '' );
            }
        }
        return FALSE;
    }

    public static function destroy_session() {
        session_destroy();
        session_unset();
        session_cache_expire(0);
        session_set_cookie_params([
            'lifetime' => 0,
            'path' => '/',
            'samesite' => 'Lax'
        ]);
        session_cache_limiter('nocache');
        ini_set('session.cookie_lifetime',0);
        ini_set('session.gc_maxlifetime',0);

    }

    public static function drop_session_temp_tables() {
        global $GID;

        $temp_tables = ['temp', 'temp_filter', 'temp_query', 'temp_roll_table', 'temp_roll_stable', 'temp_roll_list'];
        foreach ($temp_tables as $temp_table) {
            pg_query($GID,sprintf("DROP TABLE IF EXISTS temporary_tables.%s_%s_%s",$temp_table, constant('PROJECTTABLE'), session_id()));
        }
    }

    public static function delete_cookie($cookie_name) {
        setcookie($cookie_name, $_COOKIE[$cookie_name], time()-(60*60*24), '/');
        unset($_COOKIE[$cookie_name]);
    }
}

?>
