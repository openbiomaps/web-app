<?php 

/* Recursive groups reading for a user-id 
 * Used by access functions
 * Returning ..
 * */
function read_groups ($user_id) {
    global $BID;

    // How it can be possible?
    // Sometimes there are more than one role_id for the same user_id, I think it is a bug, but I can handle it by LIMIT the subquery results

    $role_cmd = sprintf('WITH RECURSIVE
        pairs as (
            SELECT * FROM (select role_id, unnest(container) as cont, user_id from project_roles WHERE project_table = \'%1$s\') as foo
        ),
        groups as (
            SELECT role_id, cont, user_id FROM pairs WHERE user_id = %2$d
            UNION
            SELECT p.role_id, p.cont, p.user_id FROM pairs p
            INNER JOIN groups g ON p.cont = g.role_id
        ) SELECT * FROM groups;',
        PROJECTTABLE,$user_id);
    $rres = pg_query($BID,$role_cmd);
    if (pg_last_error($BID)) {
        log_action(pg_last_error($BID),__FILE__,__LINE__);
        log_action($role_cmd);
    }
    $roles = array();
    $cont = '';
    //role_id  cnt
    // 12       12
    //205        5
    //201        1
    //202      202
    while ($grow = pg_fetch_assoc($rres)) {
        $roles[] = $grow['role_id'];
        if ($grow['role_id'] == $grow['cont'] and $grow['user_id'] == $user_id)
            $cont = $grow['role_id'];
    }
    $roles = implode(',',array_filter(array_unique($roles)));
    $vars = array('roles','cont');
    return compact($vars);
}


/**
 * 
 */
class User
{
    public $user_id;
    public $role_id;
    public $name;
    public $email;
    public $hash;
    public $status;
    public $institute;
    public $options;
    public $rolename;
    public $roles;
    public $terms_agree;
    public $excluded_taxon_columns = [];
    
    function __construct($user_id) {
        
        if ($user_id === 'non-logined-user') {
            $this->non_logined_user();
            return;
        }
        else {
            $this->fetch_user_info($user_id);
        }
    }
    
    public function __toString() {
        return (string)$this->user_id;
    }
    
    private function non_logined_user() {
        $this->user_id = 0;
        $this->name = 'non-logined-user';
        $this->email = 'non-logined-user';
        $this->hash = '';
        $this->status = 'non-logined-user';
    }
    
    public function is_master() {
        return ($this->status === 'master');
    }
    
    public function is_member_of($role_id) {
        $R = new Role((int)$role_id);
        return $R->has_member($this->user_id);;
    }

    // Array
    public function update($userinfo) {
        global $BID;

        foreach ($userinfo as $key=>$value) {

            if (!in_array($key,array('user','password','email','address','institute','username','orcid','familyname','givenname','references','dropmyaccount'))) {
                return false;
            }
            if ($key == 'password')
                if (!$value = gen_password_hash($value))
                    return false;
        }

        $cmd = sprintf("UPDATE \"public\".\"users\" SET \"$key\"=%s WHERE id='{$this->user_id}'",quote($value));
        $res = pg_query($BID,$cmd);
        
        if ($res and pg_affected_rows($res))
            return true;

        return false;
    }

    // Object
    public function register($userinfo) {
        global $BID;

        if (!is_null($this->user_id)) {
            log_action('Email already registered!', __FILE__, __LINE__);
            return false;
        }
        if (!preg_match("/^[-\\'\p{L} ]+$/u", $userinfo->name) || !filter_var($userinfo->email, FILTER_VALIDATE_EMAIL)) {
            log_action('Invalid user name or email address!', __FILE__, __LINE__);
            return false;
        }
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $random_pw = substr( str_shuffle( $chars ), 0, 16 );
        
        if (isset($userinfo->password))
            $random_pw = $userinfo->password; // overwrite the generated random password

        if (!$hash = gen_password_hash($random_pw)) {
            return false;
        }

        pg_query($BID,'BEGIN');
        $cmd = sprintf('INSERT INTO "users" (password,status,username,email,inviter,pwstate,openid_provider) VALUES (%s, 1, %s, %s, NULL, true, %s) RETURNING id;',
            quote($hash),
            quote($userinfo->name ?? ""),
            quote($userinfo->email),
            quote($userinfo->provider ?? "")
        );

        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)){
            if (preg_match('/duplicate key value/',pg_last_error($BID))) {
                $err = "The user already exists.";
            }
            else {
                $err = "Registration error.";
            }
            log_action($err,__FILE__,__LINE__);
            pg_query($BID,'ROLLBACK');
            return false;
        } else {
            $row = pg_fetch_assoc($res);
            $this->user_id = $row['id'];
        }


        $cmd = sprintf("INSERT INTO project_users (project_table,user_id) VALUES ('%s',%d);", PROJECTTABLE, $this->user_id);
        $res = pg_query($BID,$cmd);
        if (!pg_affected_rows($res)) {
            if (preg_match('/duplicate key value/',pg_last_error($BID))) {
                $err = "This email already registered in this project.";
            }
            else {
                $err = "Registration error.";
            }
            log_action($err,__FILE__,__LINE__);
            pg_query($BID,'ROLLBACK');
            return false;
        }
        
        pg_query($BID,'COMMIT;');
        $this->fetch_user_info($this->user_id);

        return true;
        #new user's info
        #$M = new Messenger();

        #$M->send_system_message($this, 'welcome_to', [ 'user_profile_id' => $this->hash ], true);
        
    }

    public function login() {

        if (in_array($this->status, ['0', 'parked', 'banned'])) {
            return false;
        }


        // set LogIn SESSION variables
        $_SESSION['Tid'] = $this->user_id;

        //these are roles not groups - like in PostgreSQL
        $r = read_groups($this->user_id);
        $_SESSION['Tgroups'] = $this->roles;
        $_SESSION['Trole_id'] = $this->role_id;
        
        $_SESSION['Tterms_agree'] = $this->terms_agree;
        $_SESSION['Tstatus'] = $this->status;
        $_SESSION['Tname'] = $this->name;
        $_SESSION['Tcrypt'] = $this->hash; // hash string
        $_SESSION['Tproject'] = PROJECTTABLE;
        $_SESSION['Tmail'] = strtolower($this->email);

        $_SESSION['LANG'] = select_language();
        
        $_SESSION['Tuser'] = serialize($this);

        return true;
    }

    private function fetch_user_info($user_id) {
        global $BID;

        if (filter_var($user_id, FILTER_VALIDATE_EMAIL)) {
            $where = sprintf("u.email = %s", quote($user_id));
        }
        elseif (preg_match('/^\d+$/', $user_id)) {
            $where = sprintf("u.id = %s", quote($user_id));
        }
        elseif (preg_match('/^[a-f0-9]{32}$/', $user_id)) {
            $where = sprintf('"user" = %s', quote($user_id));
        }
        else {
            log_action('invalid user_id: '. $user_id,__FILE__,__LINE__);
            return false;
        }
        
        $cmd = sprintf(
            'SELECT 
                u.id, u."user", u.username, u.status, u.institute, u.address, u.email, u.terms_agree, pu.user_status, pr.role_id,
                jsonb_object(array_agg(po.option_name), array_agg(po.option_value)) as options, pr.description 
            FROM users u
            LEFT JOIN project_users pu ON pu.project_table = \'%1$s\' AND pu.user_id = u.id
            LEFT JOIN project_roles pr ON pr.project_table = \'%1$s\' AND pr.user_id = u.id
            LEFT JOIN project_options po ON po.project_table = \'%1$s\' AND po.role_id = pr.role_id
            WHERE %2$s
            GROUP BY u.id, u."user",u.username, u.status, u.institute, u.address, u.email, u.terms_agree, pu.user_status, pr.role_id, pr.description;', 
            PROJECTTABLE, $where);
        if ( !$res = pg_query($BID, $cmd)) {
            log_action('query error',__FILE__,__LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        if ( !pg_num_rows($res) ) {
            log_action("user id: $user_id does not exists",__FILE__,__LINE__);
            $this->user_id = null;
            return false;
        }
        $user = pg_fetch_assoc($res);
        
        $this->user_id = (int)$user['id'];
        $this->name = $user['username'];
        $this->email = $user['email'];
        $this->hash = $user['user'];
        $this->status = $user['user_status'];
        $this->institute = $user['institute'];
        $this->terms_agree = $user['terms_agree'];
        $this->role_id = $user['role_id'];
        $this->options = json_decode($user['options']);
        $this->rolename = $user['description'];
        $this->excluded_taxon_columns = $this->excluded_taxon_columns();
        $r = read_groups($this->user_id);
        $this->roles = $r['roles'];
    }

    /**
     * Returns an array with [languge => taxon column name] pairs, other than the 
     * user's preferred language
     */
    private function excluded_taxon_columns()
    {
        $excluded_taxon_columns = [];
        if (isset($this->options->language) && isset($_SESSION['st_col']['NATIONAL_C'][$this->options->language])) {
            $userlang = $this->options->language;
            $excluded_taxon_columns = array_filter($_SESSION['st_col']['NATIONAL_C'], function ($col, $lang) use ($userlang) {
                return $lang !== $userlang;
            }, ARRAY_FILTER_USE_BOTH);
        }

        return $excluded_taxon_columns;
    }
}
