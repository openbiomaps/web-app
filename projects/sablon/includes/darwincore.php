<?php
# This is not ready, just a skeleton!

class darwincore {

    public $archive_name;

    function __construct($D) {

    }

    public function create_simple($metadata) {
        $file = file_get_contents($metadata);
        $parsed_meta = parse_datacite_meta($file);
        $result = create_simple_darwin_record($parsed_meta,'array-to-xml');
        #if ($result['status'] == 'success')
        return json_decode($result,true);
        #else
        #    return 
    }

    public function create_archive($metadata) {
        // Autoload the dependencies
        require 'vendor/autoload.php';

        // enable output of HTTP headers
        $options = new ZipStream\Option\Archive();
        $options->setSendHttpHeaders(true);



        // create a new zipstream object
        $zip = new ZipStream\ZipStream($metadata['title'].'.zip', $options);

        $path = getenv('PROJECT_DIR').'archive_files';
        $this->archive_name = $path."/".$metadata['title'].'.zip';
    }
    public function create_simple_darwin_record($parsed_meta,$type) {

        // This schema defines Simple Darwin Core records and record sets using the terms from namespace http://rs.tdwg.org/dwc/terms/. 
        // This schema allows the terms listed to appear exactly once in any order. 
        // https://dwc.tdwg.org/xml/tdwg_dwc_simple.xsd
        //
        //
        //
        // The Generic Darwin Core schema defining all property terms as global elements. Domain classes are not defined here, but in a separate schema (tdwg_dwc_classes.xsd) with the same namespace. There are two ways in which references to domains can be defined: a) through an abstract base term anyXXXTerm, which is derived from the type dwc:anyPropery and which all properties for that domain use as their substitution group. If you refer to dwc:anyXXXTerm in your schema, you will be able to reference any of the terms, but it will be impossible to create a sequence of all terms occurring only once. b) through a group of elements called XXXTerms, which reference properties exactly once as a sequence. This group can be used to refer to all the domain properties only once each. In order to make a property required, you must create your own group referring to the individual dwc terms. If you want to specify every term once at most and in any order, you will also have to refer to the individual dwc term yourself - a limitation of the xs:all model. 
        // https://dwc.tdwg.org/xml/tdwg_dwcterms.xsd


        // example dwc
        /*
        <?xml version="1.0"?>
        <dwr:SimpleDarwinRecordSet
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://rs.tdwg.org/dwc/xsd/simpledarwincore/  http://rs.tdwg.org/dwc/xsd/tdwg_dwc_simple.xsd"
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:dcterms="http://purl.org/dc/terms/"
            xmlns:dwc="http://rs.tdwg.org/dwc/terms/"
            xmlns:dwr="http://rs.tdwg.org/dwc/xsd/simpledarwincore/">
            <dwr:SimpleDarwinRecord>
                <dc:type>PhysicalObject</dc:type>
                <dcterms:modified>2009-02-12T12:43:31</dcterms:modified>
                <dc:language>en</dc:language>
                <dcterms:rightsHolder>Regents of the University of California</dcterms:rightsHolder>
                <dcterms:license>http://creativecommons.org/publicdomain/zero/1.0/legalcode</dcterms:license>
                <dwc:basisOfRecord>PreservedSpecimen</dwc:basisOfRecord>
                <dwc:institutionCode>MVZ</dwc:institutionCode>
                <dwc:collectionCode>Mammal specimens</dwc:collectionCode>
                <dwc:catalogNumber>MVZ:Mamm:14523</dwc:catalogNumber>
                <dec:sex>male</dwc:sex>
                <dwc:occurrenceID>http://arctos.database.museum/guid/MVZ:Mamm:14523?seid=770093</dwc:occurrenceID>
                <dwc:country>United States</dwc:country>
                <dwc:countryCode>US</dwc:countryCode>
                <dwc:stateProvince>California</dwc:stateProvince>
                <dwc:county>Kern County</dwc:county>
                <dwc:locality>8 mi NE Bakersfield</dwc:locality>
                <dwc:decimalLatitude>35.45038</dwc:decimalLatitude>
                <dwc:decimalLongitude>-118.9092</dwc:decimalLongitude>
                <dwc:geodeticDatum>epdg:4267</dwc:geodeticDatum>
                <dwc:coordinateUncertaintyInMeters>13696</dwc:coordinateUncertaintyInMeters>
                <dwc:eventDate>1911-05-14</dwc:eventDate>
                <dwc:scientificName><Perognathus inornatus inornatus/dwc:scientificName>
            </dwr:SimpleDarwinRecord>
            </dwr:SimpleDarwinRecordSet>

            more examples here:
            https://dwc.tdwg.org/xml/
         */
        if ($type == 'array-to-xml') {
            $xml = new SimpleXMLElement("<?xml version=\"1.0\"?>");
            array_to_xml($parsed_meta,$xml);
            return $xml->asXML();
        }
    }

    public function parse_datacite_meta($file) {
        return json_decode($file, true);
    }

}
?>
