<?php
/* deprecated - not used!
 * */
exit;
include_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
if(!isset($_SESSION['Tth'])) {
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

    // Configure your imap mailboxes
    $password = decode($_SESSION['Tth'],session_id());
    $mailbox = array(
        'label'     => 'openbiomaps',
        'mailbox'   => '{openbiomaps.org:143/notls}INBOX',
        'username'  => "{$_SESSION['Tuser']}@openbiomaps.org",
        'password'  => "$password"
    );

?>
<div class="mailbox">
    <h2>Inbox</h2>
    <?     
    // Open an IMAP stream to our mailbox
    $stream = @imap_open($mailbox['mailbox'], $mailbox['username'], $mailbox['password'],OP_READONLY,1);
    #print 1;
     
    if (!$stream) {
    ?>
        <p>Connection error: <?=imap_last_error()?></p>
    <?
    } else {
        // Get our messages from the last week
        $emails = imap_search($stream, 'SINCE '. date('d-M-Y',strtotime("-1 week")));
 
        // Instead of searching for this week's messages, you could search
        // for all the messages in your inbox using: $emails = imap_search($stream, 'ALL');
         
        if (!count($emails)){
        ?>
            <p>No message found.</p>
        <?
        } else {
 
            // If we've got some email IDs, sort them from new to old and show them
            rsort($emails);
             
            foreach($emails as $email_id){
             
                // Fetch the email's overview and show subject, from and date.
                $overview = imap_fetch_overview($stream,$email_id,0);                      
?>
<div class="email_item clearfix <?=$overview[0]->seen?'read':'unread'?>">
    <span class="subject"><?=decode_imap_text($overview[0]->subject)?></span>
    <span class="from"><?=decode_imap_text($overview[0]->from)?></span>
    <span class="date"><?=$overview[0]->date?></span>
</div>
                <?
            }
        }
 
        // Close our imap stream.
        imap_close($stream);
    }
    ?>
</div>

