<?php

class OidcToken extends Entity {
    protected $tableName = 'oidc_tokens';
    protected $db = 'biomaps';
    protected $mode = 'insert';

    public $id;
    public $token;
    public $token_type;
    public $provider;
    public $user_id;
    public $expires;
    public $scope;
    public $client;

    function __construct() {
        parent::__construct();
    }
}
?>
