<?php
require_once(getenv('OB_LIB_DIR') . '/Auth/Auth.php');
require_once(getenv('OB_LIB_DIR') . '/Auth/obmAuth.php');
require_once(getenv('OB_LIB_DIR') . '/Auth/oidcAuth.php');

if (defined('OPENID_CONNECT')) {
    foreach (array_keys(constant('OPENID_CONNECT')) as $provider) {
        require_once(getenv('OB_LIB_DIR') . "/Auth/OidcButtons/$provider.php");
    }
}