<?php 
/**
 * 
 */
class linnaeus_load extends iapi {
    
    private static $strings = ["str_actions", "str_accepted_name", "str_synonym_name", "str_mispelled_name", "str_undefined", "str_save", "str_add", "str_metaname", "str_please_choose", "str_adjust_similarity_index"];
    
    public function execute($params) {

        $j = json_decode($params,true);

        if (count($j['functions'])) {
            if (in_array('ajax',$j['functions'])) {
                return self::ajax($j['options']);
            }
            else if (method_exists(__CLASS__, $j['functions'][0])) {
                $meth = $j['functions'][0];
                return self::$meth($j['options']);
            }
        }

    }
    
    private static function ajax($options) {
        if (isset($options['linnaeus']) && method_exists(__CLASS__, $options['linnaeus'])) {
            $meth = $options['linnaeus'];
            return self::$meth($options);
        }
    }
    
    private static function translations() {
        
        $x = array_map(function ($str) {
            return [$str=>t($str)];
        }, self::$strings);
        $v = array_map('current', $x);
        $k = array_map('key', $x);
        return array_combine($k,$v);
    }
    
    private static function enabled_modules() {
        $modules = new modules(PROJECTTABLE);
        $x_modules = new x_modules();
        
        $enabled_modules = [
            'taxon' => TaxonManager::get_tables_subjects(),
            'terms' => false,
            'taxonMeta' => false,
            'validation' => false
        ];
        
        if ($x_modules->is_enabled('taxon_meta')) {
            $enabled_modules['taxonMeta'] = false; //$modules->_include('taxon_meta', 'meta_columns', 'array');
        }
        if ($modules->is_enabled('list_manager')) {
            $enabled_modules['terms'] = $modules->_include('list_manager', 'get_tables_subjects');
        }
        if ($modules->is_enabled('validation')) {
            $enabled_modules['validation'] = $modules->_include('validation', 'settings');
        }
        return $enabled_modules;
    }
    
    private static function get_list($request) {
        global $modules;
        
        $args = self::prepare_get_list_arguments($request);
        if ($args['table'] === 'terms' && !$modules->is_enabled('list_manager')) {
            throw new \Exception('List manager module not enabled!', 1);
        }
        
        return new L_list($args);
    }
    
    /**
     * This function transforms the request to a format, which is suitable
     * for both the TaxonManager and the list_manager
     **/
    private static function prepare_get_list_arguments($request) {
        $managed_columns = self::get_linnaeus_options();
        
        if ($request['table'] === 'terms' && isset($request['only_managed_columns']) && $request['only_managed_columns']) {
            $managed_columns = array_column($managed_columns, 'name');
            if (isset($request['subject']) && $request['subject']) {
                $request['subject'] = array_unique(array_merge(
                    (is_array($request['subject'])) ? $request['subject'] : [$request['subject']], 
                    $managed_columns
                ));
            }
            else {
                $request['subject'] = $managed_columns;
            }
            unset($request['only_managed_columns']);
        }
        
        return [
            'table' => $request['table'],
            'id' => $request['taxon_id'] ?? null,
            'data_table' => $request['data_table'] ?? null,
            'colname' => $request['subject'] ?? null, 
            'status' => $request['status'] ?? null, 
            'limit' => $request['limit'] ?? null,
            'offset' => $request['offset'] ?? null,
            'term' => $request['filter'] ?? null,
            'excluded' => $request['excluded'] ?? null,
            'search_algoritm' => $request['search_algoritm'] ?? 'like',
            'preFilter' => $request['preFilter'] ?? null,
            'bilingual_search' => $request['bilingual_search'] ?? false,
            'orderby' => $request['orderby'] ?? false,
            'order' => $request['order'] ?? false,
            'list_and_count' => true
        ];
    }
    
    private static function save_word($request) {
        if (!is_numeric($request['word']['id']) && $request['word']['id'] !== "") {
            throw new \Exception('Invalid word_id!', 1);
        }
        $w = new L_term($request['word']);
        if (!$w->save()) {
            throw new \Exception('Word save failed!', 1);
        }
        return $w;
    }
    
    private static function delete_word($request) {
      
        if (!is_numeric($request['word']['id']) && $request['word']['id'] !== "") {
            throw new \Exception('Invalid word_id!', 1);
        }
        $w = new L_term($request['word']);
        if (!$w->delete()) {
            throw new \Exception("Deleting word failed!", 1);
        } 
        return true;
    }
    
    private static function load_list($request) {
        global $modules;
        
        if ($request['table'] === 'terms' && !$modules->is_enabled('list_manager')) {
            throw new \Exception('List manager module not enabled!', 1);
        }
        
        $args = self::prepare_get_list_arguments($request);
        
        $list = new L_list($args);
        
        return array_map(function ($id) use ($list) {
            return new L_taxon(array_values(array_filter($list->list, function ($t) use ($id) {
                return $t->taxon_id === $id;
            })));
        }, $list->get_taxon_ids());
    }
    
    private static function create_or_replace_matviews() {
        $st_col = st_col($pa['mtable'], 'array', 0);
        if ( count($st_col['NATIONAL_C']) ) {
            self::drop_taxon_matviews();
            foreach ($st_col['NATIONAL_C'] as $lang => $col) {
                self::create_taxon_matview($col, $st_col['SPECIES_C_SCI']);
            }
        }
        else {
            self::drop_taxon_matviews();
        }
    }
    
    private function get_taxon_matviews() {
        global $ID;
        $cmd = sprintf("SELECT matviewname FROM pg_matviews WHERE schemaname = 'temporary_tables' AND matviewname LIKE '%s_taxon_%%';", PROJECTTABLE);
        if (!$res = pg_query($ID, $cmd)) {
            log_action("query error: $cmd", __FILE__, __LINE__);
            return [];
        }
        if (pg_num_rows($res) === 0 ) {
            return [];
        }
        return array_column(pg_fetch_all($res), 'matviewname');
    }
    
    private function drop_taxon_matviews() {
        global $ID;
        
        $matviews = self::get_taxon_matviews();
        if (empty($matviews)) {
            return;
        }
        
        $cmd = array_map(function ($matview_name) {
            return sprintf("DROP MATERIALIZED VIEW temporary_tables.%s;", $matview_name);
        }, $matviews);
        query($ID, $cmd);
    }
    
    private function create_taxon_matview($usr_col, $sci_col) {
        global $ID;
        
        // TODO: nem csak a taxonra kell ez, hanem a searchre is.
        $table = 'taxon';
        
                //getSQL('species_name_validation', getenv('OB_LIB_DIR') . 'api/linnaeus/linnaeus.sql') 
        $cmd = sprintf(getSQL('create_taxon_matview', 'linnaeus'), PROJECTTABLE, $usr_col, $sci_col, quote($usr_col), quote($sci_col) );
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = "query error: $cmd";
            return false;
        }
        return true;
    }
    
    private function get_admin_data($request) {

        $linnaeus_options = self::get_linnaeus_options();
        $admin_data = [];
        foreach ($linnaeus_options as $table => $options) {
            $admin_data[$table] = [
                'healthcheck' => [
                    'linnaeus_table_exists' => self::lhc_linnaeus_table_exists($table),
                    'data_table_linnaeus_sync_function' => self::lhc_data_table_linnaeus_sync_function_exists(),
                    'data_table_linnaeus_sync_trigger' => self::lhc_data_table_linnaeus_sync_trigger_exists($table),
                    'add_term_function' => self::lhc_add_term_function_exists($table),
                    'add_term_trigger' => self::lhc_add_term_trigger_exists($table),
                    'terms_table_linnaeus_sync_function' => self::lhc_terms_table_linnaeus_sync_function_exists(),
                    'terms_table_linnaeus_sync_trigger' => self::lhc_terms_table_linnaeus_sync_trigger_exists()

                ],
                'columns' => (function ($options, $table) {

                    $columns = dbcolist('array', $table);

                    if (!empty($options)) {
                        $managed_columns = [];
                        foreach ($options->managed_columns as $col) {
                            $managed_columns[$col->name] = $col->multiterm;
                        }

                        foreach (array_keys($columns) as $column) {
                            if (in_array($column, array_keys($managed_columns))) {
                                $columns[$column] = [
                                    'managed' => true,
                                    'multiterm' => ($managed_columns[$column] === 'true'),
                                ];
                            }
                            else {
                                $columns[$column] = ['managed' => false];
                            }
                        }
                    }
                    else {
                        $columns = array_map(function ()
                        {
                            return ['managed' => false];
                        }, $columns);
                    }


                    return $columns;
                })($options, $table)
            ];
        }
        return $admin_data;
    }
    
    public static function get_linnaeus_options($table = []) {
        $tables = (!empty($table)) ? [$table['table']] : getProjectTables();

        return array_combine($tables, array_map(function ($table) {
            $linnaeus_options = get_option("{$table}_linnaeus_options");

            return ($linnaeus_options) ? json_decode(base64_decode($linnaeus_options)) : [];
        }, $tables));
    }
    
    private function set_linnaeus_options($request) {
        if (!isset($request['managed_columns'])) {
            throw new \Exception("Error Processing Request", 1);
        }
        foreach ($request['managed_columns'] as $table => $columns) {
            if (!in_array($table, getProjectTables())) {
                throw new \Exception("Error Processing Request", 1);
            }
            if ($columns === 'none') {
                $columns = [];
            }
            $options = json_decode(base64_decode(get_option("{$table}_linnaeus_options")));
            if (!is_array($options)) {
                $options = [];
            }
            $options['managed_columns'] = $columns;

            if (!set_option("{$table}_linnaeus_options", base64_encode(json_encode($options)))) {
                throw new \Exception("Error saving!", 1);
            }

            self::init($table);

        }
        return 'ok';
    }


    private static function activate_linnaeus_table($request) {
        if (!in_array($request['table'], getProjectTables())) {
            throw new \Exception("Error Processing Request", 1);
        }
        global $ID, $GID;

        if (self::lhc_linnaeus_table_exists($request['table'])) {
            throw new Exception("{$request['table']} already exists");
        }
        $cmd = sprintf( getSQL('create_linnaeus_table', getenv('OB_LIB_DIR') . 'api/linnaeus/linnaeus.sql'), $request['table']);
        if (!$cmd || !pg_query($GID,$cmd)) {
            throw new Exception('linnaeus init failed: table creation error');
        }
        $cmd = sprintf("GRANT SELECT, INSERT, UPDATE, DELETE ON system.%s_linnaeus TO %s_admin;", $request['table'], PROJECTTABLE);
        if (!$cmd || !pg_query($GID,$cmd)) {
            throw new Exception('linnaeus init failed: grant error');
        }
        return true;
    }
    
    private static function init($table) {
        global $ID, $BID, $GID;

        $modules = new modules($table);

        /**
         * creating a trigger and function to keep the data table and the 
         * table of ids (PROJECT_linnaeus) in sync
         **/
        $fun_name = PROJECTTABLE . '_l_sync_row';
        if (!self::lhc_data_table_linnaeus_sync_function_exists()) {
            $cmd = sprintf( getSQL('l_sync_data_linnaeus', getenv('OB_LIB_DIR') . 'api/linnaeus/linnaeus.sql'), $fun_name);
            if (!$cmd || !pg_query($GID,$cmd)) {
                throw new Exception('linnaeus init failed: table creation error');
            }
        }
        
        $tg_name = "{$table}_linnaeus_sync_rows";
        if (!self::lhc_data_table_linnaeus_sync_trigger_exists($table)) {
            $cmd = sprintf('CREATE TRIGGER %s AFTER INSERT OR UPDATE OR DELETE ON %s FOR EACH ROW EXECUTE PROCEDURE %s();',
                $tg_name,
                $table,
                $fun_name
            );
            if (!$cmd || !pg_query($GID,$cmd)) {
                throw new Exception('linnaeus init failed: table creation error');
            }
        }


        // inserting the missing row_ids into the linnaeus table
        $cmd = sprintf("INSERT INTO system.%1\$s_linnaeus (row_id) SELECT obm_id FROM %1\$s WHERE obm_id NOT IN (SELECT row_id FROM system.%1\$s_linnaeus);", $table);
        if (!$cmd || !pg_query($GID,$cmd)) {
            throw new Exception('linnaeus init failed: table creation error');
        }
        
        
        // adding/removing maanged columns from linnaeus table
        
        $linnaeus_options = self::get_linnaeus_options(['table' => $table]);
        $linnaeus_options = $linnaeus_options[$table];
        if (isset($linnaeus_options->managed_columns) && !empty($linnaeus_options->managed_columns)) {
            $mTableColumns = getColumnsOfTables($table);
            $definedColumns = array_values(array_filter(
                getColumnsOfTables(sprintf('%s_linnaeus', $table), 'system'),
                function ($col) {
                    return !in_array($col, ['row_id', 'updated_at', 'tcid']);
                }
            ));
            $definedColumns = array_map(function ($col) {
                $col = preg_replace('/_ids$/', "", $col);
                return $col;
            }, $definedColumns);
            
            // adding new columns
            $cmd = array_values(array_map(
                function ($col) use ($table) {
                    return sprintf("ALTER TABLE system.%s_linnaeus ADD COLUMN %s integer[] NULL;", $table, "{$col->name}_ids");
                },
                array_filter($linnaeus_options->managed_columns, function ($col) use ($mTableColumns, $definedColumns) {
                    return (
                        in_array($col->name, $mTableColumns) &&
                        !in_array($col->name, $definedColumns)
                    );
                })
            ));
            
            //removing deleted columns
            $delete_cmds = array_values(array_map(
                function ($col) use ($table) {
                    return sprintf("ALTER TABLE system.%s_linnaeus DROP COLUMN %s;", $table, "{$col}_ids");
                },
                array_filter($definedColumns, function ($col) use ($linnaeus_options) {
                    return !in_array($col, array_column($linnaeus_options->managed_columns, 'name'));
                })
            ));
            $cmd = array_merge($cmd, $delete_cmds);
            if (!empty($cmd) && !query($GID,$cmd)) {
                throw new Exception('linnaeus init failed: adding / removing columns error');
            }
            
            // creating the trigger which updates the terms table
            $definedColumns = array_values(array_filter(
                getColumnsOfTables(sprintf('%s_linnaeus', $table), 'system'),
                function ($col) {
                    return !in_array($col, ['row_id', 'updated_at', 'tcid']);
                }
            ));
            
            $fun_name = $table . "_add_term";
            $cmd = sprintf("CREATE OR REPLACE FUNCTION system.\"%s\" () RETURNS trigger LANGUAGE plpgsql AS $$\nDECLARE \n\tproject varchar; \n\ttbl varchar; \n\tsbj varchar; \n\ttrm varchar;\nBEGIN\n\t-- do not modify this function it is generated automatically\n \tproject := '%s'; tbl := TG_TABLE_NAME;\n", $fun_name, PROJECTTABLE);
            foreach ($linnaeus_options->managed_columns as $col) {

                $cmd .= "\tsbj := '{$col->name}'; trm := NEW.{$col->name};\n\tIF trm IS NOT NULL THEN \n";
                if ($col->multiterm === 'true') {
                    
                    $cmd .= "\t\tEXECUTE format('INSERT INTO %s_terms (data_table,subject,term,status) SELECT %L, %L, trim(unnest(regexp_split_to_array(%L,E'',\\s*''))) as o, ''undefined'' ON CONFLICT DO NOTHING;', project, tbl, sbj, trm);\n";
                }
                else {
                    $cmd .= "\t\tEXECUTE format('INSERT INTO %s_terms (data_table,subject,term,status) VALUES (%L, %L, %L, ''undefined'') ON CONFLICT DO NOTHING;', project, tbl, sbj, trm);\n";
                }
                $cmd .= "\tEND IF;\n\n";
            }
            $cmd .= "\tRETURN NEW;\n END;\n $$;";
            if (!$res = pg_query($GID,$cmd)) {
                throw new Exception('linnaeus init failed: add term function creation error');
            }
            $tg_name = "{$table}_add_term_tr";
            
            if (!self::lhc_add_term_trigger_exists($table)) {
                $cmd = sprintf('CREATE TRIGGER %s AFTER INSERT OR UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE system.%s();', $tg_name, $table, $fun_name);
                if (!pg_query($GID,$cmd)) {
                    throw new Exception('linnaeus init failed: trigger creation error');
                }
            }
            
            // creating the trigger and function which syncs between the terms table and a linnaeus table
            $fun_name = PROJECTTABLE . '_l_sync_terms_linnaeus';
            
            if (!self::lhc_terms_table_linnaeus_sync_function_exists()) {
                $cmd = sprintf( getSQL('l_sync_terms_linnaeus', getenv('OB_LIB_DIR') . 'api/linnaeus/linnaeus.sql'), PROJECTTABLE, $fun_name);
                if (!$cmd || !pg_query($GID,$cmd)) {
                    throw new Exception('linnaeus init failed: function creation error');
                }
            }
            
            $tg_name = "{$fun_name}_tr";
            
            if (!self::lhc_terms_table_linnaeus_sync_trigger_exists()) {
                $cmd = sprintf('CREATE TRIGGER %s BEFORE UPDATE OR DELETE ON %s FOR EACH ROW EXECUTE PROCEDURE %s();',
                    $tg_name,
                    PROJECTTABLE . '_terms',
                    $fun_name
                );
                if (!pg_query($GID,$cmd)) {
                    throw new Exception('linnaeus init failed: function creation error');
                }
            }
        }
        return;
    }


    /*****************
     * HEALTHCHECK FUNCTIONS
     *****************/


    /**
     * Checks the existence of the linnaeus table of the specified data table
     * 
     * @param table string name of the data table
     *
     * @return boolean
     */
    private static function lhc_linnaeus_table_exists($table) {
        return Healthcheck::table_exists("{$table}_linnaeus", "system");
    }


    /**
     * checking the existence of the PROJECTTABLE_l_sync_row function, which 
     * keeps the data table and the linnaeus_table in sync
     *
     * @param table string name of the data table
     *
     * @return boolean
     */
    private static function lhc_data_table_linnaeus_sync_function_exists() {
        return Healthcheck::func_exists(PROJECTTABLE . '_l_sync_row', "system");
    }


    /**
     * Checks the existence of the {data_table}_linnaeus_sync_rows trigger on data_table
     * which triggers the PROJECTTABLE_l_sync_row function
     *
     * @param table string name of the data table
     *
     * @return boolean
     */
    private static function lhc_data_table_linnaeus_sync_trigger_exists($table) {
        return Healthcheck::trigger_exists("{$table}_linnaeus_sync_rows", $table, "system");
    }

    /**
     * Checks the existence of the {data_table}_add_term function.
     * This function inserts the new values of the managed columns into the 
     * PROJECTTABLE_terms table
     *
     * @param table string name of the data table
     *
     * @return boolean
     */
    private static function lhc_add_term_function_exists($table) {
        return Healthcheck::func_exists("{$table}_add_term", "system");
    }

    /**
     * Checks the existence of the {data_table}_add_term_tr trigger.
     *
     * @param table string name of the data table
     *
     * @return boolean
     */
    private static function lhc_add_term_trigger_exists($table) {
        return Healthcheck::trigger_exists("{$table}_add_term_tr", $table, "system");
    }

    /**
     * Checks the existence of the PROJECTTABLE_l_sync_terms_linnaeus function.
     * The role of this function is to syncronize between the terms table and the
     * linnaeus table
     *
     * @return boolean
     */
    public function lhc_terms_table_linnaeus_sync_function_exists()
    {
        return Healthcheck::func_exists(PROJECTTABLE . '_l_sync_terms_linnaeus', "system");
    }

    /**
     * Checks the existence of the l_sync_terms_linnaeus_tr trigger 
     * (see above)
     *
     * @return boolean
     */
    public function lhc_terms_table_linnaeus_sync_trigger_exists()
    {
        return Healthcheck::trigger_exists(PROJECTTABLE . '_l_sync_terms_linnaeus_tr', PROJECTTABLE . '_terms', "system");
    }

}

/**
 * Custom data structure for the linnaeus module. It converts the different data 
 * coming from the taxon.php or the list_manager module to a uniform format
 */
class L_list {
    
    public $list = [];
    public $count = 0;
    
    function __construct($args) {
        global $modules;
        
        if (!$args) {
            $this->list = [];
            return; 
        }
        if ($args['table'] === 'taxon') {
            $list = TaxonManager::get_list_and_count($args);
            
            // reorder list by dist
            $dist = array_column($list['list'], 'dist');
            array_multisort($dist, SORT_ASC, $list['list']);
            
            $this->list = array_map(function ($term) use ($args) {
                return new L_term([
                    'taxon_id' => $term['taxon_id'],
                    'id' => $term['wid'],
                    'table' => PROJECTTABLE,
                    'subject' => $term['lang'],
                    'word' => $term['word'],
                    'meta' => $term['meta'],
                    'status' => $term['status'],
                    'taxon_db' => $term['taxon_db'],
                    'terms_table' => $args['table'],
                ]);
            }, $list['list']);
            $this->count = $list['count'];
        }
        elseif ($args['table'] === 'terms') {
            $list = $modules->_include('list_manager', 'get_list', $args);
            $this->list = array_map(function ($term) use ($args) {
                return new L_term([
                    'taxon_id' => $term->term_id,
                    'id' => $term->wid,
                    'table' => $term->data_table,
                    'subject' => $term->subject,
                    'word' => $term->term,
                    'status' => $term->status,
                    'taxon_db' => $term->taxon_db,
                    'meta' => $term->meta,
                    'terms_table' => $args['table'],
                ]);
            }, $list['list']);
            $this->count = $list['count'];
        }
    }
    
    public function get_taxon_ids() {
        return array_unique(array_map(function ($t) {
            return $t->taxon_id;
        }, $this->list));
    }
}

class L_taxon {
    public $id = null;
    public $table = "";
    public $words = [];
    public $warnings = [];
    public $errors = [];
    
    private $states = ['accepted','synonym','misspelled','undefined'];
    
    function __construct($terms) {
        global $ID;
        
        // taxon id 
        $this->id = $terms[0]->taxon_id;
        $this->table = $terms[0]->table;
        
        foreach ($terms as $t) {
            $t->status = (in_array($t->status, $this->states)) ? $t->status : 'undefined';
            $this->words[] = $t;    
        }
    }
    
    public function getWord($status, $lang) {
        $word = array_filter($this->words, function($w) use ($status, $lang) {
            return ($w->lang === $lang && $w->status === $status);
        });
        return (count($word)) ? $word[0] : false;
    }
    
    public function words() {
        return $this->words;
    }
    
    public function delete_taxon() {
        array_map(function($w) {
          return $w->delete();
        }, $this->words);  
    }
    
}

class L_term {
    
    private $props = ['table', 'subject', 'word', 'id', 'taxon_db', 'status', 'parent', 'taxon_id', 'terms_table', 'meta'];
    private $term;

    public function __construct($t) {
        global $modules;
        
        foreach ($this->props as $p) {
            if (isset($t[$p]))
                $this->$p = $t[$p];
        }
        if ($this->terms_table === 'taxon') {
            $this->term = new Word([
                'wid' => $this->id,
                'word' => $this->word,
                'status' => $this->status,
                'lang' => $this->subject,
                'taxon_id' => $this->taxon_id,
            ]);
        }
        else {
            $this->term = $modules->_include('list_manager', 'get_term', $this);
        }
    }
    
    public function save() {
        if ($this->term->save()) {
            $this->id = $this->term->wid;
            return true;
        }
        return false;
    }
    
    public function delete() {
        return $this->term->delete();
    }
    
}

class Metaname {
    
    private $string = '';
    
    public static $repositories = [
        /*
        'CoL' => [
            'short' => 'CoL',
            'name' => 'Catalogue of Life',
            'url' => 'http://www.catalogueoflife.org/col/webservice?name=',
            'format' => 'xml',
            'icon' => 'col_icon.jpg',
            'wspace_char' => '+',
        ],
        */
        'GNI' => [
            'short' => 'GNI',
            'name' => 'Global Name Index',
            'url' => 'http://gni.globalnames.org/name_strings.xml?search_term=',
            'format' => 'xml',
            'icon' => 'gni_icon.jpg',
            'wspace_char' => '+',
        ],
        'EoL' => [
            'short' => 'EoL',
            'name' => 'Enciclopedia of Life',
            'url' => 'https://eol.org/api/search/1.0.json?page=1&exact=false&filter_by_taxon_concept_id=&filter_by_hierarchy_entry_id=&filter_by_string=&cache_ttl=&q=',
            'format' => 'json',
            'icon' => 'eol_icon.jpg',
            'wspace_char' => '+',
        ],
        'GBiF' => [
            'short' => 'GBiF',
            'name' => 'GBiF',
            'url' => 'https://api.gbif.org/v1/species/match?verbose=false&name=',
            'format' => 'json',
            'icon' => 'gbif_icon.png',
            'wspace_char' => '+',
        ]    
    ];
    private $repository = [];
    
    public $raw = null;
    public $data = [ ];
    
    function __construct($request) {
        
        if (!in_array($request['repository'], array_keys(self::$repositories))) {
            return "Unknown repository!";
        }
        $this->repository = self::$repositories[$request['repository']];
        
        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");
        
        $this->string = mb_ereg_replace('\s+', $this->repository['wspace_char'], $request['term']);
        
        // fetching the data
        $fetch_fun = "fetch_{$this->repository['format']}";
        $this->$fetch_fun();
        
        // processing the data
        $process_fun = "process_{$this->repository['short']}";

        $this->$process_fun();        
    }
    
    private function url() {
        return $this->repository['url'] . $this->string;
    }
    
    private function fetch_json() {
        $this->raw = file_get_contents($this->url());
    }
    
    private function fetch_xml() {
        $this->raw = fetch_remote_xml($this->url());
    }
        
    //Catalogue of Life
    /*
    private function process_CoL() {
        $sa = "";
        if (is_object($data)) {
            if ($data['total_number_of_results']!='0') {
                //http://www.catalogueoflife.org/col/search/all/key/Pica+pica/fossil/0/match/1
                $sa .= $data['total_number_of_results']." records found.<br>";
                if ($data['total_number_of_results'] > 1)
                $sa .= "<a href='http://www.catalogueoflife.org/col/search/all/key/$string/fossil/0/match/1' target='_blank'>Catalogue of Life</a><br>";
                else {
                    $sa .= '<a href="'.$data->result->url.'" target="_blank">Catalogue of Life</a><br>';
                    $sa .='Rank:<b>'.$data->result->rank."</b><br>";
                    $sa .='Name status:<b>'.$data->result->name_status."</b><br>";
                }
            } else {
                $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
                $sa .= "No results.";
            }
        } else {
            $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    */
    
    //Global Name Index
    private function process_GNI() {
        if (is_object($this->raw)) {
            if (!$this->raw->name_strings[0]['nil']) {
                $this->data['number_of_records'] = (string)$this->raw->name_strings_total;
            }
        } else {
            $this->error = true;
        }
        $this->data['url'] = "<a href='" . preg_replace('/\.xml/','.html',$this->url()) . "' target='_blank'>" . $this->repository['name'] . "</a>";
    }
    
    //Enciclopedia of Life
    private function process_EoL() {
        $this->data['url'] = "<a href='https://eol.org/api/search/1.0.json?q={$this->string}' target='_blank'>" . $this->repository['name'] . "</a>";
        if (is_json($this->raw)) {
            $j = json_decode($this->raw);
            if (isset($j->{'totalResults'})) {
                $this->data['number_of_records'] = $j->{'totalResults'};
                $this->data['url'] = "<a href='" . $j->{'results'}[0]->{'link'} . "' target='_blank'>" . $this->repository['name'] . "</a>";
            } 
        } else {
            $this->error = true;
        }
    }
    
    //GBiF
    //https://api.gbif.org/v1/species?name=Puma%20concolor
    //https://api.gbif.org/v1/species/match?verbose=false&name=Oenante%20oenanthe
    private function process_GBiF() {
        if (is_json($this->raw)) {
            $j = json_decode($this->raw);
            if (isset($j->{'scientificName'})) {
                $this->data["Distributions"] = "<a href='https://api.gbif.org/v1/species/" . $j->{'usageKey'} . "/distributions' target='_blank'>Distributions</a>";
                $this->data['kingdom'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'kingdomKey'}."/children' target='_blank'>" . $j->{'kingdom'} . "</a>";
                $this->data['phylum'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'phylumKey'}."/children' target='_blank'>" . $j->{'phylum'} . "</a>";
                $this->data['order'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'orderKey'}."/children' target='_blank'>" . $j->{'order'} . "</a>";
                $this->data['family'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'familyKey'}."/children' target='_blank'>" . $j->{'family'} . "</a>";
                $this->data['genus'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'genusKey'}."/children' target='_blank'>" . $j->{'genus'} . "</a>";
                $this->data['scientificName'] = "<a href='https://api.gbif.org/v1/species/" . $j->{'speciesKey'}. "' target='_blank'>" . $j->{'scientificName'} . "</a>";
            } else {
                $this->data['try fuzzy match?'] = "<a href='https://api.gbif.org/v1/species/match?verbose=true&name={$this->string}' target='_blank'> try fuzzy match? </a>";
            }
        } else {
            $this->error = true;
            $this->data['urls'] = [
                [ "href" => "https://gbif.org", "text" => $this->repository['name'] ],
            ];
        }
    }
}
 ?>
