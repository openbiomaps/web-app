-- create_linnaeus_table
CREATE TABLE "system"."%1$s_linnaeus" ( 
    row_id integer NOT NULL,
    updated_at timestamptz NULL,
    CONSTRAINT %1$s_linnaeus_fk FOREIGN KEY (row_id) REFERENCES %1$s(obm_id) ON DELETE CASCADE
) WITH (oids = false);

-- species_name_validation
WITH rows AS (
    SELECT DISTINCT f.%1$s, foo.valid_word 
    FROM %5$s f 
    LEFT JOIN (
        SELECT 
            t.word as src_word, 
            CASE WHEN tt.word IS NULL 
                THEN fb.word 
                ELSE tt.word 
            END as valid_word 
        FROM %4$s_taxon t 
        LEFT JOIN 
            %4$s_taxon tt ON t.taxon_id = tt.taxon_id AND tt.status = 'accepted'
        LEFT JOIN
            %4$s_taxon fb ON t.taxon_id = fb.taxon_id AND fb.lang = '%2$s' AND fb.status = 'accepted'
        WHERE 
            tt.lang = '%3$s' AND tt.word IS NOT NULL
        ) foo ON f.%1$s = foo.src_word 
    WHERE f.%3$s IS DISTINCT FROM foo.valid_word)
UPDATE %5$s ff SET %3$s = rows.valid_word 
FROM rows 
WHERE rows.%1$s = ff.%1$s AND ff.%3$s IS DISTINCT FROM rows.valid_word;

-- count_observations
WITH counts AS (
    SELECT word, CASE WHEN c IS NULL THEN 0 ELSE c END as c from %1$s_taxon 
    LEFT JOIN (SELECT %2$s, count(*) as c FROM %4$s GROUP BY %2$s) foo ON %2$s = word WHERE lang = %3$s
) 
UPDATE %1$s_taxon t SET taxon_db = counts.c FROM counts WHERE t.word = counts.word AND t.lang = %3$s;

-- match_terms_singleterm
WITH tu as (
    SELECT 
        t.term_id, 
        l.row_id
    FROM %7$s m 
    LEFT JOIN system.%7$s_linnaeus l ON l.row_id = m.obm_id
    LEFT JOIN %2$s_terms t ON t.data_table = %3$s AND t.subject = %4$s AND m.%6$s = t.term
    WHERE 
        (
            (l.updated_at IS NULL AND l.%1$s IS NULL) OR 
            l.updated_at IS NOT NULL 
        ) AND
        (
            t.term_id IS NOT NULL OR
            l.%1$s IS NOT NULL
        )
)
UPDATE system.%7$s_linnaeus l SET %1$s = ARRAY[tu.term_id] FROM tu WHERE l.row_id = tu.row_id;

-- match_terms_multiterm
WITH %2$s_split as (
    SELECT *, trim(unnest(regexp_split_to_array(%6$s,E',\\s*'))) as %6$s_split FROM %7$s
),
tu as (
    SELECT DISTINCT
        l.row_id,
        array_agg(t.term_id) as term_id
    FROM %2$s_split m
    LEFT JOIN %2$s_terms t ON t.data_table = %3$s AND t.subject = %4$s AND t.term = m.%6$s_split
    LEFT JOIN system.%7$s_linnaeus l ON l.row_id = m.obm_id
    WHERE 
        (
            (l.updated_at IS NULL AND l.%1$s IS NULL) OR 
            l.updated_at IS NOT NULL 
        ) AND
        (
            t.term_id IS NOT NULL OR
            l.%1$s IS NOT NULL
        )
    GROUP BY row_id
)
UPDATE system.%7$s_linnaeus l SET %1$s = tu.term_id FROM tu WHERE l.row_id = tu.row_id;

-- l_sync_data_linnaeus
CREATE OR REPLACE FUNCTION "%s" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE 
    tbl varchar;
    row_id integer;
BEGIN
    tbl := TG_TABLE_NAME;
    IF (TG_OP = 'INSERT') THEN
        row_id = NEW.obm_id;
        EXECUTE FORMAT('INSERT INTO system.%%s_linnaeus (row_id) VALUES (%%L);', tbl, row_id);
        RETURN NEW;
    ELSIF (TG_OP = 'UPDATE') THEN
        row_id = NEW.obm_id;
        EXECUTE FORMAT('UPDATE system.%%s_linnaeus SET updated_at = NOW() WHERE row_id = %%L;', tbl, row_id);
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$;

-- l_sync_terms_linnaeus
CREATE OR REPLACE FUNCTION "%2$s" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE 
    columns record;
    cmd character varying;
BEGIN
    IF (TG_OP = 'UPDATE') THEN
        EXECUTE FORMAT('UPDATE system.%%s_linnaeus SET updated_at = NOW() WHERE ARRAY[%%L]::integer[] <@ %%s;', OLD.data_table, OLD.term_id, OLD.subject || '_ids');
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        EXECUTE FORMAT('UPDATE system.%%s_linnaeus SET updated_at = NOW() WHERE ARRAY[%%L]::integer[] <@ %%s;', OLD.data_table, OLD.term_id, OLD.subject || '_ids');
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$;

-- create_taxon_matview
CREATE MATERIALIZED VIEW "temporary_tables"."%1$s_taxon_%2$s_%3$s" as SELECT
    CASE WHEN usr.word IS NOT NULL 
        THEN usr.word || ' / ' || fb.word 
        ELSE fb.word 
    END as word, 
    CASE WHEN usr.word IS NOT NULL 
        THEN usr.meta || ' / ' || fb.meta 
        ELSE fb.meta 
    END as meta,                                                                                                                  
    fb.taxon_id,                                                          
    fb.taxon_db,
    CASE WHEN usr.status = 'accepted' AND fb.status = 'accepted'
        THEN 'accepted'
        ELSE 'other'
    END as status,
    usr.lang || ' / ' || fb.lang as lang,
    -1 as wid
FROM %1$s_taxon fb
LEFT JOIN %1$s_taxon usr ON (usr.lang = %4$s AND usr.taxon_id = fb.taxon_id)
WHERE fb.lang = %5$s;

-- reset_updated_at
UPDATE system.%1$s_linnaeus SET updated_at = NULL WHERE updated_at IS NOT NULL;

