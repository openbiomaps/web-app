<?php

/*
// Create tar file with export_attachments.php job
require_once('admin_pages/jobs.php');
$p = new job_module('export_attachments',PROJECTTABLE);
$p->set_filename('export_attachments.php');
$p->set_runOpts(json_encode(array('protocol'=>$protocol,'table'=>$_POST['download_files'])));
$p->runJob('export_attachments');
 */
class export_attachments_load extends iapi {

    public static $main_taxon_field;

    public function execute($params) {
        global $ID;

        // turn off max_execution_time limit for this script
        set_time_limit(0);
        
        $j = json_decode($params,true);
        $filename = isset($j['options']['filename']) ? $j['options']['filename'] : genhash(12);

        // Autoload the dependencies
        require getenv('OB_LIB_DIR').'vendor/autoload.php';

        // enable output of HTTP headers
        $options = new ZipStream\Option\Archive();
        $options->setSendHttpHeaders(true);

        // create a new zipstream object
        $zip = new ZipStream\ZipStream('export_attachments_'.$filename.'.zip', $options);

        $path = getenv('PROJECT_DIR').'local/attached_files';

        $dir = opendir($path);
        $files = array();
        while (false !== ($fname = readdir($dir)))
        {
            if (is_file($path.'/'.$fname))
            {
                $files[] = $fname;
            }
        }
        closedir($dir);

        #debug($j,__FILE__,__LINE__);
        $DATATABLE = isset($j['options']['table']) ? $j['options']['table'] : PROJECTTABLE;

        $filter_file = '';
        if (isset($j['options']['filter_files'])) {
            
            $main_cols = dbcolist('columns',$j['options']['filter_files_table']);
            $main_cols[] = 'obm_id';
            $main_cols[] = 'reference';
            
            $items = array_map(function($item) {
                return "'$item'";
            }, explode(",",$j['options']['filter_files']));

            if (isset($j['options']['filter_files_column']) and in_array($j['options']['filter_files_column'],$main_cols))
                $filter_file = sprintf(" AND f.%s IN (%s)",$j['options']['filter_files_column'],implode($items,','));
        
        }
        elseif (isset($_SESSION['filter_files']) and $_SESSION['filter_files']!='') {

            $main_cols = dbcolist('columns',$_SESSION['filter_files_table']);
            $main_cols[] = 'obm_id';
            if (isset($_SESSION['filter_files_column']) and in_array($_SESSION['filter_files_column'],$main_cols))
                $filter_file = sprintf(" AND %s IN (%s)",$_SESSION['filter_files_column'],$_SESSION['filter_files']);
        }

        $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                    FROM system.files f
                    LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                    LEFT JOIN %4$s k ON (k.obm_files_id=fc.conid)
                    WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                           (k.obm_files_id IS NULL AND f.data_table=%1$s) ) %5$s
                    GROUP BY f.id,f.reference,f.comment
                    ORDER BY f.datum',quote($DATATABLE),'obm_id',PROJECTTABLE,$DATATABLE, $filter_file);

        $res = pg_query($ID,$cmd);

        while ($row = pg_fetch_assoc($res)) {

            $key = array_search($row['reference'],$files);
            if ($key!==false) {    
                $fname = $files[$key];
                
                $conids = preg_split('/,/',$row['conid']);
                foreach($conids as $conid) {
                    
                    if (!isset($j['options']['conid']) or (isset($j['options']['conid']) and $j['options']['conid']!='off') )
                        $valid_filename = preg_replace('/[*]/','_',$conid.'_'.$fname);
                    else
                        $valid_filename = $fname;

                    $zip->addFileFromPath($valid_filename, $path.'/'.$fname);
                }
            }
        }
        if (file_exists($path.'/export_data_'.$filename.'.json')) {
            $zip->addFileFromPath('data.json', $path.'/export_data_'.$filename.'.json');
        }

        // finish the zip stream
        $zip->finish();
        
        return common_message('ok','done');

    }
}

?>
