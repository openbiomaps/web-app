<?php
/* AJAX request functions
 *
 * */
 require(getenv('OB_LIB_DIR').'db_funcs.php');

 if (session_status() == PHP_SESSION_NONE) {
     session_start();
 }

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require(getenv('OB_LIB_DIR').'auth.php');
require(getenv('OB_LIB_DIR').'prepare_auth.php');

pg_query($ID,'SET search_path TO system,public,temporary_tables');
pg_query($GID,'SET search_path TO system,public,temporary_tables');

require(getenv('OB_LIB_DIR').'languages.php');

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

$mtable = $_REQUEST['data_table'] ?? $_SESSION['current_query_table'] ?? PROJECTTABLE;

# table modules
$modules = new modules($mtable);
# project modules
$x_modules = new x_modules();

if (!defined('STYLE_PATH')) {
    $style_def = constant('STYLE');
    $style_name = $style_def['template'];
    define('STYLE_PATH',"/styles/app/".$style_name);
}

/************** Include the ajax method of the modules ************************/
if (isset($_REQUEST['m'])) {
    switch($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $request = &$_GET;
            break;
        case 'POST':
            $request = &$_POST;
            break;
        default:
    }

    $m = preg_replace("/[^A-Za-z0-9-_]/", "", $request['m']);
    #debug('Requested module: ' . $m,__FILE__,__LINE__);
    if ($modules->is_enabled($m,$mtable)) {
        if (!is_array($request))
            $request = array($request);
        $modules->_include($m,'ajax',$request);
    } elseif ($x_modules->is_enabled($m)) {
        if (!is_array($request))
            $request = array($request);
        $x_modules->_include($m,'ajax',$request);
    } else {
        $err_msg = "$m module is not enabled!";
        log_action($err_msg, __FILE__,__LINE__);
        echo common_message('error', $err_msg);
        exit;
    }

    exit;
}

/******** GET REQUEST PROCESSING ************/

/*export a module file */
if (isset($_GET['module_export']) and vreq()) {
    if ( file_exists(sprintf("%sincludes/modules/private/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']))) )
        $file = sprintf("%sincludes/modules/private/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']));
    elseif ( file_exists(sprintf("%sincludes/modules/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']))) ) {
        $file = sprintf("%sincludes/modules/%s",getenv('PROJECT_DIR'),basename($_GET['module_export']));
    }
    else {
        printf('no such file');
        exit;
    }
    print_file_content($file, $_GET['module_export']);
    exit;
}

if (isset($_GET['get_file_content']) and has_access('master')) {
    list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt(preg_replace('/ /','+',$_GET['get_file_content']),MyHASH);
    print_file_content($decrypted_text, '', false);
    exit;
}

if (isset($_GET['download-query-results'])) {

    $iv = obm_cache('get',"download-query-results-iv-".$_GET['id'],'','',FALSE);

    if ($iv === false )
    {
        echo common_message('error','Expired');
        exit;
    }
    $cipher="AES-128-CBC";

    $_GET['download-query-results'] = preg_replace('/\s/','+',$_GET['download-query-results']);
    $cmd = openssl_decrypt(base64_decode($_GET['download-query-results']), $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA, $iv);
    
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename='export-query.csv';");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);

    $fp = fopen('php://output', 'w');

    $result = pg_query($ID,$cmd);
    $i = pg_num_fields($result);
    $fieldnames = array();
    for ($j = 0; $j < $i; $j++) {
        $fieldnames[] = pg_field_name($result, $j);
    }
    fputcsv($fp, $fieldnames);

    while ($row = pg_fetch_assoc($result)) {
        fputcsv($fp, $row);
    }
    fclose($fp);
    exit;
}

if (isset($_GET['exportimport'])) {

    if ($_GET['exportimport'] == '') {
        // amikor nincs ref, akkor új feltöltés felől jövünk és a legutolsó mentést exportáljuk
        $cmd = sprintf("SELECT file,ref FROM system.imports WHERE file='upload_%s_%s' AND datum > (NOW() - '1 day'::interval) ORDER BY datum DESC LIMIT 1",PROJECTTABLE,session_id());
    } else {
        $REF = quote(preg_replace("/[^0-9a-zA-Z]/",'',$_GET['exportimport']));
        $cmd = "SELECT file,ref FROM system.imports WHERE ref=$REF";
    }
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $filename = $row['file'].'_'.$row['ref'].'.csv';

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=\"$filename\";");
        header("Expires: -1");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        $cmd = sprintf('SELECT data FROM temporary_tables."%1$s_%2$s"',$row['file'],$row['ref']);
        $res = pg_query($ID,$cmd);

        $fp = fopen('php://output', 'w');
        while ($row = pg_fetch_assoc($res)) {
            fputcsv($fp, json_decode($row['data'],true));
        }
        fclose($fp);

    }

    exit;
}
if (isset($_GET['check-captcha'])) {
    echo ($_SESSION['phrase'] === $_GET['check-captcha']) ? common_message('ok','ok') : common_message('fail', str_captcha_does_not_match );
    exit;
}

if (isset($_GET['refresh-captcha'])) {
    require_once('vendor/autoload.php');
    
    $builder = new Gregwar\Captcha\CaptchaBuilder;
    $builder->build();
    $_SESSION['phrase'] = $builder->getPhrase();
    
    header('Content-type: image/jpeg');
    $builder->output();
    exit;
}

# jobs-admin
#
/*export a job file */
if (isset($_GET['export-editor-file']) and vreq() and has_access('master')) {


    list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt(preg_replace('/ /','+',$_GET['file']),MyHASH);

    $decrypted_text = preg_replace("/\.\./","",$decrypted_text); // remove relative path
    $decrypted_text = preg_replace("/local_vars.php.inc/","",$decrypted_text); // remove sensitive files

    $path = pathinfo($decrypted_text);
    
    $req_dir = preg_replace("|/|","#",$path['dirname']);
    $projdir = preg_replace("|/|","#",getenv('PROJECT_DIR'));

    if (preg_match("/^$projdir/",$req_dir)) {

        if ( file_exists(sprintf("%s",$decrypted_text)) ) {
            $file = sprintf("%s",$decrypted_text);
            $filename = sprintf("%s",basename($decrypted_text));
            print_file_content($file, $filename);
        }
        else {
            printf('no such file');
            exit;
        }
    }

    exit;
}

if (isset($_GET['export_data_sheet']) && $_GET['export_data_sheet'] == 'pdf') {

    require_once(getenv('OB_LIB_DIR') . 'prepare_vars.php');
    
    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }
    
    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
        $_SESSION['current_query_table'] = $_GET['table'];
    }
    
    require_once("iapi.php");
    $uri = "/api/data_sheet/prepare_data/load.php?export_data_sheet=pdf";
    $call = iapi::load($uri);
    $r = json_decode($call,true);
    $html = ($r['status'] == 'success') ? render_view('data-sheet-page-pdf', $r['data'], false) : render_view('error-page', ['error_message' => $r['message']]);
    $filename = "{$_SESSION['current_query_table']}_{$r['data']['obm_id']}_data_sheet.pdf";
    $_SESSION['current_query_table'] = $restore_cqt;

    require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML(file_get_contents(getenv('PROJECT_DIR') . 'css/pure/pure-min.css'), \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML(file_get_contents(getenv('PROJECT_DIR') . STYLE_PATH . '/scss/data-sheet-page-pdf.css'), \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename, "D");

    exit;
}

if (isset($_GET['load_data-sheet-page_access_control'])) {
    require_once(getenv('OB_LIB_DIR') . 'prepare_vars.php');
    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }
    $restore_cqt = $_SESSION['current_query_table'];
    if (isset($_GET['table'])) {
        if (preg_match("/^".PROJECTTABLE."/",$_GET['table']))
        $_SESSION['current_query_table'] = $_GET['table'];
    }
    require_once("iapi.php");
    $uri = "/api/data_sheet/load_access_control/load.php";
    $call = iapi::load($uri);
    $r = json_decode($call,true);
    echo render_view('pages/partials/data-sheet-page_access_control', $r['data'], false);
    exit;
}

/******** POST REQUEST PROCESSING ***********/

if (isset($_POST['selectMainTable']) and has_access('master')) {
    list($schema,$table) = preg_split("/\./",$_POST['selectMainTable']);

    $cmd = sprintf('SELECT *
      FROM information_schema.columns
      WHERE table_schema = %1$s
        AND table_name   = %2$s',
            quote($schema),quote($table));

    $res = pg_query($ID,$cmd);
    $d = array();
    while ($row = pg_fetch_assoc($res)) {
        $d[] = "d.".$row['column_name'];
    }

    echo common_message('ok',$d);
    exit;
}
if (isset($_POST['get_columns_of_table']) and has_access('master')) {
    list($schema, $table) = preg_split("/\./",$_POST['get_columns_of_table']);

    $cmd = sprintf('SELECT *
      FROM information_schema.columns
      WHERE table_schema = %1$s
        AND table_name   = %2$s',
            quote($schema),quote($table));

    $res = pg_query($ID,$cmd);
    $d = array();
    while ($row = pg_fetch_assoc($res)) {
        $d[] = $row['column_name'];
    }

    echo common_message('ok',$d);
    exit;
}

if (isset($_POST['refreshViewCode']) and has_access('master')) {

    $main_table = $_POST['mainTable'];
    $join1_table = $_POST['joined1Table'];

    $main_table_columns = isset($_POST['mainTableColumns']) ? $_POST['mainTableColumns'] : array();
    $join1_columns = isset($_POST['joined1Columns']) ? $_POST['joined1Columns'] : array();
    
    $mainTableJoinColumn = $_POST['mainTableJoinColumn'];
    $joined1TableJoinColumn = $_POST['joined1TableJoinColumn'];

    $schema = isset($_POST['schema']) ? $_POST['schema'] : 'public';
    $view_name = isset($_POST['view_name']) ? $_POST['view_name'] : $main_table.'_view';

    #$main_table_columns = array_map(
    #    function($x){return "d.".$x;},
    #        explode(",",$main_table_columns));

    $join1_columns = array_map(
        function($x){
            return "        vf.".$x." AS vf_".$x;
        },
            $join1_columns);

    $join1_statement = "$mainTableJoinColumn = vf.$joined1TableJoinColumn";

    $cmd = sprintf('CREATE VIEW %7$s.%1$s AS
    SELECT 
-- COLUMNS FROM THE MAIN TABLE
%2$s,
-- COLUMNS FROM THE JOINED TABLE
%4$s
-- MAIN TABLE REFERENCE
    FROM %8$s d
-- A JOIN STATEMENT
    LEFT JOIN %3$s vf ON (%5$s);',
    $view_name,
    implode(",\n",$main_table_columns),
    $join1_table,
    implode(",\n",$join1_columns),
    $join1_statement,
    '',
    $schema,
    $main_table
    );

    echo common_message('ok',$cmd);
    exit;
}

if (isset($_POST['save_view']) and has_access('master')) {
    $view_name = "";
    $view_cmd = $_POST['content'];
    $table = $_POST['table'];
    if (preg_match("/\./",$_POST['table'])) {
        list($schema,$table) = preg_split("/\./",$_POST['table']);
    } else {
        $table = $_POST['table'];
        $schema = 'public';
    }
    $errors = array();
    $alter_cmd = '';
    $existing_view = false;
    $move_table = ($_POST['move_table'] == 't') ? true : false;

    if ($move_table) {
        $cmd = sprintf('SELECT EXISTS (
        SELECT FROM 
            information_schema.tables 
        WHERE 
            table_schema LIKE \'%s\' AND 
            table_type LIKE \'BASE TABLE\' AND
            table_name = \'%s\'
        );',PROJECTTABLE,$table);
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            if ($row['exists'] == 'f') {
                $alter_cmd = sprintf('ALTER TABLE %s SET SCHEMA %s;',$table,PROJECTTABLE);  // move table to the Project's own schema
            }
        } else {
             $errors[] = "Schema query error.";
        }
    }
    
    if ($_POST['save_view']=='') {
        # New View
        if (preg_match("/CREATE VIEW (.+) AS/i",$_POST['content'], $m)) {
            $view_name = $m[1];
            $tables = getProjectTables(true);
            list($new_schema,$new_table) = preg_split("/\./",$view_name);
            $create_new_table = true;
            foreach ($tables as $t) {
                if ($t == $view_name) {
                    $create_new_table = false;
                }
            }
            if ($create_new_table and !$move_table) {
                $cmd = sprintf("INSERT INTO header_names (f_project_schema, f_project_name,f_project_table) VALUES (%s,'%s',%s)",
                    quote($new_schema),
                    PROJECTTABLE,
                    quote($new_table));
                $res = pg_query($BID,$cmd);
                if (pg_last_error($BID)) {
                    $errors[] = pg_last_error($BID);
                }
            }
            /*elseif ($create_new_table and $move_table) ...
             * move table esetén lehet, hogy egyes oszlopok többé nem felelnek meg többé  a header names táblának, kézzel kell módosítani!!
             */
        }
    } else {
        # Existing View
        $view_name = $_POST['save_view'];
        $existing_view = true;
        
        $view_cmd = sprintf('DROP VIEW IF EXISTS %1$s;',$view_name);

        if (!preg_match("/CREATE VIEW/i",$_POST['content'])){
            $view_cmd .= sprintf("CREATE VIEW %s AS %s",$view_name,$_POST['content']);
        }
        if (preg_match("/\./",$_POST['table'])) {
            list($schema,$table) = preg_split("/\./",$_POST['table']);
        } else {
            $table = $_POST['table'];
            $schema = 'public';
        }
    }

    $cmd = sprintf('BEGIN;');
    if ($move_table) {
        $cmd .= sprintf('CREATE SCHEMA IF NOT EXISTS %s;%s;',PROJECTTABLE,$alter_cmd);
    }
    $cmd .= $view_cmd;

    # Only drop_view - create_view process 
    if (!$move_table) {
        $res = pg_query($ID,$cmd);
        if (pg_last_error($ID)) {
            $errors[] = pg_last_error($ID);
            log_action($cmd,__FILE__,__LINE__);
        }

        if (count($errors)) {
            pg_query($ID,'ROLLBACK');
            echo common_message('error', pg_last_error($ID).'; '.implode("; ",$errors));
        } else {
            pg_query($ID,'COMMIT');
            echo common_message('ok','ok');
        }
        exit;
    }

    # Move table things
    #
    # An instead of RULE
    preg_match_all('/\s+(d|vf\d*)\.([a-zA-Z0-9_]+)(\s+AS\s+([a-zA-Z_0-9]+))?,?$/misU', $view_cmd, $m);
    $column_state = $m[1];
    $all_columns = $m[2];
    $as_ = $m[4];
    $columns = array(); // Main table column with their orgianl names
    $renamed_columns = array(); // Main and joined table columns with their new (alias) name
    $return_columns = array(); // Returning columns from the INSERT Rule
    $update_columns = array(); // Update column statements for the UPDATE Rule

    for ($i=0; $i<count($all_columns); $i++) {
        if ($column_state[$i] == 'd') {
            if ($as_[$i] != '') {
                if ($all_columns[$i] != 'obm_id') {
                    $renamed_columns[] = $as_[$i];
                    $renamed_columns_i = $as_[$i];
                }
            } else {
                if ($all_columns[$i] != 'obm_id') {
                    $renamed_columns[] = $all_columns[$i];
                    $renamed_columns_i = $all_columns[$i];
                }
            }
            if ($all_columns[$i] == 'obm_id') {
                $return_columns[] = 'obm_id::int';#'NULL::int';
                $update_columns[] = 'NULL::int';
            } else {
                $columns[] = $all_columns[$i];
                $return_columns[] = $all_columns[$i];
                $update_columns[] = "{$all_columns[$i]} = NEW.{$renamed_columns_i}";
            }
        } elseif (preg_match("/vf\d*/",$column_state[$i])) {
            $return_columns[] = 'NULL::character varying';
        } elseif (preg_match("/COALESCE/",$column_state[$i])) {
            $return_columns[] = 'NULL::character varying';
        }
    }
    
    $new_columns = array_map(function ($x) {
        if ($x == 'obm_datum')
            return 'now()';
        else
            return "NEW.$x";
    },$renamed_columns);

    $idx = array_search('--',$update_columns);
    array_splice($update_columns, $idx, 1);
    
    $cmd .= sprintf('DROP RULE IF EXISTS %1$s_ins ON %1$s;',$table);
    $cmd .= sprintf('CREATE RULE %1$s_ins AS ON INSERT TO public.%1$s
        DO INSTEAD
        INSERT INTO %5$s.%1$s (%2$s) VALUES (%3$s) RETURNING
         %4$s;',
            $table,
            implode(",",$columns),
            implode(",",$new_columns),
            implode(",",$return_columns),
            PROJECTTABLE);

    /*debug( sprintf('CREATE RULE %1$s_ins AS ON INSERT TO public.%1$s
        DO INSTEAD
        INSERT INTO %5$s.%1$s (%2$s) VALUES (%3$s) RETURNING
         %4$s;',
            $table,
            implode(",",$columns),
            implode(",",$new_columns),
            implode(",",$return_columns),
            PROJECTTABLE) );*/

    $cmd .= sprintf('DROP RULE IF EXISTS %1$s_upd ON %1$s;',$table);
    $cmd .= sprintf('CREATE RULE %1$s_upd AS ON UPDATE TO public.%1$s
        DO INSTEAD
        UPDATE %3$s.%1$s SET
            %2$s 
    WHERE
         obm_id = NEW.obm_id;',
            $table,
            implode(",",$update_columns),
            PROJECTTABLE);

    $cmd .= sprintf('DROP RULE IF EXISTS %1$s_del ON %1$s;',$table);
    $cmd .= sprintf('CREATE RULE %1$s_del AS ON DELETE TO public.%1$s
        DO INSTEAD
        DELETE FROM %2$s.%1$s 
        WHERE
         obm_id = OLD.obm_id;',
            $table,
            PROJECTTABLE);

    $cmd .= sprintf('ALTER SCHEMA %3$s OWNER TO %2$s;
                     ALTER VIEW %1$s OWNER TO %2$s;', $view_name, PROJECTTABLE."_admin", PROJECTTABLE);

    $res = pg_query($GID,$cmd);
    if (pg_last_error($GID)) {
        $errors[] = pg_last_error($GID);
    }

    if (count($errors)) {
        pg_query($GID,'ROLLBACK');
        echo common_message('error', pg_last_error($GID).'; '.implode("; ",$errors));
    } else {
        pg_query($GID,'COMMIT');
        echo common_message('ok','ok');
    }

    exit;
}

if (isset($_POST['edit_view_name']) and has_access('master')) {

    if (preg_match("/(.+)\.(.+)$/i",$_POST['edit_view_newname'],$m)) {
        $schema = $m[1];
        $new_name = $m[2];
    }
    else {
        #$new_name = preg_replace("/([0-9a-z_]+\.)?([0-9a-z_]+)$/i",'$2',$_POST['edit_view_newname']);
        $schema = 'public';
        $new_name = preg_replace("/[^0-9a-z_]/i","",$_POST['edit_view_newname']);
    }
    if (preg_match("/(.+)\.(.+)$/i",$_POST['edit_view_name'],$m)) {
        $old_schema = $m[1];
        $old_name = $m[2];
    } else {
        $old_schema = 'public';
        $old_name = $_POST['edit_view_name'];
    }

    if (!preg_match("/^".PROJECTTABLE."_/",$new_name) and $new_name != PROJECTTABLE) {
        $new_name = PROJECTTABLE . "_" . $new_name;
    }

    $errors = array();

    $cmd = sprintf('BEGIN; ALTER VIEW %s RENAME TO %s',$_POST['edit_view_name'],$new_name);
    $res = pg_query($ID,$cmd);

    if (pg_last_error($ID)) 
        $errors[] = pg_last_error($ID);
    else {
        $cmd = sprintf("UPDATE header_names SET f_project_table = %s WHERE f_project_schema=%s AND f_project_name=%s AND f_project_table=%s",
                    quote($new_name),
                    quote($old_schema),
                    quote(PROJECTTABLE),
                    quote($old_name));
        $res = pg_query($BID,$cmd);
        if (pg_last_error($BID)) {
            $errors[] = pg_last_error($BID);
        }
    }

    if (count($errors)) {
        pg_query($ID,'ROLLBACK');
        echo common_message('error',implode($errors,'; '));
    } else {
        pg_query($ID,'COMMIT');
        echo common_message('ok','ok');
    }

    exit;
}

if (isset($_POST['drop_view']) and has_access('master')) {

    list($schema,$table) = preg_split("/\./",$_POST['drop_view']);

    $cmd = sprintf('BEGIN;DROP VIEW %s.%s', $schema, $table);
    $res = pg_query($ID,$cmd);
    if (pg_last_error($ID)) 
        echo common_message('error',pg_last_error($ID));
    else {
        if ($schema != 'public') {

/*
 *      Lehetne törölni, de jobb kézzel, mert nehezen követhető, hogy mit kell kitörölni...
        $cmd = sprintf("DELETE FROM header_names WHERE f_project_schema=%s AND f_project_name=%s AND f_project_table=%s ",
            quote($schema),
            PROJECTTABLE,
            quote($table));
        $res = pg_query($BID,$cmd);
        if (pg_last_error($BID)) {
            pg_query($ID,'ROLLBACK');
            echo common_message('error',pg_last_error($ID));
        } else {
            pg_query($ID,'COMMIT');
            echo common_message('ok','ok');
        }
 */
        }
        pg_query($ID,'COMMIT');
        echo common_message('ok','ok');
    }

    exit;
}
if (isset($_POST['refresh_view']) and has_access('master')) {

    list($schema,$table) = preg_split("/\./",$_POST['refresh_view']);

    $cmd = sprintf('REFRESH MATERIALIZED VIEW %s.%s', $schema, $table);
    $res = pg_query($ID,$cmd);
    if (pg_last_error($ID)) {
        echo common_message('error',pg_last_error($ID));
    } else {
        echo common_message('ok','ok');
    }

    exit;
}


if (isset($_POST['put_file_content']) and has_access('master')) {
    $content = $_POST['put_file_content'];
    list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt(preg_replace('/ /','+',$_POST['path']),MyHASH);
    if (file_exists($decrypted_text)) {
        
        if (file_put_contents($decrypted_text,$content)===false) {
            echo common_message('error','Permission denied??');
        } else {
            debug(3);
            echo common_message('ok','OK');
        }
    
    }

    exit;
}
/* bottom panel, cookie warning
 *
 * */
if (isset($_POST['cookies_accepted'])) {
    $_SESSION['cookies_accepted'] = 1;
    exit;
}
/* Called: maps.js
 * Set current_query_table link
 * */
if (isset($_POST['set-current-query-table'])) {
    unset($_SESSION['current_query_keys']);
    unset($_SESSION['query_build_string']);
    unset($_SESSION['qf']);
    unset($_SESSION['filter_type']);
    unset($_SESSION['orderby']);
    unset($_SESSION['orderascd']);
    unset($_SESSION['orderad']);
    list($schema,$table) = preg_split('/\./',$_POST['set-current-query-table']);
    //unset($_SESSION['']);
    //"qform":[],
    //"qf":{"szamossag":["darab"]},
    //"taxon_join_filter":0,
    //"uploading_join_filter":0,
    //"filter_type":["text: [\"darab\"]"],
    //"query_build_string":"\"szamossag\" IN ($MtcDqgFhNfzdVbmj$darab$MtcDqgFhNfzdVbmj$)"
    $cmd = sprintf('SELECT 1 FROM header_names 
        WHERE f_project_name=\'%1$s\' AND f_project_schema=%3$s AND f_project_table=%2$s',
            PROJECTTABLE,
            quote($table),
            quote($schema));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $_SESSION['current_query_schema'] = $schema;
        $_SESSION['current_query_table'] = $table;

        if (defined('SHINYURL') and constant("SHINYURL"))
            echo common_message('ok',$protocol.'://'.URL.'/table/'.$_POST['set-current-query-table'].'/?map');
        else
            echo common_message('ok',$protocol.'://'.URL.'/?qtable='.$_POST['set-current-query-table'].'&map');
    } else {
        echo common_message('error','Unknown table');
    }
    exit;
}
/* Called: main.js
 * Project admin: upload forms, deactivate a form
 * */
if (isset($_POST['deacform']) and $_POST['deacform'] and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('upload_forms')) {
        if($_POST['active']==1) $x = 2;
        elseif($_POST['active']==2) $x = 1;
        else {
            exit;
        }
        $cmd = sprintf("UPDATE project_forms SET \"active\"=$x WHERE form_id=%d",$_POST['deacform']);
        $res = pg_query($BID,$cmd);
    }
    exit;
}
/* Called: main.js
 * Project admin: upload forms, delete a form
 * */
if (isset($_POST['delform']) and $_POST['delform'] and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('upload_forms')) {
        $cmd = sprintf("DELETE FROM project_forms WHERE form_id=%d AND project_table='%s'",$_POST['delform'],PROJECTTABLE);
        $res = pg_query($BID,$cmd);
    }
    exit;
}
// check POST Content-Length x bytes exceed
if (isset($_POST['check_file_size'])) {
    //debug((substr(ini_get('post_max_size'), 0, -1) ));
    if ($_POST['check_file_size'] > (substr(ini_get('post_max_size'),0, -1) * 1024 * 1024))
        echo common_message('error','Post max size is: '.ini_get('post_max_size'));
    else
        echo common_message('ok','ok');
    exit;
}
/* Upload file
 * Called: maps.js, admin.js
 * XMLHttpRequest() automatikus fájl feltöltés
 * Description: shp feltöltés térbeli lekérdezéshez a filterbox_load_layer() függvényből hívva
 *              main map page shape file upload
 *              modules files upload, 
 *              computation packages,
 *              ...
 * */

if (isset($_POST['load-files']) and vreq()) {

    $dest = ''; //default temporay destination - clean automatically

    if ($_POST['ext']=='shp') {
        //shp feltöltés
        $dest = sys_get_temp_dir()."/ob_shp_upl_".session_id();
    }
    elseif ($_POST['ext']=='list') {
        //form list for list type
        $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
        if ($_FILES['files']['size'][0]>4096) {
            $rr['err'][] = 'File size exceeds the allowed size!';
            echo json_encode($rr);
            exit;
        }
        // nincs befejezve!
        $_SESSION['upl_form_listfile'] = file_get_contents($_FILES['files']['tmp_name'][0]);
        exit;
    }
    elseif ($_POST['ext']=='pict') {
        //képfeltöltés
        $dest = getenv('PROJECT_DIR').'local/attached_files';
    }
    elseif ($_POST['ext']=='comp-package') {
        //Computational package files feltöltés
        $dest = getenv('PROJECT_DIR').'computational_packages/'.$_POST['exu'];
    }
    elseif ($_POST['ext']=='language-files') {
        $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
        $rr = language_file_process();
        echo json_encode($rr);
        exit;
    }
    elseif ($_POST['ext']=='module-files') {
        //non php files can getting through!!!!!!!
        $e = '';

        $dest = getenv('PROJECT_DIR').'local/includes/modules';
        $m = array();
        preg_match("/(.+)\.(\w+)$/",$_FILES['files']['name'][0],$m);
        $uploaded_tmp_file = $_FILES['files']['tmp_name'][0];

        $zip_process = false;
        $zip_files = array();
        if (isset($m[2]) and $m[2] == 'zip') {
            # Extract zip file
            # loop on content somehow... 
            $zip = new ZipArchive;
            $res = $zip->open($uploaded_tmp_file);
            if ($res === TRUE) {
                $overwrite = false;
                for( $i = 0; $i < $zip->numFiles; $i++ ){ 
                    $stat = $zip->statIndex( $i ); 
                    $zip_content = basename( $stat['name'] );
                    if (file_exists($dest.'/'.$zip_content)) {
                        $overwrite = true;
                        log_action('Module files cannot be overwritten from a Zip archive.',__FILE__,__LINE__);
                    }
                    if ($m[1].".php" == $zip_content) {
                        $uploaded_tmp_file = $dest.'/'.$zip_content;
                        $zip_files[] = $dest.'/'.$zip_content;
                    }
                }
                if (!$overwrite) {
                    $ze = $zip->extractTo($dest);
                    if ($ze)
                        $zip_process = true;
                }
                $zip->close();
            } else {
                echo common_message('fail', 'Zip fail error code:' . $res);
            }
        }


        if (!php_check_syntax($uploaded_tmp_file,$e)) {
            log_action('Module-file parse error: '.$e,__FILE__,__LINE__);
            $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
            $rr['err'][] = $e;
            echo common_message('error',"php sytax check failed for file. ".json_encode($rr['err']));
            if ($zip_process) {
                foreach ($zip_files as $z) {
                    unlink($z);
                }
            }
            exit;
        }
        // module yaml header check
        $content = file($uploaded_tmp_file);
        $process = 0;
        $yaml = "";
        $n = 0; // only looking for modules yaml header at the beggining of the file;
        foreach ($content as $rows) {
                if (($process === 0 or $process == 2 ) and preg_match("/^#'/",$rows)) {
                    $process = 2;
                } elseif ($process){
                    $process = 1;
                    continue;
                }
                if ($process == 2) {
                    $yaml .= preg_replace("/^#' /","",$rows);
                }
                if ($process === 0 and $n > 3) {
                    break;
                } 
                $n++;
        }
        if ($yaml != '') {
            $parsed = yaml_parse($yaml);
            if (!isset($parsed['Module'])) {
                echo common_message('error',"Module yaml not contains module name. ". basename($_FILES['files']['name'][0]));
                if ($zip_process) {
                    foreach ($zip_files as $z) {
                        unlink($z);
                    }
                }
                exit;
            }
            if (!isset($parsed['Methods'])) {
                echo common_message('error',"Module yaml not contains methods. ". basename($_FILES['files']['name'][0]));
                if ($zip_process) {
                    foreach ($zip_files as $z) {
                        unlink($z);
                    }
                }
                exit;
            }

        } else {
            echo common_message('error',"Module yaml header is missing. ". basename($_FILES['files']['name'][0]));
            if ($zip_process) {
                foreach ($zip_files as $z) {
                    unlink($z);
                }
            }
            exit;
        }
    }
    elseif ($_POST['ext']=='job-files') {
        //non php files can getting through!!!!!!!
        $e = '';
        
        $ext = pathinfo(basename($_FILES['files']['name'][0]), PATHINFO_EXTENSION);
        //$basename = pathinfo(basename($_FILES['files']['name'][0]), PATHINFO_BASENAME);
        if ($ext == 'php' and !php_check_syntax($_FILES['files']['tmp_name'][0],$e)) {
            log_action('Job-file parse error: '.$e,__FILE__,__LINE__);
            $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
            $rr['err'][] = $e;
            echo common_message('error',"php sytax check failed for file. ".json_encode($rr['err']));
            exit;
        }
        
        $dest = getenv('PROJECT_DIR').'jobs/run/';
        if ($_POST['exu']=='lib') {
            $dest .= 'lib/';
            $jobname = $_POST['pr3'].'.'.$ext;
        }
        elseif ($_POST['exu']=='run') {
            $jobname = $_POST['pr3'];
        }
        
        if (file_exists($dest.basename($jobname)))
            unlink($dest.basename($jobname)); // drop current version

        //overwrite uploaded file name to the expected module/job name
        $_FILES['files']['name'][0] = $jobname;
    }

    if (isset($_FILES['files'])) {
        $r = move_upl_files($_FILES['files'],$dest);
        $rr = json_decode($r,true);
        if (isset($rr['err']) and count($rr['err'])) {
            echo common_message('error',$rr['err'][0]);
            exit;
        }

    } else {
        echo common_message('error','No files sent');
        exit;

    }

    if ($_POST['ext']=='pict') {
        $rr['file_names'] = file_upload_process($rr['file_names'],$rr['sum'],$rr['type']);
        if ($rr['file_names']===false) {
            echo common_message('error','Upload process failed');
            exit;
        }
    }
    // return uploaded file names or a JSON ARRAY ['tmp'=>'','file_names'=>array(),'err'=>array()];
    //echo implode(',',$rr->{'file_names'});
    //echo json_encode($rr);
    echo common_message('ok',$rr);
    exit;
}
/* turn off local language definition for change it */
/*if (isset($_POST['turn_off_lang']) and vreq()) {
    $_SESSION['skip_local_lang'] = array($_SESSION['LANG']=>1);
    exit;
}*/
/*export language defintion */
/*if (isset($_GET['lang_export']) and vreq()) {
    $file = sprintf("%slanguages/local_%2s.php",getenv('PROJECT_DIR'),$_SESSION['LANG']);
    $content = file($file);
    $r = array("definition,value");
    foreach($content as $row) {
        $m = array();
        preg_match("/define\(['\"](\w+)['\"],['\"]?(.*)['\"]?\);/",$row,$m);
        if (isset($m[1]) and isset($m[2]))
            $r[] = "'$m[1]','$m[2]'";
    }
    $csv = implode("\n",$r);
    $filesize = strlen($csv);
    $filename = sprintf("%s.csv",$_SESSION['LANG']);
    # header definíció!!!
    header("Content-Type: text/csv");
    header("Content-Length: $filesize");
    header("Content-Disposition: attachment; filename=\"$filename\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    printf("%s",$csv);
    exit;
}*/
/* Custom shp Layer feltöltés
   az eredeti a query_builder-ben volt, mert egy query stringet csinált a végén */
if (isset($_POST['shaperead']) and vreq()) {
    session_write_close();

    $dir = sys_get_temp_dir()."/ob_shp_upl_".session_id();

    $s = glob("$dir/*.shp");
    $shp = array_pop($s);
    $data = array();
    if ($shp=='') {
        $s = glob("$dir/*.kml");
        // pszeudo shp
        $shp = $s[0];
        //$file_extension = (false === $pos = strrpos($_FILES["shpfile"]["name"][0], '.')) ? '' : substr($_FILES["shpfile"]["name"][0], $pos);
        //
        if (isset($s[0])) {
            $xml = file_get_contents($s[0]);
            $data = readKML($xml,'csv');
        } else {
            echo common_message('error','No files sent?');
            exit;
        }
    } else {
        if (!command_exist('ogrinfo')) {
            $errorID = uniqid();
            echo common_message('failed', "Ogrinfo error. ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".'Ogrinfo not found',__FILE__,__LINE__);
            exit;
        }
        exec("ogrinfo $shp 2>&1 >/dev/null", $r_stderr);
        if (!count($r_stderr)) {
            //general function to read data into an array
            $data = readSHP($shp,'csv','',true,'');
        } else {
            $errorID = uniqid();
            echo common_message('failed', str_shape_error.": ".implode(' ',$r_stderr).". ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".'Read shape error: '.implode(' ',$r_stderr),__FILE__,__LINE__);
            exit;
        }
    }

    $h = 0;
    //$header = array();
    $wkt = array();
    $wid = array();
    $cnt=1;
    $csv_rows_count = count($data);
    $message = array();
    $shp_header = array_shift($data);
    $name_idx = false;
    if (isset($_POST['shp_label'])) {
        $name_idx = array_search($_POST['shp_label'],explode(',',$shp_header));
    }

    foreach ($data as $l) {
        /* Nincs header!! a readSHP-ben el lett dobva!!
         * if ($h==0) {
              $h++;
              $header = str_getcsv($l,',','"');
              continue;
           }
         */
        $a = str_getcsv($l,',','"');
        $geom_string = preg_replace('/^(\w+) \(/','$1(',$a[0]);
        if(trim($geom_string)=='') continue;
        $h = count($a);
        if($cnt) {
                // csak maximum 1 geometriát adunk vissza megjelenítésre...
            $wkt[] = $a[0];
            $cnt--;
        }
        mb_internal_encoding("UTF-8");
        // the name of the shared polygon...
        if ($name_idx!==false)
            $name_column = $a[$name_idx];
        else
            $name_column = $a[1];

        //$path_parts = pathinfo($shp);
        //$polygon_name = mb_substr($name_column .' - '. $path_parts['filename'],0,128);
        $polygon_name = mb_substr($name_column,0,128);
        //$c = 0;
        /*foreach($a as $an) {
            if ($c==0) {
                $c++;
                continue;
            }
            $name .= ",".utf8_decode(urldecode($an));
            $c++;
        }*/

        // if logined user SAVE to named selections
        if (isset($_SESSION['Tid'])) {
            pg_query($GID,'BEGIN');
            $cmd = sprintf("INSERT INTO system.shared_polygons (\"geometry\",\"project_table\",\"name\",\"user_id\",\"original_name\",\"access\") VALUES (ST_GeomFromText(%s,4326),'".PROJECTTABLE."',%s,%d,%s,'public') RETURNING id",
                quote($geom_string),quote($polygon_name),$_SESSION['Tid'],quote($shp));
            $res = pg_query($GID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $cmd = sprintf("INSERT INTO system.polygon_users (user_id,polygon_id,select_view) VALUES (%d,%d,'%s')",
                    $_SESSION['Tid'],$row['id'],'select-upload');
                $res = pg_query($GID,$cmd);
                if (pg_affected_rows($res)) {
                    pg_query($GID,'COMMIT');
                    // visszaküldjük a WKT stringet, habár ez már nincs használva!!!!
                    //echo common_message('ok',json_encode($wkt));
                    $message[] = array('ok',json_encode($wkt));
                } else {
                    pg_query($GID,'ROLLBACK');
                    echo common_message('error',"Geometry saving error");
                    exec("rm -rf $dir");
                    exit;
                }
            } else {
                pg_query($GID,'ROLLBACK');
                echo common_message('error',"Geometry saving error");
                exec("rm -rf $dir");
                exit;
            }
        } else {
            echo common_message('error',"Should be logined!");
            exec("rm -rf $dir");
            exit;
        }
    }
    echo common_multi_message($message);
    // kitörli a /tmp/ob_shp_upl_... könyvtárat
    exec("rm -rf $dir");
    exit;
}
/* not used??
 *
 * */
if (isset($_POST['lbuf_reread'])) {
    require(getenv('OB_LIB_DIR').'interface.php');
    echo filterbox_load_selection();
    exit;
}

/* Load selections
 * Ajax request - called: maps.js, hrsz_query, box_load_selection
 * */
if (isset($_POST['getWktGeometry'])) {
    $id = preg_replace('/[^0-9]/','',$_POST['getWktGeometry']);

    if (isset($_POST['custom_table']) and isset($_SESSION['private_key'])) {

        $_SESSION['selection_geometry_type'] = 'customPolygon';

        $cipher="AES-128-CBC";
        $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
        $table_reference = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        $stcol = st_col($table_reference,'array');

        $cmd = "SELECT {$stcol['ID_C']} AS id,ST_AsText(ST_Transform({$stcol['GEOM_C']},4326)) as q FROM $table_reference WHERE {$stcol['ID_C']}='$id'";

    } else {
        //$cmd = sprintf('SELECT id,ST_AsText(geometry) as q FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id WHERE select_view IN (1,3) AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$id);

        $geom = (isset($_POST['buffer'])) ? sprintf("st_buffer(geometry::geography, %d)",$_POST['buffer']) : "geometry";
        $_SESSION['selection_geometry_type'] = 'sharedPolygon';
        $user_id = $_SESSION['Tid'] ?? 0;
        $cmd = sprintf(
            "SELECT id,ST_AsText(%s) as q
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id
            WHERE select_view IN ('only-select','select-upload') AND p.user_id=%d AND id='%d'", 
            $geom,
            $user_id, 
            $id
        );
    }

    $res = pg_query($ID,$cmd);
    if ($row = pg_fetch_assoc($res)){
            //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //$random = substr( str_shuffle( $chars ), 0, 16 );
            //$_SESSION['query_build_string'][$random] = $row['id'];
            //publikus hivatkozás string a lekérdezéshez : WKT string a kirajzoláshoz
            //ez lesz majd a _GET['qstr'] változó, amit a mapservernek a proxy rak össze.

            //print out geometry for drawing it on the map
            echo json_encode(array($row['q']));
            $_SESSION['selection_geometry_id'] = $row['id'];
    } else {
            //echo "t:"."no records available in the current access level";
            echo 0;
    }
    exit;
}
/**
* similar to the singular form, just it queries the geometries with one query
* it accepts the ids as an array
**/
if (isset($_POST['getWktGeometries']) && isset($_POST['custom_table']) and isset($_SESSION['private_key'])) {
    $ids = array_values(array_filter($_POST['getWktGeometries'], function($el) {
        return preg_match('/\d+/',$el);
    }));

    $_SESSION['selection_geometry_type'] = 'customPolygon';

    $cipher="AES-128-CBC";
    $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
    $table_reference = openssl_decrypt(base64_decode($_POST['custom_table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
    $stcol = st_col($table_reference,'array');

    $cmd = "SELECT {$stcol['ID_C']} AS id,ST_AsText(ST_Transform({$stcol['GEOM_C']},4326)) as q FROM $table_reference WHERE {$stcol['ID_C']} IN (".implode(',',$ids).")";

    $res = pg_query($ID,$cmd);
    while ($row = pg_fetch_assoc($res)){
            //$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //$random = substr( str_shuffle( $chars ), 0, 16 );
            //$_SESSION['query_build_string'][$random] = $row['id'];
            //publikus hivatkozás string a lekérdezéshez : WKT string a kirajzoláshoz
            //ez lesz majd a _GET['qstr'] változó, amit a mapservernek a proxy rak össze.
            $wkts[] = [$row['q']];
            //print out geometry for drawing it on the map
            $_SESSION['selection_geometry_id'] = $row['id'];
    }
    echo json_encode($wkts);
    exit;
}
/* Project admin
 * Called: main.js
 * Return: string output of image conversion
 * Testing mapserver map file
 * Not used anymore!!
 * */
if (isset($_POST['maptest']) and isset($_SESSION['Tid'])) {
    if (has_access(PROJECTTABLE,'mapserv')) {
        if ($_POST['maptest'] == 'public') $p = 'public';
        elseif ($_POST['maptest'] == 'private') $p = 'private';
        else exit;

        if (!isset($_SESSION['shp2img_process'])) {
            $cmd = "shp2img -m ".getenv('PROJECT_DIR')."$p/$p.map -o ".OB_TMP."test_".PROJECTTABLE."_".session_id().".png -all_debug 5 >".OB_TMP."test_".PROJECTTABLE."_".session_id().".log 2>&1";
            $ps = run_in_background($cmd,0,0);
            if ($ps) {
                $_SESSION['shp2img_process'] = $ps;
                echo '.';
            }
            else {
                echo 'no'; // process not started
                unlink(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
            }
        } else {
            if(is_process_running($_SESSION['shp2img_process'])) {
                echo '.';
            } else {
                if (file_exists(OB_TMP."test_".PROJECTTABLE."_".session_id().".log")) {
                    echo file_get_contents(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
                    unlink(OB_TMP."test_".PROJECTTABLE."_".session_id().".log");
                } else echo 'no'; // process previously strted but stopped without results
                unset($_SESSION['shp2img_process']);
            }
        }
    }
    exit;
}
/* Check given process exists
 * Return 1 or 0
 * not used anywhere
 * */
if (isset($POST['check_proc']) and isset($_SESSION['Tid'])) {
    session_write_close();
    //while(is_process_running($ps)) {
    if(is_process_running($ps)) {
        echo '.';
        ob_flush(); flush();
        //sleep(2);
    } else {
        echo 'no';
    }
    exit;
}
/* Project admin
 * Called: main.js
 * Return:
 * Description: Create project's SQL functions
 * */

if (isset($_POST['create-function']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('functions')) {
        $cmd = Create_Project_SQL_functions($_POST['schema'],$_POST['table'],$_POST['create-function'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Create {$_POST['create-function']} function: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            log_action("$ID: $cmd",__FILE__,__LINE__);
            return;
        }
    }
    exit;
}

if (isset($_POST['create-table']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('create_table')) {
        $cmd = Create_Project_plus_tables($_POST['schema'],$_POST['table'],$_POST['create-table'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Create {$_POST['create-table']} function: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            return;
        }
    }
    exit;
}

if (isset($_POST['enable-trigger-general']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('functions')) {
        $cmd = sprintf("SELECT EXISTS ( SELECT tgenabled FROM pg_trigger WHERE tgname='%s' AND tgenabled != 'D')",preg_replace("/[^a-z0-9_]+/","",$_POST['enable-trigger-general']));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);
        if($row['exists']=='t'){

            $cmd = sprintf('ALTER TABLE %3$s.%1$s DISABLE TRIGGER %2$s',$_POST['table'],$_POST['enable-trigger-general'],$_POST['schema']);
            $en = 'disabled';

        } else {
            $cmd = sprintf('ALTER TABLE %3$s.%1$s ENABLE TRIGGER %2$s',$_POST['table'],$_POST['enable-trigger-general'],$_POST['schema']);
            $en = 'enabled';
        }
        if (pg_query($ID,$cmd)) echo "Trigger {$_POST['enable-trigger-general']} $en";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            return;
        }


    }
    exit;
}


if (isset($_POST['enable-trigger']) and isset($_SESSION['Tid'])) {
    session_write_close();
    if (has_access('functions')) {
        $cmd = Create_Project_SQL_functions($_POST['schema'],$_POST['table'],$_POST['enable-trigger'],gisdb_user);
        if (pg_query($ID,$cmd)) echo "Setting {$_POST['enable-trigger']} trigger: done.";
        else {
            $e = pg_last_error($ID);
            echo "SQL error: $e";
            return;
        }
    }
    exit;
}

if (isset($_POST['rule-save'])) {
    $cmd = sprintf('CREATE OR REPLACE FUNCTION public.rules_%1$s()
        RETURNS TRIGGER AS %2$s LANGUAGE plpgsql;',
        $_POST['table'], quote($_POST['rule-function']));

    $res = pg_query($ID,$cmd);
    if (!pg_last_error($ID))
        echo common_message('ok','ok');
    else {
        $errorID = uniqid();
        echo common_message('fail',"SQL Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
    }
    exit;
}


/* Profile
 * Called: admin_function.js
 * Return: number of invites left
 * Description: Send invitation letter
 * */
if (isset($_POST['invitesend']) and isset($_SESSION['Tid'])) {
    require(getenv('OB_LIB_DIR').'languages.php');
    if (defined('INVITATIONS')) $inv = constant('INVITATIONS');
    else $inv = 10;
    session_write_close();

    //zero invitation project allow admins to send invitations
    if ($inv==0 and has_access('master'))
        $inv = 20;

    $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '7 days' > now()";
    $res = pg_query($BID,$cmd);
    $inv_sent = 0;
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        $inv_sent = $r['c'];
    }
    if ($inv_sent>=$inv) {
        echo common_message("error","You cannot send more invitations at this time");
        return false;
    }

    $name = '';
    $mail = '';

    $val = strip_tags($_POST['name']);
    if(strlen($val)>64) {
       echo common_message("error","The name cannot be longer than 64 characters");
       return false;
    }
    $ret_name = $val;
    $name = quote($val);

    if (! in_array($_POST['lang'], array_keys(LANGUAGES))) {
        echo common_message("error","Choose an other language for the message.");
        log_action('Invalid language selected!',__FILE__,__LINE__);
        return false;
    }
    $lang = $_POST['lang'];
   
    if (preg_match('/<([A-Za-z0-9-_.@]+)>/',$_POST['mail'],$m)) {
       $_POST['mail'] = $m[1];
    }
    $post_mail=preg_replace("/[^A-Za-z0-9-_+.@]/","",$_POST['mail']);
    if (strlen($post_mail)>64) {
        echo common_message("error","The email address cannot be longer than 64 characters");
        return false;
    }
    $ret_mail = strtolower($post_mail);
    $mail = quote(strtolower($post_mail));

    $comment = strip_tags($_POST['comment']);
    if ($comment != "")
       $comment .= "<br>.............................<br>";

    $code = md5($name.$mail);

    //$comment = nl2br($comment);
    $comment = str_replace( "\n", '<br />', $comment );

    pg_query($BID,"BEGIN;");

    $cmd = "INSERT INTO \"public\".\"invites\" 
            (user_id,code,created,name,mail,project_table,lang) 
        VALUES ('{$_SESSION['Tid']}','$code',now(),$name,$mail,'".PROJECTTABLE."',".quote($lang).")";

    $nur = pg_query($BID,$cmd);
    if ($nur) {
        $M = new Messenger();
        $from = new User($_SESSION['Tid']);
        $mt = new MessageTemplate('invitation');
        
        $words = compact('ret_name', 'comment', 'code');
        
        $retval = $M->send_only_email($from, $ret_mail, $mt->get('subject',$lang, $words), $mt->get('message',$lang, $words));
        if (!$retval) {
            echo common_message("error","Sending invitation to $post_mail failed.");
            pg_query($BID,"ROLLBACK;");
            return;
        }
        pg_query($BID,"COMMIT;");
        
        $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '7 days' > now()";
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $r = pg_fetch_assoc($res);
            echo common_message('ok',sprintf("%d",5-$r['c']));
            return;
        }
    } else {
        echo common_message("error","Failed to create the invitation");
        log_action("Sending invitation to $post_mail failed.",__FILE__,__LINE__);
    }
    exit;
}
/* Profile
 * Ajax request - called: main.js
 * Return:
 * Description: Drop selected invites
 * */
if (isset($_POST['delinv']) and isset($_SESSION['Tid'])) {
    session_write_close();
    foreach ($_POST['delinv'] as $d) {
        $pd = preg_replace('[^0-9]','',$d);
        if ($pd == '') continue;
        $cmd = "DELETE FROM invites WHERE id='$pd' AND user_id='{$_SESSION['Tid']}'";
        pg_query($BID,$cmd);
    }
    exit;
}
/* Save Table data for further actions
 * Ajax request - called: uploader.js
 * Only for logined users
 */
if (isset($_POST['state_posth']) and isset($_POST['state_postd']) and vreq()) {
    //
    // Ezt át kell alakítani úgy hogy minden fájl típusra működjön!!!!
    //
    //
    if (!isset($_SESSION['Tid'])) {
        echo common_message('error',"Not saved. It is available only for logined users.");
        exit;
    }
    if (!isset($_POST['form_id'])) {
        echo common_message('error',"Not saved.");
        exit;
    }

    // unused database columns
    $header = quote($_POST['state_posth']);

    // default fields
    $default = $_POST['state_postm'];

    // current data sheet
    $j = json_decode($_POST['state_postd']);
    /*
    "data": [
        [
            "0",
            "47.429111497",
            "18.413363798",
            "POINT(18.413363798 47.429111497)",
            "Fringilla",
            "c",
            "07-MAR-20 9:17:48",
            "07-MAR-20 9:17:48",
            "Flag, Blue"
        ], ...
    */ 
    $count = count($j->{'data'});

    if ($_POST['form_type']=='file') {

        if (!isset($_POST['sheetDataPage_counter']))
            $startpage = 0;
        else
            $startpage = $_POST['sheetDataPage_counter'];

        $n = 0;

        pg_query($ID,"BEGIN;");
        $row_data_column_count = array();

        if ($startpage == '')  {
            echo common_message("error","error");
            exit;
        }
        for ( $i=$startpage; $i<($startpage+$count);$i++ ) {
            //$row = $_SESSION['sheetData'][$i];
            $row = get_sheet_row($i+1);
            $row_data = json_decode($row['data'],true);
            // {"WKT":31.979730998243,"NAME":"625","HELYSEG":"Alsoujlak","ALLOMANY":"1","TERMOHELY":"kaszaloret","DATUM":"2015.04.15.","MEGJEGYZES":"elohelyfolt hatara","FENOLOGIA":"","FAJNEV":"Fritillaria meleagris","":""}
            
            if ($row_data) {
                $k = 1;
                // columns
                foreach ($row_data as $key=>$val) {
                    if (isset($j->{'data'}[$n])) {
                        if (isset($j->{'data'}[$n]->{$key})) {
                            $newdata = $j->{'data'}[$n]->{$key};
                            //$_SESSION['sheetData'][$i][$key] = $newdata;
                            $row_data[$key] = $newdata;
                        } else {
                            debug("Search for shifted columns: $key",__FILE__,__LINE__);
                            if (is_null($j->{'data'}[$n]->{$key})) {
                                // Látható 20 sor esetén amikor valamelyik oszlopba multiselect listát dobunk aminek nincs megfeleltett értéke!
                                // Az adott cella értéke nem fog változni, ami jól van így!!
                                debug('null',__FILE__,__LINE__);
                                //$row_data[$key] = '';
                            } else {
                                // mikor van olyan, hogy nem létezik $j->{'data'}[$n][$k]?
                                debug($j->{'data'}[$n],__FILE__,__LINE__);
                                debug($row_data,__FILE__,__LINE__);
                            }
                        }
                    }
                    $k++;
                }
                #if($_SESSION['Tid'] == 1 and $_POST['form_id']==58) {
                #       $order = array("lat","lon","wkt","ele","time","name","cmt","sym","type","extensions","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB");
                #       $ordered_array = array_replace(array_flip($order), $row_data);
                #       $row_data = $ordered_array;
                #}
                $row_data_column_count[] = count($row_data);
                update_sheet_row($i+1,$row_data,$j->{'data'}[$n]->{'skip_row'});
            }
            $n++; // max is the visible rows on the screen
        }
        if (count(array_unique($row_data_column_count))==1) {
            pg_query($ID,"COMMIT;");
            
        } else {
            pg_query($ID,"ROLLBACK;");
            $errorID = uniqid();
            echo common_message('fail',"Update rows error. ErrorID#$errorID#");
            log_action("Update row error: ErrorID#$errorID#",__FILE__,__LINE__);
            exit;
        }

        $data = quote(json_encode(array('title'=>$j->{'title'},'header'=>$j->{'header'},'default'=>$default)));
    } else {
        // web, api
        create_upload_temp();
        $data = array();
        for ($i=0;$i<count($j->{'data'});$i++) {
            $one = 0;
            foreach($j->{'data'}[$i] as $key=>$value) {
                $data[$i][$key] = $value;
                if ($one==0) {
                    $one++;
                }
            }
            add_sheet_row($i+1,$data[$i],$j->{'data'}[$i]->{'skip_row'});
        }

        $data = quote(json_encode(array('title'=>$j->{'title'},'header'=>$j->{'header'},'default'=>$default)));
    }

    $form_type=quote(preg_replace('/[^a-z]/','',$_POST['form_type']));
    $form_id=quote(preg_replace('/[^0-9]/','',$_POST['form_id']));

    /* header: 
     *  {
     *          slist:{}
     *          olist:{}
     *  }
     *
     * data:
     *  {
     *
     *  }
     * */
    $cmd = '';
    if (isset($_SESSION['upload']['loadref'])) {
        $ref = $_SESSION['upload']['loadref'];
        $cmd = sprintf("UPDATE system.imports SET datum=NOW(),header=%s,data=%s WHERE project_table='%s' AND ref=%s",$header,$data,PROJECTTABLE,quote($ref));
    } else if (isset($_SESSION['upload']['saveref'])) {
        if (isset($_GET['massiveedit']) and isset($_SESSION['upload']['massiveedit_ids']))
            $massedit_ids = json_encode($_SESSION['upload']['massiveedit_ids']);
        else
            $massedit_ids = NULL;

        $ref = $_SESSION['upload']['saveref'];
        //$ref = crc32(session_id().time());
        //$cmd = sprintf("SELECT ref FROM system.imports WHERE file='%s' AND form_id=%s AND form_type=%s AND user_id='%d'","upload_".PROJECTTABLE."_".session_id(),$form_id,$form_type,$_SESSION['Tid']);
        $cmd = sprintf("SELECT ref FROM system.imports WHERE ref='%s'",$ref);
        $res = pg_query($GID,$cmd);
        if (!pg_num_rows($res))
            $cmd = sprintf("INSERT INTO system.imports (project_table,user_id,ref,datum,header,data,form_type,form_id,file,massive_edit) VALUES ('%s','%d','%s',NOW(),%s,%s,%s,%s,'%s',%s)",PROJECTTABLE,$_SESSION['Tid'],$ref,$header,$data,$form_type,$form_id,"upload_".PROJECTTABLE."_".session_id(),quote($massedit_ids));
        else {
            $cmd = sprintf("UPDATE system.imports SET datum=NOW(),header=%s,data=%s WHERE project_table='%s' AND ref='%s'",$header,$data,PROJECTTABLE,$ref);
        }
    } else {
        if (isset($_GET['editrecord']))
            echo common_message('ok','');
        else {
            // expired session??
            echo common_message('error','Session error...');
            log_action('Upload autosave session expired or saveref is missing...?',__FILE__,__LINE__);
        }
    }
    if ($cmd!='') {
        $result = pg_query($GID,$cmd);
        $status = pg_result_status($result);
        if ($status!=1) {
            $errorID = uniqid();
            echo common_message('fail',str_sql_error.". ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            exit;
        }
    }

    if (isset($ref) and $ref != '') {
        $cmd = sprintf('DROP TABLE IF EXISTS temporary_tables.upload_%1$s_%2$s_%3$s',PROJECTTABLE,session_id(),preg_replace('/[;:".\'$^()\[\]]/','',$ref));
        $result = pg_query($ID,$cmd);
        $cmd = sprintf('CREATE TABLE temporary_tables.upload_%1$s_%2$s_%3$s AS SELECT * FROM temporary_tables.upload_%1$s_%2$s',PROJECTTABLE,session_id(),preg_replace('/[;:".\'$^()\[\]]/','',$ref));
        $result = pg_query($ID,$cmd);
        //$cmd = "UPDATE system.imports SET header=$header,data=$data WHERE ref='{$_SESSION['imports']}'";
        //$n = PGsqlcmd($ID,$cmd);

        //echo "Save the following link to load and continue this uploading:<br><a href='$p?load=$ref'>$p?load=$ref</a>";
        echo common_message('ok','ok');
    }
    exit;
}
/* upload.php
 * sheets paging */
if (isset($_POST['upload-file-pager']) and vreq()){

    $pager = pager($_POST['changepp'],$_POST['sheetDataPage'],$_POST['sheetDataPage_counter']);
    echo $pager;
    exit;
}
/*
 *
 * */
if (isset($_POST['upload-file-skip']) and vreq()){

    if (!isset($_POST['sheetDataPage_counter']))
        $pagestart = 0;
    else
        $pagestart = $_POST['sheetDataPage_counter'];

    $rows = get_sheet_skips($pagestart,$_POST['changepp']);
    $checked = array();
    foreach ( $rows as $r ) {
        if ($r['skip_marked']=='t') {
            $checked[] = 1;
        } else {
            $checked[] = 0;
        }
    }
    echo json_encode($checked);
    exit;
}
/* upload.php
 * reverse skip */
if (isset($_POST['reverse_table_skip']) and vreq()) {

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = NOT skip_marked",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    exit;
}
/* upload.php
 * deselect skip */
if (isset($_POST['deselect_table_skip']) and vreq()) {

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = FALSE",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    exit;
}
/* Filter distinct values of an upload table column */
if (isset($_POST['filterplus']) and vreq()) {

    $idx  = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['filterplus'])-2);

    $u_theader = json_decode($_POST['u_theader'],true);
    $key = $u_theader[$idx];

    # SELECT data -> 'szamossag' FROM upload_dinpi_kh2de8h7a7g6sv2iomvhuco5ua
    # ...
    $jdata = get_sheet_page(0,-1);
    $uval = array();
    foreach($jdata as $row) {
        foreach ( $row as $colname=>$value) {
            if ($colname == $key)
                $uval[] = $value;
        }
    }
    $a = array_values(array_unique($uval));
    sort($a);
    echo json_encode($a);

    exit;
}
/* Reverse Skip Mark filtered elements of upload table column */
if (isset($_POST['filterplus_skipmark']) and vreq()) {

    $idx = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['filterplus_idx'])-2);
    $u_theader = json_decode($_POST['u_theader'],true);
    $key = $u_theader[$idx];

    $cmd = sprintf("UPDATE temporary_tables.upload_%s_%s SET skip_marked = TRUE",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $filter = array();
    foreach($_POST['filterplus_skipmark'] as $val) {
        $filter[] = sprintf('data->>\'%1$s\' = %2$s',$key,quote($val));
    }
    $cmd = sprintf('UPDATE temporary_tables.upload_%1$s_%2$s SET skip_marked = FALSE WHERE %3$s',PROJECTTABLE,session_id(),implode(' OR ',$filter));
    $res = pg_query($ID,$cmd);

    $rows = get_sheet_skips(0,-1);
    echo json_encode($rows);

    exit;
}

/* apply obfun on visible cells
 * called in javascript loop
 * only for web forms
 * */
if (isset($_POST['upload_fill_onecell']) and vreq() ) {

    // update cell:                 function
    $data = array();
    foreach ($_POST['rowindex'] as $rowindex) {
        $data[] = upload_table_obfun($_POST['upload_fill_function'],$rowindex,$_POST['colindex'],$_POST['idmatch'],$_POST['autofill_data'],$_POST['u_theader']);
    }
    echo json_encode($data);
    exit;
}
/* upload.php
 * autofill
 * apply obfun on non-visible cells: sheetData */
if (isset($_POST['upload_fill_column-id']) and vreq()) {

    $u_theader = json_decode($_POST['u_theader'],true);
    // get column id
    $id  = sprintf('%d',preg_replace('/[^0-9]/','',$_POST['upload_fill_column-id'])-2);
    $key = $u_theader[$id];
    //$n = array_keys($_SESSION['sheetData']);
    $sheetData_count = get_upload_count();
    for ($i = 0;$i<$sheetData_count;$i++) {
        $pkey = $key;
        //sleep(1);

        if (get_row_skip($i+1) == 't') continue;
        $row = get_sheet_row($i+1);
        $sheetData = json_decode($row['data'],true);

        // tricky function apply: obfun:[1]replace:120->asd
        // read cell content from the 1st column WHERE its value is 120 apply it in the current column!!!
        // obfun:[FAJ]replace:Parus major->2
        if (preg_match('/^`apply:\[(.+)?\].+$/i',$_POST['upload_fill_function'],$m)) {
            if (count($m)==2) {
                $pkey = $m[1];
                if ($pkey == -1)
                    $pkey = $key;
            } else
                $pkey = $key;
            $_POST['upload_fill_column-value'] = $sheetData[$key];
        } elseif (preg_match('/^`apply:.+$/i',$_POST['upload_fill_function'],$m)) {
            $_POST['upload_fill_column-value'] = $sheetData[$key];
        }

        // update cell:                 function
        $data = upload_table_obfun($_POST['upload_fill_function'],$i,$_POST['upload_fill_column-id'],$_POST['idmatch'],$_POST['autofill_data'],$_POST['u_theader']);
    }
    exit;
}
/* upload.php
 * update current sheet
 * */
if (isset($_POST['upload_table_page']) and vreq()) {

    $u_theader = json_decode($_POST['u_theader'],true);
    $upload_column_assign = json_decode($_POST['a_theader'],true);

    $j = json_decode($_POST['upload_table_page']);
    $perpage = isset($_POST['changepp']) ? $_POST['changepp'] : 0;

    //nem a cél lap kell, hanem az aktuális!!!
    if (!isset($_POST['sheetDataPage_counter']))
        $startpage = 0;
    else
        $startpage = $_POST['sheetDataPage_counter'];

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $selected_form_type = preg_replace('[^a-z]','',$_POST['form_page_type']);
    $insert_allowed = $selected_form_id;

    // elvileg kötelező módon küldjük, hogy hányadik lapra érkezünk
    $page = 0;
    if (isset($_POST['page'])) {
        $page = preg_replace("/[^0-9]/",'',$_POST['page']);
        //$_SESSION['sheetDataPage_counter'] = $page;
    }

    //web form esetén létrehozom a sheetData-t
    if ($selected_form_type=='web') {
        create_upload_temp();
        $data = array();
        $skip = array();
        for ($i=0;$i<count($j->{'data'});$i++) {
            if ($j->{'data'}[$i]=='') $j->{'data'}[$i] = array();
            $one = 0;
            // a data oszlopokra
            foreach($j->{'data'}[$i] as $key=>$value) {
                // multiselect elements
                if (is_array($value))
                    $value = implode(',',$value);
                
                if ($key!='skip_row')
                    $data[$i][$key] = $value;

                if ($one==0) {
                    //$skip[] = array($j->{'data'}[$i][0]);
                    $one++;
                }
            }
            if (isset($data[$i])) {
                if (implode('',array_values($data[$i])) == '') {
                    // DEFAULT DATA!!!!!
                    $skip = 1;
                    if (isset($_POST['default_data']) && implode('',$_POST['default_data'])!='' and $i==0) {
                        $skip = 0;
                    }
                    if ($skip) {
                        $j->{'data'}[$i]->{'skip_row'} = true;
                        echo common_message("ok","force_skip:$i;");
                        exit;
                    }
                }
                add_sheet_row($i+1,$data[$i],$j->{'data'}[$i]->{'skip_row'});
            } else {
                $skip = 1;
                if (isset($_POST['default_data']) && implode('',$_POST['default_data'])!='' and $i==0) {
                    $skip = false;
                }

                add_sheet_row($i+1,array(),$skip);
                echo common_message("ok","force_skip:$i;");
                exit;
            }
        }
        echo common_message("ok","ok");
        exit;

    } elseif($selected_form_type=='file' ) {
        $n = 0;

        if ($startpage === '')  {
            echo common_message("error","error");
            exit;
        }
        for ( $i=$startpage; $i<($startpage+$perpage); $i++ ){
            //i. sor
            $row = get_sheet_row($i+1);
             /*{
                "row_id": "20",
                "skip_marked": "f",
                "data": "{\"lat\":\"47.728\",\"lon\":\"19.132\",\"wkt\":\"POINT(19.132 47.728)\",\"ele\":\"102.747\",\"time\":\"2018-07-16T08:51:00Z\",\"name\":\"1019\",\"sym\":\"City (Small)\",\"extensions\":\"\"}"
            }*/
            if (!$row) continue;

            $k = 1;
            // cellák beírása
            $row_data = json_decode($row['data'],true);
            foreach($row_data as $key=>$val) {
                if (isset($j->{'data'}[$n])) {

                    //if ( isset($_SESSION['theader'][$k-1]) and $key == $_SESSION['theader'][$k-1]) {
                        if (!isset($_POST['read_visible_cells']) or $_POST['read_visible_cells']) {
                            if (isset($j->{'data'}[$n]->{$key})) {
                                if (is_array($j->{'data'}[$n]->{$key}))
                                    // multiselect elements
                                    $row_data[$key] = implode(',',$j->{'data'}[$n]->{$key});
                                else
                                    $row_data[$key] = $j->{'data'}[$n]->{$key};
                            }
                        }
                    //}
                }
                $k++;
            }
            if (isset($j->{'data'}[$n])) {
                // extra oszlopok
                /*for ($m=$k-1;$m<count($j->{'data'}[$n])-1;$m++) {
                    debug('talán már sosem járunk itt...');
                    //$_SESSION['sheetData'][$i][$_SESSION['theader'][$m]] =  $j->{'data'}[$n][$m+1];
                    if (isset($j->{'data'}[$n][$m+1])) {
                        $row_data[$_SESSION['theader'][$m]] = $j->{'data'}[$n][$m+1];
                    }
                }*/
                /*
                 {
                    "obm_geometry": "obm_geometry",
                    "ele": "hely",
                    "I": "egyedszam",
                    "extensions": ""
                 }
                 {
                    "skip_row": "1",
                    "lat": "47.728640986606479",
                    "lon": "19.132310980930924",
                    "wkt": "POINT(19.132310980930924 47.728640986606479)",
                    "ele": "101.40097799999999",
                    "time": "2018-07-16T08:51:19Z",
                    "name": "1020",
                    "sym": "City (Small)",
                    "extensions": "",
                    "I": "",
                    "J": "",
                    "K": "",
                    "L": "",
                    "M": "",
                    "N": "",
                    "O": "",
                    "P": "",
                    "Q": ""
                }
                 */
                foreach ($upload_column_assign as $table_key=>$db_key) {
                    if (!isset($row_data[$table_key]))
                        $row_data[$table_key] = $j->{'data'}[$n]->{$table_key};
                }
                if (isset($j->{'data'}[$n]->{'skip_row'}))
                    update_sheet_row($i+1,$row_data,$j->{'data'}[$n]->{'skip_row'});
            }
            $n++;
        }
        // extra sorok hozzáadása a fájl végéhez
        // elegáns lenne, ha bárhol lehetne hozzáadni!
        if (isset($_POST['plusrow']) ) {
            for ($i=0; $i < $_POST['plusrow']; $i++) {
                $a = array();
                foreach($u_theader as $t) {
                    $a[$t] = '';
                }
                if (isset($j->{'data'}[$n]->{'skip_row'}))
                    add_sheet_row('',$a,$j->{'data'}[$n]->{'skip_row'});
            }
        }
        ob_start();
        $only_uft = 1;
        $show_geom_ref = '';
        //$theader = $u_theader;
        $table_type = "file";

        $cmd = sprintf("SELECT destination_table FROM project_forms WHERE form_id=%d and active=1",$selected_form_id);
        $res = pg_query($BID,$cmd);
        if ($row=pg_fetch_assoc($res)) {
            if ( $row['destination_table'] == '')
                $selected_form_table = PROJECTTABLE;
            else
                $selected_form_table = $row['destination_table'];
        }
        $sheetDataPage_counter = $page;

        include('upload_show-table-form.php');
        $table = ob_get_clean();
        if ($_POST['print'])
            echo $table;
        else
            echo common_message("ok","ok");

        exit;
    }

    exit;
}
/*
 *
 * */
if(isset($_POST['cancel_massiveedit']) and vreq()) {
    if (isset($_SESSION['upload']['massiveedit_ids']))
        unset($_SESSION['upload']['massiveedit_ids']);
    exit;
}
/* upload / insert data rows into database
 * Ajax request - called: uploader.js
 * should be compatible with API request
 */
if(isset($_POST['upload_table_post']) and vreq()) {

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $selected_form_type = preg_replace('[^a-z]','',$_POST['form_page_type']);
    $perpage = isset($_POST['changepp']) ? $_POST['changepp'] : 20;

    if (isset($_SESSION['openpage_key']))
        $this_page = md5($_SESSION['openpage_key'].$selected_form_id.$selected_form_type);

    if (!isset($this_page) or !in_array($this_page,$_SESSION['upload']['openpages'])) {
        http_response_code(400);
        echo common_message('error',"Invalid session! Another OBM project page opened?"); // Should be translated!
        exit;
    }

    include(getenv('OB_LIB_DIR').'upload_funcs.php');
    $up = new upload_process($_POST['massiveedit'],$_POST['editrecord']);
    $up->perpage = $perpage;
    list($destination_schema,$destination_table) = $up->access_check($selected_form_id,$selected_form_type);
    if (!$destination_table) {
        http_response_code(401);
        echo common_message('error',str_access_denied);
        exit;
    }

    $res = $up->new_upload($destination_schema,$destination_table);
    // check it is failed or success? No, just send message...
    echo $up->messages[0];

    exit;
}
/* Save dragndrop columns' order in file upload
 * called: uploader.js
 */
if(isset($_POST['template_post']) and isset($_POST['template_name']) and isset($_SESSION['Tid'])) {
    session_write_close();
    $table = PROJECTTABLE;

    if ($_POST['template_name'] == '') {
        echo common_message('error','No name??');
        exit;
    }

    $j = json_decode($_POST['template_post']);
    if ($_POST['sheet_name']=='')
        $cmd = sprintf("DELETE FROM settings_import WHERE user_id='{$_SESSION['Tid']}' AND project_table='".PROJECTTABLE."' AND name=%s AND sheet IS NULL",quote($_POST['template_name']));
    else
        $cmd = sprintf("DELETE FROM settings_import WHERE user_id='{$_SESSION['Tid']}' AND project_table='".PROJECTTABLE."' AND name=%s AND sheet=%s",quote($_POST['template_name']),quote($_POST['sheet_name']));
    $n = pg_query($BID,$cmd);

    $cmd = sprintf("INSERT INTO settings_import (user_id,project_table,name,sheet,assignments) VALUES (%d,'%s',%s,%s,%s) RETURNING name",$_SESSION['Tid'],$table,quote($_POST['template_name']),quote($_POST['sheet_name']),quote(json_encode($j->{'assignments'})));
    $res = pg_query($BID,$cmd);
    if(!pg_num_rows($res)) {
        $errorID = uniqid();
        echo common_message('fail',"Insert error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
    } else {
        $row = pg_fetch_assoc($res);
        echo common_message('ok',str_done.": ".$row['name'].".");
    }
    exit;
}
// nem használom sehol
if (isset($_POST['wkt_valid']) and vreq()) {
    session_write_close();
    if(is_wkt($_POST['wkt_valid'])) echo 1;
    else echo 0;
    exit;
}
/* upload_show-table-form - uploader.js
 * returnig geometry as WKT string
 * */
if(isset($_POST['getpolygon'])) {
    session_write_close();
    global $ID;
    $post_id=preg_replace("/[^0-9]/","",$_POST['getpolygon']);
    $text = "";

    if(isset($_SESSION['Tid'])) $tid = $_SESSION['Tid'];
    else $tid = 0;

    $cmd = sprintf('SELECT ST_AsText(geometry) as q
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id
            WHERE select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$post_id);

    $res = pg_query($ID,$cmd);
    if ($res) {
        $row = pg_fetch_assoc($res);
        $text .= $row['q'];
    }
    //module call
    /*if (isset($_SESSION['Tid']))
        $a = "0,1";
    else
        $a = "0";
    $cmd = sprintf("SELECT \"function\" as f,module_name,array_to_string(params,';') as p FROM modules WHERE \"enabled\"=TRUE AND \"file\"=afuncs.php' AND project_table='%s' AND module_access IN ($a)",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $modules = array();
    if(pg_num_rows($res)) {
        $modules = pg_fetch_all($res);
        if(file_exists(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php')) {
            if( php_check_syntax(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php','e')) {
                include_once(getenv('OB_LIB_DIR').'../projects/'.PROJECTTABLE.'/modules/afuncs.php');
            } else {
                $error[] = str_module_include_error.': '.$e;
            }
        }
    }
    $mki = array_search('getgeom', array_column($modules, 'module_name'));
    if ($mki!==false and function_exists($modules[$mki]['f'])) {
        //$cmd = call_user_func($modules[$mki]['f']);
        //...valami ilyesmi: $cmd = sprintf('SELECT ST_AsText(geometry) as q FROM milvus_alapegysegek,system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id WHERE select_view IN (2,3) AND p.user_id=\'%d\' AND id=\'%d\'',$tid,$post_id);

    } elseif ($mki!==false) {
        #require_once(getenv('OB_LIB_DIR').'default_modules.php');
        //default itegrated module
        #$dmodule = new defModules();
        #$dmodule->set('upload_show-table-form.php');
        #$modules_div .= $dmodule->photo_div();
    }*/
    echo $text;
    exit;
}

/* Save draw selection on main map
 * functions.js GetDataOfSelection()
 * auto called ajax function
 * can't be standalone vreq()
 * */
if (isset($_POST['auto_save_selection']) and vreq()) {
    if (AutoSaveGeometry($_POST['auto_save_selection']))
        echo common_message('ok');
    else
        echo common_message('error',"Error in auto save geometry");
    exit;
}
/* Main map page
 * Share queries: main.js
 * */
if (isset($_POST['sharequery'])) {
    if(!isset($_SESSION['Tid'])) {
        echo "Should be logined!";
        exit;
    }
    echo ShareQuery();
    exit;
}
/* Main map page
 * Bookmark queries: main.js
 * */
if (isset($_POST['bookmarkquery'])) {
    if(!isset($_SESSION['Tid'])) {
        echo "Should be logined!";
        exit;
    }
    $label = substr($_POST['bookmarklabel'], 0, 255);
    if ($label == '') {
        echo common_message('error','The label is mandatory!');
        exit;
    }
    echo BookmarkQuery($label);
    exit;
}

/* Interconnect
 * Share Query AJAX call
 * sending request to remote OpenBioMaps project
 * posting csv data to remote OpenBioMaps project
 * */
if (isset($_POST['shareQuery'])) {
    require(getenv('OB_LIB_DIR').'results_builder.php');
    $r = new results_builder(['method'=>'csv','filename'=>'shared query','sep'=>',','quote'=>'"']);
    if ($r->results_query()) {
        ob_start();
        $_SESSION['force_use'] = array('results_asCSV' => true);
        $r->printOut();
        unset($_SESSION['force_use']);
        $out = ob_get_clean();

        $ret = interconnect_master_connect($_POST['shareQuery']);

        if (isset($ret['failed'])) {
            echo common_message('error',$ret['failed']);
            exit;
        }

        $accept_key = '';
        $request_key = '';
        if (isset($ret['accept_key']))
            $accept_key = $ret['accept_key'];
        elseif (isset($ret['request_key']))
            $request_key = $ret['request_key'];

        // sending GET request
        // remote server add this request to interconnect_slave table
        if ($accept_key!='')
            $res = file_get_contents($_POST['shareQuery']."index.php?interconnect_server=".URL."&interconnect_request=$accept_key");
        elseif ($request_key!='')
            $res = file_get_contents($_POST['shareQuery']."index.php?interconnect_server=".URL."&interconnect_request=$request_key");
        else {
            echo common_message('error','key work failed');
            exit;
        }

        $j = json_decode($res,true);
        if ($j['status']=='success') {

            if ($ret) {
                // send csv file content to remote server
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $_POST['shareQuery']."pds.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => array('Accept: text/json'),
                    CURLOPT_POST => 1,
                    )
                );

                //Save string into temp file
                $file = tempnam(sys_get_temp_dir(), 'POST');
                file_put_contents($file, $out);

                //Post file
                $post = array();
                if ($accept_key!='')
                    $post = array(
                        #"interconnect_post_file"=>'@'.$file,
                        "accept_key"=>$accept_key,
                        "request_key"=>'',
                        "scope"=>"interconnect_post_file_with_key",
                        "slave_project"=>PROJECTTABLE,
                        "slave_server"=>URL,
                    );
                else
                    $post = array(
                        #"interconnect_post_file"=>'@'.$file,
                        "accept_key"=>'',
                        "request_key"=>$request_key,
                        "scope"=>"interconnect_post_file_with_key",
                        "slave_project"=>PROJECTTABLE,
                        "slave_server"=>URL,
                    );

                $post['interconnect_post_file'] = new CurlFile($file, 'text/csv');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

                $result = curl_exec($curl);

                //Remove the file
                unlink($file);

                echo common_message('ok',str_data_posted);
            }
        } else {
            echo common_message('error',$j['data']);

        }
    }

    exit;
}
/* Custom polygon management - 
 * It is used in box_load_selection module and core system as well
 * Set name, accessibility of polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['polygon_update'])) {
    session_write_close();

    $polygon_id = preg_replace('/[^0-9]/','',$_POST['polygon_update']);
    $f = array();
    if (isset($_POST['polygon_access'])) $f[] = "access='".preg_replace('/[^a-z]/','',$_POST['polygon_access'])."'";
    if (isset($_POST['polygon_name'])) $f[] = "name=".quote($_POST['polygon_name']);

    $update_polygon_ok_msg = '';
    $filters = implode(',',$f);
    if ($filters != '') {
        if (has_access('master')) {
            $cmd = "UPDATE system.shared_polygons SET $filters WHERE id=$polygon_id RETURNING name";
        } else {
            $cmd = "UPDATE system.shared_polygons SET $filters WHERE id=$polygon_id AND user_id={$_SESSION['Tid']} RETURNING name";
        }
        $res = pg_query($GID,$cmd);
        if (pg_affected_rows($res)) {
            $update_polygon_ok_msg = "Updated polygon: $polygon_id";
            $row = pg_fetch_assoc($res);
            $polygon_name = $row['name'];
            $M = new Messenger();
            if (isset($_POST['polygon_access']) and $_POST['polygon_access']=='public') {
                $M->send_system_news("new_shared_polygon", compact('polygon_name'));
            }
            elseif (isset($_POST['polygon_access']) and $_POST['polygon_access']=='project') {
                $M->send_project_news("new_shared_polygon", compact('polygon_name'));
            }
            echo common_message('ok',"$polygon_id updated");
            exit;
        }
        elseif (pg_last_error($GID)) {
            $errorID = uniqid();
            echo common_message('error',str_sql_error.". ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            exit;
        }
    }

    echo common_message('fail',"An error occured");
    exit;
}

/* Update own polygon attributes to show/hide in for selecting or uploading
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['showhide_polygon'])) {
    session_write_close();
    $id = preg_replace('/[^0-9]/','',$_POST['showhide_polygon']);
    $show = preg_replace('/[^a-z-]/','',$_POST['show']);
    $type = preg_replace('/[^a-z-]/','',$_POST['type']);


    $S = "select_view='none'";
    if ($show=='none' and $type=='upload') {$S = "select_view='only-upload'";$ret='only-upload';}
    elseif ($show=='none' and $type=='select') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='only-upload' and $type=='upload') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='only-upload' and $type=='select') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='upload') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='select') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='select-upload' and $type=='upload') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='select-upload' and $type=='select') {$S = "select_view='only-upload'";$ret='only-upload';}

    $cmd = "SELECT 1 FROM system.polygon_users WHERE user_id='{$_SESSION['Tid']}' AND polygon_id='$id'";
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res))
        $cmd = "UPDATE system.polygon_users SET $S WHERE user_id='{$_SESSION['Tid']}' AND polygon_id='$id'";
    else
        $cmd = "INSERT INTO system.polygon_users (polygon_id,user_id,select_view) VALUES ($id,{$_SESSION['Tid']},'$ret')";

    $res = pg_query($GID,$cmd);

    if (pg_affected_rows($res)) {
        echo common_message('ok',$ret);
    } else {
        $errorID = uniqid();
        echo common_message('fail',str_sql_error.". ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
    }

    exit;
}
/* Update force polygon attributes to show/hide in for selecting or uploading
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['force_showhide_polygon'])) {
    session_write_close();

    if (!has_access('master')) {
        echo common_message('error',str_access_denied);
        exit;
    }

    $id = preg_replace('/[^0-9]/','',$_POST['force_showhide_polygon']);
    $show = preg_replace('/[^a-z-]/','',$_POST['show']);
    $type = preg_replace('/[^a-z-]/','',$_POST['type']);
    $ret = $show;

    $S = "select_view='$show'";
    if ($show=='none' and $type=='upload') {$S = "select_view='only-upload'";$ret='only-upload';}
    elseif ($show=='none' and $type=='select') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='only-upload' and $type=='upload') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='only-upload' and $type=='select') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='upload') {$S = "select_view='select-upload'";$ret='select-upload';}
    elseif ($show=='only-select' and $type=='select') {$S = "select_view='none'";$ret='none';}
    elseif ($show=='select-upload' and $type=='upload') {$S = "select_view='only-select'";$ret='only-select';}
    elseif ($show=='select-upload' and $type=='select') {$S = "select_view='only-upload'";$ret='only-upload';}

    if (isset($_POST['force_users'])) {
        $ok = 1;
        $errorID = uniqid();

        if (!is_array($_POST['force_users'])) $_POST['force_users'] = array($_POST['force_users']);

        if (is_array($_POST['force_users'])) {
            foreach ($_POST['force_users'] as $u) {

                $cmd = sprintf('SELECT user_id FROM system.polygon_users WHERE user_id=\'%1$d\' AND polygon_id=\'%2$d\'',$u,$id);
                $res = pg_query($ID,$cmd);
                if (pg_num_rows($res)) {
                    if ($ret == 'none')
                        $cmd = sprintf('DELETE FROM system.polygon_users WHERE user_id=\'%1$s\' AND polygon_id=\'%2$s\'',$u,$id);
                    else
                        $cmd = sprintf('UPDATE system.polygon_users SET %1$s WHERE user_id=\'%2$s\' AND polygon_id=\'%3$s\'',$S,$u,$id);
                } else {
                    $cmd = sprintf('INSERT INTO system.polygon_users (polygon_id,user_id,select_view) VALUES (%1$d,%2$d,%3$s)',$id,$u,quote($show));
                }
                $res = pg_query($GID,$cmd);
                if (pg_last_error($GID)) {
                    $ok--;
                    log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
                }
            }
        }
        if ($ok)
            echo common_message('ok',$ret);
        else
            echo common_message('fail',"User settings not changed due to some errors. ErrorID#$errorID#");
        exit;
    }

    echo common_message('error','An error occured.');
    exit;
}
/* Drop force polygons
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['polygon_drop'])) {
    session_write_close();


    $id = preg_replace('/[^0-9]/','',$_POST['polygon_drop']);
    if (has_access('master')) {
        $cmd = "DELETE FROM system.shared_polygons WHERE id=$id";
    } else {
        $cmd = "DELETE FROM system.shared_polygons WHERE id=$id AND user_id='{$_SESSION['Tid']}'";
    }
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Nothing happened. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else
        echo common_message('ok',$id);
    exit;
}


/* Drop own queries
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['dropsq'])) {
    session_write_close();
    $d = $_POST['dropsq'];
    $d = array_map('quote',$d);
    $d = implode(',',$d);
    $cmd = sprintf("DELETE FROM bookmarks WHERE user_id='{$_SESSION['Tid']}' AND project='".PROJECTTABLE."' AND id IN (%s)",$d);
    $res = pg_query($BID,$cmd);
    if(!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        echo common_message('ok',"OK");

    }
    exit;
}
if (isset($_SESSION['Tid']) and isset($_POST['droprq'])) {
    session_write_close();
    $d = $_POST['droprq'];
    $d = array_map('quote',$d);
    $d = implode(',',$d);
    $cmd = sprintf("DELETE FROM project_repository WHERE user_id='{$_SESSION['Tid']}' AND datetime IN (%s)",$d);
    $res = pg_query($BID,$cmd);
    if(!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        echo common_message('ok',"OK");

    }
    exit;
}

/* upload reset forms
 * */
if(isset($_POST['closepage']) and isset($_SESSION['upload'])) {
    $key = array_search($_POST['closepage'],$_SESSION['upload']['openpages']);
    if (false !== $key)
    {
        unset($_SESSION['upload']['openpages'][$key]);
    }
    exit;
}
/* Drop selected imports
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_import'])) {
    session_write_close();
    $ref = $_POST['drop_import'];
    $ref = array_map('quote',$ref);
    $ref = implode(',',$ref);

    $cmd = sprintf("DELETE FROM system.imports WHERE ref IN (%s) AND user_id='%d' AND project_table='%s' RETURNING file,ref",$ref,$_SESSION['Tid'],PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        while ($row = pg_fetch_assoc($res)) {
            //drop saves
            $cmd = sprintf("DROP TABLE IF EXISTS temporary_tables.%s_s%s",$row['file'],$row['ref']);
            $res = pg_query($GID,$cmd);
        }
        echo common_message('ok',"OK");
    }
    exit;
}
/* Drop selected imports by admin
 * project_admin.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_1import'])) {
    session_write_close();
    $ref = $_POST['drop_1import'];

    $cmd = sprintf("DELETE FROM system.imports WHERE ref IN (%s) AND project_table='%s' RETURNING file,ref",quote($ref),PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    } else {
        while ($row = pg_fetch_assoc($res)) {
            //drop saves
            $cmd = sprintf("DROP TABLE IF EXISTS temporary_tables.%s_s%s",$row['file'],$row['ref']);
            $res = pg_query($GID,$cmd);
        }
        echo common_message('ok',"OK");
    }
    exit;
}
if (isset($_SESSION['Tid']) and isset($_POST['save_import_template'])) {
    session_write_close();
    $d = $_POST['save_import_template'];

    $name = $_POST['name'];

    $cmd = sprintf("UPDATE system.imports SET template_name=%s WHERE ref=%s AND user_id='%d' AND project_table='%s'",quote($name),quote($d),$_SESSION['Tid'],PROJECTTABLE);
    $res = pg_query($GID,$cmd);
    if (!pg_affected_rows($res)) {
        $errorID = uniqid();
        echo common_message('failed',"Error. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
    }
    exit;
}


/* Drop selected access_token key
 * profile.php, admin.js
 * */
if (isset($_SESSION['Tid']) and isset($_POST['drop_apikey']) and isset($_POST['tokenType'])) {
    session_write_close();

    if ($_POST['tokenType']=="access")
        $cmd = sprintf("DELETE FROM oauth_access_tokens WHERE access_token=%s AND user_id='%s'",quote($_POST['drop_apikey']),$_SESSION['Tmail']);
    elseif($_POST['tokenType']=="refresh")
        $cmd = sprintf("DELETE FROM oauth_refresh_tokens WHERE refresh_token=%s AND user_id='%s'",quote($_POST['drop_apikey']),$_SESSION['Tmail']);

    pg_query($BID,$cmd);
    exit;
}

/* Download attached files
 * */
if (isset($_GET['download_files']) and has_access('files')) {

    require_once("iapi.php");
    $table = isset($_GET['table']) ? $_GET['table'] : PROJECTTABLE;
    $uri = "/api/export_attachments/zip_stream/load.php?table=$table";
    $call = iapi::load($uri);
    $r = json_decode($call,true);
    if ($r['status'] == 'success')
        $results = $r['data'];
    else
        echo $r['message'];

    exit;
}

/* Linnaeus
 * */
if (isset($_REQUEST['linnaeus'])) {
    header("Content-type: application/json");
    try {
        require('remove_accents.function.php');
        $uri = "/api/linnaeus/ajax/load.php?";
        $call = iapi::load($uri . http_build_query($_REQUEST));
        echo common_message('ok', $call);
    } catch (\Exception $e) {
        echo common_message('error', $e->getMessage());
    }
    exit;
}

/* Metaname
 * */
if (isset($_REQUEST['metaname'])) {
    header("Content-type: application/json");
    try {
        $uri = "/api/metaname/repositories/load.php?";
        $call = iapi::load($uri . http_build_query($_REQUEST));
        echo common_message('ok', $call);
    } catch (\Exception $e) {
        echo common_message('error', $e->getMessage());
    }
    exit;
}

/* taxon name list load
 * authorisation not needed
 * */
if (isset($_POST['tpost'])) {
    session_write_close();
    global $ID;
    $where = array();
    $WHERE = '';
    $taxon = '';

    # trgm_string ["8396#Abramisbalerus"]
    if (isset($_REQUEST['trgm_string']) and $_REQUEST['trgm_string']!='') {
        $wo = array();
        $jo = json_decode($_REQUEST['trgm_string'],true);
        $lq = array();
        $id = '';
        $a_id = array();
        $a_names = array();
        foreach ($jo as $j) {
            $ilt = preg_split('/#/',$j);
            $id = $ilt[0];
            $taxon = $ilt[1];
            $a_id[] = $id;
            $a_name[] = $taxon;
        }
        if ($_REQUEST['allmatch'] and count($a_id)) {
            array_push($where,sprintf("taxon_id IN (%s)",implode(',',$a_id)));
        } else {
            array_push($where,sprintf("meta IN (%s)",implode(',',array_map('quote',$a_name))));
        }
    }
    if (count($where))
        $WHERE = "WHERE ".implode('AND ',$where);

    $species_array = array_unique(array_merge(array($_SESSION['st_col']['SPECIES_C']),$_SESSION['st_col']['ALTERN_C']));
    $species_array = array_filter($species_array);

    /*$species_array = array();
    $cmd = "SELECT DISTINCT lang lang FROM ".PROJECTTABLE."_taxon";
    $result = pg_query($ID,$cmd);
    while(pg_fetch_assoc($row = $result)) {
        $species_array[] = $row['lang'];
    }*/

    $join = array();
    foreach($species_array as $column) {
        $join[] = "t.word=$column";
    }
    $join_s = implode(" OR ",$join);

    $o = implode(",",$species_array);

    $table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    $cmd = "SELECT $o,t.taxon_id as id FROM $table LEFT JOIN ".PROJECTTABLE."_taxon t ON ($join_s) $WHERE GROUP BY $o,id ORDER BY $o";
    //log_action($cmd);
    $result = pg_query($ID,$cmd);

    $t = array();
    $allid = array();
    while ($row = pg_fetch_assoc($result)) {
        $allid[] = $row['id'];
        $n = "";
        //$diff_counter = array();
        $missing_taxon_id = 0;
        foreach (array_values($species_array) as $s) {
            if (mb_strtolower(mb_ereg_replace(' ','',$row[$s]),'UTF-8') == mb_strtolower($taxon,'UTF-8')) {
                $n .= sprintf("<td style='padding-right:20px'><a href='$protocol://".URL."/boat/?metaname&id={$row['id']}' style='font-variant:normal' target='_blank'>%s</a></td>",$row[$s]);
                $diff_counter[] = $row['id'];
            } elseif ($row[$s] != '') {
                $cmd = sprintf("SELECT taxon_id FROM ".PROJECTTABLE."_taxon t WHERE word=%s",quote($row[$s]));
                $res2 = pg_query($ID,$cmd);
                if (pg_num_rows($res2)) {
                    $row2 = pg_fetch_assoc($res2);
                    $n .= sprintf("<td style='padding-right:20px'><a href='$protocol://".URL."/boat/?metaname&id={$row2['taxon_id']}' target='_blank'>%s</a></td>",$row[$s]);
                    //$diff_counter[] = $row2['taxon_id'];
                    $allid[] = $row2['taxon_id'];
                } else {
                    $n .= "<td style='padding-right:20px'><a href='$protocol://".URL."/boat/?metaname&id={$row['id']}' style='font-variant:normal;color:red' target='_blank' title='This name is missing from the taxon table, please add manually: INSERT INTO ...'>{$row[$s]}</a></td>";
                    $missing_taxon_id = 1;
                }
            }
        }
        //if (count(array_unique($diff_counter))>1)
        if ($missing_taxon_id)
            $t[] = "<tr><td style='padding:2px 8px 0px 2px'><i class='fa fa-2x fa-warning' style='color:red'></i></td>$n</tr>";
        else
            $t[] = "<tr><td style='padding:2px 8px 0px 2px'><i class='fa fa-2x fa-check' style='color:green'></i></td>$n</tr>";
    }
    $out = "<table style='border-top:2px solid #dadada'>";
    $out .= "<tr><th style='border-bottom:1px solid lightgray'></th>";
    foreach($species_array as $sp) {
        $out .= "<th style='border-bottom:1px solid lightgray'>$sp</th>";
    }
    $out .= "<tr>";
    foreach ($t as $row) $out .= $row;
    $out .= "</table>";

    if (count(array_unique($allid))>1) {
        $out .= sprintf("<a href='$protocol://".URL."/boat/?metaname&id[]=%s' style='font-variant:normal' target='_blank' class='pure-button button-href'>%s</a>",implode('&id[]=',array_unique($allid)),str_all_names);
    }
    echo $out;
    exit;
}

if (isset($_POST['change_lang'])) {
    $_SESSION['update_lang'] = 1;

    $_SESSION['LANG'] = preg_replace('/[^a-z]/i','',substr($_POST['change_lang'],0,2));
    if (isset($_SESSION['Tcrypt'])) {
      set_preferred_lang($_SESSION['LANG'], $_SESSION['Trole_id']);
      setcookie("obm_lang_" . $_SESSION['Tcrypt'], $_SESSION['LANG'], time()+2592000,"/");
    }
    else {
      setcookie("obm_lang", $_SESSION['LANG'], time()+2592000,"/");
    }
    exit;
}

/* Upload data
 * upload_show-web-form
 * generated list from autocomplete
 * */
if(isset($_POST['genlist']) and isset($_POST['term'])) {

    $selected_form_id = preg_replace('/[^0-9]/','',$_POST['form_page_id']);
    $term = preg_replace('/;/','',$_POST['term']);
    $c = preg_replace('/;/','',$_POST['id']);
    $a_theader = json_decode($_POST['a_theader'],true);

    if (isset($_SESSION['upload']) and $selected_form_id) {
        $table = "";
        $column = "";

        if (preg_match('/default-/',$c)) {
            ## default column
            $c = preg_replace('/default-/','',$c);
            $a_theader[$c] = $c; 
        } elseif (preg_match('/f-/',$c)) {
            ## autofill row
            $c = preg_replace('/f-/','',$c);
        }

        $cmd = sprintf("SELECT \"column\",description,\"type\",\"control\",array_to_string(\"count\",':') as cnt,array_to_string(list,',') as list,obl,fullist,
                                    genlist,relation,pseudo_columns,custom_function,default_value,list_definition 
                        FROM project_forms_data 
                        WHERE form_id=%s AND \"column\"=%s", quote($selected_form_id), quote($a_theader[$c]));

        $res = pg_query($BID,$cmd);
        $x = array();
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);

            $sel = new upload_select_list($row, $term, false, false);
            $options = $sel->get_options('array');
            echo json_encode(array_column($options,'value'));
        }
        else {
            log_action('project_forms_data query returned with no results',__FILE__,__LINE__);
            echo json_encode(array());
        }
    }
    exit;
}

/* fetch openbiomaps servers */
if (isset($_POST['fetch_obm_servers'])) {

    $url = 'https://openbiomaps.org/projects/openbiomaps_network/index.php?query_api={"available":"up"}&output=json&filename=';
    $data = file_get_contents($url);
    echo $data;
    exit;
}
if (isset($_POST['fetch_obm_projects'])) {

    $url = $_POST['fetch_obm_projects'].'/pds.php?scope=get_project_list';
    $data = file_get_contents($url);
    $j = json_decode($data,true);
    echo json_encode($j['data']);
    exit;
}

/* results rolltable change order by click on viewport header */
if(isset($_POST['rolltable_order'])) {
    $_SESSION['orderby'] = $_POST['order'];
    exit;
}

/* results show switch button */
if(isset($_POST['set-morefilter'])) {

    if ($_POST['set-morefilter'] == 0) {
/*        "SELECT EXISTS (
           SELECT 1
           FROM   information_schema.tables
           WHERE  table_schema = 'temporary_tables'
           AND    table_name = 'temp_filter_%_%s')"
*/
        pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
        unset($_SESSION['morefilter_query']);
        unset($_SESSION['morefilter']);
        unset($_SESSION['filter_type']);

        //drop query tables as well
        //if ($_POST['clear-morefilter']==2) {
        //    pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
        //}
        exit;
    }

    $_SESSION['morefilter'] = 'on';


    $cmd = sprintf("CREATE UNLOGGED TABLE IF NOT EXISTS temporary_tables.temp_filter_%s_%s ( rowid INTEGER )",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    $cmd = sprintf("GRANT ALL ON temporary_tables.temp_filter_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
    pg_query($ID,$cmd);

    update_temp_table_index(PROJECTTABLE.'_'.session_id());

    $cmd = sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);

    /*$cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_query_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if(pg_affected_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            $cmd = sprintf('INSERT INTO temporary_tables.temp_filter_%1$s_%2$s SELECT obm_id FROM temporary_tables.temp_query_%1$s_%2$s',PROJECTTABLE,session_id());
        else {
            $cmd = sprintf("INSERT INTO temporary_tables.temp_filter_%s_%s VALUES (%d);",PROJECTTABLE,session_id(),$i);
        }
    }*/

    /*$cmd = sprintf('INSERT INTO temporary_tables.temp_filter_%1$s_%2$s SELECT obm_id FROM temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if(pg_affected_rows($res)) {
    //    echo pg_affected_rows($res);
        //the unlogged "temporary" table name
        $_SESSION['morefilter'] = sprintf('temp_filter_%s_%s',PROJECTTABLE,session_id());
        //he whole JOIN query
        $_SESSION['morefilter_query'] = sprintf("RIGHT JOIN temporary_tables.temp_filter_%s_%s on rowid=obm_id",PROJECTTABLE,session_id());
    }*/
    exit;
}

/* results show switch button */
if(isset($_POST['clear-morefilter'])) {

    $cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_filter_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_filter_%s_%s",PROJECTTABLE,session_id()));
    }

    // clear join
    unset($_SESSION['morefilter_query']);
    // clear filter type
    unset($_SESSION['filter_type']);
    // turn-off button
    //$_SESSION['morefilter'] = 'clear';

    exit;
}
/* reset-map call
 * drop query_build_string
 * It is important to reset base layer maps
 * */
if (isset($_POST['clear-session_querybuildstring'])) {
    unset($_SESSION['query_build_string']);
    $cmd = sprintf('SELECT EXISTS (
        SELECT 1
        FROM   pg_catalog.pg_class c
        JOIN   pg_catalog.pg_namespace n ON n.oid = c.relnamespace
        WHERE  n.nspname = \'temporary_tables\'
        AND    c.relname = \'temp_%s_%s\'
        AND    c.relkind = \'r\')',PROJECTTABLE,session_id());
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t')
            pg_query($ID,sprintf("DELETE FROM temporary_tables.temp_%s_%s",PROJECTTABLE,session_id()));
    }
    unset($_COOKIE['lastZoom']);
    setcookie('lastZoom', null, -1, sprintf('/projects/%s/',PROJECTTABLE));

    if (isset($_SESSION['selection_geometry'])) unset($_SESSION['selection_geometry']);
    if (isset($_SESSION['selection_geometry_type'])) unset($_SESSION['selection_geometry_type']);
    if (isset($_SESSION['selection_geometry_id'])) unset($_SESSION['selection_geometry_id']);

    exit;
}

/* track data view
 * */
if(isset($_POST['track_data_view'])) {
    global $BID;
    $data_id = preg_replace('/[^0-9]/','',$_POST['track_data_view']);
    if ($data_id!='')
        echo track_data($data_id);
    exit;
}
/*roller query subset of huge data
 * */
if (isset($_POST['Rprepare']) and isset($_POST['arrayType'])) {
    $cmd = '';

    if ($_POST['arrayType'] == 'list') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_list_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'table') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_table_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'stable') {
        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_roll_stable_%s_%s',PROJECTTABLE,session_id());
    } elseif ($_POST['arrayType'] == 'normaltable') {
        if (isset($_POST['Table']) and $_POST['Table']!='' and isset($_SESSION['private_key'])) {
            $cipher="AES-128-CBC";
            $iv = base64_decode($_SESSION['openssl_ivs']['asTable_table']);
            $table_reference = openssl_decrypt(base64_decode($_POST['Table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            if ($table_reference!='') {
                list($schema,$table) = preg_split('/\./',$table_reference);
            }
        }

        $cmd = sprintf('SELECT count(*) as c FROM %s.%s',$schema,$table);
    }
    if ($cmd != '') {
        $res = pg_query($ID,$cmd);
        if (pg_last_error($ID)) {
            log_action(pg_last_error($ID));
            echo "Is the module loaded?";
        } else {
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                echo $row['c'];
            } else {
                echo 0;
            }
        }
    }
    exit;
}
// rolltables
// roller.js
if (isset($_POST['RreadFrom']) and isset($_POST['arrayType'])) {
    global $ID;

    $from = preg_replace("/[^0-9]/","",$_POST['RreadFrom']);
    $to = 80;
    $b = array();

    if ($_POST['arrayType'] == 'list') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_list_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $b[] = array($row['rlist']);
        }
    } elseif ($_POST['arrayType'] == 'table') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_table_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $j = json_decode($row['rlist']);
            $foo = (array) $j;
            $bar = array();
            foreach($foo as $i) {
                $bar[] = $i;
            }
            $b[] = $bar;
        }
    } elseif ($_POST['arrayType'] == 'stable') {
        $cmd = sprintf('SELECT rlist FROM temporary_tables.temp_roll_stable_%s_%s ORDER BY ord LIMIT %d OFFSET %d',PROJECTTABLE,session_id(),$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_assoc($res)) {
            $j = json_decode($row['rlist']);
            $foo = (array) $j;
            $bar = array();
            foreach($foo as $i) {
                $bar[] = $i;
            }
            $b[] = $bar;
        }
    } elseif ($_POST['arrayType'] == 'normaltable') {
        if (isset($_POST['Table']) and $_POST['Table']!='' and isset($_SESSION['private_key'])) {
            $cipher="AES-128-CBC";
            $iv = base64_decode($_SESSION['openssl_ivs']['asTable_table']);
            $table_reference = openssl_decrypt(base64_decode($_POST['Table']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            if ($table_reference!='') {
                list($schema,$table) = preg_split('/\./',$table_reference);
            }
            if (isset($_POST['Order'])) {
                $iv = base64_decode($_SESSION['openssl_ivs']['asTable_orderby']);
                $order = 'ORDER BY '.openssl_decrypt(base64_decode($_POST['Order']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
            } else
                $order = '';
        }

        $cmd = sprintf('SELECT * FROM %s.%s %s LIMIT %d OFFSET %d',$schema,$table,$order,$to,$from);
        $res = pg_query($ID,$cmd);
        $b = array();
        while($row = pg_fetch_row($res)) {
            $b[] = $row;
        }
    }

    echo json_encode($b);

    exit;
}
//get mime - set mime icon/ thumbnail
//It can't be called standalone
if (isset($_POST['get_mime']) and vreq()) {
    if (isset($_POST['size']))
        $size = preg_replace('/[^0-9]/','',$_POST['size']);
    if (isset($_POST['file']))
        $file = $_POST['file'];
    if (isset($size) and isset($file)) {
        if ($thumb = mkThumb($file,$size)){
            if (isset($_POST['thumb']) and $_POST['thumb']=='no')
                $url = "$protocol://".URL."/getphoto?ref=$file";
            else
                $url = "$protocol://".URL."/getphoto?ref=/thumbnails/$file";
        } else {
            $url = mime_icon($file,$size);
        }
        echo $url;
    }
    exit;
}
/* uploader.js
 * file upload drop column label over a column
 * set column's params
 * */
/*if (isset($_POST['column_assign']) ) {

    //$_POST['colname'] can be used to teach auto column assign:
    //store column name (of uploaded data) and column_assign (database column) together and use them to fine tuning
    //auto column assign in upload_funcs-table-form.php

    if (isset($_SESSION['upload_column_assign']) ) {
        while(($key = array_search($_POST['column_assign'],$_SESSION['upload_column_assign'] )) !== false) {
            unset($_SESSION['upload_column_assign'][$key]);
        }
    }
    $_SESSION['upload_column_assign'][$_POST['coli']] = $_POST['column_assign'];


    exit;
}*/
/* uploader.js
 *
 * */
/*if (isset($_POST['column_remove']) and isset($_SESSION['upload_column_assign'])) {
    while(($key = array_search($_POST['column_remove'],$_SESSION['upload_column_assign'] )) !== false) {
        unset($_SESSION['upload_column_assign'][$key]);
    }
    //unset($_SESSION['upload_column_assign']);
    //$_SESSION['upload_column_assign'] = array_values($_SESSION['upload_column_assign']);
    exit;
}*/
/* upload table, change active sheet of uploaded excel files
 *
 * */
if (isset($_POST['change_exs_sheet'])) {
    $_SESSION['upload_file']['activeSheet'] = $_POST['change_exs_sheet'];
    exit;
}
/* dynamic drop-down select list in file upload default elements
 *
 * */
if (isset($_POST['dynamic_upload_list'])) {
    //default-observation_con_id
    $condition = $_POST['condition'];
    //_list string
    $cmd = sprintf("SELECT control,custom_function,list_definition FROM project_forms_data WHERE form_id=%d AND \"column\"=%s",$_POST['sfid'],quote($_POST['dynamic_upload_list']));
    $res = pg_query($BID,$cmd);
    if ($row = pg_fetch_assoc($res)) {
        $sl = process_trigger($row);
        if ($sl) {
            $out = array();
            foreach ($sl['target_column'] as $target_column) {
                $rc = $target_column;
                $cmd = sprintf("SELECT type, list, control, custom_function, default_value,list_definition FROM project_forms_data WHERE form_id=%d AND \"column\"=%s",$_POST['sfid'],quote($target_column));
                $res2 = pg_query($BID,$cmd);
                if ($row2 = pg_fetch_assoc($res2)) {

                    if ($row2['type'] == 'list') {
                        $sel = new upload_select_list($row2);
                        $opt = $sel->get_options('html');
                        $out[] = ['type'=>'list', 'col'=>$rc,'opt'=>implode($opt)];
                    }
                    elseif ($row2['type'] == 'autocomplete') {
                        $sel = new upload_select_list($row2);
                        $opt = $sel->get_options('array');
                        $out[] = ['type'=>'autocomplete', 'col'=>$rc, 'opt'=>json_encode($opt)];
                    }
                    else {
                        $trg = process_trigger($row2);
                        if ($trg) {
                            $cmd = '';

                            $prefilter = ($trg['preFilterColumn']) ? prepareFilter($trg['preFilterColumn'],$trg['preFilterValue']) : '1 = 1';
                            if ($row2['type'] == 'wkt' || $row2['type'] == 'point' || $row2['type'] == 'line' || $row2['type'] == 'polygon') {
                                $cmd = sprintf('SELECT ST_AsText(%s) as val FROM %s.%s WHERE %s = %s AND %s',$trg['value_col'],$trg['schema'],$trg['table'],$trg['foreign_key'],quote($condition),$prefilter);
                            }
                            elseif ($row2['type'] == 'text') {
                                $cmd = sprintf('SELECT %s as val FROM %s.%s WHERE %s = %s AND %s',$trg['value_col'],$trg['schema'],$trg['table'],$trg['foreign_key'],quote($condition), $prefilter);
                            }

                            if ($cmd != '') {
                                $res = pg_query($ID,$cmd);
                                $val = ($row = pg_fetch_assoc($res)) ? $row['val'] : '';
                                $out[] = ['type'=>'text', 'col' => $rc, 'opt' => $val];
                            }
                        }
                        else
                            log_action("get_value() not formed properly!",__FILE__,__LINE__);
                    }
                }
            }
            echo json_encode($out);
        }
    }

    exit;
}
/* kill R-shiny server */
if (isset($_POST['r_server'])) {
    $RSERVERS = constant('RSERVER_PORT');
    $rport = $RSERVERS[PROJECTTABLE];

    if ($_POST['r_server'] == 'stop') {
        // test, shiny server is running
        if (service_test('localhost',$rport)) {
            $pid = file_get_contents(getenv("PROJECT_DIR")."r-server.pid");
            exec("kill -9 $pid");
        }
    } elseif ($_POST['r_server']=='start') {
        if (!service_test('localhost',$rport)) {

            if (!file_exists(getenv('PROJECT_DIR')."shiny/r-server.log")) {
                if(!touch(file_exists(getenv('PROJECT_DIR')."shiny/r-server.log"))) {
                    echo common_message("error","Log file couldn't be created");
                    exit;
                }
            }
            if (!is_writable(getenv('PROJECT_DIR')."shiny/r-server.log")) {
                echo common_message("error","Log file is not writable");
                exit;
            }

            if (isset($RSERVERS[PROJECTTABLE])) {
                $pid = exec("Rscript --vanilla --no-save ".getenv('PROJECT_DIR')."shiny/run_server.R --port $rport --project ".PROJECTTABLE." --path ".getenv('PROJECT_DIR')."shiny/ > ".getenv('PROJECT_DIR')."shiny/r-server.log & echo $! &");
                if ($pid) {
                    log_action("R-server PID: $pid",__FILE__,__LINE__);
                    file_put_contents(getenv("PROJECT_DIR")."r-server.pid", $pid);
                }
            }
        }
    }

    echo 1;
    exit;
}
/* Update project description
 *
 * */
if (isset($_POST['update_project_description_lang'])) {

    $cmd = sprintf('INSERT INTO project_descriptions (short,long,projecttable,language) VALUES 
        (%1$s,%2$s,\'%3$s\',%4$s) 
            ON CONFLICT (projecttable,language)
            DO UPDATE SET short=%1$s, long=%2$s WHERE project_descriptions.projecttable=\'%3$s\' AND project_descriptions.language=%4$s',
            quote($_POST['update_project_short_description']),
            quote($_POST['update_project_long_description']),
            PROJECTTABLE,
            quote($_POST['update_project_description_lang']));
    $res = pg_query($BID,$cmd);
    if (!pg_last_error($BID))
        echo common_message('ok','ok');
    else
        echo common_message('error',pg_last_error($BID));
}

/* Create new database table
 * */
if (isset($_POST['create_new_table'])) {

    if (!has_access('create_table')) exit;

    if (preg_match("/\./",$_POST['create_new_table']))
        list($schema,$table) = preg_split("/\./",$_POST['create_new_table']);
    else {
        $schema = 'public';
        $table = $_POST['create_new_table'];
    }

    if (!preg_match('/^[a-z0-9_]+$/',$table)) {
        echo common_message('error','Only a-z character 0-9 numbers and _ allowed!');
        exit;
    }
    if (strlen($_POST['create_new_table'])>24) {
        echo common_message('error','Max 24 characters allowed!');
        exit;
    }
    if ($_POST['create_new_table']=='') {
        echo common_message('error','Give a name!');
        exit;
    }
    // removing project pre
    $table = preg_replace("/^".PROJECTTABLE."_/","",$table);

    $cmd = sprintf('SELECT 1 FROM header_names WHERE f_project_name=\'%1$s\' AND f_project_table=%2$s AND f_project_schema=%3$s',
        PROJECTTABLE,
        quote(PROJECTTABLE."_".strtolower($table)),
        quote($schema));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        echo common_message('error',"$table table already exists!");
        exit;
    }

    //pg_query($BID,'BEGIN');
    $cmd = sprintf('BEGIN;INSERT INTO header_names (f_project_schema,f_project_name,f_date_columns,f_cite_person_columns,f_alter_speciesname_columns,f_file_id_columns,f_project_table,f_srid,f_id_column)
        VALUES(%s,\'%s\',NULL,NULL,NULL,NULL,%s,4326,\'obm_id\')',
        quote($schema),
        PROJECTTABLE,
        quote(PROJECTTABLE.'_'.$table));
    $res = pg_query($BID,$cmd);
    if (!pg_affected_rows($res)) {
        pg_query($BID,'ROLLBACK');
        $errorID = uniqid();
        echo common_message('fail',"$table adding to project failed. ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
        exit;
    }

    if ($schema == 'public') {
        $tgrelid = PROJECTTABLE.'_'.$table;
    } else {
        $tgrelid = $schema.'.'.PROJECTTABLE.'_'.$table;
    }

    $cmd = "SELECT EXISTS (
        SELECT FROM information_schema.tables 
        WHERE  table_schema = '$schema'
            AND    table_name   = '".PROJECTTABLE."_$table')";
    $res = pg_query($GID,$cmd);
    $row = pg_fetch_assoc($res);
    if ($row['exists']=='f') {
    
        //$cmd = sprintf('SELECT tgname from pg_trigger WHERE NOT tgisinternal AND tgrelid = \'%4$s\'::regclass AND tgname=\'file_connection_%1$s_%2$s_%3$s\'',$schema,PROJECTTABLE,$table,$tgrelid);
        //$res = pg_query($GID,$cmd);
        //if (!pg_num_rows($res))
            $trigger_row = sprintf('CREATE TRIGGER file_connection_%3$s_%1$s_%2$s AFTER INSERT ON "%3$s"."%1$s_%2$s" FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();',PROJECTTABLE,$table,$schema);
        //else
        //    $trigger_row = '';

        $cmd = sprintf('CREATE TABLE "%5$s"."%1$s_%2$s" (
            obm_id integer NOT NULL,
            obm_geometry geometry,
            obm_uploading_id integer,
            obm_validation numeric,
            obm_comments text[],
            obm_modifier_id integer,
            obm_files_id character varying( 32),
            CONSTRAINT enforce_dims_obm_geometry CHECK ((st_ndims(obm_geometry) = 2)),
            CONSTRAINT enforce_srid_obm_geometry CHECK ((st_srid(obm_geometry) = 4326))
        );

        COMMENT ON TABLE "%5$s".%1$s_%2$s IS %3$s;

        CREATE SEQUENCE "%5$s".%1$s_%2$s_obm_id_seq
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1;

        ALTER TABLE "%5$s".%1$s_%2$s_obm_id_seq OWNER TO %1$s_admin;

        ALTER TABLE ONLY "%5$s".%1$s_%2$s ADD CONSTRAINT %1$s_%2$s_pkey PRIMARY KEY (obm_id);

        ALTER TABLE ONLY "%5$s".%1$s_%2$s ALTER COLUMN obm_id SET DEFAULT nextval(\'%1$s_%2$s_obm_id_seq\'::regclass);

        ALTER TABLE ONLY "%5$s".%1$s_%2$s ADD CONSTRAINT uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES uploadings(id);

        ALTER TABLE "%5$s".%1$s_%2$s OWNER TO %1$s_admin;%4$s',
            PROJECTTABLE,
            $table,
            quote($_POST['new_table_comment']),
            $trigger_row,
            $schema);

        if (!pg_query($GID,$cmd)) {
            pg_query($BID,'ROLLBACK');
            $errorID = uniqid();
            echo common_message('fail',"Failed to create ".PROJECTTABLE."_$table. ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);

        } else {
            // triggering create sql query for the new table
            pg_query($BID,'COMMIT');
            echo common_message('ok',PROJECTTABLE."_$table created");
        }
    } else {
        pg_query($BID,'ROLLBACK');
        $errorID = uniqid();
        echo common_message('fail',"Failed to create ".PROJECTTABLE."_$table. Table already exists.");
    }

    exit;
}
/* agree new terms
 *
 * */
if (isset($_POST['agree_new_terms'])) {
    global $BID;
    if (!isset($_SESSION['Tid']) or !isset($_SESSION['Tcrypt']))
        exit;

    $cmd = sprintf("UPDATE users SET terms_agree=1 WHERE \"user\"='%s' AND id=%d",$_SESSION['Tcrypt'],$_SESSION['Tid']);
    $res = pg_query($BID,$cmd);
    if (pg_affected_rows($res))
        $_SESSION['Tterms_agree'] = 1;
    exit;
}
/*
 *
 * */
if (isset($_POST['get-terms-text'])) {
    ob_start();
    $fileName = '';
    switch ($_POST['get-terms-text']) {
    case 'privacy':
        $fileName = 'at';
        break;

    case 'terms':
        $fileName = 'aszf';
        break;
    case 'cookies':
        $fileName = 'cookies';
        break;
    }

    $lang = preg_replace("/[^A-Za-z]/", "", $_POST['lang']);
    if (file_exists(getenv('PROJECT_DIR')."{$fileName}_{$lang}.html"))
        include(getenv('PROJECT_DIR')."{$fileName}_{$lang}.html");
    elseif (file_exists(getenv('PROJECT_DIR')."{$fileName}.html"))
        include(getenv('PROJECT_DIR')."{$fileName}.html");
    elseif (file_exists(OB_ROOT."{$fileName}_{$lang}.html"))
        include(OB_ROOT."{$fileName}_{$lang}.html");
    else
        include(OB_ROOT."{$fileName}.html");

    $out = ob_get_clean();
    echo $out;
}
/*
 *
 * */
if (isset($_POST['process']) && $_POST['process'] = 'getNestedOptions') {

    $cmd = sprintf("SELECT %s as val, %s as name FROM %s WHERE %s = %s ORDER by name;", $_POST['v'], $_POST['n'], $_POST['table'], $_POST['fk'], $_POST['fkv']);
    $res = pg_query($ID,$cmd);
    $results = [];
    while ($row = pg_fetch_assoc($res)) {
        $results[] = $row;
    }
    header("Content-type: application/json");
    print(json_encode($results));
}
/*
 *
 * */
//has_access form-editor??
if (isset($_POST['list-validator']) and isset($_SESSION['Tid'])) {

    $return = false;
    if (preg_match('/^{/',$_POST['list-validator'])) {

        // JSON format given
        echo json_schema_validator($_POST['list-validator'],'upload_list.schema.json');
    } else {

        // simplified format given
        /* create JSON from new line separated list elements
         *
         * */
        $x = preg_split("/\n/",$_POST['list-validator']);
        $a = array();
        foreach ($x as $elem) {
                $a[$elem] = array($elem);
        }
        echo json_schema_validator(json_encode(array("list"=>$a)),'upload_list.schema.json');
    }
    exit;
}

if (isset($_POST['list-content-parser']) and isset($_SESSION['Tid'])) {
    $value = isset($_POST['value']) ? $_POST['value'] : '';
    $schema = isset($_POST['schema']) ? $_POST['schema'] : 'public';

    if ( $_POST['list-content-parser'] == 'optionsTable' )
        $response = getDataTables($value,$schema);
    elseif ( $_POST['list-content-parser'] == 'optionsSchema' )
        $response = getProjectSchemas();
    elseif ( in_array($_POST['list-content-parser'],array('valueColumn','labelColumn','filterColumn','preFilterColumn')) )
        $response = getColumnsOfTables($value,$schema);
    elseif ( $_POST['list-content-parser'] == 'triggerTargetColumn' )
        $response = getColumnsOfTables(PROJECTTABLE,$schema);
    elseif ( in_array($_POST['list-content-parser'],array('add_empty_line','multiselect')) )
        $response = array('true','false');
    elseif ( $_POST['list-content-parser'] == 'Function' )
        $response = array('select_list','get_value');
    elseif ( in_array($_POST['list-content-parser'],array('disabled','selected')) ) {
        if (is_array($value)) {
            $response = array_keys($value);
        } else
            $response = array($value);
    }
    elseif ( $_POST['list-content-parser'] == 'pictures' ) {
        if (is_array($value)) {
            $keys = array_keys($value);
            $response = array_map(function($x){return json_encode(array($x=>'url-string'));},$keys);
        } else
            $response = array($value);

    }

    if (!is_array($response))
        $response = array("string");
    elseif (!count($response))
        $response = array("string");

    echo json_encode($response);
    exit;
}
/* Update local translations
 *
 * */
if (isset($_POST['update_translations']) and isset($_SESSION['Tid'])) {
    //$cmd = sprintf("SELECT id, const, translation FROM translations WHERE scope = 'local' AND project = '%s' AND lang = '%s' ORDER BY const;", PROJECTTABLE, $_SESSION['LANG']);
    $cmd = sprintf('SELECT t2.id,t2.lang,t1.const,t1.translation AS translation_en,t2.translation
                    FROM translations t1  LEFT JOIN translations t2 ON (t1.const=t2.const AND t2.lang=%1$s)
                    WHERE t1.scope = \'local\' AND t1.lang IN (\'en\') AND t1.project=\'%2$s\' ORDER BY t1.const',quote($_SESSION['LANG']),PROJECTTABLE);

    $LANGS = explode(',',LANGUAGES);
    if (pg_send_query($BID, $cmd)) {
        $res = pg_get_result($BID);
        $state = pg_result_error($res);
        if ($state != '') {
            $errorID = uniqid();
            echo common_message('fail',$state." ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".$state,__FILE__,__LINE__);
            return;
        }

        $translations = [];
        while ($row = pg_fetch_assoc($res)) {
            $translations[] = $row;
        }

        $cmd = '';
        $cmdCount = 0;
        $delete = -1;
        foreach ($translations as $t) {
            if ($t["lang"]!='') {
                if (isset($_POST['const-'.$t['id']]) and $_POST['const-'.$t['id']] !== $t['const']) {
                    if ($_POST['const-'.$t['id']] == '') {
                        $delete = $t['id'];
                        $cmd .= sprintf("DELETE FROM translations WHERE id = %d;", $t['id']);
                        $cmdCount++;
                    }
                    else {
                       $cmd .= sprintf("UPDATE translations SET const = %s WHERE id = %s;", quote($_POST['const-'.$t['id']]), $t['id']);
                       $cmdCount++;
                    }
                }

                if (isset($_POST['translation-'.$t['id']]) and $_POST['translation-'.$t['id']] !== $t['translation'] and $delete !== $t['id']) {
                    $cmd .= sprintf("UPDATE translations SET translation = %s WHERE id = %d;", quote($_POST['translation-'.$t['id']]), $t['id']);
                    $cmdCount++;
                }
            }
            elseif ($t["lang"]=='') {
                if (isset($_POST['const-'.$t['id']])) {
                    $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($_POST['const-'.$t['id']]),quote($_POST['translation-'.$t['id']]));
                    $cmdCount++;
                } else {
                    $val = '';
                    $idx = array_search($t['const'],$_POST);
                    //const-12new
                    if ($idx!==false) {
                        if (preg_match('/^const(-\d+new)/',$idx,$m)) {
                            $tra = "translation".$m[1];
                            $val = $_POST[$tra];
                            $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($_POST[$idx]),quote($val));
                            $cmdCount++;
                        }
                    }
                }
            }
        }

        foreach ($_POST as $key => $value) {
            if (preg_match('/new-const-(\d+)/',$key,$m) && $value != '') {
                if (preg_match('/str_(\w+)/',$value)) {
                    $i = $m[1];
                    if (isset($_POST['new-translation-'.$i]) && $_POST['new-translation-'.$i] !== '') {
                        $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,%s);", PROJECTTABLE,$_SESSION['LANG'],quote($value),quote($_POST['new-translation-'.$i]));
                        $cmdCount++;
                        foreach ($LANGS as $lang) {
                            if ($lang != $_SESSION['LANG'])
                                $cmd .= sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local','%s','%s',%s,'%s');", PROJECTTABLE,$lang,quote($value),'__not_translated__');
                                $cmdCount++;
                        }
                    }
                    else {
                        echo common_message('error','translation empty');
                        return;
                    }
                }
                else {
                    echo common_message('error','constant not formed properly!');
                    return;
                }

            }
        }

        if (pg_send_query($BID, $cmd)) {
            for ($i = 0; $i < $cmdCount; $i++) {
                $res = pg_get_result($BID);
                $state = pg_result_error($res);
                if ($state != '') {
                    $errorID = uniqid();
                    echo common_message('failed',$state." ErrorID#$errorID#");
                    log_action("ErrorID#$errorID# ".$state,__FILE__,__LINE__);
                    log_action("ErrorID#$errorID# ".$cmd,__FILE__,__LINE__);
                    return;
                }
            }
            echo common_message('ok','done');
        }
    }
    exit;
}
/*
 *
 * */
if (isset($_POST['choose_display_grid_for_map'])) {
    global $ID;

    $cmd = sprintf("SELECT c.column_name, pgd.description FROM pg_catalog.pg_statio_all_tables as st inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
            INNER JOIN information_schema.columns c on
                (pgd.objsubid=c.ordinal_position and c.table_schema=st.schemaname and c.table_name=st.relname and c.table_name = '%s_qgrids' and c.table_schema = 'public')",$_SESSION['current_query_table']);

    $res = pg_query($ID,$cmd);
    while ($row = pg_fetch_assoc($res)) {
        if ($_POST['choose_display_grid_for_map'] == $row['column_name']) {
            $_SESSION['display_grid_for_map'] = $row['column_name'];
            echo common_message('ok','ok');
            exit;
        }
    }
    echo common_message('error','invalid grid');
    exit;
}
/* Called in maps_functions.js
 *
 * Not used???
 * */
if (isset($_POST['check_layer'])) {
    global $BID;

    $layers_state = array();

    if ($modules->is_enabled('grid_view')) {

        $grid_layer = $modules->_include('grid_view','get_grid_layer');
        // get layer name based on js layer name

        for ($i=0; $i<count($_POST['check_layer']);$i++) {

            $cl = $_POST['check_layer'][$i];
            $layers_state[$i] = 'ok';

            $layer = '';
            $cmd = sprintf("SELECT ms_layer FROM project_layers WHERE concat_ws('_',layer_name,layer_order)=%s",quote($cl));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $layer = $row['ms_layer'];
            }

            if (isset($_SESSION['display_grid_for_map'])) {

                // For grid coordinates disable original layer
                foreach ($grid_layer as $gc=>$gl) {
                    if ($_SESSION['display_grid_for_map'] == $gc and $layer!='' and $layer!=$gl) {
                        $layers_state[$i] = 'drop';
                    }
                }
            } else {
                $grid_geom_col = $modules->_include('grid_view','default_grid_geom');
                foreach ($grid_layer as $gc=>$gl) {
                    if ($grid_geom_col == $gc and $layer!='' and $layer!=$gl) {
                        $layers_state[$i] = 'drop';
                    }
                }
            }
        }
    }
    echo common_message('ok',json_encode($layers_state));
    exit;
}

// creates a new gitlab issue
if (isset($_SESSION['Tid']) and isset($_POST['new_gitlab_issue'])) {

    //not a solution yet
    //curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.example.com/api/v4/projects/4/issues?title=Issues%20with%20auth&labels=bug

    if (defined("AUTO_BUGREPORT_ADDRESS")) {

        /*$cipher="AES-128-CBC";
        if (isset($_POST['eincf']) and $_POST['eincf']!='' and isset($_SESSION['private_key'])) {
            $iv = base64_decode($_SESSION['openssl_ivs']['eincf']);
            $included_files = openssl_decrypt(base64_decode($_POST['eincf']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        }
        if (isset($_POST['edefv']) and $_POST['edefv']!='' and isset($_SESSION['private_key'])) {
            $iv = base64_decode($_SESSION['openssl_ivs']['edefv']);
            $defined_vars = openssl_decrypt(base64_decode($_POST['edefv']), $cipher, $_SESSION['private_key'], $options=OPENSSL_RAW_DATA, $iv);
        }*/

        //$message .= "<br>Included files:<br>".$included_files."<br>defined variables:<br>".$defined_vars;

       $M = new Messenger();
       $mt = new MessageTemplate('new_gitlab_issue');
       $from = new User($_SESSION['Tid']);
       
       if ($M->send_only_email($from, AUTO_BUGREPORT_ADDRESS, $mt->get('subject',LANG, $_POST), $mt->get('message', LANG, $_POST))) {
           echo common_message('ok','ok');
       }
       else {
           echo common_message('error','bugreport sending failed');
       }
        
    } else {
        echo common_message('error','auto bugreport address not defined');
    }
    exit;
}
// switch profile
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['switchprofile'])) {
    $_SESSION['switchprofile'] = $_POST['switchprofile'];
    echo login_box(TRUE);
    exit;
}

// send sql command from sql console
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['send-sql-cmd'])) {

/*    if (!isset($_SESSION['reauth-for-sqlconsole'])) {
        echo common_message("error",login_box(TRUE));
        exit;
    } else {
        if ($_SESSION['reauth-for-sqlconsole'] < time()-1800) {
            echo common_message("error",login_box(TRUE));
            exit;
        }
    }
*/

    $cmd = preg_replace('/\s+/u',' ',$_POST['send-sql-cmd']);
    $result = pg_query($ID,$cmd);
    $status = pg_result_status($result);
    if ($status!=1) {

        if (!pg_last_error($ID)) {
            if (pg_num_rows($result) > 100) {
                $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
                $iv = openssl_random_pseudo_bytes($ivlen);

                $hash = genhash();
                obm_cache('set',"download-query-results-iv-".$hash,$iv,300,FALSE);
                
                $enc = base64_encode(openssl_encrypt($cmd, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA, $iv));
                echo common_message("error","<div style='background-color:#ccc'>Use `LIMIT 100 OFFSET n` to paginate results or </div><a href='$protocol://".URL."/includes/ajax?download-query-results=$enc&type=csv&id=$hash' target='_blank' class='pure-button button-href button-xlarge' style='height:230px;padding-top:100px'>download results<br><i class='fa fa-download fa-2x'></i></a>");
            } else
                echo common_message("ok",pg_fetch_all($result));
        } else {
            echo common_message("fail",pg_last_error($ID));
        }
        exit;
    } else {
        echo common_message("ok",pg_affected_rows($result));
    }
    exit;
}

// authentication testing
if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['reauth'])) {
    if (ObmAuth::login($_POST['reauth'][0]['value'],$_POST['reauth'][1]['value'], false)) {
        $_SESSION['reauth-for-sqlconsole'] = time();
    }

    if (isset($_SESSION['switchprofile'])) {
        unset($_SESSION['switchprofile']);
        echo 'reload_profile';
    }
    exit;
}
// auth token response for new mobile client
if (isset($_GET['auth_key'])) {
    $response = "";
    if ($_GET['auth_key'] != '') {
        $auth_key = $_GET['auth_key'];
        $response = ObmAuth::get(
            $auth_key
        ); 
    }
    echo common_message("ok",$response);
    exit;
}

// syntax highlight an sql command
if (isset($_POST['sql-cmd-highlight'])) {
    require('vendor/autoload.php');

    $h = SqlFormatter::format($_POST['sql-cmd-highlight']);
    $f = SqlFormatter::format($_POST['sql-cmd-highlight'],false);
    echo json_encode(array($h,$f));
    exit;
}

if (isset($_SESSION['Tid']) and has_access('master') and isset($_POST['privileges']) && $_POST['privileges'] === 'save') {
    global $BID;

    $cmd1 = sprintf("DELETE FROM group_privileges WHERE project = '%s';", PROJECTTABLE);
    if (!$res = pg_query($BID,$cmd1)) {
        $errorID = uniqid();
        echo common_message('failed',str_sql_error.". ErrorID#$errorID#");
        log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
        exit;
    }
    $sql = [];
    foreach ($_POST as $name => $value) {
        if (preg_match('/^(\w+)@(\w+)$/',$name,$pairs))
            $sql[] = sprintf("('%s',%s,%s)",PROJECTTABLE,quote($pairs[1]),quote($pairs[2]));
    }
    if (count($sql)) {
        $cmd2 = sprintf('INSERT INTO group_privileges (project, role_id, action) VALUES %s;', implode(',',$sql));
        if (!$res = pg_query($BID,$cmd2)) {
            $errorID = uniqid();
            echo common_message('failed',str_sql_error.". ErrorID#$errorID#");
            log_action("ErrorID#$errorID# ".pg_last_error($BID),__FILE__,__LINE__);
            exit;
        }

    }
    echo common_message('ok','OK!');
    exit;
}

// data-sheet page
if (isset($_POST['edit-record']) and isset($_POST['table'])) {
    $nullform_id = getNullFormId($_POST['table'],true);

    echo $nullform_id;
    exit;
}

if (isset($_POST['action']) && $_POST['action'] === 'get_messages') {
    $M = new Messenger();
    
    if (in_array($_POST['message_type'], $M->non_news_types)) {
        echo common_message('ok', json_encode($M->get_my_messages($_POST['message_type'])));
        exit;
    }
    elseif ($_POST['message_type'] === 'sent') {
        echo common_message('ok', json_encode($M->get_my_sent_messages()));
        exit;
    }
    elseif ($_POST['message_type'] === 'news') {
        echo common_message('ok', json_encode($M->get_news()));
        exit;
    }
    
    $errorID = uniqid();
    echo common_message('failed',"Invalid message type. ErrorID#$errorID#");
    log_action("ErrorID#$errorID# ",__FILE__,__LINE__);
    exit;
    
}
if (isset($_POST['action']) && $_POST['action'] === 'message_read') {
    $msg = new Message($_POST['message_id']);
    if ($msg->set_read()) {
        $M = new Messenger();
        $message_type = $_POST['message_type'];
        echo common_message('ok',$M->get_unread_numbers(true));
        exit;
    }
    $errorID = uniqid();
    echo common_message('failed',"Set message read failed. ErrorID#$errorID#");
    log_action("ErrorID#$errorID# ",__FILE__,__LINE__);
    exit;
}

/*
 ************************ Messenger request ********************************/


if (isset($_POST['action']) && $_POST['action'] === 'send_pm') {
    if (! isset($_SESSION['Tid'])) {
        exit;
    }
    if ($_POST['message-subject'] === "" && $_POST['message-body'] === '<p><br></p>') {
        exit;
    }
    
    /*$recipients = (isset($_POST['to'])) ? array_map(function($t) {
        return (is_numeric($t)) ? new User($t) : null;
    },$_POST['to']) : [];*/
    $recipients = array();
    
    if (isset($_POST['to'])) {
        foreach ($_POST['to'] as $role_id) {
            if (! is_numeric($role_id)) {
                continue;
            }
            $gr = new Role($role_id);
            $gm = $gr->get_members();
            if (is_array($gm)) {
                foreach($gm as $u) {
                    $recipients[] = $u;
                }
            } else {
                $recipients[] = $gm;
            }
        }
    }
    $recipients = array_unique($recipients);
    
    $M = new Messenger();
    $from = new User($_SESSION['Tid']);
    
    $send_email = ($from->is_master() and isset($_POST['message-as-email']) and $_POST['message-as-email']=='1');
    
    $results = [];
    foreach ($recipients as $to) {
        //$results[$to->name] = ($M->send_pm($from, $to, $_POST['message-subject'], $_POST['message-body'], 0, $send_email)) 
        //    ? ($to->options->receive_personal_message_mails) ? "Message sent to {$to->name}!" : "Message sent to {$to->name}, but email notifications are turned off."
        //    : "Message sending failed to {$to->name}!";
        $res = $M->send_pm($from, $to, $_POST['message-subject'], $_POST['message-body'], 0, $send_email);
        if ($res === true and $M->error=='') $results[$to->name] = ($to->options->receive_personal_message_mails) ? str_message_sentto." => {$to->name}!" : str_message_delivered_locallyto." => {$to->name} ".str_butemail_notif_disabled;
        else if ($res === true and $M->error!='') $results[$to->name] = str_message_delivered_locallyto." => {$to->name} || $M->error.";
        else  $results[$to->name] = str_message_sending_failed." {$to->name}!";
    }
    
    echo common_message('ok', implode("<br>",$results));
    exit;
}

if (isset($_POST['get-addresses-of-group']) and has_access('master')) {
    $recipients = array();
    
    if (isset($_POST['get-addresses-of-group'])) {
        foreach ($_POST['get-addresses-of-group'] as $role_id) {
            if (! is_numeric($role_id)) {
                continue;
            }
            $gr = new Role($role_id);
            $gm = $gr->get_members();
            if (is_array($gm)) {
                foreach($gm as $u) {
                    $recipients[] = $u;
                }
            } else {
                $recipients[] = $gm;
            }
        }
    }
    $recipients = array_unique($recipients);
    echo common_message('ok',$recipients);
    exit;
}

if (isset($_POST['action']) && $_POST['action'] === 'clear_st_col') {
    st_col('default_table', 'session', 0);
}

if (isset($_POST['action']) && $_POST['action'] === 'set_option') {
    if (! isset($_SESSION['Tid'])) {
        echo common_message('error', 'Should be logged in');
        exit;
    }
    if (preg_match('/\W/', $_POST['option_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    $Me = new User($_SESSION['Tid']);
    if (set_option($_POST['option_name'], $_POST['option_value'], $Me->role_id))
        echo common_message('ok', 'ok');
    else
        echo common_message('error', 'Update failed');
    exit;
}

if (isset($_GET['action']) && $_GET['action'] === 'get_option') {
    if (isset($_GET['role_id']) && !isset($_SESSION['Tid'])) {
        echo common_message('error', 'Should be logged in');
        exit;
    }
    if ( (isset($_GET['role_id']) && preg_match('/\D/', $_GET['role_id'])) ||
        preg_match('/\W/', $_GET['option_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    $role_id = $_GET['role_id'] ?? null;
    echo common_message('ok', get_option($_GET['option_name'], $role_id));
    exit;
}

if (isset($_POST['action']) && $_POST['action'] === 'set_option_bool') {
    if (! isset($_SESSION['Tid'])) {
        echo common_message('error', 'Should be logged in');
        exit;
    }
    if (preg_match('/\W/', $_POST['option_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    $Me = new User($_SESSION['Tid']);
    $val = ($_POST['option_value'] === 'true') ? 1 : 0;
    if (set_option($_POST['option_name'], $val, $Me->role_id))
        echo common_message('ok', 'ok');
    else
        echo common_message('error', 'Update failed');
    exit;
}

if (isset($_GET['action']) && $_GET['action'] === 'get_message_template') {
    
    if (!isset($_GET['template_name']) || preg_match('/\W/', $_GET['template_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    
    $template = new MessageTemplate($_GET['template_name']);
    echo json_encode([
        'subject' => $template->get_raw('subject'),
        'message' => $template->get_raw('message')
    ]);
    exit;
}

if (isset($_POST['action']) && $_POST['action'] === 'save_message_template') {
    if (!isset($_POST['template_name']) || preg_match('/\W/', $_POST['template_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    $template = new MessageTemplate($_POST['template_name']);
    foreach (array_keys(LANGUAGES) as $lang) {
        $template->set('subject', $lang, $_POST[$lang]['subject']);
        $template->set('message', $lang, $_POST[$lang]['message']);
        
    }
    if ($template->save()) {
        $te = new TemplateEditor();
        echo common_message('ok',$te->get_templates_list());
    }
    else {
        echo common_message('error', 'Error saving the template!');
    }
    exit;
}
if (isset($_POST['action']) && $_POST['action'] === 'delete_template') {
    if (!isset($_POST['template_name']) || preg_match('/\W/', $_POST['template_name'])) {
        echo common_message('error', 'Bad request');
        exit;
    }
    $template = new MessageTemplate($_POST['template_name']);
    if ($template->delete()) {
        $te = new TemplateEditor();
        echo common_message('ok',$te->get_templates_list()); 
    }
    else {
        echo common_message('error', 'Error deleting the template!');
    }
    
    exit;
}

if (isset($_POST['update-data-rule'])) {
    if (!has_access('master')) {
        echo common_message('error',str_access_denied);
        exit;
    }
    $cmd = sprintf("SELECT row_id FROM %s_rules WHERE row_id=%d AND data_table=%s",PROJECTTABLE,$_POST['update-data-rule'],quote($_POST['qtable']));
    $res = pg_query($ID,$cmd);
    if (pg_num_rows($res)) {
        $cmd = sprintf("UPDATE %s_rules SET read=%s, write=%s, sensitivity=%s WHERE row_id=%d AND data_table=%s",PROJECTTABLE,
            quote("{".$_POST['read']."}"),
            quote("{".$_POST['write']."}"),
            quote($_POST['type']),$_POST['update-data-rule'],quote($_POST['qtable']));
    } else {
        $cmd = sprintf("INSERT INTO %s_rules (data_table,row_id,read,write,sensitivity) VALUES (%s,%d,%s,%s,%s)",PROJECTTABLE,quote($_POST['qtable']),$_POST['update-data-rule'],
            quote("{".$_POST['read']."}"),
            quote("{".$_POST['write']."}"),
            quote($_POST['type'])
        );
    
    }
    $res = pg_query($ID,$cmd);
    if (!pg_last_error($ID)) {
        echo common_message('ok','ok');
    } else {
        log_action(pg_last_error($ID), __FILE__, __LINE__);
        echo common_message('error','not-ok');
    
    }

    exit;
}
// GitLab job download
if (isset($_POST['get-job-from-gitlab'])) {


    $url = "https://gitlab.com/api/v4/projects/22147108/repository/files/lib%2F{$_POST['get-job-from-gitlab']}?ref=master";
    $fp = file_get_contents($url);
    $j = json_decode($fp,true);
    $fp = base64_decode($j['content']);

    $job_lib_file = getenv('PROJECT_DIR') . 'jobs/run/lib/'.$_POST['get-job-from-gitlab'];

    if ( ! file_exists( $job_lib_file ) ) {
        if ( file_put_contents($job_lib_file, $fp) ) {
            
            $url = "https://gitlab.com/api/v4/projects/22147108/repository/files/run%2F{$_POST['get-job-from-gitlab']}?ref=master";
            $fp = file_get_contents($url);
            if ($fp) {
                $j = json_decode($fp,true);
                $fp = base64_decode($j['content']);
                $job_run_file = getenv('PROJECT_DIR') . 'jobs/run/'.$_POST['get-job-from-gitlab'];
                if ( !file_put_contents($job_run_file, $fp) ) {
                
                    // error ..
                }
                echo common_message('ok','ok');

            } else {
                require_once('admin_pages/jobs.php');
                $path_parts = pathinfo($_POST['get-job-from-gitlab']);

                $p = new job_module($path_parts['filename'],PROJECTTABLE);
                $p->set_filename($path_parts['filename'].".".$path_parts['extension']);
                $p->newJob(1);
            }     
            
        }
        else {
        
            echo common_message('error','error');
        }
    } else {
            echo common_message('error',"Job already exists");
    
    }
    exit;
}
/*if (isset($_GET['get-job-form-gitlab'])) {
    $url = "https://gitlab.com/api/v4/projects/22147108/repository/files/lib%2F{$_GET['file']}?ref=master";

    $fp = file_get_contents($url);
    $j = json_decode($fp,true);
    $fp = base64_decode($j['content']);

    $filesize = $j['size'];
    $filename = $j['file_name'];
    # header definíció!!
    header("Content-Type: text");
    header("Content-Length: $filesize");
    header("Content-Disposition: attachment; filename=\"$filename\";");
    header("Expires: -1");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    printf("%s",$fp);
    exit;
}*/

if (isset($_POST['get-job-file-list-from-gitlab'])) {
        $jobs_on_gitlab = "https://gitlab.com/api/v4/projects/22147108/repository/tree?path=lib";
        $files = file_get_contents($jobs_on_gitlab);
        $job_file_list = json_decode($files,true);
        $options = array();
        foreach($job_file_list as $jfl) {
            if ($jfl['name']=='.gitkeep') continue;
            $options[] = array("value"=>$jfl['name'],"text"=>$jfl['name']);
        }
        echo json_encode($options);
        exit;
}

if (isset($_POST['admin-job']) and vreq()) {
    $request = $_POST['parameters'];

    if (isset($request['vm'])) {

        require_once('admin_pages/jobs.php');
        $p = new job_module($request['vm'],PROJECTTABLE);
        if ( isset($request['action']) && $request['action'] == 'save-job') {
            $p->set_filename($request['vm'].".".$request['ext']);
            $p->saveJob($request);
        }
        elseif ( isset($request['action']) && $request['action'] == 'run-job') {
            $p->set_filename($request['vm'].".".$request['ext']);
            $p->runJob($request);
        }
        elseif ( isset($request['action']) && $request['action'] == 'read-job') {
            $p->set_filename($request['vm'].".".$request['ext']);
            echo common_message("ok",$p->get_results());
        }
        elseif ( isset($request['action']) && $request['action'] == 'new-job') {
            if (pathinfo($request['vm'], PATHINFO_EXTENSION)=='')
                $p->set_filename($request['vm'].'.php');
            else
                $p->set_filename($request['vm']);
            $p->newJob($request);
        }
        elseif ( isset($request['action']) && $request['action'] === 'delete-job') {
            $p->set_filename($request['vm'].".".$request['ext']);
            $p->deleteJob($request);
        }
    }
    exit;
}
if (isset($_REQUEST['language-administration'])) {
    include( getenv("OB_LIB_DIR") . "admin_pages/languages.php");
}

if (isset($_POST['repo_wget']) and vreq()) {
    $D = (isset($_POST['D'])) ? $_POST['D'] : 0;

    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');
    $repo = new repository($D);
    $dv = new dataverse($D);

    $header = array("Content-Type:application/json", "X-Dataverse-key:{$repo->api_token}");
    $rest = "/api/dataverses/{$dv->project_dataverse}/contents";
    $result = wget($repo->server_url.$rest,$header);

    echo $result;
    exit;
}

if (isset($_POST['repo_dataset_editor'])) {

    $D = (isset($_POST['D'])) ? $_POST['D'] : 0;

    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');
    $repo = new repository($D);
    $dv = new dataverse($D);

    $SERVER_URL = $repo->server_url;
    $API_TOKEN = $repo->api_token;
    $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");
    $PROJECT_DATAVERSE = $dv->project_dataverse;
    
    $datasets = $dv->get_datasets($SERVER_URL, $API_TOKEN);
    $datasets_options = array('');

    foreach ($datasets as $id=>$d) {
        $title = $dv->get_dataset_attribute($d, 'title');
        $datasets_options[$id] = "$title ($id)::$id";
    }

    $options = "<datalist id='dataset-list'>".selected_option($datasets_options)."</datalist>";

    // Creating dynamic html from
    $em  = "<b>Uploading data from <span style='color:#006FD6'>[".$_POST['name']."]</span> to the repository:</b><br>
        <i class='ai ai-dataverse ai-lg'></i> ";
    $em .= $SERVER_URL.'/'.$PROJECT_DATAVERSE;

    $em .= '<br><hr class="pure-u-1-3" style="border-top:1px solid gray;border-bottom:none">';

    $em .= '<form id="new-dataverse-dataset" class="pure-form pure-form-aligned">
        <input id="obm_pid" type="hidden" name="Share" value="'.$_POST['obm_pid'].'">
        <input id="obm_label" type="hidden" name="Share-name" value="'.$_POST['name'].'">
    <fieldset>
        <div class="pure-control-group">
            <label for="ds-dataset">Dataset title/id</label>
            <input list="dataset-list" name="ds-dataset" id="ds-dataset" placeholder="Dataset" autocomplete="off" required />'.$options.'</select>
            '.$options.'
            <span class="pure-form-message-inline">Choose an existing dataset or create a new with give a name</span>
        </div>
        <div class="pure-control-group ds-autohide">
            <label for="ds-title">Author</label>
            <input id="ds-title" name="ds-author" class="ds-autohide-input" placeholder="Author" value="'.$_SESSION['Tname'].'" />
        </div>
        <div class="pure-control-group ds-autohide">
            <label for="ds-email">Contact E-mail</label>
            <input type="email" id="ds-email" name="ds-email" class="ds-autohide-input" placeholder="Email Address" value="'.$_SESSION['Tmail'].'" required />
        </div>
        <div class="pure-control-group ds-autohide">
            <label for="ds-description">Description</label>
            <textarea class="pure-u-1-5 ds-autohide-input" name="ds-description" type="text" id="ds-description" placeholder="Enter something here..." required />Data from a query '.$_POST['name'].'.</textarea>
        </div>
        <div class="pure-control-group ds-autohide">
            <label for="ds-subject" class="pure-checkbox">Subjects</label>
            <select id="ds-subject" name="ds-subject" class="ds-autohide-input" multiple required />
                    <option>Agricultural Sciences</option>
                    <option>Computer and Information Science</option>
                    <option>Earth and Environmental Sciences</option>
                    <option>Medicine, Health and Life Sciences</option>
                    <option>Other</option></select><br>
        </div>
        <button type="submit" class="pure-button pure-button-primary">Submit</button>
    </fieldset>
    </form>';
    $em .= '<hr class="pure-u-1-3" style="border-top:1px solid gray;border-bottom:none">';

    echo json_encode(array("content"=>$em,"data"=>$datasets));

    exit;
}

if (isset($_POST['new-dataverse-dataset'])) {
    $f = array();
    $f_subj = array();
    foreach($_POST['new-dataverse-dataset'] as $r) {
        if ($r['name'] == 'ds-subject') {
            $f_subj[] = $r['value'];
                
        } else
            $f[$r['name']] = $r['value'];
    }
    $f['ds-subject'] = $f_subj;

    $D = 0;
    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');
    $repo = new repository($D);
    $dv = new dataverse($D);

    $SERVER_URL = $repo->server_url;
    $API_TOKEN = $repo->api_token;
    $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");
    $PROJECT_DATAVERSE = $dv->project_dataverse;

    $file = file_get_contents("$protocol://".URL."/Share/{$f['Share']}/data/");
    $file_name = $f['Share-name'];
    $file_name = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $file_name).".json";

    # Alternative way 1. to create a Darwin Core metadata and DWCA archive zip file
    #require((getenv('OB_LIB_DIR').'darwincore.php');
    #$dwc = new darwinCore;
    #$result = $dwc->create_simple("$protocol://".URL."/Share/{$f['Share']}/metadata/");
    #if ($result['status'] == 'success') {
        #$file = $dwc->create_archive("$protocol://".URL."/Share/{$f['Share']}/data/");
        #$file_name $dwc->archive_name;
    #}
    #
    # https://libraries.frictionlessdata.io/docs/table-schema/php#schema
    # Alternative way 2. to create a FrictionLess Data metadata archive zip file
    #
    # require_once('vendor/autoload.php');
    #
    # Instantiate a table object without schema, in this case the schema will be inferred automatically based on the data
    # $table = new Table("tests/fixtures/data.csv");
    # $table->schema()->fields();  // ["first_name" => StringField, "last_name" => StringField, "order" => IntegerField]
    # $table->save("output.csv")  // iterate over all the rows and save the to a csv file

    if (!is_numeric($f['ds-dataset'])) {
        # Creating a new dataset
        $response = $dv->new_dataset($f,$PROJECT_DATAVERSE,$SERVER_URL,$API_TOKEN);
        $j = json_decode($response,true);
        $dataset = null;
        $doi = null;
        $versionState = null;
        $dataset_id = null;
        if ($j['status'] == 'success') {
            $dataset = $j['data'];
            $doi = $dv->get_dataset_attribute($dataset,'doi');
            $versionState = $dv->get_dataset_attribute($dataset,'version');
            $dataset_id = $dv->get_dataset_attribute($dataset,'id');
        }

    } else {
        # Using an existing dataset
        $response = $dv->get_dataset($SERVER_URL, $API_TOKEN, $f['ds-dataset']);
        $j = json_decode($response,true);
        $dataset = null;
        $doi = null;
        $versionState = null;
        if ($j['status'] == 'success') {
            $dataset = $j['data'];
            $doi = $dv->get_dataset_attribute($dataset,'doi');
            $versionState = $dv->get_dataset_attribute($dataset,'version');
            $dataset_id = $dv->get_dataset_attribute($dataset,'id');
        }
    }

    // Upload a file into Dataset
    $response = $dv->file_add($file, $file_name, $dataset_id, $f['Share'], $SERVER_URL, $API_TOKEN);
    if (is_json($response)) {
        $j = json_decode($response,true);
        if ($j['status'] == 'OK') {
            //{"status":"success","message":"","data":{"dataset":{"id":164,"identifier":"ADATTAR\/GZBJD9","persistentUrl":"https:\/\/doi.org\/10.48428\/ADATTAR\/GZBJD9","protocol":"doi","authority":"10.48428","publisher":"University of Debrecen","storageIdentifier":"file:\/\/10.48428\/ADATTAR\/GZBJD9","latestVersion":{"id":47,"datasetId":164,"datasetPersistentId":"doi:10.48428\/ADATTAR\/GZBJD9","storageIdentifier":"file:\/\/10.48428\/ADATTAR\/GZBJD9","versionState":"DRAFT","lastUpdateTime":"2021-11-09T21:03:46Z","createTime":"2021-11-09T09:18:43Z","license":"NONE","fileAccessRequest":false,"metadataBlocks":{"citation":{"displayName":"Citation Metadata","fields":[{"typeName":"title","multiple":false,"typeClass":"primitive","value":"Estonia air"},{"typeName":"author","multiple":true,"typeClass":"compound","value":[{"authorName":{"typeName":"authorName","multiple":false,"typeClass":"primitive","value":"B\u00e1n Mikl\u00f3s"}}]},{"typeName":"datasetContact","multiple":true,"typeClass":"compound","value":[{"datasetContactEmail":{"typeName":"datasetContactEmail","multiple":false,"typeClass":"primitive","value":"pobiomaps@gmail.com"}}]},{"typeName":"dsDescription","multiple":true,"typeClass":"compound","value":[{"dsDescriptionValue":{"typeName":"dsDescriptionValue","multiple":false,"typeClass":"primitive","value":"Estonia air"}}]},{"typeName":"subject","multiple":true,"typeClass":"controlledVocabulary","value":["Earth and Environmental Sciences"]}]}},"files":[{"description":"","label":"g\u00fcg\u00fcg.json","restricted":false,"version":1,"datasetVersionId":47,"categories":["Data"],"dataFile":{"id":175,"persistentId":"","pidURL":"","filename":"g\u00fcg\u00fcg.json","contentType":"application\/json","filesize":618,"description":"","storageIdentifier":"file:\/\/17d068396e1-0252c78695b8","rootDataFileId":-1,"md5":"13a1939428f0eff789c3dc226851abd6","checksum":{"type":"MD5","value":"13a1939428f0eff789c3dc226851abd6"},"creationDate":"2021-11-09"}}]}},"file":{"files":[{"description":"","label":"Estonia air.json","restricted":false,"version":1,"datasetVersionId":47,"categories":["Data"],"dataFile":{"id":176,"persistentId":"","pidURL":"","filename":"Estonia air.json","contentType":"application\/json","filesize":592,"description":"","storageIdentifier":"file:\/\/17d06871bfc-317c422c4840","rootDataFileId":-1,"md5":"a07726a81e1aea4e864d41eadcbd561c","checksum":{"type":"MD5","value":"a07726a81e1aea4e864d41eadcbd561c"},"creationDate":"2021-11-09"}}]}}}
            
            //https://adattar.unideb.hu/dataset.xhtml?persistentId=doi:10.48428/ADATTAR/GZBJD9&version=DRAFT
            //
            $fileId = $j['data']['files'][0]['dataFile']['id'];

            list($id,$ses) = preg_split("/@/",$f['Share']);
            $cmd = sprintf("UPDATE project_repository SET doi = %s, doi_state = %s, repository_link = %s, file_id = %s WHERE id=%s AND sessionid=%s",
                quote($doi),
                quote($versionState),
                quote($SERVER_URL."/dataset.xhtml?persistentId=".$doi),
                quote($fileId),
                quote($id),quote($ses));
            $res = pg_query($BID,$cmd);
            if ($e = pg_last_error($BID)) {
                debug("ERROR! Couldn't update project_repository: $e",__FILE__,__LINE__);
            }
            echo common_message('ok',array('dataset'=>$dataset,'file'=>$j['data']));
        } else {
            debug('ERROR! '.json_encode($response));
            echo $response;
        }
    }
    else
        echo common_message('error','Malformed data output');
    
    exit;
}

if (isset($_POST['delete-dataset-file'])) {

    $D = 0;
    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');
    $repo = new repository($D);
    $dv = new dataverse($D);

    $SERVER_URL = $repo->server_url;
    $API_TOKEN = $repo->api_token;

    $response = $dv->file_drop($_POST['delete-dataset-file'], $_POST['obm_uid'], $SERVER_URL, $API_TOKEN);
    if (is_json($response)) {
        $j = json_decode($response,true);
        if ($j['status'] == 'success') {
            list($id,$ses) = preg_split("/@/",$_POST['obm_uid']);
            $cmd = sprintf("UPDATE project_repository SET file_id='', repository_link='', doi_state='', doi='' WHERE id=%s AND sessionid=%s",
                quote($id),quote($ses));
            $res = pg_query($BID,$cmd);
            if ($e = pg_last_error($BID)) {
                debug("ERROR! Couldn't update project_repository: $e",__FILE__,__LINE__);
            }
        }
    }
    echo $response;

    exit;
}

if (isset($_POST['dataset-publish'])) {

    $D = 0;
    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');
    $repo = new repository($D);
    $dv = new dataverse($D);

    $SERVER_URL = $repo->server_url;
    $API_TOKEN = $repo->api_token;

    $response = $dv->dataset_publish($_POST['doi'], '', $SERVER_URL, $API_TOKEN);
    if (is_json($response)) {
        $j = json_decode($response,true);
        if ($j['status'] == 'success') {
            list($id,$ses) = preg_split("/@/",$_POST['obm_uid']);
            $cmd = sprintf("UPDATE project_repository SET file_id='', repository_link='', doi_state='', doi='' WHERE id=%s AND sessionid=%s",
                quote($id),quote($ses));
            $res = pg_query($BID,$cmd);
            if ($e = pg_last_error($BID)) {
                debug("ERROR! Couldn't update project_repository: $e",__FILE__,__LINE__);
            }
        }
    }
    echo $response;

    exit;
}
?>
