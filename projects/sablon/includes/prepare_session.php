<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(getenv("PROJECT_DIR") !== false) {
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
} else {
    exit;
}

$token = array('projecttable'=>PROJECTTABLE,'session_id'=>session_id());

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    // new database loaded
    $_GET['logout'] = 1;

} elseif (!isset($_SESSION['token'])) {
    $_SESSION['token'] = $token;
} elseif (isset($_SESSION['token']) and isset($_SESSION['token']['projecttable']) and $_SESSION['token']['projecttable']==PROJECTTABLE and $_SESSION['token']['session_id']!=session_id()) {
    #expired session_id
    $_SESSION['token'] = $token;
}

if (isset($_SESSION['current_query_table'])) {
    if (!preg_match('/^'.PROJECTTABLE.'/',$_SESSION['current_query_table'])) {
        $tables =getProjectTables();
        if (!in_array($_SESSION['current_query_table'],$tables))
            $_GET['logout'] = 1;
    }
}

if (!isset($_SESSION['current_query_table'])) { 

    // This condition addedd due to the query api call in results_query.php
    // If it is not handled, prepare session overwrites the current_query_table and ....
    if (isset($_GET['qtable'])) {

        if (!preg_match('/_/',$_GET['qtable'])) {
            //query table can't be in other schema,other prefix!!!
            $cmd = sprintf('SELECT 1 FROM header_names WHERE f_project_name=\'%1$s\' and f_project_table=%2$s',PROJECTTABLE,quote(PROJECTTABLE."_".$_GET['qtable']));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $_SESSION['current_query_table'] = PROJECTTABLE."_".$_GET['qtable'];
            
            } else {
                if (defined('DEFAULT_TABLE'))
                    $_SESSION['current_query_table'] = DEFAULT_TABLE;
                else
                    $_SESSION['current_query_table'] = PROJECTTABLE;
            }
        } else {
            $cmd = sprintf('SELECT 1 FROM header_names WHERE f_project_name=\'%1$s\' and f_project_table=%2$s',PROJECTTABLE,quote($_GET['qtable']));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $_SESSION['current_query_table'] = $_GET['qtable'];
            
            } else {
                if (defined('DEFAULT_TABLE'))
                    $_SESSION['current_query_table'] = DEFAULT_TABLE;
                else
                    $_SESSION['current_query_table'] = PROJECTTABLE;
            }
            //unset($_GET['qtable']);
        }
        //unset($_SESSION['orderby']);
        // reloading session variables
        //st_col($_SESSION['current_query_table'],'session');
    } elseif (defined('DEFAULT_TABLE'))
        $_SESSION['current_query_table'] = DEFAULT_TABLE;
    else
        $_SESSION['current_query_table'] = PROJECTTABLE;

}

#debug('include prepare_session.php',__FILE__,__LINE__);

#main module initialization
$modules = new modules($_SESSION['current_query_table']);
$x_modules = new x_modules();

//$_SESSION['LANG_SET_TIME'] = time();
?>
