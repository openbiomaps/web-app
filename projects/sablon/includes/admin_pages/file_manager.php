<?php

    if (isset($_POST['offset'])) {
        require(getenv('OB_LIB_DIR').'db_funcs.php');
        if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
            die("Unsuccessful connect to GIS database.");

        if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
            die("Unsuccessful connect to GIS database.");

        if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
            die("Unsuccesful connect to UI database.");

        require(getenv('OB_LIB_DIR').'modules_class.php');
        require(getenv('OB_LIB_DIR').'common_pg_funcs.php');

        pg_query($ID,'SET search_path TO system,public,temporary_tables');
        pg_query($GID,'SET search_path TO system,public,temporary_tables');

        if(!isset($_SESSION['token'])) {
            require(getenv('OB_LIB_DIR').'prepare_vars.php');
            st_col('default_table');
        }
        require(getenv('OB_LIB_DIR').'languages.php');
    }

    $OUTPUT = '';

    $cmd = "SELECT f_project_table as m,f_project_schema as s FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);

    // which table?
    if (isset( $_GET['choose'])) $choose =  $_GET['choose'];
    else $choose = 'public.'.PROJECTTABLE;

    if (isset($_POST['choose']))
        $choose = $_POST['choose'];


    $options = array();
    while($row = pg_fetch_assoc($res)) {
        $s = "";
        if ($row['s'].'.'.$row['m'] == $choose) {
            $s = "selected";
            $stable = $row['m'];
            $sschema = $row['s'];
            $choose = $sschema.'.'.$stable;
        }
        $options[] = "<option $s>{$row['s']}.{$row['m']}</option>";
    }
    $list = sprintf("<select id='file_manager_choose_table' class='pure-button button-secondary'><option></option>%s</select>",implode('',$options));
    $OUTPUT .= $list;

    //set choose table available for ajax file coment save option
    $_SESSION['selected_data-table'] = $choose;

    if (isset($stable)) {
        $OUTPUT .= " <a href='ajax?download_files&table={$choose}' class='pure-button button-href button-success'>".str_export."</a>";
        /*if (file_exists(getenv('PROJECT_DIR').'attached_files/exports/'."attached_files_{$stable}.tar")) {
            $stat = stat(getenv('PROJECT_DIR').'attached_files/exports/'."attached_files_{$stable}.tar");
            $jobhash = hash('ripemd160', MyHASH);
            $OUTPUT .= sprintf(" [<a href='$protocol://".URL."/attached_files/exports/attached_files_{$stable}.tar?$jobhash' target='_blank'>attached_files_{$stable}.tar %dKb %s</a>]",$stat['size']/1024,date('Y-m-d H:i',$stat['mtime']));
        }*/
    }

    $main_cols = dbcolist('columns',$choose);
    $stcol = st_col($choose,'array',false);
    $main_cols[] = 'obm_id';
    $main_cols[] = 'obm_files_id';
    $options = array();
    foreach($main_cols as $mc) {
        $s = '';
        if (isset($_GET['filter_column']) and $mc == $_GET['filter_column']) $s = 'selected';
        $options[] = "<option $s>$mc</option>";
    }
    $OUTPUT .= sprintf(" <select id='filter_files_columns'><option></option>%s</select>",implode('',$options));

    $filter_file = isset($_GET['filter_file']) ? $_GET['filter_file'] : '';
    $OUTPUT .= " ".str_filter.": <input id='filter_files' class='pure-input' value='$filter_file'>";

    $p = array();
    if (is_dir(getenv('PROJECT_DIR').'local/attached_files')){

        $dir = opendir(getenv('PROJECT_DIR').'local/attached_files');
        $files = array();
        while (false !== ($fname = readdir($dir)))
        {
            if (is_file(getenv('PROJECT_DIR').'local/attached_files/'.$fname))
            {
                $files[] = $fname;
            }
        }
        if (!isset($_POST['offset']))
            $_SESSION['attached_files_list'] = $files;
        
        if (!isset($_POST['offset'])) {
            $offset = 0;
            $_SESSION['files_offset'] = 0;
        } else {
            $offset = 20 + $_SESSION['files_offset'];
            $_SESSION['files_offset'] = $offset;
        }

        $delete_button = has_access('master') ? "" : "disabled";

        $connect = 'obm_files_id';
        if ($connect!='') {
            $cmd = '';
            if ($choose == '') {
                    /*$cmd = sprintf('SELECT array_to_string(array_agg(fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(pk.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        LEFT JOIN %4$s pk ON (pk.%2$s=k.%3$s)
                        WHERE f.data_table=\'%1$s\' AND f.project_table=\'%4$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.reference',$choose,'obm_id',$choose,PROJECTTABLE);*/
                    /*$cmd = sprintf('SELECT array_to_string(array_agg(fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        WHERE f.data_table=\'%1$s\' AND f.project_table=\'%1$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.reference',$choose,'obm_id');*/

                $cmd = sprintf('SELECT array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,
                        f.comment,to_char(f.datum,\'YYYY-MM-DD HH24:MI:SS\') as datum,f.data_table,f.user_id,
                        count(*) OVER() AS full_count
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        WHERE f.project_table=\'%1$s\'
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.data_table,f.datum LIMIT 20 OFFSET %2$d',PROJECTTABLE,$offset);

            } else {

                    $filter_file = '';
                    if (isset($_GET['filter_file']) and $_GET['filter_file']!='') {
                        if ($_GET['filter_column'] == 'obm_id' or $_GET['filter_column'] == 'obm_files_id') {
                            $ids = preg_split("/\s*,\s*/",$_GET['filter_file']);
                            $value = implode(',',array_map(function($value) {
                                return preg_replace("/[^0-9]/","",$value);
                            }, $ids));
                        } else
                            $value = quote($_GET['filter_file']);

                        $_SESSION['filter_files_table'] = $choose;
                        $_SESSION['filter_files'] = $value;
                        $_SESSION['filter_files_column'] = $_GET['filter_column'];
                        if (isset($_GET['filter_column']) and in_array($_GET['filter_column'],$main_cols))
                            $filter_file = sprintf(" AND %s IN (%s)",$_GET['filter_column'],$value);
                    }
                    else {
                        unset($_SESSION['filter_files']);
                        unset($_SESSION['filter_files_column']);
                        unset($_SESSION['filter_files_table']);
                    }

                    $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,
                        f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,
                        to_char(f.datum,\'YYYY-MM-DD HH24:MI:SS\') as datum,f.user_id,count(*) OVER() AS full_count
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN "%6$s"."%1$s" k ON (k.obm_files_id=fc.conid)
                        WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                               (k.obm_files_id IS NULL AND f.data_table=\'%1$s\') ) %5$s
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.datum LIMIT 20 OFFSET %4$d',
                            $stable, 'obm_id', PROJECTTABLE, $offset, $filter_file, $sschema);

            }
            if ($stcol['FILE_ID_C'] != '') {
                $res = pg_query($ID,$cmd);

                if (pg_last_error($ID)) {
                    log_action($cmd,__FILE__,__LINE__);    
                    log_action(pg_last_error($ID),__FILE__,__LINE__);    
                }
            } else
                $res = false;


            if (!$res) {
                $OUTPUT .= "<div class='warning'>The file connect columns not set properly or there is no obm_files_id column in the selected table.<br>Check in 'database columns' option.</div>";
            } else {
                $OUTPUT .= "<div class='sevencov'>";
                $OUTPUT .= "<div id='filemanager' class='filemanager tbl'><div class='tbl-row' style='position:sticky;background-color:#dadada;top:0'><div class='tbl-h' style='min-width:60px;font-weight:bold'>icon</div><div class='tbl-h' style='min-width:400px;font-weight:bold'>filename</div><div class='tbl-h' style='font-weight:bold'>".str_comment."</div><div class='tbl-h' style='font-weight:bold'>references</div><div class='tbl-h' style='font-weight:bold'>".str_update."</div><div class='tbl-h' style='font-weight:bold'>".str_delete."</div></div>";
                $n=0;

                while ($row = pg_fetch_assoc($res)) {
                    
                    $full_count = $row['full_count'];

                    $key = array_search($row['reference'],$files);
                    if ($key!==false) {
                        $fname = $files[$key];
                        unset($_SESSION['attached_files_list'][$key]);

                        if (isset($row['obm_id'])) {
                            $gids = explode(',',$row['obm_id']);
                            sort($gids);
                            $gid_links = implode(', ',array_map('make_gid_links',$gids));
                            $gids = implode(', ',$gids);

                            $gids_input = sprintf('<div contenteditable="true" class="data_link" id="origgids%1$d_%2$s">%3$s</div>',$n,$row['obm_id'],$gid_links);
                            //$gids_input = sprintf('<div contenteditable="true" class="data_link" id="origgids%1$d_%2$s">%3$s</div>',$n,$row['obm_id'],$gids);
                            $action = 'save_connection';
                        } else {
                            $gids_input = sprintf('<div contenteditable="true" class="table_link" id="origtable%1$d_%2$s">%2$s</div>',$n,$row['data_table']);
                            //$gids_input = sprintf('<div contenteditable="true" class="table_link" id="origtable%1$d_%2$s">%2$s</div>',$n,$row['data_table']);
                            $action = 'update_table';
                        }

                        $comment = $row['comment'];

                        $userdata = new userdata($row['user_id'],'userid');
                        $username = $userdata->get_username($userdata);

                        $fsize = formatSizeUnits(filesize(getenv('PROJECT_DIR').'local/attached_files/'.$fname));
                            
                        if ($thumb = mkThumb($row['reference'],60)) {
                            // image attachment
                            $thf = "http://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                            $p[] = "<div class='tbl-row' style='background-color:white'>
                                        <div class='tbl-cell'><a href='http://".URL."/getphoto?c={$row['conid']}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$thf' title='{$row['comment']}' class='thumb'></a></div>
                                        <div class='tbl-cell'>$fname ($fsize)<br><span style='font-size:80%'>{$row['datum']}<br>{$username}</span></div>
                                        <div class='tbl-cell'><div class='comment' contenteditable='true'>$comment</div></div>
                                        <div class='tbl-cell'>$gids_input</div>
                                        <div class='tbl-cell'><button id='file_{$row['id']}' data-name='$fname' class='$action pure-button button-success'>".str_save."</button></div>
                                        <div class='tbl-cell'><button id='filedrop_{$row['id']}' data-name='$fname' class='delete_file pure-button button-warning' $delete_button>".str_delete."</button></div>
                                        </div>";
                        } else {
                            // non-image attachment
                            $mime_url = mime_icon($fname,32);
                            $p[] = "<div class='tbl-row' style='background-color:white'>
                                        <div class='tbl-cell'><a href='http://".URL."/getphoto?ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$mime_url' title='{$row['comment']}' class='thumb'></a></div>
                                        <div class='tbl-cell'>$fname ($fsize)<br><span style='font-size:80%'>{$row['datum']}<br>{$username}</span></div>
                                        <div class='tbl-cell'><div class='comment' contenteditable='true'>$comment</div></div>
                                        <div class='tbl-cell'>$gids_input</div>
                                        <div class='tbl-cell'><button id='file_{$row['id']}' data-name='$fname' class='$action pure-button button-success'>".str_save."</button></div>
                                        <div class='tbl-cell'><button id='filedrop_{$row['id']}' data-name='$fname' class='delete_file pure-button button-warning' $delete_button>".str_delete."</button></div>
                                        </div>";
                        }
                    }
                    $n++;
                }
            }
            $e = implode(" ",$p);

            //nincs db bejegyzés
            if ($choose == '' and (!isset($full_count) or $offset >= $full_count)) {
                $t = 0;
                foreach($_SESSION['attached_files_list'] as $fname) {

                    $idx = array_search($fname,$_SESSION['attached_files_list']);
                    unset($_SESSION['attached_files_list'][$idx]);
                    
                    $mime_url = mime_icon($fname,32);
                    $fsize = formatSizeUnits(filesize(getenv('PROJECT_DIR').'local/attached_files/'.$fname));
                    $p[] = "<div class='tbl-row' style='background-color:orange'>
                                <div class='tbl-cell'><a href='http://".URL."/getphoto?ref=$fname' id='gfnodb_$t' class='photolink' target='_blank'><img src='$mime_url' class='thumb'></a></div>
                                <div class='tbl-cell'>$fname ($fsize)</div>
                                <div class='tbl-cell'><div class='comment' contenteditable='true'></div></div>
                                <div class='tbl-cell'><div contenteditable='true' class='table_link' id='newtable$t'></div></div>
                                <div class='tbl-cell'><button id='file_$t' data-name='$fname' class='add_file pure-button button-success'>".str_save."</button></div>
                                <div class='tbl-cell'><button id='filedrop_$t' data-name='$fname' class='delete_file pure-button button-warning' $delete_button>".str_delete."</button></div>
                            </div>";
                }
                $e = implode(" ",$p);
                $t++;
            }
            $OUTPUT .= $e.'</div></div>';

            if (isset($_POST['offset'])) {
                $OUTPUT = $e;
            }
        } else {
        }
    } else {
        $OUTPUT .= str_photos_dir_not_exists;
    }

    if (!isset($_POST['offset']))
        echo "<div class='infotitle'>".wikilink('admin_pages.html#file-manager',t('str_documentation'))."</div>";

    echo $OUTPUT;

function make_gid_links($value) {
    global $choose, $protocol;
    return '<a href="'.$protocol.'://'.URL.'/?data&id='.$value.'&table='.$choose.'">'.$value.'</a>';
};

?>
