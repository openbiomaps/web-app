<?php
    #$mtable = (isset($_GET['mtable'])) ? $_GET['mtable'] : PROJECTTABLE;

$errors = '';
if ($mtable=="Error:") {
    $errors = $_SESSION['update_table_error'];
    unset($_SESSION['update_table_error']);
    $mtable = 'public'.PROJECTTABLE;
}

if (!preg_match("/\./",$mtable)) {
    $mtable_schema = "public";
} else {
    list($mtable_schema, $mtable) = preg_split("/\./",$mtable);
}


    $modules->set_main_table($mtable);
    $custom_menu = $modules->which_has_method('getMenuItem');

    $n = array();
    foreach ($csor as $row) {
        $n[] = $row['description']."::".$row['role_id'];
    }

    $module_list = $modules->list_modules('table');
    $datalist = "<datalist id='modules'>";
    foreach ($module_list as $ml) {
        $datalist .= "<option value='{$ml['name']}/{$ml['path']}'>";
    }
    $datalist .= "</datalist>";
    echo $datalist;

$cmd = "SELECT f_project_table as m, f_project_schema as s FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
$res = pg_query($BID,$cmd);
$mrow = array();
while ($row = pg_fetch_assoc($res)) {
    $mrow[] = $row['m'];        // table
    $srow[] = $row['s'].'.'.$row['m'];        // schema
}

    if (isset($_GET['module_type'])) $editor = $_GET['module_type']; # state clikk
    else $editor = 'project_modules';

    if ($editor == 'project_modules')
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=project_modules&module_type=table_modules' title='".t(str_project_modules)."' class='admin_direct_link' style='float:right;margin-right:1em;font-size:1.2em;'><i class='fa fa-plug'></i> ".t(str_project_modules_management)."</a>";
    else
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=modules&module_type=project_modules' title='".t(str_table_modules)."' class='admin_direct_link pure-button button-href' style='float:right;margin-right:10px;font-size:1.5em;font-weight:bold'>".t('tábla modulok kezelése')." <i class='fa fa-plug fa-rotate-180'></i></a>";
    
if ($errors != '') {
    echo "<div class='message-error'>$errors</div>";
}

    echo "<div class='infotitle'>".wikilink('modules.html',t('str_documentation'))."</div>";

    echo "<h3>".t(str_table_modules_management)."</h3>";

echo "<input type='hidden' class='main_table_scheme_selector' value='$mtable_schema'>";
echo t(str_choose_mtable).": <select class='main_table_selector' style='font-size:125%;vertical-align:middle' id='modules'>".selected_option($srow,$mtable_schema.".".$mtable)."</select> <button class='main_table_refresh' id='modules'><i class='fa fa-refresh'></i></button> ";

    $cmd = "SELECT f_project_schema,id,path,\"enabled\",\"module_name\",\"module_access\",array_to_string(params,'#~#') as params,array_to_string(group_access,',') as ga,new_params, methods, examples
        FROM modules 
        LEFT JOIN header_names ON (f_project_name=project_table AND f_project_table=main_table)
        WHERE project_table='".PROJECTTABLE."' AND main_table=".quote($mtable)." AND module_type='table' AND f_project_schema=".quote($mtable_schema)."
        ORDER BY module_name";
    $res = pg_query($BID,$cmd);

    while ($row = pg_fetch_assoc($res)) {

        # module param parsing (v2.0) from OBM 4.3
        $params = json_encode(json_decode($row['new_params']),JSON_PRETTY_PRINT);

        $schema = $row["f_project_schema"];

        # module params parsing (v1.1) from OBM 1.0 to OBM 4.0
        if ($params == 'null') {
            $params = explode('#~#',$row['params']);
            for ($i = 0; $i < count($params); $i++) {
                if (preg_match("/^JSON:(.*)$/",$params[$i],$json))
                    $params[$i] = base64_decode($json[1]);
            }
            $params = implode("\n", $params);
        }
        $access_groups_options = selected_option($n,explode(',',$row['ga']));

        $id = $row['id'];

        $module_exists = 'color:#FF6F6F;';
        $module_name = $row['module_name'];
        $module_name_file = $row['module_name'].'.php';
        $path = getenv('PROJECT_DIR').$row['path'];

        $module_location = 'local';
        if (preg_match('/^includes/',$row['path'])) {
            $module_location = 'system';
        }

        if (file_exists($path.$module_name_file)) {
            $module_exists = 'color:green;';
            if ($row['enabled']=='f')
                $module_exists = 'color:#cacaca;';
        } else {
            $module_exists = 'color:red;';
            $module_name_file = 'Missing module file!';
        }
        // module just copied, not initialized
        if ($row['methods'] == '' and $module_exists!='color:red;') {
            $content = file($path.$module_name_file);
            $process = 0;
            $yaml = "";
            foreach ($content as $rows) {
                if (($process === 0 or $process == 2 )and preg_match("/^#'/",$rows)) {
                    $process = 2;
                } elseif ($process){
                    $process = 1;
                    continue;
                }
                if ($process == 2) {
                    $yaml .= preg_replace("/^#' /","",$rows);
                }
            }
            if ($yaml != '') {
                $parsed = yaml_parse($yaml);
                $row['methods'] = $parsed['Methods'];
                $row['examples'] = isset($parsed['Examples']) ? $parsed['Examples'] : '';
            } else {
                log_action("$module_name: Bad module format, yaml header missing.",__FILE__,__LINE__);
                $module_exists = 'color:red;';
            }
        }

        $cm = "-";
        if (count($custom_menu) && in_array($module_name, $custom_menu)) {
            $m = $modules->_include($module_name,'getMenuItem');
            if (isset($m['url']))
                $cm = "<a class='module_submenu link' data-url='includes/project_admin.php?options={$m['url']}&mtable=$mtable'><i class='fa fa-cogs'></i> ".t(str_settings)."</a>";
        }

        echo "<label style='$module_exists' onClick='$(\"#module-card-$id\").toggle();$(this).find(\"i\").toggleClass(\"fa-caret-right\");$(this).find(\"i\").toggleClass(\"fa-caret-down\")'><i class='fa fa-large fa-caret-right'></i> $module_name</label>";
        echo "<div id='module-card-$id' class='module-card tbl' style='display:none;margin:1em;border-collapse:separate;border-spacing:2px'>";

        echo "<b>".t(str_path).":</b> ".$module_location;

        if ($module_exists != 'color:red;') {
            $switch = "<label class='switch'><input id='module-ena_$id' name='enabled' type='checkbox' ".checkbox_check($row['enabled'],'t')." value='t'><span class='slider round'></span></label>";
            echo "<div class='tbl-row'><div class='tbl-cell'><b>".t(str_enabled).":</b> ".$switch." <span style='float:right'>$cm</span></div></div>";

            echo "<div class='tbl-row'><div class='tbl-cell'>";
            echo sprintf('<input type=\'hidden\' readonly id=\'module-file_%1$s\' value=\'%2$s\'>',$id,$module_name_file);
            echo sprintf('<input  type=\'hidden\' readonly id=\'module-name_%1$s\' style=\'%3$s\' value=\'%2$s\'>',
                    $id,
                    $module_name,
                    $module_exists);
            echo "</div></div>";


            $placeholder_text = "JSON parameters";
            if ($e = $modules->get_example($module_name_file)) {
                $placeholder_text = $e;
            }
            $module_params_interface = sprintf("<div class='add-column link' style='display:none' id='columnref_%s' data-type='Column' data-table='$mtable' data-schema='$schema'>Add column</div>",$id);
            $module_params_interface .= sprintf("<textarea class='editorArea' id='module-params_%s' style='box-shadow:none;border:1px solid lightgray;resize:none;scrollbar-width:none;width:35em;height:5em;padding:0 0 0 5px' placeholder='%s'>%s</textarea>",
                $id,
                $placeholder_text,
                $params);

            if (in_array($module_name,$modules->which_has_method('module_params_interface'))) {
                $row['textarea'] = $module_params_interface;
                $row['mtable'] = $mtable;
                $row['placeholder_text'] = $placeholder_text;
                $module_params_interface = $modules->_include($module_name, 'module_params_interface', $row);
            }
            echo "<div class='tbl-row'><div class='tbl-cell'><b>".t(str_parameters).":</b><br>".$module_params_interface.'</div></div>';

            echo "<div class='tbl-row'><div class='tbl-cell'><b>".t(str_example).":</b><br>".$row['examples'].'<br><br></div></div>';

            echo "<div class='tbl-row'><div class='tbl-cell'>".wikilink("modules.html#".preg_replace("/_/","-",$module_name),t('str_documentation'))."<br><br></div></div>";

            echo "<div class='tbl-row'><div class='tbl-cell'><span style='vertical-align:top;font-weight:bold'>".t(str_access).':</span> '.sprintf("<span style='vertical-align:top'><select id='module-access_$id' size=2>".selected_option(array('everybody::0','logined users::1'),$row['module_access'])."</select></span>");
            echo sprintf("<select multiple id='module-gaccess_$id'>$access_groups_options</select></div></div>");
            
            echo "<div class='tbl-row'><div class='tbl-cell'>".sprintf("<button name='copld' class='button-warning pure-button module_update' id='module-mod_".$id."'>".str_modifyit."</button></div></div>");
        } else {
            echo "Module file '$module_name_file' not exists in the module path.";
        }

        echo "</div>";
        $id++;
    }

    echo "<h3>".t(str_new_module)."</h3>";
    
    echo t(str_upload_module_file)." (.php/.zip): <input type='file' name='module_name_file' id='module_file-upload' title='.php/.zip file'><br><br>";

    echo t(str_choose_a_module_to_add).":<br>";
    echo "<input list='modules' id='module-name_new'>
          <input type='hidden' id='module-file_new' value='system'>
          <input type='hidden' id='module-ena_new' value='FALSE'>
          <input type='hidden' id='module-params_new' value='[]'>
          <input type='hidden' id='module-gaccess_new' value='0'>
          <select id='module-access_new'><option value=''>".t(str_access)."</option><option value='0'>everybody</option><option value='1'>logined users</option></select>
          <input type='button' name='aopld' class='button-success pure-button module_update' id='module-new_new' value='".str_add."'>";

?>
