<?php

    require('vendor/autoload.php');

    echo "<div class='infotitle'>".wikilink('admin_pages.html#functions',t('str_documentation'))."</div>";

    $mtable = PROJECTTABLE;
    if (isset($_GET['mtable'])) $mtable = $_GET['mtable'];

    if (!preg_match("/\./",$mtable)) {
        $schema = "public";
    } else {
        list($schema, $mtable) = preg_split("/\./",$mtable);
    }
    $st_col = st_col($schema.".".$mtable,'array',false);

    $cmd = "SELECT f_project_table as m, f_project_schema as s FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);
    $mrow = array();
    $srow = array();
    while ($row = pg_fetch_assoc($res)) {
        $mrow[] = $row['m'];
        $srow[] = $row['s'].'.'.$row['m'];        // schema
    }
    echo t(str_choose_mtable).": <select class='main_table_selector' id='functions'>".selected_option($srow,$schema.".".$mtable)."</select><br>";


    echo "<br><h3>Rules</h3>";

    $cmd = "SELECT n.nspname AS rule_schema, 
       c.relname AS rule_table, 
       CASE r.ev_type  
         WHEN '1' then 'SELECT' 
         WHEN '2' then 'UPDATE' 
         WHEN '3' then 'INSERT' 
         WHEN '4' then 'DELETE' 
         ELSE 'UNKNOWN' 
       END AS rule_event, rulename
FROM pg_rewrite r  
  JOIN pg_class c ON r.ev_class = c.oid 
  LEFT JOIN pg_namespace n ON n.oid = c.relnamespace 
  LEFT JOIN pg_description d ON r.oid = d.objoid 
WHERE c.relname='".$mtable."'";
    $res = pg_query($ID,$cmd);
    $tbl = new createTable();
    $tbl->def(['tid'=>'cola','tclass'=>'resultstable']);
    $tbl->addHeader(array('rule_schema','rule_table','rule_event','rulename'));

    while ($row = pg_fetch_assoc($res)) {
        $tbl->addRows($row);
    }

    echo "<br>";
    echo $tbl->printOut();



    echo "<br><h3>Triggers</h3>";

    $cmd = sprintf("SELECT event_object_schema as table_schema,
           event_object_table as table_name,
           trigger_schema,
           trigger_name,
           action_timing as activation,
           string_agg(event_manipulation, ',') as event,
           action_condition as condition,
           action_statement as definition
        FROM information_schema.triggers
        WHERE event_object_table='%s' AND trigger_schema='%s'
        GROUP BY 1,2,3,4,5,7,8
        ORDER BY table_schema,
         table_name",$mtable,$schema);

    $res = pg_query($ID,$cmd);
    $tbl = new createTable();
    $tbl->def(['tid'=>'cola','tclass'=>'resultstable']);
    $tbl->addHeader(array('table_schema','table_name','trigger_schema','trigger_name','event','activation','condition','definition','status'));

    while ($row = pg_fetch_assoc($res)) {
        $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='%s'",$row['trigger_name']);
        $res2 = pg_query($ID,$cmd);
        $num = pg_num_rows($res2);
        if (pg_num_rows($res)) {
            $row2 = pg_fetch_assoc($res2);
            if ($row2['tgenabled'] == 'O' and $num==1) {

                $row['status'] = "<span class='trigger-onoff' data-schema='$schema' data-table='$mtable' id='{$row['trigger_name']}' style='cursor:pointer;color:green;font-weight:bold'>ON</span>";
            } elseif ($row2['tgenabled'] == 'D' and $num==1) {

                $row['status'] = "<span class='trigger-onoff' data-schema='$schema' id='{$row['trigger_name']}' style='cursor:pointer;color:orange;font-weight:bold'>OFF</span>";
            } else {
                
                $row['status'] = "<span style='color:red;font-weight:bold'>Name not unique!</span>";
            }
        }

        $tbl->addRows($row);
    }

    echo "<br>";
    echo $tbl->printOut();
    
    echo "<br><h3>".t(str_functions)."</h3>";

    if (isset($st_col['SPECIES_C']) and $st_col['SPECIES_C']!='') {

        echo "<h5>".t(str_taxon_list_update).":</h5>";
        $on = 'off';
        $color = 'button-passive';
        $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='taxon_update_%s'",$mtable);
        $result = pg_query($ID,$trigger_enabled);
        $cbc = "button-warning";
        if ( pg_num_rows($result) ) {
            $row = pg_fetch_assoc($result);
            if ($row['tgenabled'] == 'O') {
                $on = 'on';
                $color = 'button-success';
            }
            $cbc = "button-passive";
        }

        ## proname MAX 64 character!!!
        echo "<b>update_".$mtable."_taxonlist()</b> <button id='taxonlist-".$mtable."' data-schema='$schema' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button> Trigger: <button id='taxonlist_trigger-".$mtable."' data-schema='$schema' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";
        $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='update_%s_taxonlist'",$mtable,'%');
        $res = pg_query($ID,$cmd);
        $h = "";
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $h = SqlFormatter::highlight($row['prosrc']);
        }
        echo sprintf("<br><div contenteditable='false' id='taxonlist-function' style='border:1px solid lightgray;width:900px'>$h</div>");


        echo "<br><br><h5>".t(str_taxon_name_update).":</h5>";
        $on = 'off';
        $color = 'button-passive';
        $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='%s_name_update'",PROJECTTABLE);
        $result = pg_query($ID,$trigger_enabled);
        $cbc = "button-warning";
        if ( pg_num_rows($result) ) {
            $row = pg_fetch_assoc($result);
            if ($row['tgenabled'] == 'O') {
                $on = 'on';
                $color = 'button-success';
            }
            $cbc = "button-passive";
        }

        echo "<b>update_".PROJECTTABLE."_taxonname()</b> <button id='taxonname-".$mtable."' data-schema='$schema' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button>  Trigger: <button id='taxonname_trigger-".PROJECTTABLE."' data-schema='$schema' class='enablefunction $color pure-button'><i class='fa-toggle-$on fa'></i></button>";
        $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='update_%s_taxonname'",PROJECTTABLE,'%');
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $h = SqlFormatter::highlight($row['prosrc']);
        echo sprintf("<br><div contenteditable='false' id='taxonname-function' style='border:1px solid lightgray;width:900px'>$h</div>");
    }

    // HISTORY -----
    echo "<br><br><h5>".str_history_create.":</h5>";
    $on = 'off';
    $color = 'button-passive';
    $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='history_update_%s'",$mtable);
    $result = pg_query($ID,$trigger_enabled);
    $cbc = "button-warning";
    if ( pg_num_rows($result) ) {
        $row = pg_fetch_assoc($result);
        if ($row['tgenabled'] == 'O') {
            $on = 'on';
            $color = 'button-success';
        }
        $est_cmd = sprintf("SELECT reltuples AS approximate_row_count FROM pg_class WHERE relname = '%s_history'",PROJECTTABLE);
        $result = pg_query($ID,$est_cmd);
        $row=pg_fetch_assoc($result);
        echo "Estimated rows count in history table: ".$row['approximate_row_count']."<br>";
        $cbc = "button-passive";
    }

    echo "<b>".$mtable."_history()</b> <button id='history-".$mtable."' data-schema='$schema' class='createfunction $cbc pure-button'><i class='fa-exclamation fa'></i> ".str_create."</button> Trigger: <button id='history_trigger-".$mtable."' class='enablefunction $color pure-button' data-schema='$schema'><i class='fa-toggle-$on fa'></i></button>";
    $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname='history_%s'",$mtable,'%');
    $res = pg_query($ID,$cmd);
    $h = "";
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $h = SqlFormatter::highlight($row['prosrc']);
    }
    echo sprintf("<br><div contenteditable='false' id='history-function' style='border:1px solid lightgray;width:900px'>$h</div>");


    # Rules functions...
    #
    $on = 'off';
    $color = 'button-passive';
    $trigger_enabled = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s'",$mtable);
    $result = pg_query($ID,$trigger_enabled);
    $cbc = "button-warning";
    if ( pg_num_rows($result) ) {
        $row = pg_fetch_assoc($result);
        if ($row['tgenabled'] == 'O') {
            $on = 'on';
            $color = 'button-success';
        }
        $cbc = "button-passive";
    }

    echo "<br><br><h5>".t(str_rules_create).":</h5>";
    echo "<b>".$mtable."_rules()</b> <button id='rules-".$mtable."' class='createfunction $cbc pure-button' data-schema='$schema'><i class='fa-exclamation fa'></i> ".str_create."</button> Trigger: <button id='rules_trigger-".$mtable."' class='enablefunction $color pure-button' data-schema='$schema'><i class='fa-toggle-$on fa'></i></button>";
    echo " " . t(str_save)." <button id='rule_save-$mtable' data-schema='$schema' class='rule-save pure-button button-warning'> <i class='fa fa-floppy-o fa-large'></i> </button>";

    $cmd = sprintf("SELECT proname,prosrc FROM pg_proc WHERE proname LIKE 'rules_%s'",$mtable,'%');
    $res = pg_query($ID,$cmd);
    //$count = (substr_count($row['prosrc'], "\n") + 1);
    $h = "";
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $h = SqlFormatter::highlight($row['prosrc']);
    }
    echo "<br><div contenteditable='true' id='rule-function' style='border:1px solid lightgray;width:900px'>$h</div>";

?>
