<?php
if (isset($_GET['subpage']) and $_GET['subpage'] == 'views') {
    include('views.php');
    exit;
}

// Error return from mod_project.php
$errors = '';
if ($mtable=="Error:") {
    $errors = $_SESSION['update_table_error'];
    unset($_SESSION['update_table_error']);
    $mtable = 'public'.PROJECTTABLE;
}
if (!preg_match("/\./",$mtable)) {
    $mtable_schema = "public";
} else {
    list($mtable_schema, $mtable) = preg_split("/\./",$mtable);
}

$st_col = st_col($mtable_schema.".".$mtable,'array',false);

if ($modules->is_enabled('list_manager',$mtable)) {
    echo $modules->_include('list_manager','modal');
}


$cmd = "SELECT f_project_table as m, f_project_schema as s FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
$res = pg_query($BID,$cmd);
$mrow = array();
while ($row = pg_fetch_assoc($res)) {
    $mrow[] = $row['m'];        // table
    $srow[] = $row['s'].'.'.$row['m'];        // schema
}


echo "<div class='infotitle'>".wikilink('admin_pages.html#database-columns',t('str_documentation'))."</div>";

echo "<h3><i class='fa fa-database'></i> ".t(str_sql_tables)."</h3>";

if ($errors != '') {
    echo "<div class='message-error'>$errors</div>";
}
echo "<div class='infotitle'>This project contains the following data tables</div>";

$cmd = sprintf('
    SELECT table_schema, table_name, table_type 
    FROM information_schema.tables 
    WHERE table_schema IN ( \'%1$s\',\'public\' ) AND (table_name LIKE \'%1$s_%2$s\' OR table_name=\'%1$s\') 
        UNION
    SELECT schemaname as table_schema, matviewname as table_name, \'MATERIALIZED VIEW\' as table_type 
    FROM pg_matviews 
    WHERE schemaname IN (\'%1$s\', \'public\') AND (matviewname LIKE \'%1$s_%2$s\' OR matviewname=\'%1$s\')
    ORDER BY table_name',
    PROJECTTABLE,'%');

$res = pg_query($GID,$cmd);
echo "<div><div id='db-collist-toggle' style='cursor:pointer'><i class='fa fa-caret-right fa-2x' id='db-collist-down'></i><i class='fa fa-caret-down fa-2x' id='db-collist-up' style='display:none;'></i><div id='db-collist-helper'><ul style='margin-top:-20px'><li>".PROJECTTABLE."</li><li>....</li></ul></div></div><ul id='db-collist' style='display:none;margin-top:-20px'>";

#echo sprintf("<li>%s</li>",PROJECTTABLE);

$system_tables = array('taxon','history','jobs','jobs_results','qgrids','rules','search','search_connect','tracklogs','taxonmeta','terms','terms_connect');
$s = array_map(function($n) {return PROJECTTABLE."_".$n;}, $system_tables);

//$table_schema_type = array();

while ($row = pg_fetch_assoc($res)) {
    /*$table_schema_type[$row['table_schema'].'.'.$row['table_name']] = array(
        'table_schema'=>$row['table_schema'],
        'table_name'=>$row['table_name'],
        'table_type'=>$row['table_type']);*/

    $cmd = sprintf("
    SELECT (CASE WHEN c.reltuples < 0 THEN NULL       -- never vacuumed
             WHEN c.relpages = 0 THEN float8 '0'  -- empty table
             ELSE c.reltuples / c.relpages END
     * (pg_catalog.pg_relation_size(c.oid)
      / pg_catalog.current_setting('block_size')::int)
       )::bigint
    FROM   pg_catalog.pg_class c
    WHERE  c.oid = '%s.%s'::regclass;",$row['table_schema'],$row['table_name']);
    $res2 = pg_query($GID,$cmd);
    $row2 = pg_fetch_assoc($res2);

    $base_table = ($row['table_type']!='BASE TABLE') ? "[view]" : "";
    $sys = (in_array($row['table_name'],$s)) ? "[system table]" : "";

    if (in_array($row['table_name'],$mrow) or $sys)
        echo sprintf("<li>%s.%s <i>%s%s</i> (Estimated row count: %d)</li>",$row['table_schema'],$row['table_name'],$base_table,$sys,$row2['int8']);
    else
        echo sprintf("<li style='color:red'>%s <i>%s%s</i></li>",$row['table_name'],$base_table,$sys);
}
echo "</ul></div>";

//$mtable_schema = $table_schema_type[$$mtable]['table_schema'];

// Get Table Structure
//
/*
SELECT 
 table_name, 
 column_name, 
 data_type 
FROM 
 information_schema.columns
WHERE 
 table_name = '$table';
 */

echo "<br><h3><i class='fa fa-support'></i> ".t(str_sql_access)."</h3><div class='infotitle'></div>";
$host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];
echo t(str_desktop_access).": <b>Host:</b> ".$host." <b>Port:</b> ".POSTGRES_PORT." <b>Database:</b> ".gisdb_name."<br><br>";

// Query private role exists
$cmd = sprintf("SELECT * FROM pg_roles LEFT JOIN pg_group ON oid = ANY (grolist) WHERE rolname = '%s'",preg_replace("/[@.-]/","_",$_SESSION['Tmail']));
$res = pg_query($BID,$cmd);
if (pg_num_rows($res)) {
    $row = pg_fetch_assoc($res);
    
    echo "Role <i>". preg_replace("/[@.-]/","_",$_SESSION['Tmail']) . "</i> exists and valid until <i>" . $row['rolvaliduntil'] . "</i> Member of <i>{$row['groname']}</i>.<br><br>";
    //echo "Add <br>";

} else {
    echo "Create a private user to yourself with the postgres_user module or use the <i>project admin</i> role to access the database.<br><br>";
}

$phppgadmin = (defined('PHPPGADMIN')) ? constant('PHPPGADMIN') : "https://".$host."/phppgadmin";

echo "<a href='$phppgadmin' target='_blank'><img src='images/Postgresql_elephant.svg' style='width:18px;vertical-align:middle'> phpPgAdmin</a> &nbsp; ";
echo "<span class='fa-stack'><i class='fa fa-square fa-stack-2x'></i><i class='fa fa-terminal fa-stack-1x fa-inverse'></i></span> <a href='#tab_basic' id='open-sql-console' class='' alt='SQL console'>SQL Console</a>";

echo "<br><br><br><h3><i class='fa fa-gavel'></i> ".t(str_create_view)."</h3>";
echo "<div class='infotitle'>Create/add a new SQL view to the project</div>";
#echo "<a href='' class='admin_direct_link' data-url='view.php'>Manage views</a>";
echo "<a href='#tab_basic' class='admin_direct_link' data-url='includes/project_admin.php?options=dbcols&subpage=views'> ".t(str_manage_views)."</a>";

/*
 * Create table
 *
 * */

echo "<br><br><br><h3><i class='fa fa-gavel'></i> ".t(str_create_table)."<button id='create_new_table' style='margin-left:50px' class='pure-button button-warning button-xsmall'><i class='fa fa-floppy-o fa-lg'></i></button></h3>";
echo "<div class='infotitle'>Create/add a new SQL table to the project</div>";

echo "<form class='pure-form pure-form-stacked' style='width:400px'>";
echo "<fieldset>";
echo "<div class='pure-control-group'>";
    echo '<label for="new_table">'.str_table_name.":</label> <input class='pure_input pure-u-1-2' id='new_table' maxlength='24' required='required' pattern='[a-z_0-9]{2,24}'><div>";
echo "<div class='pure-control-group'>";
    echo '<label for="new_table_comment">'.str_table_comment.":</label> "."<input class='pure_input pure-u-1' id='new_table_comment' maxlength='128'></div>";
echo "<div class='pure-controls'>";
echo "</div></fieldset></form>";

/*  Table choice
 *
 * */

echo "<br><br><h3><i class='fa fa-tasks'></i> ".t(str_db_cols)."</h3>";
echo "<div class='infotitle'>".str_db_cols_help."</div>";

echo "<input type='hidden' class='main_table_scheme_selector' value='$mtable_schema'>";
echo t(str_choose_mtable).": <select class='main_table_selector' style='font-size:125%;vertical-align:middle' id='dbcols'>".selected_option($srow,$mtable_schema.".".$mtable)."</select> <button class='main_table_refresh' id='dbcols'><i class='fa fa-refresh'></i></button> ";
echo wikilink('admin_pages.html#database-columns',str_what_this,'faq-light');
echo "<br>";

if ( $st_col['USE_RULES'] ) {
    echo "<div style='padding:.5em;'><b>Access restriction:</b> by rules (rules table/rules trigger)</div>";
} else {
    echo "<div style='padding:.5em;'><b>Access restriction:</b> by project setting (Using rules is not enabled)</div>";
}

$cmd = sprintf("select obj_description(oid) from pg_class where relname = %s",quote($mtable));
$rs = pg_query($ID,$cmd);
if (pg_num_rows($rs)) {
    $row = pg_fetch_assoc($rs);
    echo "<div style='padding:.5em;'><b>Table comments:</b> ". $row['obj_description'] ."</div>";
} else {
    echo "<div style='padding:.5em;'>No table comments</div>";
}

/*
 * Table columns
 * */

$col_names = array();
$cmd = sprintf("SELECT column_name,short_name,\"order\",project_table,description FROM project_metaname WHERE project_table='%s' and project_schema='%s'",$mtable,$mtable_schema);
$rs = pg_query($BID,$cmd);
if (pg_num_rows($rs))
    $col_names = array_merge($col_names, pg_fetch_all($rs));

/* special columns from header_names */
$cmd = sprintf("SELECT f_geom_column,f_species_column,
    f_quantity_column,f_id_column,f_x_column,f_y_column,f_use_rules,f_utmzone_column,
    ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
    ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,f_srid,f_order_columns,
    ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
    ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
    FROM header_names
    WHERE f_project_name='".PROJECTTABLE."' AND f_project_table=%s",quote($mtable));
$res = pg_query($BID,$cmd);

if (!pg_num_rows($res)) {
    $main_col = array();
    echo "No header_names entries about $mtable!<br>";
    log_action("$mtable not defined in header_names",__FILE__,__LINE__);
} else {
    $main_col = pg_fetch_assoc($res);
}

//$order_col = explode(",",$main_col['f_order_columns']);
if (isset($main_col['f_order_columns']))
    $order_col = json_decode($main_col['f_order_columns'],true);
else
    $order_col = array();

// default ordering
$values = "('',1)";

$ordered_values = array();
if (isset($order_col[$mtable])) {
    foreach($order_col[$mtable] as $key=>$val) {
        if (!is_numeric($val)) $val = 0;
        $ordered_values[] = "('$key',$val)";
    }
}

//('eov_x',Xc),('eov_y',Yc)

if (count($ordered_values))
    $values = implode(',',$ordered_values);

/* column names from postgres */
$w = array();
$w[] = sprintf("attrelid='%s.%s'::regclass",$mtable_schema,$mtable);

// if there invalid table names in "projects" there will be no columns in project_admin's column page!
$cmd = sprintf('SELECT attrelid::regclass,attnum,attname,column_comment FROM pg_attribute
    LEFT JOIN (
        SELECT
            cols.column_name,
            (
                SELECT
                    pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                FROM
                    pg_catalog.pg_class c
                WHERE
                    c.oid = (SELECT (\'"\' || cols.table_schema || \'"."\' || cols.table_name || \'"\')::regclass::oid)
                    AND c.relname = cols.table_name
            ) AS column_comment
        FROM
            information_schema.columns cols
        WHERE
            cols.table_catalog    = \'%1$s\'
            AND cols.table_name   = \'%2$s\'
            AND cols.table_schema = \'%5$s\'
    ) as cato ON cato.column_name=pg_attribute.attname
    LEFT JOIN (
      VALUES %3$s
     ) AS x (id, ordering) on pg_attribute.attname = x.id
    WHERE (%4$s) AND attnum>0 AND NOT attisdropped 
    ORDER BY x.ordering',
        gisdb_name,$mtable,$values,join(' OR ',$w),$mtable_schema);

$res = pg_query($ID,$cmd);
if (pg_last_error($ID)) {
    if (preg_match('/ERROR:  relation "'.$mtable_schema.'.'.$mtable.'" does not exist/',pg_last_error($ID))) {
        
        echo "$mtable metadata dropped due to $mtable does not exists!<br>";

        // DROP orphaned table's metaname
        $cmd = sprintf("DELETE FROM header_names WHERE f_project_schema=%s AND f_project_name=%s AND f_project_table=%s",
        quote($mtable_schema),quote(PROJECTTABLE),quote($mtable));
        $res = pg_query($BID,$cmd);
        
        // DROP sequence if exists
        $cmd = sprintf("DROP SEQUENCE %s_obm_id_seq",$mtable);
        $res = pg_query($ID,$cmd);
    }   
    $results = array();
}
elseif (!pg_num_rows($res)) {
    echo "Might be wrong query based on <i>main_table</i> content.<br>";
    log_action($cmd,__FILE__,__LINE__);
    $results = array();
}
else {
    $results = pg_fetch_all($res);
}

$tbl = new createTable('cola');
$tbl->def(['tclass'=>'resultstable pure-form']);

$help_col = wikilink('obm_workflow.html#sql-columns-vs-openbiomaps-columns',str_what_this,'faq-light');
$help_name = wikilink('obm_workflow.html#column-names',str_what_this,'faq-light');
$help_comment = wikilink('admin_settings.html#database-tables-and-columns',str_what_this,'faq-light');
$help_command = wikilink('admin_settings.html#database-tables-and-columns',str_what_this,'faq-light');
$help_type = wikilink('obm_workflow.html#sql_columns-vs-openbiomaps-columns',str_what_this,'faq-light');
$help_order = wikilink('obm_workflow.html#column-order',str_what_this,'faq-light');
$header = array(str_column.' '.$help_col,
    str_visible_name.' '.$help_name,
    str_comment.' '.$help_comment,
    str_command.' '.$help_command,
    'OpenBioMaps '.str_type.' '.$help_type."<br><button id='set_columns' class='pure-button' style='cursor:pointer'>set *</button>",
    str_order.' '.$help_order);

//list_manager include
if ($modules->is_enabled('list_manager',$mtable)) {
    $header[] = $modules->_include('list_manager','db_cols_header');
    $list_buttons = $modules->_include('list_manager','manage_list_buttons',[ 
        'table' => $mtable,
        'collist' => array_column($results, 'attname'),
    ]);
}

$tbl->addHeader($header);
$n = 0;

$orphaned_columns = array();
$existing_columns = array();

$obm_columns = array('obm_id','obm_uploading_id','obm_validation','obm_modifier_id','obm_comments');

foreach ($results as $row) {
    $order = '';
    $column_command = "";
    if ($row['attrelid'] == PROJECTTABLE.'.'.$mtable ) {
        # Fallback attrelid if it is has own view
        $row['attrelid'] = $mtable;
    }
    if (isset($order_col[$row['attrelid']])) {
        $order_table = $order_col[$row['attrelid']];
        if (array_key_exists($row['attname'],$order_table) and is_numeric($order_table[$row['attname']])) {
            $order = $order_table[$row['attname']];
        }
    }
    # skip the system cols
    #if  ($row['attname']=='obm_id') continue;
    #if  ($row['attname']=='obm_modifier_id') continue;
    #if  ($row['attname']=='obm_uploading_id') continue;
    #if  ($row['attname']=='obm_comments') continue;
    #if  ($row['attname']=='obm_validation') continue;
    $column_comment = $row['column_comment'];

    $val = '';
    $ro = '';
    if (in_array($row['attname'],$obm_columns)) {
        $ph = 'OBM field';
        $ro = 'readonly';
    } else
        $ph = 'non handled field';

    $selected = "";
    foreach ($col_names as $cn) {
        if ($cn['column_name']==$row['attname'] and $cn['project_table']==$row['attrelid']) {
            $val = $cn['short_name'];
            $selected='data';
            $existing_columns[] = $cn['column_name'];

            if ($cn['description']!='')
                $column_comment = $cn['description'];
        } elseif ($cn['project_table']==$row['attrelid']) {
            // There is no record in the metatable for the field
            $orphaned_columns[] = $cn['column_name'];
        }
    }

    if ($row['attname'] == 'obm_id') {
        if ( $st_col['USE_RULES'] )
            $column_command = 'use_rules:1';
        else
            $column_command = 'use_rules:0';

    } elseif ($row['attname'] == 'obm_geometry') {
        $column_command = 'srid:'.$st_col['SRID_C'];
    }
        
        // special column marking and naming if no name defined
    foreach ($main_col as $key=>$value) {
        $split_value = preg_split('/,/',$value);

        foreach ($split_value as $svalue) {
            if($key=='f_file_id_columns' and $row['attname']=='obm_files_id' and $row['attrelid']==$svalue) {
                $selected='attachment';
                $ro='';
                $ph='special field';
            }

            if ($key === 'f_alter_speciesname_columns') {
                $val_splt = preg_split('/@/', $svalue);
                $svalue = $val_splt[0];
                $lang = $val_splt[1] ?? null;
            }
            //set default translation if local not exists
            //dinpi.faj == ...
            if ($svalue==$row['attname']) {
                #f_species_column	f_date_column	f_quantity_column	f_id_column	f_x_column	f_y_column	f_cite_person	f_srid	f_geom_column
                if ($key=='f_quantity_column') {
                    $selected='numind';
                    $ro='';
                    $ph='special field';
                }
                elseif  ($key=='f_date_columns') {
                    $selected='datum';
                    $ro='';
                    $ph='special field';
                }
                elseif  ($key=='f_species_column') {
                    $selected='species';
                    $ro='';
                    $ph='special field';
                }
                elseif  ($key=='f_id_column') {
                    $selected='id';
                    $ro='readonly';
                    $ph='OBM field';
                    $val='';
                }
                elseif  ($key=='f_x_column') {
                    $selected='Xc';
                    $ro='';$ph='special field';
                }
                elseif  ($key=='f_y_column') {
                    $selected='Yc';
                    $ro='';
                    $ph='special field';
                }
                elseif  ($key=='f_cite_person_columns') {
                    $selected='cp';
                    $ro='';
                    $ph='special field';
                }
                elseif  ($key=='f_geom_column') {
                    $selected='geometry';
                    $ro='';
                    $ph='special field';
                }
                elseif ($key=='f_alter_speciesname_columns') {
                    $selected='alternames' . (($lang) ? "@$lang" : "");
                    $ro='';
                    $ph='special field';
                }
                elseif ($key=='f_utmzone_column') {
                    $selected='utmzone';
                    $ro='';
                    $ph='special field';
                }
            }
        }
    }

    $field = "<input class='element' value='$val' id='pcsi-$n' $ro placeholder='$ph' style='width:250px'>";

    if ($ph == 'OBM field')
        $options = array('OBM column::obm-column');
    else {
        $options = array( '', str_data.'::data', str_spatial_geometry.'::geometry', str_sci_name.'::species', str_alt_names.'::alternames', str_date.'::datum', str_no_inds.'::numind', str_latitude.'::Yc', str_longitude.'::Xc', str_cite_person.'::cp', str_attachment.'::attachment', str_utmzone.'::utmzone' );
        foreach (LANGUAGES as $L => $label) {
          $options[] = str_alt_name . " - $label::alternames@$L";
        }
    }

    $rows = array(
        "<span style='color:gray'>{$row['attrelid']}.</span><input class='element' value='{$row['attname']}' readonly style='width:250px'>",
        $field,
        "<input class='element' id='pcomm-$n' value='".htmlentities($column_comment,ENT_QUOTES)."'>",
        "<input class='element' list='commands' id='pcommand-$n' value='".htmlentities($column_command,ENT_QUOTES)."'>",
        "<select class='element proj_cols_set' id='pcs-$n'>".selected_option($options,$selected)."</select>",
        "<input class='element' size=3 value='$order'>"
    );

    if ($modules->is_enabled('list_manager',$mtable)) {
        
        $rows[] = $list_buttons[$row['attname']];
    }
    $tbl->addRows($rows);

    $n++;
}


$orphaned_columns = array_unique($orphaned_columns);
$diff_columns = array_diff($orphaned_columns,$existing_columns);
foreach ($diff_columns as $oc_key=>$oc) {
        $tbl->addRows(
        array("<span style='color:gray'>$mtable.</span><input class='element' value='$oc' readonly style='width:250px;color:red;font-weight:bold'>",
        "<input class='element' readonly value='$oc' id='pcsi-$n' placeholder='$ph' style='width:250px;color:red;font-weight:bold'>",
        #"<input class='element' readonly id='pcomm-$n' value='".htmlentities($column_comment,ENT_QUOTES)."'>",
        "<input class='element' readonly id='pcomm-$n' value=''>",
        "<input class='element' readonly id='pcommand-$n' value=''>",
        #"<select class='element proj_cols_set' id='pcs-$n'>".selected_option($options,'')."</select>",
        "<select class='element proj_cols_set' id='pcs-$n'></select>",
        "<input class='element' readonly size=3 value='$oc_key'>",""));
        #"<input class='element' readonly size=3 value='$order'>",""));
    $n++;
}

echo $tbl->printOut();
echo '<datalist id="commands">
  <option value="">
  <option value="DROP">
  <option value="RENAME:">
  <option value="DROP-TABLE">
  <option value="SET srid:">
  <option value="SET use_rules">
</datalist>';
if ($n>=2) echo "<button class='button-success button-xlarge pure-button' id='proj_column'><i id='ficon' class='fa fa-floppy-o'></i> ".str_save."</button>";


## Altering column type feature? 
# ALTER TABLE "public"."lepke_idolimit" ALTER COLUMN "ido_kezdes" TYPE time without time zone USING ido_kezdes::time without time zone


$tbl = new createTable('new-col','div');
$tbl->def(['class'=>'resultstable pure-form']);
$tbl->addHeader(array(
    str_column." ".$help_col,
    str_visible_name." ".$help_name,
    str_comment." ".$help_comment,
    'PostgreSQL '.str_type.' <a href="https://www.postgresql.org/docs/11/datatype.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',
    str_length.' <a href="https://www.postgresql.org/docs/11/datatype.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',
    str_default_value.' <a href="https://www.postgresql.org/docs/11/ddl-default.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',
    str_array.' <a href="https://www.postgresql.org/docs/11/arrays.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>',
    str_constraints.' <a href="https://www.postgresql.org/docs/11/ddl-constraints.html" target="_blank"><i class="fa fa-lg fa-question-circle-o"></i></a>'));

// Add new column to a table
echo "<br><br><h3><i class='fa fa-columns'></i> ".t(str_new_column)." <button style='margin-left:50px' class='button-warning pure-button button-xsmall' id='add_new_column'><i class='fa fa-floppy-o fa-lg'></i></button></h3>";
echo "<div class='infotitle'>This will add a new field/column to your `$mtable` SQL table</div>";

$column_types = array(
    str_decimal_number.'::numeric',
    str_integer.'::integer',
    str_date.'::date',
    'timestamp (without time zone)::timestamp without time zone',
    'time (without time zone)::time without time zone',
    ''.str_text.' ('.str_set_it.')::character varying',
    ''.str_text.'::text',
    'true/false::boolean');

$tbl->addRows(
        array("<input type='text' class='element' id='column_name' value='' style='font-size:120%;width:12em' maxlength='24' required='required' pattern='[a-z0-9]{2,24}'>",
        "<input type='text' class='element' value='' id='column_label' style='font-size:120%;width:12em'>",
        "<input type='text' class='element' value='' id='column_comment' style='font-size:120%;'>",
        "<select class='element' id='column_type' style='font-size:120%;'>".selected_option($column_types,'text')."</select>",
        "<input type='text' class='element' id='column_length' value='' style='font-size:120%;width:4em' pattern='[0-9]{1,4}'>",
        "<input type='text' class='element' id='column_default' value='' style='font-size:120%;'>",
        "<select class='element' id='column_array' style='font-size:120%;'><option value=''></option><option value='[]'>[]</option></select>",
        "<input type='text' class='element' id='column_check' value='()' style='font-size:120%'>",
    ));
echo $tbl->printOut();


// clean dbcolist cache
obm_cache('delete',"dbcolist.$mtable",'',0,FALSE);
?>
