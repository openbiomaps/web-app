<?php

    echo "<div class='infotitle'>".wikilink('admin_pages.html#interrupted-uploads',t('str_documentation'))."</div>";

    $timeZone = date_default_timezone_get();
    $cmd = "begin;";
    $res = pg_query($ID,$cmd);
    $cmd = "set local timezone to '$timeZone'";
    $res = pg_query($ID,$cmd);
    $cmd = "SELECT ref,user_id,to_char(datum, 'YYYY-MM-DD HH24:MI:SS') as datum,form_id,form_type,file FROM system.imports WHERE project_table='".PROJECTTABLE."' ORDER BY datum DESC,form_id,user_id";
    $res = pg_query($ID,$cmd);
    while($row=pg_fetch_assoc($res)) {
        $cmd = sprintf("SELECT username FROM users WHERE id=%d",$row['user_id']);
        $rs = pg_query($BID,$cmd);
        $r = pg_fetch_assoc($rs);

        $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id=%d",$row['form_id']);
        $rs2 = pg_query($BID,$cmd);
        $r2 = pg_fetch_assoc($rs2);

        $cmd = sprintf('SELECT 1
                FROM information_schema.tables
                WHERE table_schema = \'temporary_tables\'
                AND table_name = \'%1$s_%2$s\'',$row['file'],$row['ref']);
        $rs3 = pg_query($ID,$cmd);
        if (pg_num_rows($rs3)) {
            $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.%1$s_%2$s',$row['file'],$row['ref']);
            $rs3 = pg_query($ID,$cmd);
            $r3 = pg_fetch_assoc($rs3);
        } else
            $r3 = array("c"=>"missing");

        echo "<button data-id='{$row['ref']}' class='dropimport pure-button button-warning'>".str_delete."</button> <a href='includes/ajax?exportimport={$row['ref']}' class='pure-button button-href button-success'>".str_export."</a> <a href='http://".URL."/upload/?load={$row['ref']}' target='_blank'>{$r['username']}, {$row['datum']} - {$r2['form_name']} ({$row['form_type']}). ".t(str_rows).": {$r3['c']}</a><br>";
    }

    $cmd = "end;";
    $res = pg_query($ID,$cmd);

?>
