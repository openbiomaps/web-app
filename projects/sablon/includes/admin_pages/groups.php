<?php
    echo "<div class='infotitle'>".wikilink('admin_pages.html#groups',t('str_documentation'))."</div>";

    foreach ($csor as $csrow) {
        // skip user roles
        if ($csrow['user_id']!='') continue;

        $a = array();
        $tbl = new createTable();
        $tbl->def(['tid'=>'mytable','tclass'=>'resultstable']);

        printf("<h3><i title='%s' style='cursor:help'>%s</i></h3>",$csrow['role_id'],t($csrow['description']));

        $cmd = "SELECT description, role_id, ARRAY_TO_STRING(container,',') AS g FROM project_roles WHERE project_table = '".PROJECTTABLE."' AND role_id!='{$csrow['role_id']}' ORDER BY description";
        $n = array();
        $res = pg_query($BID,$cmd);
        while($grow = pg_fetch_assoc($res)) {
            //$n[] = $grow['description']."::".$grow['role_id'];
            if (in_array($grow['role_id'],explode(',',$csrow['roles']))) $s = 'checked';
            else $s = '';
            $n[] = "<input class='ng-{$csrow['role_id']}' type='checkbox' value='{$grow['role_id']}' $s> ".$grow['description'];
        }

        //$options = selected_option($n,explode(',',$csrow['roles']));
        //$a[] = "<select name='nested_groups' id='ng-{$csrow['role_id']}' multiple>$options</select>";
        $a[] = '<div style="max-height:10em;overflow-y:scroll;">'.implode($n,'<br>').'</div>';
        $a[] = "<button class='button-warning button-large pure-button mrg_send' id='edg-{$csrow['role_id']}'><i class='fa fa-cog'></i> ".str_save."</button>";
        $a[] = "<button class='button-error button-large pure-button mrg_drop' id='edt-{$csrow['role_id']}'><i class='fa fa-trash'></i> ".str_delete."</button>";

        $tbl->addRows($a);

        $tbl->addHeader(array('included roles','','drop role'));
        echo $tbl->printOut();

    }
    echo "<br><h3>".t(str_new)." ".str_group.":</h3>
        <div class='pure-form pure-form-stacked'>
        <fieldset>";
    echo "<label for='crgr-name'>".t(str_group)." ".str_name.":</label>";
    echo "<input id='crgr-name' class='pure-u-1-5'>";
    echo "<button' id='crgr' class='pure-u-1-5 button-success button-large pure-button'><i class='fa fa-lg fa-cog'></i> ".str_create."</button></fieldset></div>";

?>
