<?php

$remember_me = false;
if (isset($_COOKIE['remember'])) {
    $remember_cookie = json_decode($_COOKIE['remember']);
    $remember_me = $remember_cookie->data->remember;
}

/* Update Login vars - refresh auth tokens */
if (isset($_COOKIE['access_token'])) {

    $cookie = json_decode($_COOKIE["access_token"]);

    // az átmenet idejére, amig mindenhol lecserélődik a süti és belekerül a provider
    if (!isset($cookie->provider)) {
        Auth::logout();
    }

    $auth = ($cookie->provider === 'self') ? new ObmAuth() : new OidcAuth($cookie->provider);

    if (isset($_SESSION['Tid'])) {
        if (!$auth->verify_access_token($cookie->data->access_token, $_SESSION['Tid'], $remember_me)) {
            Auth::logout();
        }
    } else {
        if (!$auth->login_with_access_token($cookie->data->access_token)) {
            Auth::logout();
        }
    }
} elseif (isset($_COOKIE['refresh_token'])) {
    #debug('refresh_token', __FILE__, __LINE__);
    $cookie = json_decode($_COOKIE["refresh_token"]);

    if (!isset($cookie->provider)) {
        Auth::logout();
    }
    $auth = ($cookie->provider === 'self') ? new ObmAuth() : new OidcAuth($cookie->provider);

    if (!$access_token = $auth->refresh_token($cookie->data->refresh_token, $remember_me)) {
        Auth::logout();
    } else {
        if (!$auth->login_with_access_token($access_token)) {
            Auth::logout();
        }
    }
} else {
    // If not logging in
    if (!isset($_POST['loginname'])) {
        #debug('nincs token', __FILE__, __LINE__);

        /* 
         * Kitörli az összes session változót a nem bejelentkezett felhasználóknak!!!
         *
            Auth::logout();
         */
    }
}
