<?php
#' ---
#' Module:
#'   results_buttons
#' Files:
#'   [results_buttons.php, results_buttons.js, results_buttons.css]
#' Description: >
#'   Download as ... buttons  
#'   Share button
#'      save query button
#'      save results button
#'      save spatial selection button
#'   Share data buttons
#' Methods:
#'   [getMenuItem, profileItem, adminPage, print_results_buttons, print_js, print_css, download, count_data, query]
#' Examples: >
#'   {
#'     "bookmarks": "off",
#'     "sharing": "off",
#'     "server_share": "on",
#'   }
#'   bookmarks default is on, sharing and server_share default is off
#' Module-type:
#'   both
#' Author:
#'   banm@vocs.unideb.hu, boneg@milvus.ro
#' Version:
#'   2.0
class results_buttons extends module {

    var $error = '';
    var $strings = ['str_follow_this_link', 'str_downloadascsv', 'str_downloadasgpx', 'str_downloadasjson','str_downloadaskml', 'str_downloadasshp'];
    var $ajax_deps = ['results_builder'];
    var $formats = ['csv','gpx','shp','kml','json','pdf'];
    var $protocol = '';
    public $retval;

    private $modules;
    private $x_modules;
    private $bgproc_limit;

    var $dlr;

    function __construct($action = null, $params = null,$pa = array()) {
        global $ID, $modules, $x_modules;

        $this->protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        $this->modules = $modules;

        $this->x_modules = $x_modules;

        $this->bgproc_limit = defined('DATA_EXPORT_BGPROC_LIMIT') ? constant('DATA_EXPORT_BGPROC_LIMIT') : 1000000;
        
        if ($action)
            $this->retval = $this->$action($params,$pa);
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function getMenuItem() {
        return ['label' => t('str_data_exports'), 'url' => 'results_buttons' ];
    }

    public function profileItem() {
        ob_start();
        ?>
        <tr>
            <td>
                <i class='fa fa-external-link'></i> 
                <a href='#tab_basic' data-url='includes/project_admin.php?options=results_buttons&mtable=<?= constant('PROJECTTABLE') ?>' class='profilelink'>
                    <?= t('str_data_exports') ?>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <?= wikilink("user_interface.html#data-exports",t('str_what_are_data_exports')); ?>
            </td>
        </tr>
        <?php 
        return [
            'label' => t('str_data_exports'),
            'fa' => 'fa-download',
            'item' => ob_get_clean()
        ];
    }

    private function prepAdminPageData() {
        global $BID, $ID;
        
        $where = (!has_access('master')) ? sprintf("WHERE user_id = '%s'",$_SESSION['Tid']) : '';

        $cmd = sprintf("SELECT * FROM system.%s_data_exports r %s ORDER BY requested DESC;", constant('PROJECTTABLE'), $where);
        if ($res = pg_query($ID, $cmd)) {
            $requests_unfiltered = [];
            $requsters = [];
            while ($row = pg_fetch_assoc($res)) {
                $de = new DataExport();
                $de->setArr($row);
                $requests_unfiltered[] = $de;
                $requsters[] = $row['user_id'];
            }
        }


        $users = [];
        if (count($requsters)) {
            $cmd = sprintf("SELECT id, username, email FROM users WHERE id IN (%s);",implode(',',$requsters));
            if (!$res = pg_query($BID,$cmd)) {
                $this->error = "query error: $cmd";
            }
            while ($row = pg_fetch_assoc($res)) {
                $users[$row['id']] = $row;
            }
        }

        $requests = array_filter($requests_unfiltered, function($req) use ($users) {
            return (isset($users[$req->user_id]));
        });
        return compact('users', 'requests');
    }
    
    private function requestTable($requests, $users, $status) {
        $requests = array_filter($requests, function ($r) use ($status) {
            return $r->status == $status;
        });
        ob_start();
        if (!count($requests)) {
            return ob_get_clean();
        }
        ?>
        <h3><?= t("str_{$status}_requests")?></h3>
        <table id="<?= $status ?>Requests" class="pure-table pure-table-striped" style="width: 90%">
            <colgroup>
                <col style="width: 5%">
                <col style="width: 27%">
                <col style="width: 10%">
                <col style="width: 30%">
                <col style="width: 10%">
                <col style="width: 18%">
            </colgroup>
            <thead>
                <tr>
                    <th>id</th>
                    <th><?= t('str_filename') ?></th>
                    <th><?= t('str_requested_by') ?></th>
                    <th><?= t('str_message') ?></th>
                    <th><?= ($status === 'pending') ? t('str_actions') : t('str_valid_until') ?></th>
                    <th><?= t('str_download') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($requests as $r): ?>
                    <tr>
                        <td><?= $r->id ?></td>
                        <td>
                            <?= $r->filename ?>
                        </td>
                        <td><?= $users[$r->user_id]['username'] ?></td>
                        <td><?= $r->message ?></td>
                        <td>
                            <?php if (has_access('master') && $status == 'pending'): ?>
                                <a title="<?= t('str_approve') ?>" class="button-href pure-button button-success approve" data-request_id="<?= $r->id ?>"><i class="fa fa-thumbs-up"></i></a>
                                <a title="<?= t('str_reject') ?>" class="button-href pure-button button-error reject" data-request_id="<?= $r->id ?>"><i style="color: white" class="fa fa-thumbs-down"></i></a>
                            <?php else: ?>
                                <?= $r->valid_until ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if (($r->is_downloadable() === true) && ($r->user_id === (int)$_SESSION['Tid'] || has_access('master')) && in_array($status, ['approved', 'ready'])): ?>
                                <?php if ($this->modules->is_enabled('results_asCSV')): ?>
                                    <a id='export_asCSV' class="pure-button button-small" target="_blank" href='<?= $this->protocol ?>://<?= constant('URL') ?>/ajax?m=results_buttons&action=download&id=<?= $r->id ?>&format=csv'>.CSV</a>
                                <?php endif; ?>
                                <?php if ($this->modules->is_enabled('results_asGPX')): ?>
                                    <a id='export_asGPX' class="pure-button button-small" target="_blank" href='<?= $this->protocol ?>://<?= constant('URL') ?>/ajax?m=results_buttons&action=download&id=<?= $r->id ?>&format=gpx'>.GPX</a>
                                <?php endif; ?>
                                <?php if ($this->modules->is_enabled('results_asKML')): ?>
                                    <a id='export_asKML' class="pure-button button-small" target="_blank" href='<?= $this->protocol ?>://<?= constant('URL') ?>/ajax?m=results_buttons&action=download&id=<?= $r->id ?>&format=kml'>.KML</a>
                                <?php endif; ?>
                                <?php if ($this->modules->is_enabled('results_asSHP')): ?>
                                    <a id='export_asSHP' class="pure-button button-small" target="_blank" href='<?= $this->protocol ?>://<?= constant('URL') ?>/ajax?m=results_buttons&action=download&id=<?= $r->id ?>&format=shp'>.SHP</a>
                                <?php endif; ?>
                                <?php if ($this->modules->is_enabled('results_asJSON')): ?>
                                    <a id='export_asJSON' class="pure-button button-small" target="_blank" href='<?= $this->protocol ?>://<?= constant('URL') ?>/ajax?m=results_buttons&action=download&id=<?= $r->id ?>&format=json'>.JSON</a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php
        return ob_get_clean();
    }

    public function adminPage() {
        
        extract($this->prepAdminPageData());
        ob_start();
        ?>
        
        <div id="download_requests">
            <?php if ($this->x_modules->is_enabled('download_restricted')): ?>
                <?= $this->requestTable($requests, $users, 'pending'); ?>
                <?= $this->requestTable($requests, $users, 'approved'); ?>
                <?= $this->requestTable($requests, $users, 'rejected'); ?>
            <?php endif; ?>
            <?= $this->requestTable($requests, $users, 'ready'); ?>
        </div>

        <?php 
        
        return ob_get_clean();
    }
    
    public function download($params, $request) {
        try {
            $id = (int)$request['id'];
            if (!$id) {
                throw new \Exception('Invalid id');
            }
            $de = new DataExport($id);
            if ($de->user_id !== (int)$_SESSION['Tid'] && !has_access('master')) {
                throw new \Exception('ERROR: Not your file!');
            }
            if (!in_array($request['format'], ['csv', 'gpx', 'json', 'kml', 'shp'])) {
                throw new \Exception('ERROR: Invalid format!');
            }
            $formatU = strtoupper($request['format']);
            
            $this->modules->_include("results_as$formatU", "print_data", ['de_id' => $id]);
            
            // ob_start is necessary to not to append the ajax response to the end of the file
            ob_start();
            return 'clean_ob';
        
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function count_data() {
        global $ID;

        $cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_query_%1$s_%2$s;', constant('PROJECTTABLE'), session_id());
        
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = 'query error';
            return false;
        }
        $results = pg_fetch_assoc($res);
        return [
            'c' => (int)$results['c'],
            'limit' => $this->bgproc_limit,
            'dlr' => $this->x_modules->_include('download_restricted', 'isDownloadRestricted')
        ];
    }
    
    public function print_results_buttons($params,$def) {
        global $BID;

        $strings = [
            'str_download', 'str_export_data', 'str_bookmarks', 'str_label', 'str_shares','str_bookmarking_link',
            'str_save', 'str_share', 'str_share_data_with_other_openbiomaps_project','str_getsharelink',
            'str_get_obm_servers_list', 'str_share_data_width_project','str_other_download_options'
        ];
        

        $b = 0;
        
        $buttons = [
            'url' => $this->protocol."://".constant('URL') . '/',
            't' => array_combine($strings, array_map(function ($s) {
               return t($s); 
            }, $strings)),
            'filename' => sprintf("%s_%s_[%s]",constant('PROJECTTABLE'),date("Y-m-d_G:i"),genhash(6)),
            'logged_in' => (isset($_SESSION['Tid'])),
            'server_share' => (isset($params['server_share'])) ? ($params['server_share']=='on') ? true : false : false,
            'other_download_options' => 'https://openbiomaps.org/documents/'.$_SESSION['LANG'].'/faq.html#what-data-download-options-are-there'
        ];
        
        $cnt = $this->count_data();
        if ($cnt['c'] >= $cnt['limit'] || $cnt['dlr'] === 'yes') {
            $buttons['bg']['dlr'] = ($cnt['dlr'] === 'yes') ? "&dlr=yes" : "";
        }
        else {
            $export_buttons = [];
            foreach ($this->formats as $format) {
                if ($format === 'pdf') {
                    continue;
                }
                if ($this->modules->is_enabled('results_as'. strtoupper($format))) {
                    $export_buttons[] = [
                        'name' => $format, 
                        'nameU' => strtoupper($format), 
                        'btnLabel' => t('str_downloadas' . $format)
                    ];
                    $b++;
                }
            }
            $buttons['export_buttons'] = $export_buttons;
        }

        // if it is an Share mode, do not print save results, save query, save spatial options.

        // Save Query Button for logined users
        if (isset($_SESSION['Tid'])) {

            $buttons['shares'] = [];
            $buttons['bookmarks'] = [];
            $buttons['isbookmarks'] = false;

            if ( isset($params['shares']) and $params['shares'] != 'off' ) {
                $buttons['shares'][] = [
                    'label' => t('str_sharequery'),
                    'icon' => 'share-alt'
                ];
                $b++;
            }

            //ha meg vannak engedve a gombok és nem bookmarked query van végrahajtva
            if ( (!isset($params['bookmarks']) or (isset($params['bookmarks']) and $params['bookmarks'] != 'off')) and 
                 !isset($_SESSION['load_bookmark'])) {
                $buttons['bookmarks'][] = [
                    'label' => t('str_bookmarkquery'),
                    'icon' => 'bookmark-o'
                ];
                $b++;
                $buttons['isbookmarks'] = true;
            }

            //ha meg vannak engedve a gombok és a felhasználó által rajzolt polygonnal készült a lekérdezés
            //sset($_SESSION['selection_geometry_type']) and $_SESSION['selection_geometry_type']=='draw'

            if (isset($_SESSION['load_bookmark'])) {
                unset($_SESSION['load_bookmark']);
            }
        } 

        if ($this->modules->is_enabled('results_asPDF')) {
            $buttons['reporting'] = [];
            $buttons['reporting']['reports'] = $this->modules->_include('results_asPDF', 'get_params');
            $buttons['t'] = array_merge($buttons['t'], $this->modules->_include('results_asPDF', 'translations'));
        }

        if ($b) {
            return render_view('modules/results_buttons', $buttons, false);
        }

        return;
    }
    
    // called from results_buttons.js
    public function query($params, $request) {
        if (!isset($request['filename'])) {
            $this->error = 'filename expected';
            return;
        }
        $method = 'export';
        $filename = $_GET['filename'];
        $format = (in_array($_GET['format'], $this->formats)) ? $_GET['format'] : 'csv';
        $export_options = $this->read_export_options();
        
        $dlr = $this->x_modules->_include('download_restricted', 'isDownloadRestricted');
        
        $bgproc = (isset($_GET['bgproc']) || $dlr === 'yes');
        
        $dlr_message = $_GET['message'] ?? "";
        
        $r = new results_builder(compact('method', 'filename', 'format', 'export_options', 'bgproc', 'dlr', 'dlr_message'));
        if ($r->results_query()) {
            $dl_status = $r->export();
            if (preg_match('/^ready:?(\d+)?$/', $dl_status, $m)) {
                $print_data_params = ['de_id' => $m[1]];
                if (isset($_GET['report']) && $this->modules->is_enabled('results_asPDF')) {
                    $print_data_params['report'] = $_GET['report'];
                }
                $this->modules->_include('results_as' . strtoupper($format), 'print_data', $print_data_params);
                // ob_start is necessary to not to append the ajax response to the end of the file
                ob_start();
                return 'clean_ob';
            }
            elseif (preg_match('/^running:(\d+)$/', $dl_status, $m)) {
                if ($dlr === 'yes') {
                    // the dlr module continues from here
                    return $m[1];
                }
                else {
                    // the export_data job will send a message when the export is ready
                    return t('str_download_started_as_bgproc');
                }
            }
            else {
                return 'Something went wrong!';
            }
        }
        $this->error = $r->error;
        return false;
    }
    
    private function read_export_options() {
        $supported_options = ['sep', 'quote', 'encoding'];
        $export_options = [];
        foreach ($supported_options as $opt) {
            if (isset($_GET[$opt])) {
                $export_options[$opt] = $_GET[$opt];
            }
        }
        return $export_options;
    }
}
?>
