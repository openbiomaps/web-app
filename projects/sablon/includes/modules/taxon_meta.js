$(document).ready(function() {
    $("body").on("change",".taxon-order-selector", async function() {
        try {
            const prefOrd = $(this).find(':selected').val() ?? 'none';
            const data = await $.post('ajax',{
                action: 'set_option',
                option_name: 'taxon_order',
                option_value: (prefOrd === "") ? 'none' : prefOrd
            });
            
            var retval = jsendp(data);
            if (retval.status !== 'success') {
                throw new Error(retval['message']);
            }
        }
        catch (err) {
            alert(err);
        }
    });
});
