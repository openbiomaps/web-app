$(document).ready(function() {
    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === "ioc_bird_list") {
            $('.update_ioc_list').click(async function() {
                try {
                    let resp = await $.post('ajax', {
                        m: "ioc_bird_list",
                        action: "update_ioc_list"
                    })
                    resp = JSON.parse(resp)
                    if (resp.status !== 'success') {
                        throw resp.message
                    }
                    $('#ioc_tmp').html(resp.data);
                } catch (e) {
                    $("#dialog").html(e);
                    $("#dialog").dialog('open');
                }
            });
        }
    });
})
