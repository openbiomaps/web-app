$(document).ready(function() {
    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === "dbcols") {
            
            $(".open-list-manager").on("click", function(e) {
                e.preventDefault();
                const lm = $("#list-manager");
                
                lm.data('colname', $(this).data('colname'));
                lm.data('table', $(this).data('table'));
                
                do_action('get_list');
            });
            
            $(".close-list-manager").on("click",function(e) {
                e.preventDefault();
                const lm = $("#list-manager");
                lm.hide();
            });
            
            $( "#list-manager" ).dialog({
                autoOpen: false,
                title: "List manager",
                modal: true,
                width: 700,
                height: 500,
                draggable: true,
                resizable: true,
                buttons: [
                    {
                        text: "Save",
                        icon: "ui-icon-heart",
                        click: save_list,
                        id: "save-list"
                    },
                    {
                        text: "Generate list from data",
                        icon: 'ui-icon-heart',
                        click: generate_list,
                        id: 'generate-list'
                    },
                    {
                        text: "Cancel",
                        icon: "ui-icon-heart",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
            });
            
            async function save_list() {
                try {
                    const lm = $("#list-manager");
                    
                    const col = lm.data('colname');
                    const tbl = lm.data('table');
                    
                    const data = $('#list-manager-textarea').val();
                    
                    const resp = await $.post('ajax',{
                        m: 'list_manager',
                        action: 'save_list',
                        data: data,
                        colname: col,
                        data_table: tbl
                    });
                    lm.dialog('close');
                    if (data !== "") {
                        $('button[data-colname="'+col+'"]').addClass('button-success');
                    }
                    else {
                        $('button[data-colname="'+col+'"]').removeClass('button-success');
                    }
                }
                catch (error) {
                    console.log(error);
                }
            }
            
            async function generate_list() {
                do_action('generate_list');
            }
            
            
        }
        else if (ev.page === 'upload_forms') {
            
            $("body").on("click", ".lm-list-elements", async function() {
                try {
                    $('#list-editor-help-box').hide();
                    const wid = $('#list-editor').data('wid');
                    const tbl = $('#form_table option:selected').val();
                    
                    const list_ul = $('#lm-list');
                    list_ul.html('');
                    
                    if ($(this).val() == 'all') {
                        clear_list();
                        update_list('optionsTable',obj.project + '_terms');
                        update_list('valueColumn','term');
                        update_list('preFilterColumn',['data_table','subject']);
                        update_list('preFilterValue',[tbl,wid]);
                    }
                    else if ($(this).val() == 'select') {
                        
                        const l = Object.keys(get_list('list'));
                        clear_list();
                        
                        const resp = await $.getJSON('ajax',{
                            m: 'list_manager',
                            action: 'get_list',
                            data_table: tbl,
                            colname: wid,
                        });
                        if (resp.status !== 'success') {
                            throw resp.message
                        }
                        const list = resp.data;
                        list_ul.append('<span><input type="checkbox" name="lm-list-element" class="lm-list-element" value=""> Empty line </span>');
                        for (let k in list) {
                            if (!list.hasOwnProperty(k)) continue;
                            let checked = (l.indexOf(list[k]) >= 0) ? 'checked="true"' : '';
                            list_ul.append('<span><input type="checkbox" name="lm-list-element" class="lm-list-element" value="'+list[k]+'" '+ checked +'>'+list[k]+'</span>');
                        };
                        update_list('list',l);
                    }
                    else if ($(this).val() == 'standard-editor') {
                        list_ul.html('');
                        $('#list-editor-help-box').show();
                    }
                }
                catch (error) {
                    console.log(error);
                }
            });
            
            $("body").on("click", ".lm-list-element", function () {
                let checked = $('#lm-list input:checked').map(function() {
                    return this.value;
                }).get();
                update_list('list',checked);
            });
            
            $('body').on('list-editor-save-success list-editor-cancel',function() {
                $('#lm-list').html('');
                $('.lm-list-elements').prop('checked', false);
            });
        }
    });
    
    async function do_action(action) {
        try {
            const lm = $("#list-manager");
            const tbl = lm.data('table');
            const col = lm.data('colname');
            
            let data = await $.getJSON('ajax',{
                m: 'list_manager',
                action: action,
                colname: col,
                data_table: tbl
            });
            if (data.status !== 'success') {
                throw data.message;
            }
            data = data.data;
            $('#list-manager-textarea').val(data.join("\n"));
            lm.dialog("open");
        }
        catch (error) {
            console.log(error);
        }
    }
});

