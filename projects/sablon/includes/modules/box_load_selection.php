<?php
#' ---
#' Module:
#'   box_load_selection
#' Files:
#'   [box_load_selection.php]
#' Description: >
#'   Map Filter Functions
#'   These functions returns with a html table which displayed beside the map window
#'   These are optional boxes. Setting are in the biomaps db projects' table.
#'   Load prevously saved spatial queries' polygons
#' Methods:
#'   [print_box, adminPage, getMenuItem, profileItem, query_button, print_js]
#' Module-type:
#'   project
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class box_load_selection extends module {
    public $error = '';
    
    function __construct($action = null, $params = null, $pa = array()) {
        global $BID;

        $this->params = $this->split_params($params); 

        if ($action)
            $this->retval = $this->$action($params,$pa);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }
    
    public function print_box ($params) {
        global $ID;

        $strings = ['str_load_selections', 'str_spatial_relationship', 'str_shared_geom_buffer'];
        $box_data = [
            'print_box' => true,
            't' => array_combine($strings, array_map('t', $strings))
        ];
        
        // Access: Private (1), Login (2), Project (3), Public (4)
        // The Project and Public is equal 
        if (isset($_SESSION['Tid']))
            $cmd = sprintf("SELECT id as value,name || ' - ' || to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as label 
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id 
            WHERE  project_table = '%s' AND
            select_view IN ('only-select','select-upload') AND p.user_id=%d 
            ORDER BY name,timestamp",PROJECTTABLE,$_SESSION['Tid']);
        else 
            $cmd = sprintf("SELECT id as value,name || ' - ' || to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as label 
            FROM system.shared_polygons s LEFT JOIN system.polygon_users p ON polygon_id=id  
            WHERE  project_table = '%s' AND
            select_view IN ('only-select','select-upload') AND p.user_id=0
            ORDER BY name,timestamp",PROJECTTABLE);
     
        $res = pg_query($ID,$cmd);
        $box_data['polygons'] = (pg_num_rows($res)) ? pg_fetch_all($res) : [];
        

        $spatial_relationships = ['contains','intersects','crosses','disjoint'];

        $p = array_values(array_intersect($spatial_relationships, $this->params));
        if (count($p)) {
            $spatial_relationships = $p;
        }

        $spatial_relationship_options = [];
        for ($i = 0, $l = count($spatial_relationships); $i < $l; $i++) {
            $spatial_relationship_options[] = [
                'selected' => ($i == 0) ? 'checked' : '',
                'value' => $spatial_relationships[$i],
                'label' => t("str_{$spatial_relationships[$i]}")
            ];
        }
        $box_data['spatial_relationship_options'] = $spatial_relationship_options;
        
        return render_view('modules/box_load_selection', $box_data, false);

    }
    
    public function profileItem() {

        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='?profile&showsharedgeoms#tab_basic' data-url='/includes/profile.php?options=module_function&m=box_load_selection&params=shared' class='profilelink'>".t(str_shared_polygons)."</a><br><br>");
        $em .= sprintf("<tr><td>");

        $em .= str_shp_upload.":<br>KML, SHP (.shp,.shx,.dbf, .prj)<br>";
        $em .= "<form enctype='multipart/form-data' action='' method='post'><table>
            <tr><td><input type='file' id='shpfile' name='shpfile[]' multiple=multiple size='8' class='pure-button'></td></tr>
            <tr><td><div id='shpuplresp'></div></td></tr>
            <tr><td>
label column: <input id='shp_label'><br>
<button id='shp_upload' class='button-large button-secondary pure-button pure-u-1-1'><i class='fa fa-upload' id='shp_upload-i'></i> ".t(str_upload)."</button></td></tr>
            <tr><td>".wikilink('user_interface.html#custom-geometries',str_what_are_custom_geometries)."</td></tr>
            </table></form>";
        $em .= "</td></tr>";

        return [
            'label' => str_polygon_settings,
            'fa' => 'fa-map',
            'item' => $em
        ];
    }
    public function adminPage() {

        if (!has_access('master')) return;

        global $ID,$BID;

        $W = "";


        $em = "<h2>".t(str_all_polygons)."</h2>";
        $em .= "<a href='#tab_basic' data-url='includes/project_admin.php?options=project_modules' title='".t(str_project_modules)."' class='admin_direct_link pure-button button-href' style='margin-top:-80px;float:right;margin-right:10px;font-size:1.5em;font-weight:bold'>".t(str_project_modules)." <i class='fa fa-plug'></i></a>";
        $em .= "<div class='shortdesc'>".str_geomdesc."</div>";
        
        $cmd = "SELECT id, name, original_name, access, p.select_view, p.user_id AS user, s.user_id as owner,st_asText(geometry) as wkt
                    FROM system.shared_polygons s 
                    LEFT JOIN system.polygon_users p ON (id=polygon_id) 
                    WHERE project_table = '".PROJECTTABLE."'
                    ORDER BY name,id";
 
        $share_options = $admin_funcs = $save_b = $del_b = "" ;
        $view_button = '';
        $viewu_button = '';

        $res = pg_query($ID,$cmd);
        $n = 0;
        $table = "";
        $polygonid = '';
        while ( $row = pg_fetch_assoc($res) ) {

            $U = new User($row['owner']);
            $owner = $U->name;
            
            $user = $row['user'];

            $s1 = $s2 = $s3 = $s4 = '';

            // kicsit komplikált, mert egy értéken tárolok két beállítást
            if ($row['select_view']=='none') {
                // elméletileg ilyen eset nincs, mert törlődik 
                $view_button = '<button class="pure-button button-gray button-small polygon_showforce_options sel" id="ops%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Disabled for selection">
                      <i  id="iops%3$d_%2$d-%4$d" class="fa fa-map-o fa-2x"></i>
                  </button>';
                $viewu_button ='<button class="pure-button button-gray button-small polygon_showforce_options upl" id="opu%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Disabled for upload">
                    <i id="iopu%3$d_%2$d-%4$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='only-select') {
                $view_button = '<button class="pure-button button-success button-small polygon_showforce_options sel" id="ops%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Enabled for selection (web map)">
                    <i id="iops%3$d_%2$d-%4$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button ='<button class="pure-button button-gray button-small polygon_showforce_options upl" id="opu%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Disabled for upload">
                    <i id="iopu%3$d_%2$d-%4$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='only-upload') {
                $view_button = '<button class="pure-button button-gray button-small polygon_showforce_options sel" id="ops%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Disabled for selection">
                    <i id="iops%3$d_%2$d-%4$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-success button-small polygon_showforce_options upl" id="opu%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Enabled for upload (web/mobile)">
                    <i id="iopu%3$d_%2$d-%4$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='select-upload') {
                $view_button = '<button class="pure-button button-success button-small polygon_showforce_options sel" id="ops%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Enabled for selection (web map)">
                    <i id="iops%3$d_%2$d-%4$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-success button-small polygon_showforce_options upl" id="opu%3$d_%2$d-%4$d" value="'.$row['select_view'].'" title="Enabled for upload (web/mobile)">
                    <i id="iopu%3$d_%2$d-%4$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            }

            $access_array = array(''=>'public','private'=>'private','public'=>'public');
            $access = $access_array[$row['access']];
            $options = selected_option(array(sprintf('%s::private',str_private),sprintf('%s::public',str_public)),$access);


            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

            $input = '<span style="font-size:8px">%4$s</span><br><input value="%1$s" style="width:300px" class="pure-input polygon_name" id="ope-name%3$d_%2$d">';
            $save_b = '<button class="pure-button button-gray button-small polygon_name_save" id="ope%3$d_%2$d"><i class="fa fa-floppy-o fa-2x"></i></button>';
                
            $maplink = '<a href="#" onClick="MyWindow=window.open(\''.$protocol.'://'.URL.'/geomtest?wkt='.$row['wkt'].'&name='.$row['name'].'\',\'MyWindow\',\'width=600,height=300\'); return false;"><i class="fa fa-map"></i> Show geometry</a>';
                
            $del_b = '<button class="pure-button button-error button-small droppolygon" id="opd%3$d_%2$d"><i class="fa fa-trash-o fa-2x"></i></button>';

            #$cmd = sprintf("SELECT user_id FROM system.polygon_users WHERE polygon_id=%d",$row['id']);
            #$res3 = pg_query($ID,$cmd);
            #if (pg_num_rows($res3) > 0)
            #    $del_b .= " [".pg_num_rows($res3)."]";

            $share_options = '<select class="polygon_access" id="ope-access_%2$d">'.$options.'</select>';

            $cmd = "SELECT id,username,array_to_string(familyname,' ') as familyname,array_to_string(givenname,' ') as givenname
                FROM users u 
                LEFT JOIN project_users pu ON (pu.user_id=u.id)
                WHERE pu.project_table='".PROJECTTABLE."' 
                ORDER BY familyname,givenname,username";

            //Use roles instead of users
            //$cmd = "SELECT role_id,description,ARRAY_TO_STRING(container,',') AS roles,user_id FROM project_roles WHERE project_table='".PROJECTTABLE."' ORDER BY description";
            $res4 = pg_query($BID,$cmd);

            $def_options = array("<option value='0'>".str_public."</option>");
            $def_admin_funcs = '';
            while ( $user_row = pg_fetch_assoc($res4) ) {

                //Use roles instead of users
                //if ($access=='private' and $user_row['user_id']!=$row['owner']) continue;
                //$name = $user_row['description'];
                //$def_options[] = "<option value='{$user_row['role_id']}'>$name</option>";

                

                if ($user_row['familyname'].$user_row['givenname']=='')
                    $name = $user_row['username'];
                else
                    $name = $user_row['familyname'].' '.$user_row['givenname'];


                if ($access=='private' and $user_row['id']!=$row['owner']) 
                    $def_options[] = "<option disabled value='{$user_row['id']}'>$name</option>";
                else
                    $def_options[] = "<option value='{$user_row['id']}'>$name</option>";
                 
            }
            
            $def_admin_funcs .= sprintf('<select multiple class="poly_users_force" id="poly-users-force%3$d_%1$d">%2$s</select>',$row['id'],implode("",$def_options),$n);

            // ez a felső sora minden geometriának
            if ($row['id'] != $polygonid) {

                
                $path_parts = pathinfo($row['original_name']);
                $polygon_name = $path_parts['filename'];
                if (isset($path_parts['extension']))
                    $polygon_name .= ".".$path_parts['extension'];

                $table .= sprintf('<tr class="z_%2$d">
                    <td style="text-align:center">'.$del_b.'</td>
                    <td>'.$input.' '.$save_b.'<br>'.$maplink.'</td>
                    <td>'.t("str_owner").': '.$owner.'<br>'.$share_options.'</td>
                    <td>'.$def_admin_funcs.'</td></tr>',
                $row['name'], $row['id'], $n, $polygon_name );
            }

            $U = new User($user);

            if ($view_button != '')
                $table .= sprintf('<tr class="z_%2$d">
                    <td><input type="hidden" value="%1$s"></td>
                    <td>'.$U->name.'</td>
                    <td colspan=2>'.$view_button.' '.$viewu_button.'</td>
                    </tr>',
                $row['name'],$row['id'],$n,$user);

            $polygonid = $row['id'];
            
            if ($row['id'] != $polygonid) {
                $n++;
            }
        }
        $em .= '<input type="hidden" id="location" data-url="includes/project_admin.php?options=box_load_selection">';
        $em .= "<table class='resultstable'>";
        $em .= "<tr><th>".t(str_delete)."</th><th>".t(str_polygon)."</th><th>Access control</th><th>Add user to access polygon</th></tr>";
        $em .= $table;
        $em .= "</table>";
        return $em;


    }
    // Show saved queries in user profile
    // Returns a html table
    // shared: owner (own) - not owner (shared)
    public function profilePage($params,$pa) {
        global $ID,$BID;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $user_id = (isset($_SESSION['Tid'])) ? $_SESSION['Tid'] : 0;

        $em = "";

        if (isset($_GET['showowngeoms'])) {
            $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
        }
        $em .= "</h2><div class='shortdesc'>".str_geomdesc."</div>";
        $em = "<h2>".t(str_shared_polygons);
        if (isset($_GET['showsharedgeoms'])) {
            $em .= "<a href='?profile={$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
        }
        $em .= "</h2><div class='shortdesc'>".str_geomdesc."</div>";

        // Public is project level
        $cmd = "SELECT id,name,access,p.select_view,p.user_id,s.user_id as owner,st_asText(geometry) as wkt
            FROM system.shared_polygons s 
            LEFT JOIN system.polygon_users p ON (id=polygon_id) 

            WHERE 
              \"project_table\"='".PROJECTTABLE."' AND 
               ( 
                p.user_id='$user_id' 
                OR
                access != 'private'
               ) 
            GROUP BY wkt,id,select_view,p.user_id
            ORDER BY name,id";
        
        $share_options = $save_b = $del_b = "" ;

        $res = pg_query($ID,$cmd);

        $table = "";
        $polips = array();
        while ( $row = pg_fetch_assoc($res) ) {
            if (!isset($polips[$row['id']]))
                $polips[$row['id']] = $row;
            else {
                if ($row['user_id'] == $user_id)
                    $polips[$row['id']] = $row;
            }
        }
        foreach ($polips as $k => $row) {
            
            $share_options = "";
            $del_b = "";
            $save_b = "";
            $own = false;
            if ($user_id===$row['owner']) {
                $own = true;
            }
            
            $s1 = $s2 = $s3 = $s4 = '';
            if ($row['user_id']!==$user_id) $row['select_view']='none';
            #if ($row['user_id']!==$user_id and $row['owner']!==$user_id and $row['user_id']!='') continue;


            // kicsit komplikált, mert egy értéken tárolok két beállítást
            if ( $row['select_view']=='none') {
                $view_button = '<button class="pure-button button-gray button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button ='<button class="pure-button button-gray button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='only-select') {
                $view_button = '<button class="pure-button button-success button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button ='<button class="pure-button button-gray button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='only-upload') {
                $view_button = '<button class="pure-button button-gray button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-success button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            } elseif ( $row['select_view']=='select-upload') {
                $view_button = '<button class="pure-button button-success button-small polygon_show_options sel" id="ops_%2$d" value="'.$row['select_view'].'"><i id="iops_%2$d" class="fa fa-map-o fa-2x"></i></button>';
                $viewu_button = '<button class="pure-button button-success button-small polygon_show_options upl" id="opu_%2$d" value="'.$row['select_view'].'"><i id="iopu_%2$d" class="fa fa-upload fa-2x"></i> <span style="font-size:1.5em">/</span> <i class="fa fa-mobile fa-2x"></i></button>';
            }

            $access_array = array(''=>'public','private'=>'private','public'=>'public');
            $access = $access_array[$row['access']];
            $options = selected_option(array(sprintf('%s::private',str_private),sprintf('%s::public',str_public)),$access);


            if ($own) {
                $share_options = '<select class="polygon_access" id="ope-access_%2$d">'.$options.'</select>';
            }

            $cmd = sprintf("SELECT user_id FROM system.polygon_users WHERE polygon_id=%d AND user_id!=%d",$row['id'],$user_id);
            $res3 = pg_query($ID,$cmd);
            // disable delete option if polygon is used by others
            $shares = 0;
            if (pg_num_rows($res3))
                $shares = pg_num_rows($res3);

            if ($own) {
                if ($shares)
                    $del_b = '<button class="pure-button button-error button-small droppolygon" id="opd0_%2$d" title="'.$shares.'"><span style="font-size:1.6em">!</span> <i class="fa fa-trash-o fa-2x"></i></button>';
                else
                    $del_b = '<button class="pure-button button-warning button-small droppolygon" id="opd0_%2$d"><i class="fa fa-trash-o fa-2x"></i></button>';
                $save_b = '<button class="pure-button button-gray button-small polygon_name_save" id="ope0_%2$d"><i class="fa fa-floppy-o fa-2x"></i></button>';
            }
            $maplink = '<a href="#" onClick="MyWindow=window.open(\''.$protocol.'://'.URL.'/geomtest?wkt='.$row['wkt'].'&name='.$row['name'].'\',\'MyWindow\',\'width=600,height=300\'); return false;"><i class="fa fa-map"></i> Show geometry</a>';

            $table .= sprintf('<tr class="z_%2$d"><td style="text-align:center">'.$del_b.'</td>
                <td><input value="%s" style="width:300px" class="pure-input polygon_name" id="ope-name0_%2$d"> '.$save_b.'<br>'.$maplink.'</td>
                <td style="text-align:center">'.$view_button.'</td><td style="text-align:center">'.$viewu_button.'</td>
                <td>'.$share_options.'</td></tr>',$row['name'],$row['id']);
        }
        $em .= "<table class='resultstable'>";
        $em .= "<tr><th>".t(str_delete)."</th><th>".t(str_selection_name)."</th><th>".t(str_select)."</th><th>".t(str_upload)." / mobile</th><th>".t(str_available_for)."</th></tr>";
        $em .= $table;
        $em .= "</table>";

        echo $em;
    }

    public function getMenuItem() {
        return ['label' => str_all_polygons, 'url' => 'box_load_selection' ];
    }

    public function ajax($params,$pa) {

        return;
    
    }

}
?>
