<?php
#' ---
#' Module:
#'   natura2000
#' Files:
#'   [natura2000.php]
#' Description: >
#'   Natura 2000 validation interface
#' Methods:
#'   [print_js, profileItem, profilePage, adminPage, ajax]
#' Module-type:
#'   project
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.1
class natura2000 extends module {
    
    public $error = '';
    public $project = PROJECTTABLE;
    public $message = '';
    
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;


        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function profilePage($static_params,$dynamic_params) {
        global $ID;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        # Including Tabulator
        echo '<link href="https://unpkg.com/tabulator-tables@5.5.0/dist/css/tabulator.min.css" rel="stylesheet">';
        echo '<script type="text/javascript" src="https://unpkg.com/tabulator-tables@5.4.4/dist/js/tabulator.min.js"></script>';

        echo '<h2>Natura 2000 </h2>';
        echo " <a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=manageDoc' class='pure-button button-transparent profilelink'>Dokumentumok kezelése</a> ";
        echo " <a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=browseSites' class='pure-button button-transparent profilelink'>Területek böngészése</a> ";
        echo " <a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=browseSpecies' class='pure-button button-transparent profilelink'>Fajok böngészése</a> ";

        $this->project = PROJECTTABLE;

        $params = $this->split_params($static_params,'pmi');

        /* Parameters example
        {
            "np-kod": "DI",
            "elohelyterkepek_tabla": "dinpi_elohelyterk_n2000",
            "fajok_x_sites": "dinpi_x_natura2000sites",
            "schema.table": "public.dinpi",
            "date_column": "datum_tol",
            "date_column_2": "datum_ig",
            "species_name_column": "faj_auto",
            "egyedszam_column": "egyedszam",
            "column_list": [
                {
                    "title": "Lel\u0151hely",
                    "field": "lelohely"
                },
                {
                    "title": "Sz\u00e1moss\u00e1g",
                    "field": "szamossag"
                },
                {
                    "title": "M\u00e9ret",
                    "field": "meret_szoveges"
                },
                {
                    "title": "El\u0151fordul\u00e1si \u00e1llapot",
                    "field": "eloford_allapot"
                },
                {
                    "title": "F\u00e9szkel\u00e9s",
                    "field": "feszkeles"
                },
                {
                    "title": "Gy\u00fcjt\u0151",
                    "field": "gyujto"
                },
                {
                    "title": "Kapcsol\u00f3d\u00f3 dokumentumok",
                    "field": "kapcs_dok"
                }
            ]
        }
         */
        
        $elohelyterkepek_tabla = $params["elohelyterkepek_tabla"]; #dinpi_elohelyterk_n2000
        $fajok_x_sites = $params["fajok_x_sites"]; #dinpi_x_natura2000sites
        $data_table = $params["schema.table"]; #"public.dinpi";
        $date_column = $params["date_column"]; #"datum_tol";
        $species_column = $params["species_name_column"]; #"faj_auto";
        $NP_Code = $params["np-kod"]; #'DI';
        $egyedszam_column = $params["egyedszam_column"]; # egyedszam
        $date_column_2 = $params["date_column_2"];

        #lelohely,szamossag,meret_szoveges,eloford_allapot,feszkeles,gyujto,kapcs_dok 
        if (isset($params["column_list"])) {
            $cl_titles = implode(",",array_column($params["column_list"], 'title'));
            $cl_columns = implode(",",array_column($params["column_list"], 'field'));
            $column_array = json_encode($params["column_list"],JSON_PRETTY_PRINT);
            $column_array = preg_replace('/^\[/','',$column_array);
            $column_array = preg_replace('/\]$/','',$column_array);
        }

        if ($dynamic_params[0] == 'manageDoc') {
            echo "<h4>Dokumentumok kezelése</h4>";

        }
        elseif ($dynamic_params[0] == 'browseSites') {

            if (!isset($dynamic_params[1])) {
                $site = '';
            }
            else {
                $site = $dynamic_params[1];
            }
            $np = (isset($dynamic_params[3])) ? $dynamic_params[3] : 'HU'.$params['np-kod']; //{"np-kod": "HUDI"};

            if ($site == '') {
                echo '<h4>Területek böngészése</h4>';

                echo "<ul class='siteBrowser'>";
                $cmd = "SELECT DISTINCT azonosito,nev FROM natura2000.sites WHERE azonosito LIKE '$np%' ORDER BY azonosito,nev";
                if ($res = pg_query($ID, $cmd)) {
                    while ($row = pg_fetch_assoc($res)) {
                        echo sprintf('<li><a href="?profile&natura2000#tab_basic" data-url="/includes/profile.php?options=module_function&m=natura2000&params=browseSites,%1$s,%3$s" class="profilelink">%1$s %2$s</a></li>',
                            $row['azonosito'],
                            $row['nev'],
                            urlencode($row['nev'])
                        );
                    }
                }
                echo "</ul>";
            } else {
                echo "<h4>$site {$dynamic_params[2]}</h4>";

                // Össz jelölő élőhely arány
                $cmd = sprintf('WITH TotalArea AS (
                  SELECT
                    SUM(area_ha) AS total_area
                  FROM
                    %3$s
                  WHERE
                    nat_ter_kod = %1$s 
                )
                SELECT
                  CONCAT_WS(\' \',ROUND(SUM(d1.area_ha) / ta.total_area * 100, 1),\'%2$s\') AS percentage
                FROM
                  %3$s d1, TotalArea ta 
                WHERE
                  d1.nat_ter_kod = %1$s AND d1.nat_eh_kod IS NOT NULL  AND tdo IS NOT NULL
                GROUP BY
                  ta.total_area', quote($site),'%',$elohelyterkepek_tabla);

                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<b>Jelölő élőhelyek aránya site teljes területéhez képest</b>';
                    echo '<div id="mdb-site-eh-3"></div>';
                    echo '<script>var table1 = new Tabulator("#mdb-site-eh-3", {
                        data:tableData,
                        height:"100px",
                        columns:[
                            {title:"Jelölő élőhely aránya", field:"percentage"},
                        ],
                    });</script>';
                }
                // Élőhelyek és összterületük
                $cmd = sprintf('SELECT (
                       SELECT nat_eh_nev
                       FROM %2$s
                       WHERE nat_eh_kod = d1.nat_eh_kod AND nat_eh_nev IS NOT NULL
                       LIMIT 1 ) AS nat_eh_nev, nat_eh_kod, ROUND(SUM(area_ha),3) as sum 
                    FROM %2$s d1
                    WHERE nat_ter_kod=%1$s AND nat_eh_nev IS NOT NULL 
                    GROUP BY nat_eh_kod', quote($site), $elohelyterkepek_tabla);
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Jelölő élőhelyek és összterületük</b>';
                    echo '<div id="mdb-site-eh-1"></div>';
                    echo '<script>var table1 = new Tabulator("#mdb-site-eh-1", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Jelölő élőhely", field:"nat_eh_nev"},
                            {title:"Élőhely kód", field:"nat_eh_kod"},
                            {title:"Össz terület (ha)", field:"sum"},
                        ],
                    });</script>';
                }

                // TDO arányok
                $cmd = sprintf('
                WITH TotalArea AS (
                  SELECT
                    nat_eh_kod,
                    SUM(area_ha) AS total_area
                  FROM
                    %2$s
                  WHERE
                    nat_ter_kod = %1$s AND nat_eh_kod IS NOT NULL AND tdo IS NOT NULL
                  GROUP BY
                    nat_eh_kod
                )
                SELECT
                  (
                    SELECT nat_eh_nev
                    FROM %2$s
                    WHERE nat_eh_kod = d1.nat_eh_kod AND nat_eh_nev IS NOT NULL
                    LIMIT 1
                  ) AS nat_eh_nev,
                  d1.nat_eh_kod,
                  d1.tdo,
                  ROUND(SUM(d1.area_ha), 3) AS sum,
                  ROUND(SUM(d1.area_ha) / ta.total_area * 100, 3) AS percentage
                FROM
                  %2$s d1
                JOIN
                  TotalArea ta ON d1.nat_eh_kod = ta.nat_eh_kod
                WHERE
                  d1.nat_ter_kod = %1$s AND d1.nat_eh_kod IS NOT NULL  AND tdo IS NOT NULL
                GROUP BY
                  d1.nat_eh_kod, d1.tdo, ta.total_area', quote($site),$elohelyterkepek_tabla);

                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Jelölő élőhelyek és TDO arányok</b>';
                    echo '<div id="mdb-site-eh-2"></div>';
                    echo '<script>var table1 = new Tabulator("#mdb-site-eh-2", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Jelölő élőhely", field:"nat_eh_nev"},
                            {title:"Élőhely kód", field:"nat_eh_kod"},
                            {title:"TDO", field:"tdo"},
                            {title:"Össz terület (ha)", field:"sum"},
                            {title:"%", field:"percentage"},
                        ],
                    });</script>';
                }


                // SDF-en szereplő fajok
                $cmd = sprintf('SELECT species, array_agg(year) FROM
                         (SELECT ervenyes_nev as species, year 
                          FROM "natura2000"."mdb_species"
                          LEFT JOIN natura2000.mdb_species_list ON (species_name = "SPECIES_NAME")
                          WHERE "SITE_CODE" = %s
                          GROUP BY year, species
                          ORDER BY species,year) foo
                        GROUP BY species
                        ORDER BY species',quote($site));
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>SDF-ből (mdb) fajok</b>';
                    
                    /*$cmd = 'SELECT DISTINCT substring("SITE_CODE" from \'^HU\w{2}\') FROM natura2000.mdb_site';
                    $res = pg_query($ID, $cmd);
                    while ($row = pg_fetch_assoc($res)) {
                        $mrow[] = $row['substring'];
                    }
                    echo '<select id="mdb-choose-site" data-site="'.$site.'">' . selected_option($mrow,$np) . '</select>';
                     */

                    echo '<div id="mdb-site1"></div>';
                    echo '<script>var table1 = new Tabulator("#mdb-site1", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Faj", field:"species"},
                            {title:"Melyik években fordult elő", field:"array_agg"},
                        ],
                    });</script>';
                }

                // Adatbázisból hiányzó fajok, amelyek ott vannak az SDF-en (mdb)
                $cmd = sprintf('SELECT
                    species_name,
                    \'not in Biotika\' AS note
                FROM
                    (SELECT DISTINCT (ervenyes_nev)::text AS species_name 
                     FROM natura2000.mdb_species 
                     LEFT JOIN natura2000.mdb_species_list ON ("SPECIES_NAME" = ervenyes_nev)
                     WHERE "SITE_CODE"=%1$s) foo
                EXCEPT
                    SELECT 
                            species_name,
                            \'not in Biotika\' AS note
                    FROM 
                            (SELECT DISTINCT (%3$s)::text AS species_name
                             FROM "natura2000"."%2$s" x 
                             LEFT JOIN natura2000.natura2000_fajok f ON x.%3$s=f.faj
                             LEFT JOIN natura2000.sites s ON s.azonosito=x.site_code
                             WHERE x.site_code=%1$s AND s.type=f.type AND f.faj IS NOT NULL) bar',quote($site),$fajok_x_sites,$species_column);
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Adatbázisból hiányzó fajok, amelyek ott vannak az SDF-en (mdb)</b>';
                    echo '<div id="mdb-site3"></div>';
                    echo '<script>var table2 = new Tabulator("#mdb-site3", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Faj", field:"species_name"},
                            {title:"Állapot", field:"note"},
                        ],
                    });</script>';
                }

                //SDF-ről (mdb) hiányzó fajok, amelyek ott vannak az adatbázisban
                $cmd = sprintf('SELECT
	            species_name,
        	    \'not in SDF\' AS note
                FROM
	        (SELECT DISTINCT (%3$s)::text AS species_name
		    FROM "natura2000"."%1$s" x 
		    LEFT JOIN natura2000.natura2000_fajok f ON x.%3$s=f.faj
	            LEFT JOIN natura2000.sites s ON s.azonosito=x.site_code
		    WHERE x.site_code=%2$s AND s.type=f.type AND f.faj IS NOT NULL) bar
                EXCEPT
                	SELECT 
	        	species_name,
	        	\'not in SDF\' AS note
        	    FROM 
		        (SELECT DISTINCT (ervenyes_nev)::text AS species_name 
		 	FROM natura2000.mdb_species 
                        LEFT JOIN natura2000.mdb_species_list ON (species_name = "SPECIES_NAME")
	 		WHERE "SITE_CODE"=%2$s) foo',
                    $fajok_x_sites,quote($site),$species_column);
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>SDF-ről (mdb) hiányzó fajok, amelyek ott vannak az adatbázisban</b>';
                    echo '<div id="mdb-site4"></div>';
                    echo '<script>var table3 = new Tabulator("#mdb-site4", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Faj", field:"species_name"},
                            {title:"Állapot", field:"note"},
                        ],
                    });</script>';
                }

                // Évenkénti faj megfigyelési mennyiségek az adatbázisban
                $cmd = sprintf('SELECT %3$s,count(%3$s),sum(%5$s) as egyedszam, year
                        FROM
                        (SELECT %3$s,%5$s,
                           CASE
                              WHEN extract(year from %6$s) IS NULL THEN extract(year from %4$s)
                              WHEN extract(year from %4$s) IS NULL THEN extract(year from %6$s)
                              ELSE extract(year from %4$s)
                           END as year
                        FROM "natura2000"."%1$s" x 
                        LEFT JOIN natura2000.natura2000_fajok f ON x.%3$s=f.faj
                        LEFT JOIN (SELECT DISTINCT azonosito,type FROM natura2000.sites) s ON s.azonosito=x.site_code
                        WHERE x.site_code=%2$s AND s.type=f.type AND f.faj IS NOT NULL) foo
                        GROUP BY %3$s,year',
                            $fajok_x_sites,
                            quote($site),
                            $species_column,
                            $date_column,
                            $egyedszam_column,
                            $date_column_2);

                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Évenkénti faj megfigyelési mennyiségek az adatbázisban</b>';
		    echo '<div><select id="filter-field4">
				<option></option>
				<option value="'.$species_column.'">Faj</option>
			</select>
			<select id="filter-type4">
				<option value="=">=</option>
				<option value="!=">!=</option>
				<option value="like">like</option>
			</select>
			<input id="filter-value4" type="text" placeholder="value to filter">
			<button id="filter-clear4">Clear Filter</button></div>';
                    echo '<div id="mdb-site5"></div>';
                    echo '<script>
			var fieldEl4 = document.getElementById("filter-field4");
			var typeEl4 = document.getElementById("filter-type4");
			var valueEl4 = document.getElementById("filter-value4");
			function updateFilter(){
			  var filterVal = fieldEl4.options[fieldEl4.selectedIndex].value;
			  var typeVal = typeEl4.options[typeEl4.selectedIndex].value;

			  var filter = filterVal == "function" ? customFilter : filterVal;

			  if(filterVal == "function" ){
			    typeEl4.disabled = true;
			    valueEl4.disabled = true;
			  }else{
			    typeEl4.disabled = false;
			    valueEl4.disabled = false;
			  }
			  if(filterVal){
			    table4.setFilter(filter,typeVal, valueEl4.value);
			  }
			}
			document.getElementById("filter-field4").addEventListener("change", updateFilter);
			document.getElementById("filter-type4").addEventListener("change", updateFilter);
			document.getElementById("filter-value4").addEventListener("keyup", updateFilter);
			//Clear filters on "Clear Filters" button click
			document.getElementById("filter-clear4").addEventListener("click", function(){
			  fieldEl4.value = "";
			  typeEl4.value = "=";
			  valueEl4.value = "";
			  table4.clearFilter();
			});

                        var table4 = new Tabulator("#mdb-site5", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Faj", field:"'.$species_column.'"},
                            {title:"Megfigyelések száma", field:"count"},
                            {title:"Egyedszám", field:"'.$egyedszam_column.'"},
                            {title:"Év", field:"year"},
                        ],
                    });</script>';
                }

                // Évenkénti megfigyelési effort minden natura 2000 fajra
                $cmd = sprintf('SELECT count(*),CASE
                                  WHEN extract(year from %5$s) IS NULL THEN extract(year from %4$s)
                                  WHEN extract(year from %4$s) IS NULL THEN extract(year from %5$s)
                                  ELSE extract(year from %4$s)
                               END as year
                            FROM "natura2000"."%1$s" x 
                            LEFT JOIN natura2000.natura2000_fajok f ON x.%3$s=f.faj
                            WHERE x.site_code=%2$s AND f.faj IS NOT NULL
                            GROUP BY year 
                            ORDER BY year',
                    $fajok_x_sites,
                    quote($site),
                    $species_column,
                    $date_column,
                    $date_column_2);
                
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Évenkénti megfigyelési effort minden natura 2000 fajra</b>';
                    echo '<div id="mdb-site6"></div>';
                    echo '<script>var table = new Tabulator("#mdb-site6", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Megfigyelések száma", field:"count"},
                            {title:"Év", field:"year"},
                        ],
                    });</script>';
                }

                // Adatbázisban szereplő fajok
                $cmd = sprintf('SELECT DISTINCT obm_id,x.obm_id,%3$s,%4$s,%5$s,%6$s,%7$s
                        FROM "natura2000"."%1$s" x 
                        LEFT JOIN "natura2000".natura2000_fajok foo ON x.%3$s=foo.faj
                        LEFT JOIN "natura2000".sites s ON s.azonosito=x.site_code 
                        WHERE x."site_code"=%2$s AND s.type=foo.type AND foo.faj IS NOT NULL
                        ORDER BY %3$s,%4$s',
                            $fajok_x_sites,
                            quote($site),
                            $species_column,
                            $date_column,
                            $egyedszam_column,
                            $date_column_2,
                            $cl_columns);
                
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                    echo '<br><b>Adatbázisból állapot natura2000 fajokra</b>';
		    echo '<div><select id="filter-field5">
				<option></option>
				<option value="'.$species_column.'">Faj</option>
			</select>
			<select id="filter-type5">
				<option value="=">=</option>
				<option value="!=">!=</option>
				<option value="like">like</option>
			</select>
			<input id="filter-value5" type="text" placeholder="value to filter">
			<button id="filter-clear5">Clear Filter</button></div>';
                    echo '<div id="mdb-site2"></div>';
                    echo '<script>
			var fieldEl5 = document.getElementById("filter-field5");
			var typeEl5 = document.getElementById("filter-type5");
			var valueEl5 = document.getElementById("filter-value5");
			function updateFilter(){
			  var filterVal = fieldEl5.options[fieldEl5.selectedIndex].value;
			  var typeVal = typeEl5.options[typeEl5.selectedIndex].value;

			  var filter = filterVal == "function" ? customFilter : filterVal;

			  if(filterVal == "function" ){
			    typeEl5.disabled = true;
			    valueEl5.disabled = true;
			  }else{
			    typeEl5.disabled = false;
			    valueEl5.disabled = false;
			  }
			  if(filterVal){
			    table5.setFilter(filter,typeVal, valueEl5.value);
			  }
			}
			document.getElementById("filter-field5").addEventListener("change", updateFilter);
			document.getElementById("filter-type5").addEventListener("change", updateFilter);
			document.getElementById("filter-value5").addEventListener("keyup", updateFilter);
			//Clear filters on "Clear Filters" button click
			document.getElementById("filter-clear5").addEventListener("click", function(){
			  fieldEl5.value = "";
			  typeEl5.value = "=";
			  valueEl5.value = "";
			  table5.clearFilter();
			});
			var table5 = new Tabulator("#mdb-site2", {
                        data:tableData,
                        height:"311px",
                        columns:[
                            {title:"Faj", field:"'.$species_column.'"},
                            {title:"Dátum-tól", field:"'.$date_column.'"},
                            {title:"Dátum-ig", field:"'.$date_column_2.'"},
                            {title:"Egyedszám", field:"'.$egyedszam_column.'"},
                            '.$column_array.'
                        ],
                    });</script>';
                }
            
            }
            
/*
 *  Browse species
 *
 * 
 * */

        } elseif ($dynamic_params[0] == 'browseSpecies') {
            if (!isset($dynamic_params[1])) {
                $species = '';
            }
            else {
                $species = $dynamic_params[1];
            }
            $np = (isset($dynamic_params[2])) ? $dynamic_params[2] : 'HU'.$params['np-kod'];

            if ($species == '') {
                echo '<h4>Fajok böngészése</h4>';
                $species = array();
                $cmd = "SELECT DISTINCT ervenyes_nev AS species_name FROM natura2000.mdb_species_list ORDER BY species_name";
                if ($res = pg_query($ID, $cmd)) {
                    echo "<ul class='speciesBrowser'>";
                    while ($row = pg_fetch_assoc($res)) {
                        $species_name = $row['species_name'];
                        echo "<li><a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=browseSpecies,".urlencode($species_name)."' class='profilelink'>$species_name</a></li>";
                    }
                    echo "</ul>";
                }
            } else {
                echo "<h4>$species</h4>";
                
                
                echo '<b>MDB állapot</b> ';

                $cmd = "SELECT * FROM natura2000.select_species_onsite_mdb(".quote($species).",'$np%') ORDER BY \"YEAR\", site_name";
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                }

                $cmd = 'SELECT DISTINCT substring("SITE_CODE" from \'^HU\w{2}\') FROM natura2000.mdb_site';
                $res = pg_query($ID, $cmd);
                while ($row = pg_fetch_assoc($res)) {
                    $mrow[] = $row['substring'];
                }

                echo '<select id="mdb-choose" data-species="'.$species.'">' . selected_option($mrow,$np) . '</select>';
		echo '<div>
    <button id="download-csv">Download CSV</button>
    <button id="download-xlsx">Download XLSX</button>
</div>';
                echo '<div id="sdf-mdb"></div>';
                echo '<script>
		var table1 = new Tabulator("#sdf-mdb", {
                    data:tableData,
                    height:"311px",
                    columns:[
                        {title:"Év", field:"YEAR"},
                        {title:"site_code", field:"site_code"},
                        {title:"site_name", field:"site_name"},
                        {title:"species_id", field:"species_id"},
                        {title:"species_group", field:"species_group"},
                        {title:"species_code", field:"species_code"},
                        {title:"species_sensitive", field:"species_sensitive"},
                        {title:"species_np", field:"species_np"},
                        {title:"species_type", field:"species_type"},
                        {title:"species_size_min", field:"species_size_min"},
                        {title:"species_size_max", field:"species_size_max"},
                        {title:"species_unit", field:"species_unit"},
                        {title:"species_category", field:"species_category"},
                        {title:"species_data_quality", field:"species_data_quality"},
                        {title:"species_population", field:"species_population"},
                        {title:"species_conservation", field:"species_conservation"},
                        {title:"species_isolation", field:"species_isolation"},
                        {title:"species_global", field:"species_global"},
                    ],
                });
//trigger download of data.csv file
document.getElementById("download-csv").addEventListener("click", function(){
    table1.download("csv", "data.csv");
});
//trigger download of data.xlsx file
document.getElementById("download-xlsx").addEventListener("click", function(){
    table1.download("xlsx", "data.xlsx", {sheetName:"My Data"});
});
//trigger download of data.json file
//document.getElementById("download-json").addEventListener("click", function(){
//    table1.download("json", "data.json");
//});
//trigger download of data.pdf file
//document.getElementById("download-pdf").addEventListener("click", function(){
//    table1.downloadToTab("pdf", "data.pdf", {
//        orientation:"portrait", //set page orientation to portrait
//        title:"Example Report", //add title to report
//    });
//});
//trigger download of data.html file
//document.getElementById("download-html").addEventListener("click", function(){
//    table1.download("html", "data.html", {style:true});
//});


</script>';


                $cmd = "SELECT * FROM natura2000.select_species_onsite(".quote($species).",'HU{$params['np-kod']}%') ORDER BY \"year_\", site_code_";
                if ($res = pg_query($ID, $cmd)) {
                    $rows = pg_fetch_all($res);
                    echo "<script>tableData=".json_encode($rows)."</script>";
                }

                #YEAR	site_code	site_name	species_id	species_group	species_code	species_name	species_sensitive	species_np	species_type	species_size_min	species_size_max	species_unit	species_category	species_data_quality	species_population	species_conservation	species_isolation	species_global
                echo '<br><b>SDF Excel állapot</b>';
                echo '<div id="sdf-excel"></div>';
                echo '<script>var table2 = new Tabulator("#sdf-excel", {
                    data:tableData,
                    height:"311px",
                    columns:[
                        {title:"Év", field:"year_"},
                        {title:"file", field:"file_"},
                        {title:"sheet", field:"sheet_"},
                        {title:"site_code", field:"site_code_"},
                        {title:"site_name", field:"site_name_"},
                        {title:"fajnév", field:"species_name_"},
                        {title:"species_group", field:"species_group_"},
                        {title:"species_type", field:"species_type_"},
                        {title:"species_size_min", field:"species_size_min_"},
                        {title:"species_size_max", field:"species_size_max_"},
                        {title:"species_unit", field:"species_unit_"},
                        {title:"species_category", field:"species_category_"},
                        {title:"species_data_quality", field:"species_data_quality_"},
                        {title:"species_population", field:"species_population_"},
                        {title:"species_conservation", field:"species_conservation_"},
                        {title:"species_isolation", field:"species_isolation_"},
                        {title:"species_global", field:"species_global_"},
                        {title:"Módosítandó adat típusa", field:"modositando_adat_tipusa_"},
                        {title:"Régi adat", field:"regi_adat_"},
                        {title:"Módosított adat", field:"modositott_adat_"},
                        {title:"Adatváltozás indoklása", field:"adatvaltozas_indoklasa_"},
                        {title:"TMF megjegyzés 1", field:"tmf_megjegyzes_1_"},
                        {title:"NPI válasz 1", field:"npi_valasz_1_"},
                        {title:"TMF megjegyzés 2", field:"tmf_megjegyzes_2_"},
                        {title:"NPI válasz 2", field:"npi_valasz_2_"},
                        {title:"Adatforrás", field:"adatforras_"},
                        {title:"Módszer", field:"modszer_"},
                        {title:"Megjegyzés", field:"megjegyzes_"},
                    ],
                });</script>';


                # Query data tables

                #
                # Loop on data tables

                #SELECT azonosito, Max(dt_to) as max_date,nev
                #FROM dinpi.dinpi LEFT JOIN vedett.sac ON (st_within(obm_geometry,geom))
                #WHERE azonosito LIKE 'HUDI%' AND faj_auto='Acrocephalus melanopogon' 
                #GROUP BY azonosito,nev
                #
                echo '<br><b>Utolsó megfigyelések N2000 területenként</b>';
                echo '<div id="last-observations"></div>';
                echo '<script>var table3 = new Tabulator("#last-observations", {
                    ajaxURL:"'.$protocol.'://'.URL.'/boat/ajax?m=natura2000&getLastObservations&species='.urlencode($species).'&data_table='.$data_table.'&date_column='.$date_column.'&species_column='.$species_column.'&NP_Code='.$NP_Code.'",
                    //data:tableData,
                    height:"311px",
                    columns:[
                        {title:"Azonosító", field:"azonosito"},
                        {title:"Név", field:"nev"},
                        {title:"Utolsó megfigyelés", field:"max_date"},
                    ],
                });</script>';


                # Create summaries
                # - by sites
                # - spatial query by
            }
        } else {

        }
    }

    // obligatory function
    public function deck_bodypage() {
    
    }

    public function profileItem() {

        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i>
                <a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=browseSites' class='profilelink'>".t('Területek')."</a><br>
            <i class='fa fa-external-link'></i>
                <a href='?profile&natura2000#tab_basic' data-url='/includes/profile.php?options=module_function&m=natura2000&params=browseSpecies' class='profilelink'>".t('Fajok')."</a>");

        $em .= "<tr><td>".wikilink('modules.html#computation-packages',t('str_what_is_natura2000'))."</td></tr>";

        return [
            'label' => 'Natura 2000',
            'fa' => 'fa-area-chart',
            'item' => $em
        ];
    }

    public function ajax($params, $request) {
        global $ID;
        if (array_key_exists('getLastObservations',$request)) {
            $species = urldecode($request['species']);
            $data_table = $request['data_table'];
            $species_column = $request['species_column'];
            $date_column = $request['date_column'];
            $NP_Code = $request['NP_Code'];
            # Query species occurrence data by sites of a species
            $cmd = sprintf('SELECT azonosito,nev,MAX(%7$s) as max_date
                    FROM %2$s d
                    LEFT JOIN natura2000.sac ON (st_within(obm_geometry,geom)) 
                    WHERE azonosito LIKE \'HU%5$s%6$s\' AND "%4$s"=%1$s 
                    GROUP BY azonosito,nev
                    ORDER BY azonosito,max_date',
                    quote($species),
                    $data_table,
                    $date_column,
                    $species_column,
                    $NP_Code,'%',
                    $date_column_2);

            if ($res = pg_query($ID, $cmd)) {
                $rows = pg_fetch_all($res);
                echo json_encode($rows);
            }
        }
    }
    
    public function adminPage($static_params,$dynamic_params) {

        if (!has_access('master')) return;

        return "<div>Admin page content of Natura2000</div>";
    }

    public function print_js($request) {
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        return "
    // Natura2000
    $(document).ready(function() {
        $('#tab_basic').on('change','#mdb-choose',function() {
            species = encodeURIComponent($(this).data('species'));
            np = $('option:selected',this ).text();
            var url = 'includes/profile.php?options=module_function&m=natura2000&params=browseSpecies,' + species + ',' + np;
            $('#sdf-mdb').load(projecturl+url, function(){});
        });
        /*$('#tab_basic').on('change','#mdb-choose-site',function() {
            site = encodeURIComponent($(this).data('site'));
            np = $('option:selected',this ).text();
            var url = 'includes/profile.php?options=module_function&m=natura2000&params=browseSites,' + site + ',' + np;
            $('#mdb-site1').load(projecturl+url, function(){});
        });*/

    });";
    } 
}
?>
