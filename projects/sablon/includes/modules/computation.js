var pos;
var caretPos;
var command = '';
var pollRetVal = '';

function pollRefresh(url) {
    
    var refreshID = setInterval( function() {

        $.ajax({
            type: 'GET',
            url: projecturl + url,
            dataType: 'json',
            success: function(data, textStatus) {
                var retval = jsendp(data);
                
                //Handle the return data (1 for refresh, 0 for no refresh)
                if(retval['status'] == 'success')
                {
                    if (pollRetVal == retval['data']) {
                        $('#tab_basic').find('.catbox').append('.');
                        
                    } else
                        $('#tab_basic').find('.catbox').append("\n" + retval['data']);

                    pollRetVal = retval['data'];
                } else {

                    $('#tab_basic').find('.catbox').append(retval['message']);
                    stopRefresh(refreshID);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                alert(errorThrown?errorThrown:xhr.status);
            }
        });
    }, (1000 * 2)); //Poll every seconds.
}

function stopRefresh(refreshID) {
    pollRetVal = '';
    clearInterval(refreshID);
}


$(document).ready(function() {

    $('#tab_basic').on('click','.getfile', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.post('ajax',{'m':'computation','loadFile':$(this).attr('href')},function(data){
            var w = window.open(href + '&filekey='+data, 'myWin', 'width = 500, height = 500');
            //$(w.document.body).html(data);
        });
        return false;

    });
    $('#tab_basic').on('mouseover','.jumplink',function(e){
        alert('Drop ??');
    });
    $('#tab_basic').on('click','.fileBrowser li',function(e){
        $(this).find('a').trigger('click');
    });
    $('#tab_basic').on('click','#addNewFile',function(e){

        $.post('ajax',{'m':'computation','addNewFile':$('#fileName').val(),'dest':$('#dest').text()},function(data){
            alert('ok');
        });
        
    });
    $('#tab_basic').on('click','#addNewDir',function(e){
        $.post('ajax',{'m':'computation','addNewDir':$('#fileName').val(),'dest':$('#dest').text()},function(data){
            alert('ok');
        });
        
    });
    $('#tab_basic').on('change','#addNewDir',function(e){
        $('#dest').text();
        
    });

    $('#tab_basic').on('change','#addPackageFile',function(e){
        //e.preventDefault();
        //var files = $(this).files;
        var files = document.getElementById('addPackageFile').files;
        var u = new Uploader;
        u.param('comp-package');
        u.param2($('#dest').text());
        u.file_upload(files);

    });
    /*$('#tab_basic').on('click','#savePackageFile',function(e){
        e.preventDefault();
        var content = $('#editPackageFileContent').find('#code').html();
        
    });*/

    $('#tab_basic').on('click','#sendPackage', async function(e){
        e.preventDefault();

        var url = $(this).attr("data-url");

        try {
         const pack = $(this).data('package');
         const data = await $.post('ajax',{
                'm':'computation',
                'sendPackage':pack
            });
            
            var retval = jsendp(data);
            if (retval['status']=='error' || retval['status']=='fail') {
                throw retval['message'];
            }
            if ($('#tab_basic').find('.catbox').length) {
                $('#tab_basic').find('.catbox').append(retval['data']);

            } else {
                $('#tab_basic').append("<div class='catbox'>" + retval['data'] + "</div>");
            }
            pollRefresh(url);
        }
        catch (err) {
            alert(err);
        }
    });

    $('#tab_basic').on('change','#addPackageFile',function(e){
        //e.preventDefault();
        //var files = $(this).files;
        var files = document.getElementById('addPackageFile').files;
        var u = new Uploader;
        u.param('comp-package');
        u.param2($('#dest').text());
        u.file_upload(files);

    });
    $('#tab_basic').on('click','#savePackageFile',function(e){
        e.preventDefault();
        var content = $('#editPackageFileContent').find('#code').html();
        var fileName = $('#editPackageFile').val();
        $.post('ajax',{'m':'computation','savePackageFile':fileName,'content':content},function(data){
            retval = JSON.parse(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
            } else if (retval['status']=='success') {
                alert('Ok');
            }
        });
    });

    $('#tab_basic').on('click','#createPackage',function(e){
        e.preventDefault();
        var name=$('#createPackageName').val();
        $.post('ajax',{'m':'computation','createPackage':name},function(data){
            retval = JSON.parse(data);
            if (retval['status']=='error') {
                alert(retval['message']);
            } else if (retval['status']=='fail') {
                alert(retval['message']);
            } else if (retval['status']=='success') {
                alert('Ok');
            }
        });
    });
    $('#tab_basic').on('click', 'ul.topnav li .to', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('.topnav').find('ul.subnav').hide();
        $('.topnav').find('li a.to').removeClass('to-t');

        if ($(this).parent().find('ul.subnav').css('display') == 'block') {
            $(this).parent().find('ul.subnav').slideUp('slow');
            return;
        }
        
        //Following events are applied to the subnav itself (moving subnav up and down)
        $(this).addClass('to-t');
        var w=$(this).parent().width();
        $(this).parent().find('ul.subnav').css({'right':'auto'});
        $(this).parent().find('ul.subnav').css({'min-width':w});
        $(this).parent().find('ul.subnav').slideDown('fast').show(); //Drop down the subnav on click
        $(this).parent().hover(
            function() { 
                $(this).addClass('subhover'); //On hover over, add class 'subhover'
            }, function(){	//On Hover Out
                //$(this).removeClass('subhover'); //On hover out, remove class 'subhover'
            });
    });
    $('#tab_basic').on('keydown', '#code', function(e) {
        if (e.keyCode === 13) {
            // Enter
            document.execCommand('insertHTML', false, '\n');
            var ln = $('#ln').text();
            var lnsplit = ln.split('\n');
            var n = lnsplit[lnsplit.length - 1];
            $('#ln').append('\n' + (parseInt(n,10)+1));

            return false; 
        }
        else if (e.keyCode === 27) {
            // ESC
            pos = getPos();
            document.getElementById('viline').focus();
        }
    });
    $('#tab_basic').on('keydown', '#viline', function(e) {
        if (e.keyCode === 13) {
            var l = $(this).text();
            if (l.substring(0,1) == ':') {
                if ( l.substring(1,2) == 'w') {
                    $('#savePackageFile').trigger('click');
                } 
                if ( l.substring(1,2) == 'q' || l.substring(2,3) == 'q') {
                    $('#goUp').trigger('click');
                }
                if ( l.substring(1,3) == '%s') {
                    // replace in all lines
                    var text = $('#code').text();
                    var p = l.split('/');
                    var u = text.replace(new RegExp(p[1],'gm'),p[2]);
                    $('#code').text(u);
                }
                else if ( l.substring(1,2) == 's') {
                    // replace in current line
                    var text = $('#code').text();
                    var lines = text.split('\n');
                    var currentLine = lines[pos.row ];
                    var p = l.split('/');
                    var u = currentLine.replace(new RegExp(p[1],p[3]),p[2]);
                    lines[pos.row] = u
                    $('#code').text(lines.join('\n'));
                }
            }
            $(this).html('');
            return false; 
        }
        else if (!command.length && e.keyCode === 68) {
            command = 'd'
            return false;
        } else if (command.length) {
            if (command == 'd') {
                if (e.keyCode === 68) {
                    var text = $('#code').text();
                    var lines = text.split('\n');
                    lines.splice(pos.row,1);
                    $('#code').text(lines.join('\n'));
                    command = '';
                }
            }
            return false;
        }
        else if (e.keyCode === 27) {
            command = '';
            //document.getElementById('code').focus();
            setCaret(pos);
        }
    });
});
function getPos() {
    var sel = document.getSelection(),
        nd = sel.anchorNode,
        text = nd.textContent.slice(0, sel.focusOffset);

    var line=text.split('\n').length - 1;
    var col=text.split('\n').pop().length;
    return {row:line, col:col }
}
function setCaret(pos) {
    var el = document.getElementById('code');
    var range = document.createRange();
    var sel = window.getSelection();
    var lines = $('#code').text().split('\n');
    var len = 0;
    for (i = 0; i < pos.row; i++) {
        len += lines[i].length;
        len++;
    }
    len += lines[i].length
    var d = len - lines[pos.row].length
    
    range.setStart(el.childNodes[0], d + pos.col);
    range.collapse(true);
    
    sel.removeAllRanges();
    sel.addRange(range);
}

