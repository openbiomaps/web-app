<?php
#' ---
#' Module:
#'   massive_edit
#' Files:
#'   [massive_edit.php]
#' Description: >
#'   Massive edit query result in file upload interface
#'   It has no valid hooks, It is deeply integrated into the main code, but has a module like interface to enable it.
#' Methods:
#'   [print_results_editor_button]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class massive_edit extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    function print_results_editor_button ($params) {
        
        $button = new button(["url"=>'upload/?massiveedit=1&mapselection',"title"=>t(str_edit_these_data),"icon"=>'fa fa-pencil']);
        $div = sprintf("<div style='width:600px'><span class='mtitle'><a name='view_options'>".t(str_edit_options)."</a></span><div class='options-box' style='display:none'>%s</div></div>",$button->print([]));
        return $div;

     }
}
?>
