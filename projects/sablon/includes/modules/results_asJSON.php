<?php
#' ---
#' Module:
#'   results_asJSON
#' Files:
#'   [results_asJSON.php]
#' Description: >
#'   Export results as JSON
#' Methods:
#'   [print_data, export_as]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asJSON extends module {
    public $error = '';
    public $retval;

    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_data($params,$pa) {
        global $ID,$BID, $modules;
        

        $de = new DataExport((int)$pa['de_id']);
        if (isset($de->filename)) {
            $filename = $de->filename;
        }
        $no_file = (isset($pa['no_file'])) ? true : false;

        $st_col = st_col($_SESSION['current_query_table'],'array');
        $data_rows = array();
        // default encoding
        
        $track_id = array();

        // visible columns
        extract(allowed_columns($modules,$_SESSION['current_query_table']));

        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;

        if (isset($_SESSION['orderby']))
            $orderby = sprintf('ORDER BY %1$s %2$s',$_SESSION['orderby'],$_SESSION['orderascd']);
        else
            $orderby = 'ORDER BY obm_id';

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s %3$s',constant('PROJECTTABLE'),session_id(),$orderby));
        $has_master_access = has_access('master');

        while($line = pg_fetch_assoc($res)) {
            //add meta columns
            $cell = array();
            // ski0pping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($line)==1) {
                if ($line[0]==$chk) continue;
                else $chk = $line[0];
            }
            
            $acc = rst('acc',$line['obm_id'],$_SESSION['current_query_table'], $has_master_access); 
            extract(data_access_check($acc,$st_col['USE_RULES'],$allowed_cols,$tgroups,$allowed_cols_module,$line));

            // print cells
            foreach ($de->colnames as $k=>$v) {

                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = 'NA';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = 'NA';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = 'NA';
                    }
                }

                if (isset($line[$k]))
                    $cl = $line[$k];
                else
                    $cl = "";

                $cell[$k] = $cl;
            }
            // ez a tömb  túl nagyra nőhet - ki lehet kerülni folyamatos printteléssel
            $data_rows[] = $cell;
            // ez a tömb is túl nagyra nőhet!!
            $track_id[] = $line['obm_id'];
        }
        track_download($track_id);

        // direct print 
        if ($no_file) {
            header('Content-type:application/json;charset=utf-8');
            print json_encode($data_rows);
        } else {
            // file print
            $filesize = mb_strlen(json_encode($data_rows), '8bit');

            # header definíció!!!
            header("Content-Type: application/json");
            header("Content-Length: $filesize"); 
            header("Content-Disposition: attachment; filename=\"$filename\";");
            header("Expires: -1");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            print json_encode($data_rows);
        }

        return;
    }
}
?>
