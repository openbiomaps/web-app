<?php
class custom_module_example {

    public function getMenuItem() {
        return ['label' => 'Custom module example','url' => 'custom_module_example'];
    }

    public function getProfileSection() {
        return [
            'label' => 'Custom module example',
            'fa' => 'fa-download',
            'href' => '#tab_basic',
            'data_url' => 'includes/project_admin.php?options=custom_module_example',
            'help' => "<i class='fa fa-lg fa-question-circle-o'></i> <a href='#' class='faq'>What are the custom modules?</a>"
        ];
    }

    public function adminPage() {
        return "<div>Custom module admin page</div>";
    }
}
?>
