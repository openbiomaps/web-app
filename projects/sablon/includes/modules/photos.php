<?php
#' ---
#' Module:
#'   photos
#' Files:
#'   [photos.php, photos.js, photos.css]
#' Description: >
#'   Photo or other file attchements uploader and viewer extension
#' Methods:
#'   [init, add_attachments, print_js, modal_dialog, print_css, view_attachment]
#' Module-type:
#'   project
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class photos extends module {
    
    public $strings = ["str_accuracy", "str_yes_i_want", "str_no_thanks", "str_set_coordinates_from", "str_invalid_filename_alert"];
    var $error = '';
    var $retval;
    
    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        $this->params = $this->split_params($params);
        $this->ajax_echo_common_message = true;
        
        if (method_exists($this, $action)) {
          $this->retval = $this->$action($params,$pa);
        }

    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    /* Dummy function for testing: - is there any photo module? */
    public function view_attachment () {
        return true;
    }
    public function print_js ($params) {
        $js = file_get_contents(getenv('OB_LIB_DIR') . 'modules/photos.js');
        if (isset($_SESSION['st_col']))
            $js .= "var geom_col = '{$_SESSION['st_col']['GEOM_C']}';";
        else
            $js .= "var geom_col = 'obm_geometry';";
        
        return $js;
    }

    /** 
    * upload file modal dialog:
    * attachment upload dialog
    * change color of buttons
    * triggering upload event
    **/
    public function add_attachments ($params, $pa) {
        ob_start();
        ?>
        <div id='photo_upload_div' class='fix-modal'>
            <h2><?= t(str_upload) ?></h2>
            <button class='pure-button button-xlarge fix-modal-close' id='pud-close'>
                <i class='fa fa-close'></i>
            </button>
            <form method='post' enctype='multipart/form-data' class='pure-form'>
                <input type='file' id='photo_upload-file' name='image_files[]' multiple='multiple' class='pure-button button-secondary'>
                <br><br>
                <button id='photo_upload' style='display:none' class='button-xlarge pure-button'>
                    <i class='fa fa-upload'></i><?= t(str_upload) ?>
                </button>
            </form>
            <div id='uresponse'></div>
            <button id='photo_anchor' class='button-xlarge pure-button' disabled>
                <i class='fa fa-anchor'></i><?= t(str_set) ?>
            </button>
        </div>
        <?php
        return ob_get_clean();
    }

    public function modal_dialog($params,$pa) {
        return $this->photo_viewer($params,$pa);
    }

    // photo/attachment viewer
    function photo_viewer($params,$pa) {
        $def = $pa[0];
        if ($def == 'upload') {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_delete.'"><i class="fa fa-trash fa-lg"></i></button>';
            $download_btn = '';
        } else {
            $drop_btn = '<button class="pure-button button-error button-large p-delete" title="'.str_unlink_file_from_data.'"><i class="fa fa-unlink fa-lg"></i></button>';
            $download_btn = '<button class="pure-button button-success button-large p-download" title="'.str_download_full_size.'"><i class="fa fa-download fa-lg"></i></button>';
        }
        ob_start();
        ?>

        <div id="photodiv">
            <div id="photodiv-carpet"></div>
            <div id="photodiv-buttons">
                <?= $download_btn ?>
                <button class="pure-button button-secondary button-large p-expand image_manip"><i class="fa fa-arrows-alt fa-lg"></i></button>
                <button class="pure-button button-secondary button-large p-exif" >Info</button>
                <button class="pure-button button-gray button-large p-save" ><i class="fa fa-floppy-o fa-lg"></i></button>
                <?= $drop_btn ?>
                <button class="pure-button button-secondary button-large p-rotate-left image_manip"><i class="fa fa-rotate-left fa-lg"></i></button>
                <button class="pure-button button-secondary button-large p-rotate-right image_manip"><i class="fa fa-rotate-right fa-lg"></i></button>
                <button class="pure-button button-secondary button-large p-close"><i class="fa fa-times fa-lg"></i></button>
            </div>
            <ul id="photodiv-exif"></ul>
            <div id="photodiv-frame">
                <div id="photodiv-rightpanel">
                    <div id="photodiv-data"></div>
                    <div id="photodiv-thumbnails"></div>
                </div>
                <textarea id="photodiv-comment"></textarea>
                <audio id="photodiv-audio" controls="controls"><source src="" type="audio/x-wav" /></audio>
            </div>
        </div>


        <?php
                
        return ob_get_clean();
    }

    public function init($params, $pa) {

        global $ID;

        $table = (isset($pa['mtable'])) ? $pa['mtable'] : PROJECTTABLE;
        $schema = (isset($pa['schema'])) ? $pa['schema'] : 'public';    // not used yet, so it is always public
        //checking trigger existence
        //
        $cmd = sprintf('SELECT EXISTS ( 
                            SELECT 1 FROM information_schema.triggers WHERE trigger_schema = \'%2$s\' AND trigger_name = \'file_connection_%2$s_%3$s_%1$s\' AND event_object_table = \'%1$s\'
                        );', $table,$schema,PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE TRIGGER file_connection_%2$s_%3$s_%1$s AFTER INSERT ON "%2$s"."%1$s" FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();',$table,$schema,PROJECTTABLE);
        }
        else {
            if ($schema == 'public') {
                $tgrelid = $table;
            } else {
                $tgrelid = $schema.'.'.$table;
            }

            $cmd = sprintf('SELECT tgenabled FROM pg_catalog.pg_trigger WHERE NOT tgisinternal AND tgrelid = \'%2$s\'::regclass and tgname = \'file_connection_%4$s_%3$s_%1$s\';', $table,$tgrelid,PROJECTTABLE,$schema);
            if( $res = query($ID,$cmd)) {
                $result = pg_fetch_assoc($res[0]);
                if ($result['tgenabled'] == 'D') {
                    $cmd1[] = sprintf('ALTER TABLE "%2$s"."%1$s" ENABLE TRIGGER file_connection_%2$s_%3$s_%1$s;',$table,$schema,PROJECTTABLE);
                }
            }
        }

        if (!empty($cmd1)) {

            return query($ID,$cmd1);
        }

        return false;
    }
    
    /* Unlink files and delete record from system.files & system.file_connect tables
    *
    * */
    public function photomanage_delete($params, $request) {
        try {
            $file_id = (int)$_POST['photomanage'];
            if (!$file = File::first(['id', $file_id])) {
                throw new \Exception("File not found", 1);
            }
            
            $file->deleteConnection('all');
            $file->delete();
            
            return;
            
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return;
        }
    }
    
    /**
    * Remove file connections
    * */
    public function photomanage_remove_connections($params, $request) {
        try {
            $reference = $request['photomanage'];
            $file = File::first([
                ['reference', $reference],
                ['project_table', PROJECTTABLE]
            ]);
            if (!$file) {
                throw new \Exception("File not found", 1);
            }
            
            $file->deleteConnection('all');
            
            return;
            
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return;
        }
    }
    
    /**
    * update comment of a file in system.files
    */
    public function photomanage_add_comment($params, $request) {
        try {
            $reference = $request['photomanage'];
            
            $file = File::first([
                ['reference', $reference],
                ['project_table', PROJECTTABLE]
            ]);
            
            if (!$file) {
                throw new \Exception("File not found", 1);
            }
            
            $file->comment = $request['comment'] ?? "";
            $file->save();
            
            return;
            
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return;
        }
    }
    
    /** 
    * Update data-table attribute in system.files
    * */
    public function photomanage_updatetable($params, $request) {
        try {
            if (!has_access('master')) {
                throw new \Exception(t('str_access_denied'), 1);
            }
            if (!in_array($request['tablelink'], getProjectTables())) {
                throw new \Exception('Unknown table name provided.', 1);
            }
            $file_id = (int)$request['photomanage'];
            
            if (!$file = File::first(['id', $file_id])) {
                throw new \Exception("File not found", 1);
            }
            
            $connections = $file->getConnections();
            if (count($connections)) {
                foreach ($connections as $con) {
                    $con->delete();
                }
            }
            
            $file->data_table = $request['tablelink'];
            $file->save();
            
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
        }
    }
    
    
    /** Not used function **/
    public function photomanage_addtable($params, $request) {
        /* Insert new record into system.files
        *
        * */
        
        if (!isset($_SESSION['Tid'])) {
            echo common_message('error',str_access_denied);
            exit;
        }
        
        $ref = $_POST['photomanage'];
        
        if ($_POST['tablelink']!='') {
            
            $root_dir = getenv('PROJECT_DIR').'local/attached_files';
            $sum = sha1_file("$root_dir/$ref");
            $finfo = new finfo(FILEINFO_MIME);
            $mimetype = $finfo->file("$root_dir/$ref");
            
            // exif creation
            if (in_array(exif_imagetype("$root_dir/$ref"),array(IMAGETYPE_JPEG ,IMAGETYPE_TIFF_II , IMAGETYPE_TIFF_MM))) {
                $exif_read = exif_read_data("$root_dir/$ref", 0, false);
                if (json_encode($exif_read)) {
                    $exif = $exif_read;
                } else
                $exif = array($exif_read['FileName'],$exif_read['FileDateTime'],$exif_read['FileSize'],$exif_read['FileType'],$exif_read['MimeType'],$exif_read['Model'],'corrupted exif...');
            } else {
                $a = exec("mediainfo $root_dir/$ref",$output);
                $n = array();
                foreach ($output as $o) {
                    $on = preg_split("/\s+: /",$o);
                    if(count($on)>1) {
                        if($on[0]=='Complete name') {
                            $on[1] = basename($on[1]);
                        }
                        $n[$on[0]] = $on[1];
                    }
                }
                $exif = $n;
            }
            $filetime = filemtime("$root_dir/$ref");
            
            
            $cmd = sprintf("INSERT INTO system.files (project_table,reference,datum,access,user_id,status,sessionid,sum,mimetype,data_table,exif)
            VALUES('%s',%s,%s,0,%d,%s,'%s',%s,%s,%s,%s) RETURNING id",PROJECTTABLE,quote($ref),quote(date ("Y-m-d H:i:s", $filetime)),0,quote('manual'),0,quote($sum),quote($mimetype),quote($_POST['tablelink']),quote(json_encode($exif)));
            
            $res = pg_query($GID,$cmd);
            if (pg_affected_rows($res))
            echo common_message('ok','ok');
            else {
                $errorID = uniqid();
                echo common_message('failed',"Error. ErrorID#$errorID#");
                log_action("ErrorID#$errorID# ".pg_last_error($GID),__FILE__,__LINE__);
            }
        } else {
            echo common_message('error',"No table name provided");
            
        }
        
    }
    
    public function photomanage_updateconnection($params, $request) {
        try {
            if (!isset($_SESSION['Tid'])) {
                throw new \Exception(t('str_access_denied'));
            }
            
            $new_ids = $request['new_ids'] ?? [];
            $orig_ids = $request['orig_ids'] ?? [];
            
            $file_id = (int)$_POST['photomanage'];
            
            if (!$file = File::first(['id', $file_id])) {
                throw new \Exception("File not found", 1);
            }
            
            $file->comment = $request['comment'];
            $file->save();
            
            $to_add = array_values(array_diff($new_ids, $orig_ids));
            foreach ($to_add as $row_id) {
                $file->addConnection($row_id);
            }
            
            $to_delete = array_values(array_diff($orig_ids, $new_ids));
            foreach ($to_delete as $row_id) {
                $file->deleteConnection($row_id);
            }
            
            return;
            
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return;
        }
    }
    
    public function photomanage_rotate($params, $request) {
        
        $root_dir = getenv('PROJECT_DIR').'local/attached_files';
        echo common_message('ok','ok');
        $fname = $root_dir.'/'.$_POST['photomanage'];
        
        $this->rotateImage($fname,$_POST['degrees']);
        
        #chdir("$root_dir/thumbnails/");
        #$list = glob("*{".basename($_POST['photomanage'])."}", GLOB_BRACE);
        #foreach($list as $s) {
        #    rotateImage($root_dir.'/thumbnails/'.$s,$_POST['degrees']); 
        #}    
        chdir("$root_dir/thumbnails/");
        $list = glob("*{".basename($fname)."}", GLOB_BRACE);
        foreach($list as $s) {
            unlink($s);
        }
    }
    
    private function rotateImage($filename, $degrees = 90) {

        $source = open_image($filename);
        
        // Rotate
        $rotate = imagerotate($source, $degrees, 0) or log_action("image rotate failed", __FILE__, __LINE__);
        
        // Output
        
        if (!$out = save_image($rotate, $filename)) {
            $this->error = "image rotate failed";
            return;
        }
        
        // Free the memory
        imagedestroy($source);
        imagedestroy($rotate);
    }

    public function get_thumbnails($params, $conid) {
        global $ID, $protocol;
        $cmd = sprintf("SELECT id,reference,substring(comment,1,12) as comment FROM system.files LEFT JOIN system.file_connect ON (files.id=file_connect.file_id) WHERE conid='%s' AND status!='deleted'", $conid);
        if (!$res = pg_query($ID,$cmd)) {
            throw new \Exception("Error Processing Query", 1);
        }
        
        $p = array();
        if (pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {
                if ($thumb = mkThumb($row['reference'],60)) {
                    $thf = "$protocol://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                    $p[] = "<a href='$protocol://".URL."/getphoto?c={$conid}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$thf' title='{$row['comment']}' class='thumb'></a>";
                } else {
                    $mime_url = mime_icon($row['reference'],32);
                    $p[] = "<a href='$protocol://".URL."/getphoto?c={$conid}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'><img src='$mime_url' title='{$row['comment']}' class='thumb'></a>";
                }
            }
            return implode(" ",$p);
        }
        return "";
    }
}
?>
