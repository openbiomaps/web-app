$(document).ready(function() {
    // coordinate click on result slides
    // mark point on the map
    // transform_data module
    // module-ba áttenni!!!
    $('body').on('click',".coord_query",function(e) {
        
        e.preventDefault();
        
        // track data view
        if ($(this).attr('id')) {
            $.post("ajax", {track_data_view:$(this).attr('id')},function(data){});
        }
        
        // get and prepare coordinates, expecting geojson: {"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
        const coords = $(this).attr('alt').replace(/\\/g,'');
        const geojson_format = new ol.format.GeoJSON();
        const feature = geojson_format.readFeatures(coords);
        const extent = feature[0].getGeometry().getExtent();
        const marker = new ol.proj.fromLonLat(ol.extent.getCenter(extent));
        
        // set map center
        if (ol.extent.getArea(extent) === 0) {
            map.getView().setCenter(marker)
        }
        else {
            map.getView().fit(ol.proj.transformExtent(extent, 'EPSG:4326', 'EPSG:3857'))
        }

        // add marker
        markerLayer.getSource().clear();
        const iconFeature = new ol.Feature({
            geometry: new ol.geom.Point(marker),
        });
        iconFeature.setStyle(
            new ol.style.Style({
                image: new ol.style.Icon({
                    crossOrigin: "anonymus",
                    imgSize: [48, 48],
                    src: 'js/img/map-marker-pink.png',
                    displacement: [0,24]
                })
            })
        )
        markerLayer.getSource().addFeature(iconFeature)

        // scroll to map
        $("html, body").animate({ scrollTop: 0 }, "slow");
        
    });
});
