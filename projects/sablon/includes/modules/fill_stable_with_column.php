<?php
/* Asynchron cml script
 *
 *
 * */
#$logfile = 'background_process_debug.log';
#file_put_contents($logfile, '5');
/*$vars = json_decode('{
    "allowed_cols_module": true,
    "stable_header": {
        "uploader_name": "felt\u00f6lt\u0151",
        "adatkozlo": "adatk\u00f6zl\u0151",
        "szamossag": "szamossag",
        "magyar": "magyar",
        "meret_kb": "meret_kb",
        "egyedszam": "egyedszam",
        "faj": "faj",
        "obm_geometry": "obm_geometry",
        "dt_to": "d\u00e1tum",
        "obm_files_id": "obm_files_id"
    },
    "protocol": "http",
    "session": {
        "orderby": "faj",
        "orderad": 1,
        "orderascd": "ASC",
        "ignore_joined_tables": 0,
        "current_query_table": "dinpi",
        "st_col": {
            "GEOM_C": "obm_geometry",
            "SPECIES_C": "faj",
            "SPECIES_C_SCI": "faj",
            "NUM_IND_C": "egyedszam",
            "ID_C": "",
            "X_C": "eov_x",
            "Y_C": "eov_y",
            "UTMZONE_C": "utm_zone",
            "SRID_C": "4326",
            "ORDER": {"dinpi":{"adatkozlo":"1","szamossag":"2","gyujto":"3","magyar":"4","eov_x":"5","hely":"5","eov_y":"6","meret_kb":"8","egyedszam":"9","him":"10","nosteny":"11","eloford_al":"12","gyujto2":"13","gyujto3":"14","hatarozo":"15","modsz":"16","elohely":"17","veszteny":"18","megj":"19","rogz_modsz":"20","faj":"21","obm_geometry":"22","natura2000":"23","vedettseg":"24","tema":"25","obm_datum":"26","dt_from":"27","vf_natura2000":"28","dt_to":"28","obm_files_id":"29","vf_vedettseg":"29","altema":"31","megjegyzes":"33","megfigyelo":"34","tsz_magassag":"35"}},
            "CITE_C": [
                "gyujto",
                "megfigyelo"
            ],
            "DATE_C": [
                "obm_datum"
            ],
            "RULES_WORKS": true,
            "ALTERN_C": [
                "magyar"
            ],
            "NATIONAL_C": {
                "hu": "magyar"
            },
            "USE_RULES": true,
            "FILE_ID_C": "dinpi"
        },
        "modules": {
            "main_table": "dinpi",
            "modules_file": "table_modules.php"
        },
        "LANG": "hu"
    },
    "tgroups": 0,
    "allowed_cols": [
        "faj",
        "obm_id",
        "uploader_name",
        "uploading_date",
        "uploading_id"
    ],
    "allowed_gcols": [
        "faj",
        "obm_id",
        "uploader_name",
        "uploading_date",
        "uploading_id"
    ],
    "MASTER": false,
    "SERVER": {
        "PROJECT_DIR": "/var/www/html/biomaps/resources/",
        "OB_LIB_DIR": "/var/www/html/biomaps/resources/includes/",
        "HTTP_HOST": "localhost",
        "HTTP_USER_AGENT": "Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0",
        "HTTP_ACCEPT": "*\/*",
        "HTTP_ACCEPT_LANGUAGE": "hu,en;q=0.8,en-US;q=0.6,es;q=0.4,ro;q=0.2",
        "HTTP_ACCEPT_ENCODING": "gzip, deflate, br",
        "HTTP_REFERER": "http://localhost/biomaps/resources/map/",
        "CONTENT_TYPE": "application/x-www-form-urlencoded; charset=UTF-8",
        "HTTP_X_REQUESTED_WITH": "XMLHttpRequest",
        "CONTENT_LENGTH": "33",
        "HTTP_ORIGIN": "http://localhost",
        "HTTP_CONNECTION": "keep-alive",
        "HTTP_COOKIE": "lastZoom=%7B%22center%22%3A%5B2466502.9305489487%2C6444132.056551708%5D%2C%22zoom%22%3A5.636233912653572%7D; PHPSESSID=12eqj73epq0i7r4qvjslfon04t; PPA_ID=3o4urbkcunbhm5ibjed9rq4s34; webfx-tree-cookie-persistence=wfxt-272+wfxt-51+wfxt-69+wfxt-8+wfxt-43+wfxt-23+wfxt-21+wfxt-19+wfxt-4+wfxt-10+wfxt-35+wfxt-37+wfxt-39+wfxt-11+wfxt-237+wfxt-16+wfxt-61+wfxt-89+wfxt-26+wfxt-36+wfxt-40+wfxt-68; LoadCookie=1",
        "HTTP_SEC_FETCH_DEST": "empty",
        "HTTP_SEC_FETCH_MODE": "cors",
        "HTTP_SEC_FETCH_SITE": "same-origin",
        "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
        "SERVER_SIGNATURE": "<address>Apache/2.4.54 (Debian) Server at localhost Port 80</address>n",
        "SERVER_SOFTWARE": "Apache/2.4.54 (Debian)",
        "SERVER_NAME": "localhost",
        "SERVER_ADDR": "127.0.0.1",
        "SERVER_PORT": "80",
        "REMOTE_ADDR": "127.0.0.1",
        "DOCUMENT_ROOT": "/var/www/html",
        "REQUEST_SCHEME": "http",
        "CONTEXT_PREFIX": "",
        "CONTEXT_DOCUMENT_ROOT": "/var/www/html",
        "SERVER_ADMIN": "webmaster@localhost",
        "SCRIPT_FILENAME": "/var/www/html/biomaps/resources/includes/load_results.php",
        "REMOTE_PORT": "45066",
        "GATEWAY_INTERFACE": "CGI/1.1",
        "SERVER_PROTOCOL": "HTTP/1.1",
        "REQUEST_METHOD": "POST",
        "QUERY_STRING": "",
        "REQUEST_URI": "/biomaps/resources/includes/load_results.php",
        "SCRIPT_NAME": "/biomaps/resources/includes/load_results.php",
        "PHP_SELF": "/biomaps/resources/includes/load_results.php",
        "REQUEST_TIME_FLOAT": 1682412497.517712,
        "REQUEST_TIME": 1682412497
    }
}',true);
*/

session_start();
$session_id = $argv[1];
$vars1 = getenv("allvars_$session_id");
putenv("allvars_$session_id");
$vars = json_decode(base64_decode($vars1),true); //session helyreállítása

$_SERVER = $vars['SERVER'];

$local_lib_dir = __DIR__;
$local_lib_dir_path = explode('/',$local_lib_dir);
array_pop($local_lib_dir_path);
$local_lib_dir = implode('/',$local_lib_dir_path);
array_pop($local_lib_dir_path);
$local_lib_dir_path = implode('/',$local_lib_dir_path);
$_SESSION = $vars['session'];
putenv("PROJECT_DIR=$local_lib_dir_path/");
putenv("OB_LIB_DIR=$local_lib_dir/");
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS databases.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');


#debug($vars);
#debug($session_id);

$modules->_include('results_asStable','fill_stable_with_columns',[$vars,$session_id,0]);

?>
