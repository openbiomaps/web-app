<?php
#' ---
#' Module:
#'   box_custom
#' Files:
#'   [box_custom.php]
#' Description: >
#'   Custom box - only user defined version exists
#' Methods:
#'   [print_box, print_js, query_button]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class box_custom extends module {

    public $path;
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function set_path ($path,$dynamic_params) {
        $this->path = $path;
    }

    public function print_box ($p,$dynamic_params=null) {
        $params = $this->split_params($p);
        $pmi = array();
        $boxes = "";

        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);

            if(file_exists($this->path."$c[0].php")) {
                $e = '';
                if ( php_check_syntax($this->path."$c[0].php",$e)) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    $boxes .= $bc->print_box($args);
                }
                if (!preg_match('/^No syntax errors detected/',$e))
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
            }
        }
        $this->retval = $boxes;
    }

    public function print_js ($p) {

        $params = $this->split_params($p);

        $pmi = array();
        $js = "";
        foreach($params as $pm) {
            $c = preg_split('/:/',$pm);
            if(file_exists($this->path."$c[0].php")) {
                $e = '';
                if ( php_check_syntax($this->path."$c[0].php",$e)) {
                    include_once($this->path."$c[0].php");
                    $classname=$c[0].'_Class';
                    $bc = new $classname();
                    $args = isset($c1) ? $c[1] : "";
                    $js .= $bc->print_js($args);
                }
                if (!preg_match('/^No syntax errors detected/',$e))
                    log_action(str_module_include_error.': '.$e,__FILE__,__LINE__);
            }
        }
        $this->retval = $js;
    }
}
?>
