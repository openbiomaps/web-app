<?php
#' ---
#' Module:
#'   validation
#' Files:
#'   [validation.php, validation.js, validation.sql]
#' Description: >
#'   Internal API for validation algorithms
#' Methods:
#'   [getMenuItem, adminPage, getMailText, displayError, ajax, print_js, init, external_dependencies]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class validation extends module {
    var $error = '';
    var $retval;
    var $params;
    public $ajax_deps = ['taxon'];
    public $external_dependencies = [
        '<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" media="print" onload="this.media=\'all\'; this.onload=null;" />',
        '<script async defer src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>',
        '<script async defer src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js" integrity="sha512-ozq8xQKq6urvuU6jNgkfqAmT7jKN2XumbrX1JiB3TnF7tI48DPI4Gy1GXKD/V3EExgAs1V+pRO7vwtS1LHg0Gw==" crossorigin="anonymous"></script>',
        '<script async defer src="https://unpkg.com/@turf/turf@6.3.0/turf.min.js"></script>',
        '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw-src.css" integrity="sha512-vJfMKRRm4c4UupyPwGUZI8U651mSzbmmPgR3sdE3LcwBPsdGeARvUM5EcSTg34DK8YIRiIo+oJwNfZPMKEQyug==" crossorigin="anonymous" media="print" onload="this.media=\'all\'; this.onload=null;"  />'

    ];
    private $relations = ['=', '>', '>=', '<', '<=', '!=', 'BETWEEN', 'DATE_BETWEEN', 'IN', 'IN_GRID', 'IS NULL'];

    function __construct($action = null, $params = null,$pa = array(), $main_table = null) {
        global $BID;
        
        if (is_array($params)) {
            $this->params = $params;
        }
        else {
            $this->params = $this->split_params($params);
        }
        if ($main_table)
            $this->main_table = $main_table;      
        
        if ($action) {
            $this->retval = $this->$action($this->params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function getMenuItem() {
        return ['label' => 'Validation', 'url' => 'validation' ];
    }

    public function adminPage($params) {
        return '<button class="module_submenu button-success pure-button" data-url="includes/project_admin.php?options=validation"> <i class="fa fa-refresh"></i> </button>' .
         "<div id='validation_rules'></div>";
    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }
    
    public function init($params, $pa) {
        log_action('validation init running', __FILE__,__LINE__);
        global $ID;

        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_validation_rules');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf(getSQL('create_validation_rules_table', 'validation'), PROJECTTABLE );

            if (!empty($cmd1) && !query($ID,$cmd1)) {
                $this->error = 'validation init failed: table creation error';
                return array('error'=>$this->error);
            }

        }
        return;
    }
    
    public function settings($params, $pa) {
        global $modules;
        
        // in the first phase this will work only for the default table 
        $table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE;
        
        $columns = getColumnsOfTables($table);
        if ($modules->is_enabled('grid_view')) {
            $columns = array_merge($columns, getColumnsOfTables(PROJECTTABLE . '_qgrids'));
        }
        
        $params = $this->get_params();
        $relations = $this->relations;
        
        return compact("table", "columns", "params", "relations"); 
    }
    
    public function load_rules($params, $request) {
        if (!isset($request['taxon_id']) || !is_numeric($request['taxon_id'])) {
            $this->error = 'Invalid request';
            return;
        }
        global $ID;
        
        $cmd = sprintf("SELECT * FROM %s_validation_rules WHERE taxon_id = %d ORDER BY id;", PROJECTTABLE, $request['taxon_id']);
        
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = 'query error';
            return;
        }
        $results = pg_fetch_all($res);
        return ($results) ? $results : [];
    }
    
    public function save_rule($params, $request) {
        $vr = new ValidationRule($request['rule']);
        return ($vr->save()) ? common_message('ok','ok') : common_message('error', 'save failed');
    }
    
    public function delete_rule($params, $request) {
        $vr = new ValidationRule($request['rule']);
        return ($vr->delete()) ? common_message('ok','ok') : common_message('error', 'delete failed');
    }
    
}

/**
 * 
 */
class ValidationRule {
    
    private $props = [ 'id', 'taxon_id', 'target', 'col', 'relation', 'val', 'match_value', 'else_value' ];
    
    function __construct($args) {
        foreach ($this->props as $prop) {
            $this->$prop = $args[$prop] ?? 'NULL';
        }
    }
    
    public function save() {
        global $ID;
        if ($this->id !== 'NULL') {
            $cmd = sprintf("UPDATE %s_validation_rules SET target = %s, col = %s, relation = %s, val = %s, match_value = %s, else_value = %s WHERE id = %s;",
                PROJECTTABLE, quote($this->target), quote($this->col), quote($this->relation), quote($this->val), quote($this->match_value), quote($this->else_value), quote($this->id)
            );
        }
        else {
            $cmd = sprintf("INSERT INTO %s_validation_rules (taxon_id, target, col, relation, val, match_value, else_value) VALUES (%s, %s, %s, %s, %s, %s, %s);",
                PROJECTTABLE, quote($this->taxon_id), quote($this->target), quote($this->col), quote($this->relation), quote($this->val), quote($this->match_value), quote($this->else_value),
            );
        }
        if (!$res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($res), __FILE__, __LINE__);
            return false;
        }
        return true;
    }
    
    public function delete() {
        if ($this->id === 'NULL') {
            return false;
        }
        global $ID;
        $cmd = sprintf("DELETE FROM %s_validation_rules WHERE id = %s;", PROJECTTABLE, quote($this->id));
        if (!$res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($res), __FILE__, __LINE__);
            return false;
        }
        return true;
    }
}

?>
