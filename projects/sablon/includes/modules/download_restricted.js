$(document).ready(async function() {
    let resp = await $.getJSON('ajax', { m: 'download_restricted', action: 'translations' })
    
    if (resp.status != 'success') {
      console.log(new Error(resp.message));
    }
    t['download_restricted'] = resp.data;
    
    tr = t.download_restricted
    
    const dialogButtons = {}
    dialogButtons[tr.str_submit] = submitDlr;
    dialogButtons[tr.str_cancel] = function() {
        dlrDialog.dialog( "close" );
    };
    const dlrDialog = $( "#dlr-form" ).dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: dialogButtons,
        close: function() {
            form[ 0 ].reset();
        }
    });
    
    const form = dlrDialog.submit(function( event ) {
        event.preventDefault();
        submitDlr()
    });
    
    $("#dlr-form").on("openDlrForm",function(ev){
        $("#dlr-form").data('href', ev.href)
        dlrDialog.dialog( "open" );
    });
    
    async function submitDlr() {
        try {
            var message = $('#dlr-message').val();
            if (message === '') {
                $('#dlr-message').css('border-color', 'red')
                return
            }
            const ret = JSON.parse(await queryExport(
                new URL($("#dlr-form").data('href')),
                message
            ));
            
            dlrDialog.dialog("close")
            
            if (ret.status !== 'success') {
                throw new Error(ret.message)
            }
            let txt = tr.str_download_request_sent
            
            $( "#dialog" ).html(txt);
            $( "#dialog" ).dialog( "open" ); 
            
        } catch (e) {
            dlrDialog.find('.error-message').html(e)
        } 
    }
    
    $("#tab_basic").on("click",".reject, .approve",async function(ev){
        ev.preventDefault();
        let response = ""
        try {
            let action = "";
            if ($(this).hasClass("approve")) {
                action = "approve"
            }
            if ($(this).hasClass("reject")) {
                action = "reject"
            }
            
            if (action !== "") {
                const id = $(this).data("request_id");
                const data = await $.post("ajax", { 
                    m: "download_restricted",
                    action: action, 
                    id: id 
                });
                
                const retval = JSON.parse(data);
                if (retval.status !== "success") {
                    throw retval.message
                }
                response = retval.data
            }
            else {
                throw "unsupported action"
            }
        }
        catch (error) {
            response = `ERROR: ${error}`
        }
        finally {
            $( "#dialog" ).text(response);
            var isOpen = $( "#dialog" ).dialog( "isOpen" );
            if(!isOpen) $( "#dialog" ).dialog( "open" ); 
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=results_buttons&mtable=' + obj.project);
        }
    });
    
});
