<?php
#' ---
#' Module:
#'   computation
#' Files:
#'   [computation.php]
#' Description: >
#'   Computation configuration interface
#' Methods:
#'   [print_js, package_send, script_browser, profileItem, profilePage, adminPage, getMenuItem, ajax, deck_bodypage]
#' Module-type:
#'   project
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class computation extends module {
    
    public $error = '';
    public $project = PROJECTTABLE;
    public $package = '';
    public $message = '';
    
    function __construct($action = null, $params = null, $pa = array()) {
        global $BID;

        $this->params = $this->split_params($params);

        if ($action)
            $this->retval = $this->$action($params,$pa);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function createPackage($package,$method=null) {
        if ($res = chdir(getenv('PROJECT_DIR')) )
            if (!$res = file_exists("computational_packages"))
                $res = mkdir("computational_packages");

        $this->message = "";

        if ($res) {
            chdir("computational_packages");

            $exists = file_exists($package);
            if ($method == 'pull') {
                chdir($package);
                $res = exec("git config --get remote.origin.url");
                $package = trim($res);
                chdir("../");
            }

            //if (!file_exists($package)) {

                if (preg_match("|\.git$|",$package)) {
                    #exec("sudo -u gitpull-user /var/www/html/biomaps/git-pull.sh $package dhte-mmm");
                    # git@gitlab.com:bonegabor/dhte-mmm.git
                    # https://gitlab.com/bonegabor/dhte-mmm.git

                    if (preg_match('/([a-z0-9-_]+)\.git$/i',$package,$m)) {
                        $name = $m[1];
                        $exists = file_exists($name);
                    }

                    $gitmethod = ($exists) ? 'pull' : 'clone';

                    if (preg_match('|^https?|',$package)) {
                        $p = parse_url($package);
                        if (!isset($p['user']) or !isset($p['pass']) ) {
                            $this->message = "Give your username and password at the following format:\n".$p['scheme']."://user:passw@".$p['host'].$p['path'];
                            return false;
                        }
                        
                        if ($gitmethod == 'clone')
                            $res = exec("git clone {$p['scheme']}://{$p['user']}:{$p['pass']}@{$p['host']}{$p['path']} 2>&1", $op);
                        else {
                            chdir($name);
                            $res = exec("git pull {$p['scheme']}://{$p['user']}:{$p['pass']}@{$p['host']}{$p['path']} 2>&1",$op);
                        }
                    } elseif (preg_match('|^git@|',$package)) {

                        # It not work without ssh keys...
                        # adduser gitwrap-user
                        # 
                        # Add the following lines to /etc/sudoers file
                        # www-data ALL = (gitwrap-user) NOPASSWD:SETENV: /var/www/html/biomaps/git-wrap.sh
                        # Defaults!/var/www/html/biomaps/git-wrap.sh !requiretty
                        #  
                        # ssh (rsa) key should be uploaded into PROJECT_DIR/.ssh/user@email/ folder
                        #
                        $key_file = getenv('PROJECT_DIR').".ssh/{$_SESSION['Tmail']}";

                        if (!file_exists($key_file)) {
                            $this->message = "Upload your ssh key into your local ssh folder.";
                            return false;
                        
                        }

                        $files = glob("$key_file/*");
                        if (count($files))
                            $key_file = $files[0];
                        
                        if ($gitmethod == 'clone')
                            $res = exec("sudo -u gitwrap-user /var/www/html/biomaps/git-wrap.sh $key_file $gitmethod $package $name 2>&1", $op);
                        else {
                            chdir($name);
                            $res = exec("sudo -u gitwrap-user git pull 2>&1",$op);
                        }

                    }
                    $this->message = implode("\n",$op);
                    $package = $name;

                } elseif (!$exists) {
                    if (!preg_match('/^[a-z0-9_-]+$/',$package)) {
                        $this->message = "Only letters and _ - characters allowed in package name";
                        return false;
                    }
                    mkdir($package,0755);

                    mkdir($package.'/data',0755);
                    mkdir($package.'/output',0755);
                    mkdir($package.'/scripts',0755);
                }

                chdir(getenv('PROJECT_DIR')."computational_packages");

                // append lines to computation_conf.yml
                /*
                ---
                project: dead_animals
                name: bombina
                environment:
                - type: R
                  version: "4.02"
                - type: python
                  version: "2.8"
                */

                if (!file_exists($package.'/computation_conf.yml')) {
                    $res .= yaml_emit_file($package.'/computation_conf.yml',
                        array(
                            'project'=>PROJECTTABLE,
                            'name'=>$package,
                            'environment'=>array(array('type'=>'','version'=>'')),
                            'mainscript'=>''
                            ));

                } else {
                    // update conf file
                    $yaml = yaml_parse_file($package.'/computation_conf.yml');
                    $yaml['project'] = PROJECTTABLE;
                    $yaml['name'] = $package;
                    //$yaml['environment'] = array('type'=>'','version'=>'');

                    $res .= yaml_emit_file($package.'/computation_conf.yml',$yaml);
                }

                if ($method == 'pull') {
                    return $res;
                } else {
                    $this->message = $res;
                    return true;
                }
            #} else {

            #    $this->message = "Project already exists";
            #    return false;
            #}
        }

        if ($method == 'pull') {
            return $res;
        } else {
            $this->message = $res;
            return false;
        }
    }

    public function script_browser() {
    
    }


    public function confirmDropPackage() {
        echo "<br><br><a href='?profile&computations#tab_basic' 
                    data-url='/includes/profile.php?options=module_function&m=computation&params=dropPackage,{$this->package}' 
                    class='profilelink jumplink' style='font-size:150%;position:relative'>".t('str_drop')."???</a>";
    }
    
    public function dropPackage() {
        exec("rm -r ".getenv('PROJECT_DIR')."computational_packages/".$this->package."/");
        return common_message('ok','ok');

    }
    
    public function readResultsPackage() {

        $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"client-key: $ciphertext\r\n" . 
                      "client-name: ".COMPUTATIONAL_CLIENT_NAME."\r\n" 
          )
        );
        $context = stream_context_create($opts);

        $url = $this->readURL();
        $res = file_get_contents("$url?project=".$this->project."&package=".$this->package."&method=results",false,$context);
        $j = json_decode($res, true);
        $content = "";
        if ($j['status']=='success') {
            $content = "<ul>";
            foreach ($j['data']['results_files'] as $rf) {
                $content .= "<li><a href='$url?project=".$this->project."&package=".$this->package."&method=getfile&file=$rf' class='getfile' target='_blank'>$rf</a></li>";            
            }
            $content .= "</ul>";
        }
        return $content;
    }

    // create package tar.gz &&
    // return getPackage link
    public function getPackage() {

        $hash = sslEncrypt($this->package,MyHASH);

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $link = URL;

        $path = getenv('PROJECT_DIR')."computational_packages/".$this->package;

        $phar_path = getenv('PROJECT_DIR')."computational_packages/$this->project"."_$this->package.tar";
        if (file_exists($phar_path.'.gz'))
            unlink(realpath($phar_path.".gz"));
        
        $pharPath = '';

        $dir = "$path/";
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir,
                FilesystemIterator::SKIP_DOTS)
        );

        $filterIterator = new CallbackFilterIterator($iterator , function ($file) {
            return (in_array($file,array('.git')) === false);
        });

        try {
            $phar = new PharData($phar_path);
            $phar->buildFromIterator($filterIterator, $dir);
            $phar->compress(Phar::GZ);
            unlink(realpath($phar_path));
        } catch (Exception $e) {
            debug('Compress failed',__FILE__,__LINE__);
        }
        
        /*$.post('ajax',{'m':'computation','addNewFile':$('#fileName').val(),'dest':$('#dest').text()},function(data){
            alert('ok');
        });*/
        return "<a href='$protocol://$link/boat/ajax?m=computation&getPackageFile&package=$hash'>Download package</a>";

    }
    
    public function readResultsPackageStatus($level) {

        $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"client-key: $ciphertext\r\n" . 
                      "client-name: ".COMPUTATIONAL_CLIENT_NAME."\r\n" 
          )
        );
        $context = stream_context_create($opts);

        $url = $this->readURL();
        $res = file_get_contents("$url?project=".$this->project."&package=".$this->package."&method=state&level=$level",false,$context);
        $j = json_decode($res, true);

        if ($j['status']=='success') {
            if (isset($j['data']['data'])) {
                $time =  gmdate("Y-m-d\TH:i:s\Z", $j['data']['datetime'] );
                return $time."<br>".preg_replace('/\\\n/',"<br>",implode('\n',$j['data']['data']));
            } else
                return $this->print_res($j['data']); 
        } else
            return $this->print_res($res); 
    }

    // print an array as formatted JSON
    public function print_res($result) {
        $p = json_encode($result, JSON_PRETTY_PRINT);
        return "<pre>$p</pre>";
    }

    public function prepareSendingPackage() {
        // E.g.
        // define('COMPUTATIONAL_SERVERS',array('https://openbiomaps.org/obm_runner/api.php','http://193.146.75.188/api.php'));
        //

        $content = '';

        if (!defined('COMPUTATIONAL_SERVERS'))
            return array("No Computational servers available (should be added into /etc/openbiomaps/system_vars.php.inc)","");

        else {
            $servers = COMPUTATIONAL_SERVERS;
            $running_servers = array();
            $ports = array('http'=>80,'https'=>443);
            $content = "<p>";
            foreach ($servers as $s) {
                $p = parse_url($s);
                $host = $p['host'];
                $scheme = $p['scheme'];
                if ($this->serverSate($host,$ports[$scheme])) {
                    $content .=  "$host is online<br>";
                    $running_servers[] = $s;
                } else
                    $content .= "$host is down<br>";
            }
            $content .= "</p>";
            $content .= "<p>";
            $loadstates = array();
            
            $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
            $opts = array(
              'http'=>array(
                'method'=>"GET",
                'header'=>"client-key: $ciphertext\r\n" . 
                          "client-name: ".COMPUTATIONAL_CLIENT_NAME."\r\n" 
              )
            );
            $context = stream_context_create($opts);

            foreach ($running_servers as $s) {
                $p = parse_url($s);
                $host = $p['host'];
                $l = json_decode(file_get_contents($s."?load",false,$context),true);
                //var_dump($l);
                if ($l['status']=='success') {
                    $loadavg = explode(" ",$l['data']['loadavg']);
                    $mem = preg_replace('/^\d+(\w+)$/','',$l['data']['mem_available']);
                    $ncpu = $l['data']['ncpu'];
                    $load_index1 = $loadavg[0] / $ncpu;
                    $load_index5 = $loadavg[1] / $ncpu;
                    $load_index15 = $loadavg[2] / $ncpu;

                    $load_index = $load_index1 + $load_index5 + $load_index15;

                    if ( $load_index > 1 and $load_index1 > 0.8 ) {
                        $loadstates[] = $load_index1 + $load_index5 + $load_index15;
                        $content .= "$host is overloaded<br>";
                    } else {
                        $loadstates[] = $load_index;
                        $content .= "$host is calm (cpu: {$l['data']['ncpu']} mem:{$l['data']['mem_available']})<br>";
                    }
                } else
                        $content .= "$host state is unknown<br>";
            }
            $content .= "</p>";
            $content .= "<p>";

            // use log($mem) !!!
            //
            arsort($loadstates);
            $k = array_key_last($loadstates);
            $url = $running_servers[$k];
            $content .= "$url will be used for running computations<br>";
            $content .= "</p>";
            return array($content,$url);
        }
    }
    
    public function sendPackage($package) {

        list($content,$url) = $this->prepareSendingPackage();

        // Async fork of uploading
        $phpbin = trim(shell_exec('which php'));
        exec("nohup $phpbin ".getenv('OB_LIB_DIR')."modules/computation_fork.php $package $url {$_SESSION['Tmail']} >/dev/null 2>/dev/null &",$op);
        
        return json_encode(array(
                    "data"=>$content,
                    "op"=>$op));

    }
    
    public function readURL() {
        $path = getenv('PROJECT_DIR')."computational_packages/".$this->package;
        #$cmd = sprintf("SELECT computation...");
        $y = yaml_parse_file("$path/computation_state.yml");
        return $y['working-server'];
    }

    public function workPackage($method) {

        $url = $this->readURL();
        $user = $_SESSION['Tmail'];

        $path = getenv('PROJECT_DIR')."computational_packages/".$this->package;
        $y = yaml_parse_file("$path/computation_conf.yml");

        if (!$y) {

            return "Syntax error in config file<br>";
        } else  {

            $project = $y['project'];

            $j = json_encode($y);
            $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);

            $cmd = array('project'=>$this->project,'user'=>$user,'config-file'=>$j,
                'package'=>$this->package,'method'=>$method);
            
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Accept: text/json',"client-key: $ciphertext", 'client-name: '.COMPUTATIONAL_CLIENT_NAME),
                CURLOPT_POST => 1,
                )
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, $cmd);

            $res = curl_exec($ch);

            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);
                $content = "";
                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        $content .= 'Took '. $info['total_time'] . ' seconds to send a request to ' . $info['url'] . "<br>";
                        $content .= $res;
                        break;
                    default:
                        $content = 'Unexpected HTTP code: ' . $http_code . "\n";
                }
            } else {
                $content = 'Curl error: ' . curl_error($ch);
            }

            // Close handle
            curl_close($ch);
            return $content;
        }
    }

    public function profilePage($params,$pa) {
        
        echo '<style>
                #editPackageFileContent {position:relative; overflow: hidden; border:1px solid #eaeaea;padding:5px; }
                #viline { position:absolute;bottom:0;background-color:#aeaeaa;height:17px;z-index: 3;width: 100%;left: 0; }
                .fileBrowser {border: 1px solid lightgray;list-style-type:none;padding-left:10px;line-height:2.2em;max-width:600px;}
                .fileBrowser li:hover{
                    background-color: #efefef;
                }
                .fileBrowser li {border-bottom:1px solid lightgray}
                #viline { color: black; padding-left:4px}
                .catbox { margin-top: 30px;border: 1px solid lightgray;padding: 5px;}
            </style>';
        echo '<h2>Scientific analyses - packages</h2>';
        echo " <a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=createPackage' class='pure-button button-transparent profilelink'>".t('str_create_package')."</a> ";
        echo " <a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,/' class='pure-button button-transparent profilelink'>".t('str_browse_packages')."</a> ";

        if (isset($pa[1]))
            $this->package = $pa[1];
        $this->project = PROJECTTABLE;

        if ($pa[0] == 'createPackage') {
            echo "<p>";
            echo "Give a name for package or a GIT link to download a package from repository.";
            echo "<br>";
            echo "Package name: <input id='createPackageName' style='width:42em' >";
            echo "<input type='button' id='createPackage' value='create'>";
            echo "</p>";

        }
        elseif ($pa[0] == 'updatePackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->workPackage('init')."</div>";
        }
        elseif ($pa[0] == 'dropPackage') {
            echo $this->dropPackage();

        }
        elseif ($pa[0] == 'confirmDropPackage') {
            echo $this->actionMenu($this->package);
            echo $this->confirmDropPackage();

        }
        elseif ($pa[0] == 'buildRemotePackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->workPackage('build')."</div>";
        }
        elseif ($pa[0] == 'runRemotePackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->workPackage('run')."</div>";
        }
        elseif ($pa[0] == 'stopRemotePackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->workPackage('stop')."</div>";
        }
        elseif ($pa[0] == 'pullPackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->createPackage($this->package,'pull')."</div>";
        }
        elseif ($pa[0] == 'readRemotePackage') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->readResultsPackage()."</div>";
        }
        elseif ($pa[0] == 'readRemotePackageStatus') {
            echo $this->actionMenu($this->package);
            echo "<div class='catbox'>".$this->readResultsPackageStatus($pa[2])."</div>";
        }
        #elseif ($pa[0] == 'sendPackage') {
        #    echo $this->actionMenu($this->package);
        #    echo "<div class='catbox'>".$this->sendPackage()."</div>";
        #
        #}
        elseif ($pa[0] == 'addFiles') {
            echo "<p>Upload files into <span id='dest'>" . $pa[1]."</span></p>";
            echo "<form method='post' enctype='multipart/form-data' class='pure-form'><input type='file' id='addPackageFile' name='files[]' multiple='multiple' class='pure-button button-secondary'></form>";
        
        }
        elseif ($pa[0] == 'newFile') {
            echo "<p>Create new file in <span id='dest'><a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,".$pa[1]."' class='profilelink'>" . $pa[1]."</a></span></p>";
            echo "<input id='fileName'><input type='button' id='addNewFile' value='+'>";
        
        }
        elseif ($pa[0] == 'newDir') {
            echo "<p>Create new directory in <span id='dest'><a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,".$pa[1]."' class='profilelink'>" . $pa[1]."</a></span></p>";
            echo "<input id='fileName'><input type='button' id='addNewDir' value='+'>";
        
        }
        elseif ($pa[0] == 'browsePackages') {

            if (!isset($pa[1])) {
                $pa[1] = '/';
                $exp = explode('/',$pa[1]);
            }
            else {
                $exp = explode('/',$pa[1]);
                $package = array_shift($exp);
                if ($package!='') {
                    echo $this->actionMenu($package);
                }
            }

            $path = "";
            if (isset($pa[1]) and is_file(getenv('PROJECT_DIR')."computational_packages/$pa[1]"))
                $file_open = getenv('PROJECT_DIR')."computational_packages/$pa[1]";
            else
                $file_open = '';


            if ($package == '') {
                echo '<h4>Browse packages</h4>';
                $files = glob(getenv('PROJECT_DIR')."computational_packages/*",GLOB_ONLYDIR);
            }
            elseif ($file_open!='') {
                $file = explode("/",$pa[1]);
                $file = array_pop($file);
                echo '<br><h4>'.t('str_open').' '.$file.'</h4>';
                
                echo "<div>".str_edit." | <a href='?profile&computations#tab_basic' id='savePackageFile' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,".$path.basename($pa[1])."'>".str_save."</a></div>";
                $path = $pa[1].'/';
                $files = array();
            }
            else {
                echo "<br><div>
                        <ul class='topnav'>
                            <li><div style='vertical-align: middle;display: table-cell;height: 30px;'>$pa[1] / </div></li>
                            <li><a href='#' class='to' style='border:1px solid lightgray;height: auto;padding: 7px 10px;'><i class='fa fa-plus fa-fw'></i><i class='fa fa-angle-down fa-fw'></i></a>
                                <ul class='subnav'>
                                    <li><a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=newFile,".$path.$pa[1]."' class='profilelink'>New file</a></li>
                                    <li><a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=newDir,".$path.$pa[1]."' class='profilelink'>New directory</a></li>
                                    <li><a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=addFiles,".$path.$pa[1]."' class='profilelink'>Upload files</a></li>
                                </ul></li>
                        </ul>
                    </div><br>";

                $files = glob(getenv('PROJECT_DIR')."computational_packages/$pa[1]"."/*");
                $path = $pa[1].'/';
            }

            echo "<ul class='fileBrowser'>";
            $e = explode('/',$path);
            array_pop($e); 
            array_pop($e);

            echo "<li><a href='?profile&computations#tab_basic' id='goUp' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,".implode('/',$e)."' class='profilelink'><strong>..</strong></a></li>";

            foreach ($files as $package_name) {
                if (is_dir(getenv('PROJECT_DIR')."computational_packages/".$path.basename($package_name)))
                    $icon = "fa-folder-open";
                else
                    $icon = "fa-file-o";
                echo "<li><i class='fa fa-fw $icon'></i> <a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,".$path.basename($package_name)."' class='profilelink'>".basename($package_name)."</a></li>";
            }
            echo "</ul>";

            if ($file_open!='') {
                $mime = mime_content_type(getenv('PROJECT_DIR')."computational_packages/$pa[1]");
                if ($mime == 'text/plain' or $mime == 'application/x-empty') {
                    $content = file_get_contents(getenv('PROJECT_DIR')."computational_packages/$pa[1]");
                    if ($mime == 'application/x-empty')
                        $content = "\n\n";
                    $c = count(file(getenv('PROJECT_DIR')."computational_packages/$pa[1]"));
                    $lines = implode("\n",range(1,$c));
                    echo "<input type='hidden' id='editPackageFile' value='$pa[1]'>";
                    echo "<div id='editPackageFileContent'><div id='viline' contenteditable=true></div><pre id='ln' style='position:absolute;color:#ddd'>$lines</pre><pre contenteditable=true id='code' style='margin-left:20px;background-color:white'>$content</pre></div>";
                    echo "Some vi commands supported: &lt;esc&gt; :w :q :wq dd :s/// :%s///";
                }
            }

        } else {

        }
    }

    public function actionMenu($package) {

        $a =   "<div style='padding-top:15px;'>[<a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params=browsePackages,$package' class='profilelink'>$package</a>] actions: <ul class='topnav' style='padding-left:0'>";
        // delete
        $a .=  "<li><a href='?profile&computations#tab_basic' 
                    data-url='/includes/profile.php?options=module_function&m=computation&params=confirmDropPackage,$package' 
                    class='profilelink button-error' style='border:1px solid lightgray;color:white'>".t('str_drop')."</a></li>";
        if (file_exists(getenv('PROJECT_DIR')."computational_packages/$package/.git")) {
            // git-pull
            $a .= "<li><a href='?profile&computations#tab_basic' 
                    data-url='/includes/profile.php?options=module_function&m=computation&params=pullPackage,$package' 
                    class='profilelink' style='border:1px solid lightgray'>".t('str_pull')."</a></li>";
        }
        // send
        $a .= "<li><a href='#' class='to' style='border:1px solid lightgray'>".t('str_send')."<i class='fa fa-angle-down fa-fw'></i></a>
                <ul class='subnav'>
                    <li>
                        <a href='?profile&computations#tab_basic' 
                        id='sendPackage'
                        data-package='$package' 
                        data-url='/includes/modules/computation_fork.php?package=$package'>".t('str_send')."</a></li>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=readRemotePackageStatus,$package,create' 
                        class='profilelink'>".t('str_status')."</a></li>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=updatePackage,$package' 
                        class='profilelink' style='border:1px solid lightgray'>".t('str_update')."</a></li></ul></li>";
        // stop
        $a .= "<li><a href='?profile&computations#tab_basic' 
                    data-url='/includes/profile.php?options=module_function&m=computation&params=stopRemotePackage,$package' 
                    class='profilelink' style='border:1px solid lightgray'>".t('str_stop')."</a></li>";


        // build
        $a .= "<li><a href='#' class='to' style='border:1px solid lightgray'>".t('str_build')."<i class='fa fa-angle-down fa-fw'></i></a>
                <ul class='subnav'>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=buildRemotePackage,$package' 
                        class='profilelink'>".t('str_build')."</a></li>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=readRemotePackageStatus,$package,build' 
                        class='profilelink'>".t('str_status')."</a></li></ul></li>";
        // run
        $a .= "<li><a href='#' class='to' style='border:1px solid lightgray'>".t('str_run')."<i class='fa fa-angle-down fa-fw'></i></a>
                <ul class='subnav'>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=runRemotePackage,$package' 
                        class='profilelink'>".t('str_run')."</a></li>
                    <li><a href='?profile&computations#tab_basic' 
                        data-url='/includes/profile.php?options=module_function&m=computation&params=readRemotePackageStatus,$package,run' 
                        class='profilelink'>".t('str_status')."</a></li></ul></li>";
        // results
        $a .= "<li><a href='?profile&computations#tab_basic' 
                    data-url='/includes/profile.php?options=module_function&m=computation&params=readRemotePackage,$package' 
                    class='profilelink' style='border:1px solid lightgray'>".t('str_results')."</a></li>";
        $a .= "</ul></div>";
        return $a; 
    }
    
    public function profileItem() {

        $em = sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='?profile&computations#tab_basic' data-url='/includes/profile.php?options=module_function&m=computation&params' class='profilelink'>".t('str_edit_computations')."</a>");

        $em .= "<tr><td>".wikilink('modules.html#computation-packages',t('str_what_is_computation_packages'))."</td></tr>";

        return [
            'label' => 'Scientific analyses',
            'fa' => 'fa-area-chart',
            'item' => $em
        ];
    }

    public function deck_bodypage($params, $request) {
        
        if (isset($request['viewcomputation'])) {
            $this->viewPackage($params,array($_GET['viewcomputation'],$_GET['params']));
        }
    }

    public function viewPackage($params, $request) {
        $package = '';
        list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt(preg_replace('/ /','+',$request[0]),MyHASH);

        $calcmac = hash_hmac('sha256', $ciphertext_raw, MyHASH, $as_binary=true);
        if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
        {
            $package = $decrypted_text;
        }
        $head = "<style>.catbox { margin-top: 30px;border: 1px solid lightgray;padding: 5px;}</style><h2>Analysis package: $package</h2>";

        if ( $request[1] == 'getPackage' ) {
            $this->package = $package;
            echo $head;
            echo "<div class='catbox'>".$this->getPackage()."</div>"; 
            return;
        } else if ( $request[1] == 'readRemotePackageStatus,run' ) {
            $this->package = $package;
            echo $head;
            echo "<div class='catbox'>".$this->readResultsPackageStatus('run')."</div>"; 
            return;
        } else if ( $request[1] == 'readRemotePackage' ) {
            $this->package = $package;
            echo $head;
            echo "<div class='catbox'>".$this->readResultsPackage()."</div>";
            return;
        }


        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $link = URL;

        $getPackage = "<li>
            Scripts, data, results, and runtime environment. All in one compressed file. The runtime environment can be built using the Docker file in the file.<br>
            Quick help:<br>
            Docker image generation: 
                <div style='font-family:monospace;padding:8px'>docker build --tag something: 1.0 .</div>
            To run analyzes in a docker environment:
                <div style='font-family:monospace;padding:8px'>docker run --rm -v path_to_something_dir:/payload something: 1.0</div>
            <a href='$protocol://$link/boat/?viewcomputation=$request[0]&params=getPackage'>".t('str_get_package')."</a></li>";

        $status = "<li>Example running output: <a href='$protocol://$link/boat?viewcomputation=$request[0]&params=readRemotePackageStatus,run'>".t('str_view')."</a></li>";
        $results = "<li>Example running output files: <a href='$protocol://$link/boat/?viewcomputation=$request[0]&params=readRemotePackage'>".t('str_download_results_files')."</a></li>";
        
        echo $head;
        echo "<ul style='line-height:2em'>$getPackage</ul><ul style='line-height:2em'>$status $results</ul>";

    }

    public function ajax($params, $request) {
        if (array_key_exists('savePackageFile',$request[0])) {
            if ($this->savePackageFile($request[0])) {
                echo common_message('ok','');
            } else {
                echo common_message('error','error');
            }
        } elseif (array_key_exists('createPackage',$request[0])) {
            if ($this->createPackage($request[0]['createPackage'])) {
                echo common_message('error',$this->message); // it always send error due to displaying messages...
            } else {
                echo common_message('error',$this->message);
            }
        } elseif (array_key_exists('addNewFile',$request[0])) {
            $file_name = getenv('PROJECT_DIR').'/computational_packages/'.$request[0]['dest'].'/'.$request[0]['addNewFile'];
            $file = fopen($file_name, 'a');  
            fclose($file);

            if (is_file($file_name))
                echo common_message('ok','');
            else
                echo common_message('error','error');
            
        } elseif (array_key_exists('addNewDir',$request[0])) {
            if (mkdir(getenv('PROJECT_DIR').'/computational_packages/'.$request[0]['dest'].'/'.$request[0]['addNewDir']))
                echo common_message('ok','');
            else
                echo common_message('error','error');
        
        } elseif (array_key_exists('loadFile',$request[0])) {
            echo $this->loadUrl($request[0]['loadFile']);

        } elseif (array_key_exists('sendPackage',$request[0])) {
            echo $this->sendPackage($request[0]['sendPackage']);
        }

        if (array_key_exists('getPackageFile',$request[0])) {
            $package = '';
            list($hmac, $decrypted_text, $ciphertext_raw) = sslDecrypt(preg_replace('/ /','+',$request[0]['package']),MyHASH);

            $calcmac = hash_hmac('sha256', $ciphertext_raw, MyHASH, $as_binary=true);
            if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
            {
                $package = $decrypted_text;
            }

            $path = getenv('PROJECT_DIR')."computational_packages/$this->project"."_$package.tar.gz";
            $file = realpath($path);
            $mime = mime_content_type($file);
            header('Content-Description: File Transfer');
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            return;
        
        } 

    }

    private function loadUrl ($url) {

        $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"client-key: $ciphertext\r\n" . 
                      "client-name: ".COMPUTATIONAL_CLIENT_NAME."\r\n" 
          )
        );
        $context = stream_context_create($opts);

        return file_get_contents($url."&getfilekey",false,$context);
    }

    private function savePackageFile($request) {

        if (file_put_contents(getenv('PROJECT_DIR')."computational_packages/".$request['savePackageFile'],$request['content']))
            return true;
        else
            return false;
    }

    #Don't need it, computational_packages.js exists
    #public function print_js($params) {
    #}
    
    public function adminPage($params, $pa) {

        if (!has_access('master')) return;

        return "<div>Admin page content of computations</div>";
    }

    public function getMenuItem() {
        return ['label' => 'Computation servers', 'url' => 'computation' ];
    }
    
    private function serverSate($server,$port=80) {
        //$server = 'google.com';
        if ($socket =@ fsockopen($server, $port, $errno, $errstr, 30)) {
            return true;
            fclose($socket);
        } else {
            return false;
        }
    }
}
?>
