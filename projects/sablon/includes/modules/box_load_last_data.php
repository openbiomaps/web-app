<?php
#' ---
#' Module:
#'   box_load_last_data
#' Files:
#'   [box_load_last_data.php]
#' Description: >
#'   Query last uploaded data by user or by project
#' Methods:
#'   [print_box, query_button, limits, ajax, print_js]
#' Examples: >
#'   [ 20 ]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class box_load_last_data extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_box () {
        global $BID;

        $report_options = array();
        if (isset($_SESSION['Tid'])) {
            if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
                $tgroups = $_SESSION['Tid'];
            else
                $tgroups = $_SESSION['Tgroups'];
            
            $cmd = sprintf("SELECT key,id FROM bookmarks WHERE project_table='%s' AND user_id IN (%s)",$_SESSION['current_query_table'],$tgroups);
            $res = pg_query($BID,$cmd);
            while($row = pg_fetch_assoc($res)) {
                $report_options[] = sprintf('%s::obm_custom_report_%d',$row['key'],$row['id']);
            }
        }

        $t = new table_row_template();
        $t->cell(mb_convert_case(t(str_quick_queries), MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        if (isset($_SESSION['Tid']))
            $mine = sprintf('%s::obm_uploading_id_mine',str_my_last_upload);
        else
            $mine = '';

        $n = "";

        $options = selected_option(array_merge(array(sprintf('%s::obm_uploading_id',str_last_upload),sprintf('%s::obm_id',str_last_rows),$mine),$report_options));
        $t->cell("<select id='last_data_query_option'><option></option>$options</select>",2,'content');
        $t->row();
        //$t->cell("<button id='last_data_query' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        $t->row();
        return sprintf("<table class='mapfb'>%s</table>",$t->printOut());
    }
    public function print_js($p) {
        return "
    // box load last data module
    $(document).ready(function() {
        $(document).on('change','#last_data_query_option',function(e){
            last_data_enabled = 1;
        });

    });";
    }

}
?>
