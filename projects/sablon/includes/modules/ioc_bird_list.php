<?php
#' ---
#' Module:
#'   ioc_bird_list
#' Files:
#'   [ioc_bird_list.php, ioc_bird_list.js]
#' Description: >
#'   Metadata for taxons
#' Methods:
#'   [moduleName, getMenuItem, adminPage, displayError, ajax, init, print_js]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class ioc_bird_list extends module {
    
    public $error = '';
    private $xml = null;
    
    function __construct($action = null, $params = null, $pa = array()) {
        global $BID;

        $this->params = explode(';',$params);

        if ($action)
            $this->retval = $this->$action($params,$pa);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }
    
    public function adminPage($params, $pa) {

        if (!has_access('master')) return;
        ob_start();
        ?>
        <h3>IOC Word Bird List Module</h3>
        
        <button class="module_submenu button-success pure-button" data-url="includes/project_admin.php?options=ioc_bird_list"> <i class="fa fa-refresh"></i> </button>
        <button type="button" class="update_ioc_list pure-button button-success"> <i class="fa fa-download"></i> </button>
        <pre id='ioc_tmp'></pre>
        <?php
        
        return ob_get_clean();
    }

    public function getMenuItem() {
        return ['label' => str_ioc_bird_list, 'url' => 'ioc_bird_list' ];
    }


    public function ajax($params,$request) {
        
        if (!isset($request['action'])) {
            echo common_message('error', 'action missing');
            return;
        }
        $retval = "";
        switch ($request['action']) {
            case 'update_ioc_list':
                $retval = $this->update_ioc_list();
                break;
            
            default:
                $retval = common_message('error', "invalid action: {$request['action']}");
                break;
        }
        
        echo $retval;
    
    }
    
    private function update_ioc_list() {
        // $this->xml = simplexml_load_file("http://milvus.openbiomaps.org/projects/tmp/ioc_short.xml");
        $this->xml = simplexml_load_file("http://www.worldbirdnames.org/master_ioc-names_xml.xml");
        
        $downloaded_versions = $this->downloaded_versions();
        
        $version = (string)$this->xml->attributes()->{'version'};
        
        if (in_array($version, $downloaded_versions)) {
            return common_message('ok', "Version $version already downloaded");
        }
        if (! $this->create_ioc_table($version)) {
            return common_message('error', "Table creation error");
        }
        if (! $this->fill_ioc_table($version))
        return common_message('ok', "Table $version created");
        
    }
    
    private function fill_ioc_table($version) {
        global $ID;
        $ioc_table = "shared.ioc_" . str_replace('.', '_', $version);
        
        foreach ($this->xml->list->children() as $order) {
            if ($order->getName() !== 'order') {
                continue;
            }
            $cmd = [];
            foreach ($order->children() as $family) {
                if ($family->getName() !== 'family') {
                    continue;
                }
                foreach ($family->children() as $genus) {
                    if ($genus->getName() !== 'genus') {
                        continue;
                    }
                    foreach ($genus->children() as $species) {
                        if ($species->getName() !== 'species') {
                            continue;
                        }
                        foreach ($species->children() as $subspecies) {
                            if ($subspecies->getName() !== 'subspecies') {
                                continue;
                            }
                            $cmd[] = sprintf("INSERT INTO %s (\"order\", family, genus, species, subspecies) VALUES (%s, %s, %s, %s, %s);",
                                $ioc_table,
                                quote((string)$order->latin_name), 
                                quote((string)$family->latin_name), 
                                quote((string)$genus->latin_name), 
                                quote((string)$species->latin_name), 
                                quote((string)$subspecies->latin_name)
                            );
                        }
                    }
                }
            }
            query($ID, $cmd);
        }
    }
    
    private function create_ioc_table($version) {
        global $ID;
        $ioc_table = "shared.ioc_" . str_replace('.', '_', $version);
        
        $cmd = "CREATE TABLE $ioc_table (
            id serial,
            \"order\" varchar,
            family varchar,
            genus varchar,
            species varchar,
            subspecies varchar
        );";
        
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        return true;
    }
    
    private function get_ioc_tables() {
        global $ID;
        
        $cmd = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'shared' AND table_name LIKE 'ioc_%';";
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        return (pg_num_rows($res) === 0) ? [] : array_column(pg_fetch_all($res), "table_name");
    }
    
    
    private function downloaded_versions() {
        $tables = $this->get_ioc_tables();
        $versions = array_map(function ($tbl) {
            return str_replace('_', '.', str_replace('ioc_', '', $tbl));
        }, $tables);
        return $versions;
    }
}
?>
