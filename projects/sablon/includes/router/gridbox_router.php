<?php

session_cache_limiter('nocache');
session_cache_expire(0);
session_start();

if (!isset($_SESSION['Tid'])) {

    include_once(getenv('PROJECT_DIR').'local_vars.php.inc');
    $mainpage = MAINPAGE;
    if (isset($mainpage['restrictaded_pages'])) {
        $x = array_intersect(array_keys($chunks), $mainpage['restrictaded_pages']); 
        if (count( $x )) {
            $chunks = []; 
            $load = 'view.php';
        }
    }
}

?>
