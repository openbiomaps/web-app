<?php

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $ref =  sprintf('%s://%s/',$protocol,URL);
    $out = "";


    if (defined('PWA_LINK') and constant('PWA_LINK') == 'on')
        $out .= "<div class='centerbox'><a href='".$ref."pwa/'  class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-lg fa-mobil'></i> ".str_mobil_app."</a></div>";

    $out .= "<div class='centerbox'><a href='".$ref."upload/'  class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-excel-o'></i> ".str_file_upload."</a></div>";
    //$out = "<div class='centerbox'><a href='".$ref."upload/?form=000&type=web'  class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-binoculars'></i> ".str_observation_upload."</a></div>";
    //$out .= "<div class='centerbox'><a href='".$ref."upload/?form=000&type=file' class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-upload'></i> ".str_file_upload."</a></div>";
    $out .= "<div class='centerbox'><a href='".$ref."index.php?map' class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-search'></i> ".str_data_query."</a></div>";

    echo $out;
?>
