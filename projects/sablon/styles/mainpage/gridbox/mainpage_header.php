<?php
global $OB_project_title;
$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
$title_rgb = (defined("TITLE_RGB")) ? constant('TITLE_RGB') : '159,198,81';
$titlew = mb_strlen($OB_project_title)*27;

if (file_exists(getenv('PROJECT_DIR').'local/styles/app/images/logo.png'))
    $logo = '/local/styles/app/images/logo.png';
else
    $logo = STYLE_PATH.'/images/logo.png';
?>

<div style='display:table; width:100%'>
    <div id='header-row' style='display:table-row'>
        <!--<img style='display:table-cell;vertical-align:bottom' src="<?php echo $protocol ?>://<?php echo URL.$logo ?>">-->
        <div style='display:table-cell;vertical-align:baseline'>
            <span style='font-size:400%;padding-left:1rem;color:#ccc;text-shadow: 2px 2px #444'>
                <?php echo $OB_project_title ?>
            </span>
        </div>

        <div style='display:table-cell;vertical-align:baseline;text-align:right'>
            <a href='upload/'><i class='fa fa-upload' style='padding-right:2rem;color:#ccc;text-shadow:2px 2px #444;font-size:300%'></i></a>
            <a href='map/'><i class='fa fa-globe' style='padding-right:2rem;color:#ccc;text-shadow:2px 2px #444;font-size:300%'></i></a>
            <a href='profile/'><i class='fa fa-user' style='padding-right:2rem;color:#ccc;text-shadow:2px 2px #444;font-size:300%'></i></a>
        </div>
    </div>
</div>

