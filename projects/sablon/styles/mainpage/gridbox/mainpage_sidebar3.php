<?php

    $mf = new mainpage_functions();

    $out = "<h2>".t(str_activity)."</h2>";

    if (isset($MAINPAGE_VARS['sidebar3'])) {
        $boxes = preg_split('/\|/',$MAINPAGE_VARS['sidebar3']);
        $box_params = array();
        foreach ($boxes as $x) {
            $p = '';
            $t = $x;
            if (preg_match('/(\w+)\((.+)\)$/',$x,$m)) {
                $p = $m[2];
                $t = $m[1];
            }
            $params[$t] = $p;
        }
    }
    else {
        $boxes = array('uploads','data');
        $params = array('uploads'=>'uploads','data'=>'data');
    }

    $boxes = array_keys($params);

    if (in_array('uploads',$boxes)) {
        $out .= "<div class='downbox'><h3>".str_most_data."</h3>";
        $out .= "<ul class='boxul'>";
        #$mf->column = "observer";
        #$rows = $mf->most_active_users('custom');
        $rows = $mf->most_active_users($params['data']);
        $list = array();
        foreach($rows as $row) {
            $list[] = "<div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div>";
        }
        $out .= "<li><div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div></li>";

        $out .= "</ul></div>";
    }

    if (in_array('data',$boxes)) {
        $out .= "<div class='downbox'><h3>".str_most_uploads."</h3>";
        $out .= "<ul class='boxul'>";
        $rows = $mf->most_active_users($params['uploads']);

        $list = array();
        foreach($rows as $row) {
            $list[] = "<div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div>";
        }
        $out .= "<li><div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div></li>";

        $out .= "</ul></div>";
    }

    echo $out;
?>
