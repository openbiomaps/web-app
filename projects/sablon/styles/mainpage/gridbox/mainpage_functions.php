<?php

class mainpage_functions {
    public $table;
    public $debug = FALSE;
    // data_table
    public $columns = array();
    public $headers = array();
    public $limit = 1000;
    public $orderby = 'obm_uploading_id';
    public $obm_id_order = ',obm_id DESC';
    public $order = 'DESC';
    // users' stat
    public $column = "";
    // default cache times in minutes
    public $count_members_cache = 60;
    public $count_data_cache = 10;
    public $count_uploads_cache = 10;
    public $count_species_cache = 30;
    public $stat_species_cache = 60;
    public $stat_species_user_cache = 2;
    public $most_active_users_cache = 60;
    public $force_update_cache = false;

    public function __construct() {
        $this->table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    }

    function returnFunc($method,$args) {
        return call_user_func(array($this, $method), $args);
    }
    
    // Summary for a custom column
    function stat_custom_column($table,$column) {
        global $ID;

        $cmd = sprintf('SELECT DISTINCT %1$s %1$s FROM %2$s WHERE %1$s IS NOT NULL ORDER BY %1$s LIMIT 10',$column,$table);
        $res = pg_query($ID,$cmd);

        return pg_num_rows($res);
    }
    // Summary sum for a custom column
    function stat_custom_column_summary($table,$column) {
        global $ID;

        $cmd = sprintf('SELECT json_agg(t) FROM (SELECT %1$s, count(%1$s) FROM %2$s WHERE %1$s IS NOT NULL GROUP BY %1$s ORDER BY count DESC LIMIT 10) t',$column,$table);
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);

        return $row['json_agg'];
    }

    // a summary functions
    // a data table
    // default order is uploads
    function data_table($args) {

        if (!preg_match('/^'.PROJECTTABLE.'/',$this->table)) {
            return;
        }

        global $ID;
        $columns = implode(',',$this->columns);
        $cmd = sprintf("SELECT %s FROM %s ORDER BY %s %s %s LIMIT %s",$columns,$this->table,$this->orderby,$this->order,$this->obm_id_order,$this->limit);
        $res = pg_query($ID,$cmd);

        $table = new createTable();
        $table->def(['tid'=>'mytable','tclass'=>'pure pure-table pure-table-striped','tstyle'=>'width:100%']);

        // auto create link from obm_id column
        if ($this->columns[0] == 'obm_id' )
            $table->tformat(['format_col'=>'0','format_string'=>'<a href=\'data/'.$this->table.'/COL-0\' style="color:lightblue"><i class="fa fa-lg fa-newspaper-o"></i></a>']);

        while ($row=pg_fetch_assoc($res)) {
            $table->addRows($row);
        }

        $table->addHeader($this->headers);
        return $table->printOut();
    }
    // a summary functions
    // count members
    // Recommended CACHE: 6 minutes
    function count_members($project = PROJECTTABLE) {
        global $BID;

        $cnt = obm_cache('get',"count_members",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        if ($project == '') $project = PROJECTTABLE;

        $cmd = sprintf("SELECT count(*) as c FROM project_users WHERE project_table=%s AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant')",quote($project));
        $result = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($result);

        $time = $this->count_members_cache * 6;
        obm_cache('set',"count_members",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count data
    // Recommended CACHE: 30 minutes
    // $param  =  processing only one table:  schema.table
    function count_data($params) {
        global $BID, $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;

        $cnt = obm_cache('get',"count_data",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        // A view-kat ki kellene zárni
        $cmd = sprintf("SELECT f_project_schema,f_project_table,f_id_column FROM header_names WHERE f_project_name=%s",quote(PROJECTTABLE));
        $result = pg_query($BID,$cmd);
        if ($params!=''){
            list($s,$t) = preg_split("/\./",$params);
                
            $cmd = sprintf("SELECT '%s' as f_project_schema, '%s' as f_project_table, 'obm_id' as f_id_column",$s,$t);
            $result = pg_query($BID,$cmd); 
        }
        $fn = 0;
        $c = 0;
        while ($row = pg_fetch_assoc($result))  {
            if ($row['f_id_column'] != '') {
                $cmd = sprintf("SELECT count({$row['f_id_column']}) as c FROM \"%s\".\"%s\"",$row['f_project_schema'],$row['f_project_table']);
                $result2 = pg_query($ID,$cmd);
                if ($result2) {
                    $row2 = pg_fetch_assoc($result2);
                    $c += $row2['c'];
                }
                $fn++;
            }
        }

        $time = $this->count_data_cache * 30;
        obm_cache('set',"count_data",$c,$time,FALSE);
        return $c;
    }
    // a summary functions
    // count data
    function count_tables() {
        global $ID;

        $cmd = sprintf("SELECT f_project_table FROM header_names WHERE f_project_name=%s",quote(PROJECTTABLE));
        $result = pg_query($BID,$cmd);
        return $pg_num_rows($result);

    }
    // a summary functions
    // count uplods
    // Recommended CACHE: 1 minutes
    function count_uploads($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        $cnt = obm_cache('get',"count_uploads",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        if ($project == '') $project = PROJECTTABLE;

        $cmd = sprintf("SELECT count(id) as c FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        $time = $this->count_uploads_cache * 1;
        obm_cache('set',"count_uploads",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count species
    // CACHE: 2 minutes
    public function count_species($use_taxon_db=FALSE) {
        global $ID;

        $st_col = st_col($this->table,'array');
        $SPECIES_C = $st_col['SPECIES_C'];

        //if ($table_ext!='')
        //    $table = PROJECTTABLE.'_'.$table_ext;

        if ($SPECIES_C!='') {
            $cnt = obm_cache('get',"count_species",'','',FALSE);
            if ($this->force_update_cache) $cnt = false;
            if ($cnt !== false )
                return $cnt;

            $sss = preg_replace('/[a-z0-9_]+\./','',$SPECIES_C);

            #count only valid names
            if ($use_taxon_db)
                $cmd = "SELECT COUNT(DISTINCT word) AS c FROM ".PROJECTTABLE."_taxon 
                        WHERE status::varchar IN ('undefined','accepted') AND lang='".$SPECIES_C."' AND taxon_db>0";
            else
                $cmd = "SELECT COUNT($sss) AS c 
                        FROM (SELECT DISTINCT $SPECIES_C FROM ".$this->table." LEFT JOIN ".PROJECTTABLE."_taxon ON ($SPECIES_C=word) 
                        WHERE status::varchar IN ('undefined','accepted','common','synonym')) AS temp";

            $result = pg_query($ID,$cmd);
            $row=pg_fetch_assoc($result);

            $time = $this->count_species_cache * 2;
            obm_cache('set',"count_species",$row['c'],$time,FALSE);
            return  $row['c'];
        }
    }
    // a summary functions
    // count species
    // CACHE: 2 minutes
    public function stat_species($use_taxon_db=FALSE,$stat_type=array('frequent'),$addlinks=true) {

        $table = obm_cache('get',"stat_species",'','',FALSE);
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        global $ID;
        $sum = "0";
        $count = "";
        $order = "cnt";
        $where = "";

        $st_col = st_col($this->table,'array');
        $qtable = "";
        if ($this->table!='PROJECTTABLE') {
            $t = preg_replace('/'.PROJECTTABLE.'_/','',$this->table);
            $qtable = "&qtable=$t";
        }
        $SPECIES_C = $st_col['SPECIES_C'];

        if ($SPECIES_C!='') {
            $count = "count($SPECIES_C)";
        }

        if ($st_col['NUM_IND_C']!='') {
            $NUM_IND_C = $st_col['NUM_IND_C'];
            $sum = "SUM($NUM_IND_C) AS cnt";
            $where = "WHERE $NUM_IND_C IS NOT NULL AND $SPECIES_C IS NOT NULL";
        } else {
            $count .= " AS cnt";
        }

        if ($SPECIES_C!='') {

            $table = "";

            if (in_array('rarest',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$table." $where GROUP BY $SPECIES_C ORDER BY $order LIMIT 3";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small'>{$row['cnt']}</div>";
                }
                $table = "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_rarest_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }
            if (in_array('frequent',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$this->table." $where GROUP BY $SPECIES_C ORDER BY $order DESC LIMIT 6";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small' style='text-align:right'>".number_format($row['cnt'])."</div>";
                }
                $table .= "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_most_frequent_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }

            $time = $this->stat_species_cache * 2;
            obm_cache('set',"stat_species",$table,$time,FALSE);
            return $table;
        }
    }
    // a summary functions
    // last upload date
    function last_upload($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        if ($project == '') $project = PROJECTTABLE;

        $cmd = sprintf("SELECT max(uploading_date) as d FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        return $row['d'];
    }
    // a summary functions
    // most frequent species per user
    // CACHE: 20 minutes
    function stat_species_user($params) {
        if (!isset($_SESSION['Tid']))
            return;

        $table = obm_cache('get',"stat_species_user");
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        global $ID,$BID;
        $st_col = st_col($this->table,'array');

        $table = $this->table;
        $species_c = $st_col['SPECIES_C'];

        // f_main_table????
        /*$cmd = sprintf("SELECT f_main_table,f_species_column FROM header_names WHERE f_table_name='%s' AND f_species_column IS NOT NULL",PROJECTTABLE);
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return;
        else {
            while ($row = pg_fetch_assoc($result)) {
                $table = $row['f_main_table'];
                $species_c = $row['f_species_column'];
            }
        }*/

        $list = array();
        if ($species_c != '') {
            $cmd = sprintf('SELECT %1$s as f,count(%1$s) FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id 
                WHERE uploader_id=%3$d GROUP BY %1$s ORDER BY count DESC LIMIT 5',$species_c,$table,$_SESSION['Trole_id']);
            $result = pg_query($ID,$cmd);
            while($row=pg_fetch_assoc($result)) {
                $list[] = "<div class='tbl-cell'>{$row['f']}</div><div class='tbl-cell' style='text-align:right'>{$row['count']}</div>";
            }
        }
        $table = "<div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";

        $time = $this->stat_species_user_cache * 20;
        obm_cache('set',"stat_species_user",$table,$time);
        return $table;
    }
    // a summary functions
    // most active users: upload count
    // CACHE: 30 minutes
    function most_active_users($type) {
        global $ID;

        $st_col = st_col($this->table,'array');
        $CITE_C = $st_col['CITE_C'];

        $rows = obm_cache('get',"most_active_users",'','',FALSE);
        if ($this->force_update_cache) $rows = false;
        if ($rows !== false )
            return $rows;

        if ($type == 'data') {
            if ($this->table == '') $this->table = PROJECTTABLE;
            if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;

            $union = [];
            foreach ($CITE_C as $cite_col) {
                $union[] = "SELECT $cite_col AS collector, COUNT(*) as count FROM \"$this->table\" GROUP BY $cite_col";
            }
            $union_all = implode(' UNION ALL ',$union);

            if (count($union))
                $cmd = "
                WITH all_collectors AS (
                    $union_all
                ),
                distinct_collectors AS (
                    SELECT collector, SUM(count) AS total_count
                    FROM all_collectors
                    WHERE collector IS NOT NULL
                    GROUP BY collector
                )
                SELECT collector as name, SUM(total_count) AS cn
                FROM distinct_collectors
                GROUP BY name
                ORDER BY cn DESC
                LIMIT 10";

            else
                $cmd = "SELECT uploader_name as name,uploader_id,COUNT(*) as cn
                FROM system.uploadings u
                LEFT JOIN \"".$this->table."\" d ON (d.obm_uploading_id=u.id)
                WHERE uploader_id>0 AND obm_uploading_id IS NOT NULL
                GROUP BY name,uploader_id
                ORDER BY cn DESC LIMIT 100";

        }
        elseif ($type == 'uploads') {
            $cmd = sprintf("SELECT uploader_name as name,uploader_id,COUNT(*) as cn
                FROM system.uploadings u
                WHERE uploader_id>0 AND project=%s
                GROUP BY name,uploader_id
                HAVING COUNT(*)>1
                ORDER BY cn DESC LIMIT 100",quote(PROJECTTABLE));

        } elseif ($type == 'custom' and $this->column!='') {
            if ($this->table == '') $this->table = PROJECTTABLE;
            if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;
            $cmd = "SELECT COUNT(obm_id) as cn, \"".$this->column."\" AS name
                FROM \"".$this->table."\"
                GROUP BY \"".$this->column."\"
                ORDER BY cn DESC LIMIT 100";
            if ($this->debug)
                log_action($cmd);

        }

        $result = pg_query($ID,$cmd);

        $counter = array();
        while ($row = pg_fetch_assoc($result)) {
            if (!isset($counter[$row['name']]))
                $counter[$row['name']] = 0;
            $counter[$row['name']] += $row['cn'];

        }
        arsort($counter);
        $rows = array();
        foreach($counter as $name=>$cn) {
            $rows[] = array('uploader_name'=>$name,'uploader_id'=>0,'cn'=>$cn);
        }
        $rows = array_slice($rows, 0, 10);

        $time = $this->most_active_users_cache * 30;
        obm_cache('set',"most_active_users",$rows,$time);
        return $rows;
    }
    function view_table_links ($sep='<br>') {
        require_once(getenv('OB_LIB_DIR'). 'modules_class.php');

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $modules = new modules(PROJECTTABLE);

        $links = array();

        if ($modules->is_enabled('read_table')) {
            $l = $modules->_include('read_table','list_links');

            foreach ($l as $link) {
                $links[] = sprintf('<a href="%1$s://%2$s/view-table/%3$s/" target="_blank">%4$s</a>', 
                                $protocol, URL, $link['link'], $link['label'] );
            }
        }
        return implode($sep,$links);

    }
}
// include local mainpage functions
if (file_exists($MAINPAGE_PATH."private/mainpage_local_functions.php")) {

    require_once($MAINPAGE_PATH.'private/mainpage_local_functions.php');

}

?>
