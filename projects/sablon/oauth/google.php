<?php

require_once('../includes/base_functions.php');
require_once('../includes/postgres_functions.php');
require_once('../includes/user.php');

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

# Create Google User
$U = new User($payload['email']);

#debug('Google Auth requested', __FILE__,__LINE__);

$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
$password = substr( str_shuffle( $chars ), 0, 16 );

if (is_null($U->user_id)) {
    $userinfo = ['name'=>$payload['family_name'].' '.$payload['given_name'],'email'=>$payload['email'],'provider'=>'google','password'=>$password];
    if (!$U->register((object) $userinfo)) {
        // User create error
    }
} else {
    if (!$U->update(["password"=>$password])) {
        // User update error
    }
}

// OAuth user check & create
// $user = $storage->getUser($payload['sub']);
// if (!$user) {
//     $storage->setUser($payload['sub'], 'password',$payload['given_name'], $payload['family_name'], $payload['email']);  // a jelszó beállítása egy állandó értékre!
// }

# Authenticate Google User
$request = OAuth2\Request::createFromGlobals();

# Doing a client_credential auth to create an access_token
$request->request['client_id'] = $client;
$request->request['client_secret'] = $client_secret;
$request->request['grant_type'] = 'password';
$request->request['username'] = $payload['email'];
$request->request['password'] = $password;  // Random jelszó használata
$request->request['scope'] = $scopeRequired; 

$token = $server->handleTokenRequest($request)->getParameters();

if (isset($token['access_token'])) {
    $request->request['access_token'] = $token['access_token'];
} else {
    $token['error_description'] .= ": ".$scopeRequired;
    log_action($token);
    // return error somehow...
    // {"error":"invalid_scope","error_description":"The scope requested is invalid for this client"}
}

?>
