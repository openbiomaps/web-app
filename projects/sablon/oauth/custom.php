<?php

class MyPdo extends OAuth2\Storage\Pdo
{

    public function getUser($username)
    {   
        # modified to openbiomaps
        $stmt = $this->db->prepare($sql = sprintf('SELECT * from %s where email=:username', $this->config['user_table']));
        $stmt->execute(array('username' => $username));

        if (!$userInfo = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return false;
        }

        // the default behavior is to use "username" as the user_id
        return array_merge(array(
            'user_id' => $username
        ), $userInfo);
    }

    public function setUser($username, $password, $firstName = null, $lastName = null, $email = null)
    {
        // do not store in plaintext
        $password = $this->hashPassword($password);

        // if it exists, update it.
        if ($this->getUser($username)) {
            $stmt = $this->db->prepare($sql = sprintf('UPDATE %s SET password=:password, first_name=:firstName, last_name=:lastName, email=:email where username=:username', $this->config['user_table']));
        } else {
            $stmt = $this->db->prepare(sprintf('INSERT INTO %s (username, password, first_name, last_name, email) VALUES (:username, :password, :firstName, :lastName, :email)', $this->config['user_table']));
        }

        return $stmt->execute(compact('username', 'password', 'firstName', 'lastName', 'email'));
    }
    protected function checkPassword($user, $password)
    {
        #return $user['password'] == $this->hashPassword($password);
        return password_verify($password,$user['password']);
    }
    protected function hashPassword($password)
    {
        #return sha1($password);
        $options = [
            'cost' => 10,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $hash = password_hash($password, PASSWORD_BCRYPT, $options);

        return $hash;
    }

}

?>
