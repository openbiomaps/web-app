--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--
-- General superuser for both, biomaps and gisdata databases
CREATE ROLE biomapsadmin;
ALTER ROLE biomapsadmin WITH SUPERUSER NOINHERIT CREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5538770fd215c91629c9c3ba2bd73647c' VALID UNTIL 'infinity';


-- A super-admin user only for biomaps database
CREATE ROLE mainpage_admin;
ALTER ROLE mainpage_admin WITH NOINHERIT NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md56e554766f00142aa8735f0d16022c831' VALID UNTIL 'infinity';

--
-- Role memberships
--


--
-- PostgreSQL database cluster dump complete
--

