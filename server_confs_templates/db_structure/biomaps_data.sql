--
-- OpenBioMaps biomaps database default content for web application.
-- Version: 5.7.9
--
-- Changes:
-- 5.4.8: admin_pages


\c biomaps;
--
-- Data for Name: db_updates; Type: TABLE DATA; Schema: public; Owner: gisadmin
--


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: gisadmin
-- Password: 12345

COPY users (id, "user", password, status, username, institute, address, email, inviter, validation, reactivate, orcid, "references", givenname, familyname, pwstate, terms_agree) FROM stdin;
1	1d98f5f03cc23fb8cabe964a4dbb758b	$2y$10$L6OStG5Q6DHouVSOazQ7DeazeDnWMAKca7Ax5ykRTtnT6mokjtR5S	1	Valaki Valahol	OpenBioMaps	H-4032, Debrecen, Egyetem tér 1.	valaki@openbiomaps.org	1	1	\N	\N	\N	\N	\N	t	1
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.projects (comment, admin_uid, project_table, creation_date, email, validation, domain, "Creator", stage, doi, running_date, licence, rum, licence_uri, alternate_id, collection_dates, project_hash, subjects, geolocation, local_project) FROM stdin;
Sablon Database:::Sablon Faj Adatbázis	1	sablon	2012-02-01	valaki@openbiomaps.org	1	\N	Valaki Valahol	stable	\N	2012-08-01	\N	\N	\N	\N	\N	7qaoqtihkd50	\N	\N	t
\.


--
-- Data for Name: header_names; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.header_names (f_project_schema, f_project_name, f_species_column, f_date_columns, f_quantity_column, f_order_columns, f_id_column, f_cite_person_columns, f_x_column, f_y_column, f_srid, f_geom_column, f_alter_speciesname_columns, f_file_id_columns, f_use_rules, f_project_table) FROM stdin;
public	sablon	faj	{dt_to}	egyedszam	{"sablon":{"adatkozlo":"1","szamossag":"3","gyujto":"4"}}	obm_id	{gyujto}	eov_x	eov_y	4326	obm_geometry	{magyar}	{sablon}	objectid	sablon
\.


--
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

-- COPY public.modules (project_table, module_name, file, function, enabled, id, params, module_access, main_table, group_access) FROM stdin;


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.oauth_clients (client_id, client_secret, redirect_uri, grant_types, scope, user_id) FROM stdin;
mobile	123	\N	password refresh_token	get_profile apiprofile get_project_list put_data get_data get_tables webprofile get_attachments get_form_list get_form_data get_message_count tracklog get_project_vars request_time get_trainings get_training_questions training_results training_toplist	\N
pwa	pwa	\N	password refresh_token	get_profile apiprofile get_project_list put_data get_data get_tables webprofile get_attachments	\N
R	\N	\N	password refresh_token	get_profile apiprofile get_project_list put_data get_data get_tables webprofile get_attachments get_form get_history set_rules pg_user get_report use_repo get_specieslist computation tracklog	\N
web	web	\N	password refresh_token	apiprofile webprofile get_tables get_data get_attachments	\N
webprofile	\N	\N	\N	\N	\N
\.


--
-- Data for Name: oauth_scopes; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.oauth_scopes (scope, is_default) FROM stdin;
get_specieslist	f
get_form_list	f
get_form_data	f
get_profile	f
get_data	f
get_history	f
get_report	f
set_rules	f
put_data	f
webprofile	f
get_tables	f
get_project_list	t
\.


--
-- Data for Name: project_descriptions; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_descriptions (projecttable, language, short, long) FROM stdin;
sablon	hu	Teszt Adatbázis	 célja a közpénzen gyűjtött biotikai adatok elérhetővé tétele az érdeklődők számára. Az adatbázis a nem regisztrált felhasználók számára a természetvédelmileg érzékeny adatok kivételével szabadon elérhető.
sablon	en	Test Database	Species Database.\nIts contains ~500.000 occurrence data of ~5000 species.\nThe database is public however it is working in testing stage. Please contact the admins for recent data.
\.


--
-- Data for Name: project_forms; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_forms (form_id, project_table, form_name, form_type, form_access, user_id, active, groups, description, destination_table, srid, data_owners, data_groups, last_mod, draft) FROM stdin;
1	sablon	public upload	{file,web,api}	0	1	1	{}	\N	sablon	{}	\N	{}	2018-09-05 09:43:10.068486	false
\.


--
-- Data for Name: project_forms_data; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_forms_data (form_id, "column", description, type, control, "count", list, obl, fullist, default_value, genlist, api_params, relation, regexp, spatial, pseudo_columns, custom_function, list_definition) FROM stdin;
1	adatkozlo	\N	text	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	szamossag	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "true": [],\n        "false": []\n    }\n}
1	gyujto	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	altema	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": [],\n        "allatok": [\n            "állatok"\n        ],\n        "novenyek": [\n            "növények"\n        ]\n    }\n}
1	dt_from	\N	text	nocheck	{}	\N	2	0	\N	\N	[""]	(altema=allatok){obligatory(1)}	\N	\N	\N	\N	\N
1	dt_to	\N	date	nocheck	{}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	egyedszam	\N	numeric	minmax	{1,50}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	eloford_al	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	elohely	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	eov_x	\N	numeric	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	eov_y	\N	numeric	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	faj	Az állat neve	autocomplete	nocheck	{}	\N	2	0	\N	\N	["sticky"]	\N	\N	\N	\N	\N	{\n    "optionsTable": "sablon",\n    "valueColumn": "faj"\n}
1	obm_files_id	\N	file_id	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	hatarozo	\N	list	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": []\n    }\n}
1	him	\N	numeric	minmax	{1,100}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	magyar	\N	text	nocheck	{0,0}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	megj	\N	crings	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
1	natura2000	\N	boolean	nocheck	{}	\N	2	0	\N	\N	[""]	\N	\N	\N	\N	\N	{\n    "list": {\n        "": [],\n        "false": [],\n        "true": []\n    }\n}
1	obm_geometry	\N	point	nocheck	{}	\N	1	0	\N	\N	[""]	\N	\N	\N	\N	\N	\N
\.



--
-- Data for Name: project_layers; Type: TABLE DATA; Schema: public; Owner: gisadmin
--
COPY public.project_layers (project_table, layer_name, layer_def, tipus, url, map, name, enabled, id, layer_order, ms_layer, singletile, legend) FROM stdin;
sablon	layer_data_biotika	{"layers":"sablon_points","isBaseLayer":"false","visibility":"true","opacity":"1.0","format":"image\\/png","transparent":"true","numZoomLevels":"20"}	WMS	default	default	Sablon Biotika Points	t	4	\N	salon_points	t	t
\N	layer_tile_mapbox	url: 'https://{a-c}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=12345'	XYZ	\N	\N	Mapbox	t	3	\N	\N	f	f
\N	layer_tile_Bing	key:'12345',imagerySet:'AerialWithLabels'	Bing	\N	\N	Bing	t	2	\N	\N	f	f
\N	layer_tile_osm	\N	OSM	\N	\N	OpenStreetMap	t	1	\N	\N	\N	\N
\.

--
-- Data for Name: project_mapserver_layers; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY project_mapserver_layers (mapserv_layer_name, project_table, geometry_type) FROM stdin;
sablon_points	sablon	POINT
\.


--
-- Data for Name: project_metaname; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_metaname (project_table, column_name, description, short_name, rights, "order") FROM stdin;
sablon	szamossag	\N	szamossag	{0}	\N
sablon	gyujto	\N	gyujto	{0}	\N
sablon	faj	\N	faj	{0}	\N
sablon	magyar	\N	magyar	{0}	\N
sablon	eov_x	nincs értelme kitölteni	eov X	{0}	\N
sablon	eov_y	nincs értelme kitölteni	eov Y	{0}	\N
sablon	hely	\N	hely	{0}	\N
sablon	meret_kb	\N	meret_kb	{0}	\N
sablon	egyedszam	\N	egyedszam	{0}	\N
sablon	him	\N	hím	{0}	\N
sablon	nosteny	\N	nosteny	{0}	\N
sablon	eloford_al	\N	eloford_al	{0}	\N
sablon	gyujto2	\N	gyujto2	{0}	\N
sablon	gyujto3	\N	gyujto3	{0}	\N
sablon	hatarozo	\N	hatarozo	{0}	\N
sablon	adatkozlo	\N	adatkozlo	{0}	\N
sablon	modsz	\N	modsz	{0}	\N
sablon	elohely	\N	elohely	{0}	\N
sablon	veszteny	\N	veszteny	{0}	\N
sablon	kapcs_dok	\N	kapcs_dok	{0}	\N
sablon	megj	\N	megj	{0}	\N
sablon	rogz_modsz	\N	rogz_modsz	{0}	\N
sablon	altema	\N	altema	{0}	\N
sablon	obm_geometry	srid:4326	obm_geometry	{0}	\N
sablon	natura2000	\N	natura2000	{0}	\N
sablon	vedettseg	\N	vedettseg	{0}	\N
sablon	tema	\N	tema	{0}	\N
sablon	obm_datum	\N	obm_datum	{0}	\N
sablon	dt_from	\N	dt_from	{0}	\N
sablon	dt_to	\N	dátum	{0}	\N
sablon	rogz_dt	\N	rogz_dt	{0}	\N
sablon	obm_files_id	\N	files	{0}	\N
sablon	time	\N	time	{0}	\N
sablon	objectid	\N	objectid	{0}	\N
\.


--
-- Data for Name: project_queries; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_queries (project_table, layer_query, enabled, id, layer_type, rst, layer_cname, main_table) FROM stdin;
sablon	SELECT d.obm_id,obm_geometry\n%selected%\nFROM %F%sablon d%F%\n%rules_join%\n%taxon_join%\n%uploading_join%\n%morefilter%\nWHERE %geometry_type% %envelope% %qstr%	t	37	query	0	layer_data_biotika	sablon
\.


--
-- Data for Name: project_roles; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

--COPY public.project_roles (role_id, container, project_table, description, user_id) FROM stdin;
--1	{1}	sablon	Valaki Valahol	1
--\.


--
-- Data for Name: project_users; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_users (project_table, user_id, user_status, join_date) FROM stdin;
sablon	1	2	2016-07-06 15:38:04.795721+02
\.


--
-- Data for Name: project_variables; Type: TABLE DATA; Schema: public; Owner: gisadmin
--

COPY public.project_variables (project_table, map_center, map_zoom, click_buffer, zoom_wheel, default_layer, fixheader, turn_off_layers, subfilters, langs, legend, training, rserver) FROM stdin;
sablon	0101000020E61000001BD82AC1E238334058569A9482A64740	7	0	f	layer_gsat	\N		f	{"faj":"en","magyar":"hu"}	t	f	t
\.


--
-- Data for Name: admin_pages; Type: TABLE DATA; Schema: public; Owner: biomapsadmin
--

COPY public.admin_pages (name, label, "group", filename) FROM stdin;
access	str_data_access	project_admin	access
dbcols	str_table_management	project_admin	tables
description	str_project_description	project_admin	description
files	str_file_manager	project_admin	file_manager
functions	str_functions	project_admin	functions
groups	str_groups	project_admin	groups
imports	str_intr_uploads	project_admin	imports
invites	str_invites	invites	\N
jobs	str_job_management	project_admin	\N
languages	str_lang_definitions	project_admin	languages
mapserv	str_map_definitions	project_admin	maps
members	str_members	project_admin	members
modules	str_modules	project_admin	modules
new_project_form	str_new_project	new_project	\N
newsread	str_messages	messages	\N
privileges	str_group_privileges	project_admin	privileges
query_def	str_query_definitions	project_admin	queries
r_server	rshinyserver	project_admin	r_server
serverinfo	str_serverinfo	project_admin	server_info
server_logs	str_server_logs	project_admin	logs
taxon	str_taxon_names	project_admin	linnaeus
upload_forms	str_upload_forms	project_admin	upload_forms
\.

--
-- Name: biomaps_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.biomaps_events_id_seq', 1, true);


--
-- Name: bookmarks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.bookmarks_id_seq', 1, true);


--
-- Name: evaluations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.evaluations_id_seq', 1, true);


--
-- Name: form_training_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.form_training_id_seq', 1, true);


--
-- Name: form_training_questions_qid_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.form_training_questions_qid_seq', 1, true);


--
-- Name: invites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.invites_id_seq', 1, true);


--
-- Name: modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.modules_id_seq', 1, true);


--
-- Name: pds_upload_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.pds_upload_test_id_seq', 1, true);


--
-- Name: preojects_news_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.preojects_news_stream_id_seq', 1, true);


--
-- Name: project_forms_form_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_forms_form_id_seq', 2, true);


--
-- Name: project_layers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_layers_id_seq', 5, true);


--
-- Name: project_queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_queries_id_seq', 1, true);


--
-- Name: project_roles_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_roles_role_id_seq', 1, true);


--
-- Name: project_visitors_visitor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gisadmin
--

SELECT pg_catalog.setval('public.project_visitors_visitor_id_seq', 1, true);


--
-- Name: statusz_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.statusz_id_seq', 1, false);


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: biomapsadmin
--

SELECT pg_catalog.setval('public.translations_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


