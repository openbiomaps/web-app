SYMBOLSET
SYMBOL
    NAME "circle"
    TYPE ellipse
    POINTS
        1 1
    END 
    FILLED true
    #ANCHORPOINT 0.5 0.5 
END
SYMBOL
    NAME 'square'
    TYPE vector
    FILLED true
    POINTS
        0 1
        0 0
        1 0
        1 1
        0 1
    END 
END 
SYMBOL
    NAME 'square_full'
    TYPE vector
    FILLED true
    POINTS
        0 0
        0 1
        1 1
        1 0
        0 0
    END 
END
SYMBOL
  NAME "downwarddiagonalfill"
  TYPE vector
  TRANSPARENT 0
  POINTS
    0 1 
    1 0 
  END 
END
END
