<?php
# If you use OBM in a local environment without proxy but non-standard http ports, set it true!
#define("USE_NON_STANDARD_HTTP_PORTS",false);

# obm app settings
# This variable MUST BE set correctly to use openbiomaps instance. E.g. obm.yourdomain.org
# The localhost/biomaps work only for the localhost intallations
define("OB_DOMAIN", "localhost/biomaps" );

# Path for OpenBioMaps system files, e.g. mapserver's access.key if it is in a non-default place; 
# The default is /var/lib/openbiomaps/
#define("OB_SYSDIR","/var/lib/openbiomaps/");

define("OB_TMP", "/var/lib/openbiomaps/tmp/" );

# Maybe you need to update this path. 
# This is a directory which contains the `projects` directory - this constant used by the obm-application (afuncs, common_pg_funcs, ...)
define("OB_ROOT", "/var/www/html/biomaps/root-site/" );

# Maybe you need to update this path. 
# This is a directory which contains the `supervisor.md` file - this constant only used by supervisor.php
define("OB_ROOT_SITE", "/var/www/html/biomaps/root-site/" ); 

# Used by supervisor and in the new project creating process
define("OB_RESOURCES", "/var/www/html/biomaps/resources/" );

# Used by the server_vars.php.inc sometimes the `biomaps` path is the correct value
#define("OB_WEB_PRE",""); 

# Not used, but can be interesting?
#define("IN_DOCKER",true); 

# For docker
define("PHP_PATH", "/usr/local/bin/php" );

# Database access
define("POSTGRES_PORT","5432"); // should be removed
define("MAPSERVER_HOST",'mapserver');
#define('MAPSERVER_TMP','/tmp');

define('biomapsdb_user','biomapsadmin');
define('biomapsdb_pass',$_ENV['BIOMAPS_DB_PASSWORD']);
define('biomapsdb_port',5432);

define('biomapsdb_name','biomaps');
define('biomapsdb_host','biomaps_db'); // Instead of localhost using docker style standalone biomaps host

define('mainpage_user','mainpage_admin');
define('mainpage_pass',$_ENV['MAINPAGE_ADMIN_PASSWORD']);

# Used by supervisor; list of gisdata databases
define('gisdata_dbs',array('gisdata')); 

#used by supervisor; list of host names for each databases
define('gisdb_hosts',array('biomaps_db'));
define('gisdb_ports',array(5432));

# Used by create-new-project; 
# SHOULD BE COMMENT OUT to improve create new project on big servers which contains more than one database...
define("GISDB_HOST",'biomaps_db'); 

# PostGIS version
define('POSTGIS_V','2.3');

# sendmail  or smtp
# If smtp, use SMTP_ variables in local_vars.php.inc files to enable per project smtp authentication
define('SENDMAIL','smtp');

# obm cache method
define('CACHE','memcache');

# Global Mail settings
# Project can override them....
#define('SMTP_GLOBAL_AUTH',false); # true
#define('SMTP_GLOBAL_HOST','...');
#define('SMTP_GLOBAL_USERNAME','...');
#define('SMTP_GLOBAL_PASSWORD','...');
#define('SMTP_GLOBAL_SECURE','tls'); # ssl
#define('SMTP_GLOBAL_PORT','587'); # 465
#define('SMTP_GLOBAL_SENDER','openbiomaps@...');

# R-Shiny Server ports
#define('RSERVER_PORT_sablon',7982);

# Bug report 
#define('AUTO_BUGREPORT_ADDRESS','');

# Computation
#define('COMPUTATIONAL_SERVERS',array(''));
#define('COMPUTATIONAL_CLIENT_SEC','');
#define('COMPUTATIONAL_CLIENT_KEY','');
#define('COMPUTATIONAL_CLIENT_NAME','');

# REPOSITORIES: Dataverse
#define('REPOSITORIES',array(
#    array(
#        'TYPE'=>'dataverse',
#        'SERVER_URL'=>'',
#        'API_TOKEN'=>'',
#        'ROOT_DATAVERSE'=>'')
#));
define('WEB_CLIENT_SECRET','web');
define('MOBILE_CLIENT_SECRET','123');
define('PWA_CLIENT_SECRET','pwa');
?>
