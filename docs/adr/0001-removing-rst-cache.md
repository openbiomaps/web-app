# ADR 0001: Gyorsítótárazás eltávolítása az rst függvényből a csoport színtű hozzáféréskezelés esetén

## Státusz
Elfogadott

## Dátum
2024-11-29

## Kontextus
A csoport szintű hozzáféréssel működő projektekben az rst függvény különböző helyzetekben megvizsgálja, hogy a felhasználónak van-e olvasás és/vagy írás joga egy adat rekordhoz. Az eredményt későbbi felhasználásra eltárolja a gyorsítótárban. A cache-be írás és olvasás nagy adatmennyiség esetén nagy adatforgalmat okoz.

## Döntés
A gyorsítótárazás használatát töröljük az rst függvényből, csoport szintű hozzáféréskezelés esetén.

## Indoklás
A tesztelések során bizonyíthatóan sokkal lassabban kapunk eredményt a cachelés használatával adatexportáláskor, vagy bekapcsolt download_restricted modullal akár a lekérdezéskor. 

Teszteltem saját fejlesztői gépemen.

#### csv export 3000 rekord

| de_nocache3k | de_cache3k |
|--------------|-------------|
| 8,11         | 20          |
| 5,47         | 13,74       |
| 5,85         | 30,38       |
| 4,73         | 29,04       |
| 5,6          | 41,44       |

#### csv export 12000 rekord

| de_nocache12k | de_cache12k  |
|---------------|---------------|
| 16,95         | nem fut le    |
| 16,73         | nem fut le    |
| 16,31         | nem fut le    |
| 18,01         | nem fut le    |
| 16,39         | nem fut le    |

#### eredmények lekérdezése bekapcsolt download_restricted modullal 3000 rekord

| dlr_nocache3k | dlr_cache3k |
|---------------|-------------|
| 0,58          | 45,04       |
| 0,74          | 48,7        |
| 0,59          | 49,2        |
| 0,88          | 45,75       |

#### eredmények lekérdezése bekapcsolt download_restricted modullal 12000 rekord

| dlr_nocache12k | dlr_cache12k |
|----------------|---------------|
| 2,4            | nem fut le    |
| 2,03           | nem fut le    |
| 2,36           | nem fut le    |
| 2,26           | nem fut le    |
| 2,79           | nem fut le    |
| 2,67           | nem fut le    |


Az indokláshoz tartozhat még az a feltételezés is, hogy nem valószínű, hogy gyakori az a helyzet, amikor ugyanazokat az adatokat többször egymás után kérdezné le a felhasználó, amikor esetleg előnyös lehet a gyorsítótár használata.


## Következmények
Remélhetőleg látványosan gyorsabb működés nagyobb projekteknél.
