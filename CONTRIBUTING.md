# How to contribute

I'm really glad you're reading this, because we need volunteer developers to help this project come to fruition.

Here are some important resources:
    
   - Docs
   - Events
   - Mailing lists: Join our developer list

## Testing

    - Docker ...
    - Virtualbox ...


## Do you want to contribute to the OBM documentation?

    - Please read Contributing to the OBM Documentation (github.com/openbiomaps/documentation/).

## Did you find a bug?

    - Do not open up a GitLab issue if the bug is a security vulnerability in OBM, OTRS is where to report them.

## Do you intend to add a new feature or change an existing one?

    - Please send a GitLab Pull Request to ..., OR?



Thanks, Miklós Bán, OpenBioMaps Team
