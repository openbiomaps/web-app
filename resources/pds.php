<?php
#if (getenv('PROJECT_DIR') !== __DIR__.'/') exit;
define('PROJECT_DIR',basename(__DIR__));
#define('CALL',dirname(__DIR__,2)); # php >7 only
define('CALL',"/var/www/html/biomaps"); # php <7 

// JSON request proxy ... ha szeretnénk Application/JSON kéréseket is küldeni
// Application/JSON adat feldolgozása
// Ellenőrizzük, hogy a kérés POST metódussal érkezett-e
/*if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Olvassuk be a POST adatokat JSON formátumból
    $json_data = file_get_contents('php://input');
    
    if ($json_data !== false) {
        // Átalakítjuk a JSON adatokat asszociatív tömbbé
        $data = json_decode($json_data, true);
        
        if ($data !== null) {
            // Átalakítjuk az adatokat application/x-www-form-urlencoded formátummá
            $form_data = http_build_query($data);
            
            // Átadjuk az átalakított adatokat a feldolgozó scriptnek
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://.../pds.php');
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $form_data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            
            // Válasz küldése a kliensnek
            echo $response;
        } else {
            http_response_code(400); // Bad Request
            echo "Invalid JSON data.";
        }
    } else {
        http_response_code(400); // Bad Request
        echo "Error reading POST data.";
    }
} else {
    http_response_code(405); // Method Not Allowed
    echo "Only POST requests are allowed.";
}
 */

# authentication
require(getenv('PROJECT_DIR')."oauth/resource.php");
# pds service
require(getenv('PROJECT_DIR')."pds/service.php");
?>
