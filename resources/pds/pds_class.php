<?php
# Latest API_VERSION 2.6
# v.2.5
# published_form_id
# response tpye modifications for the new mobile app
#
# v.2.6
# get_notification, share_location
#

class pds {

    public $pfs_put_data = false;
    public $method = '';
    public $request_error = '';
    public $request_warnings = array();
    public $request = array();

    private $table = '';
    private $schema = '';
    private $access_token = '';
    private $project = '';
    private $dto = array();
    private $user = array();
    private $rights = array();
    private $results = array();
    private $debug = false;     // not used
    private $API_VERSION = 1.0;

    // constructor
    public function __construct($API_POST,$API_VERSION) {
        $BID = $GLOBALS['BID'];
        $ID = $GLOBALS['ID'];
        $this->API_VERSION = $API_VERSION;

        if (defined('DEBUG_PDS') and constant('DEBUG_PDS')===true) {
            log_action($_POST,__FILE__,__LINE__);
            log_action($API_VERSION,__FILE__,__LINE__);
            log_action($API_POST,__FILE__,__LINE__);
            if (isset($_POST['get500'])) {
                http_response_code(500);
                exit;
            }
            // debugging async call errors..
            if (isset($_POST['waitRandom'])) {
                $wait = rand ( 0, 5 );

                if ($wait == 5) {
                    $wait = rand ( 0, 30 );
                    if ($wait < 5) {
                        exit;
                    }
                    if ($wait > 25) {
                        $wait = rand ( 0, 90 );
                        if ($wait < 5) {
                            exit;
                        }
                    }
                }
                sleep($wait);
            }
        }

        require('request_types.php');

        foreach ($API_POST as $key=>$val) {

            // If the requested key exists ...
            if (in_array($key,$valid_requests)) {
                // ez így töbszörös azonos kulcsokat is feldolgoz
                // az utolsó marad benne...
                if ($key == 'service') {
                    $this->method = $val;               // for service.php
                } else if ($key == 'put_data') {
                    $this->pfs_put_data = true;         // for service.php
                } else if ($key == 'post_tracklogs') {
                    $this->pfs_put_data = true;         // for service.php
                } else if ($key == 'debug') {
                    $this->debug = true;                // private, not used !
                } else if ($key == 'table') {
                    $this->table = $val;                // private
                } else if ($key == 'shared_link') {
                    $this->shared_link = $val;
                } else if ($key == 'access_token') {
                    $this->access_token = $val;         // private
                } else {
                    // egyéb request kulcsok a request tömbbe...
                    $this->request[$key]=$val;          // public
                }
            } else {
                http_response_code(400);
                $this->request_error="$key is not a valid request!";    // public
            }
        }

        //it is defined in project's own pds.php
        if (defined('CALL')) {

            //$this->project = PROJECT_DIR;
            //$this->project = basename(getenv('PROJECT_DIR'));
            $this->project = PROJECTTABLE;

            // ha már nem lett volna behívva a db_funcs-ban!
            //require_once(CALL."/projects/".PROJECT_DIR."/local_vars.php.inc");
            require_once(getenv('OB_LIB_DIR').'languages.php');

        } else {

            http_response_code(400);
            $this->request_error = "Obsolate request method!";
            return;

        }

        /* Oauth hack for creating general SESSION variables
         *
         * This should be solved an other way 
         *
         * */
        if (defined('FAKE_PROJECT')) {
            $cmd = sprintf("SELECT id,username,pu.project_table
                FROM users
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s GROUP BY users.id,pu.project_table",quote($this->access_token));
            $res = pg_query($BID,$cmd);
            while ($row = pg_fetch_assoc($res)) {
                $_SESSION['Tid'] = $row['id'];
                $_SESSION['Trole_id'] = '';
                $_SESSION['Tname'] = '';
                $_SESSION['Tmail'] = '';
            }
        } else {

            /* POssible access token check, but we need the user of the token
            if ($this->access_token != '') {
                $t = $this->access_token;
                require(getenv('PROJECT_DIR').'oauth/server.php');

                $params = array(
                    'access_token' => $t,
                    'scope' => $_POST['scope']
                );
                $serverRequest = new OAuth2\Request($params);
                $response = new OAuth2\Response();

                $tokenData = $server->getAccessTokenData($serverRequest, $response);

                if ($tokenData && $tokenData['access_token'] == $t) {

                } else {
                    debug('Auth failed', __FILE__, __LINE__);
                }
            } else {
            
                debug('Auth failed', __FILE__, __LINE__);
            } */


            $cmd = sprintf("SELECT id,username,email
                FROM users
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email)
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table='%s' GROUP BY users.id",quote($this->access_token),$this->project);
            $res = pg_query($BID,$cmd);

            $_SESSION['Tid'] = 0;
            $_SESSION['Tmail'] = 'non-logined-user';
            $_SESSION['Tname'] = 'non-logined-user';
            $_SESSION['Tgroups'] = '';
            $_SESSION['Trole_id'] = 0;

            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $_SESSION['Tid'] = $row['id'];
                $_SESSION['Tmail'] = $row['email'];
                $_SESSION['Tname'] = $row['username'];

                $r = read_groups($row['id']);
                $_SESSION['Tgroups'] = $r['roles'];
                $_SESSION['Trole_id'] = $r['cont'];

            }
            st_col($this->table,'session');
        }

        // check project requests
        if ($this->project!='') {

            # Set this->table
            if (!isset($this->table) or $this->table=='')
                $this->table = $this->project;

            if ($this->table != $this->project)
                $this->table = $this->project.'_'.$this->table;

            # Set SESSION['current_query_table']
            $_SESSION['current_query_table'] = $this->table;

            // Check requested table is exists and accessible
            // Muszáj ezt itt csinálni???
            $cmd = sprintf('SELECT * FROM projects LEFT JOIN header_names ON (f_project_name=project_table) 
                            WHERE project_table=%s AND f_project_table=%s',
                                quote($this->project),quote($this->table));
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                
                // Get materialized views - postgres access right let us accecc only those views which are belonging to our project
                $cmd = sprintf("SELECT schemaname as schema_name,
                   matviewname as view_name,
                   matviewowner as owner,
                   ispopulated as is_populated
                FROM pg_matviews
                WHERE matviewname=%s
                ORDER BY schema_name, view_name", quote($this->table));
            	$res = pg_query($ID,$cmd);
                if (!pg_num_rows($res)) {
                    
                    // Get views
                    $cmd = sprintf("SELECT viewname FROM pg_catalog.pg_views WHERE viewname=%s", quote($this->table));
            	    $res = pg_query($ID,$cmd);
                    if (!pg_num_rows($res)) {
                	http_response_code(400);
                	$this->request_error = $this->table." table does not exists!";
                        return;
                    }
		}
                $row = pg_fetch_assoc($res);

            } else {
                $row = pg_fetch_assoc($res);
                #
                # other project params
            }

            if (defined('FAKE_PROJECT')) {
                $this->project = '';
                $this->table = "";
            }

        } else {

            http_response_code(400);
            $this->request_error = "PROJECT_IS_NOT_DEFINED";
            return;

        }
        // end __construct
    }

    /* SERVICE: PRS
     * query data point or history-data point based on its "id"
     * _GET request
     * E.G.: http://openbiomaps.org/pds/service.php?service=PRS&table=danubefish&data=(3699,4677) */
    public function getData() {

        if (!isset($this->project) or $this->project=='') {
            
            http_response_code(400);
            $this->request_error = "PROJECT_IS_NOT_DEFINED";
            return;

        }
        if (!isset($this->table) or $this->table=='') {
            
            http_response_code(400);
            $this->request_error = "TABLE_IS_NOT_DEFINED";
            return;

        }

        $ID = $GLOBALS['ID'];

        # prepare a query and
        # return the result as json object
        $request_table = '';
        $request_value = '';
        $request_column = '';

        foreach ($this->request as $key=>$val)
        {
            if ($key == 'get_data_rows') {
                $request_table = $this->table;
                // defaults
                $request_column = '--id--';
                $request_value = $val;

                $limit = '';
                $filter = '';
                $exclude_columns = array();

                if ( $this->API_VERSION >= 2.2) {
                    $filter = array();
                    $request_column = '--filter--';
                    
                    $values = preg_split('/&/',$request_value);

                    foreach ($values as $value) {

                        if ($value == '*') {
                            $filter[] = "1=1";
                        }
                        if (preg_match('/exclude-columns=([A-Za-z0-9_,]+)/',$value,$m)) {
                            $exclude_columns = preg_split("/,/",$m[1]);
                        }

                        if (preg_match('/limit=(\d+):(\d+)/',$value,$m)) {
                            $limit = 'LIMIT '.$m[1].' OFFSET '.$m[2];
                        }
                        
                        if ($value == 'filter') {

                            $request_column = '--filter--';
                            $obm_columns = dbcolist('columns',$this->table);
                            $obm_columns[] = 'obm_id';
                            foreach ($this->request['filters'] as $fk=>$fv) {
                                if (in_array($fk,$obm_columns)) {
                                    if ($fk == 'obm_geometry') {
                                        $filter[] = "st_within(obm_geometry,st_setsrid(st_geomFromText('$fv[0]'),4326))";
                                    } elseif (is_array($fv)) {
                                        $filter[] = $fk .' IN ('. implode(',',array_map('quote',$fv)) .')';
                                    } else {
                                        $filter[] = $fk .'='. quote($fv);
                                    }
                                }
                            }
                        
                        }
                        else if (preg_match('/filter=([a-z0-9_]+)([!=]+)([A-Za-z0-9-_% "]+)/i',$value,$m)) {

                            $obm_columns = dbcolist('columns',$this->table);
                            $obm_columns[] = 'obm_id';
                            if (in_array($m[1],$obm_columns)) {
                                
                                # Simple SQL Query Processing
                                if (preg_match('/^%|%$/',$m[3])) {
                                    if ($m[2] == '!=')
                                        $filter[] = $m[1].' NOT LIKE '.quote($m[3]);
                                    else
                                        $filter[] = $m[1].' LIKE '.quote($m[3]);
                                } else {
                                    if ($m[2] == '!=')
                                        $filter[] = $m[1].'!='.quote($m[3]);
                                    else
                                        $filter[] = $m[1].'='.quote($m[3]);
                                }
                            } else {
                                // not valid obm_column in the request!
                                echo common_message('error','Not existing or not allowed column requested');
                                return;
                            }
                        }
                    }
                    if ($filter != '')
                        $filter = implode(' AND ',$filter);


                } else {
                    // It is used by old R client without API version
                    // custom
                    // e.g. species="Motacilla flave"
                    if (preg_match('/([a-z0-9_]+)=(.+)/i',$val,$m)) { 
                        $request_column = $m[1];
                        $request_value = $m[2];
                    } elseif ($val == '*') {
                        $request_column = 1;
                        $request_value = 1;
                    }
                }
            }
            elseif ($key == 'get_history_rows') {
                $request_table = $this->table.'_history';
                $request_column = '--history--';
                $request_value = $val;
            }
        }

        // adat lekérdezés
        if (!isset($this->request['type']))
            $this->request['type'] = 'json';

        $response = dataQuery($request_table,$request_value,$request_column,$this->request['type'],
                                $filter,$limit,$exclude_columns,$this->shared_link);
        $j = json_decode($response);

        if ($j->{'status'}=='error') {
            
            http_response_code(400);
            $this->request_error = $response;
            return;

        } else if ($j->{'status'}=='fail') {

            http_response_code(401);
            $this->request_error = $response;
            return;

        } else {
            $cmd = $j->{'data'};
            $result = pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                $errorID = uniqid();
                http_response_code(202);
                echo common_message('fail','PROCESSING_FAILED',$errorID);
                log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                log_action($cmd,__FILE__,__LINE__);
                return;
            }
            if (!pg_num_rows($result)) {
                //$this->request_warnings[] = 'Data query has no data'; Is the array processed properly?
                http_response_code(204);
                echo common_message('warning','NO_DATA_RETURNED');
                return;
            }

            if ($this->request['type']=='xml') {
                // collapse with large arrays!!!
                $this->results = pg_fetch_all($result);
                // creating object of SimpleXMLElement
                $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><OBM_data></OBM_data>");
                // function call to convert array to xml
                array_to_xml($this->results,$xml);
                // print out XML
                header('Content-type: text/xml');
                print $xml->asXML();

            } elseif ($this->request['type']=='export') {
                $p = pg_num_rows($result);
                $n = 0;
                $data = array("status"=>"success","data"=>array());
                //tricky way to fetch large respones
                $files = array();
                while ($row = pg_fetch_row($result)) {
                    $n++;
                    $x = json_decode($row[0],true);
                    $files[] = $x['obm_filename'];
                    $data["data"][] = $x;
                }
                $json_data = json_encode($data);
                $filename = genhash(12);
                $path = getenv('PROJECT_DIR').'local/attached_files';

                // JSON data to file
                if (file_put_contents($path.'/export_data_'.$filename.'.json', $json_data) === false) {
                    log_action("Export file creation error",__FILE__,__LINE__);
                }

                header('Content-type: application/json');
                $uri = "/api/export_attachments/zip_stream/load.php?table={$request_table}&filter_files_table={$request_table}&filter_files_column=reference&filter_files=".implode($files,',')."&conid=off&filename=".$filename;
                $call = iapi::load($uri);
                $r = json_decode($call,true);
                if ($r['status']!='success')
                    echo json_encode($r);
                #echo json_encode(array("status"=>$r["status"],"data"=>array("download_link"=>"export.zip")));

            } else {
                header('Content-type: application/json');
                //defult request type is JSON
                //simulating common_message() function
                echo '{"status":"success","data":[';
                //echo '["';
                $p = pg_num_rows($result);
                $n = 0;
                //tricky way to fetch large respones
                while ($row = pg_fetch_row($result)) {
                    $n++;
                    echo $row[0];
                    if($n<$p) {
                        echo ",";
                    }
                }
                echo "]}";
                //the following way not works with large objects!
                //$this->results = pg_fetch_all($result);
                //print array2json($this->results);
            }
        }
    }


    // SERVICE: PFS
    // project feature servise
    // It is a function
    // $this->request['table'] should be defined and valid
    //
    public function getFunction($key,$value) {

        $BID = $GLOBALS['BID'];
        $ID = $GLOBALS['ID'];
        $cache = XCache::getInstance();

        $modules = new modules($this->table);
        
        $xmodules = new x_modules();

        // Get Function result
        if ($key == 'request_time') {
            // measure request time, testing network timing
            if (ctype_digit($value))
                echo common_message('ok',time() - $value);
            return;

        } else if ($key == 'history') {
            # Ha a value tömb: (id1,id2,...)...
            #if(!is_array($value)) {
            #    if (preg_match("/^\((.+)\)$/",$value,$m)) {
            #        $value = preg_split("/,/",$m[1]);
            #    }
            #}

            if (!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (!is_array($value)) $value = array($value);
            foreach ($value as $var) {
                $result = histHeader($this->table,$var);
                if (pg_num_rows($result))
                    $r1 = pg_fetch_all($result);
                else
                    $r1 = array();
                $result = histRows($this->table,$var);
                if (pg_num_rows($result))
                    $r2 = pg_fetch_all($result);
                else
                    $r2 = array();
                if (count($r1) and count($r2) and count($r1)==count($r2)) {
                    $c = array_combine((array_merge(array_keys($r1[0]),array_keys($r2[0]))),array_merge(array_values($r1[0]),array_values($r2[0])));
                    $this->results[] = $c;
                }
            }
            $this->$key = array2json($this->results);

        } elseif ($key == 'track_data') {

            if (!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_data_'.$this->table.'_'.$value})) {
                    print $cache->{'track_data_'.$this->table.'_'.$value};
                    return;
                }
            } else {
                //not handling array
                http_response_code(400);
                $this->request_error = 'ARRAY_REQUIRED';
                return;
            }

            $cmd = sprintf("SELECT counter FROM track_data_views WHERE project_table=%s AND id=%d",
                quote($this->project),
                preg_replace('/(\d+)[_]?(\d+)?/','$1',$value));
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->$key = array2json(array($row['counter']));
            $cache->set('track_data_'.$this->project.'_'.$value,$this->$key,60);

        } elseif ($key == 'track_download') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_download_'.$this->project.'_'.$value})) {
                    print $cache->{'track_download_'.$this->project.'_'.$value};
                    return;
                }
            } else {
                //not handling array
                http_response_code(400);
                $this->request_error = 'ARRAY_REQUIRED';
                return;
            }

            $cmd = sprintf("SELECT counter FROM track_downloads WHERE project_table=%s AND id=%d",
                quote($this->project),
                $value);
            $res = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->$key = array2json(array($row['counter']));
            $cache->set('track_download_'.$this->table.'_'.$value,$this->$key,60);

        } elseif ($key == 'track_citation') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (!is_array($value)) {
                if (isset($cache->{'track_citation_'.$this->project.'_'.$value})) {
                    print $cache->{'track_citation_'.$this->project.'_'.$value};
                    return;
                }
            } else {
                //not handling array
                http_response_code(400);
                $this->request_error = 'ARRAY_REQUIRED';
                return;
            }

            $cmd = sprintf("SELECT unnest(source) as source FROM track_citations WHERE project_table=%s AND id=%d",
                quote($this->project),
                $value);
            $res = pg_query($BID,$cmd);
            $cit = array();
            while($row = pg_fetch_assoc($res)){
                $cit[] = $row['source'];
            }
            $this->$key = array2json($cit);
            $cache->set('track_citation_'.$this->project.'_'.$value,$this->$key,60);

        } elseif ($key == 'set_rules') {

            $this->$key = json_encode('a');
            print (json_encode('1'));

        } elseif ($key == 'get_profile') {

            $result = pds_profile($this->access_token);
            if (!$result) {
                http_response_code(403);
                $this->request_error = "NO_DATA_RETURNED";
                return;
            }

            echo common_message('ok',$result);
            return;

        } elseif ($key == 'get_specieslist') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            require(getenv("OB_LIB_DIR")."iapi.php");
            $uri = "/api/species_list/taxon_table/load.php";
            $call = iapi::load($uri);
            $r = json_decode($call,true);
            if ($r['status'] == 'success')
                $results = $r['data'];
            else
                return($r['message']);

            if (!$result) {
                $this->request_error = "NO_DATA_RETURNED";
                http_response_code(204);
                return;
            }

            return $call;

        } elseif ($key == 'LQ') {

            if(!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }
            /* Previously saved query load
             * */
            $response = $this->loadq($value);
            $j = json_decode($response);

            if ($j->{'status'}=='error') {
                echo $response;
            } else if ($j->{'status'}=='fail') {
                echo $response;
            } else {
                $data = $j->{'data'};
                // adat lekérdezés
                $response = dataQuery($data[0],$data[1],'id','JSON');
                $j = json_decode($response);

                if ($j->{'status'}=='error') {
                    echo $response;
                } else if ($j->{'status'}=='fail') {
                    echo $response;
                } else {
                    $cmd = $j->{'data'};
                    $result = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        http_response_code(202);
                        echo common_message('fail','PROCESSING_FAILED',$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                        return;
                    }
                    if (!pg_num_rows($result)) {
                        http_response_code(204);
                        echo common_message('error','NO_DATA_RETURNED');
                        return;
                    }
                    // heavy memory load warning!
                    echo common_message('ok',pg_fetch_all($result));
                }
            }

        } elseif ($key == 'get_mydata_rows') {

            if (!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }
            $cmd = sprintf("SELECT id FROM system.uploadings WHERE project='%s' AND project_table='%s' AND uploader_id=%d",
                $this->project,
                $this->table,
                $_SESSION['Trole_id']);
            $res1 = pg_query($ID,$cmd);

            $data = array();
            $limit = 0;
            if ($value!='')
                $limit = $value;

            $counter = 0;
            while($row = pg_fetch_assoc($res1)) {
                $response = dataQuery($this->table,$row['id'],'obm_uploading_id','JSON');
                $j = json_decode($response);

                if ($j->{'status'}=='error') {
                    http_response_code(400);
                    $this->request_error = $response;
                    return;
                } else if ($j->{'status'}=='fail') {
                    http_response_code(400);
                    $this->request_error =  $response;
                    return;
                } else {
                    $cmd = $j->{'data'};
                    $result = pg_query($ID,$cmd);
                    if (pg_last_error($ID)) {
                        $errorID = uniqid();
                        http_response_code(202);
                        echo common_message('fail','PROCESSING_FAILED',$errorID);
                        log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                        return;
                    }
                    //if (!pg_num_rows($result)) {
                    //    echo common_message('error','Data query has no data: '.$cmd);
                    //    return;
                    //}
                    $odata = array();
                    while ($row_data = pg_fetch_assoc($result)) {
                        if ($limit and $counter == $limit)
                            break;
                        $odata[] = json_decode($row_data['row_to_json'],true);
                        $counter++;
                    }
                    if (count($odata))
                        $data[] = $odata;
                }
            }

            echo common_message('ok',$data);

        } elseif ($key == 'get_form_list') {

            // GET FORM LIST ========================================->

            # list of available forms
            if(!isset($this->table) or $this->table=='') {
                http_response_code(400);
                $this->request_error = 'TABLE_IS_NOT_DEFINED';
                return;
            }

            if (isset($_SESSION['Tgroups']))
                $tgroups = $_SESSION['Tgroups'];
            else
                $tgroups = '';

            if (isset($_SESSION['Tid']))
                $user = $_SESSION['Tid'];
            else
                $user = 0;

            $form_list = array();
            $form_ids = pds_form_choose_list($this->table,$user,$tgroups);
            foreach ($form_ids as $form) {
                $id = $form['id'];
                $pubid = $form['pubid'];
                $row = pds_form_element($id,$this->table);
                $translated = $row['form_name'];
                if (defined($row['form_name'])) $translated = constant($row['form_name']);
                // backward compatibility
                // id, visibility, , [{“form_id”:”93”,”form_name”:”lepke űrlap”}
                $form_list[] = array(
                    'id'=>$id,
                    'visibility'=>$translated,
                    'form_id'=>$id,
                    'published_form_id'=>$pubid,     // from API v2.5
                    'form_name'=>$translated,
                    'last_mod'=>$row['last_mod']);
            }
            if (!count($form_list)) {
                http_response_code(204);
                echo common_message('error','NO_DATA_RETURNED');
            } else {
                echo common_message('ok',$form_list);
            }
            return;

        } elseif ($key == 'get_form_data') {

            // GET FORM DATA ========================================->
            //  Vajon miért csak az API formokat engedem itt lekérdezni?

            # fields and settings of a selected form
            if (!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (isset($_SESSION['Tgroups']))
                $tgroups = $_SESSION['Tgroups'];
            else
                $tgroups = '';

            if (isset($_SESSION['Tid']))
                $user = $_SESSION['Tid'];
            else
                $user = 0;

            if ($this->API_VERSION < 2.5) {
                $cmd = sprintf("SELECT form_id,destination_table FROM project_forms WHERE form_id=%s AND project_table=%s AND 'api'=ANY(form_type)",
                    quote($value),
                    quote($this->project));
            } else {
                // From API 2.5 query form_data by published_form_id and returning the most recent form_data instead of the explicit query
                $cmd = sprintf('
                    (SELECT form_id,destination_table,draft FROM project_forms 
                     WHERE project_table=%1$s AND draft=false AND published_form_id=%2$s AND \'api\'=ANY(form_type)
                     ORDER BY last_mod DESC 
                     LIMIT 1)
                  UNION
                    (SELECT form_id,destination_table,draft FROM project_forms 
                     WHERE project_table=%1$s AND draft=true AND user_id=%3$d AND published_form_id=%2$s AND \'api\'=ANY(form_type))
                  ORDER BY draft DESC LIMIT 1',
                    quote($this->project),
                    quote($value),
                    $_SESSION['Tid']);
            }
            
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                $this->request_error = 'INVALID_FORMID_REQUESTED';
                http_response_code(202);
                return;
            }
            $row = pg_fetch_assoc($res);
            $st_col = st_col($row['destination_table'],'array');
            $destination_table = $row['destination_table'];
            $form_id = $row['form_id'];

            if ( pds_form_access_check($form_id,$user,$tgroups) !==1 ) {
                $this->request_error = 'FORM_ACCESS_DENIED';
                http_response_code(403);
                return;
            }
            
            if (isset($st_col['ORDER']))
                $order_col = json_decode($st_col['ORDER'],true);
            else
                $order_col = array();

            $ordered_values = array();
            if (isset($order_col[$destination_table]))
                foreach($order_col[$destination_table] as $key=>$val) {
                    if ($val!='')
                        $ordered_values[$val] = $key;
                }
            $order = "'".implode("','",array_values($ordered_values))."'";

            $cmd = sprintf("SELECT \"column\",position_order FROM project_forms_data WHERE form_id=%d ORDER BY position_order",
                $form_id);
            $respo = pg_query($BID,$cmd);
            $column_positions = pg_fetch_all($respo);
            $columns = array_column($column_positions, 'column');
            $positions = array_column($column_positions, 'position_order');

            if (array_filter($positions))
                $order = "'".implode("','",$columns)."'";

            $schema = 'public';

            #  	form_id 	column 	description 	type 	length 	count 	list 	obl 	fullist 	default_value 	genlist 	api_params
            $cmd = sprintf('SELECT f.description as form_description,d.description,default_value,"column",short_name,array_to_string(list,\',\') as list,control,count,
                    type,genlist,obl,api_params,st_asText(spatial) AS spatial_limit, list_definition,custom_function,column_label,
                    observationlist_mode,observationlist_time,tracklog_mode,periodic_notification,m.description as field_description,share_location
                FROM project_forms_data d
                LEFT JOIN project_forms f ON (d.form_id=f.form_id)
                LEFT JOIN project_metaname m ON (column_name="column" AND m.project_table=f.destination_table)
                WHERE d.form_id=%1$d AND f.project_table=%2$s AND \'api\'=ANY(form_type) AND f.project_schema=\'%4$s\' AND m.project_schema = \'%4$s\'
                ORDER BY idx(Array[%3$s],"column"::text)',
                    $form_id,
                    quote($this->project),
                    $order,
                    $schema);

            $form_data = array();
            $form_header = array("login_name"=>$_SESSION['Tname'],"login_email"=>$_SESSION['Tmail']);

            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                http_response_code(203);
                echo common_message('error','FORMDATA_IS_NOT_DEFINED');
                return;
            }

            //define permanent_sample_plots any way
            //$form_header['permanent_sample_plots'] = array();
            // It is moved to get_project_vars from API v2.4
            if ($this->API_VERSION > 2.0 and $this->API_VERSION < 2.4) {
                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=%d
                        ORDER BY name',
                            $this->project,
                            $_SESSION['Tid']);
                else
                    $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=0
                        ORDER BY name',
                                $this->project);

                $res3 = pg_query($ID,$cmd);
                $gopt = array();
                while($row3=pg_fetch_assoc($res3)) {
                    $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>$row3['geom']);
                }
                if (count($gopt)) {
                    $form_header['permanent_sample_plots'] = $gopt;
                }
            }
            elseif ($this->API_VERSION >= 2.4) {
                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id,name
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=%d
                        ORDER BY name',
                            $this->project,
                            $_SESSION['Tid']);
                else
                    $cmd = sprintf('SELECT id,name
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=0
                        ORDER BY name',
                            $this->project);

                $res3 = pg_query($ID,$cmd);
                $gopt = array();
                while($row3=pg_fetch_assoc($res3)) {
                    $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name']);
                }
                if (count($gopt)) {
                    $form_header['permanent_sample_plots'] = $gopt;
                }
            }
            // Filter polygons by form somehow ? 
            // forms should contains polygon selector...
            /*
             if ($modules->is_enabled('box_load_selection') and $this->API_VERSION > 2.4 ) {
                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE select_view IN (\'2\',\'3\',\'only-upload\',\'select-upload\') AND p.user_id=%d
                        ORDER BY id',$_SESSION['Tid']);
                else
                    $cmd = sprintf('SELECT id
                            FROM system.shared_polygons
                            WHERE access IN (\'4\',\'public\') OR (project_table=\'%s\' AND access IN (\'3\',\'project\'))
                            ORDER BY id',$this->project);

                $res3 = pg_query($ID,$cmd);
                $gopt = array();
                while($row3=pg_fetch_assoc($res3)) {
                    $gopt[] = array("id"=>$row3['id']);
                }
                if (count($gopt)) {
                    $form_header['permanent_sample_plots'] = $gopt;
                }
            }
            */

            $form_header['boldyellow'] = array();
            $bold_yellow_columns = array();
            // bold-yellow columns for uploaded data list columns
            if ($modules->is_enabled('bold_yellow',$destination_table) and $this->API_VERSION > 2.0) {
                $params = $modules->get_params('bold_yellow',$destination_table);
                if ($params != '') {
                    if ($this->API_VERSION>=2.5) 
                        $bold_yellow_columns = $params;
                    else
                        $form_header['boldyellow'] = $params;
                }
            }

            // number of individuals
            if ($this->API_VERSION>=2.5) 
                $form_header['num_ind'] = null;
            else
                $form_header['num_ind'] = $st_col['NUM_IND_C'];
            
            // observation list mode
            $form_header['tracklog_mode'] = '';
            $form_header['observationlist_mode'] = 'false';
            $form_header['observationlist_time_length'] = '';
            // periodic notification
            $form_header['periodic_notification_time'] = '';
            $form_header['share_location'] = '';

            if ($this->API_VERSION>=2.5) 
                $form_header['form_description'] = '';

            if ($this->API_VERSION>=2.6) 
                $form_header['share_location'] = isset($row['share_location']) ? $row['share_location'] : false;

            $target_col_list = [];
            $trigger_targets = array(); //target columns of triggered functions
            while($row = pg_fetch_assoc($res)) {

                if ($this->API_VERSION<2.1) {
                    if ($row['api_params']!='') {
                        $row['api_params'] = json_decode($row['api_params']);
                    }
                } elseif ($this->API_VERSION>2.0) {
                    $api_params = array('sticky'=>'off','hidden'=>'off','readonly'=>'off','list_elements_as_buttons'=>'off','once'=>'off','unfolding_list'=>'off');
                    if ($row['api_params']!='') {
                        $ap = json_decode($row['api_params']);
                        foreach ($ap as $ape) {
                            if ($ape=='sticky') $api_params['sticky'] = 'on';
                            elseif ($ape=='hidden') $api_params['hidden'] = 'on';
                            elseif ($ape=='readonly') $api_params['readonly'] = 'on';
                            elseif(preg_match('/^list_elements_as_buttons:?(.+)?/',$ape,$m)) {
                                if (isset($m[1]))
                                    $api_params['list_elements_as_buttons'] = $m[1];
                                else
                                    $api_params['list_elements_as_buttons'] = 'on';
                            }
                            elseif ($ape=='once') $api_params['once'] = 'on';
                            elseif ($ape=='unfolding_list') $api_params['unfolding_list'] = 'on';
                        }
                    }
                    $row['api_params'] = $api_params;
                }

                $json_genlist = '';
                if ($row['genlist']!='') {

                    $genlist_limit = defined('PDS_GENLIST_LIMIT') ? sprintf('LIMIT %d',constant('PDS_GENLIST_LIMIT')) : 'LIMIT 5000';
                    
                    $genlist_def = preg_split('/:/',$row['genlist']);
                    if (count($genlist_def)==2 and $genlist_def[0] == 'SELECT') {
                        //SELECT:table.column
                        if (preg_match('/SELECT:(\w+)\.(\w+):(\w+)/',$row['genlist'],$m)) {
                            //SELECT:table.column:label
                            $bar = sprintf("%s AS bar",$m[2]);
                            $baro = $m[2];
                            if (isset($m[3]) and $m[3]!='') {
                                $bar = sprintf("%s AS bar",$m[3]);
                                $baro = $m[3];
                            }
                            $cmd = sprintf('SELECT DISTINCT %1$s foo, %3$s FROM public.%2$s ORDER BY %4$s %5$s',
                                $m[2],
                                $m[1],
                                $bar,
                                $baro,
                                $genlist_limit);
                            $res3 = pg_query($ID,$cmd);
                            $out = array();
                            while ($row3 = pg_fetch_assoc($res3)) {
                                $out[$row3['foo']] = $row3['bar']; # label not handled!!!
                            }
                            $json_genlist = $out;

                        } else {
                            list($table,$column)=explode(".",$genlist_def[1]);
                            $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s" FROM public."%2$s" ORDER BY %1$s %3$s',
                                $column,
                                $table,
                                $genlist_limit);
                            $res3 = pg_query($ID,$cmd);
                            if (pg_num_rows($res3))
                                $x = pg_fetch_all($res3);
                            $json_genlist = array_column($x,$column);
                        }
                    } elseif (count($genlist_def)>1) {
                        //id:label
                        $genlist_elements = preg_split('/,/',$row['genlist']);
                        $out = array();
                        foreach($genlist_elements as $ge) {
                            $ps = preg_split('/:/',$ge);
                            if (count($ps)==2)
                                $out[$ps[0]] = $ps[1];
                        }
                        $json_genlist = $out;

                    } elseif (preg_match('/\./',$row['genlist'])) {
                        // backward compatibility
                        // table.column
                        list($table,$column) = explode(".",$row['genlist']);
                        $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s" FROM public."%2$s" ORDER BY %1$s %3$s',
                            $column,
                            $table,
                            $genlist_limit);
                        $res3 = pg_query($ID,$cmd);
                        if (pg_num_rows($res3))
                            $x = pg_fetch_all($res3);
                        $json_genlist = array_column($x,$column);

                    } else {
                        // normal list
                        $e = preg_split('/,/',$row['genlist']);
                        $result = preg_grep('~' . $_POST['term'] . '~', $e);
                        $json_genlist = $result;
                    }
                    $row['genlist'] = $json_genlist;

                }
                elseif ($row['list_definition']=='' and $row['type']=='list') {
                    // 
                    // old forms, before the list_definition 
                    // backward compatibility
                    //
                    $sel = new upload_select_list($row);
                    $prelist = $sel->get_options('array');
                    $json_genlist = array();

                    foreach ($prelist as $pe) {
                        // prepend whitespace for numeric values, to keep associative array keys
                        if (is_integer($pe['value'])) $pe['value'] = " ".$pe['value'];
                            
                        if ($this->API_VERSION < 2.1)
                            $json_genlist[$pe['label'][0]] = $pe['value'];
                        elseif ($this->API_VERSION < 2.3)
                            $json_genlist[$pe['value']] = $pe['label'][0];
                        else {
                            # >= 2.3
                            $json_genlist[] = array($pe['value']=>$pe['label'][0]);
                        }
                    }

                    $row['list'] = $json_genlist;

                } elseif (
                    $row['list_definition']!='' and (
                        $row['type']=='list' or  $row['type']=='autocomplete' or $row['type']=='autocompletelist')) {

                    /* Kapcsolt lista példa
                     * {
                            "optionsSchema": "shared",
                            "optionsTable": "lista1",
                            "valueColumn": "lista1_id",
                            "labelColumn": "value",
                            "Function": "select_list"
                            "triggerTargetColumn": [
                                "objectid"
                            ],
                        }
                        {
                            "optionsSchema": "shared",
                            "optionsTable": "lista2",
                            "valueColumn": "value",
                            "filterColumn": "lista1_id",      <- előző listából jön
                            "Function": "select_list",
                            "triggerTargetColumn": [
                                ""
                            ]
                        }
                     */

                    $list_definition = json_decode($row['list_definition'],true);
                    $has_filterColumn = (isset($list_definition['filterColumn']));

                    // normal new list method
                    $sel = new upload_select_list($row, '', $has_filterColumn);

                    //$target_col_list = []; // miért csak egy elemű ez a tömb???
                    if ($has_filterColumn) {
                        $target_col_list[$row['column']] = $sel;
                    }

                    if (isset($list_definition['triggerTargetColumn'])) {
                       foreach ($list_definition['triggerTargetColumn'] as $ttc) {
                            if ($ttc !='' ) {
                                $trigger_targets[] = array("source" => $row['column'], "target" => $ttc);
                            }
                       }
                    }

                    $prelist = (!$has_filterColumn) ? $sel->get_options('array') : [];

                    $json_genlist = array();

                    foreach ($prelist as $pe) {
                        // prepend whitespace for numeric values, to keep associative array keys
                        if (is_integer($pe['value'])) $pe['value'] = " ".$pe['value'];
                            
                        if ($this->API_VERSION < 2.1)
                            $json_genlist[$pe['label'][0]] = $pe['value'];
                        elseif ($this->API_VERSION < 2.3)
                            $json_genlist[$pe['value']] = $pe['label'][0];
                        else {
                            # >= 2.3
                            $json_genlist[] = array($pe['value']=>$pe['label'][0]);
                        }
                    }

                    $triggerTargetColumn = $list_definition['triggerTargetColumn'] ?? [];
                    $Function = $list_definition['Function'] ?? '';
                    
                    $row['list_definition'] = array(
                        'multiselect'=>$sel->multiselect,
                        'selected'=>$sel->selected,
                        'triggerTargetColumn'=>$triggerTargetColumn,
                        'Function'=>$Function);

                    // Is the autocomplete/autocomple_list is extendable with user input?
                    if ($this->API_VERSION>=2.5) {
                        $row['list_definition']['extendable'] = $sel->extendable;
                    }
                    
                    if ($row['type']=='list') 
                        $row['list'] = $json_genlist;
                    else
                        $row['genlist'] = array_column($prelist,'value');
                }

                // shared polygons as permanent_sample_plots
                // "permanent_sample_plots":"[{
                //	"geometry": "POINT(-72.6945975075 42.34444296)",
                //	"name": "Obs. G43,12h"
                //}, { ...

                if ($this->API_VERSION < 2.1) {
                    if ($row['type']=='point' or $row['type']=='polygon' or $row['type']=='line' or $row['type']=='wkt') {
                        if (isset($_SESSION['Tid']))
                            $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                                FROM system.shared_polygons
                                LEFT JOIN system.polygon_users p ON polygon_id=id
                                WHERE project_table=\'%s\' select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=%d
                                ORDER BY name',
                                    $this->project,
                                    $_SESSION['Tid']);
                        else
                            $cmd = sprintf('SELECT id,name,st_asText(geometry) as geom
                                FROM system.shared_polygons
                                LEFT JOIN system.polygon_users p ON polygon_id=id
                                WHERE project_table=\'%s\' select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=0
                                ORDER BY name',
                                    $this->project);

                        $res3 = pg_query($ID,$cmd);
                        $gopt = array();
                        while($row3=pg_fetch_assoc($res3)) {
                            $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>$row3['geom']);
                        }
                        if (count($gopt)) {
                            $row['permanent_sample_plots'] = $gopt;
                        }
                    }
                }

                // column label
                if ($row['column_label']!='') {
                    $row['short_name'] = $row['column_label'];
                }
                if ($row['short_name']=='')
                    $row['short_name'] = $row['column'];
                else {
                    $translated = $row['short_name'];
                    if (defined($row['short_name'])) $translated = constant($row['short_name']);

                    $row['short_name'] = $translated;
                }

                // default user email
                if ($row['default_value'] == '_email') {
                    if (isset($_SESSION['Tid'])) {
                        $row['default_value'] = $_SESSION['Tmail'];
                    } else{
                        $row['default_value'] = "_input";
                    }
                } elseif ($row['default_value'] == '_login_name') {
                    if (isset($_SESSION['Tid'])) {
                        $row['default_value'] = $_SESSION['Tname'];
                    } else{
                        $row['default_value'] = "_input";
                    }
                } elseif ($row['default_value'] == '_login_email') {
                    if (isset($_SESSION['Tid'])) {
                        $row['default_value'] = $_SESSION['Tmail'];
                    } else{
                        $row['default_value'] = "_input";
                    }
                }

                if ($this->API_VERSION>2.0) {

                    // magic worlds replaced to null, because Mobile client does not process it just transform it to simple input field
                    //if (in_array($row['default_value'],array('_input','_list','_email','_login_name','_geometry','_datum','_none','_attachment','_autocomplete','_boolean')) and $row['default_value']!='') {
                    if (in_array($row['default_value'],array('_input','_list','_email','_login_name','_geometry','_datum','_none','_attachment','_autocomplete','_boolean','_time'))) {
                        //if ($row['default_value'] != '') {
                            // that means, it has a default value
                            //$row['api_params']['once']="on";
                        //}
                        $row['default_value'] = null;
                    }
                    elseif ($row['default_value'] == '_auto_geometry') {
                        //$row['api_params']['once']="on";
                        $row['default_value'] = null;
                        $row['api_params']['auto_geometry'] = "on";
                    }
                    //elseif ($row['default_value'] != '') {
                    //    $row['api_params']['once'] = "on";
                    //}
                }
                /*
                {
                    "column": "altema",
                    "short_name": "altema",
                    "list": {
                      "1": "abc",
                      "2": "abcd
                    },
                    ...
                */

                if ($row['observationlist_mode']!='')
                    $form_header['observationlist_mode'] = $row['observationlist_mode'];
                if ($row['tracklog_mode']!='')
                    $form_header['tracklog_mode'] = $row['tracklog_mode'];
                $form_header['observationlist_time_length'] = $row['observationlist_time'];
                $form_header['periodic_notification_time'] = $row['periodic_notification'];
                unset($row['observationlist_time']);
                unset($row['observationlist_mode']);
                unset($row['tracklog_mode']);
                unset($row['periodic_notification']);
                unset($row['share_location']);

                if ($this->API_VERSION>=2.5) {
                    // update num_inds field
                    if ($row['column'] == $st_col['NUM_IND_C'])
                        $form_header['num_ind'] = $st_col['NUM_IND_C'];
                    
                    // update bold_yellow array
                    if (in_array($row['column'],$bold_yellow_columns))
                        $form_header['boldyellow'][] = $row['column'];

                    $form_header['form_description'] = $row['form_description'];
                }
                unset($row['form_description']);

                $form_data[] = $row;
            }

            $form_cols = array_column($form_data,'column');

            foreach($target_col_list as $tv => $sel) {
                $idx = array_search($tv,$form_cols);
                $row = $form_data[$idx];
                $prelist = $sel->get_options('array');

                $row['filtering_list'] = new stdClass();
                //$row['filtering_list']->list_fkeys = array();
                $row['filtering_list']->list_labels = array_column(array_column($prelist,'label'),0);
                $row['filtering_list']->list_values = array_column($prelist,'value');
                $row['filtering_list']->filter_values = array_column($prelist,'filter');
                $tvkey = array_search($tv, array_column($trigger_targets, 'target'));
                $row['filtering_list']->filter_selector = array($trigger_targets[$tvkey]['source']);
                $form_data[$idx] = $row;
            }

            #proposed api variables:
            #$form_header["observationlist_mode"] = "force";
            #$form_header["observationlist_time_length"] = "900";
            #$form_header["observationlist_timeout_text"] = "Observation time has expired, turn off the list.";

            if ($this->API_VERSION>2.0) {
                echo common_message('ok',array("form_header"=>$form_header,"form_data"=>$form_data));
            } else {
                echo common_message('ok',$form_data);
            }
            return;

        } elseif ($key == 'get_attachments') {
            
            // GET ATTACHMENTS ========================================->

            $dir = opendir(getenv('PROJECT_DIR').'local/attached_files');
            $files = array();
            while (false !== ($fname = readdir($dir)))
            {
                if (is_file(getenv('PROJECT_DIR').'local/attached_files/'.$fname))
                {
                    $files[] = $fname;
                }
            }
            $cmd = '';

            $limit = '';
            $limit_name = '';
            $filter = array();
            
            $values = preg_split('/&/',$value);

            foreach ($values as $value) {

                if (preg_match('/limit=(\d+):(\d+)/',$value,$m)) {
                    $limit = 'LIMIT '.$m[1].' OFFSET '.$m[2];
                    $limit_name = '_'.$m[1].'-'.$m[2];
                }
                elseif (preg_match('/filter=(\w+)([!=]+)([A-Za-z0-9-_% "]+)/i',$value,$m)) {

                    $obm_columns = array_merge(dbcolist('columns',$this->table),array('obm_id'));
                    if (in_array($m[1],$obm_columns)) {
                        
                        # Simple SQL Query Processing
                        if (preg_match('/^%|%$/',$m[3])) {
                            if ($m[2] == '!=')
                                $filter[] = 'k.'.$m[1].' NOT LIKE '.quote($m[3]);
                            else
                                $filter[] = 'k.'.$m[1].' LIKE '.quote($m[3]);
                        } else {
                            if ($m[2] == '!=')
                                $filter[] = 'k.'.$m[1].'!='.quote($m[3]);
                            else
                                $filter[] = 'k.'.$m[1].'='.quote($m[3]);
                        }
                    }
                }
            }
            if (count($filter))
                $filter = 'AND '.implode(' AND ',$filter);
            else
                $filter = '';


            $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum
                        FROM system.files f
                        LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                        LEFT JOIN %1$s k ON (k.obm_files_id=fc.conid)
                        WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                               (k.obm_files_id IS NULL AND f.data_table=\'%1$s\') ) %5$s
                        GROUP BY f.id,f.reference,f.comment
                        ORDER BY f.datum %4$s',$this->table,'obm_id',$this->project,$limit,$filter);
            $res = pg_query($ID,$cmd);

            if (pg_num_rows($res) == 1) {
                $row = pg_fetch_assoc($res);

                $key = array_search($row['reference'],$files);
                if ($key!==false) {    
                    $fname = $files[$key];
                    $outputfile = getenv('PROJECT_DIR').'local/attached_files/'.$fname;
                    if (file_exists($outputfile)) {
                        $file = resize_image($outputfile,500);

                        $imageData = base64_encode($file);

                        header('Content-Description: File Transfer');
                        header('Content-Type: image/jpeg');
                        header('Content-Disposition: inline; filename="'.$fname.'"');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . strlen($imageData));
                        echo $imageData;
                    }
                }

            } else {

                $archive = 'attached_files_'.$this->table.$limit_name.'.tar';
                $a = new PharData(getenv('PROJECT_DIR').'local/attached_files/'.$archive);
                $tarfile = getenv('PROJECT_DIR').'local/attached_files/'.$archive;

                while ($row = pg_fetch_assoc($res)) {

                    $key = array_search($row['reference'],$files);
                    if ($key!==false) {    
                        $fname = $files[$key];
                        #debug($fname.' '.$row['conid']);
                        $conids = preg_split('/,/',$row['conid']);
                        foreach($conids as $conid) {
                            $valid_filename = preg_replace('/[*]/','_',$conid.'_'.$fname);
                            #debug($valid_filename);
                            $a->addFile(getenv('PROJECT_DIR').'local/attached_files/'.$fname,$valid_filename);
                        }
                    }
                }
                if (file_exists($tarfile)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.$archive.'"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($tarfile));
                    readfile($tarfile);
                    unlink($tarfile);
                }
            }

        /*} elseif ($key == 'upload_test' and $this->API_VERSION=='1.0') {
            // NOT USED ANY MORE

            // print out the uploaded data
            // TEST!!!
            $cmd = "SELECT id,datum,comment,project_table,ST_AsText(geometry) as geom,files,error FROM pds_upload_data ORDER BY id DESC";
            $res = pg_query($BID,$cmd);
            echo "datum - comment - project-table - geometry - files - errors<br>";
            while ($row = pg_fetch_assoc($res)) {
                echo $row['id'].'.&nbsp; &nbsp; '.$row['datum'].' - '.$row['comment'].' - '.$row['project_table'].' - '.$row['geom'].' - '.$row['files'].' - '.$row['error']."<br>";
            }
            return;
         */
        } elseif ($key == 'upload' and $this->API_VERSION<1.3) {


            // UPLOAD DATA ========================================->

            if (!isset($this->project) or $this->project=='') return;

            /* _POST upload into temporary table using API
             * It was the old API upload method, but can be used with some modification and with a new `temp upload` control parameter
             * */
            $response = array("error"=>array(),"result"=>array());

           ##     $user_id = $this->hs_req[0];
           ##     $project_table = $this->hs_req[1];
           ##     $form_id = $this->hs_req[2];

            //Processing uploaded data somehow....

            if ($value == 'mapp') {
                $geoms = preg_replace('/[^POINTLEYG0-9.() ]/','',$_POST['m_geometry']);
                $dates = preg_replace('/[^0-9.-\/ ]/','',$_POST['m_datum']);
                $comms = $_POST['m_comment'];
            }

            if ($value == 'mapp') {
                $file = 'm_file0';
            }

            // nincs ilyen most!
            if ( $value == 'japp') {
                $j=json_decode($value);
                if (!is_object($j)) {
                    $response['error'][] = "$value is not a valid json object!";
                }
            }

            if(!is_array($geoms)) $geoms = (array) $geoms;
            if(!is_array($dates)) $dates = (array) $dates;
            if(!is_array($comms)) $comms = (array) $comms;
            $good_data = 0;
            if ($geoms[0]=='')
                $response['error'][] = "empty geometry!";

            for($i=0;$i<count($geoms);$i++) {
                $file_dest = OB_TMP.$this->hs_req[1]."/".$this->hs_req[0]."/".$this->hs_req[2];
                if (isset($_FILES['m_file0']['tmp_name'][$i]) and $_FILES['m_file0']['tmp_name'][$i]!='')
                    move_upl_files($_FILES['m_file0'],$file_dest);

                if($value=='japp') {
                    $file = "m_file$i";
                }

                $g = $geoms[$i];
                $d = $dates[$i];
                $c = $comms[$i];
                if (!is_wkt($g)){
                    $response['error'][] = "$g is not a valid WKT string";
                    $g = '';
                }
                $files = '{'.join(',',$_FILES[$file]['name']).'}';
                //swap wkt x,y: ST_FlipCoordinates
                //should be optional
                $cmd = sprintf("INSERT INTO pds_upload_data (datum,comment,geometry,error,files,user_id,project_table,form_id) VALUES (%s,%s, ST_FlipCoordinates(ST_GeomFromText(%s, 4326)),%s,%s,%d,'%s',%d)",quote($d),quote($c),quote($g),quote(implode($response['error'])),quote($files),$user_id,$this->project,$form_id);
                $res = pg_query($BID,$cmd);
                if (pg_affected_rows($res)) {
                    $good_data++;
                    $response['result'][] = "Successfully uploaded";
                } else {
                    $response['error'][] = pg_last_error($BID);
                }
            }
            /*if (count($response['error'])) {
                echo implode("\n",$response['error']);
            }
            echo implode("\n",$response['result']);*/
            echo json_encode($response);
            return;

        } elseif ($key == 'get_tables_data') {
            /* API_VERSION > 1.3 */
            /* Get list of tables of a project */

            if (!isset($this->project) or $this->project=='') {
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                http_response_code(400);
                return;
            }

            $cmd = sprintf('SELECT f_project_table as mt FROM header_names WHERE f_project_name=%s',quote($this->project));
            $res = pg_query($BID,$cmd);

            // fetch table structure of a single table
            if ($value!='') {
                $mt = array();
                while ($row = pg_fetch_assoc($res)) {
                    $mt[] = $row['mt'];
                }
                if (in_array($value,$mt)) {
                    $cmd = sprintf("SELECT column_name, data_type FROM information_schema.columns WHERE table_name = %s",quote($value));
                } else {
                    $cmd = sprintf("SELECT %s", quote($this->project));
                }
                $res = pg_query($ID,$cmd);
            }
            /*$mt = array();
            while ($row = pg_fetch_assoc($res)) {
                $mt[] = preg_replace('/'.$this->project.'_/','',$row['mt']);
            }*/
            echo '{"status":"success","data":';
            echo "[";
            $p = pg_num_rows($res);
            $n = 0;
            //tricky way to fetch large respones
            while ($row = pg_fetch_assoc($res)) {
                $n++;
                if (isset($row['mt'])) {
                    // table list
                    // remove projectname_ from table names!!!
                    echo '"'.preg_replace('/'.$this->project.'_/','',$row['mt']).'"';

                } else {
                    // column list
                    echo '{"column_name":"'.$row['column_name'].'","data_type":"'.$row['data_type'].'"}';
                
                }
                if($n<$p) echo ",";
            }
            echo "]}";
            return;

        } elseif ($key == 'get_report') {
            /* Perform custom query */

            if(!isset($this->project) or $this->project=='') return;
            // lekérdezési eredmény típusa
            if (!isset($this->request['type']))
                $this->request['type'] = 'json';

            $response = reportQuery($value,$this->project);

            $j = json_decode($response);

            if ($j->{'status'}=='error') {
                
                $this->request_error = $response;
                http_response_code(400);
                return;

            } else if ($j->{'status'}=='fail') {
                
                $this->request_error = $response;
                http_response_code(400);
                return;

            } else {
                $cmd = $j->{'data'};
                $result = pg_query($ID,$cmd);
                if (pg_last_error($ID)) {
                    $errorID = uniqid();
                    
                    http_response_code(202);
                    echo common_message('fail','PROCESSING_FAILED',$errorID);
                    log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);

                    return;
                }
                if (!pg_num_rows($result)) {
                    
                    http_response_code(204);
                    echo common_message('error','NO_DATA_RETURNED');
                    return;
                }

                if ($this->request['type']=='xml') {
                    // collapse with large arrays!!!
                    $this->results = pg_fetch_all($result);
                    // creating object of SimpleXMLElement
                    $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><OBM_data></OBM_data>");
                    // function call to convert array to xml
                    array_to_xml($this->results,$xml);
                    // print out XML
                    header('Content-type: text/xml');
                    print $xml->asXML();
                    return;
                } else {
                    //defult request type is JSON
                    //common message simulation
                    echo '{"status":"success","data":';
                    echo "[";
                    $p = pg_num_rows($result);
                    $n = 0;
                    //tricky way to fetch large respones
                    while ($row = pg_fetch_row($result)) {
                        $n++;
                        echo $row[0];
                        if($n<$p) echo ",";
                    }
                    echo "]}";
                    return;
                }
            }

        } elseif ($key == 'get_project_list') {

            /* Get list of active local projects
             *
             * Listing those project where user is member or has upload rights!!
             * scope=get_project_list&value=2
             *
             * */
            if ($value['project']=='all-projects')
                $a = "";
            elseif ($value['project']!='')
                $a = " AND p.project_table=".quote($value['project']);
            else
                $a = ""; //all-projects

            // <  API v2.5 all
            // >= API v2.5 1
            if ($value['access_options']=='all' or $value['access_options']=='1') {
                // query all projecs regardless if them accessible or not
                // this is the default from 2019.06.25
                $cmd = sprintf("
                    SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                        protocol || '://' || domain || '/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                    FROM projects p
                    LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                    LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                    WHERE local_project=TRUE AND stage NOT IN ('abandoned','intermittent') AND project_hash!='' AND project_hash IS NOT NULL $a
                    GROUP BY p.project_table,pd.short,training,rserver,language
                    ORDER BY p.project_table");

            } else {
                // $value['access_options']=='accessible'

                if (!isset($_SESSION['Tid'])) {

                    $cmd = sprintf("
                            SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                                protocol || '://' || domain || '/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                            FROM projects p
                            LEFT JOIN project_forms pf ON (p.project_table = pf.project_table)
                            LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                            LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                            WHERE local_project=TRUE AND stage NOT IN ('abandoned','intermittent') AND project_hash!='' AND project_hash IS NOT NULL AND form_access=0 $a
                            GROUP BY p.project_table,pd.short,training,rserver,language
                            ORDER BY p.project_table");
                } else {
                    //query those projects which are accessible for the given user and those which have public forms

                    $cmd = sprintf("SELECT p.project_table,creation_date,\"Creator\",email,stage,doi,running_date,licence,rum,collection_dates,subjects,project_hash,
                                protocol  || '://' || domain || '/' AS project_url,pd.short AS project_description,'-' AS public_mapserv,training,rserver,language
                            FROM projects p
                            LEFT JOIN project_users pu ON (p.project_table = pu.project_table)
                            LEFT JOIN project_forms pf ON (p.project_table = pf.project_table)
                            LEFT JOIN project_descriptions pd ON (p.project_table = pd.projecttable)
                            LEFT JOIN project_variables pv ON (pv.project_table = p.project_table)
                            WHERE local_project=TRUE AND stage NOT IN ('abandoned','intermittent') AND project_hash!='' AND project_hash IS NOT NULL AND (pu.user_id=%d OR form_access=0) $a
                            GROUP BY p.project_table,pd.short,training,rserver,language
                            ORDER BY p.project_table",$_SESSION['Tid']);
                }
            }

            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {

                http_response_code(204);
                // No local projects available
                echo common_message('error','NO_DATA_RETURNED');
                return;
            }

            echo '{"status":"success","data":';
            echo "[";

            $rows = array();
            $projects = array();
            while ($row = pg_fetch_assoc($res)) {
                //$row['project_description'] = preg_replace("/'/","&apos;",$row['project_description']); # ' problem in React
                if (!isset($projects[$row['project_table']]))
                    $projects[$row['project_table']] = $row;
                else {
                    if ($row['language']==$_SESSION['LANG'])
                        $projects[$row['project_table']] = $row;
                }
            }

            $p = count($projects);
            $n = 0;
            foreach ($projects as $row) {
                $n++;
                // API 2.0 compatibility
                if ($row['training'] == 't') $row['game'] = 'on';
                else $row['game'] = 'off';

                if ($row['rserver']== 't') {
                    if (defined('RSERVER_PORT_'.$this->project))
                        $row['rserver_port'] = constant('RSERVER_PORT_'.$this->project);
                    else
                        $row['rserver_port'] = 0;
                } else {
                    $row['rserver_port'] = 0;
                }

                echo json_encode($row,JSON_UNESCAPED_SLASHES);
                if($n<$p) echo ",";
            }
            echo "]}";
            return;

        } elseif ($key == 'get_project_vars') {

            if (!isset($this->project) or $this->project=='') {
                http_response_code(400);
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                return;
            }

            if (defined('PUBLIC_MAPSERV'))
                $public_mapserv = PUBLIC_MAPSERV;

            // local_vars.php query
            $cmd = sprintf("SELECT st_asText(map_center) AS map_center,map_zoom,click_buffer,zoom_wheel,'$public_mapserv' AS public_mapserv,
                                    default_layer,fixheader,turn_off_layers,subfilters,langs,legend,training,rserver
                            FROM project_variables
                            WHERE project_table='%s'",$this->project);
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                
                http_response_code(204);
                echo common_message('error','NO_DATA_RETURNED');
                return;

            }

            $permanent_sample_plots = array();
            if ($this->API_VERSION >= 2.4) {
                if (isset($_SESSION['Tid']))
                    $cmd = sprintf('SELECT id,name,st_asGeoJSON(geometry) as geom
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=%d
                        ORDER BY name',
                            $this->project,
                            $_SESSION['Tid']);
                else
                    $cmd = sprintf('SELECT id,name,st_asGeoJSON(geometry) as geom
                        FROM system.shared_polygons
                        LEFT JOIN system.polygon_users p ON polygon_id=id
                        WHERE project_table=\'%s\' AND select_view IN (\'only-upload\',\'select-upload\') AND p.user_id=0
                        ORDER BY name',
                            $this->project);

                $res3 = pg_query($ID,$cmd);
                $gopt = array();
                while($row3=pg_fetch_assoc($res3)) {
                    if ($this->API_VERSION >= 2.5) 
                        $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>transformGeoJson($row3['geom'])); // non-geoJSON
                    else
                        $gopt[] = array("id"=>$row3['id'],"name"=>$row3['name'],"geometry"=>$row3['geom']); // standard geoJSON
                }
                if (count($gopt)) {
                    $permanent_sample_plots = $gopt;
                }
            }


            if ($this->API_VERSION >= 2.4) {

                $row = (pg_num_rows($res)) ? pg_fetch_assoc($res) : array();

                if (isset($row['training']) and $row['training'] == 't') 
                    $row['game'] = 'on';
                else 
                    $row['game'] = 'off';

                $ALLOWED_FILE_SIZE = defined('ALLOWED_FILE_SIZE') ? ALLOWED_FILE_SIZE : ini_get('upload_max_filesize');
                if (preg_match('/(\d+)M$/',$ALLOWED_FILE_SIZE,$m))
                    $ALLOWED_FILE_SIZE = $m[1] * 1024 * 1024;

                $row['allowed_file_size'] = $ALLOWED_FILE_SIZE;

                $row['permanent_sample_plots'] = $permanent_sample_plots;

                if (isset($row['rserver']) and $row['rserver']== 't')
                    $row['rserver_port'] = constant('RSERVER_PORT_'.$this->project);
                else
                    $row['rserver_port'] = 0;

                echo '{"status":"success","data":' . json_encode($row,JSON_UNESCAPED_SLASHES) . '}';
            } else {
                echo '{"status":"success","data":';
                echo "[";
                    $p = pg_num_rows($res);
                    $n = 0;
                    while ($row = pg_fetch_assoc($res)) {
                        if ($row['training'] == 't') $row['game'] = 'on';
                        else $row['game'] = 'off';

                        $ALLOWED_FILE_SIZE = defined('ALLOWED_FILE_SIZE') ? ALLOWED_FILE_SIZE : ini_get('upload_max_filesize');
                        if (preg_match('/(\d+)M$/',$ALLOWED_FILE_SIZE,$m)) {
                            $ALLOWED_FILE_SIZE = $m[1] * 1024 * 1024;
                        }
                        $row['allowed_file_size'] = $ALLOWED_FILE_SIZE;

                        if ($this->API_VERSION >= 2.4) {
                            $row['permanent_sample_plots'] = $permanent_sample_plots;
                        }

                        if ($row['rserver']== 't') {
                            $row['rserver_port'] = constant('RSERVER_PORT_'.$this->project);
                        } else {
                            $row['rserver_port'] = 0;
                        }

                        $n++;
                        echo json_encode($row,JSON_UNESCAPED_SLASHES);
                        if($n<$p) echo ",";
                    }
                echo "]}";
            }
            return;

        } elseif ($key == 'get_trainings') {

            if (!isset($this->project) or $this->project=='') {
                $this->request_error = 'PROJECT_IS_NOT_DEFINED';
                http_response_code(400);
                return;
            }

            $cmd = sprintf("SELECT  id,form_id,html,task_description,enabled,title,qorder,project_table FROM form_training WHERE project_table='%s' ORDER BY qorder",$this->project);
            $res = pg_query($BID,$cmd);
            echo '{"status":"success","data":';
            echo json_encode(pg_fetch_all($res),JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            echo "}";
            return;
        } elseif ($key == 'get_training_questions') {

            if(!isset($this->project) or $this->project=='') return;

            $cmd = sprintf("SELECT qid,training_id,caption,answers,qtype FROM form_training_questions WHERE training_id=%d ORDER BY qid",$value);
            $res = pg_query($BID,$cmd);
            if (!pg_num_rows($res)) {
                http_response_code(204);
                echo common_message('error','NO_DATA_RETURNED');
                return;
            }

            echo '{"status":"success","data":';
            $b = array();
            while ($row = pg_fetch_assoc($res)) {
                $b[] = array("qid"=>$row['qid'],"training_id"=>$row['training_id'],"caption"=>'<h4>'.$row['caption'].'</h4>',"answers"=>json_decode($row['answers'],true),"qtype"=>$row['qtype']);
            }
            echo json_encode($b,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            echo "}";

            return;
        } elseif ($key == 'training_results') {

            if(!isset($this->project) or $this->project=='') return;

            // check user passed the trainings?
            $cmd = sprintf("SELECT form_id FROM form_training WHERE project_table='%s' ORDER BY qorder",$this->project);
            $res = pg_query($BID,$cmd);
            $training_forms = array();
            while ($row = pg_fetch_assoc($res)) {
                $training_forms[$row['form_id']] = -1; //untreated
                $cmd = sprintf("SELECT validation FROM system.uploadings WHERE form_id=%d AND project_table='%s' AND uploader_id=%d",
                        $row['form_id'],$this->project,$_SESSION['Trole_id']);
                $res2 = pg_query($ID,$cmd);
                while ($row2 = pg_fetch_assoc($res2)) {
                    if ($row2['validation']>0.6)
                        $training_forms[$row['form_id']] = 1; //done
                    else
                        $training_forms[$row['form_id']] = 0; //failed
                }
            }
            echo common_message('ok',$training_forms);

            return;
        } elseif ($key == 'training_toplist') {
            if(!isset($this->project) or $this->project=='') return;

            // toplist....
            $cmd = sprintf("SELECT string_agg(form_id::varchar,',') AS form_id FROM form_training WHERE project_table='%s'",$this->project);
            $res = pg_query($BID,$cmd);
            $training_top = array();
            $form_id_list = "";
            while ($row = pg_fetch_assoc($res)) {
                $form_id_list = $row['form_id'];
            }

            if ($value=='nonames') {
                foreach(explode(',',$form_id_list) as $form_id ) {
                    $cmd = sprintf("SELECT sum(validation)/count(validation) AS mean,count(id) AS count,max(validation) AS max 
                                        FROM system.uploadings WHERE form_id = %d AND project_table='%s'",
                                $form_id,$this->project);
                    $res2 = pg_query($ID,$cmd);
                    while ($row2 = pg_fetch_assoc($res2)) {
                        $training_top[$form_id] = array("mean"=>$row2['mean']*100,"count"=>$row2['count'],"max"=>$row2['max']*100);
                    }
                }
            } else {
                $cmd = sprintf("SELECT sum(validation)/count(validation) AS mean,count(id) AS count,max(validation) AS max,uploader_name 
                                        FROM system.uploadings WHERE form_id IN (%s) AND project_table='%s' GROUP BY uploader_name",
                                $form_id_list,$this->project);
                $res2 = pg_query($ID,$cmd);
                while ($row2 = pg_fetch_assoc($res2)) {
                        $training_top[$row2['uploader_name']] = array("mean"=>$row2['mean']*100,"count"=>$row2['count'],"max"=>$row2['max']*100);
                }

            }
            echo common_message('ok',$training_top);

            return;
        } elseif ($key == 'pg_user') {

            // create or query pg_user

            if ($xmodules->is_enabled('create_pg_user') and $this->API_VERSION > 1.0) {
                $username = preg_replace('/[@.-]/','_',$_SESSION['Tmail']);

                $res = pg_query($BID,sprintf('SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = \'%s\'',$username));
                if (pg_num_rows($res)) {
                    echo common_message('ok',array("username"=>$username));
                } else {
                    echo $xmodules->_include('create_pg_user','create_pg_user');
                }
            } else {
                http_response_code(404);
                echo common_message('error',"MODULE_IS_NOT_ENABLED");
            }

            return;

        } elseif ($key == 'get_tile_list') {
            # Mit szólsz ha egy api hívással elérhetővé teszem a zip-ek listáját, méretét, dátumát és cimkéjét? Ebből tudnál egy listát csinálni a telefonon amit tud a felhasználó használni....
            #

            if ($value=='') {
                // getlist
                $list = array();

                $tile_services = json_decode(file_get_contents('https://openbiomaps.org/projects/openbiomaps_network/?qtable=openbiomaps_network_tileservers&query_api={%22available%22:%22true%22}&output=json&filename='),true);


                foreach ($tile_services as $serv) {
                    $list[] = $serv['regio'];
                }

                $local_tiles = array();

                foreach ($local_tiles as $serv) {
                    $list[] = $serv['regio'];
                }

                echo json_encode($list);

            } elseif (isset($_POST['list'])) {

                $regios[$new_regio_label] = $regio_extent;

                //store_available_regios($regios);
            }

        } elseif ($key == 'get_tile_zip') {

            $regio = $value;

            # 47.0,22.0_46.0,23.0_12:13_hungary.zip
            # 47.0,22.0_46.0,23.0_11:12_hungary.zip
            #
            #
            $filename = "";
            foreach (glob("zipfiles/*_$value.zip") as $zip) {
                $filename = $zip;
            }

            if (file_exists($filename)) {
                header('Content-Type: application/zip');
                header('Content-Disposition: attachment; filename="'.basename($filename).'"');
                header('Content-Length: ' . filesize($filename));

                flush();
                readfile($filename);
            }

        } elseif ($key == 'get_message_count') {    
            
            $M = new Messenger();
            echo common_message('ok',$M->get_unread_numbers(true));

        } elseif ($key == 'get_notification') {    
            
            //$M = new Observation_notification(); not exists yet
            echo common_message('ok',array("UUID"=>"0","DATA"=>"","GEOMETRY"=>""));

        } elseif ($key == 'connect_with_shared_link') {

            $response = validate_shared_link($value);

            $j = json_decode($response);

            if ($j->{'status'}=='error') {
                $this->request_error = $response;
                http_response_code(401);
                return;

            } else if ($j->{'status'}=='fail') {
                $this->request_error = $response;
                http_response_code(401);
                return;

            } else {
                // Client Credentials authentication
                $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

                if (defined('OAUTHURL'))
                    $url = OAUTHURL.'/oauth/token.php';
                else
                    $url = $protocol.'://'.URL.'/oauth/token.php';

                if (defined("OBM_PDS_CLIENT_SECRET")) 
                    $client_secret = constant("OBM_PDS_CLIENT_SECRET");
                else
                    $client_secret = '';

                $data = array('client_id'=>'obm', 'client_secret'=>$client_secret, 'scope'=>'get_data', 'grant_type'=>'client_credentials');

                $options = array(
                        'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($data)
                        )
                );

                $context  = stream_context_create($options);
                $result = file_get_contents($url, false, $context);

                if ($result === FALSE) {
                    /* Handle error */
                    http_response_code(401);
                    log_action("Auth failed with obm oauth client",__FILE__,__LINE__);
                    return;
                } else {
                    echo common_message('ok',json_decode($result));
                }
            }
            return;

        } elseif ($key == 'use_repo') {

            require_once(getenv('OB_LIB_DIR').'repository.php');
            require_once(getenv('OB_LIB_DIR').'dataverse.php');

            if (!isset($_POST['params'])) {
                $_POST['params'] = array();
            }
            $params = json_decode($_POST['params'],true);

            # Default repo
            $D = (isset($params['REPO'])) ? $params['REPO'] : 0;
            
            $repo = new repository($D);
            $dv = new dataverse($D);

            $SERVER_URL = $repo->server_url;
            $API_TOKEN = $repo->api_token;
            $PROJECT_DATAVERSE = $dv->project_dataverse;

            if (!$SERVER_URL) {
                echo common_message('error',"No repository defined!");
                return;
            }

            if (!$API_TOKEN) {
                echo common_message('error',"API_TOKEN should be defined for the repository!");
                return;
            }

            // Here, only dataverse supported yet
            if (!$PROJECT_DATAVERSE) {
                echo common_message('error',"No project dataverse defined!");
                return;
            }

            $PARENT_DATAVERSE = isset($params['PARENT_DATAVERSE']) ? $params['PARENT_DATAVERSE'] : $PROJECT_DATAVERSE;

            # Parent dataverse cannot be out of project dataverse
            #
            if ($PARENT_DATAVERSE != $PROJECT_DATAVERSE) {
                $allowed = $dv->dataverse_search($SERVER_URL,$API_TOKEN,$PROJECT_DATAVERSE,$PARENT_DATAVERSE);
                if (!$allowed) {
                    echo common_message('error',"Dataverse `$PARENT_DATAVERSE` is outside of `$PROJECT_DATAVERSE` and therefore CANNOT BE USED as a PARENT.");
                    return;
                }
            }

            if ($value['scope']=='get_data') {

                $output_type = 'json';
                $header_opt = 0;

                $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");            
                
                # Default request is view dataverse
                if (!isset($params['type'])) $params['type'] = 'dataverse';

                if ($params['type'] == 'dataverse' or $params['type'] == 'dataverses') {
                    # View a Dataverse

                    if (isset($params['server_conf'])) {
                        $e = array();
                        if (defined("REPOSITORIES")) {
                            $e[] = count(constant("REPOSITORIES")) . " repositories defined.";
                        }
                        if (defined("PROJECT_REPO")) {
                            $e[] = constant("PROJECT_REPO");
                        }
                        echo common_message('OK',$e);
                        return;
                    }

                    $ID = isset($params['id']) ? $params['id'] : $PARENT_DATAVERSE;
                    if (isset($params['contents']) and $params['contents']==1)
                        $rest = "/api/dataverses/$ID/contents";
                    else
                        $rest = "/api/dataverses/$ID";
                }
                elseif ($params['type'] == 'datasets' or $params['type'] == 'dataset') {
                    $rest = '';
                    if (isset($params['files'])) {
                        # List Files in a Dataset
                        $version = isset($params['version']) ? $params['version'] : ':draft';
                        $rest = sprintf("/api/datasets/%d/versions/%s/files",$params['id'],$version);
                    
                    } else {
                        # Get JSON Representation of a Dataset
                        if (isset($params['persistentUrl'])) {
                            $pid = preg_replace('/^https:\/\/doi\.org\//','',$params['persistentUrl']);
                            $rest = "/api/datasets/:persistentId/?persistentId=doi:$pid";
                        }
                        elseif (isset($params['id'])) {
                            if (isset($params['version'])) {
                                # Get Version of a Dataset
                                $rest = sprintf("/api/datasets/%d/versions/%s",$params['id'],$params['version']);
                            } else {
                                $rest = sprintf("/api/datasets/%d",$params['id']);
                            }
                        }
                    }
                    if ($rest == ''){
                        echo common_message('error','Missing request param: files, id or persistentUrl needed');
                        return;
                    }
                }
                elseif ($params['type'] == 'datafile' or $params['type'] == 'datafiles')  {
                    # Getting a file
                    $rest = '';
                    $output_type = 'file';
                    $header_opt = 1;
                    $header = array("X-Dataverse-key:$API_TOKEN");            
                    if (isset($params['persistentUrl'])) {
                        $pid = preg_replace('/^https:\/\/doi\.org\//','',$params['persistentUrl']);
                        $rest = "/api/access/datafile/:persistentId/?persistentId=doi:$pid";
                    } 
                    elseif (isset($params['id'])) {
                        # Versions???
                        $rest = sprintf("/api/access/datafile/%d",$params['id']);
                    }
                    if ($rest == ''){
                        echo common_message('error','Missing request param: id or persistentUrl needed');
                        return;
                    }
                }
                else  {
                    echo common_message('error','Unknown request type: dataverses, datasets or datafile needed');
                    return;
                }
                
                $curl = curl_init();
                if ($curl) {
                    curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($curl, CURLOPT_HEADER, $header_opt);
                    $result = curl_exec($curl);

                    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                    $header = substr($result, 0, $header_size);
                    $body = substr($result, $header_size);
                    $header_items = explode("\n", $header);

                    curl_close($curl);

                    if ($output_type == 'json') {
                        if (is_json($result))
                            echo common_message('ok',$result);
                        else
                            echo common_message('error','Malformed data output');
                    }
                    else {
                        $file_name = null;
                        // find the filname in the headers.
                        if (!preg_match('/filename="(.*?)"/', $header, $matches)){
                            // If filename not found do something...
                            debug( "Unable to find filename. Please check the Response Headers or Header parsing!" );
                            echo common_message('error','Unable to find filename.');
                            exit();
                        } else {
                            // If filename was found assign the name to the variable above 
                            $file_name = $matches[1];
                        }
                        // Check header response, if HTTP response is not 200, then display the error.
                        if (!preg_match('/200/', $header_items[0])){
                            echo '<pre>'.print_r($header_items[0], true).'</pre>';
                            exit();
                        } else {
                            $content_type = '';
                            if (preg_match('/Content-Type: "(.*?); "/', $header, $matches)){
                                $content_type = $matches[1];
                            }
                            // Set the header for PHP to tell it, we would like to download a file
                            header('Content-Description: File Transfer');
                            if ($content_type!='')
                                header('Content-Type: '.$content_type);
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Disposition: attachment; filename='.$file_name);

                            // Echo out the file, which then should trigger the download
                            echo $body;
                            exit;

                        }
                    }
                } else {
                    echo common_message('error','curl error');
                }
            }
            elseif ($value['scope']=='put_data') {

                $header = array("X-Dataverse-key:$API_TOKEN");            
                if (!isset($params['type'])) $params['type'] = array('dataverse');

                if ($params['type'] == 'dataverse' or $params['type'] == 'dataverses') {
                    # Create DataSet with a PUT-POST request
                    # curl -H X-Dataverse-key:$API_TOKEN -X POST "$SERVER_URL/api/dataverses/$PARENT" --upload-file dataverse-abc1.json
                    # 
                    $meta_string = '{"name":"","alias":"","dataverseContacts":[{"contactEmail":""},{"contactEmail":""}],"affiliation":"","description":"","dataverseType":"LABORATORY"}';
                    $meta = json_decode($meta_string, true);
                    $tmp = tmpfile();
                    if (isset($params['metadata'])) {
                        $meta['name'] = $params['metadata']['Name'];
                        $meta['alias'] = $params['metadata']['Alias'];
                        $meta['dataverseContacts'][0]['contactEmail'] = $params['metadata']['ContactEmail0'];
                        if ($params['metadata']['ContactEmail1'] != '')
                            $meta['dataverseContacts'][1]['contactEmail'] = $params['metadata']['ContactEmail1'];
                        else 
                            unset($meta['dataverseContacts'][1]);
                        $meta['affiliation'] = $params['metadata']['Affiliation'];
                        $meta['description'] = $params['metadata']['Description'];
                        $meta['dataverseType'] = $params['metadata']['DataverseType'];

                        // write out meta content into tmp files
                        fwrite($tmp,json_encode($meta, JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
                        fseek($tmp,0);
                        // get tmp file path
                        $tmpfile = stream_get_meta_data($tmp)['uri'];
                    }
                    # Create dataverse in the root
                    $rest = "/api/dataverses/".$PARENT_DATAVERSE;

                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); // pseudo post
                        curl_setopt($curl, CURLOPT_PUT, true);
                        $fh_res = fopen($tmpfile, 'r');
                        curl_setopt($curl, CURLOPT_INFILE, $fh_res);
                        curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpfile));
                        $response = curl_exec($curl);
                        curl_close($curl);
                        fclose($fh_res);
                        if (is_json($response))
                            echo common_message('ok',$response);
                        else
                            echo common_message('error',$response);
                        //debug($response);
                    } else {
                        echo common_message('error','Curl error');
                    }

                    fclose($tmp); // this removes the tmp
                } elseif ($params['type'] == 'datasets' or $params['type'] == 'dataset') {

                    # Create DataSet with a PUT-POST request

                    $parent_dataverse = (isset($params['dataverse']) and $params['dataverse']!='') ? $params['dataverse'] : $PARENT_DATAVERSE; 
                    $f = array();
                    if (isset($params['metadata'])) {
                        $t = $params['metadata'];
                        $f['ds-dataset'] = $t['Title'];
                        $f['ds-author'] = $t['AuthorName'];
                        $f['ds-authorAffiliation'] = $t['AuthorAffiliation'];
                        $f['ds-contactName'] = $t['ContactName'];
                        $f['ds-email'] = $t['ContactEmail'];
                        $f['ds-description'] = $t['Description'];
                        $f['ds-subject'] = $t['Subject'];
                        
                        $response = $dv->new_dataset($f,$parent_dataverse,$SERVER_URL,$API_TOKEN);
                        if (is_json($response))
                            echo common_message('ok',$response);
                        else
                            echo common_message('error','Malformed data output');

                    } else {
                    
                        echo common_message('error','Metadata for creating a dataset is mandatory!');
                    }

                } else if ($params['type'] == 'datafile' or $params['type'] == 'datafiles') {

                    # Add file to DataSet
                    if (isset($_FILES) and count($_FILES)) {
                        $tmp_file = $_FILES['data_files']['tmp_name'];
                        $filename = basename($_FILES['data_files']['name']);
                        $file = file_get_contents($tmp_file);
                    
                        # Alternative way to create a Darwin Core metadata and DWCA archive zip file
                        # BUT here we have no dwc compatible metadata ....
                        #require((getenv('OB_LIB_DIR').'darwincore.php');
                        #$dwc = new darwinCore;
                        #$dwc->create_simple("$protocol://".URL."/LQ/{$f['LQ']}/metadata/");
                        #$file = $dwc->create_archive("$protocol://".URL."/LQ/{$f['LQ']}/data/");
                        #$file_name $dwc->archive_name;

                    }
                    $rest = '';
                    if (isset($params['persistentUrl'])) {
                        $dataset_id = $params['persistentUrl'];
                    } 
                    elseif (isset($params['id'])) {
                        $dataset_id = $params['id'];
                    }

                    if (isset($params['replace_file'])) {

                        $response = $dv->file_replace($file, $filename, $dataset_id, "obm-file-from-r-package", $SERVER_URL, $API_TOKEN);
                        echo $response;
                    
                    } else {
                    
                        $response = $dv->file_add($file, $filename, $dataset_id, "obm-file-from-r-package", $SERVER_URL, $API_TOKEN);
                        echo $response;
                    
                    }
                }
            } // END put_data
            elseif ($value['scope']=='delete_data') {
                
                $header = array("X-Dataverse-key:$API_TOKEN");

                if ($params['type'] == 'dataverse' or $params['type'] == 'dataverses') {
                    # curl -H X-Dataverse-key:$API_TOKEN -X DELETE $SERVER_URL/api/dataverses/$ID
                    if (isset($params['id'])) {
                        $rest = sprintf("/api/dataverses/%s", $params['id']);
                        $dataverse = $params['id'];
                    } else {
                        echo common_message('error','Dataverse id/alias is missing');
                        return;
                    }

                    # Check wether dataverse within project_dataverse
                    # curl -H "X-Dataverse-key: $API_TOKEN" $SERVER_URL/api/dataverses/obm/contents | jq '.'

                    $allowed = 0;
                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL."/api/dataverses/$PARENT_DATAVERSE/contents");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        $result = curl_exec($curl);
                        curl_close($curl);

                        $j = json_decode($result,true);
                        $jd = $j['data'];
                        for($i = 0; $i < count($jd); $i++) {
                            if ($jd[$i]['type'] == 'dataverse') {
                                if ($jd[$i]['title'] == $dataverse or $jd[$i]['id'] == $dataverse) {
                                    $allowed = 1;
                                }
                            }
                        }
                    }
                    if (!$allowed) {
                        echo common_message('error',"Dataverse `$dataverse` is not in $PARENT_DATAVERSE and therefore CANNOT BE deleted.");
                        return;
                    }

                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                        $response = curl_exec($curl);
                        curl_close($curl);
                        if (is_json($response))
                            echo common_message('ok',$response);
                        else
                            echo common_message('error','Malformed data output');
                    } else {
                        echo common_message('error','Curl error');
                    }

                } elseif ($params['type'] == 'datasets' or $params['type'] == 'dataset') {
                    # Delete Unpublished Dataset
                    # curl -H "X-Dataverse-key: $API_TOKEN" -X DELETE $SERVER_URL/api/datasets/$ID

                    if (isset($params['id'])) {
                        if (!is_numeric($params['id'])) {
                            echo common_message('error','The id should be the numeric id of the Dataset.');
                            return;
                        }
                        $rest = sprintf("/api/datasets/%s", $params['id']);
                        $dataset = $params['id'];
                    } else {
                        echo common_message('error','Dataset id is missing');
                        return;
                    }

                    $allowed = 0;
                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL."/api/dataverses/$PARENT_DATAVERSE/contents");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        $result = curl_exec($curl);
                        curl_close($curl);

                        $j = json_decode($result,true);
                        $jd = $j['data'];
                        for($i = 0; $i < count($jd); $i++) {
                            if ($jd[$i]['type'] == 'dataset') {
                                if ($jd[$i]['identifier'] == $dataset or $jd[$i]['id'] == $dataset or $jd[$i]['persistentUrl'] == $dataset) {
                                    $allowed = 1;
                                }
                            }
                        }
                    }
                    if (!$allowed) {
                        echo common_message('error',"Dataset `$dataset` is not in $PARENT_DATAVERSE and therefore CANNOT BE deleted.");
                        return;
                    }


                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                        $response = curl_exec($curl);
                        curl_close($curl);
                        if (is_json($response))
                            echo common_message('ok',$response);
                        else
                            echo common_message('error','Malformed data output');
                    } else {
                        echo common_message('error','Curl error');
                    }

                } else if ($params['type'] == 'datafile' or $params['type'] == 'datafiles') {
                    # Using sword api
                    # curl -u $API_TOKEN: -X DELETE $SERVER_URL/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/103
                    if (isset($params['id'])) {
                        $rest = sprintf("/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/%s", $params['id']);
                    } else {
                        echo common_message('error','File id is missing');
                        return;
                    }
 
                    $allowed = 0;
                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL."/api/dataverses/$PARENT_DATAVERSE/contents");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                        $result = curl_exec($curl);
                        curl_close($curl);

                        $j = json_decode($result,true);
                        $jd = $j['data'];
                        $ids = array();
                        for ($i = 0; $i < count($jd); $i++) {
                            if ($jd[$i]['type'] == 'dataset') {
                                $ids[] = $jd[$i]['id'];
                            }
                        }

                        $curl = curl_init();
                        if ($curl) {
                            #curl -H "X-Dataverse-key: $API_TOKEN" $SERVER_URL/api/datasets/89 | jq '.data.latestVersion.files' 
                            foreach ($ids as $dataset) {
                                curl_setopt($curl, CURLOPT_URL, $SERVER_URL."/api/datasets/$dataset");
                                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                                $result = curl_exec($curl);
                                curl_close($curl);
                                $j = json_decode($result,true);
                                $jf = $j['data']['latestVersion']['files'];
                                $ids = array();
                                for ($i = 0; $i < count($jf); $i++) {
                                    if ($jf[$i]['dataFile']['id'] == $params['id']) {
                                        $allowed = 1;
                                    }
                                }
                            }
                        }
                    }
                    if (!$allowed) {
                        echo common_message('error',"The file {$params['id']} is outside of $PARENT_DATAVERSE therefore NOT ALLOWED to delete.");
                        return;
                    }

                    #$response = $dv->file_drop($params['id'], $params['description'], $SERVER_URL, $API_TOKEN);
                    
                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                        curl_setopt($curl, CURLOPT_USERPWD, "$API_TOKEN:");
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                        $response = curl_exec($curl);
                        curl_close($curl);
                        if ($response == '')
                            echo common_message('ok',$response);
                        else
                            echo common_message('error',$response);
                    } else {
                        echo common_message('error','Curl error');
                    }

                } else {
                        echo common_message('error','Unknown type.');

                }
            } elseif ($value['scope']=='set_data') {
                # Publish, restrict, ...
                # Create a Private URL for a Dataset
                # Publish a Dataset
                # curl -H "X-Dataverse-key: $API_TOKEN" -X POST "$SERVER_URL/api/datasets/:persistentId/actions/:publish?persistentId=$PERSISTENT_ID&type=$MAJOR_OR_MINOR"
                
                #if (!isset($params[''])) 
                
                $header = array("X-Dataverse-key:$API_TOKEN");            
                $postFields = array();

                if (isset($params['publish'])) {
                    if (isset($params['persistentId'])) {
                        $majorminor = (isset($params['majorminor'])) ? $params['majorminor'] : 'major';
                        $pid = preg_replace('/^https:\/\/doi\.org\//','',$params['publish']);
                        $rest = sprintf("/api/datasets/:persistentId/actions/:publish?persistentId=%s&type=%s", $pid, $majorminor);
                    }
                }

                $curl = curl_init();
                if ($curl) {
                    curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
                    $response = curl_exec($curl);
                    curl_close($curl);
                    if (is_json($response))
                        echo common_message('ok',$response);
                    else
                        echo common_message('error','Malformed data output');
                } else {
                    echo common_message('error','Curl error');
                }
            }
            // use_repo
            //
        } elseif ($key == 'computation') {
            // POST Computation process :::::>>>>>>>>
            if ($value['scope'] == 'get-results') {
                
                echo common_message('ok',"{$value['scope']} processed");
                return;
            }
            elseif ($value['scope'] == 'get-status') {

                echo common_message('ok',"{$value['scope']} processed");
                return;
            }
            elseif ($value['scope'] == 'post') {
                
                $params = json_decode($_POST['params'],true);
                
                if (isset($_FILES) and count($_FILES)) {

                    if ($xmodules->is_enabled('computation') and $this->API_VERSION > 2.1) {

                        # Getting uploaded files via API POST
                        $tmp_file = $_FILES['data_files']['tmp_name'];
                        $filename = basename($_FILES['data_files']['name']);
                        $file = file_get_contents($tmp_file);

                        $package = $params['package'];
                        $path = getenv('PROJECT_DIR')."computational_packages/".$package;
                        $y = FALSE;
                        $content = "";

                        # Creating package
                        if (!preg_match('/^[a-z0-9_-]+$/',$package)) {
                            echo common_message("error","Only letters and _ - characters allowed in package name");
                            return;
                        }
                        if (!file_exists(getenv('PROJECT_DIR')."computational_packages")) {
                            mkdir(getenv('PROJECT_DIR')."/computational_packages");
                        }
                        if (!file_exists($path)) {
                            mkdir($path,0755);
                            mkdir($path.'/data',0755);
                            mkdir($path.'/output',0755);
                            mkdir($path.'/scripts',0755);
                        }
                        if (!move_upl_files($_FILES['data_files'],$path."/scripts")) {
                            echo common_message("error","Failed to put files into the destionation directory.");
                            return; 
                        }

                        if (move_upl_files($_FILES['config_file'],$path)) {
                            $y = yaml_parse_file("$path/computation_conf.yml");
                        }

                        # Package posting to the remote server
                        if (!$y) {
                            #$content .= "Syntax error in config file";
                            echo common_message("error","Syntax error int the config file");
                            return;
                        } else  {
                            $project = $y['project'];


                            $res = $xmodules->_include('computation','sendPackage',$package);



                            echo common_message('ok',"$res");
                            return;
                        } 
                    } else {
                        echo common_message('error',"Computational module in not enabled");
                        return;
                    }
                }
                echo common_message('error',"No files attached to process");
                return;
            }
            echo common_message('error',"Unknown request");
            return;
        }
        
        return; // return from getFunctions

    }

    // SERVICE: PFS
    // project feature service
    // It is a function
    // $this->request['table'] should be defined and valid
    //
    public function putFunction($API_POST) {
        global $ID,$BID;

        /* New API upload data method
         *
         * */
        #require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        if (isset($this->request['ic_post_data'])) {
            if ($_FILES['interconnect_post_file']['error'] == UPLOAD_ERR_OK               //checks for errors
                && is_uploaded_file($_FILES['interconnect_post_file']['tmp_name'])) {     //checks that file is uploaded
                //$file = file_get_contents($_FILES['interconnect_post_file']['tmp_name']);
                $file = readCSV($_FILES['interconnect_post_file']['tmp_name']);
                $data_count = count($file);

                // unique upload hash with 'nk' (no-key) flag
                if ($this->request['ic_accept_key']=='' and $this->request['ic_request_key']!='')
                    $hash = "nk_".$this->request['ic_request_key'];
                else if ($this->request['ic_accept_key']!='')
                    $hash = "wk_".$this->request['ic_accept_key'];
                else return;

                // append : false
                // interconnect : true
                if (create_upload_temp(readCSV($_FILES['interconnect_post_file']['tmp_name']),false,true)) {

                    $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_%3$s\' WHERE interconnect=\'t\' AND table_name=\'upload_%1$s_%2$s\'',PROJECTTABLE,session_id(),$hash);

                    if (pg_query($ID,$cmd)) {

                        $cmd = sprintf('ALTER TABLE temporary_tables.upload_%1$s_%2$s RENAME TO "upload_%1$s_%2$s_%3$s"',PROJECTTABLE,session_id(),$hash);
                        $res = pg_query($ID,$cmd);

                        #$cmd = sprintf('INSERT INTO interconnects_register ...');
                        #$res = pg_query($BID,$cmd);
                        // send email alert about new files arrived
                        if (defined("OB_PROJECT_DOMAIN")) {
                            $domain = constant("OB_PROJECT_DOMAIN");
                            $server_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                            $reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
                        } else {
                            $domain = $_SERVER["SERVER_NAME"];
                            $server_email = PROJECTTABLE."@".$domain;
                            $reply_email = "noreply@".$domain;
                        }

                        $cmd = sprintf("SELECT u.email AS email,p.email AS aemail, u.username AS name FROM projects p
                            LEFT JOIN project_users pu ON (pu.project_table=p.project_table)
                            LEFT JOIN users u ON (user_id=u.id)
                            WHERE p.project_table='%s' AND user_status::varchar IN ('2','master') AND receive_mails!=0",PROJECTTABLE);
                        $res = pg_query($BID,$cmd);
                        $emails = array();
                        $names = array();
                        while ( $row = pg_fetch_assoc($res)) {
                            $names[] = $row['name'];
                            $emails[] = $row['email'];
                        }

                        $emails = array_unique($emails);

                        $Subject = sprintf("Shared data from %s / %s database to %s",$this->request['ic_slave_server'],$this->request['ic_slave_project'],PROJECTTABLE);
                        $Title = 'Hi '.implode(', ',$names).'!';

                        $Message = sprintf("There are %d record from %s database to import<br>",$data_count,$this->request['ic_slave_project']);
                        $Message .= sprintf("Go to <a href='https://%s/upload/'>upload page</a> and choose a file upload form to add these data to your database.",URL);

                        $m = mail_to($emails,$Subject,"$server_email","$reply_email",$Title,$Message,"multipart");

                    }

                    //debug('interconnect link: '.PROJECTTABLE.'_'.session_id(),__FILE__,__LINE__);
                    echo common_message('ok','PROCESSED');
                } else {
                    http_response_code(202);
                    echo common_message('error','UPLOAD_PROCESSING_FAILED');
                }
            }

            http_response_code(400);
            echo common_message('error','NOT_PROCESSED');
            return;
        }

        // POST Tracklog processing :::::>>>>>>>>


        if (isset($this->request['tracklog'])) {
            
            require_once(getenv('OB_LIB_DIR').'modules_class.php');
            require_once(getenv('OB_LIB_DIR').'upload_funcs.php');
            
            $API_VERSION = $this->API_VERSION;

            $up = new upload_process();

            $response = $up->new_tracklog($this->project,$this->request['tracklog']);      // true | false
            $j = json_decode($up->messages[0]);                 // processing first message

            if ($j->{'status'}=='fail') {
                http_response_code(400);
                echo $up->messages[0];
                return;
            }
            elseif ($j->{'status'}=='error') {
                http_response_code(422);
                echo $up->messages[0];
                return;
            } 
            elseif ($j->{'status'}=='success') {
                // successful upload
                echo $up->messages[0];
                //http_response_code(200);
                return;
            } 
            else {
                // no status message - big problem
                http_response_code(400);
                echo common_message('error','SERVER_ERROR');
                return;
            }
        }

        // POST DATA processing :::::>>>>>>>>

        if (!isset($this->request['form_id']) or $this->request['form_id']=='') {
            http_response_code(400);
            echo common_message('error','FORMID_OR_TRACKLOG_MUST_BE_SPECIFIED');
            return;
        }

        if (isset($metadata) and isset($metadata['form_version']) ) {
            $last_modification_of_form = 0;
            $cmd = sprintf("SELECT round(extract(epoch from last_mod)) as last_mod FROM project_forms WHERE form_id=%s AND project_table=%s",quote($this->request['form_id']),quote($this->project));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $last_modification_of_form = $row['last_mod'];
            }
            if ($metadata['form_version'] < $last_modification_of_form) {
                echo common_message('error','This form is out of date, if you get an error, the upload may be completed on the server.');
            }
        }

        require_once(getenv('OB_LIB_DIR').'modules_class.php');
        require_once(getenv('OB_LIB_DIR').'upload_funcs.php');

        $_SESSION['api_warnings'] = array(); // this is used by upload_funcs.php
        $up = new upload_process();
        // Ez nem egy hozzáférés ellenőrzés, hanem egy komplex funkció; A pds_form_access_check ezt már szebben oldja meg...
        list($destination_schema,$destination_table) = $up->access_check($this->request['form_id'],'api');

        if (defined('DEBUG_UPLOAD') and constant('DEBUG_UPLOAD') === true) {
            $up->debug_upload = true;
        }

        if ( !$destination_table ) {

            http_response_code(401);
            echo common_message('error','Form access denied or form does not exists.');
            return;

        }

        $_SESSION['current_query_schema'] = $destination_schema;
        $_SESSION['current_query_table'] = $destination_table;

        # bulk upload: harvesting submitted data
        if (isset($_POST['batch'])) {
            $batch = json_decode($_POST['batch'],TRUE);
            $bacth_files = array();
            $sheetData = array();
            foreach ($batch[0]['data'] as $line) {
                $sheetData[] = $line;
                //$spf = preg_split('/,/',$line['attached_files']);
            }
            if (isset($batch[0]['attached_files'])) {
                if (is_array($batch[0]['attached_files']))
                    foreach ($batch[0]['attached_files'] as $file) {
                        $batch_files[] = $file;
                    }
                else
                    $batch_files[] = $batch[0]['attached_files'];
            }
            if (!create_upload_temp($sheetData)) {
                http_response_code(202);
                echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                return;
            }
        }

        // itt csak az attached fileok feldolgozása kell!!!
        // attached_files
        if (isset($_FILES) and count($_FILES)) {

            # only attachment uploading
            # returning saved files names
            if (isset($_FILES['attached_files'])) {
                // R client compatibility
                $r = move_upl_files($_FILES['attached_files'],getenv('PROJECT_DIR').'local/attached_files');
                $rr = json_decode($r);
                if (count($rr->{'err'})) {
                    http_response_code(422);
                    echo common_message('error',json_encode($rr->{'err'}));
                    return;
                }
                $rr->{'file_names'} = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
                if (!$rr->{'file_names'}) {
                    http_response_code(422);
                    echo common_message('error','FILE_PROCESSING_ERROR');
                    return;
                }

                //echo json_encode($rr);
                //returning with uploaded file reference name
                echo common_message('ok',$rr->{'file_names'});
                return;

            # data file uploading
            # create sheetData from file content
            } elseif (isset($_FILES['file'])) {

                $fp = new file_process();

                $log = $fp->fprocess('','');
                if (is_array($log) and isset($log['comment'])) {
                    //$upload_comment = $log['comment'];
                    if (create_upload_temp($fp->sheetData))
                        $u_theader = $fp->theader;
                    else {
                        log_action('Read file error, couldn\'t create temporary table for upload form.',__FILE__,__LINE__);
                        http_response_code(202);
                        echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                        return;
                    }
                } else {
                    http_response_code(422);
                    echo common_message('error',"File upload failed: $log");
                    return;
                }
            # packed file: data + attachments (multilines)
            # superpack
            } elseif (isset($_FILES['multipacked_lines'])) {
                # unpack super pack
                #
                $zipdir = sprintf('%s/%s/zipdir/',OB_TMP,session_id());
                if (!unpack_zip($_FILES['multipacked_lines']['tmp_name'],$zipdir)) {
                    http_response_code(422);
                    echo common_message('error','FILE_PROCESSING_ERROR');
                    return;
                }

                $list = glob("$zipdir/*.zip");

                if (isset($_POST['header'])) $u_theader = json_decode($_POST['header'],true);
                $append = false;
                foreach ($list as $zip) {
                    $F = array('tmp_name'=>$zip,'name'=>basename($zip),'type'=>'','error'=>UPLOAD_ERR_OK);
                    if (packed_data_line_process($F,$append,$u_theader) != 'done') {
                        http_response_code(422);
                        echo common_message('error','FILE_PROCESSING_ERROR');
                        return;
                    }
                    $append = true;
                }
            # packed file: data + attachments (only one data line)
            # single pack
            } elseif (isset($_FILES['packed_line'])) {
                // processing one line zipped data+attachments

                //header should be ordered as the file content processed if no other information in the file:
                //obm_gemetry,obm_files_id,species,datum,...?
                if (isset($_POST['header'])) $u_theader = json_decode($_POST['header'],true);
                if (packed_data_line_process($_FILES['packed_line'],false,$u_theader) != 'done') {
                    http_response_code(422);
                    echo common_message('error','FILE_PROCESSING_ERROR');
                    return;
                }

            } elseif (isset($batch_files) and count($batch_files)) {
                $batch_line = 1;
                foreach ($batch_files as $fu) {
                    $fuua = preg_split ('/,/',$fu);
                    $uploaded_files = array();
                    foreach($fuua as $fuuae) {
                        if (isset($_FILES[$fuuae])) {
                            $r = move_upl_files($_FILES[$fuuae],getenv('PROJECT_DIR').'local/attached_files');
                            $rr = json_decode($r);
                            if (count($rr->{'err'})) {
                                 http_response_code(422);
                                 echo common_message('error','FILE_UPLOAD_ERROR');
                                 return;
                            }
                            $e = file_upload_process($rr->{'file_names'},$rr->{'sum'},$rr->{'type'});
                            if (!$e) {
                                http_response_code(422);
                                echo common_message('error','FILE_PROCESSING_ERROR');
                                return;
                            }
                            // egyszerre it csak 1 fájl jön
                            $uploaded_files[] = $e[0];
                        }
                    }
                    $row = get_sheet_row($batch_line);
                    $jdata = json_decode($row['data'],true);
                    $jdata['obm_files_id'] = implode(',',$uploaded_files);
                    $res = update_sheet_row($batch_line,$jdata);

                    $batch_line++;
                }
                if ($API_POST['put_header']!='') {
                    $header_names = json_decode($API_POST['put_header'],TRUE);
                    if (!in_array('obm_files_id',$header_names)) {
                        $header_names[] = 'obm_files_id';
                        $API_POST['put_header'] = json_encode($header_names);
                    }
                }

            # unexpected files
            } else {
                http_response_code(422);
                echo common_message('error','UNKNOWN_FILE_TYPE');
                return;
            }

        }

        // no files, only data
        if (isset($API_POST['put_data']) and $API_POST['put_data']!="" and (!isset($_FILES) or count($_FILES)==0)) {
            if (!create_upload_temp(json_decode($API_POST['put_data'],TRUE))) {
                #if ($up->debug_upload) {
                #    debug(session_id(),__FILE__,__LINE__);
                #    debug($_POST,__FILE__,__LINE__);
                #}
                log_action('Read file error, couldn\'t create temporary table for upload form.',__FILE__,__LINE__);
                http_response_code(202);
                echo common_message('error','Read file error, couldn\'t create temporary table for upload form.');
                return;
            }

            # read multiple lines:
            # [{
            #	"species": "Tringa totanus",
            #	"brood_id": "512",
            #	"ring_code": "AWBO"
            # }, {
            #	"species": "Tringa totanus",
            #	"brood_id": "513",
            #	"ring_code": "BYWY"
            # }]
            #
            #
            #Some check of data structure??
        }

        #
        # description
        # Adat felvitel fájlból: 'teszt2.csv'

        # form_page_id
        # 58

        # form_page_type
        # file

        # srid
        # 4326

        # upload_table_post
        # {"id":["faj","the_geom","szamossag","hely","egyedszam","gyujto","adatkozlo"],
        #  "header":["faj","the_geom","szamossag",null,null,null,null,null,null,null,"hely","egyedszam","gyujto","adatkozlo"],
        #  "default_data":["úttest","","pompom","miki"],
        #  "rows":[[1,2]]}
        #
        #  Az "id" az adatbázis oszlopok amikbe beszúrunk adatokat,
        #  A "header" a feltöltött adattábla oszlopai sorban a kiválasztott/hozzárándelt oszlop nevekkel

        # Honnan jöjjön a header?
        # A "value" a 'condition' értéke vagy külön küldhető legyen, hogy ne kelljen mindig elküldeni pl?
        # OBM_get('api_form_headers')
        # OBM_set('api_form_header','koszti')
        # OBM_put('put_data','header',form-id,file)
        #
        #$j = $value = array('header'=>array('faj','hely','egyedszam'),'default_data'=>array(),'rows'=>array());


        // session theader is the column names of uploaded data
        // the database column association can be given from the put_header array
        $u_theader = get_sheet_header();

        if ($API_POST['put_header']!='') {
            $header = json_decode($API_POST['put_header'],TRUE);

            if (isset($_POST['metadata']))
                $metadata = json_decode($_POST['metadata'],TRUE);

            //empty header names given
            if (!count($header)) {
                if ( isset($metadata) and isset($metadata['observation_list_start']) and isset($metadata['observation_list_end']) and
                        $metadata['observation_list_start']!='' and $metadata['observation_list_end']!='') {
                    $header = array();
                } else {
                    log_action('Header is empty or not well formed',__FILE__,__LINE__);
                    http_response_code(400);
                    echo common_message('error','Header is empty or not well formed');
                    return;
                }
            }
            // Use or not - temporary_tables.xxx_obm_obsl table?
            // default is use but define +USE_TEMPTABLES_FOR_OBSLISTS":"false" to disabe it.
            $temptables = true;
            if (defined('USE_TEMPTABLES_FOR_OBSLISTS') && (constant('USE_TEMPTABLES_FOR_OBSLISTS')=='false' or constant('USE_TEMPTABLES_FOR_OBSLISTS')===false)) $temptables = false;

            if ($temptables and isset($metadata) and isset($metadata['observation_list_id'])) {
                $temp_table = CreateTmpTable_likeTable($destination_table,'_obm_obsl');
                $up->temp_destination = 'temporary_tables.'.$temp_table;
                if (!$temp_table) {
                    log_action('Failed to create .._obm_obsl table',__FILE__,__LINE__);
                    http_response_code(204);
                    echo common_message('error','Failed to create .._obm_obsl table');
                    return;        
                }
            }
        } else {
            // database column names equal to the sent columns' names and therefore additional header names not given
            $header = $u_theader;
        }

        //get default data JSON
        if (isset($_POST['default_values'])) {
            $default_data = array_values(json_decode($_POST['default_values'],TRUE));
            $header = array_merge($header,array_keys(json_decode($_POST['default_values'],TRUE)));
        } else
            $default_data = array();

        // ez a tömb a webes felülettel való kompatibilitás miatt kell
        $j = array('header'=>$header,'default_data'=>$default_data,'rows'=>array());
        $j = json_encode($j);

        $_POST['upload_table_post'] = $j;
        $_POST['srid'] = 4326;                  // force WGS84 in API uploads!!!!
        $_POST['description'] = 'API upload';

        $response = $up->new_upload($destination_schema,$destination_table);        // true | false
        $j = json_decode($up->messages[0]);                     // processing first message

        if ($j->{'status'}=='fail') {
            http_response_code(400);
            echo $up->messages[0];
            return;
        }

        // 
        // If upload failed, check the form version and if the form changed create a temp upload:
        // temporary_tables.PROJECTTABLE_obm_upl
        //
        // API_VERSION < 2.5 > 1.3
        if ($response===false) {
            // In this case
            // $j->{'status'} == error

            $last_modification_of_form = 0;
            $cmd = sprintf("SELECT round(extract(epoch from last_mod)) as last_mod FROM project_forms WHERE form_id=%s AND project_table=%s",
                quote($this->request['form_id']),
                quote($this->project));
            $res = pg_query($BID,$cmd);
            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                $last_modification_of_form = $row['last_mod'];
            }

            $metadata = json_decode($_POST['metadata'],TRUE);

            $hash = crc32(session_id().$this->request['form_id'].'api'.microtime());

            // 
            // Upload might be failed due to a form update,
            // lets create a null-upload - 
            //
            $form_version_mismatch = 0;
            if (isset($metadata) and isset($metadata['form_version']) and $metadata['form_version'] < $last_modification_of_form) {

                $temp_table = CreateTmpTable_likeTable($destination_table,'_failed_uploads');
                if (!$temp_table) {
                    log_action('Failed to create '.$this->project.'_failed_uploads table',__FILE__,__LINE__);
                    http_response_code(204);
                    echo common_message('error','Failed to create '.$this->project.'_failed_uploads table');
                    return;        
                }

                $up->temp_destination = 'temporary_tables.'.$temp_table;
                $up->skip_checks = true;
                $up->errors = array();
                $up->soft_errors = array();
                $up->messages = array();
                $response = $up->new_upload($destination_schema,$destination_table);        // true | false
                $j = json_decode($up->messages[0]);                     // processing first message

                if ($j->{'status'}=='fail') {
                    http_response_code(400);
                    echo $up->messages[0];
                    return;
                }
                elseif ($j->{'status'}=='error') {
                    //debug($up->messages,__FILE__,__LINE__);   // Input value is differ from the expected...
                    http_response_code(422);
                    echo $up->messages[0];
                    return;
                } 
                elseif ($j->{'status'}=='success') {
                    // successful upload
                    echo $up->messages[0];
                    //http_response_code(200);
                    return;
                } 
                else {
                    // no status message - big problem
                    http_response_code(400);
                    echo common_message('error','SERVER_ERROR');
                    return;
                }
            } else {
                // no metadata or no form-version or form_version is up-to-date: sending error message
                // Data already uploded
                // egyéb ..?
                http_response_code(422);
                echo $up->messages[0];
                return;
            }

                /* NULLFORM SOLUTION
                 * $header = '';
                $data = '';
                # nullform (id:???) should be created to use this imports
                # nullform should contains all columns!
                # - accessible only for admins
                #

                $nullform_id = getNullFormId($destination_table,false);
                if ($nullform_id) {
                    $cmd = sprintf("INSERT INTO system.imports (project_table,user_id,ref,datum,header,data,form_type,form_id,file)
                        VALUES ('%s','%d','%s',NOW(),'%s','%s','api',%d,'%s')",
                        PROJECTTABLE,$_SESSION['Tid'],$hash,$header,$data,$nullform_id,"upload_".PROJECTTABLE."_".session_id());
                    $res = pg_query($ID,$cmd);
                    if (pg_affected_rows($res)) {

                        $cmd = sprintf('UPDATE system.temp_index SET table_name=\'upload_%1$s_%2$s_%3$s\' WHERE table_name=\'upload_%1$s_%2$s\'',PROJECTTABLE,session_id(),$hash);
                        if (pg_query($ID,$cmd)) {
                            $cmd = sprintf('ALTER TABLE temporary_tables.upload_%1$s_%2$s RENAME TO "upload_%1$s_%2$s_%3$s"',PROJECTTABLE,session_id(),$hash);
                            $res = pg_query($ID,$cmd);
                            if (!pg_last_error($ID)) {
                                // Az adatok feltöltődtek, de ```talán``` az űrlap verziójának eltérése miatt nem fejezhető be. 
                                // Próbálja meg befejezni a szerveren a „megszakított importálás” funkció használatával a nullform űrlappal.
                                http_response_code(200);
                                echo common_message('ok','Data uploaded, but cannot be finished due to form version mismatch. Try to finish it on the server using "interrupted imports" function with the nullform.');
                            } else {
                                // nem sikerült átnevezni a temp táblát emiatt n
                                http_response_code(422);
                                log_action(sprintf('Failed to rename temporary_tables.upload_%1$s_%2$s',PROJECTTABLE,session_id()),__FILE__,__LINE__);
                                echo $up->messages[0];
                            }
                        } else {
                            // faild to create import: sending error message
                            http_response_code(422);
                            echo $up->messages[0];
                        }
                    } else {

                        http_response_code(422);
                        echo $up->messages[0];
                    }
                } else {

                    http_response_code(422);
                    log_action('Could not create nullform.',__FILE__,__LINE__);
                    echo $up->messages[0];
                } */

        } 
        elseif ($j->{'status'}=='success') {
            // successful upload
            //http_response_code(200);
            echo $up->messages[0];
            return;
        }
        elseif ($j->{'status'}=='error') {
            // elvileg nincs ilyen eset, mert akkor mindig false a response..
            http_response_code(422);
            echo $up->messages[0];
            return;

        } else {
            // no status message - big problem
            http_response_code(400);
            echo common_message('error','SERVER_ERROR');
            return;

        }
        return;
    }
}

?>
