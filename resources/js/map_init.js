/* Map related variables and functions
 *
 * */
var map, center, zoom, mouseWheelInt, ExampleDraw, ExampleModify, vectorLayer, markerLayer, dragBox, modify_interaction, select_interaction, ih;
var singleClickListener = 'off';
var dataLayers = new Array();
var tileLayers = new Array();
var dataLayers_ns = new Array();
var zoomTo = null; //zoomTo features.geometry

/* OpenLayers styles */
const highlight_style = { 
    pointRadius:15,
    strokeColor:'#FFAC00',//orange
    strokeWidth: 2, 
    fillColor:'#529ccd', 
    fillOpacity:0.2 
};

/* OpenLayers initialization */
function init() {

    eval(obj.loadq);

    ih=$('body').innerHeight();
    let mch=$('#mapcontrols').height();
    let bhh=$('#bheader').height();
    if (obj.fixheader=='false') {
        $('.maparea').height(ih-mch);     // this number should come from map style
    } else {
        $(".maparea").height(ih-mch-bhh);    // this number should come from map style
    }

    /* Load previously save queries: Share=...
     * Load XML and call WFSGet */
    let dlayers = new Array();
    let tlayers = new Array();
    let blayers = new Array();
    let dataLayers_titles = [];
    let tileLayers_titles = [];
    let i = 0;
    let wd = 0;
    let wt = 0;

    Object.keys(obj).forEach(function(key) {
        if (key.match(/^layer_[a-zA-Z0-9-_]+/)) {
            let title;
            if (title=key.match(/^layer_data_([a-zA-Z0-9-_]*)/)) {
                // Image layers
                    //Grid check
                    //$.post('ajax',{'check_layer':key},function(data){
                    //    var retval = jsendp(data);
                    //    if (retval['status']=='success' && retval['data']=='ok') {
                dataLayers[wd] = new Array();
                eval("dataLayers["+wd+"] = " + obj[key] + ";");
                eval("dataLayers_ns["+wd+"] = '" + key + "';");
                eval("dataLayers_titles["+wd+"] = '" + title[1] + "';");
                dlayers.push("dataLayers["+wd+"]");
                wd++;
            } else if (title=key.match(/^layer_tile_([a-zA-Z0-9-_]*)/)) {
                // Tile layers
                tileLayers[wt] = new Array();
                eval("tileLayers["+wt+"] = " + obj[key] + ";");
                eval("tileLayers_titles["+wt+"] = '" + title[1].toUpperCase() + "';");
                tlayers.push("tileLayers["+wt+"]");
                wt++;
            } else {
                //background layers
                blayers.push(key);
                eval("var " + key + "= " + obj[key] + ";");
            }
            i++;
        }
    })

    const bbox = [-180.0000, -85.0000, 180.0000, 85.0000];
    //;
    //-20026376.39 -20048966.10 20026376.39 20048966.10
    //const extent = [-20026376.39,-20048966.10,20026376.39,20048966.10];
    let extent = ol.proj.transformExtent(bbox, 'EPSG:4326', 'EPSG:3857');
    const projection = new ol.proj.Projection({
        code: 'EPSG:3857',
        extent: extent,
    });
    vectorLayer = new ol.layer.Vector({
        name: 'drawLayer',
        source: new ol.source.Vector(),
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)',
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 5,
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33',
                }),
            }),
        }),
    });
    markerLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            //features: [iconFeature]
        }),
        style: new ol.style.Style({
            /*image: new ol.style.Icon({
                anchor: [0.5, 46],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                src: 'https://openlayers.org/en/latest/examples/data/icon.png'
            })*/
        })
    });

    let data_layers_group = [];
    let base_layers_group = (tlayers.length) ? [] : [
        new ol.layer.Tile({
            title: 'OSM',
            visible: true,
            type: 'base',
            source: new ol.source.OSM(), // By default we provide OSM Map layer
        }),
        /*new ol.layer.Tile({
            title: 'Google',
            type: 'base',
            source: new ol.source.TileImage({ // By default we provide Google Map
                url: 'http://maps.google.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i375060738!3m9!2spl!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0'
            })
        })
        //new olgm.layer.Google()*/
    ];
    let n = 0;
    dlayers.forEach(function(dlayer) {
        let k = new ol.layer.Image({
            title: dataLayers_titles[n],
            visible: true,
            extent: extent,
            source: eval(dlayer),
            });
        data_layers_group.push(k);
        n++;
    });
    n = 0;
    tlayers.forEach(function(tlayer, i) {
        const order = Number(tileLayers_titles[n].split('_').pop());
        let k = new ol.layer.Tile({
            title: tileLayers_titles[n],
            type: 'base',
            visible: (isNaN(order)) ? (i === 0) : (order === 1),
            extent,
            source: eval(tlayer),
            });
        base_layers_group.push(k);
        n++;
    });
    let layers = [
        new ol.layer.Group({
            title: 'Base maps',
            layers: base_layers_group
        }),
        new ol.layer.Group({
            title: 'Markers',
            layers: [
                vectorLayer,markerLayer
            ]
        }),
        new ol.layer.Group({
            title: 'Data layers',
            layers: data_layers_group
        })
    ];
    
    const extendes = [
        new ol.control.ZoomToExtent({
          extent: ol.proj.transformExtent(obj.extent, 'EPSG:4326', 'EPSG:3857')
        }),
        new ol.control.ScaleLine(),
        new ol.control.MousePosition({
            coordinateFormat: new ol.coordinate.createStringXY(4),
            projection: 'EPSG:4326',
            className: 'custom-mouse-position',
            target: document.getElementById('mouse-position'),
        }),
        new ol.control.OverviewMap({
          className: 'ol-overviewmap ol-custom-overviewmap',
          layers: [
            new ol.layer.Tile({
              source: new ol.source.OSM(),
            }),
          ],
        }),
    ];

    if (typeof $.cookie('lastZoom')!=='undefined') {
        try {
            let v = JSON.parse($.cookie('lastZoom'));
            center = new ol.proj.fromLonLat([obj.lon,obj.lat]);
            zoom = v.zoom;
        } catch (e) {}
    } else {
        center = new ol.proj.fromLonLat([obj.lon,obj.lat]);
        zoom = obj.zoom;
    }
    
    map = new ol.Map({
      target: 'map',
      controls: new ol.control.defaults().extend(extendes),
      interactions: new ol.interaction.defaults({
            mouseWheelZoom: false,
            //dragPan: false,
        }),
      layers: layers,
      view: new ol.View({
        center: center,
        projection: projection,
        extent: extent,
        zoom: zoom,
      }),
    });

    let layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Layers', // Optional label for button
        groupSelectStyle: 'children' // Can be 'children' [default], 'group' or 'none'
    });
    map.addControl(layerSwitcher)

    // mouse wheel zoom cookie, interaction
    if (typeof $.cookie('wz')!=='undefined') {obj.zwe=$.cookie('wz');}
    mouseWheelInt = new ol.interaction.MouseWheelZoom();
    map.addInteraction(mouseWheelInt);
    mouseWheelInt.setActive(obj.zwe);

    select_interaction = new ol.interaction.Select({
        // make sure only the desired layer can be selected
        layers: function(vectorLayer) {
            return vectorLayer.get('name') === 'drawLayer';
        }
    });
    modify_interaction = new ol.interaction.Modify({
        source: vectorLayer.getSource(),
    });

    map.addInteraction(select_interaction);
    map.addInteraction(modify_interaction);

    /* It is more complicated,
     * if we would like to use ol.events.condition.platformModifierKeyOnly
     */
    /*if (is_touch_enabled) {
        mouseWheelInt.setActive(false);
        dragMove =  new ol.interaction.DragPan({
            condition: function (event) {
                    return this.getPointerCount() === 2;// || new ol.events.condition.platformModifierKeyOnly();
            }
        });
        map.addInteraction(dragMove);
        dragMove.setActive(true);
    }*/

    dragBox = new ol.interaction.DragBox({
        //condition: new ol.events.condition.platformModifierKeyOnly(),
    });

    map.addInteraction(dragBox);
    dragBox.setActive(false);

    // Draw on map
    ExampleDraw = {
      init: function () {
        map.addInteraction(this.Point);
        this.Point.setActive(false);
        map.addInteraction(this.LineString);
        this.LineString.setActive(false);
        map.addInteraction(this.Polygon);
        this.Polygon.setActive(false);
        map.addInteraction(this.Circle);
        this.Circle.setActive(false);
      },
      Point: new ol.interaction.Draw({
        source: vectorLayer.getSource(),
        type: 'Point',
      }),
      LineString: new ol.interaction.Draw({
        source: vectorLayer.getSource(),
        type: 'LineString',
      }),
      Polygon: new ol.interaction.Draw({
        source: vectorLayer.getSource(),
        type: 'Polygon',
      }),
      Circle: new ol.interaction.Draw({
        source: vectorLayer.getSource(),
        type: 'Circle',
      }),
      activeDraw: null,
      setActive: function (active) {
        if (this.activeDraw) {
          this.activeDraw.setActive(false);
          this.activeDraw = null;
        }
        if (active) {
          let type = $('input[name=draw-type]:checked', '#map-tools-box').val();
          if (typeof type === 'undefined') {
            type = 'Polygon'
          } 
          this.activeDraw = this[type];
          this.activeDraw.setActive(true);
        }
      },
    };
    ExampleDraw.init();
    
    ExampleModify = {
      init: function () {
        this.select = new ol.interaction.Select();
        map.addInteraction(this.select);

        this.modify = new ol.interaction.Modify({
          features: this.select.getFeatures(),
        });
        map.addInteraction(this.modify);

        this.setEvents();
      },
      setEvents: function () {
        const selectedFeatures = this.select.getFeatures();

        this.select.on('change:active', function () {
          selectedFeatures.forEach(function (each) {
            selectedFeatures.remove(each);
          });
        });
      },
      setActive: function (active) {
        this.select.setActive(active);
        this.modify.setActive(active);
      },
    };
    ExampleModify.init();

    ExampleDraw.setActive(false);
    ExampleModify.setActive(false);
     
    const snap = new ol.interaction.Snap({
      source: vectorLayer.getSource(),
    });
    map.addInteraction(snap);
    
    IdentifyPoint = {
        init: function () {

        },
        setActive: function (active) {
            if (active)
                singleClickListener = 'on';
            else
                singleClickListener = 'off';
        },
    }
    modify_interaction.on('modifyend',function(e){
        saveData();
    });
    dragBox.on('boxend', function () {
        const extent = dragBox.getGeometry().getExtent();
        map.getView().fit(new ol.geom.Polygon.fromExtent(extent)); 
    });


    // Sketch complete
    this.vectorLayer.getSource().on('addfeature', function(event){
        //var geom = event.feature.getGeometry();
        saveData();
    });
    
    // get the features from the select interaction
    let selected_features = select_interaction.getFeatures();
    // when a feature is selected...
    selected_features.on('add', function(event) {
        // get the feature
        let feature = event.element;
        // ...listen for changes on it
        //feature.on('change', saveData);

        $(document).on('keyup', function(event) {
            if (event.keyCode == 46) {
                // remove all selected features from select_interaction and my_vectorlayer
                selected_features.forEach(function(selected_feature) {
                  let selected_feature_id = selected_feature.getGeometry();
                  // remove from select_interaction
                  selected_features.remove(selected_feature);
                  // features aus vectorlayer entfernen
                  let vectorlayer_features = vectorLayer.getSource().getFeatures();
                  vectorlayer_features.forEach(function(source_feature) {
                    let source_feature_id = source_feature.getGeometry();
                    if (source_feature_id === selected_feature_id) {
                      // remove from my_vectorlayer
                      vectorLayer.getSource().removeFeature(source_feature);
                      // save the changed data
                      saveData();
                    }
                  });
                });
                // remove listener
                $(document).off('keyup');
            }
        });
    });

    map.on("singleclick", function(evt) {
        if (singleClickListener == 'on') {
            map_single_click(evt); // dummy function by default; can be overwrite from modules
        }
    });

} //init


