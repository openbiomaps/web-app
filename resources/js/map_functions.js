/* Openlayers map event handling
 *
 */

// dummy function
function map_single_click(evt) {
    return;
}

/* Highlight and zoom to given features
 * Arguments: GeoJSON (feature collections)
 * */
function highlightFeatures(featurecollection) {
    if (!featurecollection) return;

    let geojson_format = new ol.format.GeoJSON();
    let geojsonsource = new ol.source.Vector();

    let features = geojson_format.readFeatures(featurecollection,{
                'dataProjection': "EPSG:4326",
                'featureProjection': "EPSG:3857",
            });

    vectorLayer.getSource().addFeatures(features);
    vectorLayer.setStyle([
        new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.4)',
            }),
            stroke: new ol.style.Stroke({
                color: '#ff00ee',
                width: 5,
            }),
            image: new ol.style.Circle({
                radius: 14,
                fill: new ol.style.Fill({
                    color: 'rgba(255, 204 ,51, 0.6)',
                }),
                stroke: new ol.style.Stroke({
                    color: '#ff00ee',
                    width: 5,
                }),
            }),
        })
    ]);
    
    let extent = vectorLayer.getSource().getExtent();

    if (extent[0] == extent[2]) {
        extent = ol.proj.transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
        map.getView().setCenter( new ol.proj.fromLonLat([extent[0],extent[1]]) ); // transform to default EPSG:3857
        mat.getView().setZoom(6);
    } else {
        map.getView().fit(new ol.geom.Polygon.fromExtent(extent),{padding: [20, 20, 20, 20]}); 
    }

}
/* Highlight mapserver response GML 
 * not used
 *
 * Not updated to OpenLayers 6.x
 * */
function highlightGML(response) {

    //var format = new OpenLayers.Format.GML({extractAttributes: true});

    const format = new OpenLayers.Format.GML({
        'internalProjection': new OpenLayers.Projection("EPSG:2357"),
        'externalProjection': new OpenLayers.Projection("EPSG:4326")
    });

    let features = format.read(response.responseXML || response.responseText);
    drawLayer.addFeatures(features);
    drawLayer.setVisibility(true);
    drawLayer.redraw();
    //drawFeatures(features,highlightLayer);
    let points = []; 
    parallel = features.length;
    $( "#dialog" ).text("Processing the query...");
    let isOpen = $( "#dialog" ).dialog( "isOpen" );
    if(!isOpen) $( "#dialog" ).dialog( "open" );

    for (let i=0;i<features.length;i++) {
        let g = features[i].geometry.clone();
        g.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
        let filter = new OpenLayers.Filter.Spatial({
            type: OpenLayers.Filter.Spatial.INTERSECTS,
            value: g
        });

        //proxy and wfs xml processing
        //should be called only once !!!
        let format = new OpenLayers.Format.Filter();
        let str = format.write(filter); 
        let params = {
            "REQUEST": "GetFeature",
            "TYPENAME": obj.query_layer,
            "FILTER": str.outerHTML
        };
        let url = obj.query_url + "&" + OpenLayers.Util.getParameterString(params);
        let req = OpenLayers.Request.GET({url:url,callback:WFSGet,scope:{"skip_processing":'no'}});
    }
}

/* SELECTION on map and report the WFS result
 * callback function 
 * SEPARATE TO three FUNCTIONS:
 * draw buffered point and line string 
 * wfs query on polygons
 * highlight selected points
 * */
//var geom_here;
function GetDataOfSelection(geometry) {

    //SAVE last zoom position...
    let center = map.getView().getCenter();
    let zoom = map.getView().getZoom();
    let v = { 'center':center, 'zoom':zoom }
    $.cookie('lastZoom',JSON.stringify(v));

    //Dialog
    //$( "#dialog" ).dialog( "option", "position", { my: 'center', of: window } );
    //$( "#dialog" ).dialog( "option", "title", obj.message );
    //$( "#dialog" ).text(obj.processing_query+'...');

    //var isOpen = $( "#dialog" ).dialog( "isOpen" );
    //if (!isOpen) $( "#dialog" ).dialog( "open" );
    
    // should be included https://github.com/bjornharrtell/jsts
    // https://www.npmjs.com/package/jsts
    let b,g,xy;
    $("#navigator").hide();
    

    // Clear features
    //vectorLayer.getSource().clear();
    
    if (obj.turn_off_layers!='') {
        for (let i=0;i<dataLayers.length;i++) {
            let o = dataLayers[i];
            off = obj.turn_off_layers.split(',');
            let index = jQuery.inArray( dataLayers_ns[i] , off );
            if (index > -1) {
                //o.setVisibility(false);
            }
        }
    }
    //$( "#dialog" ).text("Drawing query map...");

    /* Select tools: point, line, polygon
     * create buffers around selection 
     * Zoom to results */
    if ($('#buffer').length > 0) {
        b = $("#buffer").val();
    } else { 
        b = obj.buffer; 
    }
    //var neighbour = 0;
    //var ce;
    
    /*
    geometry_query_neighbour = 0;
    geometry_query_obj = '';
    geometry_query_session = 0;
    if (new String(event.feature.geometry).match(/POINT|LINE/) && b>0) {
        // create buffer around point
        //var geometry = new OpenLayers.Geometry.Polygon.createRegularPolygon(event.feature.geometry, 500, 30);
        //var addCircul = new OpenLayers.Feature.Vector(geometry, null, click_style);
        var reader = new jsts.io.WKTReader();
        var input = reader.read(new String(event.feature.geometry));
        var buffer = input.buffer(b);
        var parser = new jsts.io.OpenLayersParser();
        //input = parser.write(input);
        buffer = parser.write(buffer);
        var geometry = buffer;
        var feature = new OpenLayers.Feature.Vector(buffer, null, click_style);
        // Draw the polygon on map
        drawFeatures(feature,drawLayer);
        drawLayer.refresh({force: true});
    } else {
        // polygon | zero buffer
        var geometry = event.feature.geometry.clone();
        if (new String(event.feature.geometry).match(/POINT/) && b==0) {
            // query nearest neighbourhoud
            //neighbour = 1;
            geometry_query_neighbour = 1;
            var currentExtent = map.getExtent();
            // previously ce
            geometry_query_obj = currentExtent.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
        }
    }
    g = geometry.transform(map.getProjectionObject(),new OpenLayers.Projection("EPSG:4326"));
    // it will be used in HighLightFeature()
    zoomTo = new OpenLayers.Geometry.Collection(g);
    //wkt string trasformation
    var geomlist = g.toString();
    geom_here = geomlist;
    */
    
    //Save selection
    $.post("ajax", { auto_save_selection:geometry }, function(data) {
        if (data) {
            let retval = jsendp(data);
            if (retval['status']=='error') {
                $( "#dialog" ).dialog( "open" );
                $( "#error" ).html(retval['message']);
            } else if (retval['status']=='fail') {
                $( "#dialog" ).dialog( "open" );
                $( "#dialog" ).text("An error occured while creating the geometry selection.");
            } else if (retval['status']=='success') {
                geometry_query_session = 1;

                let $blink = $("#sendQuery");
                let blink_counter1 = 10;
                let backgroundInterval = setInterval(function(){
                    $blink.toggleClass("button-success");
                    blink_counter1--;
                    if (blink_counter1==0) {
                        clearInterval(backgroundInterval);
                    }
                },600)

                //v = retval['data'];

                //var myVar = { 'geom_selection':'session','neighbour':neighbour,'ce':JSON.stringify(ce) }
                //loadQueryMap(myVar);
            }
        } else {
            $( "#dialog" ).text("Saveing selection failed.");
        }
    });

}


/* It used by HRSZ_Query moudule 
 * Draw a polygon from given WKT string
 * wkt is coming as a JSON object
 * */
function drawPolygonFromWKT(input,zoom) {

    let v = JSON.parse(input);
    /* multigeometry trick; would be better with mapserver support */
    const wkt = new ol.format.WKT();
    let options = {
        'dataProjection': 'EPSG:4326',
        'featureProjection': 'EPSG:3857',
    };
    if(v.constructor != Array) {
        v = [v];
    }

    for (var j=0;j<v.length;j++) {
        let features = wkt.readFeatures(v[j],options);
        if (features) {
                if (features.constructor != Array) {
                    features = [features];
                }
                vectorLayer.getSource().addFeature(features[0]);
        }
    }
    let bounds = vectorLayer.getSource().getExtent();
    if (zoom) {
        map.getView().fit(bounds,{padding: [20, 20, 20, 20]});
    }
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(markMyPosition);
    } else { 
        alert( "Geolocation is not supported by this browser." );
    }
}
function markMyPosition(position) {

    let X = position.coords.latitude;
    let Y = position.coords.longitude;
    let p = new ol.geom.Point([Y,X]);
    
    var currentZoom = map.getView().getZoom();
    p.transform("EPSG:4326", map.getView().getProjection()); 
    markerLayer.getSource().clear();
    const iconFeature = new ol.Feature({
        geometry: p,
        name: "My Position Marker",
    });
    const iconStyle = new ol.style.Style({
        image: new ol.style.Icon({
            anchor: [0.5, 25],
            anchorXUnits: "fraction",
            anchorYUnits: "pixels",
            src: "js/img/map-marker-32.png",
            size: [32,32],
            scale: 1,
        })
    });
    iconFeature.setStyle(iconStyle)
    markerLayer.getSource().addFeature(iconFeature);
    let centerPoint = new ol.proj.fromLonLat([Y,X]);
    map.getView().setCenter(centerPoint);
    map.getView().setZoom(currentZoom);
}
/* a colour ring box  function */
// set colour-ring-box value into text-filter box  
function set_ring_value(vj) {
   let pos=$("#colour-ring-referenceid").val();
   vj = vj.replace(/X/g,'_');
   $("#"+pos).val(vj);
}
var header_on_top = true;

function saveData() {
    
    /* Write out vector features in the selected format - cool
    // get the format the user has chosen
    var data_type = $data_type.val(),
    // define a format the data shall be converted to
    format = new ol.format[data_type](),
    // this will be the data in the chosen format
    data;
    
    try {
        // convert the data of the vector_layer into the chosen format
        data = format.writeFeatures(vector_layer.getSource().getFeatures());
    } catch (e) {
        // at time of creation there is an error in the GPX format (18.7.2014)
        $('#data').val(e.name + ": " + e.message);
        return;
    }
  
    if ($data_type.val() === 'GeoJSON') {
        // format is JSON
        $('#data').val(JSON.stringify(data, null, 4));
    } else {
        // format is XML (GPX or KML)
        var serializer = new XMLSerializer();
        $('#data').val(serializer.serializeToString(data));
    } */
    let format = new ol.format.WKT();
    //var c = new ol.geom.GeometryCollection();

    let src = 'EPSG:3857';
    let dest = 'EPSG:4326';

    //var data = format.writeFeatures(vectorLayer.getSource().getFeatures());

    let features = vectorLayer.getSource().getFeatures();
    let wktRepresenation = []
    features.forEach(function(element) {

        let type = element.getGeometry().getType();
        if (type == 'Circle') {
            let circlepolygon = new ol.geom.Polygon.fromCircle(element.getGeometry().clone());
            wktRepresenation.push(format.writeGeometry(circlepolygon.transform(src,dest)));
        } else {
            wktRepresenation.push(format.writeGeometry(element.getGeometry().clone().transform(src,dest)));
        }
    });
    GetDataOfSelection(wktRepresenation);

    /*if (data != 'GEOMETRYCOLLECTION EMPTY') {
        GetDataOfSelection(data);
    }*/

}

function toggleControl(element) {

    if (element.value == 'featureEditor' && !$('#editToggle').is(':checked')) { 
        ExampleDraw.setActive(true);
        ExampleModify.setActive(false);
        $('.maparea').awesomeCursor('pencil',{
            hotspot: 'bottom left',
        });
    }
    else if (element.value == 'featureEditor' && $('#editToggle').is(':checked')) { 
        let type = $('input[name=draw-type]:checked', '#map-tools-box').val();
        if (typeof type === 'undefined') {
            $('#editToggle').prop('checked', false);
            return false;
        }
        ExampleDraw.setActive(false);
        ExampleModify.setActive(true);
        $('.maparea').awesomeCursor('hand-lizard-o',{
            hotspot: 'center left',
        });
    }
    else if (element.value != 'featureEditor') { 
        $('.maparea').awesomeCursor('pencil',{
            hotspot: 'bottom left',
        });
        $('#editToggle').prop('checked', false);
        ExampleDraw.setActive(true);
        ExampleModify.setActive(false);
    }
}
function toggleWZ(element) {
    mouseWheelInt.setActive(!mouseWheelInt.getActive())
}

$(document).ready(function() {

    $('#mapcontrols').on('click','.maphands',function(e){
        $("#identify_point").removeClass('button-secondary');
        IdentifyPoint.setActive(false);
        dragBox.setActive(false);
    });
    $('#mapcontrols').on('click','#noneToggle',function(e){
        $("#map").css('cursor','pointer');
        $("#identify_point").removeClass('button-secondary');
        ExampleDraw.setActive(false);
        ExampleModify.setActive(false);
        IdentifyPoint.setActive(true);
        dragBox.setActive(false);
        //dragMove.setActive(true);
    });
    $('#mapcontrols').on('click','#identify_point',function(e) {
        $("#noneToggle").trigger("click");
        $("#map").css('cursor','help');
        $(this).addClass('button-secondary');
        ExampleDraw.setActive(false);
        ExampleModify.setActive(false);
        dragBox.setActive(false);
    });
    $('#mapcontrols').on('click','#zoomToggle',function(e) {
        $("#map").css('cursor','crosshair');
        $("#identify_point").removeClass('button-secondary');
        ExampleDraw.setActive(false);
        ExampleModify.setActive(false);
        IdentifyPoint.setActive(false);
        dragBox.setActive(true);
    });

    // buffer change on map selection
    
    $('body').on('change','#buffer', function(){
        let buffer = $(this).val();
        $("#bufferslide").val(buffer);
        $.cookie("buffer",$(this).val());
        if (buffer == 0) {
            $('#pointToggle').prop('disabled', true);
            $('#pointToggle_icon').css('display','none');
        }
        else {
            $('#pointToggle').prop('disabled', false);
            $('#pointToggle_icon').css('display','block');
        }
    });
    $('body').on('input','#bufferslide', function() {
        $("#buffer").val($(this).val());
        let buffer = $(this).val();
        $.cookie("buffer",$(this).val());
        if (buffer == 0) {
            $('#pointToggle').prop('disabled', true);
            $('#pointToggle_icon').css('display','none');
        }
        else {
            $('#pointToggle').prop('disabled', false);
            $('#pointToggle_icon').css('display','block');
        }

    });
    $('#mapholder').on('click','#show-my-position',function(){
        getLocation();
    });

    if(typeof $.cookie('buffer')!=='undefined'){$('#buffer').val($.cookie('buffer'));$('#bufferslide').val($.cookie('buffer'));}

    if (obj.fixheader=='false') {
        $('#bheader').css('position','fixed');
        $('#mapfilters').css('padding-top','80px');
    }
    
    // újra kell inicializálni a térképet, különben elveszik a méretezett rész...
    //$( window ).resize(function() {
    //    h=$('body').innerHeight(); // New height
        //$(".maparea").height(h-156);
    //});

    // result area of map queries
    $('#matrix').on('click','#flyingadd',function(){
        $(this).hide('slow');
    });

    /* set query table */
    $('#current_query_table').change(function(){
        $.post('ajax',{'set-current-query-table':$(this).val()},function(data) {
            let retval = jsendp(data);
            if (retval['status']=='success') { 
                window.location = retval['data'];
            }

        });
    });

    $(document).scroll(function() {

        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
            // rightside menu
            $('#scrolldown').after($('#scrollup'));
        } else {
            $('#scrollup').after($('#scrolldown'));
        }
        
        if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
            
            // top menu
            if (obj.fixheader=='false' && header_on_top) {
                $( "#bheader" ).animate({
                    top: "0",
                    right: "0",
                    height: "60px",
                    width: $('.mapfb').width(),
                }, 
                {
                    duration: 500,  
                    start: function() {
                        $('#bheader').css("border-bottom",'none'),
                        $('#bheader .main-logo-text').hide(),
                        $('#bheader .main-logo-img').addClass('logo-img_small'),
                        $('#bheader .htitle').addClass('htitle_small'),
                        $("#bheader").appendTo("#smallheader"),
                        $('#nav').parent().css('padding-top','0'),
                        $('#nav').parent().css('padding-right','0'),
                        $('#nav .topnav li:eq(0)').hide(),
                        $('#nav .topnav li:eq(1)').hide(),
                        $('#nav .topnav').css('padding','0'),
                        $('#mapfilters').css('padding-top','0'),
                        header_on_top = false
                    },
                    complete: function() {
                        $('#maintenance').hide();
                        $('#bheader').css('position','initial'),
                        $('.olControlZoomPanel').css('top','71px'), // 71
                        $('.olControlPanPanel').css('top','10px'),   // 10
                        $('.olControlLayerSwitcher').css('top','5px') // 5
                    }
                });
            }
        }
    });
    $("#scrollup").click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false; 
    });
    $("#scrolldown").click(function(){
        $("html, body").animate({ scrollTop:  $(document).height()-$(window).height() }, "slow");
        return false; 
    });
    $("#hidenavigator").click(function(){
        $("#navigator").hide('slow');
        $("#shownavigator").show();
    });
    $("#shownavigator").click(function(){
        $("#shownavigator").hide();
        $("#navigator").show();
    });

    var we = 0;
    $('#smallheader').on('click','.subnav', function(e) {
         we = 1;
    });

    /* fixing bheader - only in mappage!! */
    $('#smallheader').on('click','#bheader', function(e) {
        if (we == 0) {
            e.preventDefault();

        $( "#bheader" ).animate({
            width: '100%',
            height: 'auto',
        }, 
        {
            duration: 500,  
            start: function() {
                $('#maintenance').show();
                $('#bheader').css('position','fixed'),
                $('#bheader').css('border','none'),
                $('#bheader').css('border-bottom','1px solid #acacac')
            },
            complete: function() {
                $('#bheader .main-logo-text').show(),
                $('#bheader .htitle').removeClass('htitle_small'),
                $('#nav').parent().css('padding-top','10'),
                $('#nav').parent().css('padding-right','20'),
                $('#nav .topnav li:eq(0)').show(),
                $('#nav .topnav li:eq(1)').show(),
                $('#nav .topnav').css('padding','10px'),
                $('.olControlZoomPanel').css('top','151px'), // 71
                $('.olControlPanPanel').css('top','90px'),   // 10
                $('.olControlLayerSwitcher').css('top','85px'), // 5
                $("#bheader .main-logo-img").removeClass('logo-img_small'),
                $("#bheader").prependTo("#holder"),
                $('#mapfilters').css('padding-top','80px'),
                header_on_top = true
            }
        });
        }
    });

    //clear morefilter action
    $("body").on('click','.clear-morefilter',function(){
        $.post('ajax',{'clear-morefilter':1},function(data){
            $(".clear-morefilter").removeClass('button-warning');
            $(".clear-morefilter").removeClass('clear-morefilter');
            $(".clear-morefilter").addClass('button-passive');
            //$("#apptq").find('i').toggleClass('fa-toggle-on');
            //$("#apptq").find('i').toggleClass('fa-toggle-off');
            //$("#apptq").removeClass('button-success');

        });
    });

    //$('#drbc').draggable({handle:'div'});
    

    

    //map page more than 2000 records links
    /*$('body').on('click','.paging',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        $( "#dialog" ).text("Waiting for the WFS response...");
        var isOpen = $( "#dialog" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog" ).dialog( "open" );
        var mapurl = obj.query_url + '&repeat='+page;
        //proxy and wfs processing
        OpenLayers.Request.GET({url:mapurl,callback:WFSGet,scope:{"skip_processing":'yes'}});
    });*/

    // turn on-off mousewheel on map
    $('body').on('mousewheel DOMMouseScroll','#buffer', function(event){
        event.preventDefault();
        var n=+$(this).val();
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            // scroll up
            n=eval(n+1);
            $(this).val(n)
            $("#bufferslide").val(n);
        }
        else {
            // scroll down
            n=eval(n-1)
            $(this).val(n)
            $("#bufferslide").val(n);
        }
    });

    //Upload - webform - geometry from map selection
    //click on green button
    //the setValue function is defined in the uploader.js
    $('body').on('click','#setValue',function(e){
        var v=$(this).val();
        if(window.opener) window.opener.setValue(v);
    });

    // photo filter button toggle
    $("#obm_files_id").on('click',function(){
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).removeClass('button-success');
        } else {
            $(this).find('i').toggleClass('fa-toggle-on');    
            $(this).find('i').toggleClass('fa-toggle-off');    
            $(this).addClass('button-success');
        }
    });
    // apply text query over spatial query  - button click
    // turn on - off
    $("body").on('click',"#apptq",function(){
        if($(this).find('i').hasClass('fa-toggle-on')) {
            $(this).find('i').toggleClass('fa-toggle-on');
            $(this).find('i').toggleClass('fa-toggle-off');
            $(this).removeClass('button-success');
            $.post('ajax',{'clear-morefilter':1},function(data){
                $.post('ajax',{'set-morefilter':0},function(data){});
            });
        } else {
            $.post('ajax',{'set-morefilter':1},function(data){});
            $(this).find('i').toggleClass('fa-toggle-on');
            $(this).find('i').toggleClass('fa-toggle-off');
            $(this).addClass('button-success');
        }
    });
    /* CONTROL BOX 
     * MapfilterBox:
     * send text query to WFS server
     * */
    $("#mapcontrols").on('click',"#reset-map",function(){
        //clear filters
        $.post('ajax',{'clear-morefilter':1},function(data){});
        $.post('ajax',{'clear-session_querybuildstring':1},function(data){
            vectorLayer.getSource().clear();
            for (let i=0;i<dataLayers.length;i++) {
                dataLayers[i].refresh();
            }

            let point = new ol.proj.fromLonLat([obj.lon,obj.lat]);
            map.getView().animate({
                center: point,
                zoom: obj.zoom,
                duration: 2000,
            });
            $("#buffer").val(obj.buffer);
            $("#bufferslide").val(obj.buffer);
            geometry_query_neighbour = 0;
            geometry_query_session = 0;
            geometry_query_selection = 0;
        });
        // set dataLayers visible
        /*for(var i=0;i<dataLayers.length;i++) {
            var o = dataLayers[i];
            o.setVisibility(true);
            o.setOpacity(1);
        }
        // turn off query layers
        for(var i=0;i<queryLayers.length;i++) {
            var o = queryLayers[i];
            o.setVisibility(false);
        }*/

    });

    $("body").on("click","#legend",function(e){
        $(this).css('left',"-"+eval($(this).width()-10)+"px");
    });
    $("body").on("dblclick","#legend",function(e){
        $(this).css('left',"0");
    });
    /* a colour ring box  function */
    $( ".mapfb" ).on( "click",".show-colour-ring-box",function() {
        $("#colourringdiv").show();
    });
    $("#mapholder").on("click","#spatial-query-settings",function() {
        $("#spatial-query-settings-box").toggle();
    });
    $("body").on("click",".mtitle",function() {
        $(this).parent().find('.options-box').toggle();
    });
    $(".mapfb").on("click",".title",function(){
        $(this).closest(".mapfb").toggleClass("mapfb-pos");
    });

});
// ez sem müx...
function is_touch_enabled() {
    return ( 'ontouchstart' in window ) ||
           ( navigator.maxTouchPoints > 0 ) ||
           ( navigator.msMaxTouchPoints > 0 );
}

