function init_messenger() {
  $(".mail-box #tags .tag").on("click", function (ev) {
    ev.stopPropagation();
    const messages_section = $(".mail-box #messages");
    const detail_section = $(".mail-box #detail");
    var el = $(this);

    hide_write_section();

    if (detail_section.css("display") != "none") {
      $(".mail-box #detail").hide("fast", function () {
        show_message_list(el);
      });
    } else {
      show_message_list(el);
    }
  });

  $(".mail-box").on("click", ".message-card", function (ev) {
    ev.stopPropagation();
    const detail_section = $(".mail-box #detail");
    var el = $(this);
    detail_section.hide("fast", function () {
      show_message_detail(el);
    });
  });

  $(".mail-box").on("click", ".delete-message", function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    const message_id = $(this).data("message_id");
    delete_message(message_id);
  });

  const editor = conf_quill("#message-editor");

  $(".mail-box #write-pm").on("click", function (ev) {
    $("#to").fSelect({
      placeholder: "&nbsp;",
      searchText: "Search for user name",
      numDisplayed: 5,
      showSearch: true,
    });
    const write_section = $(".mail-box #write");
    const messages_section = $(".mail-box #messages");
    const detail_section = $(".mail-box #detail");
    detail_section.hide("fast", function () {
      messages_section.hide("fast", function () {
        write_section.show("slow", function () {
          $(".mail-box #send-pm").show();
        });
      });
    });
  });

  $(".mail-box #send-pm").on("click", async function (ev) {
    ev.preventDefault();
    $("#message-body").val(editor.root.innerHTML);
    //const messageAsHTML = .innerHTML;
    let result = await send_pm();
    if (result == true) {
      editor.setText("");
    }
  });

  // Messenger - copy addresses to clipboard
  $("#body").on("click", "#copy-addresses-to-clipboard", async function (e) {
    e.preventDefault();

    resp = await $.ajax({
      url: "ajax",
      type: "POST",
      data: {
        messenger: true,
        action: "get_group_addresses",
        group: $("#to").val(),
      },
    });
    resp = JSON.parse(resp);
    if (resp.status === "success") {
      var mails = [];
      for (var i = 0; i < resp["data"].length; i++) {
        mails.push(resp["data"][i]["email"]);
      }
      const copyToClipboard = (str) => {
        const el = document.createElement("textarea");
        el.value = str;
        el.setAttribute("readonly", "");
        el.style.position = "absolute";
        el.style.left = "-9999px";
        document.body.appendChild(el);
        const selected =
          document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
        if (selected) {
          document.getSelection().removeAllRanges();
          document.getSelection().addRange(selected);
        }
      };

      copyToClipboard(mails.join("\n"));
    } else {
      console.error("fail");
    }
  });

  $(".mail-box .reply-message").on("click", function (ev) {
    const mid = $(this).data("message_id");
    let message = obj.messages.filter(function (m) {
        return m.id == mid;
    });

    message = message[0];

    $(`#to option[data-uid=${message.user_from}]`).attr('selected', 'selected');
    $('#message-subject').val(`Re: ${message.subject}`)
    $('#reply-to').val(message.id)

    $("#to").fSelect({
      placeholder: "&nbsp;",
      searchText: "Search for user name",
      numDisplayed: 5,
      showSearch: true,
    });
    const write_section = $(".mail-box #write");
    const messages_section = $(".mail-box #messages");
    const detail_section = $(".mail-box #detail");
    detail_section.hide("fast", function () {
      messages_section.hide("fast", function () {
        write_section.show("slow", function () {
          $(".mail-box #send-pm").show();
        });
      });
    });
  });
}

async function delete_message(message_id) {
  let resp = await $.post("ajax", {
    messenger: true,
    action: "delete_message",
    message_id,
  });

  resp = JSON.parse(resp);
  if (resp.status === "success") {
    $("#dialog").html("Message deleted");
    reload_selected_tag();
  } else {
    $("#dialog").html("Deleting message failed!");
  }
  $("#dialog").dialog("open");

  return resp.status === "success";
}

function reload_selected_tag() {
  const detail_section = $(".mail-box #detail");
  const el = $(".tag.selected");
  if (detail_section.css("display") != "none") {
    $(".mail-box #detail").hide("fast", function () {
      show_message_list(el);
    });
  } else {
    show_message_list(el);
  }
}

async function get_messages(message_type, options = {}) {
  const defaults = {
    messenger: true,
    action: "load_messages",
    message_type: message_type,
    limit: 100,
    offset: 0,
  };

  Object.keys(defaults).forEach(function (key) {
    if (typeof options[key] === "undefined") {
      options[key] = defaults[key];
    }
  });

  const retval = jsendp(await $.post("ajax", options));
  if (retval.status === "success") {
    const messages = retval.data;

    obj.messages = messages;
    $(".mail-box #messages").html("");
    if (messages) {
      messages.forEach(function (message) {
        const message_type = message.message_type;

        const news = message_type.substr(-4) === "news";

        let name = "";
        if (!news) {
          name = `[ ${message.project_table} ] ${message.from_name}: `;
        }
        if (options.message_type === "sent") {
          name = `[ ${message.project_table} ] ${message.to_name}: `;
        }
        if (
          message_type === "project news" ||
          message_type === "system message"
        ) {
          name = `[ ${message.project_table} ] `;
        }
        if (message_type == "system news") {
          name = "[ OpenBioMaps ] ";
        }

        let unread = "";
        if (
          !news &&
          options.message_type != "sent" &&
          message.receiver_open === null
        ) {
          unread = "unread";
        }
        const date = message.sender_open.substring(0,19)
        let html = `<div data-message_id="${message.id}" class="message-card ${unread}">`;
        html += `<div class="from_and_subject">${name}${message.subject}</div>`;
        html += `<div class="date">${date}</div>`;
        html += "</div>";
        $(".mail-box #messages").append(html);
      });
    }
  } else if (retval.status == "fail") {
    console.error("ajax failed");
  }
}

async function show_message_detail(el) {
  $(".mail-box #messages div").removeClass("selected");
  $(".mail-box #detail #detail-toolbar button").hide()
  el.addClass("selected");

  const mid = el.data("message_id");
  let message = obj.messages.filter(function (m) {
    return m.id == mid;
  });

  message = message[0];
  if (message.own) {
    $(".mail-box #detail #detail-toolbar .delete-message").show().data('message_id', message.id)
    $(".mail-box #detail #detail-toolbar .reply-message").show().data('message_id', message.id)
  }
  if (
    message.message_type.substr(-4) !== "news" &&
    message.receiver_open === null
  ) {
    let resp = JSON.parse(
      await $.post("ajax", {
        messenger: true,
        action: "message_read",
        message_id: message.id,
        message_type: message.message_type,
      })
    );

    const numbers = resp.data;
    const total = Object.values(numbers).reduce(
      (sum, item) => Number(sum) + Number(item)
    );

    el.removeClass("unread");
    change_unread_badge(
      $('[data-message_type="' + message.message_type + '"]'),
      numbers[message.message_type]
    );
    change_unread_badge(
      $('#body [data-url="includes/profile.php?options=newsread"]'),
      total
    );
    const header_unread = $("#header-unread-envelope");
    if (total == 0) {
      header_unread.remove();
    } else {
      header_unread.find("span").html(total);
    }
  }
  $(".mail-box #detail #recipient span").html(message.to_name);
  $(".mail-box #detail #sender span").html(message.from_name);
  $(".mail-box #detail #date span").html(message.sender_open.substring(0, 19));
  $(".mail-box #detail #subject span").html(message.subject);
  $(".mail-box #detail #message_body").html(message.message);

  $(".mail-box #detail").show("fast");
}

function change_unread_badge(par, nr) {
  const badge = par.find(".unread_messages_badge");
  if (nr == 0) {
    badge.remove();
  } else {
    badge.html(nr);
  }
}

async function show_message_list(el) {
  $(".mail-box #messages").hide("fast");
  $(".mail-box #tags div").removeClass("selected");
  el.addClass("selected");
  let message_type = el.data("message_type");

  await get_messages(message_type);

  $(".mail-box #messages").show("fast");
}

function hide_write_section() {
  $(".mail-box #write").hide("fast");
  $(".mail-box #send-pm").hide();
}

async function send_pm() {
  let formElement = document.getElementById("pm-form");
  const fd = new FormData(formElement);
  fd.append("messenger", true);
  if (!fd.has("to[]")) {
    return;
  }
  if (
    fd.get("message-subject") == "" &&
    fd.get("message-body") == "<p><br></p>"
  ) {
    $("#dialog").text("Subject or message are required!");
    $("#dialog").dialog("open");
    return;
  }
  resp = await $.ajax({
    url: "ajax",
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
  });

  resp = JSON.parse(resp);
  if (resp.status === "success") {
    $("#dialog").html(resp.data.join("<br>"));
    formElement.reset();
  } else {
    console.error("fail");
    $("#dialog").html(resp.message);
  }
  if (!$("#dialog").dialog("isOpen")) {
    $("#dialog").dialog("open");
  }

  return resp.status === "success";
}

function conf_quill(selector) {
  const toolbarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["code-block"],

    [{ list: "ordered" }, { list: "bullet" }],
    [{ indent: "-2" }, { indent: "+0" }], // outdent/indent

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [0, 1, 2, 3, 4, 5, false] }],

    ["link"],

    ["clean"],
  ];

  const options = {
    modules: {
      toolbar: toolbarOptions,
    },
    placeholder: "Message text ...",
    theme: "snow",
  };
  const editor = new Quill(selector, options);
  return editor;
}

function copyToClipboard(element) {
  let $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).html()).select();
  document.execCommand("copy");
  $temp.remove();
}
