<?php
$ref =  sprintf('%s://%s',$protocol,URL);

if (defined('FOOTER'))
    $footer = constant('FOOTER');
else
    $footer = array(
        'links' => 'upload|map|about|terms|privacy',
        'languages' => 'languages',
        'partners' => array(
            array('img'=>'obm_logo.png','size'=>'110','url'=>'https://openbiomaps.org'),
            array('img'=>'unideb_logo.png','size'=>'','url'=>'https://unideb.hu')
        ),
        'text' => ''
    );
    
extract($footer,EXTR_OVERWRITE);

$links = preg_split('/\|/',$links);

?>
<div id='footer'>
<table>
<?php
if (isset($links) && count($links)) {
?>

    <tr id='footer-tr1'>
        <td style='vertical-align:top;text-align:center'>
<?php

$n = 0;
foreach($links as $l) {

    if ($n > 0)
        echo "<span class='fl-sep'></span>";

    if ($l == 'upload')
        echo "<a href='$protocol://". URL ."/upload/' class='fl'>". t(str_upload) ."</a>";
    elseif ($l == 'about')
        echo "<a href='$ref/index.php?database' class='fl'>". t(str_about_db) ."</a>";
    elseif ($l == 'terms')
        echo "<a href='$protocol://". URL ."/terms/' class='fl'>". t(str_terms_and_conditions) ."</a>";
    elseif ($l == 'privacy')
        echo "<a href='$protocol://". URL ."/privacy/' class='fl'>". t(str_privacy_policy) ."</a>";
    elseif ($l == 'map')
        echo "<a href='$protocol://". URL ."/map/' class='fl'>". t(str_map) ."</a>";
    elseif ($l == 'usage')
        echo "<a href='$protocol://". URL ."/usage/' class='fl'>". t(str_usage) ."</a>";
    elseif ($l == 'datausage')
        echo "<a href='$protocol://". URL ."/useagreement/' class='fl'>". t(str_data_usage) ."</a>";
    elseif ($l == 'whatsnew')
        echo "<a href='$protocol://". URL ."/whatsnew/' class='fl'>". t(str_whatsnew) ."</a>";
    elseif ($l == 'cookies')
        echo "<a href='$protocol://". URL ."/cookies/' class='fl'>". t(str_cookies) ."</a>";
    elseif ($l == 'technical')
        echo "<a href='$protocol://". URL ."/technical/' class='fl'>". t(str_technical_info) ."</a>";
    $n++;
}

?>
        </td>
    </tr>
<?php
}

if (isset($languages) && count(LANGUAGES)) {
?>
    <tr id='footer-tr2'>
        <td style='vertical-align:top;text-align:center'>
<?php

if ($languages == 'languages' ) {
    echo "<br><br>";
    echo str_other_langs.":<br>";
    foreach (LANGUAGES as $L=>$label) {
            echo "<a href='?lang=$L' class='fl'>$label</a>";
    }
}

?>
        </td>
    </tr>
<?php
}

if (isset($partners) && count($partners)) {
?>
    <tr id='footer-tr3'>
        <td style='vertical-align:top;text-align:center'><br><br>
<?php

    echo str_contrib_partner.":<br>";

foreach($partners as $p) {
    $size = '';
    if (isset($p['size']) and $p['size']!='')
        $size = "width:".$p['size']."px;";
    printf("<a href='%s' class='flb'><div class='inset' style='%sbackground-image:url(%s://%s%s/images/%s)'></div></a> ",
        $p['url'],$size,$protocol,URL,STYLE_PATH,$p['img']);

  //      <a href='https://www.unideb.hu' class='flb' target='_blank'><div class='inset' style='background-image:url( $protocol :// URL /images/unideb_logo.png)'></div></a>
}
?>
        </td>
    </tr>

<?php
}
if (isset($text) && $text != '') {
?>
    <tr id='footer-tr4'>
        <td style='vertical-align:top;text-align:center'>
        <?php echo $text ?>
        </td>
    </tr>

<?php
}
?>

</table>
</div><!--/footer-->

