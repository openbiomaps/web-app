<?php
if (defined('HEADER'))
    $header = constant('HEADER');
else
    $header = array(
        'links' => 'upload|map|messages|profile|localize',
        'layout' => 'obm'
    );
    
extract($header,EXTR_OVERWRITE);

$links = preg_split('/\|/',$links);

if ($layout == 'obm') {

render_view('components/header', (function () {

    global $protocol, $OB_project_title, $M, $maintenance_message, $links;

    $logged_in = (isset($_SESSION['Tid']));
    
    $ref =  sprintf('%s://%s',$protocol,URL);
    $show_project_title = !(get_option('project_title') == 'hide');
    $title_rgb = (defined("TITLE_RGB")) ? constant('TITLE_RGB') : '159,198,81';
    $doclang = ($_SESSION['LANG'] == 'hu') ? 'hu' : 'en';
    $other_databases_uri = "$protocol://".OB_DOMAIN."/projects/";    

    // logo
    if (file_exists(getenv('PROJECT_DIR').'local/styles/app/images/logo.png')){
        $logo_uri = $protocol . '://' . URL . '/local/styles/app/images/logo.png';
    } else {
        $logo_uri = $protocol . '://' . URL . STYLE_PATH.'/images/logo.png';
    }

    // translations
    $strings = [
        'str_upload', 'str_map', 'str_settings', 'str_db_summary',
        'str_feedback', 'str_logout', 'str_login', 'str_documentation', 
        'str_other_databases',
    ];
    $t = array_combine($strings, array_map('t', $strings));

    // languages
    $languages = array_map(function ($l, $label) {
        return compact('l','label');
    }, array_keys(LANGUAGES), LANGUAGES);
    $multilingual = (in_array('localize',$links) && count(LANGUAGES) > 1) ? ['languages' => $languages] : [];
    $active_lang = $_SESSION['LANG'];

    $map = in_array('map',$links) ? true : false;
    $upload = in_array('upload',$links) ? true : false;
    $profile = in_array('profile',$links) ? true : false;
    
    // user data
    if ($logged_in) {
        $profile_title = $_SESSION['Tname']; 
        $unread = $M->get_total_unread_number();
        $unread_link_title = sprintf(t('str_you_have_new_message'),$unread);
        $userdata = [
            'email' => $_SESSION['Tmail'],
            'name' => $_SESSION['Tname'],
            'profile_uri' => "$ref/profile/{$_SESSION['Tcrypt']}/"
        ];
        $request_uri = '';
    }
    else {
        $url = parse_url($protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        $request_uri = "{$url['scheme']}://{$url['host']}{$url['path']}";

        $profile_title = t('str_login');
        $unread = $unread_link_title = false;
        $userdata = [];
    }
    
    $unread = in_array('messages',$links) ? $unread : 0;

    return compact( 
        "ref",
        "request_uri",
        "doclang",
        "logo_uri",
        "show_project_title",
        "title_rgb",
        "OB_project_title",
        "other_databases_uri",
        "maintenance_message",
        "logged_in",
        "unread",
        "unread_link_title",
        "profile_title",
        "userdata",
        "active_lang",
        "multilingual",
        "map",
        "upload",
        "profile",
        "t"
    );
})());

} else {
    // non OBM layout
    // I don't know how to use this feature yet.

}

?>
