<?php

$path = array('');
$entry_point = '';
$chunks = array();

if (isset($_GET['path'])) {
    $path = explode("/",$_GET['path']);
    $entry_point = end($path);
} else {
    $entry_point = end($_GET);
}

if (end($path) == "") array_pop($path);

$serve_file = false;
if ($entry_point!='') {
    $pi = pathinfo($entry_point);
    if (isset($pi['extension']) and $pi['extension'] != 'php') {
        $serve_file = true;
    }
}

$load = 'view.php';
$w = getenv('OB_LIB_DIR');
if (end($path) == "ajax") $path[0] = 'ajax'; # /ajax can be placed anywhere
elseif (end($path) == "getphoto") $path[0] = 'getphoto'; # /ajax can be placed anywhere
elseif (end($path) == "getattachment") $path[0] = 'getattachment'; # /ajax can be placed anywhere
elseif (end($path) == "geomtest") $path[0] = 'geomtest'; # /ajax can be placed anywhere

// No path, but there are some query parameters
// e.g. query api, ?login, ?database
if (!count($path) and count($_GET)) {
    if (isset($_GET['login'])) {
        require_once('local_vars.php.inc');
        if (defined('LOGINPAGE')) {
            if (LOGINPAGE == 'profile')
                $path[] = 'profile';
            elseif (LOGINPAGE == 'mainpage')
                $path[] = 'map';
            else
                $path[] = 'map';
        }
    // can be moved to deck
    //} elseif (isset($_GET['database'])) {
    //    $path[] = 'boat';
    } elseif (isset($_GET['registration'])) {
        $path[] = 'registration';
    } elseif (isset($_GET['query'])) {
        $path[] = 'query';
    } elseif (isset($_GET['weblogin'])) {
        require_once('local_vars.php.inc');
        require_once('weblogin.php');
        exit;
    } else {
        $path[] = 'map';
        $chunks = $_GET;
    }
}

if (count($path) and $path[0] != '') {
    switch ($path[0]) {
        case "query":
            // query api
            $serve_file = false;
            break;
        case "map":
            if ($serve_file) {
                $id = $path[1];
                array_shift($path);

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } 
            $chunks['map'] = 'on';
            break;
        case "ajax":
            $load = 'afuncs.php';
            break;
        case "upload":

            $load = 'upload.php';

            if (isset($path[1]) and $path['1'] == 'history') {
                $chunks['history'] = 'upload';
                $chunks['id'] = $path[2];
                $load = 'view.php';
            }
            break;
        case "boat":
            if ($serve_file) {
                $id = $path[1];
                array_shift($path);

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            }
            $load = 'deck.php';
            break;
        case "profile":
            if ($serve_file) {
                $id = $path[1];
                array_shift($path);

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } else {
                $pl = array_pop($path);
                $chunks['profile']='';
                if ($pl == 'owngeoms') {
                    $chunks['options']='owngeoms';
                } elseif ($pl == 'sharedgeoms') {
                    $chunks['options']='sharedgeoms';
                } elseif ($pl == 'saveduploads') {
                    $chunks['showiups']='';
                } elseif ($pl == 'sharedqueries') {
                    $chunks['showsharedqueries']='';
                } elseif ($pl == 'apikeys') {
                    $chunks['showapikeys']='';
                } elseif ($pl == 'uploads') {
                    $chunks['options']='showuploads';
                    $chunks['uid'] = $path[1];
                } elseif ($pl == 'user_specieslist') {
                    $chunks['user_specieslist'] = '';
                    $chunks['userid'] = $path[1];
                } elseif ($pl == 'messages') {
                    $chunks['messages']='messages';
                } else {
                    $chunks['profile'] = $pl;
                }
            }

            $load = 'deck.php';
            break;
        case ($entry_point=='pds.php' && preg_match('/v([0-9]+\.?[0-9]*)/', $path[0], $v) ? true : false):
            $chunks['api_version'] = $v[1];
            $load = "pds.php";
            $w = getenv('PROJECT_DIR');
            break;
        case "getphoto":
            $load = 'photos.php';
            $serve_file = 0;
            break;
        case "getattachment":
            $load = 'photos.php';
            $serve_file = 0;
            break;
        case "geomtest":
            $load = 'geomtest.php';
            break;
        case "data":
            
            $table = $path[1];
            $id = $path[2];
            if ($serve_file) {
                array_shift($path); //data
                array_shift($path); //projecttable

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } else {
                $chunks['data'] = 1;
                $chunks['table'] = $table;
                $chunks['qtable'] = $table;
                $chunks['id'] = $id;
                if (isset($path[3]) and $path[3]=='editrecord') {
                    $chunks['editrecord'] = $id;
                    $load = 'upload.php';
                } elseif (isset($path[3]) and $path[3]=='history') {
                    unset($chunks['data']);
                    if (isset($path[4]) and $path[4]=='validation') {
                        $chunks['history'] = 'validation';
                    } else
                        $chunks['history'] = 'data';
                } elseif (isset($path[3]) and $path[3]=='pdf') {
                    $load = 'afuncs.php';
                    $chunks['export_data_sheet'] = 'pdf';
                }
            }
            break;
        case "table":
           if ($serve_file) {
                array_shift($path);

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } else {
                $chunks['qtable'] = $path[1];
            }
            break;
        case "uplinf":
            $id = $path[1];
            if ($serve_file) {
                array_shift($path);

                if (preg_match('/[0-9]+/',$id)) {
                    array_shift($path);    
                }
                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } else {
                $chunks['history'] = 'upload';
                $chunks['id'] = $id;
            }
            break;
        case "Share":
            // Stored data in the biomaps/queries table
            if ($serve_file) {
                $id = $path[1];
                array_shift($path);    
                if (preg_match('/^[0-9]+@/',$id)) {
                    array_shift($path);    
                }
                if ($path[0] == 'map')
                    array_shift($path);    

                $file = getenv('PROJECT_DIR').implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
                exit;
            } 
            elseif (!isset($path[2])) {
                $chunks['loadmeta'] = $path[1];
            }
            elseif ($path[2] == 'meta')
                $chunks['loadmeta'] = $path[1];
            elseif ($path[2] == 'data')
                $chunks['loaddata'] = $path[1];
            elseif ($path[2] == 'map') {
                $chunks['Share'] = $path[1];
                $chunks['map'] = '1';
            }
            elseif ($path[2] == 'table') {
                $chunks['query'] = 1;
                $chunks['show'] = 1;
                $chunks['Share'] = $path[1];
                $chunks['id'] = $path[1];
                $_GET = $chunks;
                require(getenv('OB_LIB_DIR').'load_queries.php');
                $_POST['skip_processing']='no';
                $no_echo=true;
                require(getenv('OB_LIB_DIR').'wfs_processing.php');
                require(getenv('OB_LIB_DIR').'modules/results_asTable.php');
                exit;
            }
            break;
        case "DOI":
            $chunks['metadata'] = 1;
            break;
        case "10.18426":
            $chunks['loaddoi'] = "10.18426/".$path[1];
            break;
        case "iNaturalist":
            $load = "inaturalist.php";
            if (isset($path[1]) and $path[1] == 'request_authorization_code')
                $chunks['inaturalist'] = 'request_authorization_code';
            elseif (isset($path[1]) and $path[1] == 'auth')
                $chunks['inaturalist'] = 'auth';
            elseif (isset($path[1]) and $path[1] == 'code') {
                $chunks['inaturalist'] = 'code';
                $chunks['code'] = $path[2];
            } elseif (isset($path[1]) and $path[1] == 'upload') {
                $chunks['inaturalist'] = 'upload';
                $chunks['data_id'] = $path[2];
            } else
                $chunks['inaturalist'] = '';
            break;
        case "terms":
            $chunks['p'] = 'terms';     # footer
            break;
        case "privacy":
            $chunks['p'] = 'privacy';   # footer
            break;
        case "cookies":
            $chunks['p'] = 'cookies';
            break;
        case "technical":
            $chunks['p'] = 'technical';
            break;
        case "whatsnew":
            $chunks['p'] = 'whatsnew';
            break;
        case "usage":
            $chunks['p'] = 'usage';     # footer
            break;
        case "datausage":
            $chunks['p'] = 'datausage';
            break;
        case "style":
            require_once('local_vars.php.inc');
            $style_def = constant('STYLE');
            $style_name = $style_def['template'];
            if ($serve_file) {
                array_shift($path);
                $file = getenv('PROJECT_DIR')."styles/app/".$style_name.'/'.implode('/',$path);
                if (file_exists($file)) {
                    header('Content-Type: '.mime_content_type($file));
                    readfile($file);
                    exit;
                }
            } 
            break;
        default: 
            $load = 'view.php';
            break;
    }
}
// add private or module path routings to this router folder
$router_dir = getenv('PROJECT_DIR')."local/router";
if (file_exists($router_dir)) {
    foreach (new FilesystemIterator($router_dir) as $fileinfo) {
        if ($fileinfo->getExtension() === 'php') {
            include($fileinfo->getPathname());
        }
    }
}

$_GET = array_merge($_GET,$chunks);    

if (!$serve_file)
    require($w.$load);

?>
