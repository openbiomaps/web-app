<?php
/* very basic functions
 *
 *
 *
 * */

//xdebug_start_trace('/tmp/xdebug');

if(getenv("PROJECT_DIR") !== false) {
    #require_once(getenv('OB_LIB_DIR').'system_vars.php.inc');
    require_once('/etc/openbiomaps/system_vars.php.inc');
    if (!is_dir(getenv('PROJECT_DIR'))) {
        echo '{"status":"fail","data":"No such project."}';
        exit;
    }
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
} else {
    exit;
}

require_once('postgres_functions.php');
require_once('base_functions.php');
require_once('cache_functions.php');
require_once('access_functions.php');

//xdebug_stop_trace();

?>
