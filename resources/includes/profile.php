<?php
/* profile main page 
 * included in interface.php profile()
 *
 *
 *
 *
 *
 * */

#debug('load profile.php',__FILE__,__LINE__);
require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'auth.php');
require_once(getenv('OB_LIB_DIR').'prepare_auth.php');
require_once(getenv('OB_LIB_DIR').'prepare_session.php');
require_once(getenv('OB_LIB_DIR').'interface.php');
require_once(getenv('OB_LIB_DIR').'languages.php');
require_once(getenv('OB_LIB_DIR').'taxon.php');
require_once(getenv('OB_LIB_DIR').'data_export.php');

if (!defined('STYLE_PATH')) {
    $style_def = constant('STYLE');
    $style_name = $style_def['template'];
    define('STYLE_PATH',"/styles/app/".$style_name);
    $single_profile_page = 'off';
}

#require_once(getenv('OB_LIB_DIR').'languages-admin.php');

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    echo "Stranger database connection.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}
if (isset($_SESSION['switchprofile'])) unset($_SESSION['switchprofile']);

if(!isset($_SESSION['Tid'])) {
    // csak bejelentkezve és csak a saját profilunkat tudjuk szerkeszteni
    echo "Session expired.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}


$Me = new User($_SESSION['Tid']);
$M = new Messenger();
/*
 * amikor az interface.php profile() függvényből includáljuk az ua. mintha ha GET['profile'] módban hívtuk volna
 * isset($inc)...
 * */
if (!isset($_GET['options'])) {
    $_GET['options']='profile';
}


/* read news 
 *
 * */
if (isset($_GET['options']) and $_GET['options']=='newsread') {
    
    echo $M->inbox();

/* Create new project
 *
 * */
} 
elseif(isset($_GET['options']) and $_GET['options']=='new_project_form') {
    $typo = "<option value='numeric'>".str_decimal_number."</option>
             <option value='integer'>".str_integer."</option>
             <option value='text'>".str_text."</option>
             <option value='date'>".str_date."</option>
             <option value='timestamp without time zone'>datetime</option>";

?>

<div id='newpr'>
<h2><?php echo str_new_project; ?> </h2>
<div class='infotitle'><?php echo wikilink('new_project.html',t('str_documentation')) ?></div>
<br>
<?php 
// Language vars
echo "<script>";
echo "\nvar str_recommended='".t(str_recommended)."';\n";
echo "\nvar str_obligatory='".t(str_obligatory)."';\n";
echo "</script>";
?>
<form method="post" enctype="multipart/form-data" id='new_project' class='pure-form'>
<table style='border-bottom:1px dotted gray;padding-bottom:1em;margin-bottom:1em;' class='create_project'>
<tr><td><?php echo str_nameofproject ?></td><td><input id='project_dir' class='pure-u-1-2' name='project_dir' placeholder='<?php echo  t(str_obligatory).': '.t(str_newform_help_1) ?>' required='required' pattern='[a-z_0-9]{2,16}'></td></tr>
<tr><td><?php echo str_proj_sdesc ?></td><td><input id='project_sdesc' class='pure-u-1-2' name='project_sdesc' class="pure-input-1" placeholder='<?php echo  t(str_obligatory).': '.t(str_newform_help_2) ?>' required></td></tr>
<tr><td><?php echo str_proj_desc ?></td><td><textarea name='project_desc' style='width:500px;height:120px' placeholder='<?php echo t(str_obligatory).': '.t(str_newform_help_3) ?>' required></textarea></td></tr>
<tr><td><?php echo str_proj_comment ?></td><td><textarea name='project_comment' style='width:500px;height:120px' placeholder="<?php echo str_newform_help_4 ?>"></textarea></td></tr>
<tr><td><?php echo str_map_access ?></td><td><select name='project_acc'>
    <option value='public'><?php echo str_everyone ?></option>
    <option value='login'><?php echo str_members ?></option>
    <option value='group'><?php echo str_group ?></option></select>
</td></tr>
<tr><td><?php echo str_data_mod ?></td><td><select name='project_mod'>
    <option value='public'><?php echo t(str_everyone) ?></option>
    <option value='login'><?php echo t(str_members) ?></option>
    <option value='group'><?php echo t(str_group) ?></option></select>
</td></tr>
<tr><td><?php echo str_default_lang ?></td><td><select name='project_lang'>
    <option value='hu'><?php echo t(str_hungarian) ?></option>
    <option value='en'><?php echo t(str_english) ?></option>
    <option value='de'><?php echo t(str_deutsch) ?></option>
    <option value='ru'><?php echo t(str_russian) ?></option>
    <option value='fr'><?php echo t(str_french) ?></option>
    <option value='du'><?php echo t(str_dutch) ?></option>
    <option value='it'><?php echo t(str_italian) ?></option>
    <option value='ro'><?php echo t(str_romanian) ?></option>
</td></tr>

<tr><td>PostgreSQL <?php echo str_user ?></td><td><input id='pgsql_admin' name='pgsql_admin' readonly> <span style='color:red'><?php echo str_saveit ?>!</span></td></tr>
<tr><td>PostgreSQL <?php echo str_password ?></td><td><input id='pgsql_passwd' name='pgsql_passwd' readonly> <span style='color:red'><?php echo str_saveit ?>!</span></td></tr>

<tr><td colspan=2>&nbsp;</td></tr>
<tr><td><?php echo str_center_x ?></td><td><input name='map_c_x' value='19.22221'></td></tr>
<tr><td><?php echo str_center_y ?></td><td><input name='map_c_y' value='47.30086'></td></tr>
<tr><td>EPSG srid</td><td><input name='map_c_srid' value='4326'></td></tr>
<?php
# 
# Should be replaced to use profileItem!
# echo $modules->_include('move_project','upload_project_file_field');
?>
</table>
<button type='button' id='send_newproj_req' class='button-success button-xlarge pure-button pure-u-1-3'><i class='fa fa-cogs'></i> <?php echo str_create ?></button>
<input type='hidden' name='mehet' value=1>
</form>
<br>
<br>
</div> <!--newpr-->
<div id='new_proj_messages'></div>
<?php
} 
elseif(isset($_GET['options']) and $_GET['options']=='invitations') {
    if (defined('INVITATIONS')) $inv = constant('INVITATIONS');
    else $inv = 10;

    //zero invitation project allow admins to send invitations
    if ($inv==0 and has_access('master'))
        $inv = 20;


    echo "<h2>".t(str_invites)."</h2>";

    echo "<div class='infotitle'>".wikilink('invitations.html',t('str_documentation'))."</div>";

    echo "<h3>".t(str_invitationsend)."</h3>";
    /********************
    * Send invitation form
    * ******************/
    //1 nap után akkor is tud mehívót küldeni, ha még vannak aktív meghívói
    $cmd = "SELECT count(id) as c FROM invites WHERE user_id='{$_SESSION['Tid']}' AND created + interval '1 days' > now()";
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        if ($r['c']<$inv) {

            $button = sprintf("<button id='soi' class='button-success button-large pure-button pure-u-1-4'><i class='fa fa-envelope-o'></i> %s (%d/<span id='invleft'>%d</span>)</button>",str_invitationsend,$inv,$inv-$r['c']);
            $language_selector = language_selector_widget(['id' => 'invlang']);
            printf('<div class="pure-u-2-3">
    <form class="pure-form pure-form-stacked"><fieldset>
    <label for="invlang">%s</label> %s <br>
    <label for="name">%s</label>
    <input type="text" name="name" id="name" value="" class="pure-u-1-2"><br>
    <label for="email">%s</label>
    <input type="text" name="email" id="email" value="" class="pure-u-1-2"><br>
    <label for="invcomment">%s</label>
    <textarea name="invcomment" id="invcomment" class="pure-u-1-2"></textarea><br>
    </fieldset></form>%s<br><br><hr>
    </div>',str_invitation_language, $language_selector, str_invited_name, str_invited_email,str_personal_message,$button);
        } else {
            echo "Invitation left: 0";
        }
    }


    // echo active invites
    $cmd = "SELECT user_id,id,name,created,CASE WHEN created + interval '60 days' > now() THEN '0' ELSE '1' END as v,project_table,mail FROM invites WHERE user_id=".$_SESSION['Tid']." OR project_table='".PROJECTTABLE."' ORDER BY name";
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        echo "<br><br><br><br>
            <h3>".t(str_pending_invites)."</h3>
            <table class='resultstable'><tr><th></th><th>".str_who."?</th><th>".str_when."?</th><th>".str_database."</th><th>".str_active."</th></tr>";
        while ($row = pg_fetch_assoc($res)) {
            if ($row['v']=='nem') $c = 'checked';
            else $c = '';

            $delinv = "<input type='checkbox' class='delinv' name='delinv[]' $c value='{$row['id']}'>";
            $bcolor = "";
            if ($row['user_id'] != $_SESSION['Tid']) {
                $delinv = "";
                $bcolor = "style='background-color:#b0b0b0'";
            }
            if ($row['v']) {
                $e = str_expired;
                $bcolor = "style='background-color:#baa070'";
            }
            else $e = str_yes;


            printf("<tr><td $bcolor>$delinv</td><td $bcolor><abbr title='%s'>%s</abbr></td><td $bcolor>%s</td><td $bcolor>%s</td><td $bcolor>%s</td></tr>",$row['mail'],$row['name'],$row['created'],$row['project_table'],$e);
        }
        echo "</table><br><button type='button' id='dropInv' class='button-warning pure-button'><i class='fa fa-trash'></i> ".str_dropselinv."</button>";
    }

} 
elseif(isset($_GET['options']) and $_GET['options']=='profile') {

    /********************
    * User profile Page
    * ******************/
    
    // get_own_profile = 1   -   topnav tab click, header menu settings link click: Load profile page with admin menu
    // get_own_profile = 2   -   interface.php load profile without admin menu tab
    // get_own_profile = 0   -   interface.php load foreign profile
    if (isset($_GET['get_own_profile'])) {
        // tab click || prepare_vars
        $get_own_profile = $_GET['get_own_profile'];
    }
    
    if (!isset($get_own_profile)) {
        $get_own_profile = 0;
    }
    
    if (isset($_GET['userid']) and isset($_SESSION['Tcrypt'])) {
        $userid = $_SESSION['Tcrypt'];
        $get_own_profile = 1;
    } elseif (isset($_GET['userid']) and !isset($_SESSION['Tcrypt'])) {
        echo "<center><h2>Error 404</h2>user not found</center>";
        return;
    }
    
    // new user privacy checkboxes
    if (isset($_SESSION['register_upw'])) {
        $userid = $_SESSION['Tcrypt'];
        $get_own_profile = 1;
    }

    // last security check
    if ((!isset($userid) or $userid=='' or $userid===0 or $userid===1) and isset($_SESSION['Tcrypt'])) 
        $userid = $_SESSION['Tcrypt'];

    # icons
    if ($get_own_profile) {
        $eicon = "<i class='fa fa-fw fa-pencil'></i>";
        #$excl = "<i class='fa fa-exclamation fa-lg text-important'></i>";
    }
    else {
        $eicon = "";
        #$excl = "";
    }
    $lockicon = "<i class='fa fa-fw fa-lock'></i>";

    $userobj = json_decode(get_profile_data(PROJECTTABLE,$userid),true);
    $user_roleid = $_SESSION['Trole_id'];
    $user_numid = $_SESSION['Tid'];
    $userdata = new userdata($userid,'hash');

    if (!count($userobj)) {
        echo "<center><h2>Error 404</h2>user not found</center>";
        return;
    }

    // create readable links instead of long get urls
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $baseurl = sprintf($protocol.'://'.URL.'/');

    /* 
     * ORCID get/set name */
    $orcid = $userobj['orcid'];
    $orcid_gn = '';
    $orcid_fn = '';
    $orcid_dept = '';
    $orcid_inst = '';
    $orcid_address = '';
    $orcid_country = '';
    if ($orcid!='') {
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pub.orcid.org/v1.2_rc7/$orcid/orcid-profile",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Accept: application/orcid+xml'),
            CURLOPT_POST => false,
            )
        );
        $result = curl_exec($curl);
        if ($result) {
            if (false === $result) {
                $n='';
                foreach(libxml_get_errors() as $error) {
                    $n.= "\n".$error->message;
                }
                log_action("orcid fetch failed: ".$n,__FILE__,__LINE__);
            }
            $xml = simplexml_load_string($result);
            if (isset($xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'given-names'})) $orcid_gn = $xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'given-names'};
            if (isset($xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'family-name'})) $orcid_fn = $xml->{'orcid-profile'}->{'orcid-bio'}->{'personal-details'}->{'family-name'};
            if (isset($xml->{'orcid-profile'}->{'orcid-activities'}->{'affiliations'}->{'affiliation'})) {
                foreach($xml->{'orcid-profile'}->{'orcid-activities'}->{'affiliations'}->{'affiliation'} as $afl) {
                    if ($afl->{'type'}=='employment') {
                        $orcid_dept = $afl->{'department-name'};
                        $orcid_inst = $afl->{'organization'}->{'name'};
                        $orcid_address = $afl->{'organization'}->{'address'}->{'city'};
                        $orcid_country = $afl->{'organization'}->{'address'}->{'country'};
                    }
                }
            }
        }
    }
    $aa=$wa=$na=0;
    if (isset($_GET['sopd'])){
        if($orcid_gn!='' and $orcid_fn!='') {
            $userobj['username'] =  "$orcid_gn $orcid_fn";
            $userobj['givenname'] =  "$orcid_gn";
            $userobj['familyname'] =  "$orcid_fn";
            $na = 1;
        }
        if($orcid_dept!='' or $orcid_inst!='') {
            $userobj['institute'] =  "$orcid_inst $orcid_dept";
            $wa = 1;
        }
        if($orcid_address!='') {
            $userobj['address'] =  "$orcid_address $orcid_country";
            $aa = 1;
        }
    }
    
    if ($get_own_profile == 2) {
        $em = tabs($userid,$ol); // admin menu tabs
        $em .= "<div style='padding-left:15em;width:100%;margin-top:-19px' id='tab_basic'>";
    } else {
        $em = "<div style='padding-left:1em'>";
    }
    // Single profile page
    if ($_SESSION['Tcrypt']!=$userid or $single_profile_page=='on') {
        //$username = $userdata->get_username();
        $user_roleid = $userdata->get_roleid();
        $user_numid = $userdata->get_userid();
    }
    $avatar = "<img src='$protocol://".URL."/includes/avatar.php?size=50&value={$_SESSION['Tmail']}&bg=225,225,225' style='vertical-align:middle;border:1px dashed #dadada;border-radius:8px;margin-top:-16px;margin-bottom:-16px;margin-left:-10px;margin-right:20px'>";
    $em .= "<h2 style='position:relative'>$avatar". t(str_profile_page);
    
    # Someone else profile page with sign-in as button
    if (!$get_own_profile and has_access('master')) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}/' id='switchprofile' data-switchprofile='{$userobj['email']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-2x fa-user-secret'></i></a>";
        $em .= "<div id='login-box' style='font-size:smaller;display:none;position:absolute;background-color:#eaeaea;border:1px solid #868686;padding:20px;border-radius:5px;left:0;top:0;box-shadow:0px 0px 20px rgba(0, 0, 0, 0.75);z-index:1;min-width:25%'></div>";
    }
    $em .= "</h2>";

    if ($get_own_profile) {
        $unread = $M->get_total_unread_number();
        $em .= ($unread > 0) ? "<div id='profile-unread-notification'><a href='#tab_basic' data-url='includes/profile.php?options=newsread' class='profilelink'><i class='fa fa-2x fa-envelope-o'></i> ".sprintf(t(str_you_have_new_message),$unread)."</a></div>" : "";
    }
    
    $em .= "<div class='infotitle'>".wikilink('profile.html',t('str_documentation'))."</div>";

    // USER DATA
    $em .= "<label class='profile_table_label'><i class='fa fa-user fa-2x'></i> ".t(str_userdata)."</label>";
    $em .= "<div class='profile_table'>";
    if ($get_own_profile) {
        $gicon = $ficon = $eicon;
        $name_parts = preg_split('/ /',$userobj['username']);
        if ($userobj['familyname']=='') {
            $userobj['familyname'] = $name_parts[0];
            $ficon = "<i class='fa fa-fw fa-question'></i>";
        }
        if ($userobj['givenname']=='' and isset($name_parts[1])) {
            $userobj['givenname'] = $name_parts[1];
            $gicon = "<i class='fa fa-fw fa-question'></i>";
        }

        if ($userobj['givenname']=='') $userobj['givenname'] = '....................';
        if ($userobj['familyname']=='') $userobj['familyname'] = '....................';
        
        $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </div><div class='tbl-cell'>$gicon %s</div></div>",
            '%',
            t(str_given_name),
            edit($userobj['givenname'],'given_name','givenname', $get_own_profile, 'edbox_send_profile',$na));
        $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </div><div class='tbl-cell'>$ficon %s</div></div>",
            '%',
            t(str_family_name),
            edit($userobj['familyname'],'family_name','familyname',$get_own_profile,'edbox_send_profile',$na));
        // Notice for New User - at first login:
        if (isset($_SESSION['register_upw'])) {
            $stars = sprintf("<span id='span-password' class='edbox-span blink' style='border-radius:2px;white-space:nowrap;padding:6px;margin:4px;font-size:1.8em;background-color:orange'>%s!</span><input name='password' class='edbox' data-table='checkitout' id='dc-password' value=''><button class='button-success button-small pure-button new_profile_password' id='ed-password'><i class='fa fa-floppy-o fa-lg'></i></button>",
                t(str_set_password));
        } else {
            if ($userobj['openid_provider']!='' and $userobj['inviter']=='')
                $stars = $lockicon;
            else 
                $stars = $eicon." ".edit('**********','password','password',$get_own_profile,'edbox_send_profile');
        }
        $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </div><div class='tbl-cell'>%s</div></div>",
            '%',t(str_password),$stars);
    } else {
        if (!$get_own_profile) $em .= sprintf("<div class='tbl-row'><div class='tbl-cell'><div style='font-weight:bold;white-space:nowrap;padding:10px 0 5px 0;font-size:1.1em'>%s</div></div></div>",$userobj['username']);
    }
    if ($userobj['email']=='' and $get_own_profile) $userobj['email'] = '....................';
    if ($userobj['institute']=='' and $get_own_profile) $userobj['institute'] = '....................';
    if ($userobj['address']=='' and $get_own_profile) $userobj['address'] = '....................';
    if ($userobj['references']=='' and $get_own_profile) $userobj['references'] = '....................';
    if ($userobj['orcid']=='' and $get_own_profile) $userobj['orcid'] = '....................';

    if ($get_own_profile) {
        if (!isset($_SESSION['change_email_response']) or $_SESSION['change_email_response']=='') $cer = str_conf_necessary;
        else {
            $cer = $_SESSION['change_email_response'];
            unset($_SESSION['change_email_response']);
            if ($cer==1) $cer = str_success;
        }
        $put_cer = "(<span id='cer'>$cer</span>)";
    } else 
        $put_cer = "";
    
    if (!$get_own_profile and $userobj['visible_mail']==0) {
        $userobj['email'] = "";
    }

    if ($userobj['openid_provider']!='' and $userobj['inviter']=='')
        $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </div><div class='tbl-cell'>$lockicon <span style='font-family:monospace'>%s</span></div></div>",
            '%',
            t(str_email),
            $userobj['email']);
    else
        $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s<sup>*</sup>: </div><div class='tbl-cell'>$eicon %s $put_cer </div></div>",
            '%',
            t(str_email),
            edit($userobj['email'],'email','email',$get_own_profile,'edbox_send_profile'));

    $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </div><div class='tbl-cell'>$eicon %s</div></div>",
        '%',
        t(str_pworkpl),
        edit($userobj['institute'],'institute','institute',$get_own_profile,'edbox_send_profile',$wa));
    $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </div><div class='tbl-cell'>$eicon %s</div></div>",
        '%',
        t(str_paddress),
        edit($userobj['address'],'address','address',$get_own_profile,'edbox_send_profile',$aa));
    $em .= sprintf("<div class='tbl-row'><div class='tbl-cell' style='width:1%s;padding-right:10px;white-space:nowrap'>%s: </div><div class='tbl-cell'>$eicon %s</div></div>",
        '%',
        t(str_references),
        edit($userobj['references'],'references','references',$get_own_profile,'edbox_send_profile'));
    $em .= "</div><br><br>";

    // Settings
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-sliders fa-lg'></i> ".t('str_settings')."</label>";
        $em .= "<table class='profile_table'>";

        //messaging setting
        $em .= "<tr><td colspan=2 class='settings-section'>".t('str_message_settings')."</td></tr>";
        $em .= "<tr><td colspan=2 class='settings-section-description'>".t('str_message_settings_description')."</td></tr>";
        $profile_settings = ['receive_system_message_mails', 'receive_personal_message_mails', 'receive_comment_notification_mails', 'visible_mail'];
        foreach ($profile_settings as $ps) {
            $btn_state = ($Me->options->$ps) ? "on" : "off";
            $btn_class = ($Me->options->$ps) ? "success" : "passive";
            $em .= sprintf("<tr><td>%s </td><td>%s</td></tr>", t("str_$ps"),"<button class='pure-button button-$btn_class button-small profile-settings-toggle' type='button' id='$ps'><i class='fa fa-lg fa-toggle-$btn_state'></i></button>");
        }
        // preferred language setting
        $em .= "<tr><td colspan=2 class='settings-section'>". t('str_language_settings') . "</td></tr>";
        $args = [
            'selected' => $Me->options->language ?? "",
            'class' => 'preferred-language-selector',
        ];
        $em .= "<tr><td>".t('str_select_your_preferred_language')."</td><td>".language_selector_widget($args)."</td></tr>";
        
        // preferred species column setting
        $em .= "<tr><td colspan=2 class='settings-section'>". t('str_taxon_settings') . "</td></tr>";
        $args = $Me->options->taxon_lang ?? "";
        $x = TaxonManager::taxon_lang_selector_widget($args);
        if ($x != '')
            $em .= "<tr><td>".t('str_select_species_name_type')."</td><td>".$x."</td></tr>";
            
        // preferred taxon order
        # Module hook place -> profileSettingsTaxonItem
        foreach ($x_modules->which_has_method('profileSettingsTaxonItem') as $m) {
            #$args = $Me->options->taxon_order ?? "";
            $args = $Me->options;
            if ($ps = $x_modules->_include($m,'profileSettingsTaxonItem',[$args])) {
                $em .= "<tr><td>". t($ps['label']) . "</td><td>". $ps['item'] ."</td></tr>";
            } 
        }

        # Module hook place -> profileSettingsItem
        foreach ($x_modules->which_has_method('profileSettingsItem') as $m) {
            $args = $Me->options;
            if ($ps = $x_modules->_include($m,'profileSettingsItem',[$args])) {
                $em .= "<tr><td colspan=2 class='settings-section'>". t($ps['label']) . "</td></tr>";
                $em .= $ps['item'];
            }
        }
        
        // drop profile
        $em .= sprintf("<tr><td colspan=2 class='settings-section'><a href='' id='drop_my_account'>%s</a></td></tr>",t('str_drop_profile'));
        $em .= "</table><br><br>";
    }

    // ORCID

    //$em .= "<label class='profile_table_label'><img src='$protocol://".URL."/css/img/icon-orcid.png' style='width:16px;margin-bottom:-3px'> ".t('str_orcid_profile')."</label>";
    $em .= "<label class='profile_table_label'><i class='ai ai-orcid ai-lg'></i> ".t('str_orcid_profile')."</label>";
    $em .= "<table class='profile_table'>";
    if ($get_own_profile) 
        $em .= sprintf("<tr><td>ORCID id: </td><td>$eicon %s<br><a href='%sindex.php?profile&sopd' id='orcid_profile_update' class='button-href pure-button'>%s</a></td></tr>",edit($userobj['orcid'],'orcid','orcid',$get_own_profile,'edbox_send_profile'),$baseurl,t('str_load_orcid_profile'));
    else {
        $em .= sprintf('<tr><td>ORCID id: </td><td>%1$s</td></tr>',$userobj['orcid']);
    }
    $em .= "</table><br><br>";

    // User information
    $em .= "<label class='profile_table_label'><i class='fa fa-home fa-lg'></i> ".t('str_userinfo')."</label>";
    $em .= "<table class='profile_table'>";
    $transl = array('0'=>str_parked,'1'=>str_user,'2'=>str_master,'3'=>str_operator,'4'=>str_assistant,'banned'=>str_parked,'normal'=>str_user,'master'=>str_master,'operator'=>str_operator,'assistant'=>str_assistant);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t(str_status),$transl[$userobj['user_status']]);
    $label = '';
    if (floatcmp($userobj['validation'],'0')) $label = 'warning';
    if (floatcmp($userobj['validation'],'-0.3')) $label = 'swarning';
    if (floatcmp($userobj['validation'],'-0.5')) $label = 'sswarning';
    $em .= sprintf("<tr><td>%s: </td><td>%.3f</td></tr>",t('str_valuation'),$userobj['validation']);
    
    // User information : groups
    $r = read_groups($user_numid);
    if (isset($r['roles']) and $r['roles']!='') {
        $login_group = '';
        $cmd = sprintf("SELECT description FROM project_roles WHERE role_id=ANY(ARRAY[%s]) AND user_id IS NULL",$r['roles']);
        $res = pg_query($BID,$cmd);
        $login_group = array();
        while ( $row = pg_fetch_assoc($res) ) {
            $login_group[] = $row['description'];
        }
        $login_groups = '['.implode($login_group,', ').']';
        $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t('str_groups'),$login_groups);

    }
    $em .= "</table><br><br>";

    // Other databases
    if ($userobj['status'] == '1') { 
        //some message about the extra rights??
    }
    $cmd = "SELECT p.project_table,domain FROM projects p LEFT JOIN project_users pu ON (pu.project_table=p.project_table) LEFT JOIN users u ON u.id=pu.user_id WHERE user_status::varchar IN ('2','master') AND \"user\"='$userid'";
    $rs = pg_query($BID,$cmd);
    if(pg_num_rows($rs)) {
        $em .= "<label class='profile_table_label'><i class='fa fa-database fa-lg'></i> ".t('str_other_databases')."</label>";
        $em .= "<table class='profile_table'>";
        $st = array();
        while($row = pg_fetch_assoc($rs)) {
            $st[] = sprintf('<a href="http://%s" target=_blank>%s</a>',$row['domain'],$row['project_table']);
        }
        $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t('str_project'),implode(', ',array_values($st)));
        $em .= "</table><br><br>";
    }

    // User activity
    $em .= "<label class='profile_table_label'><i class='fa fa-bar-chart fa-lg'></i> ".t('str_activity')."</label>";
    $em .= "<table class='profile_table'>";
    $em .= sprintf("<tr><td>%s: </td><td><a href='%sprofile/uploads/$userid/#tab_basic' data-url='/includes/profile.php?options=uploads&uid=$userid' class='profilelink'>%s</a></td></tr>",
        t('str_uploadcount'),$baseurl,$userobj['uplcount']);
    $em .= sprintf("<tr><td>%s: </td><td><a href='%sprofile/uploads/$userid/#tab_basic' data-url='/includes/profile.php?options=observationlists&uid=$userid' class='profilelink'>%s</a></td></tr>",
        t('str_uploadobservationlistcount'),$baseurl,$userobj['obslistcount']);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t('str_modcount'),$userobj['modcount']);
    $em .= sprintf("<tr><td>%s: </td><td>%s</td></tr>",t('str_rowcount'),$userobj['upld']);
    $em .= "</table>";

    // User activity : species list
    $em .= sprintf('<a href=\''.$baseurl.'profile/%1$s/user_specieslist/#tab_basic\' data-url=\'/includes/profile.php?options=user_specieslist&userid=%1$s\' class=\'profilelink\'>'.t('str_species_stat').'</a><br>',$userid);
    
    // User activity : area polygon
    $cmd = sprintf("SELECT ST_AsText(ST_ConvexHull(ST_Collect(obm_geometry))) as area, ST_Area(ST_ConvexHull(ST_Collect(obm_geometry))::geography)/1000000 AS size 
        FROM %s LEFT JOIN system.uploadings ON obm_uploading_id=id 
        WHERE uploader_id=%d",PROJECTTABLE,$user_roleid);
        #validity check of geoms
        #SELECT ST_AREA(the_geom), ST_ISVALID(the_geom), ST_ISVALIDREASON(the_geom), ST_SUMMARY(the_geom) FROM (SELECT ... ) as foo;

    $resa = pg_query($ID,$cmd);
    if (pg_num_rows($resa)) {
        $row = pg_fetch_assoc($resa);
        $em .= sprintf('<a href="%sgeomtest/?wkt=%s&name=%s&srid=4326" target="_blank">%s: %dkm<sup>2</sup></a>',$baseurl,$row['area'],$userobj['username'],t('str_activity_area'),round($row['size']));
    }

    $em .= "<br><br><br>";

    // Interrupted uploads
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-upload fa-lg'></i> ".t('str_intr_uploads')."</label>";
        $em .= "<table class='profile_table'>";
        $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/saveduploads/#tab_basic' data-url='/includes/profile.php?options=saveduploads' class='profilelink'>".t('str_intr_uploads')."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('profile.html#interrupted-imports',t('str_what_are_load_imports')));
        $em .= sprintf("</table><br><br>");
    }

    // Bookmarks
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-bookmark fa-lg'></i> ".t('str_bookmarks')."</label>";
        $em .= "<table class='profile_table'>";
        $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/bookmarks/#tab_basic' data-url='/includes/profile.php?options=bookmarks' class='profilelink'>".t('str_bookmarks')."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('profile.html#bookmarks',t('str_what_are_bookmarks')));
        $em .= sprintf("</table><br><br>");
    }

    // Shares
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-share-alt fa-lg'></i> ".t('str_shares')."</label>";
        $em .= "<table class='profile_table'>";
        $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/savedresults/#tab_basic' data-url='/includes/profile.php?options=savedresults' class='profilelink'>".t('str_shares')."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('profile.html#sharing-data',t('str_what_are_shares')));
        $em .= sprintf("</table><br><br>");
    }

    // Api keys
    if ($get_own_profile) {
        $em .= "<label class='profile_table_label'><i class='fa fa-key fa-lg'></i> ".t('str_apikeys')."</label>";
        $em .= "<table class='profile_table'>";
        $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='".$baseurl."profile/apikeys/#tab_basic' data-url='/includes/profile.php?options=apikeys' class='profilelink'>".t('str_apikeys')."</a>");
        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('profile.html#api-keys',t('str_what_are_apikeys')));
        $em .= sprintf("</table><br><br>");
    }

    require_once(getenv('OB_LIB_DIR').'repository.php');
    require_once(getenv('OB_LIB_DIR').'dataverse.php');

    $repo = new repository(0);
    //$repo->set(0);
    // Repozitorium
    if ($repo->repo_type) {

        // or you can render json data without creating tree
        #$em .= "const tree = JsonView.renderJSON(data, document.querySelector('.tree'));";
        
        $em .= "<label class='profile_table_label'><i class='fa fa-key fa-lg'></i> ".t('str_external_repozitory')."</label>";
        $em .= "<table class='profile_table'>";

        $repo_type = $repo->repo_type;
        $n = 0;
        while ($repo->repo_type!==false) {
            
            $em .= sprintf("<tr><td>[%s] %s</td></tr>",$repo->repo_type, $repo->server_url);
            if ($repo->repo_type == 'dataverse') {
                $dv = new dataverse($n);

                if ( $dv->project_dataverse ) {
                    $em .= sprintf('<tr><td><i class="ai ai-dataverse ai-2x"></i> Project\'s dataverse: <a href="%1$s/dataverse/%2$s" target="_blank">%2$s</a></td></tr>',
                        $repo->server_url,$dv->project_dataverse);
                    $em .= '<script type="text/javascript">';
                    $em .= 'afoo(function(data){json_viewer(data,"jv'.$n.'")},{repo_wget:1,D:'.$n.'})';
                    $em .= '</script>';
                } else
                    $em .= sprintf("<tr><td>No dataverse for this project there</td></tr>");
            } else {
                $em .= sprintf("<tr><td>Unsupported repository type</td></tr>");
            }

            $em .= sprintf("<tr><td><div class='json-view' id='jv$n'></div></td></tr>");

            $n++;
            $repo->set($n);
        }


        #if (has_access('master') and defined('REPO[1]_API_TOKEN'))
        #    $em .= sprintf("<tr><td>Api token: %s</td></tr>",constant('REPO[1]_API_TOKEN'));
        #else {
        #    $em .= sprintf("<tr><td>Api token is defined in system level</td></tr>");
        #}

        $em .= sprintf("<tr><td>%s</td></tr>",wikilink('profile.html#repozitorium',t('str_what_is_repozitorium')));
        $em .= sprintf("</table><br><br>");
    }

    // Module specific items
    if ($get_own_profile) {

        # Module hook place -> profileItem
        foreach ($x_modules->which_has_method('profileItem') as $m) {
            if ($ps = $x_modules->_include($m,'profileItem',[])) {

                $em .= "<label class='profile_table_label'><i class='fa {$ps['fa']} fa-lg'></i> ".t($ps['label'])."</label>";
                $em .= "<table class='profile_table'>";
                $em .= $ps['item'];
                $em .= sprintf("</table><br><br>");
            }
        }
        
        # This is the solution for custom submenu by custom_admin_pages
        # should be replaced in all modules to profileItem!!!
        /*foreach ($modules->which_has_method('profileSection') as $m) {
            if ($ps = $modules->_include($m,'profileSection')) {

                $em .= "<label class='profile_table_label'><i class='fa {$ps['fa']} fa-lg'></i> {$ps['label']}</label>";
                $em .= "<table class='profile_table'>";
                $em .= sprintf("<tr><td>%s</td></tr>","<i class='fa fa-external-link'></i> <a href='{$ps['href']}' data-url='{$ps['data_url']}' class='profilelink'>{$ps['label']}</a>");
                $em .= sprintf("<tr><td>%s</td></tr>",wikilink($ps['help']));
                $em .= sprintf("</table><br><br>");
            }
        }*/
    } 
    
    // Reputation
    $em .= "<label class='profile_table_label'><i class='fa fa-handshake-o fa-lg'></i> ".t('str_reputation')."</label>";

    $cmd = sprintf("SELECT SUM(valuation) AS score FROM public.evaluations WHERE row=%d AND \"table\"='users'",$userdata->get_userid());
    $sres = pg_query($BID,$cmd);
    $srow = pg_fetch_assoc($sres);
    $data_eval_score = $srow['score'];
    
    $em .= sprintf('<span style="font-size:2em;color:lightgrey;vertical-align:middle">%s</span>',$data_eval_score);

    #$eurl = sprintf($protocol.'://'.URL.'/data/%s/%d/history/validation/',PROJECTTABLE,$userdata->get_userid());

    
    #$cmd = "SELECT comments,user_name,to_char(datum, 'YYYY-MM-DD HH24:MI') as datum,valuation,user_id,id,related_id FROM evaluations WHERE \"table\"='users' AND row='$userid' AND user_id!='$userid' ORDER BY datum";
    /* No evaluation details to users... just sum of eval scores!
     *
     * $cmd = sprintf("SELECT row,comments,user_name,to_char(datum, 'YYYY-MM-DD HH24:MI') as datum,valuation,user_id,e.id,related_id 
        FROM evaluations AS e LEFT JOIN users ON (\"table\"='users' AND row=users.id AND user_id!=users.id) WHERE \"user\"=%s ORDER BY datum",quote($userid));
    
    $rs = pg_query($BID,$cmd);
    while ($row = pg_fetch_assoc($rs)) {
        // colorized value on left side 
        if ($row['valuation']>0) $val = '<span style="color:blue;font-weight:bold">+1</span>';
        elseif ($row['valuation']<0) $val = '<span style="color:orange;font-weight:bold">-1</span>';
        else $val = '&nbsp; &nbsp;';
        
        // opinion row
        $cmd = sprintf('SELECT "user" FROM users WHERE id=%d',$row['user_id']);
        $resu = pg_query($BID,$cmd);
        $rowu = pg_fetch_assoc($resu);

        $eurl = sprintf('http://'.URL.'/profile/%s/',$rowu['user']);

        $em .= '<div style="margin-top:20px;border-top:1px solid #bababa">'.$val.' '.$row['comments'];
        $em .= '<div style="text-align:right;font-size:70%;color:#bababa"><a href=\''.$eurl.'\'>'.$row['user_name'].'</a> '.$row['datum'].'</div></div>'; 
        
        // answer section to opinions
        if ($get_own_profile) {
            $cmd = "SELECT comments FROM evaluations WHERE related_id={$row['id']} and user_id={$row['id']}";
            $rsd = pg_query($BID,$cmd);
            if (!pg_num_rows($rsd)) {
                $em .= "<div style='padding-left:10px'>Is this useful for you?<br>";
                $em .= sprintf('<input type=\'hidden\' id=\'relid-%2$s\' value=\''.$row['id'].'\'>'.t('str_comment').':
                    <div id=\'yohelp-%2$s\' class=\'bubby pure-u-1-1\'>
                    <h3>'.t('str_thanks_for').'</h3>
                    <ul>
                        <li>You can post your answer with accept or refuse voting. </li>
                    </ul>
                    Avoid …
                    <ul>
                        <li>The negative voting without explanation.</li>
                        <li>The inconsistency between your comment and the voting.</li>
                    </ul></div>
                    <textarea class=\'expandTextarea yo\' id=\'evdt-%2$s\'></textarea><br>
                    <input type=\'button\' class=\'button-success pure-button pure-u-1-2 pm\' value=\'accept\' id=\'valp-%2$s\' rel=\'user\'><input type=\'button\' class=\'button-error pure-button pure-u-1-2 pm\' value=\'refuse\' id=\'valm-%2$s\' rel=\'user\'></div><br>',t('str_valuation'),$row['id']);
            } else {
                $rc = pg_fetch_assoc($rsd);
                $em .= '<div style=\'border-top:1px dotted #bababa;padding-left:25px;font-size:90%;color:#bababa\'>'.$rc['comments'].'</div>';
            }
        }
    }*/

    // your opinion box
    // It is disabled, should be automatically ranked according to their work...
    /*if (!$get_own_profile and has_access('master')) {
            $em .= sprintf('<br><br><h2>Opinioning</h2>
    <div class=\'opinioning\'><div class=\'bubby pure-u-1-1\'><h3>'.t('str_thanks_for').'</h3>'.t('str_opinion_post_1').'<br><br><span style=\'font-variant:small-caps\'>'.t('str_avoid').'…</span><br>'.t('str_opinion_post_2').'</div>
    <textarea id=\'evdt-%2$s\' class=\'yo pure-u-1-1\' style=\'border:1px solid #bababa;height:120px;\'></textarea></div>
    <br>
    <div class=\'yoband\'>
                <button class=\'button-error pure-button pure-u-1-4 pm\' value=\'-\' id=\'valm-%2$s\' rel=\'user\'><i class=\'fa-hand-o-down fa fa-lg\'></i></button> 
                <button class=\'button-secondary pure-button pure-u-1-4 pm\' value=\'ok\' id=\'valz-%2$s\' rel=\'user\'>ok</button>
                <button class=\'button-success pure-button pure-u-1-4 pm\' value=\'+\' id=\'valp-%2$s\' rel=\'user\'><i class=\'fa-hand-o-up fa fa-lg\'></i></button></div>','',$userid);
    }*/
    // opnion div
    //$em .= '</div>';

    if ($_SESSION['Tcrypt']!=$userid or $single_profile_page=='on') {
        # ...
    }
    // End of tab_basic
    $em .= "</div>";
    // print out 
    echo $em;


} elseif(isset($_GET['options']) and $_GET['options']=='observationlists') {
    ShowObservationLists($_GET['uid']); 

} elseif(isset($_GET['options']) and $_GET['options']=='uploads') {
    ShowUploads($_GET['uid']); 

} elseif(isset($_GET['options']) and $_GET['options']=='saveduploads') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowIUPS($id,1);

} elseif(isset($_GET['options']) and $_GET['options']=='savedresults') {
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowShares();

} elseif(isset($_GET['options']) and $_GET['options']=='bookmarks') {
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowBookmarks();

} elseif(isset($_GET['options']) and $_GET['options']=='apikeys') {
    $id = 0;
    if(isset($_SESSION['Tid']) ) $id = $_SESSION['Tid'];
    ShowAPIKeys($id,1);

} elseif(isset($_GET['options']) and $_GET['options']=='user_specieslist') {
    echo usertaxonlist($_GET['userid']);

} elseif(isset($_GET['options']) and $_GET['options']=='module_function') {

    $x_modules->_include($_GET['m'],'profilePage',explode(',',getval($_GET['params'])));

} elseif(isset($_GET['options']) and $_GET['options']=='module_adminpage') {

    echo $x_modules->_include($_GET['m'],'adminPage',explode(',',getval($_GET['params'])));

} elseif(isset($_GET['options']) and $_GET['options']=='administration') {

    echo "<h2>".t(str_project_administration)."</h2>";
    $admin_pages_elements = get_admin_pages_list('project_admin');
    ksort($admin_pages_elements,SORT_NATURAL|SORT_FLAG_CASE);

    $pa = '';
    $icons = ["dbcols"=>"database","mapserv"=>"map","upload_forms"=>"upload","query_def"=>"postgresql", "languages"=>"language", "jobs"=>"cogs", "groups"=>"group", "members"=>"user", "files"=>"folder-open-o","functions"=>"wrench","access"=>"key-modern","modules"=>"plug","server_logs"=>"history","serverinfo"=>"server","taxon"=>"tags","privileges"=>"id-badge","imports"=>"hand-pointer-o","description"=>"file-text-o"];
    foreach ($admin_pages_elements as $key=>$value) {
        $icon = isset($icons[$value]) ? "fa-".$icons[$value] : "";
        if (has_access($value)) {
            $pa.= "<div style=' border: 2px solid #e7e7e7;
                                border-radius: 4px;
                                padding: 1.5rem;
                                background-color:#fafafa'><a href='#tab_basic' class='profilelink' data-url='includes/project_admin.php?options=$value'><i class='fa ".$icon."'></i> ".mb_ereg_replace(' ','&nbsp;',t($key))."</a></div>";
        } else {
            // show inactive options??
        }
    }
    echo '<div class="wrapper" style="
  /**
   * User input values.
   */
  --grid-layout-gap: 10px;
  --grid-column-count: 5;
  --grid-item--min-width: 100px;
  /**
   * Calculated values.
   */
  --gap-count: calc(var(--grid-column-count) - 1);
  --total-gap-width: calc(var(--gap-count) * var(--grid-layout-gap));
  --grid-item--max-width: calc((100% - var(--total-gap-width)) / var(--grid-column-count));

  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(max(var(--grid-item--min-width), var(--grid-item--max-width)), 1fr));
  grid-gap: var(--grid-layout-gap);
  margin-right:1em;">'.$pa.'</div>';
}



# bug report
# This div including in all tabs element reload which is not a nice thing...
#       would be better to place it somewhere else
if (isset($_SESSION['Tid']) and defined("AUTO_BUGREPORT_ADDRESS")) {
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    echo "<div id='bugreport' title='".t('str_report_an_error')."' style='background:url($protocol://".URL."/images/Diaperis_boleti2.png);'></div>";
    //$included_files = json_encode(get_included_files());
    //$defined_vars = json_encode(get_defined_vars());

    //$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    //$iv = openssl_random_pseudo_bytes($ivlen);

    //if (!isset($_SESSION['openssl_ivs'])) $_SESSION['openssl_ivs'] = array();
    //$_SESSION['openssl_ivs']['eincf'] = base64_encode($iv);

    //$encrypted_included_files = base64_encode(openssl_encrypt($included_files, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
    //$iv = openssl_random_pseudo_bytes($ivlen);
    //$_SESSION['openssl_ivs']['edefv'] = base64_encode($iv);
    //$encrypted_defined_vars = base64_encode(openssl_encrypt($defined_vars, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));

        //<input type='hidden' id='eincf' value='$encrypted_included_files'>
        //<input type='hidden' id='edefv' value='$encrypted_defined_vars'>

    echo "<div id='bugreport-box'>
    <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:0px'>".t('str_report_an_error')."</h2>
    <div style='padding:5px;text-align:center'>
    <button id='bugreport-cancel' style='position:absolute;top:0;right:0' class='pure-button button-large button-passive'><i class='fa fa-close'></i></button>
    <form class='pure-form pure-u-1'>
        <input type='hidden' id='page' value='profile.php'>

        <fieldset class='pure-group'>
            <input type='text' id='issue-title' maxlength='32' class='pure-u-1' placeholder='".t('str_subject')."'>
            <textarea id='issue-body' style='min-height:10em' class='pure-u-1' placeholder='".t('str_bug_description')."'></textarea>
        </fieldset>
        <button id='bugreport-submit' class='pure-button button-large button-warning pure-u-2-3'>".t('str_send')." <i class='fa fa-lg fa-bug'></i></button>
        
    </form>
    </div>
</div>";
}

?>
