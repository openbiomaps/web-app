<?php
//include local_vars
if (getenv("PROJECT_DIR") !== false) {
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
}
// some improvements needed

// 0. User selected language
// 1. Browser language - not used
// 2. Project language
// 3. GET language
// 4. SESSION language

if (!isset($_SESSION['LANG'])) {
  //Default language is the first language of the LANGUAGES array
  $_SESSION['LANG'] = select_language();
  
}

if (defined("OB_PROJECT_DOMAIN"))
    $domain = constant("OB_PROJECT_DOMAIN");
elseif (defined("OB_DOMAIN"))
    $domain = constant("OB_DOMAIN");
else
    $domain = $_SERVER["SERVER_NAME"];


//$cmd = sprintf('SELECT * FROM translations WHERE lang = %2$s AND (project = \'%1$s\' OR (scope = \'global\' AND const NOT IN (SELECT const FROM translations WHERE project = \'%1$s\')));', PROJECTTABLE, quote($_SESSION['LANG']));
$cmd = sprintf('SELECT t2.lang,t1.const,t1.translation AS translation_en,t2.translation
FROM translations t1  LEFT JOIN translations t2 ON (t1.project IS NOT DISTINCT FROM t2.project AND t1.const=t2.const AND t2.lang=%2$s) 
WHERE t1.lang IN (\'en\') AND (t1.project = \'%1$s\' OR (t1.scope = \'global\' AND t1.const NOT IN (SELECT const FROM translations WHERE project = \'%1$s\'))) 
ORDER BY t1.const',PROJECTTABLE, quote($_SESSION['LANG']));

$res = pg_query($BID, $cmd);
while ($row = pg_fetch_assoc($res)) {
    $trans = ($row['lang']=='') ? $row['translation_en'] : $row['translation'];

    if ($row['translation'] == '__not_translated__')
        $trans = $row['translation_en'];

    if ($row['translation'] == '')
        $trans = $row['translation_en'];

    $translation = preg_replace('/%PROJECTTABLE%/',PROJECTTABLE,$trans);

    $translation = preg_replace('/%DOMAIN%/',$domain,$translation);


    if (! defined( $row['const'] )) 
        define($row['const'],$translation);
}

?>
