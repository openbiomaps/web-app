<?php

//echo "iNaturalist interface<br>";



require_once(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require(getenv('OB_LIB_DIR').'prepare_vars.php');
require(getenv('OB_LIB_DIR').'languages.php');

$site = "https://www.inaturalist.org";
$app_id = 'efa8929c9a6c13705ddac9fa494a72b712d6fdd62adbb17b364d27f84fccaddd';
$app_secret = '89edeee851165c200a3ec8d152e3b65ce60fc7cce955d2b6c5e2454128c6f1b3';
$redirect_uri = "https://openbiomaps.org/inaturalist/callback.php?site=http://".URL; # you can set this to some URL you control for testing



if (isset($_GET['inaturalist']) and $_GET['inaturalist'] == 'code') {
    /* This is the iNaturalist authentication 
     *
     * */
    $response = auth_with_authorization_code($app_id, $app_secret, $_GET['code'], $redirect_uri, $site);
    if (isset($response['access_token'])) {
        echo "<h1>Ok.</h1>";
        $_SESSION['iNaturalist'] = $response;
        var_dump($response);
        # response will be a chunk of JSON looking like
        # {
        #   "access_token":"xxx",
        #   "token_type":"bearer",
        #   "expires_in":null,
        #   "refresh_token":null,
        #   "scope":"write"
        # }
    } else {
        echo "<h1>Error</h1>";
        var_dump($response);
        
    }
    // JSON validation check?
    exit;
}

if (isset($_GET['inaturalist']) and $_GET['inaturalist']=='request_authorization_code') {

    /* This returning to the redirect uri which will redirect to this site
     *
     * */
    requests_authorization_code($site, $app_id, $redirect_uri);
    exit;
}

if (isset($_GET['inaturalist']) and $_GET['inaturalist']=='upload') {

    //$url = "https://openbiomaps.org/projects/dead_animals/";
    $url = "http://localhost/biomaps/resources/";

    $result = fetch_obm_record($url, $_GET['data_id'] );

    $st_col = st_col('default_table','array');
    if ($result['status'] == 'success') {
        if ( $result['data'][0][$st_col['DATE_C'][0]] != '') {
            $obs_time = $result['data'][0][$st_col['DATE_C'][0]];
        }
        if ( $result['data'][0][$st_col['SPECIES_C_SCI']] != '') {
            $obs_species = $result['data'][0][$st_col['SPECIES_C_SCI']];
        } 
        if ( $result['data'][0][$st_col['GEOM_C']] != '') {
            $obs_location = $result['data'][0][$st_col['GEOM_C']];
        } 
        if ( $result['data'][0]['obm_files_id'] != '') {
            $attachments = $result['data'][0]['obm_files_id'];
            // query files table, get files
            $cmd = sprintf("SELECT reference,id FROM system.file_connect LEFT JOIN system.files ON (files.id=file_connect.file_id) WHERE conid=%s ORDER BY files.id",quote($attachments),PROJECTTABLE);
            $res = pg_query($ID,$cmd);
            $files = array();
            while($row = pg_fetch_assoc($res)) {
                $files[] = $row['reference'];
            }
            
            $obs_photos = implode('|',$files);
        } 
    }
    $cmd = "SELECT st_x(st_GeomFromText('$obs_location')) as lon, st_y(st_GeomFromText('$obs_location')) as lat";
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        $observation = array(
        "timezone" => "",
        "location" => $obs_location,
        "latitude" => $row['lat'],
        "longitude" => $row['lon'],
        "time" => $obs_time,
        "taxon_name" => $obs_species,
        "photo" => $obs_photos);

        make_observation($observation);
    }

    exit;
}

echo "iNaturalist interface<br>";

function fetch_obm_record($url, $data_id = null, $table = null, $access_token = null ) {
    
    if (isset($_COOKIE['access_token'])) {
        $cookie = json_decode( $_COOKIE[ "access_token" ] );

        $payload = array(
          //"client_id" => 'web-app',
          "access_token" => "{$cookie->data->access_token}",
          "scope" => 'get_data',
          "value" => "filter=obm_id=$data_id"
        );
    }


    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, "${url}/v2.3/pds.php");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }


}

function make_observation ($observation)  {

    extract($observation);

    $result = iNat_get_taxa("$taxon_name", $inat_useragent = '', $rank = null );
    /*
    {   "total_results":1,
        "page":1,
        "per_page":30,
        "results":[{
            "observations_count":3923,
            "taxon_schemes_count":4,
            "ancestry":"48460/1/2/355675/40151/848317/848320/848324/533971/46927/846233/846361/46942",
            "is_active":true,
            "flag_counts":{"unresolved":0,"resolved":0},
            "wikipedia_url":"http://en.wikipedia.org/wiki/European_mole",
            "current_synonymous_taxon_ids":null,
            "iconic_taxon_id":40151,
            "rank_level":10,
            "taxon_changes_count":0,
            "atlas_id":null,
            "complete_species_count":null,
            "parent_id":46942,
            "name":"Talpa europaea",
            "rank":"species",
            "extinct":false,
            "id":46970,
            "default_photo":{"square_url":"https://live.staticflickr.com/311/19011953972_686fc1c0cf_s.jpg","attribution":"(c) Wildlife Wanderer, some rights reserved (CC BY-NC-ND)","flags":[],"medium_url":"https://live.staticflickr.com/311/19011953972_686fc1c0cf.jpg","id":89814208,"license_code":"cc-by-nc-nd","original_dimensions":null,"url":"https://live.staticflickr.com/311/19011953972_686fc1c0cf_s.jpg"},
            "ancestor_ids":[48460,1,2,355675,40151,848317,848320,848324,533971,46927,846233,846361,46942,46970],
            "matched_term":"Talpa europea",
            "iconic_taxon_name":"Mammalia",
            "preferred_common_name":"European Mole"}]
    } */

    


    foreach ($result['results'] as $page) {
        if ($page['rank'] == 'species') {
            $taxon_id = $page['id'];
            break;
        } else
            continue;
    }

    $observation = array(
        'species_guess'=>"$taxon_name", //string
        'taxon_id'=>"$taxon_id", //string
        'observed_on_string'=>"$time", //string
        'time_zone'=>null, //use user's timezone
        'latitude'=>$latitude, //integer or float
        'longitude'=>$longitude, //integer or float
        'geoprivacy'=>'open', //string
    );

    //var_dump($observation);
    $response = iNat_post_observation($observation, $_SESSION['iNaturalist']['access_token']);
    json_encode(json_decode($response),JSON_PRETTY_PRINT);

    foreach ($photo as $p) {
        $file_path = 'local/attached_files/'.$p;
        $rp = iNat_post_observation_photo($file_path, $response['id'], $_SESSION['iNaturalist']['access_token'] );
    }


}

# Make am authorized action....
function retrieving_profile_data($token, $site) {
    $token = json_encode($response)["access_token"];
    $opts = array(
        'http'=>array(
            'method'=>"GET",
            'header'=>"Authorization: Bearer {$token}\r\n"
        )
    );
    $context = stream_context_create($opts);
    $file = file_get_contents("{$site}/users/edit.json}", false, $context);
}

# REQUEST AN AUTHORIZATION CODE
# Your web app should redirect the user to this url. They should see a screen
# offering them the choice to authorize your app. If they aggree, they will be
# redirected to your redirect_uri with a "code" parameter
function requests_authorization_code($site, $app_id, $redirect_uri) {

    # REQUEST AN AUTH TOKEN
    # Once your app has that code parameter, you can exchange it for an access token:
    #print "Go to ${url}, approve the app, and you should be redirected to your 
    #    redirect_uri. Copy and paste the 'code' param here.";
    #print "Code: {$_GET['code']}";
    $url = "${site}/oauth/authorize?client_id=${app_id}&redirect_uri=${redirect_uri}&response_type=code";
    header("Location: $url");
}

function auth_with_authorization_code($app_id,$app_secret,$auth_code,$redirect_uri,$site) {
    $payload = array(
      "client_id" => $app_id,
      "client_secret" => $app_secret,
      "code" => $auth_code,
      "redirect_uri" => $redirect_uri,
      "grant_type" => "authorization_code"
    );
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, "${site}/oauth/token");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}



 


# kgbase / php-inaturalist
#
# https://github.com/kgbase/php-inaturalist


function iNat_auth_request_by_passwd($app_id, $app_secret, $inat_user, $inat_pass, $baseurl = 'https://www.inaturalist.org/oauth/token')
{
    $curl = curl_init();
    $payload = array('client_id' => $app_id, 'client_secret' => $app_secret, 'grant_type' => "password", 'username' => $inat_user, 'password' => $inat_pass,);
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $baseurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}

/**
 * iNaturalist POST/observations:
 * @link https://www.inaturalist.org/pages/api+reference#post-observations
 * @param array $observation observation parameters
 * example of the array:
 * $observation = array(
 *  'species_guess'=>'Some name', //string
 *  'taxon_id'=>'taxon id from iNaturalist', //string
 *  'observed_on_string'=>'some datetime', //string
 *  'time_zone'=>'some zone', //some zone
 *  'latitude'=>0.0, //integer or float
 *  'longitude'=>0.0, //integer or float
 *  'geoprivacy'=>'open', //string
 * )
 * 'taxon_id' value may be received from iNat_get_taxa function: usually it is in [0]['id'] element of array returned (if taxon name is correct);
 * For receiving some parameters of the observation, some additional functions (see tools_functions.php) may be useful:
 * if there is some geotagged photo of the observation, 'observed_on_string', 'latitude' and 'longitude' values may be received from get_data_from_exif function;
 * 'time_zone' value may be received from get_timezone_data function.
 * @param string $access_token access token of your iNaturalist application (see iNat_auth_request_by_passwd function)
 * @param string $baseurl endpoint
 * @return array|null response from iNaturalist.org: new observation data, authorization error message (if token or data is incorrect) or null if the request (curl_exec) was unsuccessfull
 */
function iNat_post_observation($observation, $access_token, $baseurl = 'https://api.inaturalist.org/v1/observations')
{
    $header = array("Content-Type:application/json", "Authorization: Bearer $access_token");
    $payload = json_encode(array("observation" => $observation));
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $baseurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}

# Create a new observation field value.
#
function iNat_post_observation_field_value($field_value, $access_token, $baseurl = 'https://api.inaturalist.org/v1/observation_field_values')
{
    $header = array("Content-Type:application/json", "Authorization: Bearer $access_token");
    $payload = json_encode($field_value, JSON_PRETTY_PRINT);
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $baseurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}

# Add photos to observations
function iNat_post_observation_photo($file_path, $observation_id, $access_token, $baseurl = 'https://api.inaturalist.org/v1/observation_photos')
{
    $boundary = uniqid();
    $delimiter = '-------------' . $boundary;
    $file_src = fopen($file_path, 'rb');
    $file_content = fread($file_src, filesize($file_path));
    fclose($file_src);
    $file_info = pathinfo($file_path);
    $file_name = $file_info['basename'];
    $header = array("Content-Type: multipart/form-data; boundary=$delimiter", "Authorization: Bearer $access_token",);
    $body = "--$delimiter\r\n";
    $body .= 'Content-Disposition: form-data; name="observation_photo[observation_id]"' . "\r\n\r\n$observation_id\r\n--$delimiter\r\n";
    $body .= 'Content-Disposition: form-data; name="file"; filename="' . $file_name . '"' . "\r\nContent-Type: image/jpeg\r\n\r\n";
    $body .= "$file_content\r\n--$delimiter--";
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $baseurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}

function iNat_get_taxa($taxa, $inat_user = '', $rank = null, $baseurl = 'https://api.inaturalist.org/v1/taxa')
{
    $taxa = str_replace(' ', '%20', $taxa);
    if (!$rank) $url = $baseurl . '?q=' . $taxa; else $url = $baseurl . '?q=' . $taxa . '&rank=' . $rank;
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, $inat_user);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($curl);
        curl_close($curl);

        $object = json_decode($out);
        return json_decode(json_encode($object), true);
    } else {
        return null;
    }
}

?>
