<?php
# These routes created by the results_asTable module
# 
$state_file = getenv('OB_LIB_DIR')."modules/state/results_asTable.state";
if (file_exists($state_file)) {
    $state = trim(file($state_file));
    if ($state === 'disabled') return;
}

if (count($path) and $path[0] != '') {
    switch ($path[0]) {
        case "show-table":
            $chunks['show'] = '';
            //$chunks['table'] = $path[1];
            $_GET = $chunks;
            require(getenv('OB_LIB_DIR').'modules/results_asTable.php');
            exit;
            break;
        case "view-table":
            $chunks['view'] = '';
            $chunks['table'] = $path[1];
            $_GET = $chunks;
            require(getenv('OB_LIB_DIR').'modules/results_asTable.php');
            exit;
            break;
    }
}
?>
