<?php

/* Postgre SQL Connect function
 *
 * */

/*function PGconnectSQL($db_user,$db_pass,$db_name,$db_host,$conn_type='new') {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    if ($conn_type == 'async') {
        $conn=pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=10",PGSQL_CONNECT_ASYNC);
    } else    
        $conn=pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    pg_set_client_encoding( $conn, 'UTF8' );
    return $conn;
}*/

function PDO_connect($db_user,$db_pass,$db_name,$db_host) {
    $dsn = "pgsql:host=$db_host;dbname=$db_name";
    $custom_log = "/var/log/openbiomaps.log";
    $date = date('M j h:i:s');

    try {
        $pdo = new PDO($dsn, $db_user, $db_pass, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
        return $pdo;
    } catch (PDOException $e) {
        error_log("[OBM_" . PROJECTTABLE . "] $date: 'Postgres server is not available'\n", 3, $custom_log);
        return null;
    }
}

function PGPconnectSQL($db_user,$db_pass,$db_name,$db_host) {
    if ($db_host=='' or $db_user=='' or $db_pass=='' or $db_name=='') return;
    //$conn = pg_pconnect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    $conn = pg_connect("host=$db_host port=".POSTGRES_PORT." user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
    if (!is_resource($conn)) {
        $custom_log = "/var/log/openbiomaps.log";
        if (is_writable($custom_log)) {
            $date = date('M j h:i:s'); 
            error_log("[OBM_".PROJECTTABLE."] $date: 'Postgres server is not available'\n", 3, $custom_log);
        } else {
            openlog("[OBM_".PROJECTTABLE."]", 0, LOG_LOCAL0);
            syslog(LOG_WARNING,'Postgres server is not available');
            closelog();
        }
        return false;
    } else {
        pg_set_client_encoding( $conn, 'UTF8' );
        pg_query($conn,"SET TIMEZONE TO '".date_default_timezone_get()."';");

        return $conn;
    }
}

/* Postgre SQL Query function
 * depreceted
 * */
function PGquery($ID,$qstr) {
  $res = pg_query($ID,$qstr);
  # Logfájlba: 1 hiba történt itt:
  #if(pg_last_error($ID)) {
  #  echo $qstr;
  #  echo pg_last_error($ID);
  #}
  #print $qstr.'<br>'; 
  return $res;
}

/* Postgre SQL Query function
 * depreceted
 * */
function PGsqlcmd($ID,$qstr) {
   $res = pg_query($ID,$qstr);
   $nur = pg_affected_rows($res);
   return $nur;
}

/** PostgreSQL Query function
 *
 * A function which sends a single SQL command or an array of commands, and returns an array of 
 * resources. On faiulure of any of the commands, the whole command series is rollbacked.
 *
 * @param resource $db DB connection
 * @param string|string[] $cmd  query string or array of query strings
 *
 * @return resource[]|false 
 */ 
function query($db,$cmd) {
    if (gettype($cmd) === "string")
        $cmd = [$cmd];

    pg_query($db, "BEGIN;");
    $cmdStr = implode('',$cmd);
    if (pg_send_query($db, $cmdStr)) {
        $results = [];
        for ($i = 0, $l = count($cmd); $i < $l; $i++) {
            $res = pg_get_result($db);
            $results[] = $res;
            $state = pg_result_error($res);
            if ($state != '') {
                log_action($state,__FILE__,__LINE__);
                log_action($cmd[$i],__FILE__,__LINE__);
                pg_query($db,'ROLLBACK;');
                return false;
            }
        }
        pg_query($db,'COMMIT;');
        return $results;
    }
    else {
        log_action("ERROR: pg_send_query failed!",__FILE__,__LINE__);
        return false;
    }
}

/**
  * General function for checking if a PostgreSQL trigger is enabled
  */
function is_trigger_enabled($trigger_name) {
    global $ID;
    $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname=%s",quote($trigger_name));
    if (!$result = pg_query($ID, $cmd)) {
        log_action('query error', __FILE__, __LINE__, __FUNCTION__);
        return false;
    }
    if ( pg_num_rows($result) === 0) {
        return false;
    }
    $row = pg_fetch_assoc($result);
    return ($row['tgenabled'] == 'O');
}

/**
 * reads a named sql command from a sql file
 *
 * for creating the sql files see includes/modules/examples/example_commands.sql
 * 
 * @param string $name name of the sql command
 * @param string $sql_file name of a module or abs path to an sql file
 *
 * @return string sql command 
 **/
function getSQL($name, $file) {
    if (file_exists($file)) {
        $sql_file = $file;
    }
    elseif (file_exists(getenv('OB_LIB_DIR') . "modules/$file.sql")) {
        $sql_file = getenv('OB_LIB_DIR') . "modules/$file.sql";
    }
    else {
        return false;
    }
    return (preg_match("/-- $name\s(.*?)\s([-]{2}?|$)/s", file_get_contents($sql_file), $o)) ? $o[1] : '';    
}

require_once('obrm.php');
    
?>
