<?php
/* modules {}   // table modules
 * x_modules {} // project modules
 *
 * */

// table modules
// at construction the table must be provided!
class modules {

    public $modules = array();
    public $schema;
    public $main_table;
    public $modules_file = 'table_modules.php';

    /*  */
    public function __construct($main_table) {
        global $BID;

        if (!preg_match("/\./",$main_table)) {
            $this->schema = "public";
            $this->main_table = $main_table;
        } else {
            list($this->schema, $this->main_table) = preg_split("/\./",$main_table);
        }

        
        #debug('set table modules: '.$this->modules_file,__FILE__,__LINE__);
        $content = file(getenv('OB_LIB_DIR').$this->modules_file);

        $m_name = 0;
        $meth = 0;
        $examp = 0;
        $desc = 0;
        $methods = array();
        $examples = array();
        $descriptions = array();

        if (isset($_SESSION['Tid'])) {
            $a = "0,1";
            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') {
                $g = $_SESSION['Tgroups'].',0';

                // inherited privileges from other groups
                $cmd = sprintf('SELECT role_id FROM project_roles WHERE container && \'{%1$s}\'::int[] AND role_id NOT IN (%1$s)',$_SESSION['Tgroups']);
                $res = pg_query($BID,$cmd);
                $ig = array();
                while ($row = pg_fetch_assoc($res)) {
                    $ig[] = $row['role_id'];
                }
                $igi = implode(',',$ig);
                if ($igi!='')
                    $g .= ",$igi";

            } else
                $g = "0";
        } else {
            $a = "0";
            $g = "0";
        }

        $cmd = sprintf("SELECT project_table,path,module_name,array_to_string(params,';') as p, enabled, main_table, new_params, methods, examples, module_access, array_to_string(group_access,',') as groups
            FROM modules 
            WHERE enabled='t' AND project_table='%s' AND module_access IN ($a) AND (group_access && ARRAY[$g] OR array_length(group_access, 1) IS NULL) AND module_type='table' 
            ORDER BY module_name,module_access DESC,group_access DESC", 
                PROJECTTABLE);

        $res = pg_query($BID,$cmd);
        $modules = array();
        if(pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {

                $path = getenv('PROJECT_DIR').$row['path'];
                $module_name_file = $row['module_name'].'.php';
                
                if ($row['new_params'] != '') $row['p'] = json_decode($row['new_params'],true);      // A new_params tömböt ad vissza, míg a params stringet!
                if ($row['methods'] != '') $row['methods'] = json_decode($row['methods'],true);      //

                if ($row['methods'] == '') {
                    $content = file($path.$module_name_file);
                    $process = 0;
                    $yaml = "";
                    foreach ($content as $rows) {
                        if (($process === 0 or $process == 2 )and preg_match("/^#'/",$rows)) {
                            $process = 2;
                        } elseif ($process){
                            $process = 1;
                            continue;
                        }
                        if ($process == 2) {
                            $yaml .= preg_replace("/^#' /","",$rows);
                        }
                    }
                    if ($yaml != '') {
                        $parsed = yaml_parse($yaml);
                        $row['methods'] = $parsed['Methods'];
                        $row['examples'] = isset($parsed['Examples']) ? $parsed['Examples'] : '';
                        $res = $this->module_registration($row,'update');
                    } else {
                        log_action("{$row['module_name']}: Bad module format, yaml header missing.",__FILE__,__LINE__);
                    }
                }
                $modules[] = $row;
            }
        }

        $this->modules = $modules;
    }

    public function set_main_table($main_table) {
        $this->main_table = $main_table;
    }

    public function get_modules() {
        return $this->modules;
    }

    # Miért van erre szükség??
    # Nem elég ha a set_modules()-ban le sem kérdezem a nem engedélyezett modulokat???
    #
    public function is_enabled($name) {
        return (count($this->filter_modules($name)) > 0);
    }

    public function get_params($name, $main_table = '') {
        if (isset($this->filter_modules($name, $main_table)[0])) {
            return $this->filter_modules($name, $main_table)[0]['p'];
        }
        return "";
    }

    /***
     *   
     *  */
    public function which_has_method($method, $all_modules = false) {

        $main_table = $this->main_table;

        return array_column(array_filter($this->modules, function ($m) use ($method, $main_table, $all_modules) {

            if (!$m['methods']) {
                return false;
            }

            return ($main_table != '') 
                ? ($m['main_table'] === $main_table && in_array($method, $m['methods']) && $m['enabled'] === 't')
                : ($all_modules && in_array($method, $m['methods']) && $m['enabled'] === 't');

        }),'module_name');
    }

    public function get_example($name) {

        foreach($this->modules as $m) {
            if ($m['examples'] != '' && $m['main_table'] == $this->main_table && $m['module_name'] == $name)
                return $m['examples'];
        }
        return "";

    }

    public function filter_modules($name) {
        $main_table = $this->main_table;
        
        return array_values(array_filter($this->modules, function ($m) use ($name, $main_table) {
            // FORCE use modules - if a module use an other 
            if (isset($_SESSION['force_use']) and isset($_SESSION['force_use'][$name]) and $_SESSION['force_use'][$name] == $name) return 1;

            // general modules without main_table
            if ($m['main_table'] == '' && $m['module_name'] === $name && $m['enabled'] === 't') return true;

            // filter all module if we explicit not ask them for a table
            if ($main_table === null && $m['module_name'] === $name) return true;

            return ($m['main_table'] === $main_table && $m['module_name'] === $name && $m['enabled'] === 't');
        }));
    }

    // A module debug function
    public function log_modules($main_table = '') {
        if ($main_table === '') $main_table = $this->main_table;
        foreach ($this->modules as $m) {
            log_action($m,__FILE__,__LINE__);
        }
    }

    # Include table modules
    # module: module name
    # method: a function name
    # dynamic_params: array
    # skip_evaluation: boolean - force loading module
    # debug: boolean - ldebugging module
    public function _include($module, $method, $dynamic_params = array(), $skip_evaluation = FALSE, $debug = FALSE) {

        require_once(getenv('OB_LIB_DIR').'default_modules.php');
        require_once(getenv('OB_LIB_DIR').'table_modules.php');

        #if ($module == 'text_filter') $debug = true;
        
        if ($debug) {
            debug($module, __FILE__, __LINE__);
            debug($this->main_table, __FILE__, __LINE__);
            debug($this->is_enabled($module,$this->main_table),__FILE__,__LINE__);
            debug($method,__FILE__,__LINE__);
            debug($dynamic_params,__FILE__,__LINE__);
            debug($skip_evaluation,__FILE__,__LINE__);
        }

        if ($skip_evaluation || $this->is_enabled($module,$this->main_table)) {
            $dmodule = new tableModules($module);
            $dmodule->main_table = $this->main_table;
            
            if (!method_exists($dmodule,$module)) {
                log_action("Method $method in $module of defModules does not exist!", __FILE__, __LINE__);
                return;
            }

            $static_params = $this->get_params($module,$this->main_table);

            $retval = $dmodule->$module($method, $static_params, $dynamic_params);
            #if ($module == 'text_filter' and $method=='print_js')
            #    debug("included for ". $method,__FILE__,__LINE__);
            return $retval;
        }
        else {
            return false;
        }
    }

    public function list_modules($module_type,$with_local_modules = true) {
        global $BID;
        
        $names = array();

        $module_files = array();
        $path = array();

        foreach (glob(getenv('OB_LIB_DIR').'modules/'."*.php") as $filename) {
            $module_files += array(basename($filename) => $filename);
            $path[] = 'system';
        }

        if ($with_local_modules) {
            # overwrite global modules if local replacments are exists
            foreach (glob(getenv('PROJECT_DIR')."local/includes/modules/"."*.php") as $filename) {
                $module_files += array(basename($filename) => $filename);
                $path[] = 'local';
            }
        }

        $x = 0;
        sort($module_files);
        foreach ($module_files as $module=>$file) {
            
            $content = file($file);
            $process = 0;
            $yaml = "";
            $n = 0; // only looking for modules yaml header at the beggining of the file;
            foreach ($content as $rows) {
                if (($process === 0 or $process == 2 ) and preg_match("/^#'/",$rows)) {
                    $process = 2;
                } elseif ($process){
                    $process = 1;
                    continue;
                }
                if ($process == 2) {
                    $yaml .= preg_replace("/^#' /","",$rows);
                }
                if ($process === 0 and $n > 3) {
                    break;
                } 
                $n++;
            }
            if ($yaml != '') {
                $parsed = yaml_parse($yaml);
                if (!isset($parsed['Module-type']) or ($parsed['Module-type'] == $module_type or $parsed['Module-type'] == 'both'))
                    $names[] = array (
                        'name' => $parsed['Module'],
                        'path' => $path[$x],
                        'tpye' => $module_type,
                        'examples' => isset($parsed['Examples']) ? $parsed['Examples'] : '',
                        'methods' =>  $parsed['Methods'],
                        'module_type' => $parsed['Module-type']
                    );
            }
            $x++;
        }

        return $names;
    }

    public function module_registration($module,$method,$project = PROJECTTABLE) {
        global $BID;
        extract($module);
        if ($method == 'insert') {
            $cmd = sprintf("INSERT INTO modules (project_table, path, module_name, enabled, main_table, new_params, methods, examples, module_type) 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                    quote($project),quote($path),quote($module_name),quote($enabled),quote($main_table),quote($new_params),quote(json_encode($methods)), quote($examples), quote($module_type));
            $res = pg_query($BID,$cmd);
            if (!pg_last_error($BID)) {
                log_action("Module $module_name registered.",__FILE__,__LINE__);
            }
            else {
                log_action($cmd,__FILE__,__LINE__);
            }
        } elseif ($method == 'update') {
            $cmd = sprintf("UPDATE modules SET 
                methods = %s, 
                examples = %s WHERE project_table='".PROJECTTABLE."' AND module_name=%s AND main_table=%s",
                    quote(json_encode($methods)), quote($examples),quote($module_name),quote($main_table));
            $res = pg_query($BID,$cmd);
            if (!pg_last_error($BID)) {
                log_action("Module $module_name registered.",__FILE__,__LINE__);
            } else {
                log_action($cmd,__FILE__,__LINE__);
            }

        }
        return $res;
    }
}

// project modules
class x_modules extends modules {

    public function __construct() {
        global $BID;

        if (isset($_SESSION['Tid'])) {
            $a = "0,1";
            if (isset($_SESSION['Tgroups']) and $_SESSION['Tgroups']!='') {
                $g = $_SESSION['Tgroups'].',0';

                // inherited privileges from other groups
                $cmd = sprintf('SELECT role_id FROM project_roles WHERE container && \'{%1$s}\'::int[] AND role_id NOT IN (%1$s)',$_SESSION['Tgroups']);
                $res = pg_query($BID,$cmd);
                $ig = array();
                while ($row = pg_fetch_assoc($res)) {
                    $ig[] = $row['role_id'];
                }
                $igi = implode(',',$ig);
                if ($igi!='')
                    $g .= ",$igi";

            } else
                $g = "0";
        } else {
            $a = "0";
            $g = "0";
        }

        $cmd = sprintf("SELECT project_table,path,module_name,array_to_string(params,';') as p, enabled, main_table, new_params, methods, examples, module_access, array_to_string(group_access,',') as groups
            FROM modules 
            WHERE enabled='t' AND project_table='%s' AND module_access IN ($a) AND (group_access && ARRAY[$g] OR array_length(group_access, 1) IS NULL) AND module_type='project' 
            ORDER BY module_name,module_access DESC,group_access DESC", PROJECTTABLE);

        $res = pg_query($BID,$cmd);
        $modules = array();
        if(pg_num_rows($res)) {
            while ($row = pg_fetch_assoc($res)) {

                $path = getenv('PROJECT_DIR').$row['path'];
                $module_name_file = $row['module_name'].'.php';
                
                if ($row['new_params'] != '') $row['p'] = json_decode($row['new_params'],true);      // A new_params tömböt ad vissza, míg a params stringet!
                if ($row['methods'] != '') $row['methods'] = json_decode($row['methods'],true);      //

                if ($row['methods'] == '') {
                    $content = file($path.$module_name_file);
                    $process = 0;
                    $yaml = "";
                    foreach ($content as $rows) {
                        if (($process === 0 or $process == 2 )and preg_match("/^#'/",$rows)) {
                            $process = 2;
                        } elseif ($process){
                            $process = 1;
                            continue;
                        }
                        if ($process == 2) {
                            $yaml .= preg_replace("/^#' /","",$rows);
                        }
                    }
                    if ($yaml != '') {
                        $parsed = yaml_parse($yaml);
                        $row['methods'] = $parsed['Methods'];
                        $row['examples'] = isset($parsed['Examples']) ? $parsed['Examples'] : '';
                        $res = $this->module_registration($row,'update');
                    } else {
                        log_action("{$row['module_name']}: Bad module format, yaml header missing.",__FILE__,__LINE__);
                    }
                }
                $modules[] = $row;
            }
        }
        # Debug
        #$ids = array_column($modules, 'module_name');
        #debug($ids,__FILE__,__LINE__);
        
        $this->modules = $modules;
       // debug($this->modules, __FILE__, __LINE__);
    }

    /***
     *  If the main_table argument is missing, the method gives back results of modules 
     *  from every table of the project 
     *  */
    public function which_has_method($method, $all_modules = false) {

        return array_column(array_filter($this->modules, function ($m) use ($method) {
            return (in_array($method, $m['methods']) && $m['enabled'] === 't');
        }),'module_name');
    }

    # Include project modules
    public function _include($module, $method, $dynamic_params = array(), $skip_evaluation = FALSE, $debug = FALSE) {

        require_once(getenv('OB_LIB_DIR').'default_modules.php');
        require_once(getenv('OB_LIB_DIR').'project_modules.php');

        #if ($module == 'linnaeus') $debug = true;
        
        if ($debug) {
            debug($module, __FILE__, __LINE__);
            debug(PROJECTTABLE, __FILE__, __LINE__);
            debug($this->is_enabled($module,PROJECTTABLE),__FILE__,__LINE__);
            debug($method,__FILE__,__LINE__);
            debug($dynamic_params,__FILE__,__LINE__);
        }

        if ($this->is_enabled($module,PROJECTTABLE)) {
            $dmodule = new projectModules($module);
        
            $dmodule->main_table = PROJECTTABLE;
            
            #if (!method_exists($dmodule,$module)) {
            #    log_action("Method $module of defModules does not exist!", __FILE__, __LINE__);
            #    return;
            #}

            $static_params = $this->get_params($module,PROJECTTABLE);

            #$retval = $dmodule->$module($method, $static_params, $dynamic_params);
            $retval = $dmodule->load_module($module, $method, $static_params, $dynamic_params);
            return $retval;
        }
        else {
            return false;
        }
    }

    public function filter_modules($name) {
        return array_values(array_filter($this->modules, function ($m) use ($name) {
            return ($m['module_name'] === $name && $m['enabled'] === 't');
        }));
    }

}

?>
