<?php
# Create the project and authentication specific js variables object
# varaibles for the maps.js 
#
#debug('include main.js.php',__FILE__,__LINE__);
$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

# Map behaviour initial variables
$cmd = sprintf("SELECT st_x(map_center) as x, st_y(map_center) as y, map_zoom, click_buffer, case zoom_wheel when 't' then 'true' else 'false' end as zoom_wheel, 
                    default_layer, fixheader::integer, turn_off_layers, subfilters::integer
                FROM project_variables 
                WHERE project_table='%s'",PROJECTTABLE);
$res = pg_query($BID,$cmd);

if ( $row = pg_fetch_assoc($res)) {
    /* mainpage content1 function */
    //define('MAINPAGE_FUNCTION_CONTENT1','upload-table');

    // default click query buffer in meters
    define('CLICK_BUFFER',$row['click_buffer']);
    $buffer = CLICK_BUFFER;             

    /* default mouse wheel behavior on map page 
     * true - zoom
     * false - scroll */
    define('ZOOM_WHEEL_ENABLED',$row['zoom_wheel']);
    $zoomWheelEnabled = ZOOM_WHEEL_ENABLED;
    if (isset($_GET['mousezoom']) and $_GET['mousezoom'] == 'off') {
        $zoomWheelEnabled = false;
    } elseif (isset($_GET['mousezoom']) and $_GET['mousezoom'] == 'on') {
        $zoomWheelEnabled = true;
    }
    
    $x = (float)$row['x'];
    $y = (float)$row['y'];
    // Personal area of interest (bottom-left - top-right corner) setting could come from user settings.... 
    $area_of_interest = sprintf("[%f,%f,%f,%f]",$x-0.5,$y-0.5, $x+0.5, $y+0.5);

    // zoom to map's center
    $zoom = $row['map_zoom'];
    /* bheader in map page fixed or dynamic 
     * project's setting */
    $fixheader = $row['fixheader'];
    if (isset($_GET['fixheader']) and $_GET['fixheader']===true)
        $fixheader = 'true';
    elseif (isset($_GET['fixheader']) and $_GET['fixheader']===false)
        $fixheader = 'false';

    $turn_off_layers = $row['turn_off_layers'];
    /* Using reqursive filters 
     * User's setting */
    $subfilters = $row['subfilters'];
    $default_layer = $row['default_layer'];
    // map's center longitude WGS 84
    $lon = $row['x'];
    // map's center latitude WGS 84
    $lat = $row['y'];

    // this shoud be defined as an empty string
    $loadq = '';

    $overview_map = 1;

    $layers = array();

    // Ha van az adatbázis elérésre megkötés
    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }

    $cmd = sprintf('SELECT f_srid FROM header_names WHERE f_project_name=\'%s\' AND f_project_table=\'%s\'',PROJECTTABLE,$_SESSION['current_query_table']);
    $res = pg_query($BID,$cmd);
    $row=pg_fetch_assoc($res);
    $layer_srid = $row['f_srid'];

    # PRIVATE - should be used by web interface!
    $cmd = sprintf(
        "SELECT 
            pl.name, pl.layer_def, pl.map, pl.url, 
            concat_ws('_',pl.layer_name,layer_order) as layer_name,
            pl.layer_name AS cname, pl.tipus, singletile, legend 
        FROM project_layers pl 
        LEFT JOIN project_queries pq ON (pl.layer_name=pq.layer_cname) 
        WHERE 
            (pl.project_table='%1\$s' AND pl.enabled=TRUE AND pq.main_table='%2\$s' AND pq.enabled=TRUE) OR 
            (pl.project_table='%1\$s' AND pl.enabled=TRUE AND pq.main_table IS NULL) OR 
            (pl.project_table IS NULL AND pl.enabled=TRUE) 
        ORDER BY pl.layer_name='%3\$s' DESC,layer_order DESC",PROJECTTABLE,$_SESSION['current_query_table'],$default_layer);

    $grid_layer = $modules->_include('grid_view','get_grid_layer');
        
    //onlymap
    //if ($load_mainpage==2)
    //    $cmd = "SELECT * FROM project_layers WHERE (project_table IS NULL) AND rst=0 AND enabled=TRUE ORDER BY layer_name='".DEFAULT_LAYER."' DESC";

    $res = pg_query($BID,$cmd);

    $LEGEND = 0;
        
    $MAPSERV = PRIVATE_MAPSERV;             // default mapfile which contains wms and wfs layers as well
    while ($row=pg_fetch_assoc($res)) {
        if ($row['tipus']!='') {

            // Local data layer
            if (preg_match('/^layer_data_(.+)/',$row['layer_name'],$m)) {
                // Json layer definitions
                if (preg_match('/^{/',$row['layer_def'])) {
                    $layer_def = json_decode($row['layer_def'],true);
                } else {
                    // backward compatibility, comma separated list
                    $layer_def = preg_split("/,/",$row['layer_def']);
                    $u = array();
                    foreach ($layer_def as $x) {
                        list($k,$v) = preg_split("/:/",$x,2,);
                        $u[trim($k)] = str_replace("'", "", trim($v));
                    }
                    $layer_def = $u;
                }
                //$layer_def = preg_replace("/[{}]/","",preg_replace('/"/',"'",json_encode($layer_def,JSON_UNESCAPED_SLASHES)));

                if (isset($layer_def['layers']) and $row['legend']=='t') {
                    // SET Legend layer for interface SHOW_LEGEND
                    $_SESSION['ms_layer'] = $layer_def['layers'];
                    $LEGEND = 1;
                }
                // GRID Module Hack....
                if ($grid_layer!='') {
                    // if grid module enabled and there are active grid layers - all layers displayed in singletile mode
                    // It is much more faster than the multi layer queries...
                    $row['singletile'] = 't';
                }
            }
            else {
                // Any other layer
                // layer_def Should be JSON
                if (preg_match('/^{/',$row['layer_def'])) {
                    $layer_def = json_decode($row['layer_def'],true);
                } else {
                    // backward compatibility, comma separated list processing
                    $layer_def = preg_split("/,/",$row['layer_def']);
                    $u = array();
                    foreach ($layer_def as $x) {
                        if ($x != '') {
                            list($k,$v) = preg_split("/:/",$x,2);
                            $u[trim($k)] = str_replace("'", "", trim($v));
                        }
                    }
                    $layer_def = $u;
                }
            }

            //mapserver map file path 
            //.htaccess setting: change PMAP to MAP and read ACCESS for set the map path
            if ($row['map']=="default" or $row['map']=="proxy" or $row['map']=="cache") $map = MAP;
            else $map = $row['map'];

            //mapserver url
            if ($row['url'] == 'proxy' or $row['url'] == "default") 
                #$url = 'http://'.PRIVATE_MAPSERV.'?time='.time();
                $url = $protocol.'://'.PRIVATE_MAPSERV.'';
            elseif ($row['url'] == 'cache') 
                $url = $protocol.'://'.PRIVATE_MAPCACHE;
            else 
                $url = $row['url'];

            if ($row['tipus']=='query-build') $row['tipus'] = 'WMS';

            // data sheet page: no data layers, no identify points
            if (isset($load_data) && $load_data) {
                if ($url) continue;
            }

            $layerdef = new defLayer([
                'Source'=>$row['tipus'],
                'Name'=>$row['name'],
                'Url'=>$url,            // string
                'Map'=>$map,            // string
                'Opts'=>$layer_def,     // array
                'Cname'=>$row['cname'],
                'SingleTile'=>$row['singletile']
                ]);
            $layers[$row['layer_name']] = $layerdef->printLayerSources();
        }
    }
    //$query_url  = "http://$MAPSERV?SERVICE=WFS&VERSION=1.1.0&MAP=".MAP;
    $query_url  = "$protocol://$MAPSERV";

    define('SHOW_LEGEND',$LEGEND);

    # END OF VARIABLE SETTINGS **********************************************************************************

    // create OpenLayers layer for the bookmarked queries
    // GET Bookmark
    // a prepare_vars ban képződik
    if (isset($load_loadquery) and $load_loadquery) 
    {
        // simple validity check
        if (strlen($load_loadquery)>40) return;
        //$loadq = "loadq: \"$.get('$protocol://".URL."/includes/load_queries.php?id=$load_loadquery',WFSGet,{'skip_processing':'no'});\",";
        $loadq = "loadq: \"$.get('$protocol://".URL."/includes/load_queries.php?id=$load_loadquery',WFSGet);\",";
    } 

    $ld = "";
    foreach ($layers as $key=>$val) {
        if ($key=='' or $val=='') continue;
        $ld .= sprintf("$key: \"$val\",\n");
    }

    $url = URL;
    $projecttable = PROJECTTABLE;
    // The layer_data layer should be exists, the other layer_ variables are optional

    $drop_profile_confirm_text = addslashes(str_drop_profile_confirm_text);
    $confirmation_email_sent = addslashes(str_confirmation_email_sent);
    $confirmation_email_failed = addslashes(str_confirmation_email_sending_failed);
    $confirmation_yes = addslashes(str_confirmation_yes);
    $confirmation_no = addslashes(str_confirmation_no);
    $no_res_exts = json_encode(array("enabled"=>"false"));

    /* This module those not exists yet! 
     * */
    $add_record_here_module = false;
    if ($modules->is_enabled('add_record_here')) {
        //default itegrated module
        $add_record_here_form = $modules->_include('add_record_here','get_form',[],true);
        $text = "<br>Add data <a href=#$protocol://".URL."/upload/?form=$add_record_here_form&type=web&set_fields=@jgf@#>here?</a>";
        $no_res_exts = json_encode(array("enabled"=>"true","text"=>$text));
    }

    $understand_error = addslashes(str_understand_error);
    $most_probaly = addslashes(str_most_probably_a_form_setting_issue);
    $follow_this_link = addslashes(str_follow_this_link);
    $what_is_this_error = addslashes(str_what_is_this_error);
    $session_expired_message = addslashes(str_session_expired_or);
    $error_occured_on_uploading = addslashes(str_error_occured_on_uploading);
    $no_results_for_the_query = addslashes(str_no_results_for_the_query);
    $failed_to_assemble_query_string = addslashes(str_failed_to_assemble_query_string);
    $preparation_of_results = addslashes(str_preparation_of_results);
    $message = addslashes(str_messages);
    $processing_query = addslashes(str_processing_query);
    $really_want_drop_intr_uplds = addslashes(str_drop_interrupted_uploads);
    $intr_uploads = addslashes(str_intr_uploads);
    $yes_sure = addslashes(str_yes_sure);
    $no_no = addslashes(str_no_no);
    $languages = json_encode(LANGUAGES);
    $lang = $_SESSION['LANG'] ?? array_keys(LANGUAGES)[0];

    $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
    $profile_hash = $u->hash;
    
$js =  <<<EOD
var obj = {
str_yes_sure: '$yes_sure',
str_no_no: '$no_no',
str_intr_uploads: '$intr_uploads',
str_drop_intr_uplds_question: '$really_want_drop_intr_uplds',
project: '$projecttable',
load_map_page:0,
selectedLayer_srid: '$layer_srid',
drop_profile_confirm_text: '$drop_profile_confirm_text',
confirmation_email_sent: '$confirmation_email_sent',
confirmation_email_failed: '$confirmation_email_failed',
confirmation_yes: '$confirmation_yes',
confirmation_no: '$confirmation_no',
no_results_for_the_query: '$no_results_for_the_query',
failed_to_assemble_query_string: '$failed_to_assemble_query_string',
preparation_of_results: '$preparation_of_results',
message: '$message',
processing_query: '$processing_query',
lon: '$lon',
lat: '$lat',
zoom: '$zoom',
zwe: '$zoomWheelEnabled',
buffer: "$buffer",
turn_off_layers: '$turn_off_layers',
fixheader: '$fixheader',
subfilters: '$subfilters',
url: "$protocol://$url/",
query_url: '$query_url',
no_res_exts: '$no_res_exts',
overview_map: '$overview_map',
str_session_expired_or: '$session_expired_message',
str_error_occured_on_uploading: '$error_occured_on_uploading',
str_what_is_this_error: '$what_is_this_error',
str_follow_this_link: '$follow_this_link',
str_most_probably_a_form_setting_issue: '$most_probaly',
str_understand_error: '$understand_error',
languages: $languages,
language: "$lang",
profile_hash: '$profile_hash',
extent: $area_of_interest,
$ld
$loadq
};
EOD;

    echo $js;
}
?>
