<?php
class projectModules {

    private $module;
    private $default_module;
    private $private_module;
    private $private_path;
    public $available_methods;
    public $main_table;

    function __construct($module) {

        $this->default_module = getenv('OB_LIB_DIR').'modules/' . $module . '.php';
        $this->private_module = getenv('PROJECT_DIR').'local/includes/modules/' . $module . '.php';
        $this->private_path = getenv('PROJECT_DIR').'local/includes/modules/';

        $wp = '';
        if (file_exists($this->private_module)) {
            $wp = 'private';
            include_once($this->private_module);
        }
        elseif (file_exists($this->default_module)) {
            $wp = 'default';
            include_once($this->default_module);
        }
        else {
            log_action("$module module not found",__FILE__,__LINE__);
            return;
        }

        if (class_exists($module)) {
            $this->available_methods = get_class_methods($module);
            $this->module = $module;
        }
        else {
            log_action("class $module does not exists on $wp path!",__FILE__,__LINE__);
            return;
        }
    }
    public function has_method($method) {
        return (!empty($this->available_methods) && in_array($method,$this->available_methods));
    }

    public function init() {
        $module_name = $this->module;
        $module_name::init();
        // $this->module::init(); # php 7
    }

    function load_module ($module,$action,$params,$request=array()) {
        ## Check module exists?
        $mf = new $module($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;    
    }
}
?>
