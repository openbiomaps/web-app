<?php

class ObmAuth extends Auth {

    public $provider = 'self';

    public static function verify_access_token($access_token, $user_id, $remember) {

        $url = (defined('OAUTHURL')) ? constant('OAUTHURL').'/oauth/resource.php' : protocol().'://'.constant('URL').'/oauth/resource.php';

        // át kellene venni a bejelentkezés jelenlegi scope-jait!
        if (isset($_SESSION['scope']))
            $data = array('access_token'=>$access_token, 'scope'=>$_SESSION['scope'], 'table'=> constant('PROJECTTABLE'), 'remember_me' => $remember);
        else
            $data = array('access_token'=>$access_token, 'scope'=>'webprofile', 'table'=> constant('PROJECTTABLE'), 'remember_me' => $remember);

        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options); 
        $result = json_decode(file_get_contents($url, false, $context),true);

        $U = new User($user_id);
        
        return ($result['status'] === 'ok' && $U->email === $result['data']['user_id']);
    }

    public static function login_with_access_token ($access_token) {
        global $BID;
        $cmd = sprintf("SELECT email FROM users
                LEFT JOIN oauth_access_tokens oa ON (LOWER(oa.user_id)=LOWER(email))
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table='%s' GROUP BY users.id, pu.user_status",quote($access_token), constant('PROJECTTABLE'));

        $res = pg_query($BID,$cmd);
        if (!pg_num_rows($res)) {
            return false;
        }

        $row = pg_fetch_assoc($res);

        $U = new User($row['email']);
        return $U->login();
    }

    public static function refresh_token($token, $remember_me) {

        $refresh_token = $token;

        $url = (defined('OAUTHURL')) ? constant('OAUTHURL') . '/oauth/token.php' : protocol() . "://". constant('URL') . '/oauth/token.php';

        $data = array(
            'refresh_token'=>$refresh_token, 
            'client_id'=>'web', 
            'client_secret' => constant('WEB_CLIENT_SECRET'),
            'grant_type'=>'refresh_token',
            'scope' => 'webprofile',// get_tables get_data',
            'remember_me' => $remember_me
        );
        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        
        if ($result === FALSE) {
            parent::delete_cookie('refresh_token');
            return false;
        }

        $j = json_decode($result);

        parent::set_token_cookies(
            (object)[
                'access_token' => $j->access_token,
                'expires_in' => 3550,
                'refresh_token' => isset($j->refresh_token) ? $j->refresh_token : false,    // Ha nincs always issue refresh_token akkor refresh_token grant_type nem ad vissza másik refresh_tokent!
                'remember_me' => $remember_me
            ],
            'self'
        );
        return $j->access_token;
        
    }

    /**
     * Save user auth info with uniq key for clients
     *
     * @param string $key
     * @param string $accesstoken
     * @param string $username
     *
     * @return array|boolean
     */
    public static function save($key, $accesstoken, $project, $username) {
        global $BID;

        $cmd = sprintf('INSERT INTO auth_keystore ("key","token","project","user") 
            VALUES (%s,%s,%s,%s)
            ON CONFLICT ("key") 
            DO NOTHING',
            quote($key), quote($accesstoken), quote($project), quote($username)
        );
        $res = pg_query($BID,$cmd);
        if (!pg_num_rows($res)) {
            return false;
        }
        return true;
    }

    /**
     * Get user auth info with uniq key for clients
     *
     * @param string $key
     *
     * @return object
     */
    public static function get($key) {
        global $BID;

        $cmd = sprintf("DELETE FROM auth_keystore WHERE key=(SELECT key FROM auth_keystore WHERE key = %s) RETURNING *",quote($key));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $r = [
                "token" => json_decode($row["token"],true),
                "user" => $row["user"],
                "server" => constant("OB_DOMAIN")
            ];
            return $r;
        }
        return [];
    }

    /**
     * Logs a user in from an external app
     *
     * @param string $email
     * @param string $password
     *
     * @return array|boolean
     */
    public static function pwalogin($username, $password, $remember_me, $scopes, $client, $cookie_path) {
        global $ID;

        require_once(getenv('PROJECT_DIR').'oauth/server.php');
        $request = OAuth2\Request::createFromGlobals();

        // Client secret hack for pwa
        // Maybe it is not necessary, we can use pwa client without secret...
        // /etc/openbiomaps/system_vars.php.inc
        if ($client['client_id'] == 'pwa') {
            $client['client_secret'] = constant('PWA_CLIENT_SECRET');
        }
        if ($client['client_id'] == 'web') {
            $client['client_secret'] = constant('WEB_CLIENT_SECRET');
        }
        if ($client['client_id'] == 'mobile') {
            $client['client_secret'] = constant('MOBILE_CLIENT_SECRET');
        }


        # Doing a client_credential auth to create an access_token
        $request->request['client_id'] = $client['client_id'];
        //If your client is public (by default, this is true when no secret is associated with the client in storage), you can omit the client_secret value in the request:
        if ($client['client_secret'] != '') {
            $request->request['client_secret'] = $client['client_secret'];     
        }
        
        $request->request['remember_me'] = $remember_me;
        $request->request['grant_type'] = 'password';
        $request->request['username'] = $username;
        $request->request['password'] = $password;
        $request->request['scope'] = $scopes; 

        $oauthtoken = $server->handleTokenRequest($request)->getParameters();
        $oauthtoken['remember_me'] = $remember_me;
        
        $_SESSION['oauthtoken'] = $oauthtoken;
        
        if (isset($oauthtoken['error'])) {
            /* Handle error */
            log_action("Logging in failed with {$username}",__FILE__,__LINE__);
            log_action($oauthtoken['error_description'],__FILE__,__LINE__);
            return false;
        }

        if ( !self::oauth((object) $oauthtoken, "self", $cookie_path) ) {
            log_action('Oauth login failed - missing user from project_users table?',__FILE__,__LINE__);
            return false;
        }

        return true;
    }

    /**
     * Confirm authentication for switchprofile & sql console functionality
     *
     * @param string $email
     * @param string $password
     *
     * @return array|boolean
     */
    public static function reauth($username, $password) {
        global $ID, $BID;
        if (!isset($_SESSION['switchprofile'])) {
            return false;
        }
        if (!has_access('master')) {
            log_action('You do not have permission for this operation!', __FILE__, __LINE__);
            return false;
        }

        require_once(getenv('PROJECT_DIR').'oauth/server.php');
        $request = OAuth2\Request::createFromGlobals();
    
        $request->request['client_id'] = 'web';    
        $request->request['client_secret'] = constant('WEB_CLIENT_SECRET');     
        $request->request['remember_me'] = false;
        $request->request['grant_type'] = 'password';
        $request->request['username'] = $username;
        $request->request['password'] = $password;
        $request->request['scope'] = 'webprofile'; 

        $oauthtoken = $server->handleTokenRequest($request)->getParameters();
        
        $_SESSION['oauthtoken'] = $oauthtoken;
        
        if (isset($oauthtoken['error'])) {
            /* Handle error */
            log_action("Logging in failed with {$username}",__FILE__,__LINE__);
            log_action($oauthtoken['error_description'],__FILE__,__LINE__);
            return false;
        }
        // changing the user id associated with the access_token
        $cmd = sprintf("UPDATE oauth_access_tokens SET user_id='%s' WHERE access_token='%s'",$_SESSION['switchprofile'],$oauthtoken['access_token']);
        $res = pg_query($BID,$cmd);
        if (pg_affected_rows($res)) {
            log_action('account switched',__FILE__,__LINE__);
        }
        

        if ( !self::oauth((object) $oauthtoken, "self", "/") ) {
            log_action('Oauth login failed - missing user from project_users table?',__FILE__,__LINE__);
            return false;
        }

        return true;
    }


    /**
     * Logs a user in from local web app
     *
     * @param string $email
     * @param string $password
     *
     * @return array|boolean
     */
    public static function login($user, $password, $remember_me) {
        global $ID;

        $url = (defined('OAUTHURL')) ? constant('OAUTHURL').'/oauth/token.php' : protocol().'://' . constant('URL') . '/oauth/token.php';
        $cookie_path = "/"; // web login default
        $provider = "self"; // web login default
        
        $data = [
            'username' => $user,
            'password' => $password,
            'client_id' => 'web',
            'client_secret' => constant('WEB_CLIENT_SECRET'),
            'scope' => 'webprofile get_tables get_data',
            'grant_type' => 'password',
            'remember_me' => $remember_me
        ];

        $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
                )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        if ($result === FALSE) {
            /* Handle error */
            log_action("Logging in failed with {$user}",__FILE__,__LINE__);
            return false;
        }

        $j = json_decode($result);
        $j->remember_me = $remember_me;

        if ( !self::oauth($j, $provider, $cookie_path) ) {
            log_action('Oauth login failed - missing user from project_users table?',__FILE__,__LINE__);
            return false;
        }

        if (isset($_SESSION['morefilter_query'])) unset($_SESSION['morefilter_query']);
        if (isset($_SESSION['morefilter'])) unset($_SESSION['morefilter']);
        if (isset($_SESSION['filter_type'])) unset($_SESSION['filter_type']);
        if (isset($_SESSION['query_build_string'])) unset($_SESSION['query_build_string']);
        if (isset($_SESSION['upload'])) unset($_SESSION['upload']);
        if (isset($_SESSION['st_col'])) unset($_SESSION['st_col']);

        self::drop_session_temp_tables();

        return true;
    }

    /* It does a compatibility check and can be used for real password expiry check...
     *
     * */
    public static function check_expired_password($user,$password,$uid=false) {
        global $BID;
        require_once(getenv('OB_LIB_DIR').'languages.php');

        $cmd = sprintf('SELECT "id","password","pwstate" FROM "users" WHERE LOWER("email")=%s',quote(strtolower($user)));
        $res = pg_query($BID,$cmd);
        if (!pg_num_rows($res)) {
            return t('str_no_such_user').'!';
        } else {
            $row = pg_fetch_assoc($res);
            if ($row['pwstate']=='t') {
                // verify is not necessery ...  double check - error messages
                if (password_verify($password,$row['password'])) {
                    if ($uid)
                        return $row['id'];
                    return true;
                } else {
                    return t('str_login_error');
                }
            } else {
                return t('str_password_expired');
            }
        }
    }

    /* Oauth
     * result - Ouath json response array
     * */
    public static function oauth($j,$provider,$path) {

        self::set_token_cookies($j,$provider,$path);

        if (self::login_with_access_token($j->{'access_token'}))
            return true;

        return false;

    }

    public static function LostPw($addr) {
        global $BID;
        $post_addr = quote(strtolower($addr));

        require_once(getenv('OB_LIB_DIR').'languages.php');

        $masters = new Role('project masters');
        $emails = $masters->get_member_emails();
        $emails[] = the_project_email();
        array_unique($emails);
        
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        $token = substr( str_shuffle( $chars ), 0, 16 );

        $cmd = "UPDATE users SET reactivate='$token' WHERE LOWER(email)=$post_addr RETURNING \"id\"";
        $res = pg_query($BID,$cmd);

        if(pg_affected_rows($res)) {
            $row = pg_fetch_assoc($res);
            $to = new User($row['id']);
            $M = new Messenger();
            
            $words = [
                'name' => $to->name,
                'token' => $token,
                'addr' => strtolower($addr),
                'emails' => implode(', ',$emails)
            ];
            
            return $M->send_system_message($to, 'lostpw', $words, true);
            //processing return message?
        }
        return 0;
    }
}

?>
