<?php
/* type-1 (url, wkt)
 *      wkt
 *      srid
 *      name
 *
 * type-2 (session)
 *      d
 *
 * type-3 (url, geojson)
 *      geojson
 *
 * */

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once(OB_ROOT_SITE.'server_vars.php.inc');
require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
require_once(getenv('OB_LIB_DIR').'db_funcs.php');

if (session_status() === PHP_SESSION_NONE) {
    session_cache_limiter('nocache');
    session_cache_expire(0);
    session_start();
}

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
include_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

// addgeom toolbox
$addgeom = true;

if (isset($_POST['geojson_post'])) {
    //upload_dynamic_js call
    //itt rakjuk össze az adatot, ami visszejön majd ide
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random_string = substr( str_shuffle( $chars ), 0, 16 );

    $_SESSION['geomtest']=array();
    $_SESSION['geomtest']['id'] = $random_string;
    $_SESSION['geomtest']['srid'] = $_POST['srid'];
    $_SESSION['geomtest']['data'] = $_POST['geojson_post'];
    $_SESSION['geomtest']['utmzone'] = $_POST['utmzone'];

    echo $random_string;
    exit;
}

$input_srid = '4326';
$errlog = "";
$input_geometry = "''";
$external_service = false;

if (isset($_GET['wktsession'])) {
    $_GET['wkt'] = $_SESSION['wktsession'];
}

/* We expecting WKT text or GeoJSON for parsing coordinates
 * This is the normal openbiomaps way */
if (isset($_GET['d']) and isset($_SESSION['geomtest']) and $_GET['d']==$_SESSION['geomtest']['id']) {
    $input_geometry = $_SESSION['geomtest']['data'];
    $input_srid =  $_SESSION['geomtest']['srid'];
    $input_utmzone = $_SESSION['geomtest']['utmzone'];
    if(!is_geojson($input_geometry)){
        $errlog .= "JSON Parse Error";
        $failed_geometry = $input_geometry;
        $geojson_parse = $input_geometry;
        $input_geometry = '{}';
    }

    if ($input_srid == 'UTM' and count($input_utmzone)) {
       if (preg_match('/^(\d{1,2})([a-z])/i',$input_utmzone[0],$m)) {
            // UTM grid zones
            // letters c-f, n-x
            if (strtolower($m[2]) == 's') {
                $input_srid = sprintf('327%02d',$m[1]);
            } elseif (strtolower($m[2]) == 'n') {
                $input_srid = sprintf('326%02d',$m[1]);
            }
        }

    }

    $addgeom = false;
}
/* We can display normal GET input (wkt or geojson) as well*/
else if(isset($_GET['wkt']) and $_GET['wkt']!='') {

    $external_service = true;

    $name = "Something";
    if (isset($_GET['name'])) $name = $_GET['name'];

    // dupla zárójeles stringet nem kezeli!!!
    if(!is_wkt($_GET['wkt'])) {
        $errlog .= "WKT Parse Error";
        $input_geometry = "''";//$_GET['wkt'];
        $failed_geometry = $_GET['wkt'];
    }
    else {
        $input_geometry = wkt_to_json($_GET['wkt']);
        $geojson = json_decode($input_geometry,true);
        #{
        #  "type": "Feature",
        #  "geometry": {
        #    "type": "Point",
        #    "coordinates": [125.6, 10.1]
        #  },
        #  "properties": {
        #    "name": "Dinagat Islands"
        #  }
        #}
        #{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[18.324707031553004,47.21072318701318],[18.445556640911015,47.5751526532584],[19.225585937677675,47.411842993008],[19.40136718765248,47.11361470808899],[19.335449218912153,46.75352215615761],[18.522460937774877,46.768574359974494],[18.072021484712543,46.557460775005204],[18.12695312533023,46.963870389447926],[17.984130859725145,47.158456062472325],[17.96215820347807,47.37465632407284],[18.324707031553004,47.21072318701318]]]},"properties":{"name":"Ember"}}],"crs":{"type":"name","properties":{"name":"EPSG:4326"}}}
        #
        $geojson = array("type"=>"FeatureCollection",
            "features"=>array(
                array("type"=>"Feature",
                      "geometry"=>array(
                          "type"=>$geojson['type'],
                          "coordinates"=>$geojson['coordinates']),
                      "properties"=>array("name"=>"$name"))),
                "crs"=>array("type"=>"name",
                             "properties"=>array("name"=>"EPSG:$input_srid")));

        $input_geometry = json_encode($geojson);
        $geojson_parse = $input_geometry;
    }
    // only for displaying a wkt geometry - used in results_asPDF module
    if (isset($_GET['showonly'])) {
        $addgeom = false;
        $external_service = false;
    }
} elseif(isset($_GET['geojson']) and $_GET['geojson']!='') {
    
    $external_service = true;

    if (is_geojson($_GET['geojson']))
        $input_geometry = $_GET['geojson'];
    else {
        $errlog .= "JSON Parse Error";
        $failed_geometry = $_GET['geojson'];
        $geojson_parse = $input_geometry;
        $input_geometry = '{}';
    }
} else if(isset($_GET['wkt'])) {
    // empty wkt
    $external_service = true;

} else if(isset($_GET['geojson'])) {
    // empty wkt
    $external_service = true;

} else if(isset($_GET['osm'])) {
    $external_service = true;
    $coordinates = getCoordinatesFromNominatim($_GET['osm']);
    if ($coordinates) {
        $geojson_parse = json_encode($coordinates);
        $input_geometry = json_encode($coordinates);
    }
}


// processing srid from url parameters
if (isset($_GET['srid']) and is_numeric($_GET['srid'])) 
    $input_srid = $_GET['srid'];

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>OpenBioMaps - GEOMETRY TEST</title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $protocol ?>://<?php echo URL ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-16x16.png">
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/mapsdata.css?rev=2" media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>

    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/style.css?rev=<?php echo rev('.'.STYLE_PATH.'/style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/colors.css.php?rev=<?php echo rev('.'.STYLE_PATH.'/colors.css.php'); ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/font-awesome/css/font-awesome.min.css?rev=<?php echo rev('css/font-awesome/css/font-awesome.min.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />

    <!--OpenLayers-->
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/openlayers_v6.14.1_build_ol.js"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/openlayers_v6.14.1_css_ol.css">
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/ol-layerswitcher.css" type="text/css" />
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/ol-layerswitcher.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.8.0/proj4.min.js"></script>
<?php
    ## get google api key from:
    ## https://console.developers.google.com/apis/
    /* Currently not supported
    if ( file_exists( getenv('OB_LIB_DIR').'local/mapserver/private_map_js.php' ) )
        include_once( getenv('OB_LIB_DIR').'local/mapserver/private_map_js.php' );
    else
        echo "<script async defer src='$protocol://maps.google.com/maps/api/js?v=3.5'></script>";
     */

?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.awesome-cursor.min.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js?rev=<?php echo rev('js/jquery.cookie.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/map_functions.js?rev=<?php echo rev('js/maps.js'); ?>"></script>
    <script type="text/javascript">
    var point, zoom, map, ExampleDraw, drawLayer, inputProj ='EPSG:<?php echo $input_srid ?>', outputProj = 'EPSG:3857';

    function init() {
        const mapbbox = [-180.0000, -85.0000, 180.0000, 85.0000];
        let mapExtent = ol.proj.transformExtent(mapbbox, 'EPSG:4326', 'EPSG:3857');
        /*const projection = new ol.proj.Projection({
            code: 'EPSG:3857',
            extent: extent,
        });*/
        const image = new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: 'rgba(194, 0, 204, 0.8)',
            }),
            stroke: new ol.style.Stroke({color: '#333333', width: 2}),
        });
        const styles = {
          'Point': new ol.style.Style({
            image: image,
          }),
          'LineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'green',
              width: 3,
            }),
          }),
          'MultiLineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'green',
              width: 3,
            }),
          }),
          'MultiPoint': new ol.style.Style({
            image: image,
          }),
          'MultiPolygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'yellow',
              width: 3,
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 0, 0.1)',
            }),
          }),
          'Polygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'blue',
              lineDash: [4],
              width: 3,
            }),
            fill: new ol.style.Fill({
              color: 'rgba(0, 0, 255, 0.1)',
            }),
          }),
          'GeometryCollection': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'magenta',
              width: 3,
            }),
            fill: new ol.style.Fill({
              color: 'magenta',
            }),
            image: new ol.style.Circle({
              radius: 10,
              fill: null,
              stroke: new ol.style.Stroke({
                color: 'magenta',
              }),
            }),
          }),
          'Circle': new ol.style.Style({
            stroke: new ol.style.Stroke({
              color: 'red',
              width: 3,
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255,0,0,0.2)',
            }),
          }),
        };

        const styleFunction = function (feature) {
          return styles[feature.getGeometry().getType()];
        };        

        drawLayer = new ol.layer.Vector({
            name: 'drawLayer',
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)',
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    width: 5,
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33',
                    }),
                }),
            }),
        });

        let featurecollection = <?php echo $input_geometry."\n"; ?>
        /* crs auto detection from geojson....?
            * 
            object.crs =  {
              "type": "name",
              "properties": {
                  "name": "epsg:3857"
              }
            }
            *
            *
            * */
        let geojson_format = new ol.format.GeoJSON();
    
        let geojsonsource = new ol.source.Vector();


        if (inputProj == 'EPSG:23700') {
            proj4.defs(inputProj, "+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +no_defs");
            new ol.proj.proj4.register(proj4);
        }

        if (featurecollection != "") {
            let features = geojson_format.readFeatures(featurecollection,{
                'dataProjection': inputProj,
                'featureProjection': outputProj,
            });
            geojsonsource = new ol.source.Vector({features: features});
        } 

        const geojsonLayer = new ol.layer.Vector({
            name: "Data test",
            source: geojsonsource,
            style: styleFunction,
        });

        const tlayers = [];
        const base_layers_group = [];
        let i = 0;

        Object.keys(obj).forEach(function (key) {
            if (title=key.match(/^layer_tile_([a-zA-Z0-9-_]*)/)) {
                tl = new Array();
                eval("tl = " + obj[key] + ";");
                const order = Number(title[1].split('_').pop());

                base_layers_group.push(new ol.layer.Tile({
                    title: title[1].toUpperCase(),
                    type: 'base',
                    visible: (isNaN(order)) ? (i === 0) : (order === 1),
                    extent: mapExtent,
                    source: tl,
                }));
                i++;
            }
        })

        const layers = [
            new ol.layer.Group({
                title: 'Base maps',
                layers: base_layers_group
            }),
            drawLayer,
            geojsonLayer,
         ];
        
        const extendes = [
            new ol.control.ZoomToExtent({
              extent: [
                1792384, 5942120.28, 1838966.96, 5916863.98],
            }),
            new ol.control.ScaleLine(),
            new ol.control.MousePosition({
                coordinateFormat: new ol.coordinate.createStringXY(4),
                projection: inputProj,
                //className: 'custom-mouse-position',
                target: document.getElementById('mouse-position'),
            }),
            new ol.control.OverviewMap({
              className: 'ol-overviewmap ol-custom-overviewmap',
              layers: [
                new ol.layer.Tile({
                  source: new ol.source.OSM(),
                }),
              ],
            }),
            new ol.control.LayerSwitcher({
                tipLabel: 'Layers', // Optional label for button
                groupSelectStyle: 'children' // Can be 'children' [default], 'group' or 'none'
            })
        ];

        map = new ol.Map({
            controls: new ol.control.defaults().extend(extendes),
            interactions: new ol.interaction.defaults({
                    mouseWheelZoom: true
                }),
            layers: layers,
            target: 'map',
            view: new ol.View({
                center: ol.proj.fromLonLat([22.2, 49.2], "EPSG:4326"),
                projection: outputProj,
                extent: mapExtent,
                zoom: 5,
            }), 
        });

        if (featurecollection != "") {
            
            //var features = geojson_format.readFeatures(featurecollection);
            //geojsonsource = new ol.source.Vector({features: features});

            //geojsonLayer.setSource( geojsonsource );
            //geojsonLayer.setStyle( styleFunction );
            let extent = geojsonLayer.getSource().getExtent();

            if(extent[0] == extent[2]) {
                extent = ol.proj.transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
                map.getView().setCenter( new ol.proj.fromLonLat([extent[0],extent[1]]) ); // transform to default EPSG:3857
                map.getView().setZoom(14);
            } else {
                map.getView().fit(new ol.geom.Polygon.fromExtent(extent),{padding: [20, 20, 20, 20]}); 
            }
        } else {
            if (typeof $.cookie('lastZoom')!=='undefined') {
                try {
                    v = JSON.parse($.cookie('lastZoom'));
                    point = [v.center[0], v.center[1]];    
                    zoom = v.zoom;
                } catch (e) {}
            } else {
                point = new ol.proj.fromLonLat([22.2, 49.2]); // transform to default EPSG:3857
                zoom = 5;
            }
            map.getView().setCenter(point);
            map.getView().setZoom(zoom);
        }
        
    let select_interaction = new ol.interaction.Select({
        // make sure only the desired layer can be selected
        layers: function(drawLayer) {
            return drawLayer.get('name') === 'drawLayer';
        }
    });
    let modify_interaction = new ol.interaction.Modify({
        source: drawLayer.getSource(),
    });

    map.addInteraction(modify_interaction);
    map.addInteraction(select_interaction);

    modify_interaction.on('modifyend',function(e){
        saveData_();
    });

    this.drawLayer.getSource().on('addfeature', function(event){
        //var geom = event.feature.getGeometry();
        //drawLayer.getSource().clear();
        let this_feature = event.feature;
        let all_features = drawLayer.getSource();
        drawLayer.getSource().getFeatures().forEach(function(feature) {
            let selected_feature = feature.getGeometry();
            if (selected_feature != this_feature.getGeometry()) {
                all_features.removeFeature(feature);
            }
        });
        saveData_();
    });
    // get the features from the select interaction
    let selected_features = select_interaction.getFeatures();
    // when a feature is selected...
    selected_features.on('add', function(event) {
        // get the feature
        let feature = event.element;
        // ...listen for changes on it
        //feature.on('change', saveData);

        $(document).on('keyup', function(event) {
            if (event.keyCode == 46) {
                // remove all selected features from select_interaction and my_vectorlayer
                selected_features.forEach(function(selected_feature) {
                  let selected_feature_id = selected_feature.getGeometry();
                  // remove from select_interaction
                  selected_features.remove(selected_feature);
                  // features aus vectorlayer entfernen
                  let vectorlayer_features = drawLayer.getSource().getFeatures();
                  vectorlayer_features.forEach(function(source_feature) {
                    let source_feature_id = source_feature.getGeometry();
                    if (source_feature_id === selected_feature_id) {
                      // remove from my_vectorlayer
                      drawLayer.getSource().removeFeature(source_feature);
                      // save the changed data
                      saveData_();
                    }
                  });
                });
                // remove listener
                $(document).off('keyup');
            }
        });
    });
    
    ExampleModify = {
    init: function () {
        this.select = new ol.interaction.Select();
        map.addInteraction(this.select);

        this.modify = new ol.interaction.Modify({
          features: this.select.getFeatures(),
        });
        map.addInteraction(this.modify);

        this.setEvents();
      },
      setEvents: function () {
        const selectedFeatures = this.select.getFeatures();

        this.select.on('change:active', function () {
          selectedFeatures.forEach(function (each) {
            selectedFeatures.remove(each);
          });
        });
      },
      setActive: function (active) {
        this.select.setActive(active);
        this.modify.setActive(active);
      },
    };
    ExampleModify.init();

    // Draw on map
    ExampleDraw = {
      init: function () {
        map.addInteraction(this.Point);
        this.Point.setActive(false);
        map.addInteraction(this.LineString);
        this.LineString.setActive(false);
        map.addInteraction(this.Polygon);
        this.Polygon.setActive(false);
        map.addInteraction(this.Circle);
        this.Circle.setActive(false);
      },
      Point: new ol.interaction.Draw({
        source: drawLayer.getSource(),
        type: 'Point',
      }),
      LineString: new ol.interaction.Draw({
        source: drawLayer.getSource(),
        type: 'LineString',
      }),
      Polygon: new ol.interaction.Draw({
        source: drawLayer.getSource(),
        type: 'Polygon',
      }),
      Circle: new ol.interaction.Draw({
        source: drawLayer.getSource(),
        type: 'Circle',
      }),
      activeDraw: null,
      setActive: function (active) {
        if (this.activeDraw) {
          this.activeDraw.setActive(false);
          this.activeDraw = null;
        }
        if (active) {
          var type = $('input[name=draw-type]:checked', '#map-tools-box').val();
          if (typeof type === 'undefined') {
            type = 'Polygon'
          } 
          this.activeDraw = this[type];
          this.activeDraw.setActive(true);
        }
      },
    };
    ExampleDraw.init();
    const snap = new ol.interaction.Snap({
      source: drawLayer.getSource(),
    });
    map.addInteraction(snap);


    // Sketch complete
};
function toggleControl_(element) {

    if (element.value == 'featureEditor' && !$('#editToggle').is(':checked')) { 
        ExampleDraw.setActive(true);
        ExampleModify.setActive(false);
        $('.map').awesomeCursor('pencil',{
            hotspot: 'bottom left',
        });
    } else if (element.value == 'featureEditor' && $('#editToggle').is(':checked')) {
        let type = $('input[name=draw-type]:checked', '#map-tools-box').val();
        if (typeof type === 'undefined') {
            $('#editToggle').prop('checked', false);
            return false;
        }
        $('.map').awesomeCursor('hand-lizard-o',{
            hotspot: 'center left',
        });
        ExampleDraw.setActive(false);
        ExampleModify.setActive(true);
    } else {

        drawLayer.getSource().clear();
        $('.map').awesomeCursor('pencil',{
            hotspot: 'bottom left',
        });
        $('#editToggle').prop('checked', false);
        ExampleDraw.setActive(true);
        ExampleModify.setActive(false);
    }
}
/* Add new data point related function
 * geomtest.php <- upload-show-table-form.php
 * */
function saveData_(event) {
    //SAVE last zoom position...
    let v = {'center':map.getView().getCenter(),'zoom':map.getView().getZoom()};
    $.cookie('lastZoom',JSON.stringify(v));
    //highlightLayer.destroyFeatures();
    
    /* Select tools: point, line, polygon
     * create buffers around selection */
    if ($('#buffer').length > 0) {
        b = $("#buffer").val();
    } else { 
        b = 0; 
    }

    var format = new ol.format.WKT();

    src = 'EPSG:3857';
    dest = 'EPSG:4326';

    var features = drawLayer.getSource().getFeatures();
    //drawLayer.getSource().clear();

    var wktRepresenation = []
    features.forEach(function(element) {

        var type = element.getGeometry().getType();
        if (type == 'Circle') {
            var circlepolygon = new ol.geom.Polygon.fromCircle(element.getGeometry().clone());
            wktRepresenation.push(format.writeGeometry(circlepolygon.transform(src,dest)));
        } else {
            wktRepresenation.push(format.writeGeometry(element.getGeometry().clone().transform(src,dest)));
        }
    });

<?php 
    echo "$('#wkt').html(wktRepresenation);";  
?>

    //this send back the coordinates to the upload process if it was called from a form's geometry cell
    if (window.opener) {
        $('#matrix').find('#flyingadd').remove();
        $('#matrix').append("<div id='flyingadd' style='display:contents;'><button id='setValue' class='pure-button button-warning' value='" + wktRepresenation + "' style='-webkit-box-shadow: 0px 0px 0px 3px rgba(255,255,255,0.50);-moz-box-shadow: 0px 0px 0px 3px rgba(255,255,255,0.50);box-shadow: 0px 0px 0px 3px rgba(255,255,255,0.50);'><?php echo t(str_save) ?></button><br><span style='display:none'>" + wktRepresenation + "</span></div>");
    }
}

</script>
</head>
<body onload='init();'>
<?php
if (count($_GET)==0 or (count($_GET)==1 and isset($_GET['path']) and $_GET['path'] == 'geomtest/')) {

    echo "<div style='position:absolute;top:0;left:25%;width:50%;z-index:100;background-color:white;padding:20px;margin:2px;border:1px solid gray'>
        <center><h1>GeoJSON and WKT testing interface</h1>
        <h3>This is a public OpenBioMaps service.</h3></center>
        <br>
        <b>Usage:</b><br>
        <a href='?wkt=POINT(21.13 47.23)'>?wkt=POINT(21.13 47.23)</a><br>
        <a href='?geojson={\"type\": \"Point\", \"coordinates\": [21.13, 47.45] }'>?geojson={\"type\": \"Point\", \"coordinates\": [21.13, 47.45] }</a></div>";

}

if ($errlog!='') {

    echo "<h3>$errlog</h3><div style='border:2px solid gray;padding:4px;width:auto;margin:4px'>";
    
    if (isset($geojson_parse) and $geojson_parse!="''" and format_json($geojson_parse)!='')
        echo format_json($geojson_parse,$html=true);
    else
        echo $failed_geometry;

    echo "</div>";
    echo "<br><br> <a href='https://en.wikipedia.org/wiki/Well-known_text' target='blank'>Well-known text on Wikipedia</a>";
    echo "<br><br> <a href='http://geojsonlint.com/' target='blank'>GeoJSON tester</a>";
    echo "<br><br> <a href='http://geojson.org/geojson-spec.html#id4' target='blank'>GeoJSON format specification</a>";

} else {

?>
<div id='map' class='map' style='position: absolute; right: 0px; top: 0px; width: 100%; height: 100%'></div>
<?php
    if ($external_service) {

        if (isset($_GET['wkt'])) {
            $service = 'wkt';
            $ph = 'POINT(21.13 47.23)';
        }
        elseif (isset($_GET['geojson'])) {
            $service = 'geojson';
            $ph = '{"type": "Point", "coordinates": [21.13, 47.45] }';
        }
        elseif (isset($_GET['osm'])) {
            $service = 'osm';
            $ph = 'Budapest';
        }

        echo "<form id='matrix' method='get' style='position:absolute;left:9px;bottom:40px;z-index:100;opacity:0.8'>
            <a href='?addgeom&osm=&srid=4326'>osm</a> <a href='?addgeom&wkt=&srid=4326'>wkt</a> <a href='?addgeom&geojson=&srid=4326'>geojson</a><br>
            <textarea style='font-size:90%;width:30rem;height:7rem;border-radius:4px' id='wkt' name='$service' placeholder='$ph'>";
        if (isset($_GET['wkt'])) echo $_GET['wkt'];
        elseif (isset($_GET['geojson'])) echo $_GET['geojson'];
        echo "</textarea><br><button class='pure-button button-success' type='submit' value='".t(str_show)."'><i class='fa fa-map-marker'></i></button></form>";
    }

    // upload process
    if ($addgeom) {
        //$buff = 0;
        $out = "<div id='map-tools-box' style='position:absolute;bottom:24px;right:0px;z-index:102;background-color:#FFFFFF;opacity:0.8;padding:3px;border-radius:4px'>". t(str_add_features).":&nbsp; 
    <label style='float:right'>
      <input type='checkbox' name='editmode' value='featureEditor' id='editToggle' class='maphands' onclick='toggleControl_(this);' />
      <span title='Edit drawn features' style='display:block;background-image:url($protocol://".URL."/css/img/edit_feature_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
    </label>   
    <label style='float:right'>
      <input type='radio' name='draw-type' value='Point' id='pointToggle' onclick='toggleControl_(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_point_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
    </label>
    <label style='float:right'>
      <input type='radio' name='draw-type' value='LineString' id='lineToggle' onclick='toggleControl_(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_line_onoff.png);width:24px;height:22px;background-repeat: no-repeat;margin-right:2px'></span>
    </label>
    <label style='float:right'>
      <input type='radio' name='draw-type' value='Polygon' id='polygonToggle' onclick='toggleControl_(this);' />
      <span style='display:block;background-image:url($protocol://".URL."/css/img/add_polygon_onoff.png);width:24px;height:22px;background-repeat: no-repeat;margin-right:2px'></span>
    </label>";
        $out .= "</div>";
        /*$out .= "<div style='position:absolute;left:100px;bottom:20px' class='pure-form'>
            ".str_select_buffer_size.": <input style='vertical-align:middle' class='pure-input' type='range' id='bufferslide' min='0' max='1000' value='$buff'><input class='pure-input-rounded' id='buffer' value='$buff' style='width:5em;'> ".str_meter."</div>";*/
        //$out .= "<div id='matrix' style='position:absolute;top:5px;left:30px;'></div>";
        echo $out;
    }
}
?>
</body>
</html>

