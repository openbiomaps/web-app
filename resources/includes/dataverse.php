<?php
class dataverse extends repository {
    /*  */
    public $project_dataverse;

    function __construct($D) {
        if (!defined('PROJECT_REPO')) {
            $this->project_dataverse = false;
            return;
        }

        $rc = constant('PROJECT_REPO');
        
        $this->project_dataverse = (isset($rc[$D]["PROJECT_DATAVERSE"])) ? $rc[$D]["PROJECT_DATAVERSE"] : false;
    }

    # Summary function for datasets
    #
    public function get_datasets($SERVER_URL, $API_TOKEN) {
        
        $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");
        $rest = "/api/dataverses/{$this->project_dataverse}/contents";
        // get a dataverse
        $result = wget($SERVER_URL.$rest,$header);
        $datasets = array();
        if (is_json($result)) {
            $j = json_decode($result,true);
            if ($j['status'] == 'OK') {

                // get each datasets
                foreach ($j['data'] as $jdata) {

                    if ($jdata['type'] == 'dataset') {
                        $rest = $rest = "/api/datasets/:persistentId/?persistentId=doi:".$jdata['authority']."/".$jdata['identifier'];    
                        $r2 = wget($SERVER_URL.$rest,$header);

                        if (is_json($r2)) {

                            $jd = json_decode($r2,true);

                            if ($jd['status'] == 'OK') {
                                $id = $jd['data']['id'];
                                $datasets[$id] = $jd['data'];
                            }
                        }
                    }
                }
            }
        }
        return $datasets;
    }

    private function is_doi($string) {
        //doi:10.48428/ADATTAR/GZBJD9
        $m = array();
        $doi = false;
        if (preg_match('/(doi:.+)$/',$string,$m)) {
            $doi = $m[1];
        }
        return $doi;
    }

    # Get all information about one specific dataset
    #
    public function get_dataset($SERVER_URL, $API_TOKEN, $dataset_id) {
        
        $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");

        $dataset = null;

        if ( $doi = $this->is_doi($dataset_id)) {
            $rest = $rest = "/api/datasets/:persistentId/?persistentId=".$doi;    
        } else {
            $rest = $rest = "/api/datasets/".$dataset_id;
        }
        $r2 = wget($SERVER_URL.$rest,$header);
        
        if (is_json($r2)) {
            $jd = json_decode($r2,true);
            if ($jd['status'] == 'OK') {
                $dataset = $jd['data'];
            } else {
                // error handling
                return common_message('error', $jd);
            }
        } else {
            // error handling
            return common_message('error','Malformed data output');
        }

        return common_message('ok', $dataset);
    }

    # Get specific attributes from datasets
    # currently supported attributes are
    #   title id doi version
    #
    public function get_dataset_attribute($dataset, $attribute) {

        $a = false;
        
        if ($attribute == 'title')
            $a = $dataset['latestVersion']['metadataBlocks']['citation']['fields'][0]['value'];
        else if ($attribute == 'id')
            $a = $dataset['id'];
        else if ($attribute == 'doi')
            $a = $dataset['latestVersion']['datasetPersistentId'];
        else if ($attribute == 'version')
            $a = $dataset['latestVersion']['versionState'];
        
        return $a;
    }

    # Publish a dataset
    #
    public function publish_dataset($DOI,$TYPE,$SERVER_URL,$API_TOKEN) {
        
        $header = array("X-Dataverse-key:$API_TOKEN");            
        $postFields = array();

        if (isset($params['persistentId'])) {
            $majorminor = ($TYPE!='') ? $TYPE : 'major';
            $DOI = preg_replace('/^https:\/\/doi\.org\//','',$DOI);
            $rest = sprintf("/api/datasets/:persistentId/actions/:publish?persistentId=%s&type=%s", $DOI, $majorminor);
        }

        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
            $response = curl_exec($curl);
            curl_close($curl);
            if (is_json($response))
                echo common_message('ok',$response);
            else
                echo common_message('error','Malformed data output');
        } else {
            echo common_message('error','Curl error');
        }
        return;
    }

    # Create a new dataset
    #
    public function new_dataset($f,$DATAVERSE,$SERVER_URL,$API_TOKEN) {
        
        $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");

        $meta_string = '{"datasetVersion":{"metadataBlocks":{"citation":{"fields":[{"value":"","typeClass":"primitive","multiple":false,"typeName":"title"},{"value":[{"authorName":{"value":"","typeClass":"primitive","multiple":false,"typeName":"authorName"},"authorAffiliation":{"value":"","typeClass":"primitive","multiple":false,"typeName":"authorAffiliation"}}],"typeClass":"compound","multiple":true,"typeName":"author"},{"value":[{"datasetContactEmail":{"typeClass":"primitive","multiple":false,"typeName":"datasetContactEmail","value":""},"datasetContactName":{"typeClass":"primitive","multiple":false,"typeName":"datasetContactName","value":""}}],"typeClass":"compound","multiple":true,"typeName":"datasetContact"},{"value":[{"dsDescriptionValue":{"value":"","multiple":false,"typeClass":"primitive","typeName":"dsDescriptionValue"}}],"typeClass":"compound","multiple":true,"typeName":"dsDescription"},{"value":[""],"typeClass":"controlledVocabulary","multiple":true,"typeName":"subject"}],"displayName":"Citation Metadata"}}}}';
        $meta = json_decode($meta_string, true);

        if (isset($f['ds-dataset']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][0]['value'] = $f['ds-dataset'];
        if (isset($f['ds-author']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][1]['value'][0]['authorName']['value'] = $f['ds-author'];
        if (isset($f['ds-authorAffiliation']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][1]['value'][0]['authorAffiliation']['value'] = $f['ds-authorAffiliation'];
        if (isset($f['ds-contactName']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][2]['value'][0]['datasetContactName']['value'] = $f['ds-contactName'];
        if (isset($f['ds-email']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][2]['value'][0]['datasetContactEmail']['value'] = $f['ds-email'];
        if (isset($f['ds-description']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][3]['value'][0]['dsDescriptionValue']['value'] = $f['ds-description'];
        if (isset($f['ds-subject']))
            $meta['datasetVersion']['metadataBlocks']['citation']['fields'][4]['value'] = $f['ds-subject'];

        $tmp = tmpfile();
        // write out meta content into tmp files
        fwrite($tmp,json_encode($meta, JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fseek($tmp,0);
        // get tmp file path
        $tmpfile = stream_get_meta_data($tmp)['uri'];

        #$dataset_id = null;
        $dataset = null;
        #$doi = null;

        $rest = "/api/dataverses/".$DATAVERSE."/datasets";
        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); // pseudo post
            curl_setopt($curl, CURLOPT_PUT, true);
            $fh_res = fopen($tmpfile, 'r');
            curl_setopt($curl, CURLOPT_INFILE, $fh_res);
            curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpfile));
            $response = curl_exec($curl);
            curl_close($curl);
            fclose($fh_res);
            if (is_json($response)) {
                $j = json_decode($response,true);
                if ($j['status'] == 'OK') {
                    # {"id": 159, "persistentId": "doi:10.48428\/ADATTAR\/D7MEBK"}
                    #$dataset_id = $j['data']['id'];
                    $doi = $j['data']['persistentId'];
                    #$versionState = 'DRAFT';   
                    
                    $rest = $rest = "/api/datasets/:persistentId/?persistentId=".$doi;
                    $r2 = wget($SERVER_URL.$rest,$header);
                    if (is_json($r2)) {
                        $j2 = json_decode($r2,true);
                        if ($j2['status'] == 'OK') {
                            $dataset = $j2['data'];
                            fclose($tmp); // this removes the tmpfile
                            return common_message('ok',$dataset);
                        } else {
                            // File created but not accessible
                            fclose($tmp); // this removes the tmpfile
                            return $j2;
                        }
                    } else {
                        // File created but not accessible
                        fclose($tmp); // this removes the tmpfile
                        return common_message('error','File created, but not accessible...');
                    }

                } else {
                    fclose($tmp); // this removes the tmpfile
                    return $response;
                }
            } else {
                fclose($tmp); // this removes the tmpfile
                return common_message('error','Malformed data output');
            }
        } else {
            fclose($tmp); // this removes the tmpfile
            return common_message('error','Curl error');
        }
    }

    # Add files to a dataset
    #
    public function file_add($FILE_CONTENT,$FILE_NAME,$DATASET_ID, $DESCRIPTION, $SERVER_URL,$API_TOKEN) {
        $postFields = array();

        $meta = array(
                "description"=>$DESCRIPTION,
                "directoryLabel"=>"",
                "categories"=>array("Data","obm_unique_id"),
                "restrict"=>"false");
        
        $postFields['jsonData'] = json_encode($meta, JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);


        $tmp = tmpfile();
        fwrite($tmp,$FILE_CONTENT);
        fseek($tmp,0);
        $tmpfile = stream_get_meta_data($tmp)['uri'];
        $postFields['file'] =  curl_file_create($tmpfile, 'application/json', $FILE_NAME);

        if (is_numeric($DATASET_ID))
            $rest = sprintf("/api/datasets/%s/add",$DATASET_ID);
        else {
            $DATASET_ID = preg_replace('/^https:\/\/doi\.org\//','',$DATASET_ID);
            $rest = "/api/datasets/:persistentId/add?persistentId=$DATASET_ID";
        }

        $header = array("X-Dataverse-key:$API_TOKEN"); // overwrite header

        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
            $response = curl_exec($curl);
            curl_close($curl);
            
            if (is_json($response)) {
                fclose($tmp); // this removes the tmpfile
                return($response);
            }
            else
                echo common_message('error','Malformed data output');
        } else {
            echo common_message('error','Curl error');
        }

        fclose($tmp); // this removes the tmpfile
        return;
    }

    # Replace a file in a dataset
    #
    public function file_replace($FILE_CONTENT,$FILE_NAME,$DATASET_ID, $DESCRIPTION, $SERVER_URL,$API_TOKEN) {
        $postFields = array();

        $meta = array(
                "description"=>$DESCRIPTION,
                "directoryLabel"=>"",
                "categories"=>array("Data","obm_unique_id"),
                "restrict"=>"false");
        
        $postFields['jsonData'] = json_encode($meta, JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);


        $tmp = tmpfile();
        fwrite($tmp,$FILE_CONTENT);
        fseek($tmp,0);
        $tmpfile = stream_get_meta_data($tmp)['uri'];
        $postFields['file'] =  curl_file_create($tmpfile, 'application/json', $FILE_NAME);

        if (is_numeric($DATASET_ID))
            $rest = sprintf("/api/datasets/%s/replace",$DATASET_ID);
        else {
            $DATASET_ID = preg_replace('/^https:\/\/doi\.org\//','',$DATASET_ID);
            $rest = "/api/datasets/:persistentId/replace?persistentId=$DATASET_ID";
        }

        $header = array("X-Dataverse-key:$API_TOKEN"); // overwrite header

        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
            $response = curl_exec($curl);
            curl_close($curl);
            
            if (is_json($response)) {
                fclose($tmp); // this removes the tmpfile
                return($response);
            }
            else
                echo common_message('error','Malformed data output');
        } else {
            echo common_message('error','Curl error');
        }

        fclose($tmp); // this removes the tmpfile
        return;
    }

    # Delete a file from a dataset
    #
    public function file_drop($FILE_ID, $OBM_UID, $SERVER_URL, $API_TOKEN) {

        $header = array("X-Dataverse-key:$API_TOKEN");
        $rest = "/api/files/$FILE_ID/metadata/draft"; # Mi van a published datasettel, ott nem draft a vége, külön le kellene vizsgálni...
        $response = wget($SERVER_URL.$rest,$header);
        if (is_json($response)) {
            # {"label":"Estonia air.json","description":"11@8IRMsyZPoaD0SfJL","restricted":false,"categories":["Data","obm_unique_id"],"id":278}
        
            $j = json_decode($response,true);

            if (isset($j['id'])) {

                if (isset($j['description']) and $j['description'] == $OBM_UID) {

                    $rest = sprintf("/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/%s", $FILE_ID);
                    $curl = curl_init();
                    if ($curl) {
                        curl_setopt($curl, CURLOPT_URL, $SERVER_URL.$rest);
                        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                        curl_setopt($curl, CURLOPT_USERPWD, "$API_TOKEN:");
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                        $response = curl_exec($curl);
                        curl_close($curl);
                        if ($response == '')
                            return common_message('ok',$response);
                        else
                            return common_message('error',$response);
                    } else {
                        return common_message('error','Curl error');
                    }
                } else {
                    return common_message('error','Access denied by OBM');
                }
            } else {
                return $response;
            }
        } else {
            return common_message('error','File check error');
        }
    }

    # Looking for a child dataverse in a dataverse
    #
    public function dataverse_search($SERVER_URL,$API_TOKEN,$PROJECT_DATAVERSE,$PARENT_DATAVERSE) {
    
        $header = array("Content-Type:application/json", "X-Dataverse-key:$API_TOKEN");            
        $allowed = 0;
        $curl = curl_init();
        if ($curl) {
            curl_setopt($curl, CURLOPT_URL, $SERVER_URL."/api/dataverses/$PROJECT_DATAVERSE/contents");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            $result = curl_exec($curl);
            curl_close($curl);

            $j = json_decode($result,true);
            $jd = $j['data'];
            for($i = 0; $i < count($jd); $i++) {
                if ($jd[$i]['type'] == 'dataverse') {
                    if ($jd[$i]['title'] == $PARENT_DATAVERSE) {
                        $allowed = 1;
                    } else {
                        $allowed = dataverse_search($SERVER_URL,$API_TOKEN,$jd[$i]['title'],$PARENT_DATAVERSE);
                    }
                }
            }
        }
        return $allowed;
    }

}

?>
