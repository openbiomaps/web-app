<?php
/* This is loading an admin page 
 * Called from profile.php
 *
 *
 * */
require_once(getenv('OB_LIB_DIR').'db_funcs.php');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'languages.php');
require_once(getenv('OB_LIB_DIR').'taxon.php');
require_once(getenv('OB_LIB_DIR').'auth.php');
require_once(getenv('OB_LIB_DIR').'prepare_auth.php');

if (isset($_SESSION['token']) and $_SESSION['token']['projecttable']!=PROJECTTABLE) {
    echo "Stranger database connection.";
    include(getenv('OB_LIB_DIR').'logout.php');
    exit;
}

if (!isset($_SESSION['st_col']))
    st_col('default_table');


$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

//only for logined users
if(!isset($_SESSION['Tid'])) {
    echo "Session expired.";
    include(getenv('PROJECT_DIR').'includes/logout.php');
    exit;
}

$mtable = isset($_GET['mtable']) ? $_GET['mtable'] : PROJECTTABLE;

$modules = new modules($mtable);
$x_modules = new x_modules();

// project roles into array
$cmd = "SELECT role_id,description,ARRAY_TO_STRING(container,',') AS roles,user_id FROM project_roles WHERE project_table='".PROJECTTABLE."' ORDER BY description";
$res = pg_query($BID,$cmd);
$csor = pg_fetch_all($res);

if (!isset($_GET['options'])) {
    echo "Project admin page";
    exit;
}

/* admin page list */
$admin_pages = array_values(array_filter(get_admin_pages_list(), function ($p) {
    return $p['group'] === 'project_admin' && !is_null($p['filename']);
}));

// project module page hack
if ($_GET['options'] == 'project_modules') {
    $admin_pages[] = array(
        "label" => str_modules,
        "page" => "project_modules",
        "group" => "project_admin",
        "filename" => "project_modules"
    );
}

$idx = array_search($_GET['options'], array_column($admin_pages, 'page'));

# Generalized loading of admin pages
if ($idx !== false) {
    $page = $admin_pages[$idx];
    
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }
    if (isset($_GET['update']))
        include("admin_pages/{$page['filename']}.php");
    else {
        echo "<h2>".$page['label']."</h2>
        <div class='be'>";
        include("admin_pages/{$page['filename']}.php");
        echo "</div>";
    }
}
# Jobs - job management - background jobs - háttérfolyamatok
elseif ($_GET['options']=='jobs') {
    if (!has_access($_GET['options'])) {
        echo str_no_access_to_this_function."!";
        return;
    }
    
    echo "<h2>".t(str_jobs)."</h2>";
    echo "<div class='be'>";
    require_once('admin_pages/jobs.php');
    $p = new jobs_manager();
    if ($p->error != '') {
        echo "Some errors occured; see the logs";
        #$p->displayError();
    }
    echo $p->adminPage();
    echo "</div>";
} 
# Module's admin pages - cog icon - table modules
elseif (in_array($_GET['options'], $modules->which_has_method('adminPage'))) {
    
    echo $modules->_include($_GET['options'], 'adminPage',['mtable' => $mtable]);
    
}
# Module's admin pages - cog icon - project modules
elseif (in_array($_GET['options'], $x_modules->which_has_method('adminPage'))) {
    
    echo $x_modules->_include($_GET['options'], 'adminPage',['mtable' => PROJECTTABLE]);
    
}


// An admin script tag...
// maybe it's not necessary
// echo "<script> $('body').trigger({ type: 'profile_page_loaded', page: '{$_GET['options']}' }); </script>";
?>
