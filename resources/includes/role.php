<?php 
/**
 * Project role object
 */
class Role {
    public $role;
    private $role_exists = false;
    
    public $user;
    private $users = [];
    private $groups = [];
    
    private $members;
    
    public $is_user = false;
    public $is_group = false;
    
    function __construct($role_id, $first_level = true) {
        global $BID;

        if ($first_level && isset($_SESSION['role_processed'])) {
            unset($_SESSION['role_processed']);
        }
        
        if (! class_exists('User')) {
            require('user.php');
        }
        
        if ($role_id === 'project members') {
            $this->role = $this->query_role_all_project_members();
            $this->role_exists = ($this->role);
        }
        elseif ($role_id === 'project masters') {
            $this->role = $this->query_project_masters();
            $this->role_exists = ($this->role);
        }
        elseif (is_numeric($role_id)) {
            $this->role = $this->query_role_by_id($role_id);
            $this->role_exists = ($this->role);
        }
        else {
            return false;
        }
        
        if ($this->role_exists and $this->role['user_id'] != '') {
            $this->is_user = true;
            $this->user = new User((int)$this->role['user_id']);
        }
        else {
            if (!isset($this->role['members'])) {
                return;
            }
            $this->is_group = true;
            $members = json_decode($this->role['members']);

            if (!isset($_SESSION['role_processed'])) $_SESSION['role_processed'] = array();

            $tm = array_map(function($member) {
                if (in_array($member,$_SESSION['role_processed'])===false) {
                    $_SESSION['role_processed'][] = $member;
                    return new Role($member, false);
                } 
            }, $members);

            $this->members = array_filter($tm);
            
        }
    }
    
    private function query_role_by_id($role_id) {
        global $BID;
        
        $cmd = sprintf('SELECT role_id, array_to_json(container) as members, user_id, description FROM project_roles WHERE project_table = \'%s\' AND role_id = %s;', PROJECTTABLE, quote($role_id));
        if (! $res = pg_query($BID, $cmd)) {
            log_action('query error',__FILE__,__LINE__);
            return false;
        }
        if (!pg_num_rows($res)) {
            return false;
        }
        
        return pg_fetch_assoc($res);
    }
    
    private function query_role_all_project_members() {
        global $BID;
        $cmd = sprintf("SELECT 0 AS role_id, array_to_json(array_agg(role_id)) AS members, '' AS user_id, 'everybody' as description FROM project_roles WHERE project_table = '%s';", PROJECTTABLE);
        if (! $res = pg_query($BID, $cmd)) {
            log_action('query error',__FILE__,__LINE__);
            return false;
        }
        if (!pg_num_rows($res)) {
            return false;
        }
        
        return pg_fetch_assoc($res);
    }
    
    private function query_project_masters() {
        global $BID;
        $cmd = sprintf("SELECT 0 AS role_id, array_to_json(array_agg(role_id)) AS members, '' AS user_id, 'masters' as description FROM project_roles pr LEFT JOIN project_users pu ON pr.project_table = pu.project_table AND pr.user_id = pu.user_id WHERE pr.project_table = '%s' AND pr.user_id IS NOT NULL AND pu.user_status IN ('2','master');", PROJECTTABLE);
        if (! $res = pg_query($BID, $cmd)) {
            log_action('query error',__FILE__,__LINE__);
            return false;
        }
        if (!pg_num_rows($res)) {
            return false;
        }
        
        return pg_fetch_assoc($res);
        
    }
    
    public function get_members_array() {
        if ($this->is_group) {
            return array_map(function($member) {
                return $member->get_members_array();
            }, $this->members);
        }
        return $this->user;
    }
    
    public function get_members($sort = null, $order = SORT_ASC) {
        if (! $this->role_exists) {
            log_action('Role does not exists');
            return [];
        }
        if ($this->is_user) {
            return [$this->user];
        }
        else {
            $ma = $this->array_values_recursive($this->get_members_array());
            if ($sort) {
                $this->sort_users($ma, $sort, $order);
            }
            return $ma;
        }
    }
    
    public function get_groups() {
        return array_values(array_filter($this->members, function ($m) {
            return ($m->is_group);
        }));
    }
    
    
    function array_values_recursive($array) {
        $flat = array();
        
        foreach($array as $value) {
            if (is_array($value)) {
                $flat = array_merge($flat, $this->array_values_recursive($value));
            }
            else {
                $flat[] = $value;
            }
        }
        return array_unique($flat);
    }
    
    private function get_user_property($prop) {
        if ( $this->is_group ) {
            return array_map(function($member) use ($prop) {
                return $member->$prop;
            }, $this->get_members());
        }
        if (gettype($this->user) == 'object')
            return [$this->user->$prop];
        else
            return [];
    }
    
    public function get_member_emails() {
        return $this->get_user_property('email');
    }
    
    public function get_member_ids() {
        return $this->get_user_property('user_id');
    }
    
    public function get_member_names() {
        return $this->get_user_property('name');
    }

    public function get_roles() {
        return query_role_by_id();
    }
    
    public function get_member_role_ids() {
        return $this->get_user_property('role_id');
    }
    
    private function sort_users(&$arr, $by, $order) {
        if ( !in_array($by, ['user_id','name','email','hash','institute'] ) ) {
            return;
        }
        $by_arr = array_map(function($m) use ($by) {
            return $m->$by;
        }, $arr);
        array_multisort($by_arr, $order, $arr);
        
    }

    public function has_member($user_id) {
        if ($this->is_user) {
            return ($this->user->user_id == $user_id) ;
        }
        $has = count(array_filter($this->get_members(), function($member) use ($user_id) {
            return ($member->user_id === $user_id);
        }));
        return ($has);
        
    }

}
?>
