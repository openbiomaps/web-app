-- init_rules_tbl_exists
SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%1$s_rules');

-- init_rules_tbl_dl_column_exists
SELECT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%1$s_rules' AND column_name = 'download');

-- init_alter_rules_table
ALTER TABLE %s_rules ADD COLUMN download integer[] NULL;

-- check_dl_rules
SELECT CASE WHEN (download && ARRAY[%s]) THEN 1 ELSE 0 END FROM %s_rules WHERE "row_id" = %d AND "data_table" = %s;

-- check_dl_no_rules
SELECT uploader_id FROM system.uploadings LEFT JOIN %1$s ON (obm_uploading_id = uploadings.id) WHERE "%1$s".obm_id= %2$d;
