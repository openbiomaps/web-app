<?php
#' ---
#' Module:
#'   taxon_meta
#' Files:
#'   [taxon_meta.php, taxon_meta.js]
#' Description: >
#'   The scope of this module is to extend the taxon table with additions metadata.
#' Methods:
#'   [moduleName, getMenuItem, adminPage, displayError, ajax, init, print_js, profileSettingsTaxonItem]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class taxon_meta extends module {
    var $error = '';
    var $retval;
    var $params = [
        'TaxonGroups' => ["enabled" => false,],
        'TaxonHierarchy' => ["enabled" => false,],
        'TaxonOrder' => ["enabled" => false,],
        'TaxonRank' => ["enabled" => false,],
    ];

    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;
        
        $params = $this->split_params($params);
        if (is_array($params)) {
            $this->params = $params;
        }
        else {
            $this->params = $this->split_params($params);
        }
        foreach ($this->params as $submodule => $options) {
            if (isset($params[$submodule])) {
                $this->params[$submodule] = $params[$submodule];
            }
        }
        
        if ($action) {
            $this->retval = $this->$action($params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function getMenuItem() {
        return ['label' => 'Taxon meta', 'url' => 'taxon_meta' ];
    }

    public function adminPage($params) {
        return "";
    }

    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function table_join($params, $alias) {
        if (is_array($alias) && empty($alias)) {
            $alias = PROJECTTABLE . '_taxon';
        }
        return sprintf(' LEFT JOIN %1$s_taxonmeta txm ON %2$s.taxon_id = txm.taxon_id', PROJECTTABLE, $alias);
    }
    
    public function meta_columns($params, $pa) {
        $cols = [];
        foreach ($this->params as $sm => $options) {
            $cols = array_merge($cols, $options['columns']);
        }
        $cols = array_values(array_unique($cols));
        
        if ($pa === 'array') {
            return $cols;
        }
        return ', '. implode(', ', array_map(function($c) {
            return "txm.$c";
        }, $cols));
    }
    
    public function get_meta($params, $pa) {
        global $ID;
        if (!isset($pa['taxon_id'])) {
            return false;
        }
        if (!isset($pa['meta_name']) || !preg_match('/^\w+$/', $pa['meta_name'])) {
            log_action('incorrect meta name', __FILE__, __LINE__);
            return false;
        }
        
        $cmd = sprintf("SELECT %s FROM %s_taxonmeta WHERE taxon_id = %d;", $pa['meta_name'], PROJECTTABLE, (int)$pa['taxon_id']);
        if (!$res = pg_query($ID, $cmd)) {
            log_action('get_meta query error');
            return false;
        }
        if (pg_num_rows($res) === 0) {
            return false;
        }
        $results = pg_fetch_assoc($res);
        return $results[$pa['meta_name']];
    }
    
    public function translations() {
        $translations = [];
        foreach ($this->params as $submodule => $options) {
            if ($options['enabled'] === true) {
                $sm = new $submodule($options);
                $translations = array_merge(
                    $translations,
                    $sm->translations()
                );
            }
        }
        return $translations;
    }
    
    public function ajax($params, $request) {

        if (!isset($request['action'])) {
            echo 'ajax action is missing';
            exit;
        }

        switch ($request['action']) {
          default:
            exit;
        }

    }

    public function set_meta($params, $pa) {
      global $ID;
      
      if (!isset($pa['taxon_id']) || !isset($pa['meta_name']) || !isset($pa['meta_value'])) {
        log_action('incorrect parameters', __FILE__, __LINE__);
        return false;
      }
      if (!preg_match('/^\w+$/', $pa['meta_name'])) {
        log_action('incorrect meta name', __FILE__, __LINE__);
        return false;
      }
      
      $cmd = sprintf("INSERT INTO %1\$s_taxonmeta (taxon_id, %3\$s) VALUES (%2\$s, %4\$s) ON CONFLICT ON CONSTRAINT %1\$s_taxonmeta_pkey DO UPDATE SET %3\$s = %4\$s;",
        PROJECTTABLE,
        quote($pa['taxon_id']),
        $pa['meta_name'],
        quote($pa['meta_value'])
      );
      if (!$res = pg_query($ID, $cmd)) {
        log_action('set_meta query error', __FILE__, __LINE__);
        return false;
      }
      return true;
    }
    
    public function init($params, $pa) {
        global $ID;

        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_taxonmeta');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_taxonmeta" ( taxon_id integer PRIMARY KEY) WITH (oids = false); ', PROJECTTABLE );
            $cmd1[] = sprintf('GRANT ALL ON %1$s_taxonmeta TO %1$s_admin; ', PROJECTTABLE );
            $cmd1[] = sprintf('INSERT INTO %1$s_taxonmeta (taxon_id) SELECT DISTINCT taxon_id FROM %1$s_taxon; ', PROJECTTABLE );

            if (!empty($cmd1) && !query($ID,$cmd1)) {
                $this->error = 'taxon_meta init failed: table creation error';
                return;
            }

        }
        
        // creating trigger on the taxon table
        $cmd = sprintf("SELECT EXISTS(SELECT * FROM pg_proc WHERE proname = 'update_%s_taxonmeta_table');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf("CREATE OR REPLACE FUNCTION \"update_%1\$s_taxonmeta_table\" () RETURNS trigger LANGUAGE plpgsql AS $$
                    BEGIN
                        IF tg_op = 'INSERT' THEN
                            INSERT INTO %1\$s_taxonmeta (taxon_id) VALUES (new.taxon_id) ON CONFLICT DO NOTHING;
                            RETURN new;
                        ELSIF tg_op = 'UPDATE' AND old.taxon_id != new.taxon_id THEN
                            INSERT INTO %1\$s_taxonmeta (taxon_id) VALUES (new.taxon_id) ON CONFLICT DO NOTHING;
                            DELETE FROM %1\$s_taxonmeta WHERE taxon_id = old.taxon_id AND NOT EXISTS (SELECT 1 FROM %1\$s_taxon WHERE taxon_id = old.taxon_id);
                            RETURN new;
                        ELSIF tg_op = 'DELETE' THEN
                            DELETE FROM %1\$s_taxonmeta WHERE taxon_id = old.taxon_id AND NOT EXISTS (SELECT 1 FROM %1\$s_taxon WHERE taxon_id = old.taxon_id);
                            RETURN old;
                        END IF;
                        RETURN new;
                    END; 
                    $$;", PROJECTTABLE);
            $cmd1[] = sprintf("CREATE TRIGGER \"%1\$s_taxon_taxonmeta_trg\" AFTER INSERT OR UPDATE OR DELETE ON \"%1\$s_taxon\" FOR EACH ROW EXECUTE PROCEDURE update_%1\$s_taxonmeta_table();", PROJECTTABLE);
            if (!empty($cmd1) && !query($ID,$cmd1)) {
                $this->error = 'taxon_meta init failed: table creation error';
                return;
            }
        }
        
        // columns already created
        $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '%s_taxonmeta';", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = array_column(pg_fetch_all($res), 'column_name');
        
        // user defined columns
        $cmd = [];
        $p_columns = [];
        foreach ($this->params as $submodule => $options) {
            if ($options['enabled'] !== true) {
                continue;
            }
            if (!isset($options['columns'])) {
                $this->error = "$submodule: columns param missing";
                continue;
            }
            $new_columns = array_values(array_diff($options['columns'], $result));
            $type = ($submodule === 'TaxonOrder' || $submodule === 'TaxonHierarchy') 
                 ? 'integer'
                 : 'character varying';
            $cmd = array_merge(
                $cmd, 
                array_map(function ($col) use ($type) {
                    return sprintf("ALTER TABLE %s_taxonmeta ADD COLUMN %s %s;", PROJECTTABLE, $col, $type);
                }, $new_columns)
            );
        }
        
        if (!empty($cmd) && !query($ID,$cmd)) {
            $this->error = 'taxon_meta init failed: adding columns error';
            return;
        }
        
        return;
    }

    public function is_submodule_enabled($params, $pa) {
        global $ID;
        $submodule = $pa[0];
        $enabled = $this->params[$submodule]['enabled'] ?? false;
        return $enabled;
    }
    
    public function include_submodule($params, $pa) {
        if (count($pa) < 2) {
            $this->error = __CLASS__ . ' include_submodule parameter missing';
            return;
        }
        $submodule = $pa[0];
        $method = $pa[1];
        $sm_pa = $pa[2] ?? null;
        
        if (!class_exists($submodule)) {
            $this->error = "$submodule class not defined";
            return;
        } 
        if (!method_exists($submodule, $method)) {
            $this->error = "$submodule::$method method does not exist.";
            return;
        }
        if (!$this->is_submodule_enabled(null, [$submodule])) {
            $this->error = "$submodule is not enabled.";
            return;
        }
        $sm_params = $this->params[$submodule];
        $sm = new $submodule($sm_params);
        
        return $sm->$method($sm_pa);
        
    }
    public function profileSettingsTaxonItem($params,$args) {

        $uargs = $args->taxon_order ?? "";
        $label = str_select_preferred_taxon_order;
        $item = $this->include_submodule($params,['TaxonOrder', 'preferred_order_selector', compact('uargs')]);
        return array('label'=>$label,'item'=>$item);
    }
}

class TaxonMetaSubModule {
    public $params;
    function __construct($params) {
        $this->params = $params;
    }
    
    public function columns() {
        return $this->params['columns'];
    }
    
    public function translations() {
        $translations = [];
        foreach ($this->columns() as $col) {
            $translations["str_$col"] = t("str_$col");
        }
        return $translations;
    }
}
/**
 * 
 */
class TaxonHierarchy extends TaxonMetaSubModule {
    
    public function load_children_ids($parent_ids) {
        global $ID;
        
        $parent_id_column = $this->columns()[0];
        $children_ids = [];
        
        $cmd = sprintf("SELECT taxon_id FROM %s_taxonmeta WHERE %s IN (%s);", PROJECTTABLE, $parent_id_column, implode(',', array_map('quote', $parent_ids)));
        
        if (!$res = pg_query($ID, $cmd)) {
            log_action( "query error", __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return [];
        }
        
        if (pg_num_rows($res) === 0) {
            return [];
        }
        
        return array_column(pg_fetch_all($res), 'taxon_id');
    }
}

/**
 * 
 */
class TaxonGroups extends TaxonMetaSubModule {
    
    public function text_filter_input_arguments($pa) {
        if (!in_array($pa, $this->columns())) {
            return false;
        }
        return [
            'allOptions' => false,
            'valueColumn' => $pa,
            'labelColumn' => $pa,
            'optionsTable' => PROJECTTABLE . '_taxonmeta',
            'optionsSchema' => 'public',
            'html_id' => $pa
        ];
    }
}

/**
 * 
 */
class TaxonOrder extends TaxonMetaSubModule {
    
    public function translations() {
        $sm_strings = ["str_alphabetical", "str_order_of_results"];
        $sm_translations = [];
        foreach ($sm_strings as $string) {
            $sm_translations[$string] = t($string);
        }
        return array_merge(
            parent::translations(),
            $sm_translations
        );
    }
    
    public function get_orders($pa) {
        global $ID;
        $pa = array_values($pa);
        if (!count($pa) || !isset($pa[0]['taxon_id'])) {
            return [];
        }
        $taxon_ids = array_column($pa,'taxon_id');
        $ordering_categories = $this->columns();
        
        if (isset($pa[0]['word'])) {
          $ordering_categories[] = 'alphabetical';
          $alphabetical_order = $this->get_alphabetical_order(array_column($pa, 'word'));
          $pa = array_map(function($row) use ($alphabetical_order) {
            $row['alphabetical'] = $alphabetical_order[$row['word']];
            return $row;
          }, $pa);
        }
        
        
        $cmd = sprintf("SELECT taxon_id, %s FROM %s_taxonmeta WHERE taxon_id IN (%s);", 
            implode(',', array_map(function($c) {
                return "\"$c\"";
            }, $this->columns())),
            PROJECTTABLE,
            implode(',', array_map('quote', $taxon_ids))
        );
        
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return [];
        }
        if (pg_num_rows($res) === 0) {
            return [];
        }
        $order = [];
        while ( $row = pg_fetch_assoc($res)) {
            $order[$row['taxon_id']] = $row;
        }
        
        //is needed append alphabetical order
        if (isset($pa[0]['word'])) {
            foreach ($pa as $tax) {
              $order[$tax['taxon_id']]['alphabetical'] = $tax['alphabetical'];
            }
        }
        return compact('ordering_categories', 'order');
    }
    
    public function reorder_list($pa) {
      $species_list = $pa['list'];
      
      $orderby = $pa['orderby'];
      
      if ((!$orderby || $orderby === 'project_taxon_order') && isset($this->params['default_order'])) {
        $orderby = $this->params['default_order'];
      }
      
      if (!$orderby || ($orderby !== 'alphabetical' && !in_array($orderby, $this->params['columns']))) {
        return $species_list;
      }
      
      $orders = $this->get_orders($species_list);
      
      $species_list = array_map(function($sp) use ($orders, $orderby) {
          $sp['order'] = $orders['order'][$sp['taxon_id']][$orderby];
          return $sp;
      }, $species_list);
      
      obm_set_locale();
      array_multisort(array_column($species_list, 'order'), SORT_NUMERIC, SORT_LOCALE_STRING, $species_list);
      
      return $species_list;
    }
    
    public function preferred_order_selector ($pa) {
        $selected = (isset($pa['args'])) ? $pa['args'] : "";
        $orderbys = array_merge($this->columns(), ['alphabetical']);
        $out = "<select class='taxon-order-selector'>";
        $out .= "<option value='project_taxon_order'>" . ucfirst(t('str_project_taxon_order')) . "</option>";
        foreach ($orderbys as $col) {
            $sel = ($col == $selected) ? "selected" : "" ;
            $out .= "<option value='$col' $sel >". ucfirst(t("str_$col")). "</option>";
        }
        $out .= "</select>";
        return $out;
    }
    
    public function get_preferred_order() {
        $preferred_order = ($ord = get_option('taxon_order')) ? $ord : 'alphabetical';
        if (isset($_SESSION['Tid'])) {
            $Me = new User($_SESSION['Tid']);
            if (isset($Me->options->taxon_order) && $Me->options->taxon_order !== 'project_taxon_order') {
                $preferred_order = $Me->options->taxon_order;
            }
        }
        return $preferred_order;
    }
    
    private function get_alphabetical_order($list) {
        obm_set_locale();
        sort($list, SORT_LOCALE_STRING);
        return array_flip($list);
    }
}

/**
 * 
 */
class TaxonRank extends TaxonMetaSubModule {
  
    public function translations() {
        $sm_strings = array_map(
            function ($rank) {
                return "str_$rank";
            },
            $this->params['ranks']
        );
        $sm_translations = [];
        foreach ($sm_strings as $string) {
            $sm_translations[$string] = t($string);
        }
        return array_merge(
            parent::translations(),
            $sm_translations
        );
    }
    
    public function filter_list($pa) {
        global $ID;
        $species_list = $pa['species_list'];
        $ranks = [
          'ranks' => [],
          'species_list' => $species_list
        ];
        
        $rank_column = $this->columns()[0];
        //$filter_by_rank = $this->filter_by_rank();
        
        $cmd = sprintf("SELECT taxon_id, %s FROM %s_taxonmeta WHERE taxon_id IN (%s);",
            "\"$rank_column\"",
            PROJECTTABLE,
            implode(',', array_map('quote', array_column($species_list, 'taxon_id')))
        );
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query_error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return false;
        }
        if (pg_num_rows($res) === 0) {
          return $ranks;
        }
        
        $results = [];
        while ($row = pg_fetch_assoc($res)) {
          $results[$row['taxon_id']] = $row[$rank_column];
        }
        
        $ranks['species_list'] = array_map(function($sp) use ($results) {
            $sp['rank'] = $results[$sp['taxon_id']];
            return $sp;
        }, $species_list);
        $ranks['ranks'] = $this->ranks_ordered(array_values(array_unique($results)));
        
        return $ranks;
    }
    
    /**
     * Counts the number of taxons groupped by the defined taxon ranks
     * results are reordered  
     **/
    public function count_taxon_by_rank($pa) {
        global $ID;
        $rankcol = $this->columns()[0];
        $st_col = st_col(PROJECTTABLE, 'array');
        
        $cmd = sprintf("SELECT %1\$s as rank, count(*) c FROM %2\$s_taxon tx LEFT JOIN %2\$s_taxonmeta txm ON tx.taxon_id = txm.taxon_id WHERE status = 'accepted' AND lang = %3\$s GROUP BY %1\$s;",
            $rankcol,
            PROJECTTABLE,
            quote($st_col['SPECIES_C'])
        );
        if (!$res = pg_query($ID, $cmd)) {
            log_action('query error: '. $cmd, __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return [];
        }
        $results = [];
        while($row = pg_fetch_assoc($res)) {
          $results[$row['rank']] = $row['c'];
        }
        $ranks = $this->ranks_ordered(array_keys($results));
        return array_map(function($r) use ($results) {
            return [
              'rank' => $r,
              'c' => $results[$r]
            ];
        }, $ranks);
        
    }
    
    /**
     * reorders the distinct rank values based on the settings given at the taxon_meta
     * module parameters
     * missing values are placed to the end
     **/
    private function ranks_ordered($ranks) {
       if (!isset($this->params['ranks'])) {
         return $ranks;
       }
       $ranks_ordered = array_merge(
          array_intersect($this->params['ranks'], $ranks), 
          array_diff($ranks, $this->params['ranks'])
        );
       return $ranks_ordered;
    }

    /*
    private function filter_by_rank() {
        if (preg_match("/^JSON:(.*)$/", $params, $o)) {
            $json = $o[1];
            $params = json_decode(base64_decode($o[1]),true);
        }
        if (!isset($params['TaxonRank'])) {
            return "";
        }
        extract($params['TaxonRank']);
        
        if (isset($ranks_accepted)) {
            return sprintf("AND rank IN (%s)", implode(',', array_map('quote', $ranks_accepted)));
        }
        if (isset($ranks_excluded)) {
            return sprintf("AND rank NOT IN (%s)", implode(',', array_map('quote', $ranks_excluded)));
        }
        return "";
      
    }
    */
}

?>
