<?php
#' ---
#' Module:
#'   restricted_data
#' Files:
#'   [restricted_data.php]
#' Description: >
#'   Rule based data restriction
#'   Old Milvus app used something similar: restricted_data -> titkos_adat()
#'   Used in results_builder.php
#' Methods:
#'   [rule_data, apply_rules]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class restricted_data extends module {
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function rule_data($params,$current_rows) {
            $n = 0;
            foreach($current_rows as $row) {
                $row_keys = array_keys($row);
                $rcr = "";
                #log_action(json_encode($row));
                $rcr = $this->apply_rules($row,$row_keys);
                if($rcr!='')
                    $current_rows[$n] = $rcr;
                $n++;
            }

    }

    /* Do some modification or removing column
     *
     * */
    public function apply_rules($row,$columns) {
        #Example
        #
        #$date1 = new DateTime("now");
        #$date2 = $row['obm_datum'];
        #$date2->add(new DateInterval('P365D'));
        #if($row['species'] == 'Aquila heliaca' and $row['nest']==1 and  $date1<$date2) {
        #    foreach($columns as $i) {
        #        if ($i == 'obm_geometry') {
        #            unset($row[$i]);
        #        }
        #        elseif ($i == 'obm_datum')
        #            $row[$i] = '2000-01-01';
        #    }
        #} 
        return $row;
    }
}
?>
