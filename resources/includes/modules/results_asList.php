<?php
#' ---
#' Module:
#'   results_asList
#' Files:
#'   [results_asList.php]
#' Description: >
#'   Create foldable slide reference list
#' Methods:
#'   [print_query_results, print_results_selector_button, print_js, print_slide, results_output_method, get_default_view]
#' Examples: >
#'   {
#'     "default": "on"
#'   }
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asList extends module {
    public $error = '';
    public $retval = null;

    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        $this->params = $this->split_params($static_params);
        if ($action)
            $this->retval = $this->$action($this->params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_results_selector_button($params,$p) {
        $button = new button(["icon"=>'fa fa-eye']);
        $b = $button->print(["title"=>t(str_as_foldable_list),"class"=>"button-href pure-button","onclick"=>"switch_list()"]);
        
        return sprintf("%s ",$b);
    }

    public function get_default_view($params,$p) {
        $p = $this->split_params($params);
        if (isset($p['default']))
           return 'list'; 
    }
    // return  ?html content
    // $p = [$this->method,$data,$this]
    public function print_query_results($params,$p) {

        //[$this->method,$data,$this]
        if ($p[0] == 'slide') {
            // return  html content
            return $this->print_slide($params,array($p[1],$p[2]->rq['columns'],['slide'=>$p[2]->slide]));
            //return $this->print_slide($params,array($p[1]));
        }
        elseif ($p[0] == 'list') {
            return $this->print_roll_list($params, $p[2]);
        }
        return false;
    }
    public function ajax($params,$request) {
    
        if (isset($_POST['switch-to-list'])) {

            $method = 'list';

            if(!isset($_SESSION['show_results'])) {
                $_SESSION['show_results'] = array();
            }

            $_SESSION['show_results']['type'] = $method;

            /*$_SESSION['show_results']['rowHeight'] = '60';
            $_SESSION['show_results']['cellWidth'] = '400';
            $_SESSION['show_results']['borderRight'] = 'none';
            $_SESSION['show_results']['borderBottom'] = 'none';*/

            include(getenv('OB_LIB_DIR').'results_builder.php');
            $r = new results_builder(['method'=>$method,'module_ref'=>'results_asList']);
            if ( $r->results_query() ) {
                $r->printOut(['content'=>'update']);
                exit;
            }
            echo common_message('error',$r->error);
            return;
        }

        // Click on list element: Open slide modal
        if (isset($_POST['printSlide'])) {
            /* toggleSlides()
             * ez olvassa be az egyes blokkok adatát
             * */

            $id = preg_replace('/[^slideb0-9:]/','',$_POST['id']);

            include_once(getenv('OB_LIB_DIR').'results_builder.php');
            $r = new results_builder(['method'=>'slide','module_ref'=>'results_asList','slide'=>$id,'clear_current_rows'=>'off']);

            $id = preg_replace('/[^0-9]/','',$_POST['id']);
            if($r->results_query($id)) {
                if ($r->fetch_data()) {
                    // print out slide
                    if ($r->updateOutput()) {
                        exit;
                    }
                } 
            }
            unset($_SESSION['roll_array']);
            return;
        }
    }

    public function print_js($params) {
        $js = 'function switch_list() {
        $.post("ajax", {m:"results_asList","switch-to-list":"list"},
        function(data){
            var retval = jsendp(data);
            if (retval["status"]=="error") {
                $( "#dialog" ).text(retval["message"]);
            } else if (retval["status"]=="fail") {
                $( "#dialog" ).text("Invalid response received.");
                console.log(retval["data"]);
            } else if (retval["status"]=="success") {
                v = retval["data"];
                $("#matrix").append(v.html);
                let rolltable = v.rolltable;
                rr.set({rowHeight:rolltable.rowHeight,cellWidth:rolltable.cellWidth,borderRight:rolltable.borderRight,borderBottom:rolltable.borderBottom,arrayType:rolltable.arrayType});
                var va = JSON.parse(rolltable.scrollbar_header)
                rr.prepare(va["w"],va["i"]);
                rr.render();
                $("#scrollbar").show();
                $("#drbc").height(eval($(".scrollbar").height()+100));
            }
        });}
        $(document).ready(function() {
            $("#matrix").on("click","#slider",function(){
                $(this).draggable({handle:"div"});
                $(this).resizable();
                if ($(".slide-body").height() > ih-100) {
                    $(".slide-body").height(ih-100);
                }
            });

            /* Slide header / detailes links */
            $("#matrix").on("click",".togglerbutton",function(e){
                var id = $(this).data("id");
                var widgetId=id.substring(id.indexOf("-")+1,id.length);
                $(".toggler").removeClass("sliderExpanded");
                $("#"+id).addClass("sliderExpanded");
                $( "#dialog" ).text("Loading ...");
                var isOpen = $( "#dialog" ).dialog( "isOpen" );
                if(!isOpen) $( "#dialog" ).dialog( "open" );
                
                $.post("ajax", {m:"results_asList",id:widgetId,printSlide:1},
                    function(data){
                            $( "#dialog" ).dialog( "close" );
                            if (data) {
                                $("#slider").html(data);
                                $("#slider").data("id",id);
                            }
                });
            });
            /* results list links: <header>, <detailes> */
            $("#drbc").on("click",".toggler",function(e){
                var id = $(this).attr("id");
                var widgetId=id.substring(id.indexOf("-")+1,id.length);
                if($(this).hasClass("sliderExpanded")) {
                    //
                    $("#slider").hide();
                    $(".toggler").removeClass("sliderExpanded");
                } else {
                    $("#slider").html("");
                    $(".toggler").removeClass("sliderExpanded");
                    $(this).addClass("sliderExpanded");
                    $( "#dialog" ).text("Loading ...");
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                
                    $.post("ajax", {m:"results_asList",id:widgetId,printSlide:1},
                        function(data){
                            $( "#dialog" ).dialog( "close" );
                            if (data) {
                                $("#slider").html(data);
                                $("#slider").data("id",id);
                            }
                    });
                    $("#slider").show();
                }
            });
            // should be change to toggleClose !!!
            $("#matrix").on("click",".close",function(){
                $(this).parent().hide();
                $(".toggler").removeClass("sliderExpanded");
            }); 
        });';
        return $js;
    }
    private function print_roll_list($params,$px) {
        global $ID,$BID;

        //kiürítjük a slidokat minden új beolvasásnál
        $_SESSION['slides'] = array();
        
        $ID_C = $_SESSION['st_col']['ID_C'];

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_list_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_list_%s_%s (rlist text, ord integer)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);
        update_temp_table_index('roll_list_'.PROJECTTABLE.'_'.session_id());
    
        // slide title as parameter: SLIDE_TITLE:column_name
        $pmi = array();
        $control = '';

        $p = $this->split_params($params);
        foreach($p as $pm) {
            if (preg_match('/^SLIDE_TITLE:(.+)/',$pm,$m)) {
                $control = $m[1];
            }
        }

        if ($control != '')
            $c = ",$control";
        else
            $c = '';
        $c='';

        $res = pg_query($ID,sprintf('SELECT DISTINCT obm_id obm_id%s FROM temporary_tables.temp_query_%s_%s ORDER BY obm_id',$c,PROJECTTABLE,session_id()));

        $k = pg_num_rows($res)*121;
        $n = 0;
        $cmd = '';

        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_list_%s_%s FROM STDIN",PROJECTTABLE,session_id()));
        $order = 0;
        while($row = pg_fetch_assoc($res)) {

            $slide_title = '';
            $slide_id = $row['obm_id'];
            
            if ($control!='' and isset($row[$control])) {
                // substring
                $slide_title = mb_strimwidth($row[$control], 0, 20, "...");
            }
            if ($ID_C != 'obm_id' and isset($row[$ID_C])) {
                $slide_id = $row[$ID_C];
            }

            pg_put_line($ID, $this->list_ref_ass($slide_id,$slide_title)."\t$order\n");
            $order++;

        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);
        
        if ($px->rq['results_count'])
            $html_output = "<div id='slider' class='slider'></div>";
        else
            $html_output = "";
        
        $table_params = array(
            'rowHeight' => '60',
            'cellWidth' => '400',
            'borderRight' => 'none',
            'borderBottom' => 'none');
        // further processing of returning content...
        return array('output'=>'rolltable','params'=>$table_params,'content'=>$html_output,'control'=>['w'=>400,'i'=>'']);
    }

    // assemble slide
    public function list_ref_ass($slide_id,$slide_title) {

        // slide header reference
        if ($slide_title!='') $slide_title = " - $slide_title";

        $dh = sprintf('<p class="toggler" id="toggler-slide%1$s">%2$s (ID: %3$s%4$s)<span class="expandSlider"><i class="fa fa-folder-o"></i></span><span class="collapseSlider"><i class="fa fa-folder-open-o"></i></span></p>',$slide_id,str_dataheader,preg_replace("/^NULL$/","undefined",$slide_id),$slide_title);
        // slide body reference
        $t = 'b'.$slide_id;
        $dh .= sprintf('<p class=\'toggler\' id=\'toggler-slideb%1$s\'><span class="expandSlider"><i class="fa fa-folder-o"></i></span><span class="collapseSlider"><i class="fa fa-folder-open-o"></i></span>%2$s</p>',$slide_id,str_details.'...');
        return "<div class='data_block'>".$dh."</div>";
    }
    function print_slide($params,$p) {
        $mf = new slide();
        return $mf->print_slide($params,$p[0],$p[1],$p[2]);
    }
}

class slide {
    
    function print_slide($params,$data,$COLUMNS=NULL,$def=NULL) {
        global $ID;
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $st_col = st_col($_SESSION['current_query_table'],'array');

        $SPECIES_C = $st_col['SPECIES_C'];
        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m))
            $SPECIES_C = $m[2];

        $table = PROJECTTABLE;
        
        preg_match('/(slide[b]?)(.+)/',$def['slide'],$m);
        $hb = "body";
        if($m[1]=='slide') $hb = "header";

        /*$limit = '';
        if(preg_match('/(\d+):?(\d+)?/',$m[2],$mm)){
            $m[2] = $mm[1];
            if (isset($mm[2]))
                $limit = sprintf('LIMIT 1 OFFSET %1d',$mm[2]);
        } else {
            //first row if no :n JOIN attribute
            //$limit = 'LIMIT 1 OFFSET 0';
        }*/

        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%s_%s WHERE obm_id=%s',PROJECTTABLE,session_id(),quote($m[2])));
        $rows = pg_fetch_all($res);
        
        $row = $rows[0];
        $r_id = $row['obm_id'];                   // data row's id
        // default slide title: species name
        // here we can apply a module again but not really necessary!
        if (isset($row[$SPECIES_C]) and $SPECIES_C!='')
            $r_spec = $row[$SPECIES_C];             // data row's species name
        else
            $r_spec = '';
        $r_valid=''; 
        $u_id = '';
        $u_date = '';
        $upl_id = '';
        $upl_name = '';
        if (isset($row['uploading_id']))            $u_id = $row['uploading_id'];               // data row's uploading id
        if (isset($row['uploading_date']))  $u_date = $row['uploading_date'];   // data row's uploading date
        if (isset($row['uploader_id']))     $upl_id = $row['uploader_id'];      // uploader's id
        if (isset($row['uploader_name']))   $upl_name = $row['uploader_name'];  // uploaders's name
        if (isset($row['data_eval']))       $r_valid = $row['data_eval'];       // data row's votes

        if($hb == "header") 
            return $this->header_assemble($table,$r_id,$r_valid,$upl_id,$upl_name,$u_id,$u_date);
        else
            return $this->body_assemble($params,$COLUMNS,$rows,$r_id);
   
    }
    /* slide header
     * set: SESSION['slide']
     * */
    function header_assemble($qtable,$r_id,$r_valid,$upl_id,$upl_name,$u_id,$u_date) {
        global $ID;
        $t = $r_id;
        $m = array();
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        
        $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$qtable)); 

        if(!isset($_SESSION["slide"]["slide$t"])) { 
            $dh = sprintf('<button class="pure-button button-gray close" style="position:absolute;top:4px;right:4px"><i class="fa fa-close"></i></button>
                <div style="position:absolute;top:0;left:0;width:100&#37;;height:37px;background-color:#afafaf;border-bottom:1px solid #999;z-index:-1"></div>
                <div>
                <table style="border-spacing:1px !important;border-collapse:unset;height:200px">
                <thead><tr>
                    <th style="background-color:#606060;height:34px" id="opb1_%1$s" class="rs_hb rs_hbs">%2$s</th>
                    <th style="background-color:#686868;height:34px" id="opb2_%1$s" class="rs_hb">%3$s</th>
                    <th style="background-color:#707070;height:34px" id="opb3_%1$s" class="rs_hb">%4$s</th>
                    <th style="background-color:#787878;height:34px" id="opb4_%1$s" class="rs_hb">%5$s</th>
                </tr></thead>',$r_id,t(str_upload),t(str_changes),t(str_metrics),t(str_comments));

            $dh .= sprintf('<tbody><tr><td colspan="7" id="optd_%1$s" style="padding:10px 0 10px 10px;vertical-align:top">',$r_id);
            

            // adatfeltöltés adatai
            $dh .= sprintf("<div id='opbd1_%s' class='rs_hbd' style='display:block'><b>%s: </b>",$r_id,t(str_uploader));
            if ($ulink=fetch_user($upl_id,'roleid','link')) 
                $dh .= $ulink; 
            else 
                $dh .= $upl_name; 

            $eurl = sprintf($protocol.'://'.URL.'/upload/history/%d/',$u_id);
            $dh .= sprintf('<br><b>%1$s: </b><a href=\'%2$s\' target=\'_blank\'>%3$d</a>',t(str_dataupid),$eurl,$u_id);
            $dh .= sprintf("<br><b>%s: </b>%s",t(str_date),$u_date);
            $dh .= sprintf("</div>");

            // data changes
            $last_mod = data_changes($qtable_history,PROJECTTABLE,$r_id);
            $dh .= sprintf("<div style='' id='opbd2_%s' class='rs_hbd'><b>%s: </b>%s %s<br>",$r_id,t(str_last_modify),$last_mod['time'],$last_mod['link']);

            $eurl = sprintf($protocol.'://'.URL.'/data/%s/%d/history/',$_SESSION['current_query_table'],$r_id);
            $dh .= sprintf("<b>%s: </b><a href='%s' target='_blank'>%s</a></div>",t(str_modcount),$eurl,$last_mod['count']);
            
            //metrics - views
            $views = metrics($r_id,'data_views');
            
            //metrics - citations
            $citations = cite_metrics($r_id,'citations',$qtable);
            
            //metrics - downloads
            $downloads = metrics($r_id,'downloads');

            if ($r_valid=='') $evaluation = '';
            else $evaluation = sprintf("%.2f",$r_valid*100)."%";
            // metrics - validitás + evaluation
            $eurl = sprintf($protocol.'://'.URL.'/data/%s/%d/history/validation/',$qtable,$r_id);
            $dh .= sprintf("<div style='' id='opbd3_%s' class='rs_hbd'>
                <b><abbr title='Known citations'>%s</abbr>: </b>%s<br>
                <b><abbr title='Estimated value'>%s</abbr>: </b>%s<br>
                <b>%s: </b>%s<br>
                <b><abbr title='mean value of project+data+uploading+user evaluates'>%s</abbr>: </b>%s<br>
                <b>%s: </b><a href='%s' target='_blank'>%s</a></div>",$r_id,t(str_citations),$citations,t(str_views),$views,t(str_downloads),$downloads,t(str_validation),validation($t),t(str_valuation),$eurl,$evaluation);

            // obm_comments
            $dh .= sprintf("<div style='' id='opbd4_%s' class='rs_hbd'>%s</div>",$r_id,comments($r_id,PROJECTTABLE,'data'));


            $dh .= sprintf('</tr></tbody></table>');
            //
            $view_url = sprintf('%://'.URL.'/data/%s/%s/',$protocol,$_SESSION['current_query_table'],$r_id);
            $dh .= sprintf('<div style="padding:10px 0 5px 7px"><a href="%4$s" target="_blank" class="button-secondary button-href pure-button"><i class="fa fa-external-link"></i> %2$s</a> 
                <button class="button-href pure-button togglerbutton" data-id="toggler-slideb%1$s"><i class="fa fa-newspaper-o"></i> %3$s</button></div>',$r_id,str_datasheet,str_details,$view_url);
            $dh .= "</div>";

            return $dh;
        }    

    }
    /* slide body
     * set: SESSION['slide']
     *
     * */
    function body_assemble($params,$COLUMNS,$rows,$r_id) {
        global $ID,$BID,$modules,$x_modules; 
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $p = $params;

        foreach( $p as $pm ) {
            $pm = preg_split('/:/',$pm);
            
            if (isset($COLUMNS[$pm[0]]) and isset($pm[1])) {
                $COLUMNS[$pm[1]] = $COLUMNS[$pm[0]];
                unset($COLUMNS[$pm[0]]);
            }
        }

        $acc = true;
        if (!rst('acc',$r_id,$_SESSION['current_query_table'], has_access('master'))) 
            $acc = false;
        if (has_access('master'))
            $acc = true;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        // e = postgres result rows
        // COLUMNS = associative array of data fields (keys=value, value=label)
        // t = body slide ID
        $t = 'b'.$r_id;
        
        if (!isset($_SESSION["slide"]["slide$t"])) { 
            //track data view
            track_data($r_id);

            // dh = an <ul> block of details
            $dh = sprintf("<button class='pure-button button-gray close' style='position:absolute;top:4px;right:4px'><i class='fa fa-close'></i></button><span style='font-size:150&#37;;margin-left:5px'>ID: %s</span><div style='position:absolute;top:0;left:0;width:100&#37;;height:37px;background-color:#afafaf;border-bottom:1px solid #999;z-index:-1'></div><br><br><ul class='slide-body'>",$r_id);
        
            // ez kötelezően csak ez lehet!
            $FILE_ID = 'obm_files_id';

            $BOLD_YELLOW = ($modules->is_enabled('bold_yellow')) 
                ? $modules->_include('bold_yellow','mark_text',[],true)
                : array();

            // visible columns
            extract(allowed_columns($modules,$_SESSION['current_query_table']));

            $photos_enabled = false;
            foreach ($x_modules->which_has_method('view_attachment') as $mm) {
                $photos_enabled = $mm;
            }
            $non_repetative = array();

            $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
            $tgroups = $u->roles ?? 0;

            //log_action($COLUMNS);
            /* loop of data rows 
             * it is more than 1 for JOINED data
             * */
            foreach($rows as $e) {
                
                extract(data_access_check($acc,$st_col['USE_RULES'],$allowed_cols,$tgroups,$allowed_cols_module,$e));

                foreach($COLUMNS as $cn=>$label) {
                    if (preg_match('/(\w)+\.(.+)$/',$cn,$m)) {
                        $cn = $m[2];
                        if (preg_match('/(\w)+\.(.+)$/',$label,$m)) {
                            $label = $m[2];
                        }
                    }

                    if ($column_restriction) {
                        if ( !in_array($cn,$allowed_cols) ) {
                            continue;
                        }
                    }
                    elseif ($geometry_restriction ) {
                        if ( $cn == 'obm_geometry')
                            continue;
                        if ( !in_array($cn,$allowed_gcols) ) {
                            continue;
                        }
                    }

                    $jts = ""; //gray style
                    if (!in_array($cn,$main_cols)) {
                        $jts = "graybg";
                    }

                    /*
                     * */
                    if (in_array($cn,$BOLD_YELLOW) and isset($e[$cn]) and !in_array($cn,$non_repetative)) { 
                        if ($jts=='')
                            $non_repetative[] = $cn;
                        
                        // bold yellow attributes: Dátum, Hivatkozás, Mennyiség, Tudományos név
                        $dh .= sprintf("<li class='lmh $jts'><span class='obm_label'>%s:</span> %s</li>",t($label),$e[$cn]);
                    }
                    elseif ($cn==$FILE_ID and $FILE_ID!='' and isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                        if ($jts=='')
                            $non_repetative[] = $cn;
                        
                        // csatolmány megjelenítése!!
                        $cmd = sprintf("SELECT id,reference,substring(comment,1,15) as comment FROM system.files LEFT JOIN system.file_connect ON (files.id=file_connect.file_id) WHERE conid='%s'",$e[$cn]);
                        $res = pg_query($ID,$cmd);
                        $p = array();
                        while ($row = pg_fetch_assoc($res)) {
                            if ($photos_enabled!==false) {
                                if ($thumb = mkThumb($row['reference'],60)) {
                                    $thf = $protocol."://".URL."/getphoto?ref=/thumbnails/{$row['reference']}";
                                    $p[] = "<a href='".$protocol."://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'>
                                                <img src='$thf' title='{$row['comment']}' class='thumb'></a>";
                                } else {
                                    $mime_url = mime_icon($row['reference'],32);
                                    $p[] = "<a href='".$protocol."://".URL."/getphoto?c={$e[$cn]}&ref={$row['reference']}' id='gf_{$row['id']}' class='photolink' target='_blank'>
                                                <img src='$mime_url' title='{$row['comment']}' class='thumb'></a>";
                                }
                            } else {
                                $p[] = "<i class='fa fa-camera' style='color:gray'></i>";
                            }
                        }
                        $e[$cn] = implode(" ",$p);
                        $dh .= sprintf("<li class='lm $jts'><span class='obm_label'>%s:</span> <span style='display:table-cell;vertical-align:bottom;padding-top:15px'>%s</span></li>",t($label),$e[$cn]);
                    }
                    elseif (isset($e[$cn]) and !in_array($cn,$non_repetative)) {

                        if ($jts=='')
                            $non_repetative[] = $cn;

                        // normal attributes
                        // link handling
                        // ezeket module hívással kéne kezelni!
                        // transform module: oszlop név -> do transform
                        if ($modules->is_enabled('transform_data')) {
                            //default itegrated module
                            $e[$cn] = $modules->_include('transform_data','text_transform',array($e[$cn],$cn,''),true);
                        }

                        if ($photos_enabled!==false && preg_match('|^https?://.+|',$e[$cn])) {
                            $e[$cn] = "<a href='{$e[$cn]}' class='photolink' target='_blank'>{$e[$cn]}</a>";
                        }
                        if (preg_match('/^str_/',$e[$cn]))
                            if (defined($e[$cn]))
                                $e[$cn] = constant($e[$cn]);
                        $dh .= sprintf("<li class='lm $jts'><span class='obm_label'>%s:</span> %s</li>",t($label),$e[$cn]);
                    }                }
            }

            $dh .= sprintf("</ul>");
            $view_url = sprintf('%://'.URL.'/data/%s/%s/',$protocol,$_SESSION['current_query_table'],$r_id);

            $dh .= sprintf('<a href="%1$s" target="_blank" class="button-secondary button-href pure-button" style="margin:15px"><i class="fa fa-external-link"></i> %3$s</a> <button class="button-href pure-button togglerbutton" style="margin:15px" data-id="toggler-slide%2$s"><i class="fa fa-newspaper-o"></i> %4$s</button>',$view_url,$r_id,str_datasheet,str_header_information);
            return $dh;
        }
    }
}

?>
