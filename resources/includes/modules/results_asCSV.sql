-- init_tbl_exists
SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'system' AND table_name = '%1$s_data_exports');

-- init_seq
CREATE SEQUENCE "system"."%1$s_data_exports_id_seq" START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1; 

-- init_tbl
CREATE TABLE "system"."%1$s_data_exports" ( 
    "id" integer DEFAULT nextval('%1$s_data_exports_id_seq') NOT NULL, 
    "filename" character varying(128) NOT NULL, 
    "user_id" integer NOT NULL, 
    "status" varchar(32) NOT NULL,
    "message" text,
    "downloaded" integer DEFAULT 0, 
    "requested" timestamp, 
    "valid_until" timestamp,  
    "colnames" json,
    "maxlength" integer,
    CONSTRAINT "%1$s_data_exports_filename" UNIQUE ("filename"), 
    CONSTRAINT "%1$s_data_exports_id" PRIMARY KEY ("id")
);

-- init_set_owner
ALTER SEQUENCE "system"."%1$s_data_exports_id_seq" OWNED BY "system"."%1$s_data_exports".id;
ALTER TABLE system.%1$s_data_exports OWNER TO %1$s_admin;

