<?php
#' ---
#' Module:
#'   results_asTable
#' Files:
#'   [results_asTable.php]
#' Description: >
#'   full size roll table
#' Methods:
#'   [print_table_page, load_fullpage_results_table, print_results_selector_button, print_js]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0

require_once(getenv('OB_LIB_DIR').'default_modules.php');

class results_asTable extends module {
#class results_asTable {
    var $error;
    var $retval;

    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($params,$pa);

    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_results_selector_button($params,$p) {
        $button = new button(["url"=>'show-table',"title"=>t(str_as_full_table),"icon"=>'fa fa-eye']);
        return sprintf("%s ",$button->print([]));
    }

    #public function print_query_results($params,$p) {
    #    $this->print_table_page($params, $p);
    #}
            
    public function print_table_page($params, $pa) {

        /*
         * Show the current result as a table
         * it is called from the map page when the users click on the "show results as table button" 
         * EZ a wfs_response session változót dolgozza fel, habár jobb lenne ha önálló process hívást csinálna az XML fájlra!!
         * */

        global $ID, $BID;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        
        #query=obm_uploading_id:1581&qtable=dinpi
        if (isset($_GET['query']) and $_GET['query']!='1') {
            foreach(preg_split('/&/',$_SERVER['QUERY_STRING']) as $key=>$value){
                parse_str($value,$arr);
                //processing arrays without getting [] after variable names!!!
                //not ready...
            }

            $qs = preg_split('/;/',$_GET['query']);
            
            $m = array();
            foreach( $qs as $q) {
                $kv = preg_split('/:/',$q);
                if (isset($kv[0]) and $kv[0]!='' and isset($kv[1]))
                    $m["qids_$kv[0]"] = json_encode(array($kv[1]));
            }
            $_REQUEST = $m;

            $_SESSION['show_results']['type'] = 'csv'; # nem is az
            
            // create query string
            include(getenv('OB_LIB_DIR').'query_builder.php');
            include(getenv('OB_LIB_DIR').'results_query.php');
        }
        
        include_once(getenv('OB_LIB_DIR').'results_builder.php');

        $reverse = '';
        if (isset($_SESSION['orderby']))
            $reverse = $_SESSION['orderby'];

        $rows = 0;
        $table_type='';
        $encrypted_table_reference = '';
        $encrypted_order_reference = '';
        $ks = array('w'=>0,'i'=>'');

        if (isset($_GET['show'])) {
            /* Original display table option for showing query results 
             *
             * */

            if(!isset($_SESSION['token'])) exit;
            
            // prepare query results
            $r = new results_builder( ['method'=>'fulltable', 'module_ref'=>'results_asTable', 'reverse'=>$reverse ]);

            if ($r->results_query()) {
                $ks = $r->printOut();
                $rows = $r->results_length();
            }
            $table_type = 'table';

            unset($_SESSION['show_results']);

        } elseif (isset($_GET['view']) and isset($_GET['table'])) {
            /* Table or report (VIEW) view option
             *
             * */

            $header = array();

            require_once(getenv('OB_LIB_DIR'). 'modules_class.php');

            $state = false;
            $modules = new modules(PROJECTTABLE);

            if ($modules->is_enabled('read_table',PROJECTTABLE)) {
                $link = $modules->_include('read_table','decode_link',$_GET['table']);

                $state = $modules->_include('read_table','check_table',"{$link['schema']}.{$link['table']}");
            }

            if ($state) {

                if (!isset($_SESSION['private_key']))
                    $_SESSION['private_key'] = genhash();


                //$cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name = %s",quote($schema),quote($table));
                $cmd = sprintf('SELECT * FROM %s.%s LIMIT 1',$link['schema'],$link['table']);

                $orderby = 1;
                $ob = 1;
                if (isset($_SESSION['orderby'])) 
                    $ob = $_SESSION['orderby'];

                $gob = array();
                if (isset($module_order)) {
                    $gob = preg_split('/,/',$link['order']);
                }

                if (isset($_GET['orderby'])) {
                    $gob = preg_split('/,/',$_GET['orderby']);
                }

                $res = pg_query($ID,$cmd);
                //$rows = pg_num_rows($res);
                $rows = '~';

                //while($row = pg_fetch_assoc($res)) {
                $row = pg_fetch_assoc($res);
                    foreach ($row as $key=>$value) {
                        $header[$key] = $key;
                        if ($ob == $key)
                            $orderby = $ob;
                    }
                //}
                $hk = array_keys($header);
                $orderby_array = array();
                foreach ($gob as $g) {
                    $gn = preg_replace("/ DESC/","",$g);
                    $gn = preg_replace("/ ASC/","",$gn);
                    $gn = trim($gn);

                    if (in_array($gn,$hk))
                        $orderby_array[] = $g; 
                }
                if (count($orderby_array)) {
                    $orderby = implode(',',$orderby_array);
                }

                // prepare a table
                //$header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'upid'=>str_dataupid),$header);
                $ks['w'] = count($header)*121;

                //$columns = array_keys($data->csv_header);
                $viewports = '';
                foreach($header as $key=>$val) {
                    $viewports .= "<div id='$key' class='viewport-header' data-type='rolltable'><span style='display:inline-block;width:110px'>$val</span><span class='sort'><i class='fa fa-lg fa-sort'></i></span></div>";
                }

                $ks['i'] = "$viewports<div class='viewport-block'></div>";
                $table_type = 'normaltable';
                $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
                $iv = openssl_random_pseudo_bytes($ivlen);
                $_SESSION['openssl_ivs']['asTable_table'] = base64_encode($iv);
                $encrypted_table_reference = base64_encode(openssl_encrypt("{$link['schema']}.{$link['table']}", $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
                $iv = openssl_random_pseudo_bytes($ivlen);
                $_SESSION['openssl_ivs']['asTable_orderby'] = base64_encode($iv);
                $encrypted_order_reference = base64_encode(openssl_encrypt($orderby, $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));
            }
        }

        ?>
        <!DOCTYPE html>
        <html>
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title><?php echo OB_DOMAIN ?> - <?php echo $rows.' rows' ?></title>
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.css?rev=<?php echo rev('js/ui/jquery-ui.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/theme/default/style.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/font-awesome/css/font-awesome.min.css?rev=<?php echo rev('css/font-awesome.min.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/style.css?rev=<?php echo rev('.'.STYLE_PATH.'/style.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/colors.css.php?rev=<?php echo rev('.'.STYLE_PATH.'/colors.css.php'); ?>" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/mapsdata.css?rev=<?php echo rev('.'.STYLE_PATH.'/mapsdata.css'); ?>" type="text/css" />
            <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/roller.css?rev=<?php echo rev('.'.STYLE_PATH.'/roller.css'); ?>" type="text/css" />
            <style>
            body {
                margin:0px 0px 0px 0px;
                background:#c4c4c4;
            }
            </style>
            <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js"></script>
            <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/dropit.js"></script>
            <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
            <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>
        </head>
        <body>
            <script type='text/javascript'>
        <?php
            echo "
            $(document).ready(function() {

                rr.set({rowHeight:20,cellWidth:120,borderRight:'1px solid #bababa',borderBottom:'1px solid #bababa',arrayType:'$table_type',Table:'$encrypted_table_reference',Order:'$encrypted_order_reference'});
                rr.prepare({$ks['w']},\"{$ks['i']}\");
                rr.render();

            });";
        ?>
            </script>
            <div id="scrollbar-header"></div>
            <div id="scrollbar" class="scrollbar"></div>
        </body>
        </html>
    <?php

    }

    public function load_fullpage_results_table($params,$data) {

        global $ID, $GID;
        
        $data = $data[0];

        $orderby = (isset($_SESSION['orderby'])) ? $_SESSION['orderby'] : 1;
        $orderascd = (isset($_SESSION['orderascd'])) ? $_SESSION['orderascd'] : 'ASC';


        $modules = new modules($_SESSION['current_query_table']);
        $st_col = st_col($_SESSION['current_query_table'],'array');

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_table_%s_%s",PROJECTTABLE,session_id()));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_table_%s_%s (rlist text, ord integer)",PROJECTTABLE,session_id());
        pg_query($ID,$cmd);

        update_temp_table_index('roll_table_'.PROJECTTABLE.'_'.session_id());

        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;

        // ide csak akkor kerül adat *és jön létre egyáltalán*, ha jártunk a wfs_processing.php-ban, azaz a térképet meghívtuk előtte!!!!
        //
        $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY %3$s %4$s',PROJECTTABLE,session_id(),$orderby,$orderascd));

        $n=0;
        $cmd = '';
        
        //exlclude national taxon_names in other languages than the user's defined 
        $excluded_taxon_columns = $u->excluded_taxon_columns;

        // visible columns
        extract(allowed_columns($modules,$_SESSION['current_query_table']));

        if (isset($_SESSION['current_query_keys'])) {
            $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
            $query_column_count = count($_SESSION['current_query_keys']);
        }

        pg_query($GID, sprintf("COPY temporary_tables.temp_roll_table_%s_%s FROM stdin",PROJECTTABLE,session_id()));
        $order = 0;
        $has_master_access = has_access('master');
        while ($row = pg_fetch_assoc($res)) {
            $line = array();
            $id = $row['obm_id'];
            $b_id = sprintf("%s",$id);
            $b_upd = $row['uploading_date'];
            $b_upid = $row['uploading_id'];
            $b_upname = $row['uploader_name'];

            //add meta columns
            array_unshift($line,$b_upid);
            array_unshift($line,$b_upd);
            array_unshift($line,$b_upname);
            array_unshift($line,$b_id);

            // This query kill the apache if we use the same connection there $ID vs $GID !!!
            $acc = rst('acc',$id,$_SESSION['current_query_table'], $has_master_access) ? true : false; 
            #if (!$acc and $st_col['USE_RULES']) {
            #if (!has_access('master') and $st_col['USE_RULES']) {
            // This method only let drop columns if use_rules and allowed column module turned on and allow column well configured!!!

            extract(data_access_check($acc,$st_col['USE_RULES'],$allowed_cols,$tgroups,$allowed_cols_module,$row));

            $drop_line = 0;
            foreach($data->csv_header as $k=>$v) {
                // to show joined table values
                $kspl = preg_split('/\./', $k);
                $k = (count($kspl) == 2) ? $kspl[1] : $kspl[0];
                
                if ($k == 'hist_time')
                    continue;
                if (in_array($k, $excluded_taxon_columns)) {
                  continue;
                }
                if (isset($row[$k]))
                    $line[$k] = $row[$k];
                else
                    $line[$k] = "&nbsp;";

                // Sensitivity based restrictions
                // cleaning non-allowed content
                if ($column_restriction) {
                    if ( !in_array($k,$allowed_cols) ) {
                        $line[$k] = '';
                    }
                }
                elseif ($geometry_restriction ) {
                    if ( $k == 'obm_geometry')
                        $line[$k] = '';
                    if ( !in_array($k,$allowed_gcols) ) {
                        $line[$k] = '';
                    }
                }
            }
            pg_put_line($GID, json_encode($line,JSON_HEX_APOS|JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE)."\t$order\n");
            $order++;
        }
        pg_put_line($GID, "\\.\n");
        pg_end_copy($GID);
        
        unset($data->csv_header['hist_time']);
        if (count($excluded_taxon_columns)) {
          foreach ($excluded_taxon_columns as $col) {
            unset($data->csv_header[$col]);
          }
        }
        
        $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'uploading_id'=>str_dataupid),$data->csv_header);
        $k = count($data->csv_header)*121;

        //$columns = array_keys($data->csv_header);
        $viewports = '';
        foreach($data->csv_header as $key=>$val) {
            $viewports .= "<div id='$key' class='viewport-header' data-type='rolltable'><span style='display:inline-block;width:110px'>$val</span><span class='sort'><i class='fa fa-lg fa-sort'></i></span></div>";
        }

        $s = "$viewports<div class='viewport-block'></div>";

        return array('w'=>$k,'i'=>$s);

    }
}

if (isset($_GET['show']) or isset($_GET['view'])) {

    require_once(getenv('OB_LIB_DIR').'db_funcs.php');
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database.");

    require_once(getenv('OB_LIB_DIR').'modules_class.php');
    require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    if(!isset($_SESSION['Tproject'])) { 
        require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
        st_col('default_table');
    }
    require_once(getenv('OB_LIB_DIR').'languages.php');

    if (!defined('STYLE_PATH')) {
        $style_def = constant('STYLE');
        $style_name = $style_def['template'];
        define('STYLE_PATH',"/styles/app/".$style_name);
    }

    $modules = new modules($_SESSION['current_query_table']);

    if (isset($_GET['view'])) {
        // view-table mode
        $table = PROJECTTABLE;

        $_SESSION['force_use'] = array('results_asCSV' => true);
        $_SESSION['force_use'] = array('results_asTable' => true);
        $modules->_include('results_asTable','print_table_page',[]);
        unset($_SESSION['force_use']);

    } else {
        # original query mode
        $table = "";

        if ($modules->is_enabled('results_asTable',$table)) {
            $modules->_include('results_asTable','print_table_page',[]);
        } else {
            echo "<h1>Sorry, you have no access to this function.</h1>";
            log_action("results_asTable module not enabled",__FILE__,__LINE__);
        }
    }
}

