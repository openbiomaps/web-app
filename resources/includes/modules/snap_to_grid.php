<?php
#' ---
#' Module:
#'   snap_to_grid
#' Files:
#'   [snap_to_grid]
#' Description: >
#'   Deprecated!
#'   alias: trasform_geometries 
#'   nincs használva, a grid_view teljesen helyettesíti
#' Methods:
#'  [geom_column_join, rules_join]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class snap_to_grid extends module {
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function  geom_column($p,$geom_col) {

        if (is_null($geom_col)) {
            if (isset($_SESSION['st_col']['GEOM_C']) and isset($_SESSION['current_query_table']))
                // if obm_geometry defined in the query table!!!!
                $t = $_SESSION['current_query_table'].'.'.$_SESSION['st_col']['GEOM_C'];
            else
                 $t = "obm_geometry";
            
        } else {
            $t = $geom_col;
        }
        // This module function only used for non login users!!
        if (!isset($_SESSION['Tid']) and !rst('acc')) {
            $params = preg_split('/;/',$p);
            if (count($params)==2) {
                $t = sprintf("ST_AsText(ST_SnapToGrid(%s,%f,%f))",$t,$params[0],$params[1]);
            } else {
                $t = sprintf("ST_AsText(ST_SnapToGrid(%s,0.13,0.09))",$t);
            }
        }
        return $t;
    }
    
    public function geom_column_join($p) {
        return $this->geom_column($p);
    }
    
    public function rules_join($p) {
        return '';
    }
}
?>
