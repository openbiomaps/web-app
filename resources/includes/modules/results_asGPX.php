<?php
#' ---
#' Module:
#'   results_asGPX
#' Files:
#'   [results_asGPX.php]
#' Description: >
#'   Export results as GPX
#' Methods:
#'   [print_data, export_as]
#' Examples:
#'   {"name": "col_1", "description": ["col_2", "col_3", ... ]}
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asGPX extends module {
    
    var $error = '';
    var $retval;
    
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        
        $params = $this->split_params($static_params);
        
        if ($action) {
            $this->retval = $this->$action($params,$dynamic_params);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }
    
    public function print_data($params,$pa) {

        $st_col = st_col($_SESSION['current_query_table'],'array');
        
        if (!isset($st_col['GEOM_C']) or $st_col['GEOM_C']=='') {
            echo common_message("fail", "No geometry column defined.");
            return;
        }
        
        $de = new DataExport((int)$pa['de_id']);
        extract($this->read_params());
        
        $col_idxs = $this->get_column_indexes(array_keys($de->colnames), $st_col['GEOM_C'], $SPECIES_C, $NUM_IND_C);
        
        if (!$col_idxs) {
            echo common_message("error", "Column missing");
            return;
        }
        
        if ($handle = fopen($de->tmpfile(), "r")) {
            
            
            $geomtypes = [];
            while (($row = fgetcsv($handle, $de->maxlength)) !== FALSE) {
                $tmprow = [];
                
                if (substr($row[$col_idxs[0]], 0, 5) === 'POINT') {
                    $gt = 'POINT';
                }
                elseif (substr($row[$col_idxs[0]], 0, 10) === 'LINESTRING') {
                    $gt = 'LINESTRING';
                }
                else {
                    log_action('Unsupported geometry type', __FILE__, __LINE__);
                    continue;
                }
                
                if (!in_array($gt, array_keys($geomtypes))) {
                    
                    $geomtypes[$gt] = [];
                    $geomtypes[$gt]['path'] = OB_TMP . $de->filename . "_" . $gt . ".gpx.csv";
                    $geomtypes[$gt]['handle'] = fopen($geomtypes[$gt]['path'], 'w');
                    fputcsv($geomtypes[$gt]['handle'], ['WKT', 'name', 'desc']);
                }
                $tmprow[] = $row[$col_idxs[0]]; // wkt
                $tmprow[] = $row[$col_idxs[1]]; // Name
                $tmprow[] = implode(' | ', array_map(function ($val) {
                    return defined($val) ? constant($val) : $val;
                }, array_intersect_key($row, array_flip(array_slice($col_idxs, 2))))); // DEscription imploded
                
                fputcsv($geomtypes[$gt]['handle'], $tmprow);
            }
            fclose($handle);
            
            foreach ($geomtypes as $geomtype => $tmpfile) {
                // closing the previously opened files
                fclose($tmpfile['handle']);
                unset($geomtypes[$geomtype]['handle']);

                // transforming the temporary csv files to gpx files using ogr2ogr
                $gpx_filename = "{$de->filename}_{$geomtype}.gpx";
                $gpx_path = OB_TMP . $gpx_filename;
                
                $force_gpx_track = ($geomtype == 'LINESTRING') ? "-lco FORCE_GPX_TRACK=YES" : "";
                $GDAL_PATH = defined("GDAL_PATH") ? constant("GDAL_PATH") : "";
                $cmd = "{$GDAL_PATH}ogr2ogr -a_srs EPSG:4326 -f GPX /vsistdout/ {$tmpfile['path']} -nlt {$geomtype} -oo KEEP_GEOM_COLUMNS=NO $force_gpx_track > $gpx_path";
                $res = exec($cmd);
                $geomtypes[$geomtype]['gpx_filename'] = $gpx_filename;
                $geomtypes[$geomtype]['gpx_path'] = $gpx_path;
                unlink($tmpfile['path']);
                unset($tmpfile['path']);
            }
            
            if (count($geomtypes) == 1) {
                $final_file = array_values($geomtypes)[0];
                if (file_exists($final_file['gpx_path'])) {
                    $file_path = $final_file['gpx_path'];
                    
                    //track_download($track_id);
                    header("Content-Type: text/xml");
                    header("Content-Length: " . filesize($file_path)); 
                    header("Content-Disposition: attachment; filename=\"{$final_file['gpx_filename']}\";");
                    header("Expires: -1");
                    header("Cache-Control: no-store, no-cache, must-revalidate");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    $handle = fopen($file_path, "rb");
                    while (!feof($handle)){
                        echo fread($handle, 8192);
                    }
                    fclose($handle);
                    unlink($file_path);
                    $de->increment_dl_count();
                    return;
                }
                echo "Error. GPX file does not exists.";
                return;
            }
            elseif (count($geomtypes) > 1) {
                $zip_file = $de->filename . '_GPX.zip';
                $zip_path = OB_TMP . $zip_file;
                
                $zip = new ZipArchive;
                
                if (!$zip->open($zip_path,  ZipArchive::CREATE)) {
                    log_action('zip could not be opened', __FILE__,__LINE__);
                    return;
                }
                
                foreach ($geomtypes as $geomtype => $fileinfo) {
                    if (file_exists($fileinfo['gpx_path'])) {
                        $zip->addFile($fileinfo['gpx_path'], $fileinfo['gpx_filename']);
                    }
                }
                if ($zip->numFiles > 0) {
                    $zip->close();
                    
                    if (file_exists($zip_path)) {
                        //    track_download($track_id);
                        # header definíció!!!
                        header("Content-type: application/zip"); 
                        header("Content-Disposition: attachment; filename=$zip_file");
                        header("Content-length: " . filesize($zip_path));
                        header("Pragma: no-cache"); 
                        header("Expires: 0"); 
                        $handle = fopen($zip_path, "rb");
                        while (!feof($handle)){
                            echo fread($handle, 8192);
                        }
                        fclose($handle);
                        
                        // deleting the temporary files
                        unlink($zip_path);
                        array_map('unlink', array_column($geomtypes, 'gpx_path'));
                        $de->increment_dl_count();
                        return;
                    }
                }
                
                echo "Error. See the system log.";
                return;
                
            }
            else {
                echo "Nothing to save";
                return;
            }
        }
    }
    
    private function read_params() {
        $st_col = st_col($_SESSION['current_query_table'],'array');
        
        if (isset($this->params['name'])) {
            $SPECIES_C =  $this->params['name'];
        }
        else {
            if (!isset($st_col['SPECIES_C']) or $st_col['SPECIES_C']=='') {
                $SPECIES_C = "'SPECIES_C'";
            }
            else {
                $SPECIES_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m)) ? $m[2] : $st_col['SPECIES_C'];
            }
        }
        
        // special case obm_taxon - for multilingual taxon names
        if ($SPECIES_C === 'obm_taxon') {
            $SPECIES_C = $st_col['SPECIES_C'];
        }
        
        if (isset($this->params['description'])) {
            $NUM_IND_C = (is_array($this->params['description'])) ? $this->params['description'] : [$this->params['description']];
        }
        else {
            if (!isset($st_col['NUM_IND_C']) or $st_col['NUM_IND_C']=='') {
                $NUM_IND_C = [];
            }
            else {
                $NUM_IND_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['NUM_IND_C'],$m)) ? [$m[2]] : [$st_col['NUM_IND_C']];
            }
        }
        
        return compact('SPECIES_C', 'NUM_IND_C');
    }
    
    private function get_column_indexes($header, $GEOM_C, $SPECIES_C, $NUM_IND_C) {
        $indexes = [];
        foreach (array_merge([$GEOM_C, $SPECIES_C], $NUM_IND_C) as $col) {
            if ($idx = array_search($col, $header)) {
                $indexes[] = $idx;
            }
            else {
                return false;
            }
        }
        return $indexes;
    }
}
?>
