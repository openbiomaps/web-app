$(document).ready(function() {
    $("body").on("click","#get_obm_servers",function(){
        $.post("ajax",{"fetch_obm_servers":"servers"},
            function(data) {
                var j = JSON.parse(data);
                var ddl = $("#share_obm_servers");
                ddl.append("<option></option>");
                for (k = 0; k < j.length; k++) {
                    if (j[k]["domain"]!="")
                        ddl.append("<option value='" + j[k]['domain']+ "'>" + j[k]['institute'] + "</option>");
                }
            });
    });
    $("body").on("change","#share_obm_servers",function(){

        var server = $("#share_obm_servers").val();
        if (server != "") {
            $.post("ajax",{"fetch_obm_projects":server},
                function(data) {
                    var j = JSON.parse(data);
                    $("#share_obm_server_project").html("");
                    var ddl = $("#share_obm_server_project");
                    for (k = 0; k < j.length; k++) {
                        if (j[k]["project_url"]!="")
                            ddl.append("<option value='" + j[k]['project_url']+ "'>" + j[k]['project_table'] + "</option>");
                    }
                    $("#share-data-send-request").show();
                });
        }
    });
    $("body").on("click","#share-data-send-request",function(){
        $.post("ajax",{"shareQuery":$("#share_obm_server_project").val()},
            function(data) {
                var retval = jsendp(data);
                if (retval["status"]=="error") {
                    alert(retval["message"]);
                } else if (retval["status"]=="fail") {
                    alert(retval["data"]);
                } else if (retval["status"]=="success") {
                    alert(retval["data"]);
                }
            });
    });
});

$(document).ready(async function () {
    let resp = await $.getJSON('ajax', { m: 'results_buttons', action: 'translations' })

    if (resp.status != 'success') {
      console.log(new Error(resp.message));
    }
    t['results_buttons'] = resp.data;

})

$(document).on('LoadResults_ready', function (ev) {
    const l = $(".export_data_fg");
    const lbg = $(".export_data_bg");
    l.click(async function (ev) {
        const prnt = $(this).parent()
        const href = $(this).prop('href')
        const url = new URL(href)
        
        window.location = href
        
        $(this).remove()
        prnt.append(t.results_buttons['str_downloadas' + url.searchParams.get('format')])
    })
    lbg.click(async function (ev) {
        try {
            ev.preventDefault();
            const href = $(this).prop('href')
            const url = new URL(href)
            
            const dlr = url.searchParams.get('dlr')
            const prnt = $(this).parent()
            
            if (dlr === 'yes') {
                $("#dlr-form").trigger({
                    type: 'openDlrForm',
                    href: href
                });
            }
            else {
                const res = JSON.parse(await queryExport(url));
                if (res.status !== 'success') {
                    throw new Error(res.message);
                }
                $("#dialog").html(res.data);
                $("#dialog").dialog('open');
            }
            $(this).hide().prop('disabled', true)
            prnt.append(`<div class='blink' style='font-size:150%;'><a href="${obj.url}/profile/${obj.profile_hash}/?options=module_adminpage&m=results_buttons">${t.results_buttons.str_follow_this_link}</a></div>`)
            
        } catch (e) {
            console.log(e);
        }
    })
})

async function queryExport(url, message = "") {
    const resp = await $.get('ajax', {
        m: 'results_buttons',
        action: 'query',
        format: url.searchParams.get('format'),
        filename: url.searchParams.get('filename'),
        bgproc: true,
        message: message
    })
    return resp;
}
