/* text filter module
 *
 * */
var species_plus = new Array();

const daysOfYear = [
    "01-01", "01-02", "01-03", "01-04", "01-05", "01-06", "01-07", "01-08", "01-09", "01-10",
    "01-11", "01-12", "01-13", "01-14", "01-15", "01-16", "01-17", "01-18", "01-19", "01-20",
    "01-21", "01-22", "01-23", "01-24", "01-25", "01-26", "01-27", "01-28", "01-29", "01-30", "01-31",
    "02-01", "02-02", "02-03", "02-04", "02-05", "02-06", "02-07", "02-08", "02-09", "02-10",
    "02-11", "02-12", "02-13", "02-14", "02-15", "02-16", "02-17", "02-18", "02-19", "02-20",
    "02-21", "02-22", "02-23", "02-24", "02-25", "02-26", "02-27", "02-28",
    "03-01", "03-02", "03-03", "03-04", "03-05", "03-06", "03-07", "03-08", "03-09", "03-10",
    "03-11", "03-12", "03-13", "03-14", "03-15", "03-16", "03-17", "03-18", "03-19", "03-20",
    "03-21", "03-22", "03-23", "03-24", "03-25", "03-26", "03-27", "03-28", "03-29", "03-30", "03-31",
    "04-01", "04-02", "04-03", "04-04", "04-05", "04-06", "04-07", "04-08", "04-09", "04-10",
    "04-11", "04-12", "04-13", "04-14", "04-15", "04-16", "04-17", "04-18", "04-19", "04-20",
    "04-21", "04-22", "04-23", "04-24", "04-25", "04-26", "04-27", "04-28", "04-29", "04-30",
    "05-01", "05-02", "05-03", "05-04", "05-05", "05-06", "05-07", "05-08", "05-09", "05-10",
    "05-11", "05-12", "05-13", "05-14", "05-15", "05-16", "05-17", "05-18", "05-19", "05-20",
    "05-21", "05-22", "05-23", "05-24", "05-25", "05-26", "05-27", "05-28", "05-29", "05-30", "05-31",
    "06-01", "06-02", "06-03", "06-04", "06-05", "06-06", "06-07", "06-08", "06-09", "06-10",
    "06-11", "06-12", "06-13", "06-14", "06-15", "06-16", "06-17", "06-18", "06-19", "06-20",
    "06-21", "06-22", "06-23", "06-24", "06-25", "06-26", "06-27", "06-28", "06-29", "06-30",
    "07-01", "07-02", "07-03", "07-04", "07-05", "07-06", "07-07", "07-08", "07-09", "07-10",
    "07-11", "07-12", "07-13", "07-14", "07-15", "07-16", "07-17", "07-18", "07-19", "07-20",
    "07-21", "07-22", "07-23", "07-24", "07-25", "07-26", "07-27", "07-28", "07-29", "07-30", "07-31",
    "08-01", "08-02", "08-03", "08-04", "08-05", "08-06", "08-07", "08-08", "08-09", "08-10",
    "08-11", "08-12", "08-13", "08-14", "08-15", "08-16", "08-17", "08-18", "08-19", "08-20",
    "08-21", "08-22", "08-23", "08-24", "08-25", "08-26", "08-27", "08-28", "08-29", "08-30", "08-31",
    "09-01", "09-02", "09-03", "09-04", "09-05", "09-06", "09-07", "09-08", "09-09", "09-10",
    "09-11", "09-12", "09-13", "09-14", "09-15", "09-16", "09-17", "09-18", "09-19", "09-20",
    "09-21", "09-22", "09-23", "09-24", "09-25", "09-26", "09-27", "09-28", "09-29", "09-30",
    "10-01", "10-02", "10-03", "10-04", "10-05", "10-06", "10-07", "10-08", "10-09", "10-10",
    "10-11", "10-12", "10-13", "10-14", "10-15", "10-16", "10-17", "10-18", "10-19", "10-20",
    "10-21", "10-22", "10-23", "10-24", "10-25", "10-26", "10-27", "10-28", "10-29", "10-30", "10-31",
    "11-01", "11-02", "11-03", "11-04", "11-05", "11-06", "11-07", "11-08", "11-09", "11-10",
    "11-11", "11-12", "11-13", "11-14", "11-15", "11-16", "11-17", "11-18", "11-19", "11-20",
    "11-21", "11-22", "11-23", "11-24", "11-25", "11-26", "11-27", "11-28", "11-29", "11-30",
    "12-01", "12-02", "12-03", "12-04", "12-05", "12-06", "12-07", "12-08", "12-09", "12-10",
    "12-11", "12-12", "12-13", "12-14", "12-15", "12-16", "12-17", "12-18", "12-19", "12-20",
    "12-21", "12-22", "12-23", "12-24", "12-25", "12-26", "12-27", "12-28", "12-29", "12-30", "12-31"
];
  
// taxon_filter function
function setTaxon( message ) {
    var a = message.split('#');
    species_plus = species_plus.concat(a[0] + '#' + a[1]);
    $('<button class="pure-button button-href remove-species" value="' + a[0] + '#' + a[1] + '">' + a[2] + ' &nbsp; <i class="fa fa-close"></i></button>').prependTo( '#tsellist' );
    $('#taxon_sim').val('');
    $('#taxon_sim').html('');
    $('#taxon_sim').text('');
    $('#taxon_sim').focus();
}

function getParamsOf_text_filter() {
    // text filter variables processing
    var taxon_val = '';
    var trgm_string = '';
    var allmatch = 0;
    var onematch = 0;
    var qids = new Array();
    var qval = new Array();
    //cleaning taxon filter
    //$("#tsellist").html('');
    //
    // a map_filter_box.php.inc-ben lévő
    // qf class input/select metők tartalmai
    // kerülnek a myVar objectbe
    $('.qf').each(function(){
        var stri = '';
        var stra = new Array();
        var qid=$(this).attr('id');
        if( $(this).prop('type') == 'text' ) {
            const v = $(this).val()
            if (v) {
                stra.push(v);    
            }
        } else if ($(this).prop('type') == 'button' ) {
            if (($(this).find('i').hasClass('fa-toggle-on'))) {
                stra.push('on');    
            }
        }
        else if ($(this).hasClass('divInput')) {
            const id = [];
            $(this).children().each(function() {
                if ($(this).hasClass('search-relation'))
                    id.push($(this).html());
                else
                    id.push($(this).data('id'));
            });
            if (id.length > 1) {
                stra = id;
            }
        } else {
            $('.mapfb').find('#' + qid + ' option:selected').each(function () {
                const v = $(this).val()
                if (v) {
                    stra.push(v);    
                }
            });
        }
        if (stra.length){
            qids.push(qid);
            qval.push(JSON.stringify(stra));
        }
    });
    /* többszörös fajválasztó */
    if (species_plus.length){
        trgm_string = JSON.stringify(species_plus);
        //species_plus = new Array();
    }
    if ($('#allmatch').is(':checked')) {
        allmatch = 1;
    }
    if ($('#onematch').is(':checked')) {
        onematch = 1;
    }

    var myVar = { trgm_string:trgm_string,allmatch:allmatch,onematch:onematch, }
    // text filter variables
    for (var i=0; i < qids.length; i++) {
        myVar['qids_' + qids[i]] = qval[i];
    }
    return myVar;
}

$(document).ready(function() {
    
    let translations = []
    $.getJSON('ajax', {
        m: 'text_filter2',
        action: 'translations',
    }).done((res) => {
        translations = res.data;
    }).fail((jqxhr, textStatus, error ) => {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    });


    const insert = function insert(main_string, ins_string, pos) {
        if(typeof(pos) == "undefined") {
            pos = 0;
        }
        if(typeof(ins_string) == "undefined") {
            ins_string = '';
        }
        return main_string.slice(0, pos) + ins_string + main_string.slice(pos);
    }
    /**
     * Observers autocomplete
     * **/
    $( function() {
        function split( val, regexp = /,\s*/ ) {
            return val.split( regexp );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( function() {
            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                },
                _renderMenu: function( ul, items ) {
                    var that = this,
                        currentCategory = "";
                    $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                            li.attr( "aria-label", item.category + " : " + item.label );
                        }
                    });
                }
            });

            $( ".multi-autocomplete" ).catcomplete({
                delay: 0,
                minLength: 2,
                source: function( request, response ) {
                    $.getJSON( "ajax", {
                        m: "text_filter2",
                        action: "get_list",
                        table: "terms",
                        filter: extractLast( request.term )
                    }, response );
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    var ids = split( $(this).data('ids'),/,/);
                    
                    // remove the current input
                    terms.pop();
                    
                    // add the selected item
                    terms.push( ui.item.value );
                    ids.push( ui.item.id );

                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );

                    var searchBox = $('#obm_terms_' + ui.item.category);
                    searchBox.append('<div class="'+ui.item.category+' search-badge pure-button" data-id='+ui.item.id+'>'+ui.item.value+'<span>x</span></div>');
                    searchBox.find('.search-relation').removeClass('hidden');
                    this.value = '';
                    return false;
                }
            })
            if ( $('.multi-autocomplete').data() ) {
                const ac = $('.multi-autocomplete').data('customCatcomplete')
                if (ac) {
                    ac._renderItem = function( ul, item ) {
                        const idx = item.meta.toLowerCase().indexOf(ac.term);
                        if (idx > -1) {
                            item.label = insert(item.label, "</strong>", idx + ac.term.length)
                            item.label = insert(item.label, "<strong>", idx)
                        }
                        return $("<li>").append(`<div>${item.label}</div>`).appendTo(ul);
                    } ;
                }
            }
            $('.search-relation').click(function(){
                  $(this).toggleText('AND', 'OR');
            })
        } );
    } );

    $("#mapfilters").on("click",".search-badge span",function(ev) {
        $(this).parent().remove();
    });
    
    text_filter_enabled = 1;
    $('#mapholder').on('click','#taxon-search-settings',function() {
        $('#taxon-search-settings-box').toggle();
    });


    $('#taxon_sim').autocomplete({
        source: function( request, response ) {
            const term = $('#taxon_sim').val(); 
            $.post('ajax', {
                m: "text_filter2",
                action: "get_list",
                table: "taxon",
                filter: term
            }, function (data) {
                data = JSON.parse(data)
                response(data.data);
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            setTaxon( ui.item.id + '#' + ui.item.meta + '#' + ui.item.value);
            $(this).val('');
            return false;
        },
    })
    if ( $('#taxon_sim').data() ) {
        const ac = $('#taxon_sim').data('uiAutocomplete')
        if (ac) {
            ac._renderItem = function( ul, item ) {
                const idx = item.meta.toLowerCase().indexOf(ac.term);
                if (idx > -1) {
                    item.label = insert(item.label, "</strong>", idx + ac.term.length)
                    item.label = insert(item.label, "<strong>", idx)
                }
                return $("<li>").append(`<div>${item.label}</div>`).appendTo(ul);
            } ;
        }
    }

    $("#period-of-year-slider").slider({
      range: true,
      min: 1,
      max: 365,
      values: [1, 365],
      slide: function (event, ui) {
        let display_val = from_val = to_val = '';
        if (ui.values[0] !== 1 || ui.values[1] !== 365) {
            if (obj.language === 'hu') {
                display_val = daysOfYear[ui.values[0] - 1] + translations.str_from + " " + daysOfYear[ui.values[1] - 1] + translations.str_to
            }
            else {
                display_val = translations.str_from + ' ' + daysOfYear[ui.values[0] - 1] + " " + translations.str_to + ' ' + daysOfYear[ui.values[1] - 1]
            }
            
            from_val = daysOfYear[ui.values[0] - 1]
            to_val = daysOfYear[ui.values[1] - 1]
        }

        $("#period-of-year").text(display_val);
        $("#exact_mfrom").val(from_val)
        $("#exact_mto").val(to_val)
      },
    });

    /* remove species
     * */
    $('body').on('click','.remove-species',function() {
        for( var i = 0; i < species_plus.length; i++) {
            if ( species_plus[i] === $(this).val()) {
                species_plus.splice(i, 1);
            }
        }
        $(this).remove();
    });

    $('#colourringdiv').appendTo('#body');


    /* Clear incremntal filters
     *
     * */
    $('#mapfilters').on('click','#clear-filters',function(){
        var thise = $(this);
        $.post('ajax', {'m':'text_filter2','action':'clear_filters'}, function (data) {
            var retval = jsendp(data);
            if (retval['status']=='success') {
                thise.css('background-color','initial');
            }
        });
        species_plus = new Array();
        $('#tsellist').find('button').remove();
        $('.divInput').find('div').remove();
        $("#period-of-year-slider").slider('values', 0, 1)
        $("#period-of-year-slider").slider('values', 1, 365)
        $('.mapfb').find('input:text').val('');
        $('.mapfb').find('textarea').html('');
        $('.mapfb').find('button').each(function() {
            if ($(this).find('i').hasClass('fa-toggle-on')) {
                $(this).find('i').toggleClass('fa-toggle-on');
                $(this).find('i').toggleClass('fa-toggle-off');
                $(this).removeClass('button-success');
            }
        });
        document.querySelectorAll('.mapfb select').forEach((s) => s.value = "")
        geometry_query_neighbour = 0;
        geometry_query_session = 0;
        geometry_query_selection = 0;
        drawLayer.destroyFeatures();
        $('#taxon_trgm').html('');
    });


    $(".filter-sh-advopt").on("change",function() {
        var val = $(this).val();
        const group = $(this).data('group');
        //turning on/off the groupped-nested filters
        if (group) {
            const nextElem =$("input.filter-sh-advopt[value="+group+"]");
            const checked = nextElem.is(":checked");
            nextElem.prop('checked', !checked).trigger('change');
        }
        if($(this).is(':checked')){
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                if($(this).hasClass('def-hidden')){
                    $(this).removeClass('def-hidden');
                }
            });
        } else {
            $(this).closest('table').find('.text-options_'+$(this).val()).each(function(){
                if(!$(this).hasClass('def-hidden')){
                    $(this).addClass('def-hidden');
                    //clear search text if search item is closed
                    $("#" + val).val('');
                }
            });
        }
    });

    // mapfilterbox query text filter autocomplete wrapper
    $('#mapfilters').on('input','.qfautocomplete',function() {
        var ref = $(this).attr('id');
        var thise = $(this);
        // query table
        var etr = $(this).data('etr');
        var nested_column = $(this).data('nested_column');
        var nested_element = $(this).data('nested_element');
        var all_options = $(this).data('all_options');
        $('#'+nested_element+' option').remove();

        // optional extension for speeding up query, if autocomplete table contains a column with clustered/indexed value
        var speedup_filter = 'off';
        var speedup = $(this).closest('.mapfb').find('.speedup_filter');
        if (typeof speedup != 'undefined') {
            if (speedup.val()=='on')
                speedup_filter = 'on';
        }
        thise.css('background-color','white');


        $('.qfautocomplete').autocomplete({

            source: function( request, response ) {
                thise.css('cursor','progress');
                $.post('ajax', {
                    m: 'text_filter2',
                    action: 'qflist',
                    id: ref,
                    etr: etr,
                    term: request.term,
                    speedup_filter: speedup_filter
                }, function (data) {
                    var retval = jsendp(data);
                    if (retval['status']=='error') {
                        thise.css('cursor','default');
                        thise.css('background-color','red');

                        if (retval['message'] == 'Page expired') {
                            alert(retval['message']);
                            location.reload();
                        }
                    } else if (retval['status']=='fail') {
                        alert('autocompleting failed');
                    } else if (retval['status']=='success') {
                        response(JSON.parse(retval['data']));
                    }
                    thise.css('cursor','default');
                });
            },
            select: function( event, ui ) {
                // select element from list fireing the nested list creation
                if (typeof nested_element != 'undefined') {
                    if (ui.item.value=='')
                        return;

                    thise.css('cursor','progress');
                    $.post('ajax', {
                        'm':'text_filter2',
                        'action': 'create_dynamic_menu',
                        'filterColumn':ref,
                        'etr':etr,
                        'filterValue':ui.item.value,
                        'nested_column':nested_column,
                        'nested_element':nested_element,
                        'allOptions':all_options
                    }, function (data) {
                        data = JSON.parse(data)
                        $('#'+nested_element).append(data.data);
                        $('#'+nested_element).trigger('nested_data_appended');
                        thise.css('cursor','default');
                    });
                }
            },
            minLength: 1,
        });
    });

    /* obm_datum
     * date query events
     * */
    $( "#obm_uploading_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      constrainInput: false,
    });
    $( "#exact_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      constrainInput: false,
    });
    $( "#exact_from" ).datepicker({
      defaultDate: "-1w",
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#exact_to" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'yy-mm-dd',
      firstDay:1,
      onClose: function( selectedDate ) {
        $( "#exact_from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    // nested menus ajax trigger call
    $('#mapfilters').on('change select','.nested',function() {
        var ref = $(this).attr('id');
        // query table
        var nested_element = $(this).data('nested_element');
        $("#"+nested_element+" option").remove();

        // if this list creates an other list depending on the chosen value
        var filter_column = $('#'+nested_element).data('filtercolumn');
        var etr = $('#'+nested_element).data('etr');
        var value_column = $('#'+nested_element).data('valuecolumn');
        var label_column = $('#'+nested_element).data('labelcolumn');

        var selectted_option =$("option:selected",this);
        var filter_term = (selectted_option.data('id')) ? selectted_option.data('id') : selectted_option.attr('value');

        var all_options = $(this).data('alloptions');
        if ($("option:selected",this).text()=='') {
            return;
        }
        $.post("ajax", {
            m: 'text_filter2',
            action: 'create_dynamic_menu',
            filterColumn: filter_column,
            etr: etr,
            filterValue:  filter_term,
            nested_element: nested_element,
            allOptions: all_options,
            valueColumn: value_column,
            labelColumn: label_column
        },
        function (data) {
            data = JSON.parse(data)
            $('#'+nested_element).append(data.data);
            $('#'+nested_element).trigger('nested_data_appended');
        });
    });


    /* Text filter admin interface
     *
     * */
    $("body").on("click",".delete-text-filter-param",function() {
        let data = JSON.parse(window.atob($("#text-filter-params").data('json')));
        const target = $(this).next('span').html();
        delete data[target];

        $('#text-filter-params').data('json', window.btoa(JSON.stringify(data)))
        $('#text-filter-params').siblings('textarea').html(JSON.stringify(data));
        $(this).parent().remove();
    });

    $("body").on("click",".add-text-filter-param",async function() {
        $('.basic-settings').removeClass('hidden');

        let buttons = $('.text-filter-buttons');

        buttons.html("<button class='pure-button button-small button-warning add-text-filter-param'><i class='fa fa-refresh'></i> Reset </button>");
        buttons.append("<button class='pure-button button-small button-success set-text-filter-param'><i class='fa fa-floppy-o'></i> Set </button>");
        buttons.append("<button class='pure-button button-small button-success advanced-settings-btn'><i class='fa fa-wrech'></i> Advanced settings </button>");

    });


    $('body').on('click','.advanced-settings-btn', async function() {
        $(this).remove();
        $('.advanced-settings').removeClass('hidden');

    });


    $('body').on('change','#optionsTable', async function() {
        try {
            let mtable = this.value;
            
            const column_list = await $.getJSON('ajax',{
                'm':'text_filter2',
                'action': 'getColumnList',
                'mtable': mtable,
                'id': 'valueColumn',
                'oo': true,
            });
            
            if (column_list.status !== 'success') {
                throw column_list.message
            }
            
            $('#valueColumn').html(column_list.data);
            $('#preFilterColumn').html(column_list.data);
        } catch (e) {
            console.log(new Error(e));
        } 
        
        
    });

    $('body').on('change','#preFilterColumn', async function() {
        try {
            let table = $('#optionsTable option:selected').text();
            if (this.value == '') {
                $('#preFilterValue').html('');
            }
            else {
                const preFilterValue = await $.getJSON('ajax',{
                    'm':'text_filter2',
                    'action': 'getDistinctValues',
                    'mtable': table,
                    'column': this.value,
                    'id': 'preFilterValue',
                    'oo': true,
                });
                if (preFilterValue.status !== 'success') {
                    throw preFilterValue.message;
                }
                $('#preFilterValue').html(preFilterValue.data);
            }
        } catch (e) {
            console.log(new Error(e));
        }
    });

    $("body").on("click", ".set-text-filter-param", async function() {
        var tfp = $('.tfp');
        const params_string = window.atob($("#text-filter-params").data('json'));
        let data = (params_string.length) ? JSON.parse(params_string) : {};
        let newParams = '';
        let target = '';

        tfp.each(function(elem) {
            if (this.id == 'target_column') {
                target = this.value;
                data[target] = {};
                newParams += '<div class="text-filter-param" style="background: wheat"> <button class="edit-text-filter-param pure-button button-small button-warning"><i class="fa fa-edit"> </i> </button> <button class="delete-text-filter-param pure-button button-small button-danger"><i class="fa fa-trash-o"> </i> </button> <span>'+ this.value +'</span> </div>';
            }
            const paramOptions = ['optionsTable','valueColumn','preFilterColumn'];
            if ((paramOptions.indexOf(this.id) >= 0) && (this.value !== '')) {
                data[target][this.id] = this.value
            }
            if (this.id == 'preFilterValue' && this.value !== '') {
                const options = this.selectedOptions;
                let values = [];
                $.each(options,function(key, elem) {
                    values.push(elem.value);
                });
                data[target]['preFilterValue'] = values;
            }
            if (this.id == 'autocomplete' && this.checked) {
                data[target]['autocomplete'] = 'true';
            }


        });

        $('#text-filter-params').data('json', window.btoa(JSON.stringify(data)));
        $('#text-filter-params').append(newParams);
        $('#text-filter-params').siblings('textarea').html(JSON.stringify(data));
    });




});
