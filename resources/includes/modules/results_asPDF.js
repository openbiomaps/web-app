$(document).on('LoadResults_ready', function (ev) {
    $('.genreport').click(async function() {
        try {
            const parameters = {
                m: 'results_asPDF',
                action: 'genreport',
                report: $(this).data('report')
            }
            const resp = await $.get('ajax', parameters);
        } catch (e) {
            console.log(e)
        }
    })
})

