<?php
#' ---
#' Module:
#'    results_summary
#' Files:
#'   [results_summary.php]
#' Description: >
#'   Query results short summary
#' Methods:
#'   [print_results_summary_count]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_summary extends module {
    public $error = '';

    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_results_summary_count($params) {
        global $ID;

        if (!has_access('master') and $_SESSION['st_col']['USE_RULES']) {
            if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
                $tgroups = 0;
            else
                $tgroups = $_SESSION['Tgroups'];

            // exclude sensitive data if rules allowed
            $cmd = sprintf('SELECT COUNT(DISTINCT obm_id) AS c FROM temporary_tables.temp_query_%1$s_%2$s
                    LEFT JOIN %1$s_rules r ON ("data_table"=\'%4$s\' AND obm_id=r.row_id) 
                    WHERE 
                    (sensitivity::varchar IN (\'1\',\'2\',\'3\',\'no-geom\',\'restricted\',\'sensitive\',\'only-own\') AND ARRAY[%3$s] && read) OR sensitivity::varchar IN (\'0\',\'public\') OR sensitivity IS NULL',PROJECTTABLE,session_id(),$tgroups,$_SESSION['current_query_table']);
            $f = t(str_public_accessible_records);
            //$cmd = sprintf('SELECT COUNT(DISTINCT obm_id) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());

        } else {
            $f = t(str_recordsfound);
            //$cmd = sprintf('SELECT count(*) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());
            $cmd = sprintf('SELECT COUNT(DISTINCT obm_id) as c FROM temporary_tables.temp_query_%s_%s',PROJECTTABLE,session_id());
        }

        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        return sprintf('%s: %d',$f,$row['c']);
    }
}
?>
