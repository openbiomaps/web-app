<?php
#' ---
#' Module:
#'   allowed_columns
#' Files:
#'   [allowed_columns.php]
#' Description: >
#'   columns visible for users in different access level
#'   it was the no_login_columns
#' Methods:
#'   [return_columns, return_gcolumns]
#' Examples: >
#'   {
#'     "for_sensitive_data": [
#'       "species",
#'       "number_of_individuals"
#'     ],
#'     "for_no-geom_data": [
#'       "species"
#'     ]
#'   }
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class allowed_columns extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    /* returns with a subset of given list of columns
     *
     * */
    public function return_columns ($p,$main_cols,$for_data='for_sensitive_data') {
        $c = array();
        $params = $this->split_params($p,'pmi');

        if (isset($params[$for_data]) and !is_array($params[$for_data])) {
            $pm = $params[$for_data];
            if (preg_match('/\*\*/',$pm))
                $c = preg_split('/\*\*/',$pm); // lefelé kompatibilitás
            else
                $c = preg_split('/,/',$pm); // lefelé kompatibilitás
        } elseif (isset($params[$for_data])) {
            $c = $params[$for_data];
        }
        if (!count($c)) {
            st_col($_SESSION['current_query_table']);
            //default options: Consortium statement
            if ($_SESSION['st_col']['SPECIES_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['SPECIES_C']);
            if ($_SESSION['st_col']['NUM_IND_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['NUM_IND_C']);
            foreach($_SESSION['st_col']['DATE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
            foreach($_SESSION['st_col']['CITE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
        }

#        if (count($params)) {
#            foreach ($params as $pkey=>$pm) {
#                if ($pkey == $for_data) {
#                    if (!is_array($pm)) {
#                        if (preg_match('/\*\*/',$pm))
#                            $c = preg_split('/\*\*/',$pm); 
#                        else
#                            $c = preg_split('/,/',$pm); 
#                    } else
#                        $c = $pm;
#                }
#            }
#        } else {
#            //default options: Consortium statement
#            if ($_SESSION['st_col']['SPECIES_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['SPECIES_C']);
#            if ($_SESSION['st_col']['NUM_IND_C']!='') $c[] = sprintf('%s',$_SESSION['st_col']['NUM_IND_C']);
#            foreach($_SESSION['st_col']['DATE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
#            foreach($_SESSION['st_col']['CITE_C'] as $i) if ($i!='') $c[] = sprintf('%s',$i);
#        } 

        $final_list = array();
        foreach($c as $ce) {
            if ( in_array($ce,$main_cols) )
                $final_list[] = $ce;
        }

        return($final_list);

    }
    /* list of allowed column for geometry restricted data 
     * */
    public function return_gcolumns ($p,$main_cols) {
        return $this->return_columns($p,$main_cols,'for_no-geom_data');
    }
}
?>
