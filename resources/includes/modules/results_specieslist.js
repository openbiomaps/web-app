$(document).on('LoadResults_ready', function (ev) {
  var initial = {}
  const prepareResultsSpecieslist = (async () => {
    try {
      let resp = await $.getJSON('ajax', {
        action: 'get_option',
        option_name: 'open_species_list'
      })
      if (resp.status !== 'success') {
        throw resp.message
      }
      initial.species_list_opened = (resp.data === 'true')

      resp = await $.getJSON('ajax', {
        m: 'results_specieslist',
        action: 'translations'
      })
      initial.translations = resp
    } catch (e) {
      console.log(e)
    }
  })()

  Vue.component('specieslist-table', {
    data () {
      return {
        t: initial.translations,
        url: window.obj.url
      }
    },
    template: `
    <table class='specieslist-table'>
      <tr><th>{{ t.str_species }}</th><th>{{ t.str_no_records }}</th><th>{{ t.str_no_inds }}</th><th>{{ t.str_view }}</th></tr>
      <tr v-for="sp in species_list">
        <td class="species-name"><a :href="url + '?metaname&id=' + sp.taxon_id" target="_blank">{{ sp.name }}</a></td>
        <td class="species-count">{{ sp.count }}</td>
        <td class="species-num">{{ sp.num }}</td>
        <td>
          <i :title="t.str_view" class="fa fa-map" @click="recursive_species_filter(sp)"></i>
        </td>
      </tr>
    </table>
    `,
    props: {
      species_list: Array
    },
    methods: {
      recursive_species_filter (sp) {
        var trgmString = JSON.stringify(new Array('' + sp.taxon_id + '#' + sp.name + ''))
        var myVar = {
          trgm_string: trgmString,
          allmatch: 1,
          onematch: 0,
          morefilter: 2
        }
        window.loadQueryMap(myVar)
      }
    }
  })

  Vue.component('specieslist-orderby-selector', {
    data () {
      return {
        orderby: this.selected,
        t: initial.translations
      }
    },
    template: `
    <div id="specieslist-order">
      <label for="order_of_results">{{ t.str_order_of_results }}: </label>
      <select id="order_of_results" v-model="orderby" @change="emitReorder">
        <option v-for="o in ordering_categories" :key="o" :value="o"> {{t['str_' + o] ?? o}} </option>
      </select>
    </div>
    `,
    props: {
      ordering_categories: Array,
      selected: String
    },
    mounted () {
        this.emitReorder()
    },
    methods: {
      emitReorder () {
        this.$emit('reorder', this.orderby)
      }
    }
  })

  Vue.component('specieslist-summary', {
    data () {
      return {
        t: initial.translations,
        showRank: this.spl_data.ranks
      }
    },
    props: {
      spl_data: Object
    },
    template: `
      <div class="specieslist-summary">
        <p> {{ t.str_no_of_taxons }}: <span v-if="!spl_data.ranks"> {{ spl_data.species_list.length }} </span> </p>
        <ul v-if="spl_data.ranks">
          <li v-for="rank in summary">
            <input v-model="showRank" type="checkbox" :value="rank.r" @change="$emit('filter_list', showRank)">
            <label :for="rank.r">{{ t['str_' + rank.r] ?? rank.r }}: {{ rank.c }}</label>
          </li>
        </ul>
      </div>
    `,
    computed: {
      summary () {
        if (this.spl_data.ranks) {
          return this.spl_data.ranks.map(r => {
            const c = this.spl_data.species_list.filter(sp => sp.rank === r).length
            return { r: r, c: c }
          })
        }
      }
    }
  })

  Vue.component('specieslist-content', {
    data () {
      return {
        showRank: this.spl_data.ranks ?? false
      }
    },
    template: `
    <div> 
      <div v-if='spl_data.error' class="message-error"> {{ spl_data.error }} </div>
      <div v-else>
        <specieslist-summary :spl_data="spl_data" v-on:filter_list="showRank = $event"></specieslist-summary>
        <specieslist-orderby-selector 
          v-if="spl_data.ordering_categories" 
          :ordering_categories="spl_data.ordering_categories"
          :selected="spl_data.selected_order"
          v-on:reorder="reorder"
        >
        </specieslist-orderby-selector>
        <div id="specieslist-tables">
          <specieslist-table v-for="list in speciesListSliced" :species_list="list"></specieslist-table>
        </div>
      </div>
    </div>
    `,
    props: {
      spl_data: Object
    },
    methods: {
      reorder (e) {
        this.spl_data.species_list.sort((a, b) => {
          if (a[e] < b[e]) {
            return -1
          } else if (a[e] > b[e]) {
            return 1
          } else {
            return 0
          }
        })
      }
    },
    computed: {
      noOfSlices () {
        const fl = Math.ceil((this.speciesList.length) / 30)
        return fl >= 3 ? 3 : fl
      },
      speciesList () {
        return (this.spl_data.ranks)
          ? this.spl_data.species_list.filter(sp => this.showRank.indexOf(sp.rank) > -1)
          : this.spl_data.species_list
      },
      speciesListSliced () {
        const slicer = Math.floor(this.speciesList.length / this.noOfSlices)
        const speciesListSliced = []
        for (var i = 0; i < this.noOfSlices; i++) {
          const start = i * slicer
          const end = (i !== this.noOfSlices - 1) ? (i + 1) * slicer : this.speciesList.length
          speciesListSliced[i] = this.speciesList.slice(start, end)
        }
        return speciesListSliced
      }
    }
  })

  Vue.component('specieslist-download', {
    data () {
      return {
        t: initial.translations,
        href: "#",
        filename: ""
      }
    },
    props: {
      spl_data: Object,
    },
    template: `
        <a :href="href" @click="downloadCSV" :download="filename" target="_blank" class="pure-button button-href"><i class="fa fa-download"> </i> {{ t.str_download_specieslist }} </a>
    `,
    methods: {
      downloadCSV () {
        this.href = this.encodedUri;
        this.filename = 'species_list.csv'
      }
    },
    computed: {
      encodedUri () {
        return encodeURI(this.csvContent)
      },
      csvContent () {
        const rows = this.spl_data.species_list.map(s => [s.name, s.count, s.num])
        rows.unshift(['name', 'count', 'num']);
        let csvContent = "data:text/csv;charset=utf-8," 
        + rows.map(e => e.join(",")).join("\n");
        return csvContent
      }
    }
  })
  
  Vue.component('specieslist', {
    data () {
      return {
        url: window.obj.url,
        autoOpen: initial.species_list_opened,
        open: false,
        t: initial.translations,
        spl_data: {
          species_list: [],
          error: false
        }
      }
    },
    template: `
    <div id="specieslist">
      <div id="specieslist-toggle" @click="toggleSpeciesList"> 
        <i :class="faClass"></i> 
        {{ t.str_specieslist }}: 
      </div>
      <div v-if="open" >
        <specieslist-content :spl_data="spl_data"></specieslist-content>
        <specieslist-download :spl_data="spl_data"></specieslist-download>
      </div>
    </div>
    `,
    computed: {
      faClass () {
        return 'fa ' + ((this.open) ? 'fa-minus' : 'fa-plus') + ' fa-lg'
      }
    },
    created() {
        if (this.autoOpen) {
            this.toggleSpeciesList()
        }
    },
    methods: {
      async toggleSpeciesList() {
        try {
          if (!this.open && this.spl_data.species_list.length === 0) {
            const resp = await $.getJSON('ajax', {
              m: 'results_specieslist',
              action: 'get_species_list'
            })
            if (resp.status !== 'success') {
              throw resp.message
            }
            this.spl_data = resp.data
          }
          this.open = !this.open
        } catch (e) {
          console.log(e)
        }
      }
    }
  })

  Vue.component('citelist', {
    template: `
    <div id="citelist">
      <div id="citelist-toggle" @click="get_cite_list">
        <i :class="faClass"></i>
        {{ t.str_inthisquerythefollowing }}:
      </div>
      <div v-if="open" id="citelist-content">
        <p>{{ cite_data.cite_list.join(', ')}}</p>
      </div>
    </div>
    `,
    data () {
      return {
        open: false,
        t: initial.translations,
        cite_data: {
          cite_list: [],
          error: false
        }
      }
    },
    computed: {
      faClass () {
        return 'fa ' + ((this.open) ? 'fa-minus' : 'fa-plus') + ' fa-lg'
      }
    },
    methods: {
      async get_cite_list () {
        try {
          if (!this.open && this.cite_data.cite_list.length === 0) {
            const resp = await $.getJSON('ajax', {
              m: 'results_specieslist',
              action: 'get_cite_list'
            })
            if (resp.status !== 'success') {
              throw resp.message
            }
            this.cite_data = resp.data
          }
          this.open = !this.open
        } catch (e) {
          console.log(e)
        }
      }
    }
  })

  prepareResultsSpecieslist.then(() => {
    const l = new Vue({
      el: '#results_specieslist',
      template: `
      <div>
        <specieslist></specieslist>
        <citelist></citelist>
      </div>
      `,
      data () {
        return {
        }
      }
    })
  })
})
