<?php
#' ---
#' Module:
#'   results_asStable
#' Files:
#'   [results_asStable.php]
#' Description: >
#'   print_roll_table
#'   compact table Stable
#' Methods:
#'   [print_query_results, print_results_selector_button, print_js, get_default_view, fill_stable_with_columns]
#' Examples:
#'   {
#'      "default": "on",
#'      "columns": [
#'         "date",
#'         "species",
#'         "observer"
#'      ]
#'   }
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asStable extends module {
    public $error = '';
    public $retval = null;
    
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        $this->params = $this->split_params($static_params);
        if ($action)
            $this->retval = $this->$action($this->params, $dynamic_params);
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function get_default_view($params,$p) {
        $p = $this->split_params($params);
        if (isset($p['default']))
           return 'stable'; 
    }

    public function print_results_selector_button($params,$p) {
        $button = new button(["icon"=>'fa fa-eye']);
        $b = $button->print(["title"=>t(str_as_compact_table),"class"=>"button-href pure-button","onclick"=>"switch_stable()"]);
        
        return sprintf("%s ",$b);
    }
    // return  ?html content
    // $p = [$this->method,$data,$this]
    public function print_query_results($params,$p) {
 
        if ($p[0] == 'stable') {
            $p[1]->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'uploading_id'=>str_dataupid),$p[1]->csv_header);
            return $this->print_roll_table($params, array($p[1],$p[2]));
        } 
        return false;
    }
    public function ajax($params,$request) {
    
        if(isset($_POST['switch-to-stable'])) {

            $reverse = '';
            if (isset($_POST['order']))
                $reverse = $_POST['order'];

            $method = 'stable';

            if(!isset($_SESSION['show_results'])) {
                $_SESSION['show_results'] = array();
            }
            $_SESSION['show_results']['type'] = $method;
            /*$_SESSION['show_results']['rowHeight'] = '30';
            $_SESSION['show_results']['cellWidth'] = '120';
            $_SESSION['show_results']['borderRight'] = '1px solid lightgray';
            $_SESSION['show_results']['borderBottom'] = '1px solid lightgray';*/

            include(getenv('OB_LIB_DIR').'results_builder.php');
            $r = new results_builder(['method'=>$method,'module_ref'=>'results_asStable','reverse'=>$reverse]);
            if ( $r->results_query() ) {
                $r->printOut(['content'=>'update']);
                exit;
            }
            echo common_message('error',$r->error);
        }
        exit;
    }

    public function print_js($params) {
        $js = 'function switch_stable() {
        $.post("ajax", {m:"results_asStable","switch-to-stable":"stable"},
        function(data){
            var retval = jsendp(data);

            if (retval["status"]=="error") {
                $( "#dialog" ).text(retval["message"]);
            } else if (retval["status"]=="fail") {
                $( "#dialog" ).text("Invalid response received.");
                console.log(retval["data"]);
            } else if (retval["status"]=="success") {
                v = retval["data"];
                let rolltable = v.rolltable;
                rr.set({rowHeight:rolltable.rowHeight,cellWidth:rolltable.cellWidth,borderRight:rolltable.borderRight,borderBottom:rolltable.borderBottom,arrayType:rolltable.arrayType});
                var va = JSON.parse(rolltable.scrollbar_header)
                rr.prepare(va["w"],va["i"]);
                rr.render();
                $("#scrollbar").show();
                $("#drbc").height(eval($(".scrollbar").height()+100));
            }
        });}

$(document).ready(function() {
    $("#drbc").on("click",".sort",function(e){
        var viewport = $(this).parent();
        //e.preventDefault();
        arrayType = viewport.data("type");
 
        //fulltable
        if (arrayType == "rolltable") {
            $.post("ajax", {rolltable_order:1,order:viewport.attr("id")},
                function(data) {
                location.reload();
            });

        
        } else {

            // stable
            $.post("ajax", {m:"results_asStable","switch-to-stable":"stable",order:viewport.attr("id").substring(7)},
            function(data){
                var retval = jsendp(data);
                if (retval["status"]=="error") {
                    $( "#dialog" ).text(retval["message"]);

                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                } else if (retval["status"]=="fail") {
                    $( "#dialog" ).text("Invalid response received."); 
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                    console.log(retval["data"]);
                } else if (retval["status"]=="success") {
                    v = retval["data"];
                    let rolltable = v.rolltable;
                    rr.set({rowHeight:rolltable.rowHeight,cellWidth:rolltable.cellWidth,borderRight:rolltable.borderRight,borderBottom:rolltable.borderBottom,arrayType:rolltable.arrayType});
                    var va = JSON.parse(rolltable.scrollbar_header)
                    rr.prepare(va["w"],va["i"]);
                    rr.render();
                    $("#scrollbar").show();
                    $("#drbc").height(eval($(".scrollbar").height()+100));
                }
            });
        }
    });
});';
        return $js;
    }
    private function print_roll_table($params, $dp) {
        global $ID,$BID,$modules;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        $session_id = session_id();

        $out = "";
        $chk = '';
        // params = columns
        if (isset($params['columns']))
            $params = $params['columns'];

        foreach($params as &$sValue)
        {
             if ( $sValue=='obm_uploader_name' || $sValue=='obm_uploader_user' ) $sValue='uploader_name';
             if ( $sValue == 'obm_filename' ) $sValue = 'filename';
             if ( $sValue === 'obm_taxon') $sValue = $st_col['SPECIES_C'];
        }

        $aa = array();
        foreach ($dp[0]->csv_header as $k=>$v) {
            if (!in_array($k,$params)) {
                $aa[] = $k;
                unset($dp[0]->csv_header[$k]);
            }
        }
        //looking for geometry column, if exists, search for it's id
        //$geom_key = $_SESSION['st_col']['GEOM_C'];

        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;

        if (!isset($_SESSION['lock_tables'])) $_SESSION['lock_tables'] = array();

        /*$lock_table = sprintf("temp_roll_stable_%s_%s",PROJECTTABLE,$session_id);

        if (in_array($lock_table,$_SESSION['lock_tables'])) {
            # send wait for the response message to user | or offer kill option?
            # Debugging misterious MILVUS's COPY ... bug
            debug('WAIT! '.$lock_table." is locked!!!");
        } else {
            //debug("Locking table: ".$lock_table);
            $_SESSION['lock_tables'][] = $lock_table;
        }*/

        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_roll_stable_%s_%s",PROJECTTABLE,$session_id));
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_roll_stable_%s_%s (rlist text, ord integer)",PROJECTTABLE,$session_id);
        pg_query($ID,$cmd);
        update_temp_table_index('roll_stable_'.PROJECTTABLE.'_'.$session_id);

        if (isset($_SESSION['orderby']))
            $orderby = sprintf('ORDER BY %1$s %2$s',$_SESSION['orderby'],$_SESSION['orderascd']);
        else
            $orderby = '';

        // ignore joins - should be optional!!
        if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
            $res = pg_query($ID,sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t %3$s',PROJECTTABLE,$session_id,$orderby));
        else
            $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s %3$s',PROJECTTABLE,$session_id,$orderby));

        //
        $rows_count = pg_num_rows($res);

        // create the temp_roll_stable_... table with empty rows.
        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_stable_%s_%s FROM STDIN",PROJECTTABLE,$session_id));
        $order = 0;
        for ($i=0; $i<$rows_count; $i++ ) {
            pg_put_line($ID, NULL."\t$order\n");
            $order++;
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

        // visible columns
        extract(allowed_columns($modules,$_SESSION['current_query_table']));

        $MASTER = has_access('master');

        # Ha nincs master és nincs engedélyezve az allowed columns, akkor láthatóak lesznek az adatok!

        // fill_stable_with_columns();
        // Fill stable with column content as background process as it slow to wait and can hung up the site
        #foreach($_SESSION as $key=>$val){
        #    debug($key);
        #    debug($val);
        #}
        $fsession = array();
        $fsession['orderby'] = $_SESSION['orderby'];
        $fsession['orderad'] = $_SESSION['orderad'];
        $fsession['orderascd'] = $_SESSION['orderascd'];
        $fsession['ignore_joined_tables'] = $_SESSION['ignore_joined_tables'];
        $fsession['current_query_table'] = $_SESSION['current_query_table'];
        if (isset($_SESSION['Tid'])) $fsession['Tid'] =  $_SESSION['Tid'];
        if (isset($_SESSION['Tgroups'])) $fsession['Tgroups'] = $_SESSION['Tgroups'];
        if (isset($_SESSION['Tstatus'])) $fsession['Tstatus'] = $_SESSION['Tstatus'];
        $fsession['st_col'] = $_SESSION['st_col'];
        if (isset($_SESSION['Trole_id'])) $fsession['Trole_id'] = $_SESSION['Trole_id'];
        $fsession['modules'] = $modules;
        $fsession['LANG'] = $_SESSION['LANG'];

        $vars = array();
        $vars['allowed_cols_module'] = $allowed_cols_module;
        #$vars['ACC_LEVEL'] = $ACC_LEVEL;
        $vars['stable_header'] = $dp[0]->csv_header;
        $vars['protocol'] = $protocol;
        $vars['session'] = $fsession;
        $vars['tgroups'] = $tgroups;
        $vars['allowed_cols'] = $allowed_cols;
        $vars['allowed_gcols'] = $allowed_gcols;
        $vars['MASTER'] = $MASTER;
        $vars['SERVER'] = $_SERVER;

        $svars = base64_encode(json_encode($vars));
        putenv("allvars_$session_id=$svars");

        $this->fill_stable_with_columns(array(),array($vars,$session_id,80));

        if (!file_exists(getenv('OB_LIB_DIR').'modules/fill_stable_with_column.php'))
            debug(getenv('OB_LIB_DIR').'modules/fill_stable_with_column.php not exists in '.dirname(__FILE__));

        $phpbin = trim(shell_exec('which php'));
        //debug("nohup $phpbin ".getenv('OB_LIB_DIR')."modules/fill_stable_with_column.php $session_id >/dev/null 2>/dev/null &");
        exec("nohup $phpbin ".getenv('OB_LIB_DIR')."modules/fill_stable_with_column.php $session_id >/dev/null 2>/dev/null &");



/*
        pg_query($ID, sprintf("COPY temporary_tables.temp_roll_stable_%s_%s FROM STDIN",PROJECTTABLE,$session_id));
        $order = 0;
        while ($row = pg_fetch_assoc($res)) {

            #$h = array();
            #$td[$i] = array();
            // skipping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($row)==1) {
                if ($row[0]==$chk) continue;
                else $chk = $row[0];
            }

            # Nem lehet rst query-t csinálni rolltáblában, mert kifagy!!!
            #$acc = 1;
            #if (!rst('acc',$row['obm_id'],$_SESSION['current_query_table'])) {
            #    $acc = 0;
            #}

            $geometry_restriction = 0;
            $column_restriction = 0;
            if (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='1' or $row['obm_sensitivity']=='no-geom')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $geometry_restriction = 1;
            }
            elseif (!$GRST and isset($row['obm_sensitivity']) and ($row['obm_sensitivity']=='2' or $row['obm_sensitivity']=='restricted')) {
                if (!count(array_intersect(explode(',',$tgroups),explode(',',$row['obm_read']))))
                    $column_restriction = 1;
            }


            $line = array();
            //$drop_line = 0;
            foreach ($data->csv_header as $k=>$v) {
                if (isset($row[$k])) {

                    if ($modules->is_enabled('transform_data')) {
                        //default itegrated module
                        $line[$k] = "<div class='viewport'>".$modules->_include('transform_data','text_transform',array($row[$k],$k,''),true)."</div>";
                    } else {
                        $line[$k] = "<div class='viewport'>".$row[$k]."</div>";
                    }

                    if ($column_restriction) {
                        if ( !in_array($k,$allowed_cols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }
                    elseif ($geometry_restriction ) {
                        if ( $k == 'obm_geometry')
                            $line[$k] = '&nbsp;';
                        if ( !in_array($k,$allowed_gcols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    if ( $k == 'obm_files_id') {
                        if ($column_restriction) {
                            if ( !in_array($k,$allowed_cols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        elseif ($geometry_restriction ) {
                            if ( !in_array($k,$allowed_gcols) ) {
                                $line[$k] = '&nbsp;';
                            }
                        }
                        else
                            $line[$k] = "<a href='getphoto?connection={$row[$k]}' class='photolink'><i class='fa fa-camera'></i></a>";
                    }
                    if ( $k == 'obm_uploader_user') {
                        $line[$k] = $row['obm_uploader_name'];
                    }

                } else {
                    $line[$k] = "&nbsp;";
                }
            }

            $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

            //prepend details buttun
            $id = $row['obm_id'];
            $uid = $row['upid'];
            // SHINYURL global control variable
            // create readable links instead of long get urls
            if (defined('SHINYURL') and constant("SHINYURL")) {
                $eurl = sprintf('%://'.URL.'/%s/data/%s/',$protocol,$_SESSION['current_query_table'],$id);
                $hurl = sprintf('%://'.URL.'/%s/uplinf/%s/',$protocol,$_SESSION['current_query_table'],$uid);
            } else {
                $eurl = sprintf('%://'.URL.'/index.php?data&id=%s&table=%s',$protocol,$id,$_SESSION['current_query_table']);
                $hurl = sprintf('%://'.URL.'/index.php?history=upload&id=%s&table=%s',$protocol,$uid,$_SESSION['current_query_table']);
            }

            $b_id = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$eurl,str_details);
            $b_upid = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary'><i class='fa fa-info'></i> %s</a>",$hurl,str_upload);
            array_unshift($line,$b_upid);
            array_unshift($line,$b_id);

            //if (!$drop_line)
            pg_put_line($ID, json_encode($line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT)."\t$order\n");
            $order++;
            //else
            //    pg_put_line($ID, json_encode(array('content disabled'),JSON_UNESCAPED_UNICODE)."\n");
            //$values_long[] = sprintf('%s',json_encode($l,JSON_HEX_QUOT));

            //sleep(1);
        }
        pg_put_line($ID, "\\.\n");
        pg_end_copy($ID);

*/

        /*if (($key = array_search($lock_table, $_SESSION['lock_tables'])) !== false) {
            unset($_SESSION['lock_tables'][$key]);
            //debug("Unlock table: ".$lock_table);
        }*/

        //pg_close($ID);
        //temp_stable_list_::
        //$res1 = pg_copy_from($ID, sprintf('temporary_tables.temp_roll_stable_%s_%s',PROJECTTABLE,$session_id), $values_long);
        //if($res1===false) {
        //    log_action("FAILED to insert temporary_tables.temp_roll_stable: cmd",__FILE__,__LINE__);
        //}

        $dp[0]->csv_header = array_merge(array(str_details),$dp[0]->csv_header);

        $k = count($dp[0]->csv_header)*121;
        //$columns = array_keys($data->csv_header);
        $viewports = '';
        foreach($dp[0]->csv_header as $key=>$value) {
            $viewports .= "<div id='stable-$key' class='viewport-header' data-type='stable'><span style='display:inline-block;width:110px'>$value</span><span class='sort'><i class='fa fa-sort'></i></span></div>";
        }

        $s = "$viewports<div class='viewport-block'></div>";

        $table_params = array(
            'rowHeight' => '30',
            'cellWidth' => '120',
            'borderRight' => '1px solid lightgray',
            'borderBottom' => '1px solid lightgray');

        // further processing of returning content...
        return array('output'=>'rolltable','params'=>$table_params,'content'=>'','control'=>['w'=>$k,'i'=>$s]);
        //return array('w'=>$k,'i'=>$s);
    }
    public function fill_stable_with_columns($params, $p) {
        
        list ($vars,$session_id,$output_limit) = $p;
        global $ID,$modules,$x_modules;

        $data_csv_header = $vars['stable_header'];
        $protocol = $vars['protocol'];
        $tgroups = $vars['tgroups'];
        $allowed_cols = $vars['allowed_cols'];
        $allowed_gcols = $vars['allowed_gcols'];
        $MASTER = $vars['MASTER'];
        $allowed_cols_module = $vars['allowed_cols_module'];

        if (!isset($_SESSION['orderby'])) {
            $_SESSION['orderby'] = 'obm_id';
            $_SESSION['orderad'] = 1;
            $_SESSION['orderascd'] = '';
        }
        if (!isset($_SESSION['orderad']) or $_SESSION['orderad']==0) {
            $_SESSION['orderad'] = 1;
        }

        // ignore joins - should be optional!!
        if (isset($_SESSION['ignore_joined_tables']) and $_SESSION['ignore_joined_tables'])
            $res = pg_query($ID,sprintf('SELECT * FROM (SELECT DISTINCT ON (obm_id) * FROM temporary_tables.temp_query_%1$s_%2$s) t ORDER BY %3$s %4$s',
                PROJECTTABLE,$session_id,$_SESSION['orderby'],$_SESSION['orderascd']));
        else
            $res = pg_query($ID,sprintf('SELECT * FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY %3$s %4$s',
                PROJECTTABLE,$session_id,$_SESSION['orderby'],$_SESSION['orderascd']));

        //
        $rows_count = pg_num_rows($res);

        $nullform_id = getNullFormId($_SESSION['current_query_table'],true);

        $transform_data_enabled = $modules->is_enabled('transform_data');

        $photos_enabled = false;
        foreach ($x_modules->which_has_method('view_attachment') as $mm) {
            $photos_enabled = $mm;
        }

        // an other separateble async process...
        $n = 0;
        $cmd = "";
        $cmd_n = 0;
        $has_master_access = has_access('master');
        while ($row = pg_fetch_assoc($res)) {

            if ($output_limit and $n==$output_limit)
                break;

            if (!$output_limit and $n<80) {
                $n++;
                continue;
            }

            // skipping print out the repeated rows if there are only one column
            // e.g. species list
            if (count($row)==1) {
                if ($row[0]==$chk) continue;
                else $chk = $row[0];
            }

            $acc = rst('acc',$row['obm_id'],$_SESSION['current_query_table'], $has_master_access) ? true : false; 
            
            extract(data_access_check($acc,$_SESSION['st_col']['USE_RULES'],$allowed_cols,$tgroups,$allowed_cols_module,$row));

            $line = array();
            foreach ($data_csv_header as $k=>$v) {
                if (isset($row[$k])) {

                    if ($transform_data_enabled) {
                        //default itegrated module
                        $line[$k] = "<div class='viewport'>".$modules->_include('transform_data','text_transform',array($row[$k],$k,''),false)."</div>";
                    } else {
                        $line[$k] = "<div class='viewport'>".$row[$k]."</div>";
                    }

                    if ($column_restriction and !in_array($k,$allowed_cols)) {
                        $line[$k] = '&nbsp;';
                    }

                    if ($geometry_restriction ) {
                        if ( $k == 'obm_geometry')
                            $line[$k] = '&nbsp;';
                        if ( !in_array($k,$allowed_gcols) ) {
                            $line[$k] = '&nbsp;';
                        }
                    }

                    if ( $k == 'obm_files_id' && $line[$k] != '&nbsp;') {
                        if ($photos_enabled !== false) {
                            $line[$k] = "<a href='getphoto?connection={$row[$k]}' class='photolink'><i class='fa fa-camera'></i></a>";
                        } else {
                            $line[$k] = "<i class='fa fa-camera' style='color:gray'></i>";
                        }
                    }
                    elseif ( $k == 'obm_uploader_user') {
                        $line[$k] = $row['obm_uploader_name'];
                    }

                } else {
                    $line[$k] = "&nbsp;";
                }
            }

            //prepend details buttun
            $id = $row['obm_id'];
            $uid = $row['uploading_id'];
            $view_url = sprintf('%s://'.URL.'/data/%s/%s/',$protocol,$_SESSION['current_query_table'],$id);
                //http://localhost/biomaps/resources/upload/?editrecord=2197472&form=125&type=file
            $edit_url = sprintf('%s://'.URL.'/data/%s/%s/editrecord/?type=file&form=%d',$protocol,$_SESSION['current_query_table'],$id,$nullform_id);

            $mod_acc = (rst('mod',$row['obm_id'],$_SESSION['current_query_table'], $has_master_access)) ? "" : "disabled"; 
            $b1 = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary' title='%s'><i class='fa fa-search-plus fa-lg'></i></a>",$view_url,str_details);
            $b2 = sprintf("<a href='%s' target='_blank' class='button-href pure-button button-small button-secondary' data-table=%s data-id=%s title='%s' %s><i class='fa fa-pencil fa-lg'></i></a>",$edit_url,$_SESSION['current_query_table'],$id,str_edit, $mod_acc);
            
            array_unshift($line,$b1.$b2);

            $cmd .= sprintf("UPDATE temporary_tables.temp_roll_stable_%s_%s SET rlist=%s WHERE \"ord\"=%d;",PROJECTTABLE,$session_id,quote(json_encode($line,JSON_UNESCAPED_UNICODE|JSON_HEX_QUOT|JSON_HEX_APOS)),$n);
            $n++;
            $cmd_n++;
            if ($cmd_n>20) {
                pg_query($ID,$cmd);
                $cmd = "";
                $cmd_n = 0;
            }
        }
        if ($cmd != '')
            pg_query($ID,$cmd);

    }

}
?>
