$(document).ready(function () {
    $("body").on("profile_page_loaded", function(ev, page) {
        if (ev.page === "custom_filetype") {
            $('.import-ft').click(async function () {
                try {
                    let resp = await $.post('ajax', {
                        m: 'custom_filetype',
                        action: 'import_ft',
                        ft: $(this).prop('id')
                    })
                    resp = JSON.parse(resp);
                    
                    if (resp.status !== 'success') {
                        $( "#dialog" ).html(resp.message);
                    }
                    else {
                        $( "#dialog" ).html('ok');
                    }
                    $( "#dialog" ).dialog( "open" );
                } catch (e) {
                    console.log(e);
                } 
            });
            
            $('.export-ft').click(async function () {
                try {
                    let resp = await $.post('ajax', {
                        m: 'custom_filetype',
                        action: 'export_ft',
                        ft: $(this).prop('id')
                    })
                    resp = JSON.parse(resp);
                    
                    if (resp.status !== 'success') {
                        $( "#dialog" ).html(resp.message);
                    }
                    else {
                        $( "#dialog" ).html('ok');
                    }
                    $( "#dialog" ).dialog( "open" );
                } catch (e) {
                    console.log(e);
                } 
            });
        }
    });
})
