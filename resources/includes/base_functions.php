<?php
function dmp($data)
{
    if (is_null($data)) {
        $str = "<i>NULL</i>";
    } elseif ($data === "") {
        $str = "<i>Empty</i>";
    } elseif (is_array($data)) {
        if (count($data) == 0) {
            $str = "<i>Empty array.</i>";
        } else {
            $str = "<table style=\"border-bottom:0px solid #000;\" cellpadding=\"0\" cellspacing=\"0\">";
            foreach ($data as $key => $value) {
                $str .= "<tr><td style=\"background-color:#008B8B; color:#FFF;border:1px solid #000;\">" . $key . "</td><td style=\"border:1px solid #000;\">" . d($value) . "</td></tr>";
            }
            $str .= "</table>";
        }
    } elseif (is_object($data)) {
        $str = d(get_object_vars($data));
    } elseif (is_bool($data)) {
        $str = "<i>" . ($data ? "True" : "False") . "</i>";
    } else {
        $str = $data;
        $str = preg_replace("/\n/", "<br>\n", $str);
    }
    return $str;
}

function dd($data)
{
    echo dmp($data) . "<br>\n";
    die;
}

function debugd($message, $file = NULL, $line = NULL, $class = "", $method = "")
{
    log_action($message, $file, $line, $class, $method);
    die;
}

/* Alias for log_action */
function debug($message, $file = NULL, $line = NULL, $class = "", $method = "")
{
    log_action($message, $file, $line, $class, $method);
}

/* Create a sylog message
 * */
function log_action($message, $file = NULL, $line = NULL, $class = "", $method = "")
{
    if (is_array($message)) {
        $message = json_encode($message, JSON_PRETTY_PRINT);
    } elseif (is_object($message)) {
        $message = json_encode($message, JSON_PRETTY_PRINT);
    }
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = "";
    if ($file != NULL or $line != NULL)
        $pointer = " $file $line";

    $class .= ($class !== "") ? "::$method" : $method;

    if (is_writable($custom_log)) {
        $date = date('M j h:i:s');
        error_log("[OBM_" . PROJECTTABLE . "] $date$pointer $class: $message\n", 3, "/var/log/openbiomaps.log");
    } else {
        openlog("[OBM_" . PROJECTTABLE . "]", 0, LOG_LOCAL0);
        syslog(LOG_WARNING, $pointer . $message);
        closelog();
    }
}

/* string quoting for sql queries
 * return a random delimetered string
 * */
function quote($text, $force_quote = false)
{
    //if ($text=='NULL' or trim($text)=='') return "NULL";
    if ($text === 'NULL') return "NULL";
    if (gettype($text) === 'boolean') {
        $text = ($text) ? 't' : 'f';
    }
    if ($force_quote === false)
        if (gettype($text) == 'integer') return $text;

    if (is_array($text) or trim($text) == '') return "NULL";

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $random_delimeter = substr(str_shuffle($chars), 0, 16);
    $text = '$' . $random_delimeter . '$' . trim($text) . '$' . $random_delimeter . '$';

    return $text;
}

function is_quoted($quoted)
{
    return (preg_match('/^\$[a-zA-Z]{16}\$(.*)\$[a-zA-Z]{16}\$$/', $quoted, $m));
}

function unquote($quoted)
{
    return (preg_match('/^\$[a-zA-Z]{16}\$(.*)\$[a-zA-Z]{16}\$$/', $quoted, $m)) ? $m[1] : false;
}

function protocol()
{
    return isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
}
/* generate password hash
 *
 * */
function gen_password_hash($password_string)
{

    if (function_exists('mcrypt_create_iv'))
        $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
    elseif (function_exists('random_bytes'))
        $salt = random_bytes(22);
    else {
        log_action("ERROR!! No random salt function available to create password!", __FILE__, __LINE__);
        return false;
    }

    $options = [
        'cost' => 10,
        'salt' => $salt,
    ];
    $hash = password_hash($password_string, PASSWORD_BCRYPT, $options);
    if ($hash != '')
        return $hash;
    else
        return false;
}
