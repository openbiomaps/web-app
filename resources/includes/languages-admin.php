<?php
//include local_vars
if(getenv("PROJECT_DIR") !== false) {
    include_once(getenv('PROJECT_DIR').'local_vars.php.inc');
}
// some improvements needed

// 1. Browser language - not used
// 2. Project language
// 3. GET language
// 4. SESSION language

if(!isset($_SESSION['LANG'])) {
    $browser_lang = '';
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
    if (isset($_COOKIE['obm_lang']))
        $browser_lang = $_COOKIE['obm_lang'];

    if ($browser_lang=='') {
        //project default language
        $_SESSION['LANG'] = LANG;
    } else {
        $_SESSION['LANG'] = $browser_lang;
    }
}

class translations {

    public function __construct (array $options = array()) {
        global $BID;
    
    }

    function load_translations ($topic) {

        if (defined("OB_PROJECT_DOMAIN"))
            $domain = constant("OB_PROJECT_DOMAIN");
        elseif (defined("OB_DOMAIN"))
            $domain = constant("OB_DOMAIN");
        else
            $domain = $_SERVER["SERVER_NAME"];

        if (!isset($_SESSION['LANG']) and defined("LANG"))
            $_SESSION['LANG'] = LANG;
        elseif (!isset($_SESSION['LANG']) and !defined("LANG"))
                $_SESSION['LANG'] = 'en';

        $cmd = sprintf('SELECT * FROM translations 
            WHERE lang = \'%2$s\' AND (project = \'%1$s\' OR (scope = \'global\' AND const NOT IN (SELECT const FROM translations WHERE project = \'%1$s\')));', PROJECTTABLE, $_SESSION['LANG']);
        $res = pg_query($BID, $cmd);
        while($row = pg_fetch_assoc($res)) {
            $translation = preg_replace('/%PROJECTTABLE%/',PROJECTTABLE,$row['translation']);
            if (defined("OB_DOMAIN"))
                $domain = constant("OB_DOMAIN");
            else
                $domain = $_SERVER["SERVER_NAME"];
            
            $translation = preg_replace('/%DOMAIN%/',$domain,$translation);

            define($row['const'],$translation);
        }
    }

    function translation_file_include () {

        #if (!isset($_SESSION['LANG']) and defined("LANG"))
        #    $_SESSION['LANG'] = LANG;
        #elseif (!isset($_SESSION['LANG']) and !defined("LANG"))
        #    $_SESSION['LANG'] = 'en';
        if (file_exists(sprintf("%slanguages/admin-%2s.php",getenv('OB_LIB_DIR'),preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])))) {
            include_once(sprintf("%slanguages/admin-%2s.php",getenv('OB_LIB_DIR'),preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])));
            // include local language files
            if(file_exists(sprintf("%slanguages/local_admin-%2s.php",getenv('PROJECT_DIR'),preg_replace('/[^a-z]{2}/','',$_SESSION['LANG']))) and !isset($_SESSION['skip_local_lang'][$_SESSION['LANG']])) {
                include_once(sprintf("%slanguages/local_admin-%2s.php",getenv('PROJECT_DIR'),preg_replace('/[^a-z]{2}/','',$_SESSION['LANG'])));
            }
        }
        else {
            // include local language files
            if(file_exists(sprintf("%slanguages/admin-%2s.php",getenv('OB_LIB_DIR'),preg_replace('/[^a-z]{2}/','',LANG))))
                include_once(sprintf("%slanguages/admin-%2s.php",getenv('OB_LIB_DIR'),preg_replace('/[^a-z]{2}/','',LANG)));
            if(file_exists(sprintf("%slanguages/local_admin-%2s.php",getenv('PROJECT_DIR'),preg_replace('/[^a-z]{2}/','',LANG))) and !isset($_SESSION['skip_local_lang'][LANG]))
                include_once(sprintf("%slanguages/local_admin-%2s.php",getenv('PROJECT_DIR'),preg_replace('/[^a-z]{2}/','',LANG)));
        }
    }
}

?>
