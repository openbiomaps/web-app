<?php
/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS for
profile and data interfaces

Do not change them
Use the local_funcs.php if you need specific functions
Damn... is not true, sorry

****************************************************************************** */


/* index functions 
 * doimetadata page
 * */
function load_doimetadata($scope, $META_ID, $DOI) {
    global $ID,$BID;

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    require_once(getenv('OB_LIB_DIR').'doi-metadata-page.php');

    if (isset($_GET['json'])) {
        header('Content-Type: application/json');
        return json_encode($json, JSON_PRETTY_PRINT);
    
    } elseif(isset($_GET['xml'])) {
        #$xml = new SimpleXMLElement('<root/>');
        #array_walk_recursive($json, array ($xml, 'addChild'));
        #return $xml->asXML();
        
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
        array_to_xml($json,$xml_data);
        #$xml_data->formatOutput = true;
        return sprintf("<pre>%s</pre>",$xml_data->asXML() );
    }
    
    return $out;
}

/* index function
 * upload history page
 * */
function upload_history() {
    global $ID,$BID, $GID;
    $qtable=PROJECTTABLE;
    $_SESSION['evaluates'] = array();
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $ACC_LEVEL = ACC_LEVEL;
    if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
        return;
    }

    #if (isset($get_id) and $get_id>0) {
    #    $cmd = "SELECT id,uploader_name,uploading_date,validation,description,collectors FROM system.uploadings WHERE id='$get_id'";
    #} elseif (isset($_SESSION['Tid']) and $_SESSION['Tid']>0) {
    #}
    #$cmd = "SELECT id,uploading_date,uploader_id,uploader_name,array_avg(validation) as valuation,collectors,description FROM system.uploadings WHERE id={$_SESSION['getid']}";
    $cmd = sprintf("SELECT id,uploading_date,uploader_id,uploader_name,validation as data_eval,collectors,description,project_table,metadata,form_id FROM system.uploadings WHERE project=%s AND id=%s",quote(PROJECTTABLE),quote($_SESSION['getid']));
    
    //$cmd = "SELECT id,uploading_date,uploader_id,uploader_name,validation as data_eval,collectors,description,project_table FROM system.uploadings LEFT JOIN ".PROJECTTABLE." WHERE project='".PROJECTTABLE."' AND id={$_SESSION['getid']}";

    $res = pg_query($ID,$cmd);
    
    if (!pg_num_rows($res)) {
        echo "<div style='margin-left:50px;font-size:150%'>This data-id does not exist or does not accessible.</div>";
        return;
    }
        
    $out = '<div style="line-height:200%;padding-left:1em">';
    while ($row=pg_fetch_assoc($res)) {
        if ($row['metadata'] != '') {
            $metadata = json_decode($row['metadata'],true);
            if (isset($metadata['measurements_num'])) {
                // observation list metadata record
            }
        }

        $get_id = $row['id'];
        $out .= "<b>".t('str_dataupid').":</b> ".$row['id'].'<br>';
        $out .= "<b>".t('str_datauptime').":</b> ".$row['uploading_date'].'<br>';
        $out .= "<b>".t('str_datauploader').":</b> ".$row['uploader_name'].'<br>';
        $out .= "<b>".t('str_description').":</b> ".$row['description'].'<br>';
        $out .= '<b>'.t('str_data_providers').':</b> ';
	$CITE_C = $_SESSION['st_col']['CITE_C'];
        $names = array();
        foreach ($CITE_C as $sl) {
            if($sl!='') {
                //$sl = quote($sl);
            }
            else continue;

            $cmd = sprintf("SELECT DISTINCT %s as fa FROM %s WHERE obm_uploading_id='%d' AND %s IS NOT NULL",$sl,$qtable,$_SESSION['getid'],$sl);
            $cres = pg_query($ID,$cmd);
            while ($crow=pg_fetch_assoc($cres)) {
                array_push($names,$crow['fa']);
            }
        }
        $names = array_unique($names);
        asort($names);
        $out .= '<div style="max-width:700px;padding-left:25px">'.implode(', ',$names).'</div>';
        
        $cmd = sprintf("SELECT count(*) as c FROM ".$row['project_table']." WHERE obm_uploading_id='%d'",$row['id']);
        $cres = pg_query($ID,$cmd);
        if ($crow=pg_fetch_assoc($cres)) {
            $out .= '<b>'.t('str_num_of_rows').':</b> '.$crow['c'] ."</a><br>";
        }
        $out .= "<b>".t('str_form').":</b> ";
        $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id=%d AND project_table='%s'",$row['form_id'],PROJECTTABLE);
        $dres = pg_query($BID,$cmd);
        if ($drow=pg_fetch_assoc($dres)) {

            $form_name = (defined($drow['form_name'])) ? constant($drow['form_name']) : $drow['form_name'];
            $out .= $form_name;
        }
        $out .= '<br>';

        $out .= sprintf('<b>'.t('str_data_query').':</b> <a href="'.$protocol.'://'.URL.'/?query=obm_uploading_id:'.$row['id'].'&qtable='.$row['project_table'].'&mousezoom=off" data-table="'.$row['project_table'].'" id="new-map-window">'.str_map.'</a> | <a href="'.$protocol.'://'.URL.'/includes/modules/results_asTable.php?show&query=obm_uploading_id:'.$row['id'].'&qtable='.$row['project_table'].'" data-table="'.$row['project_table'].'" id="new-map-window">'.str_table.'</a><input type="hidden" id="obm_uploading_id" class="qf" value="'.$row['id'].'"><br>');

        if ( has_access('master') ) {
            if ($row['metadata'] != '') {
                $metadata = json_decode($row['metadata'],true);
                if (isset($metadata['observation_list_start'])) $metadata['observation_list_start'] = date('Y-m-d H:i:s', $metadata['observation_list_start']/1000);
                if (isset($metadata['observation_list_end'])) $metadata['observation_list_end'] = date('Y-m-d H:i:s', $metadata['observation_list_end']/1000);
                if (isset($metadata['started_at'])) $metadata['started_at'] = date('Y-m-d H:i:s', $metadata['started_at']/1000);
                if (isset($metadata['finished_at'])) $metadata['finished_at'] = date('Y-m-d H:i:s', $metadata['finished_at']/1000);
                if (isset($metadata['form_version'])) $metadata['form_version'] = date('Y-m-d H:i:s', $metadata['form_version']);
                $out .= sprintf('<b>'.t('str_metadata').':</b><pre>%s</pre>',json_encode($metadata,JSON_PRETTY_PRINT));

                if (isset($metadata['observation_list_id'])) {
                    $out .= '<b>Observation list info:</b><br>';
                    $out .= '<div id="observation-list-box" style="padding-bottom:1em;padding-left:1em">';
                    $obs_list_id = $metadata['observation_list_id'];
                    $cmd = sprintf("SELECT to_timestamp(CAST(metadata->>'observation_list_start' AS bigint)/1000) AS start,
                                           to_timestamp(CAST(metadata->>'observation_list_end' AS bigint)/1000) AS end,
                                           metadata->>'recorded_session_tracklog' AS tracklog, 
                                           metadata->>'measurements_num' AS measurements_num,
                                           metadata->>'tracklog_info' AS tracklog_info
                                    FROM system.uploadings 
                                    WHERE metadata->>'observation_list_id'='%s'", $obs_list_id);
                    $ores = pg_query($GID,$cmd);
                    $obs_num = pg_num_rows($ores)-1;
                    while ($orow=pg_fetch_assoc($ores)) {
                        if ($orow['start']!='') {
                            $out .= 'Start: ' . $orow['start'] . '<br>End: ' . $orow['end'] . '<br>';
                            $out .= 'Num. of observation recorded: ' .  $orow['measurements_num'] . '<br>';
                            $out .= 'Num. of records uploaded from observations: '.$obs_num.'<br>';
                            if ($orow['tracklog'] == true) {
                                $out .= 'Tracklog: ';
                                $cmd = sprintf("SELECT project,user_id,start_time,end_time,trackname,tracklog_id,observation_list_id,tracklog_geom,st_asText(tracklog_line_geom) as geom 
                                                FROM system.tracklogs 
                                                WHERE observation_list_id = '%s'",  $obs_list_id);
                                $trex = pg_query($GID,$cmd);
                                while ($trow=pg_fetch_assoc($trex)) {
                                    $_SESSION['wktsession'] = $trow['geom'];
                                    $out .= sprintf('<a href="'.$protocol.'://'.URL.'/geomtest/?wktsession=%s" target="_blank">View trackline</a><br>','132x');
                                }
                            }
                            break;
                        }
                    }
                    $out .= '<a href="'.$protocol.'://'.URL.'/?query=obm_observation_list_id:'.$obs_list_id.'&mousezoom=off" target="_blank">Query all records from this observation</a>';
                    $out .= "</div>";
                }

            } else {
                $out .= sprintf('<b>'.t('str_metadata').':</b><br>');

            }
        }
        
        $out .= sprintf("<b>%s:</b><div style='max-width:700px' id='dp_%s'>%s</div>",t('str_comments'),$row['id'],comments($row['id'],PROJECTTABLE,'upload'));

        $eurl = sprintf($protocol.'://'.URL.'/data/%s/%d/history/validation/',$qtable,$row['id']);
        $out .= sprintf('</div><h3>%3$s</h3>
        <div style="width:600px">
        <div class=\'opinioning\'><div class=\'bubby\'><h3>'.str_thanks_for.'!</h3>'.str_opinion_post_1.'<br><br><span style=\'font-variant:small-caps\'>'.str_avoid.'…</span><br>'.str_opinion_post_2.'</div>
        <textarea class=\'expandTextarea yo\' id=\'evdt-%2$s\'></textarea></div>
        <div><b>%1$s:</b> (<a href="'.$eurl.'" target="_blank">%5$.2f%6$s</a>)
        <button class=\'button-error pure-button pm\' id=\'valm-%2$s\' rel=\'upload\' title=\''.str_dnlike.'\'><i class=\'fa-hand-o-down fa\'></i></button> 
        <button class=\'button-secondary pure-button pm\' id=\'valz-%2$s\' rel=\'upload\' title=\'ok\'>ok</button>
        <button class=\'button-success pure-button pm\' id=\'valp-%2$s\' rel=\'upload\' title=\''.str_like.'\'><i class=\'fa-hand-o-up fa\'></i></button></div>',
        t(str_valuation),$row['id'],t(str_your_opinion),'uploadings',$row['data_eval']*100,'%');
    }

    return $out;
}
/* index functions 
 * evaluation history page
 * */
function valid_history() {
    global $ID,$load_validhistory;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $ACC_LEVEL = ACC_LEVEL;
    if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
        return;
    }

    $cmd = "SELECT user_id,user_name,valuation,comments,to_char(datum,'YYYY-MM-DD HH24:MI') as datum FROM system.evaluations WHERE \"table\"='$load_validhistory' AND row='".$_SESSION['getid']."'";
    $res = pg_query($ID,$cmd);

    $eurl = sprintf($protocol.'://'.URL.'/profile/COL-0/');
    
    $table = new createTable();
    $table->def(['tid'=>'mytable','tclass'=>'resultstable']);
    $table->tformat(['format_col'=>'1','format_string'=>'<a href=\''.$eurl.'\'>COL-1</a>']);
    while ($row=pg_fetch_assoc($res)) {
        if ($row['user_name']=='NULL')
            $row['user_name'] = '-';
        $table->addRows($row);
    }
    $table->addHeader(array(-1,str_nickname,str_valuation,str_comments,str_date));
    return $table->printOut();
}

/* Index functions 
 *
 * */
/* Project's taxon list */
function taxonlist() {


    /* Taxon list
     * Called from index.php
     * */
    global $ID; 
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    
    require_once("iapi.php");
    $uri = "/api/species_list/taxon_list/load.php";
    $call = iapi::load($uri);
    $r = json_decode($call,true);
    if ($r['status'] == 'success')
        $results = $r['data'];
    else
        return($r['message']);
    
    $sf = iapi::$species_fields;
    $t = array_map(function ($row) use ($sf, $protocol) {
        // code...
        $n = "<tr>";
        foreach ($sf as $s) {
            $n .= ($row["taxon_id_$s"])
                ? sprintf("<td><a href='$protocol://".URL."/boat/?metaname&id={$row["taxon_id_$s"]}' target='_blank'>%s</a></td>",$row[$s])
                : sprintf("<td>%s</td>",$row[$s]);
        }
        $n .= "</tr>";
        return $n;
    }, $results);
    
    $thead = "<tr>" . implode('', array_map(function($s) {
      return "<th>".t("str_$s")."</th>";
    }, $sf)) . "</tr>";
    
    $tbody = implode("", $t);
    
    return "<table id='specieslist' class='resultstable'>$thead$tbody</table>";
}
// User stats on profile page: species, dates, ...
function usertaxonlist($user) {
    global $ID,$BID;
    
    if (!isset($_SESSION['Tid'])) return;

    $table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    $st_col = st_col($table, 'array');
    $User = new User($user);
    $taxon_table = is_trigger_enabled("taxon_update_$table");
    
    /*
    if ($taxon_table) {
        // with taxon table
        $cmd = sprintf('SELECT word, SUM(count) as count,t.taxon_id, \'species\' as rank FROM (
          SELECT t.status,t.taxon_id,count(t.word)
          FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id LEFT JOIN %4$s_taxon t ON (word=%1$s) 
          WHERE uploader_id=%3$d 
          GROUP BY t.status,t.taxon_id) as foo
        LEFT JOIN %4$s_taxon t ON (foo.taxon_id=t.taxon_id)
        WHERE t.status = \'accepted\' AND lang=\'%1$s\'
        GROUP by foo.taxon_id,word,t.taxon_id
        ORDER BY count DESC,word',$st_col['SPECIES_C'], $table, $User->role_id, PROJECTTABLE);
    }
    else {
        // no taxon table
        $cmd = sprintf('SELECT %1$s as word, \'species\' as rank, count(%1$s) 
            FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id 
            WHERE uploader_id=%3$d GROUP BY %1$s ORDER BY count DESC, %1$s',$st_col['SPECIES_C'],$table,$User->role_id);
    }
    */

    /*$results = pg_fetch_all($res);
    $ranks = ['species'];
    
    if ($modules->is_enabled('taxon_meta') && $modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonRank'])) {
        $taxon_rank_data = $modules->_include('taxon_meta', 'include_submodule', ['TaxonRank', 'filter_list', ['species_list' => $results]]);
        $results = $taxon_rank_data['species_list'];
        $ranks = $taxon_rank_data['ranks'];
    }*/
    
    
    /*foreach ($ranks as $rank) {
        $results_filtered = array_filter($results, function($t) use ($rank) {
            return $t['rank'] === $rank;
        });
        
        if ($modules->is_enabled('taxon_meta') && $modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonOrder'])) {
          $results_filtered = $modules->_include('taxon_meta', 'include_submodule', ['TaxonOrder', 'reorder_list', ['list' => $results_filtered, 'orderby' => $User->options->taxon_order]] );
        }
        $list = [];
        foreach ($results_filtered as $row) {
            $link = ($taxon_table)
                ? sprintf("<a href='$protocol://".URL."/boat/?metaname&id={$row["taxon_id"]}' target='_blank'>%s</a></td>",$row['word'])
                : $row['word'];
            
            $datalink = sprintf('<a href="'.$protocol.'://'.URL.'/?query=%1$s:'.$row['word'].';obm_uploader_user=%2$s" id="new-map-window" target="_blank">%3$d</a>',$st_col['SPECIES_C'],$User->hash,$row['count']);
            
            $list[] = "<div class='tbl-cell'>$link</div><div class='tbl-cell'>$datalink</div>";
        }
        $rank_transl = ((defined("str_$rank")) ? constant("str_$rank") : $rank) . " (" . count($results_filtered) . ")";
        $tables .= "<div class='tbl resultstable'> <h4>$rank_transl</h4> <div class='tbl-row'> <div class='tbl-h'>".str_specname."</div> <div class='tbl-h'>".str_rowcount."</div> </div> <div class='tbl-row'>" . implode("</div><div class='tbl-row'>",$list) . "</div> </div>";
    }*/


    $tables = "<div id='taxonlist-tables'>";
    #$m = array();
    #foreach($st_col['CITE_C'] as $c) {
    #    $m[] = "\"$c\" = '{$User->{'name'}}'";
    #}
    #$nameFilter = implode(" OR ",$m);
    $nameFilter = "\"{$st_col['CITE_C'][0]}\" = '{$User->{'name'}}'";

    // species list
    $cmd = sprintf('SELECT DISTINCT %1$s as species,sum(%4$s) as numbers,count(*) FROM %2$s
        WHERE (%3$s)
        GROUP BY %1$s ORDER BY count DESC',$st_col['SPECIES_C'],PROJECTTABLE,$nameFilter,$st_col['NUM_IND_C']);
    $result = pg_query($ID,$cmd);

    $osszegyed = 0;
    $osszfaj = 0;
    $list = array();
    while($row = pg_fetch_assoc($result)) {
        $osszfaj += 1;
        $osszegyed += $row['numbers'];
        $link = $row['species'];
        $datalink = $row['count'];
        $numbers = $row['numbers'];
        $list[] = "<div class='tbl-cell'>$link</div><div class='tbl-cell'>$datalink ($numbers)</div>";
    }

    // data count
    $cmd = sprintf('SELECT count(*) FROM %1$s
        WHERE (%2$s)',PROJECTTABLE,$nameFilter);
    $result2 = pg_query($ID,$cmd);
    $row2 = pg_fetch_assoc($result2);
    $num_of_records = $row2['count'];

    // time range
    $cmd = sprintf('SELECT DATE_PART(\'day\', MAX(%1$s) - MIN(%1$s)) as date_diff FROM %2$s 
        WHERE (%3$s)',$st_col['DATE_C'][0],PROJECTTABLE,$nameFilter);

    $result3 = pg_query($ID,$cmd);
    $time = 0;
    $row = pg_fetch_assoc($result3);
    $time = $row['date_diff'];

    // field dates
    $cmd = sprintf('SELECT DISTINCT to_char("%1$s", \'DD/MM/YYYY\')  FROM %2$s 
        WHERE (%3$s)',$st_col['DATE_C'][0],PROJECTTABLE,$nameFilter);
    $result4 = pg_query($ID,$cmd);
    $dates = pg_num_rows($result4);
    
    $out = "<h2>{$User->name} - ".str_species_stat."</h2>";
    $out .= "<h5>".t(str_summary)."</h5><div class='be'>";
    $out .= $osszfaj . " ".str_uploaded_species . " (" . $osszegyed . " specimens)<br>";
    $out .= $num_of_records . " records<br>";
    $out .= $time . " days time range<br>";
    $out .= $dates . " field days<br></div>";

    if ($dates) {

        $index_text = "Index: ((sqrt($num_of_records) * $osszfaj) / $dates) * sqrt($osszfaj * $dates)";
        $index =       round( ((sqrt($num_of_records) * $osszfaj) / $dates) * sqrt($osszfaj * $dates) );
        $effectivity = round((sqrt($num_of_records) * $osszfaj) / $dates,2);
        $efe_text = "Effectivity: (sqrt($num_of_records) * $osszfaj) / $dates";
        
        $out .= "<h5>".t(str_activity)." index</h5><div class='be'>";
        $out .= sprintf("<b><a title='%s'>%s: %d</a></b> (All activity)<br><b><a title='%s'>%s: %.2f</a></b> (Daily field activity)<br><br>",$index_text,'Activity',$index, $efe_text, 'Effectivity',$effectivity);
        
        $out .= "<i>All activity: ((sqrt(num_of_records) * all_species) / all_field_dates) * sqrt(all_species * all_field_dates)</i><br>";
        $out .= "<i>Effectivity: ((sqrt(num_of_records) * all_species) / all_field_dates)</i></div>";

        $out .= "<h5>Activity ranking</h5><div class='be'>";
        $out .= "<div style='font-size:150%'>";
        if ($index < 3500) {
            # 0) 0 - 3500
            $out .= sprintf("<br>Hajrá!");

        } elseif ($index < 10500) {
            # 1) 3500 - 10.500
            for ($i = 0;$i<1;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!",str_species_master_1);
        } elseif ($index < 24500) {
            # 2) 10.500 - 24.500
            for ($i = 0;$i<2;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!",str_species_master_2);
        } elseif ($index < 52500) {
            # 3) 24.500 - 52.500
            for ($i = 0;$i<3;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!",str_species_master_3);
        } elseif ($index < 108500) {
            # 4) 52.500 - 108.500 
            for ($i = 0;$i<4;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!",str_species_master_4);
        } elseif ($index < 220500) {
            # 4) 108.500 - 220.500 
            for ($i = 0;$i<5;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!",str_species_master_5);

        } else {
            # 5) > 220.500 
            for ($i = 0;$i<6;$i++) {
                $out .= sprintf('<i class="fa fa-trophy fa-2x" style="color:#%1$s"></i>',rand_color());
            }
            $out .= sprintf("<br>%s!!!",str_species_master_5);
        }
        $out .= "</div></div>";

        
        $out .= "<h5>Species list</h5>";
    }

    $tables .= "<div class='tbl resultstable'> <div class='tbl-row'> <div class='tbl-h'>".str_specname."</div> <div class='tbl-h'>".str_rowcount."</div> </div> <div class='tbl-row'>" . implode("</div><div class='tbl-row'>",$list) . "</div> </div>";
    $tables .= '</div>';

    return $out.$tables;
}

function file_get_contents_utf8($fn) {
     $content = file_get_contents($fn);
      return mb_convert_encoding($content, 'UTF-8',
          mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}

/* profile page tabs
 * params:
 *      user id
 *      own profile control indicator
 *
 * */
function tabs($userid,$own_profile_functions='') {

    global $M;

    $out = "<div id='tabs'>";
    $out .= "<button class='pure-button pure-u-1' id='sidenav_toggle'><i class='fa fa-angle-double-left fa-lg'></i></button>";
    $out .= "<ul class='sidenav' id='prodTabs'>
      <li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=profile&userid=$userid&get_own_profile=1&$own_profile_functions'> ".t(str_profile)."</a></li>";
    #$out .= (has_access('invites')) ? "<li class='profile_menu'><a href='#tab_basic' id='invites' data-url='includes/profile.php?options=invitations'>".t(str_invites)."</a></li>" : "";
    # OBM Alapvetés, nem lehet by default letiltani!!!
    $out .= "<li class='profile_menu'><a href='#tab_basic' id='invites' data-url='includes/profile.php?options=invitations'>".t(str_invites)."</a></li>";

    $out .= "<li class='profile_menu'><a href='#tab_basic' id='admin' data-url='includes/profile.php?options=administration'>".t(str_project_administration)."</a></li>";

    # OBM Alapvetés, nem lehet by default letiltani!!!
    #$out .= (has_access("newsread")) ? "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=newsread'>".t(str_messages)."</a></li>" : "";
    #$out .= (has_access("new_project_form")) ? "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=new_project_form'>".t(str_new_project)."</a></li>" : "";
    $out .= "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=newsread'>".t(str_messages). $M->get_unread_badge() . "</a></li>";
    $out .= "<li class='profile_menu'><a href='#tab_basic' data-url='includes/profile.php?options=new_project_form'>".t(str_new_project)."</a></li>";
    $out .= "</ul>";
    
    $out .= "</div>";
    
    ## SQL Console
    $out .= "
        <div id='sql-console' class='modal'>
            <div class='modal-draggable-handle modal-header'>SQL Console<i id='sql-console-close' class='fa fa-close fa-lg' style='cursor:pointer;position:absolute;top:6px;right:4px;color:white'></i></div>
            <div class='modal-body'>
                <div id='sql-answer-wrapper'><div id='sql-answer'></div><div style='position:absolute;z-index:0;text-shadow: 0px 1px 1px #d4d4d4;color: #999; font-size: 40px;top:10%;left: 10%'>Run your SQL command!</div></div>
                <div id='sql-cmd-wrapper'><div id='sql-cmd' contenteditable='true'></div></div>
            </div>
            <div class='modal-footer'><button id='send-sql-cmd'>send</button></div>
        </div>";


    return $out;
}

function sqlconsole() {

}
/* Profile main page
 *
 * get user profile for editing
 * return a html table
 * userid: users/user <- $_SESSION['Tcrypt']
 * od: orcid profile data
 * */
function profile($userid,$od=0) {
    global $BID, $x_modules;
    $get_own_profile = 0;

    // Profile informations for non logined users:
    if (!isset($_SESSION['Tid'])) {
        echo "<div style='padding:10px 0px 0px 20px' id='tab_basic'>";
        echo "<h2>".str_userdata."</h2>";

        
        $cmd = sprintf("SELECT username,institute,email,visible_mail FROM \"users\" WHERE \"user\"=%s",quote($userid));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            echo "<div><span style='font-size:110%'>{$row['username']}</span></div>";
            echo "<div><span style='font-size:110%'>{$row['institute']}</span></div>";
            if ($row['visible_mail']==2) {
                echo "<div><span style='font-size:110%'>{$row['email']}</span></div>";
            }
        }
        echo "</div>";
        return;
    }

    $single_profile_page = 'off';
    $cmd = sprintf("SELECT \"user\" FROM users WHERE id=%d",$_SESSION['Tid']);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        // No user_id
        if ($userid === 1 or $userid=='' or $userid == 'profile') {
            $single_profile_page = 'on';
            $userid = $row['user'];
        }
        if ($row['user'] === $userid)
            $get_own_profile = 2;
    } else {
        // error logging
        return;
    }
    $ol = ($od) ? "sopd=1" : ""; // ORCID fields

    // here, we read the profile page
    ob_start();
    $inc = 1;
    include(getenv('OB_LIB_DIR').'profile.php');
    $contents = ob_get_contents();
    ob_end_clean();


    return $contents;
}

// split ::: separated string
// the second one the default language
// improvment required!!!
// 1: more language support
// 2: generalized language set
// DEPRECATED - do not use!!!
function sepLangText($text) {
    if (preg_match('/:::/',$text)) {
       list($text_1,$text_2) = preg_split('/:::/',$text);
       if ($_SESSION['LANG'] == 'hu') $text = $text_1;
       else $text = $text_2;
    }
    return $text;
}
// Show saved queries in user profile
// Returns a html table
function ShowBookmarks() {
    global $BID;
    if (!isset($_SESSION['Tid'])) return;
    
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_bookmarks);
    if (isset($_GET['showbookmarks'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_sqdesc."</div>";

    $cmd = "SELECT id,command,user_id,key,query_string,project_table FROM bookmarks cr WHERE user_id='{$_SESSION['Tid']}' AND project='".PROJECTTABLE."' ORDER BY key"; 
    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {    
        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th><th>".t(str_label)."</th><th>".t('Link')."</th></tr>";
        while($row=pg_fetch_assoc($result)) {
            $link = "$protocol://".URL."/?bookmark={$row['id']}@{$row['key']}&qtable=".preg_replace('/^'.PROJECTTABLE.'_/','',$row['project_table']);
            $em .= "<tr><td><input type='checkbox' name='dt[]' class='dropsq' value='{$row['id']}'></td><td>{$row['key']}</td><td><a href='$link' target='_blank'>$link</a></td></tr>";
            # ADD query-edit-box here. A pop-up content editable div, width syntax highlight
        }
        $em .= "</table><button class='button-warning pure-button' id='dropsq'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button>";
    }
    echo $em;
}

// Show saved queries in user profile
// Returns a html table
function ShowShares() {
    global $BID;
    if (!isset($_SESSION['Tid'])) return;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_shares);
    if (isset($_GET['showsavedresults'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_srdesc."</div>";

    $em .= "<div id='jw1'>";
    if (isset($_GET['addToRepo'])) {
        $em .= "<p><i class='fa fa-spinner fa-pulse' aria-hidden='true'></i> Loading repository editor ...</p>";
    }    
    $em .= "</div>";

    if (isset($_GET['addToRepo'])) {
        $em .= '<script type="text/javascript">';
        $em .= 'afoo(function(data){content_loader(data,"jw1")},{repo_dataset_editor:1,D:0,obm_pid:"'.$_GET['id'].'",name:"'.$_GET['name'].'"})';
        $em .= '</script>';
    }

    $cmd = "SELECT name,datetime,query,to_char(datetime,'YYYY-MM-DD HH:MI') as date,\"type\",id,sessionid,doi,doi_state,repository_link,file_id
        FROM project_repository r LEFT JOIN header_names ON (r.project=f_project_table) 
        WHERE f_project_name='".PROJECTTABLE."' AND user_id='{$_SESSION['Tid']}' 
        ORDER BY datetime,name DESC"; 

    $result = pg_query($BID,$cmd);
    if (pg_num_rows($result)) 
    {
        require_once(getenv('OB_LIB_DIR').'repository.php');
        require_once(getenv('OB_LIB_DIR').'dataverse.php');

        $repo = new repository(0); // Default repository???
        $dv = new dataverse(0);

        $SERVER_URL = $repo->server_url;
        $API_TOKEN = $repo->api_token;

        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th>
                    <th>".t(str_label)."</th>
                    <th>".t(str_datetime)."</th>
                    <th>".t('OBM Unique ID')."</th>
                    <th>".t('PID')."</th>
                    <th>".t('PID State')."</th>
                    <th>".t('Repository action')."</th></tr>";
        while ($row=pg_fetch_assoc($result)) {
            $obm_link = "$protocol://".URL."/?Share={$row['id']}@{$row['sessionid']}";
            $repo_link = '';
            $repo_action_link_add = t(str_add);
            $repo_action_link_remove = t(str_delete);
            $repo_action_link_publish = t(str_publish);
            
            if ($repo->repo_type and $dv->project_dataverse) {

                
                if ($row['file_id'] == '') {
                    $repo_action_link_add = "<a href='?options=savedresults&addToRepo=1&name={$row['name']}&id={$row['id']}@{$row['sessionid']}' id='add-link-{$row['id']}' class='AddToRepository'>".t(str_add)."</a>";
                }
                
                if ($row['repository_link'] != '' and $row['file_id'] != '' and $row['doi_state']=='DRAFT') {
                    $repo_action_link_remove = "<a href='?options=savedresults&deleteFromRepo=1&name={$row['name']}&id={$row['id']}@{$row['sessionid']}' id='remove-link-{$row['id']}' data-obm_uid='{$row['id']}@{$row['sessionid']}' data-file_id='{$row['file_id']}' class='RemoveFromRepository'>".t(str_delete)."</a>";
                }

                if ($row['repository_link'] != '' and $row['file_id'] != '' and $row['doi_state']=='DRAFT') {

                    $version = 'DRAFT';
                    $dataset = json_decode($dv->get_dataset($SERVER_URL,$API_TOKEN,$row['doi']), TRUE);
                    if ($dataset['status'] == 'success') {
                        $version = $dv->get_dataset_attribute($dataset['data'],'version');
                    }
                    if ($version != 'RELEASED')
                        $repo_action_link_publish = "<a href='?options=savedresults&publishRepo=1&name={$row['name']}&id={$row['id']}@{$row['sessionid']}' id='publish-id-{$row['id']}' data-doi='{$row['doi']}' class='RepositoryPublish'>".t(str_publish)."</a>";
                    else {
                        # Override locally stored state based on remote (valid) state
                        # UPDATE local dataset???
                        # should
                        $row['doi_state'] = "RELEASED";
                    }
                } 
            }
            if ($row['repository_link'] != '' and $row['doi'] != '') {
                
                $repo_link = "<a href='{$row['repository_link']}' target='_blank'>{$row['doi']}</a>";
            } else 
                $repo_link = $row['doi'];

            //$pid = 'doi://...';
            $em .= "<tr>
                <td><input type='checkbox' name='dt[]' class='droprq' value='{$row['datetime']}'></td>
                <td><input id='obm-ulink-label-{$row['id']}' value={$row['name']}></td>
                <td>{$row['date']}</td>
                <td><a href='$obm_link' target='_blank'>{$row['id']}@{$row['sessionid']}</a></td>
                <td id='doi-id-{$row['id']}'>$repo_link</td>
                <td id='doi-state-{$row['id']}'>{$row['doi_state']}</td>
                <td>$repo_action_link_add<br>$repo_action_link_remove<br>$repo_action_link_publish</td>
                </tr>";
        }
        $em .= "</table><button class='button-warning pure-button' id='droprq'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button>";
    }
    echo $em;
}
// Show interrupted uplods in user profile
// Returns a html table
function ShowIUPS($user_id) {
    global $ID, $BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $em = "<h2>".t(str_intr_uploads);
    if (isset($_GET['showiups'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>".str_continue_interrupted_imports."</div>";

    $cmd = sprintf("SELECT ref,to_char(datum, 'YYYY-MM-DD HH24:MI:SS') as datum,form_type,form_id, template_name,file FROM system.imports WHERE project_table='".PROJECTTABLE."' AND user_id=%d ORDER BY datum",$user_id);
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) 
    {    
        $em .= '<table class="resultstable">';
        $em .= "<tr><th>".t(str_actions)."</th><th>".t('Link')."</th><th>".t(str_formtype)."</th><th>".t(str_formname)."</th><th>".str_template."</th></tr>";
        while($row=pg_fetch_assoc($result)) {
            $cmd = sprintf("SELECT form_name FROM project_forms WHERE form_id='%s'",$row['form_id']);
            $res2 = pg_query($BID,$cmd);
            $row2 = pg_fetch_assoc($res2);

            $cmd = sprintf("SELECT count(*) as c FROM temporary_tables.%s_%s",$row['file'],$row['ref']);
            $rs3 = pg_query($ID,$cmd);
            $r3 = pg_fetch_assoc($rs3);

            $form_name = (defined($row2['form_name'])) ? constant($row2['form_name']) : $row2['form_name'];
            $em .= sprintf('<tr><td><input type="checkbox" name="dt[]" class="dropiups" value="%2$s"> <a href="includes/ajax?exportimport=%2$s" class="pure-button button-href button-success">'.str_export.'</a></td><td><a href="'.$protocol.'://%1$s/upload/?load=%2$s" target="_blank">%3$s (%7$d %8$s)</a></td><td>%4$s</td><td>%5$s</td><td><input id="name_%2$s" value="%6$s"></td></tr>',URL,$row['ref'],$row['datum'],$row['form_type'],$form_name,$row['template_name'],$r3['c'],str_rows);
        }
        $em .= "</table>";
        $em .= "<button class='button-warning pure-button' id='dropiups'><i class='fa fa-trash'></i> ".str_drop_seleted_items."</button> ";
        $em .= "<button class='button-success pure-button' id='saveiups'><i class='fa fa-save'></i> ".str_save_seleted_items."</button>";
    }
    echo $em;
}

// Show API keys 
// Returns a html table
function ShowAPIKeys($user_id) {
    global $BID;
    $cmd = "SELECT access_token,client_id,expires,scope,CASE WHEN expires>now() THEN 1 ELSE 0 END as exp_state FROM oauth_access_tokens oa LEFT JOIN users u ON oa.user_id=u.email
        WHERE u.id='$user_id' ORDER BY expires DESC";

    $res = pg_query($BID,$cmd);

    $em = "<h2>Access tokens: ".t(str_apikeys);
    if (isset($_GET['showapikeys'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'></div>";

    $em .= '<table class="resultstable">';
    $em .= "<tr><th>access token</th><th>client id</th><th>expires</th><th>scope</th><th>action</th></tr>";
    
    while ($row=pg_fetch_assoc($res)) {
            /*if ($row['expire']=='expired') {$color='#B03434';$label='Expired';}
            elseif ($row['expire']=='short') {$color='#C46712';$label='Created at '.$row['datum'];}
            else {$color='#3BA3C4';$label='Created at '.$row['datum'];}*/

        if ($row['exp_state']==1) $button_color = 'button-error';
        else $button_color = 'button-warning';

        $em .= sprintf('<tr>
                    <td>%2$s</td>
                    <td>%3$s</td>
                    <td>%4$s</td>
                    <td>%5$s</td>
                    <td><button class="pure-button %6$s dropapikey" data-tokentype="access" id="dropat_%2$s"><i class="fa fa-trash-o"></i> %1$s</button></td></tr>',
                    str_delete,
                    $row['access_token'],
                    $row['client_id'],
                    $row['expires'],
                    $row['scope'],$button_color);
    }
    $em .= "</table>";
    echo $em;

    $cmd = "SELECT refresh_token,client_id,expires,scope,CASE WHEN expires>now() THEN 1 ELSE 0 END as exp_state FROM oauth_refresh_tokens oa LEFT JOIN users u ON oa.user_id=u.email
        WHERE u.id='$user_id' ORDER BY expires DESC";

    $res = pg_query($BID,$cmd);

    $em = "<h2>Refresh tokens: ".t(str_apikeys)."</h2><div class='shortdesc'></div>";

    $em .= '<table class="resultstable">';
    $em .= "<tr><th>refresh token</th><th>client id</th><th>expires</th><th>scope</th><th>action</th></tr>";
    
    while ($row=pg_fetch_assoc($res)) {
            /*if ($row['expire']=='expired') {$color='#B03434';$label='Expired';}
            elseif ($row['expire']=='short') {$color='#C46712';$label='Created at '.$row['datum'];}
            else {$color='#3BA3C4';$label='Created at '.$row['datum'];}*/

        if ($row['exp_state']==1) $button_color = 'button-error';
        else $button_color = 'button-warning';

        $em .= sprintf('<tr>
                    <td>%2$s</td>
                    <td>%3$s</td>
                    <td>%4$s</td>
                    <td>%5$s</td>
                    <td><button class="pure-button %6$s dropapikey" data-tokentype="refresh" id="droprt_%2$s"><i class="fa fa-trash-o"></i> %1$s</button></td></tr>',
                    str_delete,
                    $row['refresh_token'],
                    $row['client_id'],
                    $row['expires'],
                    $row['scope'],$button_color);
    }
    $em .= "</table>";
    echo $em;


}


// Show user's uploads 
// Returns a html table
function ShowUploads($user) {
    global $ID,$BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $userdata = new userdata($user,'hash');
    $role_id = $userdata->get_roleid();
    $user_name = $userdata->get_username();

    $em = "<h2>".t(str_show_uploads);
    if (isset($_GET['showuploads'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>$user_name ".str_upldesc."</div>";


    //if (isset($_GET['showuploads'])) {
    $cmd = sprintf("SELECT * FROM system.uploadings WHERE uploader_id=%d AND project=%s ORDER BY uploading_date DESC",$role_id,quote(PROJECTTABLE)); 
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) 
    {    
            $em .= '<table class="resultstable"><tr><th>Uploading date-time</th><th>Validation</th><th>Uploading ID</th><th>Details</th></tr>';
            while($row=pg_fetch_assoc($result)) {
                if ($row['description']=='') $row['description'] = "no description available";
                if ($row['validation']=='') $row['validation'] = 0;

                $eurl = sprintf($protocol.'://'.URL.'/uplinf/%d/',$row['id']);

                $em .= "<tr><td>{$row['uploading_date']}</td><td>{$row['validation']}</td><td>{$row['id']}</td><td><a href='$eurl'>{$row['description']}</a></td></tr>";
            }
            $em .= "</table>";
    }
    //}
    echo $em;
}
// Show user's observation lists 
// Returns a html table
function ShowObservationLists($user) {
    global $ID,$BID;
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    $userdata = new userdata($user,'hash');
    $role_id = $userdata->get_roleid();
    $user_name = $userdata->get_username();

    $em = "<h2>".t(str_show_observationlistuploads);
    if (isset($_GET['showuploads'])) {
        $em .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
    }
    $em .= "</h2><div class='shortdesc'>$user_name ".str_upldesc."</div>";


    //if (isset($_GET['showuploads'])) {
    $cmd = sprintf("SELECT * FROM system.uploadings 
        WHERE uploader_id=%d AND project=%s AND metadata::jsonb ? 'observation_list_id' AND metadata::jsonb ? 'observation_list_start' 
        ORDER BY uploading_date DESC",$role_id,quote(PROJECTTABLE)); 
    $result = pg_query($ID,$cmd);
    if (pg_num_rows($result)) 
    {    
            $em .= '<table class="resultstable"><tr><th>Uploading date-time</th><th>Validation</th><th>Uploading ID</th><th>Details</th></tr>';
            while($row=pg_fetch_assoc($result)) {
                if ($row['description']=='') $row['description'] = "no description available";
                if ($row['validation']=='') $row['validation'] = 0;

                $eurl = sprintf($protocol.'://'.URL.'/uplinf/%d/',$row['id']);

                $em .= "<tr><td>{$row['uploading_date']}</td><td>{$row['validation']}</td><td>{$row['id']}</td><td><a href='$eurl'>{$row['description']}</a></td></tr>";
            }
            $em .= "</table>";
    }
    //}
    echo $em;
}


// A function to decode MIME message header extensions and get the text
function decode_imap_text($str){
    $result = '';
    $decode_header = imap_mime_header_decode($str);
    foreach ($decode_header AS $obj) {
        $result .= htmlspecialchars(rtrim($obj->text, "\t"));
    }
    return $result;
}

function server_logs($source, $search, $filter) {

    if ($source=='mapserv') {
        # Put MAPSERVER_TMP constant into /etc/openbiomaps/system_vars.php.inc if you need different location
        #
        $mapserver_tmp = (defined('MAPSERVER_TMP')) ? constant('MAPSERVER_TMP') : '/tmp/mapserver';
        $syslog = $mapserver_tmp."/".PROJECTTABLE."_private_ms_error.txt";

        if (is_readable($syslog)) {
            $log = exec('tail -n 2000 '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            foreach ($la as $logline) {
                $m = array();
                //[Mon Oct 2 18:13:17 2017].434773
                if (preg_match('/^(\[.+?\])(\.\d+)(.+)$/',$logline,$m)) {
                    $logline = "<span class='loglineid'>$m[1]</span> $m[3]";
                }
                $logtext[] = $logline;
            }
            $log = implode('<br>',$logtext);

        } else {
            $log = "$syslog is not readable!";
        }
    }
    elseif ($source=='syslog') {
        $syslog = "/var/log/openbiomaps.log";
        if (is_readable($syslog)) {
            //$log = exec('grep -A 10 OBM_'.PROJECTTABLE.': '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            if ($filter!='')
                $log = exec('grep -A 10 "\[OBM_'.PROJECTTABLE.'\]" '.$syslog.'| grep "'.$filter.'"|tail -n 200|sed ":a;N;\$!ba;s/\n/\r/g"');
            else
                $log = exec('grep -A 10 "\[OBM_'.PROJECTTABLE.'\]" '.$syslog.'|tail -n 200|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            $i = 0;
            $x = true; // own table logs
            foreach ($la as $logline) {
                $m = array();
                $newline = 1;
                if (preg_match('/^\[OBM_(\w+)\]/',$logline,$m)) {
                    if ($m[1]!=PROJECTTABLE) {
                        // non priviliged log line
                        $x = false;
                        continue;
                    }
                    $m = array();
                    // [OBM_transdiptera] Oct 3 04:28:12 /var/www/libs/results_builder.php 880 :
                    if (preg_match('/^\[OBM_'.PROJECTTABLE.'\] (\w+ \d+ \d{2}:\d{2}:\d{2})(.*?): (.+)$/',$logline,$m)) {
                        $logline = "<span class='loglineid'>$m[1]$m[2]</span> $m[3]";
                        $x = true;
                    }


                } else {
                    // not new log line, but multiline logs
                    $newline = 0;
                    if (!$x) continue;
                }

                $logline = preg_replace('/SELECT[\n\r\s]/i',' <b>SELECT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?FROM[\n\r\s]/i',' <b>FROM</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?WHERE[\n\r\s]/i',' <b>WHERE</b> ',$logline);
                $logline = preg_replace('/DELETE[\n\r\s]/i',' <b>DELETE</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?LEFT[\n\r\s]/i',' <b>LEFT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?RIGHT[\n\r\s]/i',' <b>RIGHT</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?JOIN[\n\r\s]/i',' <b>JOIN</b> ',$logline);
                $logline = preg_replace('/DROP[\n\r\s]/i',' <b>DROP</b> ',$logline);
                $logline = preg_replace('/[\n\r\s]?AND[\n\r\s]/i',' <span class="logline-control">AND</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?OR[\n\r\s]/i',' <span class="logline-control">OR</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?CASE[\n\r\s]/i',' <span class="logline-control">CASE</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?IF[\n\r\s]/i',' <span class="logline-control">IF</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?ELSE[\n\r\s]/i',' <span class="logline-control">ELSE</span> ',$logline);
                $logline = preg_replace('/[\n\r\s]?ORDER BY[\n\r\s]/i',' <span class="logline-control">ORDER BY</span> ',$logline);
                $logline = preg_replace('/[\n\r\s=]?ANY/i',' <span class="logline-control">ANY</span>',$logline);
                $logline = preg_replace('/[\n\r\s]ON[\n\r\s(]/',' <span class="logline-control">ON</span>',$logline);

                $logline = preg_replace('/[\n\r\s=]?TRUE/',' <span class="logline-statement">TRUE</span>',$logline);
                $logline = preg_replace('/[\n\r\s=]?FALSE/',' <span class="logline-statement">FALSE</span>',$logline);
                $logline = preg_replace('/[\n\r\s]ASC/',' <span class="logline-statement">ASC</span>',$logline);
                $logline = preg_replace('/[\n\r\s]DESC/',' <span class="logline-statement">DESC</span>',$logline);
                if ($search!='')
                    $logline = preg_replace("/$search/","<span class='logline-highlight'>$search</span>",$logline);

                if ($newline) {
                    $logtext[] = $logline;
                    $i++;
                } else
                    $logtext[$i-1] .= " ".$logline;
            }
            $log = implode('<br>',array_reverse($logtext));
        } else {
            $log = "$syslog is not readable!";
        }
    } elseif ($source=='jobevents') {
        $syslog = getenv('PROJECT_DIR')."jobs/event.log";
        if (is_readable($syslog)) {
            $log = exec('tail -n 2000 '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            foreach ($la as $logline) {
                $m = array();
                //[Mon Oct 2 18:13:17 2017].434773
                if (preg_match('/^(\[.+?\])(\.\d+)(.+)$/',$logline,$m)) {
                    $logline = "<span class='loglineid'>$m[1]</span> $m[3]";
                }
                $logtext[] = $logline;
            }
            $log = implode('<br>',$logtext);

        } else {
            $log = "$syslog is not readable!";
        }
    } elseif ($source=='joberrors') {
        $syslog = getenv('PROJECT_DIR')."jobs/error.log";
        if (is_readable($syslog)) {
            $log = exec('tail -n 2000 '.$syslog.'|tac|sed ":a;N;\$!ba;s/\n/\r/g"');
            $la = preg_split("/\r/",$log);
            $logtext = array();
            foreach ($la as $logline) {
                $m = array();
                //[Mon Oct 2 18:13:17 2017].434773
                if (preg_match('/^(\[.+?\])(\.\d+)(.+)$/',$logline,$m)) {
                    $logline = "<span class='loglineid'>$m[1]</span> $m[3]";
                }
                $logtext[] = $logline;
            }
            $log = implode('<br>',$logtext);

        } else {
            $log = "$syslog is not readable!";
        }

    }

    return $log;

}
?>
