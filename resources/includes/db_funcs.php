<?php
/* very basic functions
 *
 *
 *
 * */

//xdebug_start_trace('/tmp/xdebug');

if(getenv("PROJECT_DIR") !== false) {
    #require_once(getenv('OB_LIB_DIR').'system_vars.php.inc');
    require_once('/etc/openbiomaps/system_vars.php.inc');
    
    require_once(OB_ROOT_SITE.'server_vars.php.inc');

    if (!is_dir(getenv('PROJECT_DIR'))) {
        echo '{"status":"fail","data":"No such project."}';
        exit;
    }
    require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
} else {
    exit;
}

require_once('base_functions.php');
require_once('postgres_functions.php');
require_once('cache_functions.php');

require_once('user.php');

require_once('access_functions.php');

require_once('role.php');
require_once('messenger.php');
require_once('taxon.php');
require_once('file.php');

require_once("iapi.php");

require_once('data_export.php');
//xdebug_stop_trace();

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database with biomaps.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

// Csak ideiglenesen, majd átalakítjuk a végleges PDO mód szerint.
$pdo_gis = PDO_connect(gisdb_user,gisdb_pass,gisdb_name,gisdb_host);
?>
