<?php

/**
 * Create tar file with export_attachments.php job
 * 
 * require_once('admin_pages/jobs.php');
 * $p = new job_module('export_attachments',PROJECTTABLE);
 * $p->set_filename('export_attachments.php');
 * $p->set_runOpts(json_encode(array('protocol'=>$protocol,'table'=>$_POST['download_files'])));
 * $p->runJob('export_attachments');
 */

class export_attachments_load extends iapi {

    public static $main_taxon_field;

    /**
     * Execution of export_attachments internal api call
     * 
     * @param string $params    a json encoded array [
     *                              "functions": ["zip_stream"],
     *                              "options": {
     *                                  "table": name of source table with schema eg. 'public.sablon' - default PROJECTTABLE ,
     *                                  "filename": optional filename - default: 12 character hash,
     *                                  "filter_files": ,
     *                                  "filter_files_table": ""
     *                                  "filter_files_column": ""
     *                                  "conid": ""
     *                              }
     *                          ]
     */
    public static function execute($params) {
        global $ID, $modules;

        // turn off max_execution_time limit for this script
        set_time_limit(0);
        
        $j = json_decode($params,true);
        $filename = isset($j['options']['filename']) ? $j['options']['filename'] : genhash(12);

        // Autoload the dependencies
        require getenv('OB_LIB_DIR').'vendor/autoload.php';

        // enable output of HTTP headers
        $options = new ZipStream\Option\Archive();
        $options->setSendHttpHeaders(true);

        // create a new zipstream object
        $zip = new ZipStream\ZipStream('export_attachments_'.$filename.'.zip', $options);

        $path = getenv('PROJECT_DIR').'local/attached_files';

        // collecting the filenames of every file
        $dir = opendir($path);
        $files = array();
        while (false !== ($fname = readdir($dir)))
        {
            if (is_file($path.'/'.$fname))
            {
                $files[] = $fname;
            }
        }
        closedir($dir);

        $DATATABLE = isset($j['options']['table']) ? $j['options']['table'] : constant('PROJECTTABLE');

        $filter_file = '';
        // ezt az ágat a pds használja
        if (isset($j['options']['filter_files'])) {
            
            $main_cols = dbcolist('columns',$j['options']['filter_files_table']);
            $main_cols[] = 'obm_id';
            $main_cols[] = 'reference';
            
            $items = array_map(function($item) {
                return "'$item'";
            }, explode(",",$j['options']['filter_files']));

            if (isset($j['options']['filter_files_column']) and in_array($j['options']['filter_files_column'],$main_cols))
                $filter_file = sprintf(" AND f.%s IN (%s)",$j['options']['filter_files_column'],implode(',',$items));
        
        }
        // ha a fájlkezelőben szűrést állítunk be, akkor használjuk a SESSION változókat
        elseif (isset($_SESSION['filter_files']) and $_SESSION['filter_files']!='') {

            $main_cols = dbcolist('columns',$_SESSION['filter_files_table']);
            $main_cols[] = 'obm_id';
            if (isset($_SESSION['filter_files_column']) and in_array($_SESSION['filter_files_column'],$main_cols))
                $filter_file = sprintf(" AND %s IN (%s)",$_SESSION['filter_files_column'],$_SESSION['filter_files']);
        }
        $check_download_permission = (isset($j['options']['check_download_permission']) && $j['options']['check_download_permission'] == true);
        $rules = ( $check_download_permission && isset($_SESSION['st_col']) && $_SESSION['st_col']['USE_RULES']) ? ", k.obm_sensitivity, k.obm_read" : "";

        $cmd = sprintf('SELECT f.data_table,array_to_string(array_agg(DISTINCT fc.conid),\',\') AS conid,f.id,f.reference,f.comment,array_to_string(array_agg(k.%2$s),\',\') as obm_id,f.datum %6$s
                    FROM system.files f
                    LEFT JOIN system.file_connect fc ON f.id=fc.file_id
                    LEFT JOIN %4$s k ON (k.obm_files_id=fc.conid)
                    WHERE f.project_table=\'%3$s\' AND (k.obm_files_id IS NOT NULL OR
                           (k.obm_files_id IS NULL AND f.data_table=%1$s) ) %5$s
                    GROUP BY f.id,f.reference,f.comment %6$s
                    ORDER BY f.datum',quote($DATATABLE),'obm_id',constant('PROJECTTABLE'),$DATATABLE, $filter_file, $rules);
        $res = pg_query($ID,$cmd);

        $allowed_columns = allowed_columns($modules, $_SESSION['current_query_table']);

        while ($row = pg_fetch_assoc($res)) {
            
            if ($check_download_permission && !self::has_download_permission($row, $allowed_columns)) {
                continue;
            }

            $key = array_search($row['reference'],$files);
            if ($key!==false) {    
                $fname = $files[$key];
                
                $conids = preg_split('/,/',$row['conid']);
                foreach($conids as $conid) {
                    
                    if (!isset($j['options']['conid']) or (isset($j['options']['conid']) and $j['options']['conid']!='off') )
                        $valid_filename = preg_replace('/[*]/','_',$conid.'_'.$fname);
                    else
                        $valid_filename = $fname;

                    $zip->addFileFromPath($valid_filename, $path.'/'.$fname);
                }
            }
        }
        if (file_exists($path.'/export_data_'.$filename.'.json')) {
            $zip->addFileFromPath('data.json', $path.'/export_data_'.$filename.'.json');
        }

        // finish the zip stream
        $zip->finish();
        
        return common_message('ok','done');

    }

    private static function has_download_permission($row, $allowed_columns) : Bool {
        extract($allowed_columns);

        $acc = rst('acc', $row['obm_id'], $_SESSION['current_query_table'], has_access('master')); 
        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;

        extract(data_access_check($acc, $_SESSION['st_col']['USE_RULES'], $allowed_cols, $tgroups, $allowed_cols_module, $row));

        if (!$acc && !in_array('obm_files_id', $allowed_cols)) {
            return false;
        }
        return true;
    }
}

?>
