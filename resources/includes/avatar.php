<?php
include_once("vendor/autoload.php");

// Set max-age to a week to benefit from client caching (this is optional)
header('Cache-Control: max-age=604800');

// Parse query string parameters
$value = $_GET['value'];
$size = min(max(intval($_GET['size']), 20), 500);
list($r,$g,$b) = explode(",",$_GET['bg']);
$r = min(max(intval($r), 0), 255);
$g = min(max(intval($g), 0), 255);
$b = min(max(intval($b), 0), 255);


// Render style
$icon_style = new \Jdenticon\IdenticonStyle(array(
'backgroundColor' => "rgba($r, $g, $b, 0.5)",
//'hues' => array(200)
));

// Render icon
$icon = new \Jdenticon\Identicon();
$icon->setValue($value);
$icon->setSize($size);
$icon->style = $icon_style;
$icon->displayImage('png');
?>
