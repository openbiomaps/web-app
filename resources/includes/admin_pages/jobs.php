<?php
/**
 * JOB API module
 *
 * 
 * @author Bóné Gábor <gabor.bone@milvus.ro>, Miklós Bán
 * @version 2.1
 */

class jobs_manager {
    var $error = '';
    var $retval;
    var $project_email;
    var $jobs = [];
    var $job_dir;
    var $mtable;
    
    function __construct($action=null) {
        global $BID,$ID;


        $jf = glob(getenv('PROJECT_DIR')."jobs/run/{*.php,*.sh,*.pl,*.perl,*.R}",GLOB_BRACE );
        $jobfiles = [];
        foreach ($jf as $f) {
            $path_parts = pathinfo($f);
            $jobfiles[] = $path_parts['basename'];
        }
        $pa['mtable'] = PROJECTTABLE;

        $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
        $row = pg_fetch_assoc($res);
        $this->project_email = $row['email'];

        $this->jobfiles = $jobfiles;
        $this->jobs_dir = getenv('PROJECT_DIR') . 'jobs/run/';
        $this->jobs_lib_dir = $this->jobs_dir . 'lib/';

        if (isset($pa['mtable'])) {
            $this->mtable = $pa['mtable'];
        }
    
        $this->include_jobs();

        if ($action)
            $this->retval = $this->$action($jobfiles,$pa);

    }

    public function adminPage() {
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo str_access_denied;
            return;
        }

        $this->init(1,1);

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        ob_start();

?>
        <link rel="stylesheet" href="<?= $protocol; ?>://<?= URL ?>/css/jobs.css" type="text/css" />
        <div id="job_admin_page">
        <div class='infotitle'><?php echo wikilink('admin_pages.html#background-jobs',t('str_documentation')) ?></div>
        Get job file from the repository:<br>
        <select name='file' id='gitlab-job-file'><option></option>
<?php
    

?>
        </select>
        <br><br>
        <div>
            <button class="module_submenu button-success button-large pure-button" title="<?= str_refresh ?>" data-url="includes/project_admin.php?options=jobs">
                    <i class="fa fa-refresh"></i>
                </button>
            <input class='pure-input' name='jobname' id='jobname' placeholder='job name'>
                <button class="pure-button button-large button-warning new-job" name="submit" type="submit">
                    <i class="fa fa-cog"> </i> <?= str_create?>
                </button> 
        </div>

        <div id="message-div" class="message fixed"></div>
        <div id="editor-div">
            <div id='editor-div-controls'>
                <button class='pure-button button-href' id='editor-save'><?=t(str_save)?></button>
                <a href="" target="_blank" class="pure-button button-href" id='editor-export'><?=t(str_export)?></a>
                <button class='pure-button button-href' id='editor-close'><?=t(str_close)?></button>
                <select id="LanguageMode" class='pure-button button-href'>
                    <option value="python">Python</option>
                    <option value="php">Php</option>
                    <option value="r">R</option>
                </select>
            </div>
            <textarea id='editorArea'></textarea>
        </div>

        <br>

        <div class='resultstable tbl pure-form'>
            <div class='tbl-row'>
                <div class='tbl-h'>job name</div>
                <div class='tbl-h'>enabled</div>
                <div class='tbl-h'>timing</div>
                <div class='tbl-h'>parameters</div>
                <div class='tbl-h'>export</div>
                <div class='tbl-h'>import</div>
                <div class='tbl-h'>run</div>
                <div class='tbl-h'><?= str_save?></div>
            </div>
<?php 
        foreach( $this->jobs as $job) { 
            $job_file = getenv('PROJECT_DIR').'jobs/run/'. $job->get_name() . '.php';
?>
            <div class='tbl-row' id="<?= $job->get_name(); ?>-form">
                <div class='tbl-cell'> <?= $job->get_name(); ?> </div>
                <div class='tbl-cell'> <?= $job->get_enabled_chkbox(); ?> </div>
                <div class='tbl-cell'> <?= $job->get_times(); ?> </div>
                <div class='tbl-cell'> <?= $job->get_params(); ?> </div>

                <div class='tbl-cell' style='vertical-align:top'> 
                    <!--<div style='border-bottom:1px dotted lightgray'><b>run/</b><a href="ajax?export-job&type=run&file=<?=$job->get_filename()?>" target="_blank" style="line-height:31px;"><?=$job->get_filename()?></a></div>-->
                    <div style='border-bottom:1px dotted lightgray'><b>run/</b><a href="" class="editor-open" data-name="<?=$job->get_filename()?>" data-file="<?=sslEncrypt($job_file,MyHASH)?>" style="line-height:31px;"><?=$job->get_filename()?></a></div>
                    <?php
                    foreach ($job->get_libfiles() as $libfile) {
                        $libfile = basename($libfile);
                        $lib_path = getenv('PROJECT_DIR').'jobs/run/lib/'. $libfile;
                    ?>
                <!--<b>lib/</b><a href="ajax?export-job&type=lib&file=<?=$libfile?>" target="_blank" style="line-height:30px"><?=$libfile?></a><br>-->
                <b>lib/</b><a href=""  class="editor-open" data-name="<?=$libfile?>" data-file="<?=sslEncrypt($lib_path,MyHASH)?>" style="line-height:30px"><?=$libfile?></a><br>
                    <?php
                    }
                    ?>

                </div>
                <div class='tbl-cell' style='vertical-align:top'> 
                    <input type='file' class='job-file-upload pure-button' name='<?= $job->get_filename()?>' id='job_<?= $job->get_name()?>_runfile' data-type='run' title='upload a runner [.php, .sh, .perl (.pl), .R] file'><br>
                    <input type='file' class='job-file-upload pure-button' name='<?= $job->get_name()?>' id='job_<?= $job->get_name()?>_libfile' data-type='lib' title='upload a lib [.php, .sh, .perl (.pl), .R, ...] file'>
                </div>
                <div class='tbl-cell' style='vertical-align:middle'> 
                    <button class="pure-button button-large button-warning run-job" data-ext="<?= $job->get_ext(); ?>" data-vm="<?= $job->get_name(); ?>"><i class="fa fa-lg fa-cogs"> </i> run</button> 
                    <button class="pure-button button-large button-secondary read-job" data-ext="<?= $job->get_ext(); ?>" data-vm="<?= $job->get_name(); ?>"><i class="fa fa-lg fa-cogs"> </i> <?= str_results?></button> 
                </div>
                <div class='tbl-cell' style='vertical-align:middle'> 
                <button class="pure-button button-large button-warning save-job" name="submit" data-ext="<?= $job->get_ext(); ?>" data-vm="<?= $job->get_name(); ?>" type="submit"><i class="fa fa-lg fa-floppy-o"> </i> <?= str_save?></button> 
                <button class="pure-button button-large button-error delete-job" data-ext="<?= $job->get_ext(); ?>" data-vm="<?= $job->get_name(); ?>" type="button"><i class="fa fa-lg fa-trash-o"> </i> <?= str_delete?></button> 
                </div>
            </div>
        <?php } ?>
        </div>

        <br style='clear: both; float: initial; display: block; position: relative;'>
        <h3> <?php echo t(str_results) ?> </h3>
            <div id='jobresults'>
            <?php foreach ( $this->jobs as $job ) {
            $results = $job->get_job_state();
            if ($results) {
    ?>
                <h4> <?= $job->get_name() ?> </h4>
                <?= $results ?>
            <?php }
                        }?>
            </div>
        </div>
<?php
        $out = ob_get_clean();

        return $out;
    }

    private function getMailText($const,$lang) { 
        global $BID;
        $cmd = sprintf("SELECT translation FROM translations WHERE project = '%s' AND lang = %s AND const = %s LIMIT 1", PROJECTTABLE, quote($lang), quote($const));
        $text = pg_fetch_assoc(query($BID, $cmd));
        return $text['translation'];
    }
        
    public function displayError() {
        $this->error = common_message('fail',$this->error);
        log_action($this->error,__FILE__,__LINE__);
    }

    public function print_js($request) {

        $js = (file_exists(getenv('PROJECT_DIR').'js/jobs.js')) ? file_get_contents(getenv('PROJECT_DIR') . 'js/jobs.js') : '';
        return $js;

    }

    static function run() {

        debug('job run running',__FILE__,__LINE__);

    }
    public function destroy($params, $pa) {
        debug('job destroy() running',__FILE__,__LINE__);
        foreach ($this->jobs as $job) {
            //job module initialization
            if ( method_exists($job,'destroy') ) {
                if (!$job->destroy($params, $pa)) {
                    $this->error = 'job modul destroy() failed';
                    return;
                }
            }
        }
        debug('job destroy() finished',__FILE__,__LINE__);
    }

    public function init($params,$pa) {
        //debug('job init running', __FILE__,__LINE__);

        global $ID;

        //checking jobs table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_jobs');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE SEQUENCE %1$s_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',PROJECTTABLE);
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_jobs" ( "id" integer DEFAULT nextval(\'%1$s_jobs_id_seq\') NOT NULL, data_table character varying NOT NULL, job character varying NOT NULL, parameters json NOT NULL, enabled smallint NOT NULL DEFAULT 0, created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, CONSTRAINT "%1$s_jobs_id" PRIMARY KEY ("id")); ', PROJECTTABLE );
        }
        if (!empty($cmd1) && !query($ID,$cmd1)) {
            $this->error = 'job init failed: table creation error';
            return;
        }

        //checking job_results table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_jobs_results');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd1 = [];
        if ($result['exists'] == 'f') {
            $cmd1[] = sprintf('CREATE TABLE "public"."%1$s_jobs_results" ( "row_id" integer NOT NULL, data_table character varying NOT NULL, job_name character varying NOT NULL, result text NOT NULL, PRIMARY KEY ("row_id", "data_table", "job_name", "result")); ', PROJECTTABLE );
        }
        if (!empty($cmd1) && !query($ID,$cmd1)) {
            $this->error = 'job init failed: table creation error';
            return;
        }
        
        //checking job_modules dir
        if ( ! file_exists( $this->jobs_dir ) ) {
            mkdir($this->jobs_dir);
        }

        //checking job_modules libdir
        if ( ! file_exists( $this->jobs_lib_dir ) ) {
            mkdir($this->jobs_lib_dir);
        }

        foreach ($this->jobs as $job) {
            //job module initialization
            if ( method_exists($job,'init') ) {
                if (!$job->init(false,false)) {
                    $this->error = 'job modul init failed';
                    continue;
                }
            }
        }
        //log_action('job init succeded',__FILE__,__LINE__);
        return;
    }

    private function include_jobs() {

        foreach ($this->jobfiles as $jobfile) {

            if ($jobfile !== '') {
                //checking job module files
                $job_file = $this->jobs_lib_dir . $jobfile;

                $path_parts = pathinfo($job_file);
                if ($path_parts['extension'] == 'php') {

                    $p = $path_parts['filename'];

                    if ( ! file_exists( $job_file) ) {
                        $template_file = getenv('PROJECT_DIR') . 'jobs/job_lib.php.template';
                        if (!file_exists($template_file)) {
                            $this->error = "jobs init failed: job_lib.php.template file missing";
                            continue;
                        }
                        $content = file_get_contents($template_file);
                        $content = preg_replace('/%%vm%%/', $p, $content);
                        if ( ! file_put_contents($job_file, $content) ) {
                            $this->error = "job init failed: job_lib.php.template copy failed";
                            continue;
                        }
                    }
                    $e = '';
                    if (!php_check_syntax($job_file,$e)) {
                        $rr = array('tmp'=>'','file_names'=>array(),'err'=>array());
                        $rr['err'][] = $e;
                        $this->error = "php sytax check failed for file. ".json_encode($rr['err']);
                        debug($job_file);
                        debug($this->error);
                        continue;
                    }
                    require_once($job_file);

                    //checking the structure of the job module
                    if ( ! class_exists($p) ) {
                        $this->error = "job init failed: $p class missing from $p.php";
                        continue;
                    }
                    // new auto_speciesname()
                    $this->jobs[$p] = new $p($this->mtable);
                    $this->jobs[$p]->set_filename($jobfile);

                } else {

                    $p = $path_parts['filename'];
                    $this->jobs[$p] = new job_module($p,$this->mtable);
                    $this->jobs[$p]->set_filename($jobfile);

                }
            }
        }
    }

}

class job_module {
    var $p;
    var $mtable;
    var $params;
    var $job_file;
    var $pid;
    var $run_opts;

    public function __construct($p,$mtable) {
        $this->p = $p;
        $this->mtable = $mtable;
        $this->request_module_params();
    }

    public function set_filename($filename) {
        $this->job_file = $filename;
    }

    public function get_filename() {
        return $this->job_file;
    }
    public function get_libfiles() {
        $this->job_file;

        $files = glob(getenv('PROJECT_DIR')."jobs/run/lib/{".$this->p.".*}",GLOB_BRACE );

        return $files;
    }
    
    public function get_name() {
        return $this->p;
    }

    public function get_ext() {
        return pathinfo($this->job_file, PATHINFO_EXTENSION);
    }

    public function get_times() {
        $h = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->hour)) ? $this->params->time->hour : '',
            'name' => 'hour',
            'pattern' => '[0-9*]{1,2}',
            'placeholder' => 'hours',
        ]);
        $m = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->minute)) ? $this->params->time->minute : '',
            'name' => 'minute',
            'pattern' => '[0-9*]{1,2}',
            'placeholder' => 'minutes',
        ]);
        $d = new input_field([
            'type' => 'text', 
            'value' => (isset($this->params->time->day)) ? $this->params->time->day : '', 
            'name' => 'day',
            'pattern' => '[0-9*]{1,2}',
            'placeholder' => 'days',
        ]);
        $out = $m->getElement() . $h->getElement() . $d->getElement();
        return $out;
    }

    public function get_enabled_chkbox() {
        $chk = new input_field([
            'type' => 'checkbox',
            'name' => 'enabled',
            'checked' => ($this->enabled) ? 'checked' : '',
        ]);
        return $chk->getElement();

    }

    public function get_params() {

        $parameters = (isset($this->params->parameters)) ? $this->params->parameters : '';

        $ta = new input_field([
            'element' => 'textarea',
            'value' => ($parameters) ? json_encode($parameters,JSON_PRETTY_PRINT) : '',
            'placeholder' => ($parameters) ? '' : 'parameters not set',
            'name' => 'parameters',
        ]);

        return $ta->getElement();
    }

    private function createJobFile() {
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo common_message('error','You don\'t have the right to do this!');
            return;
        }
        if (!file_exists($this->job_file)) {
            $template_file = getenv('PROJECT_DIR') . '/jobs/job_lib.php.template';
            if (!file_exists($template_file)) {
                return false;
            }
            $content = file_get_contents($template_file);
            $content = preg_replace('/%%vm%%/', $this->p, $content);
            file_put_contents($this->job_file, $content);
        }
        return true;
    }
    public function destroy($params,$pa) {
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo common_message('error','You don\'t have the right to do this!');
            return;
        }
        global $ID;
        if (!isset($pa['mtable'])) {
            $this->error = 'mtable missing';
            return false;
        }
        
        $cmd = sprintf('UPDATE %1$s_jobs SET enabled=0 WHERE data_table = %2$s;', PROJECTTABLE, quote($pa['mtable']));
        $res = pg_query($ID, $cmd);
        if (pg_affected_rows($res) == 0) {
            debug('query_failed',__FILE__,__LINE__);
            return false;
        }

        return $this->disableJob();
    }

    private function enableJob($times) {
        if (!$this->createJobFile()) {
            echo common_message('error','Job file creation error');
            return;
        } 
        $jobs = read_jobs();
        $jobs[$this->job_file] = [$times['minute'],$times['hour'],$times['day']];


        $write_result = write_jobs($jobs);
        if (isset($write_result['error'])) {
            echo common_message('error',$write_result['error']);
            return;
        }
        if ( $write_result === false ) {
            echo common_message('error', 'File write error!');
            return;
        }
        return;
    }
    
    private function disableJob() {
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo common_message('error','You don\'t have the right to do this!');
            return;
        }
        $jobs = read_jobs();
        unset($jobs[$this->job_file]);
        return write_jobs($jobs);

    }
    
    public function deleteJob() {
        global $ID;
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo common_message('error','You don\'t have the right to do this!');
            return;
        }
        $jobs = read_jobs();
        unset($jobs[$this->job_file]);
        $write_result = write_jobs($jobs);

        $errors = [];
        $filename = getenv('PROJECT_DIR') . 'jobs/run/' . $this->job_file;
        if (!file_exists($filename)) {
            $errors[] = 'Job file ['.$filename.'] not exists therefore not deleted';
        }
        elseif (!unlink($filename)) {
            $errors[] = "Job file [$filename] can not be deleted!" ;
        }

        $filename = getenv('PROJECT_DIR') . 'jobs/run/lib/' . $this->job_file;
        if (!file_exists($filename)) {
            $errors[] = 'Job file ['.$filename.'] not exists therefore not deleted';
        }
        elseif (!unlink($filename)) {
            $errors[] = "Job file [$filename] can not be deleted!" ;
        }
        
        if (!count($errors)) {
            $cmd = sprintf('DELETE FROM %1$s_jobs WHERE data_table = \'%1$s\' AND job = %2$s;', PROJECTTABLE, quote($this->p));
            if (!$res = pg_query($ID, $cmd)) {
                $errors[] = 'deleteJob query error';
            }
        }
        if (count($errors)) {
            echo common_message('error', implode('<br>', $errors));
        }
        else {
            echo common_message('ok','ok');
        }
        return;
    }
    
    public function get_job_state() {

        $filename = getenv('PROJECT_DIR') . 'jobs/run/' . $this->job_file;
        $path = getenv('PROJECT_DIR');
        if (file_exists(getenv('PROJECT_DIR') . "jobs/".$this->job_file.".pid")) {
            $pidfile = file(getenv('PROJECT_DIR') . "jobs/".$this->job_file.".pid");
            $res = exec("ps xau | grep ".trim($pidfile[0])." | grep -v grep");
            $data = exec("grep ".trim($pidfile[0])." ".getenv('PROJECT_DIR') . "jobs/event.log");
            if ($res!='') {
                return "<i>Job is running</i><br>$data<br>...";
            } else
                return "<i>Job finished</i><br>$data<br>...";
        } else 
            return '<i>Never runned</i>';
    }

    public function get_results() {

        $filename = getenv('PROJECT_DIR') . 'jobs/run/' . $this->job_file;
        $path = getenv('PROJECT_DIR');
        if (file_exists(getenv('PROJECT_DIR') . "jobs/".$this->job_file.".pid")) {
            $pidfile = file(getenv('PROJECT_DIR') . "jobs/".$this->job_file.".pid");
            $res = exec("ps xau | grep ".trim($pidfile[0])." | grep -v grep");
            $data = shell_exec("grep -n -A 1 'JOB: ".trim($pidfile[0])." -' ".getenv('PROJECT_DIR') . "jobs/event.log");
            $jlog = array();
            $n = 0;
            $data_x = explode("\n",$data);
            foreach ($data_x as $d) {
                if (preg_match("/^(\d+):(\[JOB: ".trim($pidfile[0])." -.+)/",$d,$m)) {
                    $jlog[] = $m[2];
                    $n = $m[1];
                } else if (preg_match("/^--/",$d)) {
                    continue;
                } else if (!preg_match("/^\d+:\[JOB: /",$d)) {
                    
                    $n = $n+1;
                    $extr = shell_exec('sed -n "'.$n.',$ p" '.getenv('PROJECT_DIR') . 'jobs/event.log');
                    $extr_x = explode("\n",$extr);
                    foreach ($extr_x as $e) {
                        if (preg_match('/^\[JOB:/',$e)) {
                            break;
                        }
                        $jlog[] = $e;
                    }
                }
            }
            $data = implode('<br>',$jlog);
            $data = preg_replace("/(\[JOB: \d+ - ".PROJECTTABLE."\]\s+)(\w+\s+\d+\s+\d+:\d+:\d+:)/","<span style='color:#bababa'>$2</span>",$data);
            
            if ($res!='') {
                return sprintf("<i>Job [%s] is running</i><pre>%s<pre>...",trim($pidfile[0]),$data);
            } else {
                return sprintf("<i>Job [%s] finished</i><pre>%s</pre>",trim($pidfile[0]),$data);
            }
        } else 
            return '<i>Never runned</i>';
    }

    public function get_logs() {

        $log = server_logs('jobevents', null, null);
        return log;
    }

    public function runJob($request) {
        
        $filename = getenv('PROJECT_DIR') . 'jobs/run/' . $this->job_file;
        $path = getenv('PROJECT_DIR');
        $error_log = $path.'jobs/error.log';
        $event_log = $path.'jobs/event.log';
        if (file_exists($path.'jobs/job_settings.php.inc'))
            require_once($path.'jobs/job_settings.php.inc');
        
        $path_parts = pathinfo($filename);
        $ext = $path_parts['extension'];
        $dirname = getenv('PROJECT_DIR');
        $run_opts = ($this->run_opts != '') ? base64_encode($this->run_opts) : '';

        if ($ext == 'php') {
            $php = (defined('PHP_PATH')) ? constant('PHP_PATH') : '/usr/bin/php';
            $this->pid = exec("$php $filename $dirname $run_opts >>$event_log 2>>$error_log & echo $!; ");
            echo common_message('success','Job call sent, check the logs: '.$this->pid);

            $pidfile = fopen(getenv('PROJECT_DIR') . "jobs/".$this->job_file.".pid", "w") or die("Unable to open file!");
            fwrite($pidfile, $this->pid."\n");
            fclose($pidfile);

            return;
        }
        elseif ($ext == 'R') {
            // direct include R job file
            $r = (defined('R_PATH')) ? constant('R_PATH') : '/usr/bin/Rscript';
            $this->pid = exec("nohup $r --vanilla $filename $dirname $run_opts >>$event_log 2>>$error_log & echo $!; ");
            echo common_message('success','Job call sent, check the logs: '.$this->pid);
            return;
        }
        else {
            // direct include any runnable job file
            $this->pid = exec("nohup $filename $dirname $run_opts >>$event_log 2>>$error_log & echo $!; ");
            echo common_message('success','Job call sent, check the logs: '.$this->pid);
            return;
        }
        echo common_message('error','something is wrong...');
        return;
    }

    public function saveJob($request) {

        global $ID;
        if (!isset($_SESSION['Tid']) || !has_access('master')) {
            echo common_message('error','You don\'t have the right to do this!');
            return;
        }
        parse_str($request['data'],$data);
        $vm_parameters = json_decode($data['parameters']);
        if ($vm_parameters===null and $data['parameters']!='') {
            echo common_message('error',"Invalid json format for parameters");
            return;
        }
        //$mtable = $data['mtable'];
        $mtable = PROJECTTABLE; // egyelőre nincs jelentősége, hogy melyik tábla..
        $enabled = (isset($data['enabled'])) ? 1 : 0;

        $times = [
            'hour'=>$data['hour'],
            'minute' => $data['minute'],
            'day' => $data['day'],
        ];

        foreach ($times as $time) {

            if (!preg_match('/^(\d{1,2}|\*)$/',$time,$outpu)) {
                echo common_message('error',"Invalid time: $time");
                return;
            }
        }
        
        $jobs = read_jobs();
        if (isset($jobs['error'])) {
            echo common_message('error',$jobs['error']);
            return;
        }

        if ($enabled)  {
            $this->enableJob($times);
        }
        else {
            $this->disableJob();
        }

        $parameters = json_encode([
            'time' => $times,
            'parameters' => $vm_parameters
        ]);

        $cmd = [
            sprintf('UPDATE %1$s_jobs SET parameters=%2$s, enabled=%5$s WHERE data_table = %3$s AND job = %4$s;', PROJECTTABLE, quote($parameters), quote($mtable), quote($request['vm']), quote($enabled)),
            sprintf('INSERT INTO %1$s_jobs (data_table, job, parameters, enabled) SELECT %3$s, %4$s, %2$s, %5$s WHERE NOT EXISTS (SELECT 1 FROM %1$s_jobs WHERE data_table = %3$s AND job = %4$s);', PROJECTTABLE, quote($parameters), quote($mtable), quote($request['vm']), quote($enabled)),
            ];

        if (!$res = query($ID, $cmd)) {
            echo common_message('error','Parameters update error');
            return;
        }

        $aff_rows = 0;
        foreach ( $res as $r ) 
            $aff_rows += pg_affected_rows($r);
        
        if ($aff_rows == 0) {
            echo common_message('success','Parameters not updated');
            return;
        }
        
        echo common_message('success','Parameters updated');
        return;

    }

    public function newJob($request) {

        $m = array();
        if (preg_match('/[a-zA-Z0-9_]+\.\w+/',$this->job_file,$m)){
            if ($m[0] != $this->job_file) {
                echo common_message('error','Invalid job name');
                return false;
            }
        } else {
            echo common_message('error','Invalid job name');
            return false;
            
        }
        $job_file = getenv('PROJECT_DIR') . 'jobs/run/'.$this->job_file;
        
        if ( ! file_exists( $job_file ) ) {

            $template_file = getenv('PROJECT_DIR') . 'jobs/job_run.php.template';
            if (!file_exists($template_file)) {
                echo common_message("error","jobs init failed: job_run.php.template file missing");
                return false;
            }
            $content = file_get_contents($template_file);
            $content = preg_replace('/%%vm%%/', $this->p, $content);
            if ( ! file_put_contents($job_file, $content) ) {
                echo common_message("error","job init failed: job_run.php copy failed");
                return false;
            }
            echo common_message('success','ok');
            return true;
            
        }
        echo common_message('error',"job file already exists");
        return false;
    }


    private function request_module_params() {
        global $ID;

        $cmd = sprintf('SELECT parameters, enabled FROM %1$s_jobs WHERE data_table = %2$s AND job = %3$s;', PROJECTTABLE, quote($this->mtable), quote($this->p));
        $res = pg_query($ID, $cmd);
        if (pg_num_rows($res)) {
            $result = pg_fetch_assoc($res);
            $this->params = json_decode($result['parameters']);
            $this->enabled = $result['enabled'];
        } else {
            $this->enabled = false;
            $this->parameters = '';
        }
    }
    public function set_runOpts($param) {
        $this->run_opts = $param;
    }

    static function getJobParams($job) {
        global $ID;

        $table = PROJECTTABLE . '_jobs';
        $res = pg_query_params($ID, "SELECT parameters FROM $table WHERE data_table = $1 AND job = $2;", [PROJECTTABLE, $job]);
        if (!$res) {
            job_log('Query failed');
            exit();
        }
        if ($results = pg_fetch_row($res)) {
            $results = json_decode($results[0]);
            return $results->parameters;
        }
        else { 
            return [];
        }
    }
}



?>
