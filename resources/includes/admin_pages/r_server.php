<?php
if (defined('RSERVER_PORT') ) {
    $RSERVERS = constant('RSERVER_PORT');
    if (isset($RSERVERS[PROJECTTABLE])) {
        $rport = $RSERVERS[PROJECTTABLE];
        // test, shiny server is running
        if (service_test($_SERVER['SERVER_NAME'],$rport)) {
            echo "R-shiny server is running at {$_SERVER['SERVER_NAME']}:$rport<br>";
            echo "<button id='control-R-server' value='stop' class='pure-button button-warning'>stop R server</button>";
        } else {
            echo "R-shiny server is not running on {$_SERVER['SERVER_NAME']}:$rport!<br>";
            echo "<button id='control-R-server' value='start' class='pure-button button-success'>start R server</button>";
        }
    } else if (!defined('RSERVER_PORT')) {
        echo "RSERVER is not defined";

    }
}

?>
