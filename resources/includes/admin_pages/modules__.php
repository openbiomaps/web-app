<?php


$mtable = (isset($_GET['mtable'])) ? $_GET['mtable'] : PROJECTTABLE;

    $modules->set_main_table($mtable);
    $custom_menu = $modules->which_has_method('getMenuItem');

    $n = array();
    foreach ($csor as $row) {
        //$sel = "";
        //if (in_array($row['role_id'],$g)) $sel = "selected";
        //$groups .= "<option value='{$row['group_id']}' $sel>".$row['description']."</option>";
        $n[] = $row['description']."::".$row['role_id'];
    }

    $module_list = $modules->list_modules();
    sort($module_list);
    $datalist = "<datalist id='modules'>";
    foreach ($module_list as $ml) {
        $datalist .= "<option value='$ml'>";
    }
    $datalist .= "</datalist>";
    echo $datalist;

    $cmd = "SELECT f_main_table as m FROM header_names WHERE f_table_name='".PROJECTTABLE."' AND f_main_table LIKE '".PROJECTTABLE."%'";
    $res = pg_query($BID,$cmd);
    $mrow = array();
    while ($row = pg_fetch_assoc($res)) {
        $mrow[] = $row['m'];
    }

    if (isset($_GET['module_type'])) $editor = $_GET['module_type']; # state clikk
    else $editor = 'project_modules';

    if ($editor == 'project_modules')
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=project_modules&module_type=table_modules' title='".t('projekt modulok')."' class='admin_direct_link pure-button button-href' style='margin-top:-80px;float:right;margin-right:10px;font-size:1.5em;font-weight:bold'>".t('projekt modulok kezelése')." <i class='fa fa-plug'></i></a>";
    else
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=modules&module_type=project_modules' title='".t('tábla modulok')."' class='admin_direct_link pure-button button-href' style='margin-top:-80px;float:right;margin-right:10px;font-size:1.5em;font-weight:bold'>".t('tábla modulok kezelése')." <i class='fa fa-plug fa-rotate-180'></i></a>";

    echo '<h3>Egyes adattáblákhoz kapcsolódó modulok kezelése</h3><span style="font-size:125%">'.t(str_choose_mtable).": <select class='main_table_selector' style='font-size:125%;vertical-align:middle' id='modules'>".selected_option($mrow,$mtable)."</select>
        <button class='main_table_refresh' id='modules'><i class='fa fa-refresh'></i></button> ".wikilink('modules.html',str_module_documents)."</span><br><br>";

    $cmd = "SELECT f_table_schema,id,\"enabled\",\"module_name\",\"function\",\"module_access\",array_to_string(params,'#~#') as params,path,array_to_string(group_access,',') as ga,new_params 
        FROM modules LEFT JOIN header_names ON (f_table_name=project_table AND f_main_table=main_table)
        WHERE project_table='".PROJECTTABLE."' AND main_table=".quote($mtable)." AND module_type='table' 
        ORDER BY module_name,function";
    $res = pg_query($BID,$cmd);
    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable pure-form']);
    while ($row = pg_fetch_assoc($res)) {

        # module param parsing (v2.0) from OBM 4.3
        $params = json_encode(json_decode($row['new_params']),JSON_PRETTY_PRINT);
        $module_name_file = $row['module_name'].'.php';

        $schema = $row["f_table_schema"];

        # module params parsing (v1.1) from OBM 1.0 to OBM 4.0
        if ($params == 'null') {
            $params = explode('#~#',$row['params']);
            for ($i = 0; $i < count($params); $i++) {
                if (preg_match("/^JSON:(.*)$/",$params[$i],$json))
                    $params[$i] = base64_decode($json[1]);
            }

            $params = implode("\n", $params);

/*
            $is_json = 0;
            for ($i = 0; $i < count($params); $i++) {
                if (preg_match("/^JSON:(.*)$/",$params[$i],$json)) {
                    $is_json++;
                    $params[$i] = json_encode(json_decode(base64_decode($json[1])),JSON_PRETTY_PRINT);
                } else {
                    $params[$i] = json_encode($params[$i]);
                }
            }
            if (!$is_json) {
                $params = json_encode(json_decode("[".implode(",",$params)."]"),JSON_PRETTY_PRINT);
            }
            else {
                $params = implode(",",$params);
            }*/
        }


        $access_groups_options = selected_option($n,explode(',',$row['ga']));

        $id = $row['id'];
        $r=array();

        $module_exists = 'color:#FF6F6F;';

        if (file_exists(getenv('PROJECT_DIR').$row['path'].$row['module_name'].'.php')) {
            $module_exists = 'color:green;';
            $module_name = $row['module_name'].'.php';
        }

        array_push($r,
            sprintf('<input class=\'pure-input pure-u-1\' id=\'module-name_%1$s\' style=\'%3$s\' value=\'%2$s\'><input type=\'hidden\' readonly id=\'module-file_%1$s\' value=\'%4$s\'>',
                $id,
                $row['module_name'],
                $module_exists,
                $module_name));

        $placeholder_text = "newline separated list of parameters";
        if ($e = $modules->get_example($row['module_name'])) {
            $placeholder_text = $e;
        }
        $module_params_interface = sprintf("<div class='add-column link' style='display:none' id='columnref_%s' data-type='Column' data-table='$mtable' data-schema='$schema'>Add column</div>",$id);
        $module_params_interface .= sprintf("<textarea class='editorArea' id='module-params_%s' style='box-shadow:none;border:none;resize:none;scrollbar-width:none;width:35em;height:5em;padding:0 0 0 5px' placeholder='%s'>%s</textarea>",
            $id,
            $placeholder_text,
            $params);

        $module_name = $row['module_name'];
        if (in_array($module_name,$modules->which_has_method('module_params_interface'))) {
            $row['textarea'] = $module_params_interface;
            $row['mtable'] = $mtable;
            $row['placeholder_text'] = $placeholder_text;
            $module_params_interface = $modules->_include($module_name, 'module_params_interface', $row);
        }

        array_push($r,$module_params_interface);
        array_push($r,sprintf("<select id='module-ena_$id' name='enabled' style='font-size:1.4em' size=2>".selected_option(array(str_true.'::t',str_false.'::f'),$row['enabled'])."</select>"));
        array_push($r,sprintf("<select id='module-access_$id' style='font-size:1.4em' size=2>".selected_option(array('everybody::0','logined users::1'),$row['module_access'])."</select>"));
        array_push($r,sprintf("<select multiple id='module-gaccess_$id'>$access_groups_options</select>"));
        array_push($r,sprintf("<button name='copld' class='button-warning pure-button module_update' id='module-mod_".$id."'>".str_modifyit."</button>"));

        $cm = sprintf("<button class='button-gray button-xlarge pure-button'><i class='fa fa-cog'></i></button>");
        if (count($custom_menu) && in_array($module_name, $custom_menu)) {
            $m = $modules->_include($module_name,'getMenuItem');
            if (isset($m['url']))
                $cm = "<button class='module_submenu button-success button-xlarge pure-button' data-url='includes/project_admin.php?options={$m['url']}&mtable=$mtable'><i class='fa fa-cog'></i></button>";
        }
        array_push($r,$cm);

        array_push($r,sprintf("<select id='module-function_$id' size=2>%s</select>",selected_option(array('default','private'),$row['function'])));
        $button = new button(["url"=>"ajax?module_export=$module_name_file","title"=>'export',"icon"=>'fa fa-download']);
        array_push($r,sprintf("%s<br>%s",$button->print([]),"<input type='file' name='$module_name_file' id='module_file-upload' title='upload .php file'>"));
        $tbl->addRows($r);
        $id++;
    }

    $tbl->addRows(array("<input list='modules' id='module-name_new'><input type='hidden' id='module-file_new' value=''>","<input id='module-params_new'>","<select id='module-ena_new'><option>TRUE</option><option>FALSE</option></select>","<select id='module-access_new'><option value='0'>everybody</option><option value='1'>logined users</option></select>","<select id='module-gaccess_new'>$access_groups_options</select>","<input type='button' name='aopld' class='button-success button-xlarge pure-button module_update' id='module-new_new' value='".str_add."'>","","<input type='hidden' id='module-function_new' value='default'>default",""));

    $tbl->addHeader(array('Module name','Parameters',t(str_enabled),t(str_access),t(str_group)." ".str_access,t(str_operations),'','Function',t(str_replace_module)));
    echo $tbl->printOut();

?>
