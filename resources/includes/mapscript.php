<?php

dl('php_mapscript.so');

// Default values and configuration

$val_zsize=3;
$check_pan="CHECKED";
$map_path="/var/www/html/biomaps/private/";
$map_file="private.map";

$map = ms_newMapObj($map_path.$map_file);
$ext = array();


$my_pdf = pdf_new();
#...


$map->set(width,1200);
$map->set(height,1200);

$map->setextent($ext[0], $ext[1], $ext[2], $ext[3]);

while($layer[]) {
    $layer=$map->getLayer($n);
    if($layer[$n]==1) {
        $layer->set(status,1);
    } else {
        $layer->set(status,0);
    }
}

$img = $map->draw();
$url = $img->saveWebImage(MS_PNG, 0, 0, 0);

$element = pdf_open_image_file($my_pdf, "png", "$webroot/$url");
pdf_place_image($my_pdf, $element, $xpos, $ypos);
pdf_close_image($my_pdf, $element);

#....
pdf_close($my_pdf);

$data = pdf_get_buffer($my_pdf);

header('Content-type: application/pdf');
header('Content-disposition: inline; filename=my_pdf.pdf');
header('Content-length: ' . strlen($data) );

echo $data;
