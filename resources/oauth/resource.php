<?php
session_start();

// include our OAuth2 Server object
require_once __DIR__.'/server.php';

$scopeRequired = $_REQUEST['scope'] ?? 'apiprofile'; //default scope for pds
if (isset($_SESSION['scope']) and $_SESSION['scope'] != '') $scopeRequired = $_SESSION['scope'];

//get_data request hack
if ($_REQUEST['scope'] == 'get_data') $scopeRequired =  'get_data';

$value = $_REQUEST['value'] ?? '';
$table = $_REQUEST['table'] ?? '';
$project = $_REQUEST['project'] ?? ''; 
$form_id = $_REQUEST['form_id'] ?? '';
$put_data = $_REQUEST['data'] ?? '';
$put_header = $_REQUEST['header'] ?? '';
$soft_error = (isset($_REQUEST['soft_error'])) ? json_decode($_REQUEST['soft_error'],true) : [];
$shared_link = $_REQUEST['shared_link'] ?? '';

// Ez lehet, hogy nem is kell ide?
/*if (isset($_POST['credential'])) {
    auth_debug("Ide mikor jövünk?",__FILE__,__LINE__);
    require '../includes/vendor/autoload.php';
    // Google redirect
    $client = new Google_Client(['client_id' => $_POST['client_id']]);  // a kliensazonosító beállítása
    $payload = $client->verifyIdToken($_POST['credential']);  // az ID Token ellenőrzése


    // Ide kell még egy Google scope kérés, hogy kapjunk vissza refresh_token-t a google-tól?

    if ($payload) {
        // a felhasználó hitelesítve van, készítünk egy access_token-t neki
        $client = 'web';
        $client_secret = 'web';
        require("google.php");
    } else {
        // érvénytelen ID Token
        echo json_encode(array("error"=>"Invalid ID Token"));
        die;
    }
    unset($_POST['credential']);
} else {
    // Local request
    $request = OAuth2\Request::createFromGlobals();
//}
*/
$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// multipart/form-data request processing
//
$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : null;
if (strpos($contentType, 'multipart/form-data') !== false) {
    $params = array(
        'access_token' => $_POST['access_token'],
        'scope' => $_POST['scope']
    );
    $serverRequest = new OAuth2\Request($params);
    $response = new OAuth2\Response();

    $tokenData = $server->getAccessTokenData($serverRequest, $response);

    if ($tokenData && $tokenData['access_token'] == $params['access_token']) {
        // Az access_token érvényes, és a kérés feldolgozható
        // Itt dolgozd fel a fájlokat
        // access token
        $code = $params;
        $scopeRequired = $params['scope'];
        require_once('set_pds_params.php');

    } else {
        // Az access_token érvénytelen vagy lejárt, vagy nincs jogosultsága a kért erőforráshoz
        // Küldj hibaüzenetet a kliensnek
        header('Content-type:application/json;charset=utf-8');
        http_response_code(403);
        #auth_debug( "Rejected a non-authenticated PDS request: $scopeRequired ". "Expired token?" );
        exit ( "{\"status\":\"error\",\"message\":\"INVALID_TOKEN\"}" );

    }
} else {

    // Handle a request to a resource and authenticate the access token
    if (!$server->verifyResourceRequest($request, $response, $scopeRequired)) {
        
        // non authenticated zone .......................................................
        // .............................................................................>
        if ($scopeRequired == 'get_project_list' or ($scopeRequired == 'get_project' and $value == 'get_project_list')) {

            $project = 'all-projects';

            if (isset($_REQUEST['accessible']))
                $access_options = $_REQUEST['accessible'];
            else
                $access_options = 'all';

            $API_PARAMS = array('service'=>'PFS',
                'get_project_list'=>array("project"=>$project,'access_options'=>$access_options));
            return;
        
        }
            
        if ($scopeRequired == 'shared_link') {
            //$API_PARAMS = array('service'=>'PRS','shared_link'=>$_REQUEST['shared_link']); // only auth & create token??
            $API_PARAMS = array('service'=>'PFS',
                'connect_with_shared_link'=>$_REQUEST['shared_link']); // get data with link
            return;

        } elseif ($scopeRequired == 'interconnect_post_file_with_key') {

            # OpenBioMaps to OpenBioMaps connection

            $API_PARAMS = array('service'=>'PFS',
                'put_data'=>1,
                'ic_post_data'=>1,
                'ic_accept_key'=>$_REQUEST['accept_key'],
                'ic_request_key'=>$_REQUEST['request_key'],
                'ic_slave_server'=>$_REQUEST['slave_server'],
                'ic_slave_project'=>$_REQUEST['slave_project']);
            return;

        } elseif ($scopeRequired == 'webprofile' or $scopeRequired == 'apiprofile' or $scopeRequired == 'webprofile apiprofile') {

            header('Content-type:application/json;charset=utf-8');
            http_response_code(401);
            $j = json_decode($server->getResponse()->getResponseBody(),true);
            exit ( "{\"status\":\"error\",\"message\":\"".$j['error_description']."\"}" );

        } elseif ($scopeRequired == 'request_time' ) {

            $API_PARAMS = array('service'=>'PFS','request_time'=>$value);

        } else {

            header('Content-type:application/json;charset=utf-8');
            http_response_code(403);
            $j = json_decode($server->getResponse()->getResponseBody(),true);
            #auth_debug( "Rejected a non-authenticated PDS request: $scopeRequired". $server->getResponse()->getResponseBody() );
            exit ( "{\"status\":\"error\",\"message\":\"".$j['error_description']."\"}" );
        }

    }
    // protected zone ...............................................................
    // .............................................................................>

    if (count(array_intersect(["webprofile", "apiprofile"], explode(" ", $scopeRequired)))) { 
        $resp = [
            'status' => 'ok', 
            'message' => 'authenticated request',
            'data' => $server->getAccessTokenData($request, $response)
        ];
        // web application request or mobile auth test
        header('Content-type:application/json;charset=utf-8');
        exit( json_encode($resp) );
    }

    // PDS request
    // PROJECT_DIR is defined in pds.php
    if (!defined('CALL')) {
        http_response_code(403);
        header('Content-type:application/json;charset=utf-8');
        exit( "{\"status\":\"error\",\"message\":\"Use pds api\"}" );
    }

    // access token
    $code = $server->getAccessTokenData($request, $response);

    ##
    # valid $scopeRequired options should be placed in supported_scopes.php
    #
    #
    ##
    #

    require_once('set_pds_params.php');
}

?>
