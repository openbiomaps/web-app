<?php
// auth protected zone .........................................................
// .............................................................................>

if (!is_array($code) or !isset($code['access_token'])) $code = array('access_token' => false);

// scope based function parameters
if ($scopeRequired == 'get_profile') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_profile'=>$value);

} elseif ($scopeRequired == 'request_time' ) {
    
    $API_PARAMS = array('service'=>'PFS','request_time'=>$value,'table'=>$table,'access_token'=>$code['access_token']);

} elseif ($scopeRequired == 'get_form_list') {
    # deprecated from v2.5
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_form_list'=>1);

} elseif ($scopeRequired == 'get_form_data') {
    # deprecated from v2.5
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_form_data'=>$value);

} elseif ($scopeRequired == 'get_form') {
    
    if ($value == 'list') {
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_form_list'=>1);
    } else {
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_form_data'=>$value);
    }

} elseif ($scopeRequired == 'get_attachments') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_attachments'=>$value);

} elseif ($scopeRequired == 'get_bookmark') {
    
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_bookmark'=>$value);

} elseif ($scopeRequired == 'get_tables') {
    
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_tables_data'=>$value);

} elseif ($scopeRequired == 'get_specieslist') {
    
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_specieslist'=>$value);

} elseif ($scopeRequired == 'get_data') {

    $filters = (isset($_POST['filters'])) ? $_POST['filters'] : [];
    $type = (isset($_POST['type'])) ? $_POST['type'] : '';

    $API_PARAMS = array('service'=>'PRS','access_token'=>$code['access_token'],'table'=>$table,
        'get_data_rows'=>$value,'shared_link'=>$shared_link,'filters'=>$filters,'type'=>$type);

} elseif ($scopeRequired == 'get_history') {

    $API_PARAMS = array('service'=>'PRS','access_token'=>$code['access_token'],'table'=>$table,'get_history_rows'=>$value);

} elseif ($scopeRequired == 'set_rules') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'set_rules'=>$value);

} elseif ($scopeRequired == 'put_data') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
        'form_id'=>$form_id,
        'put_header'=>$put_header,
        'put_data'=>$put_data,
        'soft_error'=>$soft_error);

} elseif ($scopeRequired == 'get_trainings' or ($scopeRequired == 'trainings' and $value = 'get_trainings')) {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_trainings'=>1);

} elseif ($scopeRequired == 'get_training_questions' or ($scopeRequired == 'trainings' and $value = 'get_training_questions')) {

    if ($value == 'get_training_questions') {
        $value = preg_replace('^[^0-9]+$','',$_GET['training_id']);
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_training_questions'=>$value);
    } else
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_training_questions'=>$value);

} elseif ($scopeRequired == 'training_results' or ($scopeRequired == 'trainings' and $value = 'training_results')) {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'training_results'=>$value);

} elseif ($scopeRequired == 'training_toplist' or ($scopeRequired == 'trainings' and $value = 'training_toplist')) {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'training_toplist'=>$value);

} elseif ($scopeRequired == 'get_project_list' or ($scopeRequired == 'get_project' and $value == 'get_project_list')) {
    # The get_project_list scope is deprecated since pds v.2.5

    $project = 'all-projects';

    if (isset($_REQUEST['accessible']))
        $access_options = $_REQUEST['accessible'];
    else
        $access_options = 'all';

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],
        'get_project_list'=>array("project"=>$project,'access_options'=>$access_options));

} elseif ($scopeRequired == 'get_project_vars' or ($scopeRequired == 'get_project' and $value == 'get_project_vars')) {
    # The get_project_vars scope is deprecated since pds v.2.5

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
        'get_project_vars'=>1);

} elseif ($scopeRequired == 'get_mydata_rows') {
    
    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_mydata_rows'=>$value);

} elseif ($scopeRequired == 'pg_user') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'pg_user'=>$value);

} elseif ($scopeRequired == 'get_tile_list') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_tile_list'=>$value);

} elseif ($scopeRequired == 'get_tile_zip') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_tile_zip'=>$value);

} elseif ($scopeRequired == 'get_message_count') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_message_count'=>$value);

} elseif ($scopeRequired == 'get_notification') {

    $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,'get_notification'=>$value);

} elseif ( $scopeRequired == 'tracklog' ) {

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
            'post_tracklogs'=>1,
            'tracklog'=>$value);
    
    } else {
        # _GET Request
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
            'get_tracklogs'=>1);
    }
} elseif ($scopeRequired == 'use_repo') {

    if (isset($_POST['method']) and $_POST['method'] == 'put')
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
                            'use_repo'=>array('scope'=>'put_data','params'=>$_POST['params']));
    elseif (isset($_POST['method']) and $_POST['method'] == 'get')
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
                            'use_repo'=>array('scope'=>'get_data','params'=>$_POST['params']));
    elseif (isset($_POST['method']) and $_POST['method'] == 'delete')
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
                            'use_repo'=>array('scope'=>'delete_data','params'=>$_POST['params']));
    elseif (isset($_POST['method']) and $_POST['method'] == 'set')
        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
                            'use_repo'=>array('scope'=>'set_data','params'=>$_POST['params']));

} elseif ($scopeRequired == 'computation') {

        $API_PARAMS = array('service'=>'PFS','access_token'=>$code['access_token'],'table'=>$table,
                            'computation'=>array('scope'=>$_POST['method'],'params'=>$_POST['params']));
}


?>
