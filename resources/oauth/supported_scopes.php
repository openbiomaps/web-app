<?php

$defaultScope = array(
    'apiprofile',
    'request_time'  
);
$supportedScopes = array(
    'apiprofile',
    'get_attachments',
    'get_data',
    'get_form',          # from PDS v2.5 instead of get_form_data get_form_list
        'get_form_data',    # deprecated from v2.5
        'get_form_list',    # deprecated from v2.5
    'get_history',
    'get_message_count',
    'get_mydata_rows',
    'get_project',      # from PDS v2.5 instead of get_project get_project_list get_project_vars
        'get_project_list', # deprecated from v2.5
        'get_project_vars', # deprecated from v2.5
    'get_specieslist',
    'get_profile',
    'get_tables',
    'get_tile_list',
    'get_tile_zip',
    'get_report',
    'interconnect_post_file_with_key',
    'use_repo',         # from PDS v2.5
    'pg_user',
    'put_data',
    'request_time',
    'set_rules',
    'tracklog',
    'computation',
    'trainings',        # from PDS v2.5 instead of training_results training_toplist get_trainings get_training_questions
        'training_results', # deprecated from v2.5
        'training_toplist', # deprecated from v2.5
        'get_trainings',          # deprecated from v2.5
        'get_training_questions', # deprecated from v2.5
    'webprofile',
    'supervisor',
    'get_notification'
);

?>
