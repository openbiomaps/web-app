<?php
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once(dirname(__FILE__).'/../includes/vendor/autoload.php');
#require_once(getenv('OB_LIB_DIR').'base_functions.php'); // only for debugging!

/* Create a sylog message
 * log_action function
 * */
function auth_debug($message,$file=NULL,$line=NULL) {
    if(is_array($message)) {
        $message = json_encode($message);
    }   
    elseif(is_object($message)) {
        $message = json_encode($message);
    }   
    $custom_log = "/var/log/openbiomaps.log";

    $pointer = ""; 
    if($file!=NULL or $line!=NULL)
        $pointer = " $file $line";

    $path_parts = pathinfo(__FILE__);
    if (is_writable($custom_log)) {
        $date = date('M j h:i:s'); 
        error_log("[OBM_".$path_parts['dirname']."] $date$pointer: $message\n", 3, "/var/log/openbiomaps.log");
    } else {
        openlog("[OBM_".$path_parts['dirname']."]", 0, LOG_LOCAL0);
        syslog(LOG_WARNING,$pointer.$message);
        closelog();
    }   
}

// Using OAuth2
$request = OAuth2\Request::createFromGlobals();

//hack for 2.x mobile app
$remember_me = (
    (isset( $request->request['remember_me']) && $request->request['remember_me'] == true)
    // Mobil app without remember me request
    || (isset($_SERVER['PHP_AUTH_USER']) and $_SERVER['PHP_AUTH_USER'] === 'mobile') 
);
// Egyes klienseknek ez is kell
if (isset($_SERVER['QUERY_STRING']) and (
    $_SERVER['QUERY_STRING'] == 'api_version=2.4' or
    $_SERVER['QUERY_STRING'] == 'path=v2.4/pds.php'
)) {
    $remember_me = true;
}

if ($remember_me) {
    $refresh_token_lifetime =  14*24*60*60;
    $always_issue_new_refresh_token = true;
}
else {
    $refresh_token_lifetime = 1.5*60*60;
    $always_issue_new_refresh_token = false;
}

require_once("custom.php");
// $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
// default Storage/Pdo
// $storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));
//
//
$dsn = 'pgsql:dbname='.constant('biomapsdb_name').';host='.constant('biomapsdb_host').';port='.constant('POSTGRES_PORT').'';
$storage = new MyPdo(
    array(
        'dsn' => $dsn, 
        'username' => constant('biomapsdb_user'), 
        'password' => constant('biomapsdb_pass')),
    array( 
        'user_table' => 'users'
    )
);

// Pass a storage object or array of storage objects to the OAuth2 server class
$server = new OAuth2\Server($storage, array(
    'allow_implicit' => true,
    'refresh_token_lifetime' => $refresh_token_lifetime,
    'always_issue_new_refresh_token' => $always_issue_new_refresh_token,
));

// Add the "Client Credentials" grant type (it is the simplest of the grant types)
$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage , array('allow_credentials_in_request_body' => true)));

// Add the "Authorization Code" grant type (this is where the oauth magic happens)
$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

$server->addGrantType(new OAuth2\GrantType\UserCredentials($storage));

$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage, [
    'always_issue_new_refresh_token' => $always_issue_new_refresh_token
])); 


// _REQUEST hacking
if (isset($_REQUEST['username'])) {
    // as no email with spaces we can safely remove exrta spaces which is useful for mobil auth....
    $_REQUEST['username'] = strtolower(trim($_REQUEST['username'])); 
    // as no email with spaces we can safely remove exrta spaces which is useful for mobil auth....
    $_POST['username'] = strtolower(trim($_REQUEST['username'])); 

    // add default scope on login if it hasn't been added
    if (isset($_REQUEST['scope']) and !preg_match('/apiprofile/',$_REQUEST['scope'])) {
        $_REQUEST['scope'] .= " apiprofile";
        $_POST['scope'] .= " apiprofile";
    } elseif (!isset($_REQUEST['scope'])) {
        $_REQUEST['scope'] = " apiprofile";
        $_POST['scope'] = " apiprofile";
    }
}

require('supported_scopes.php');

$memory = new OAuth2\Storage\Memory(array(
  'default_scope' => $defaultScope,
  'supported_scopes' => $supportedScopes
));
$scopeUtil = new OAuth2\Scope($memory);
$server->setScopeUtil($scopeUtil);

//$server->setConfig('enforce_state', false);
?>
