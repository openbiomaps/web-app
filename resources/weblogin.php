<?php
session_start();

require_once('/etc/openbiomaps/system_vars.php.inc');
require_once('local_vars.php.inc');
require_once(getenv('OB_LIB_DIR') . 'db_funcs.php');
require_once(getenv('OB_LIB_DIR') . 'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR') . 'languages.php');
require(getenv('OB_LIB_DIR') . '/auth.php');

if (!defined('STYLE_PATH')) {
    $style_def = constant('STYLE');
    $style_name = $style_def['template'];
    define('STYLE_PATH', "/styles/app/" . $style_name);
}

$protocol = protocol();

if (isset($_GET['callback']) and validate_url($_GET['callback'])) {
    $callback_address = $_GET["callback"];
} else {
    $callback_address = "$protocol://" . URL . "/?callback";
}
$_SESSION['login_callback_address'] = $callback_address;

$reauth = isset($_SESSION['switchprofile']);
$scope = 'apiprofile';
if (isset($_GET['scope'])) {
    $_SESSION['scope'] = $_GET['scope'];
    $scope = $_GET['scope'];
}
$get_client_id = $_GET['client_id'] ?? "";
$get_client_secret = $_GET['client_secret'] ?? "";
$cookie_path = $_GET['cookie_path'] ?? "/";
$auth_key = $_GET['auth_key'] ?? "";

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    // Get the POST data (assuming it's in JSON format)
    $post_data = file_get_contents('php://input');
    $data = json_decode($post_data, true);

    if (isset($data['username']) && isset($data['password'])) {
        $username = $data['username'];
        $password = $data['password'];
        $client_id = $data['client_id'];
        $client_secret = $data['client_secret'];
        $remember_me = $data['remember_me'];
        $auth_key = $data['auth_key'];

        $scope = $data['scope'];

        if ($reauth) {
            $ret = ObmAuth::reauth(
                $username,
                $password
            );
        } else {
            $ret = ObmAuth::pwalogin(
                $username,
                $password,
                $remember_me,
                $scope,
                array(
                    "client_id" => $client_id,
                    "client_secret" => $client_secret
                ),
                $cookie_path
            );
        }

        if ($ret) {
            header('Content-Type: application/json');
            if ($auth_key != '') {
                ObmAuth::save(
                    $auth_key,
                    json_encode($_SESSION['oauthtoken']),
                    $_SESSION['Tproject'],
                    $username
                ); 
                // Return the URL as a JSON response
                echo json_encode(['url' => URL]);
            } else {
                echo json_encode(['url' => '']);
            }
            exit;
        } else {
            // Authentication failed
            http_response_code(401); // Unauthorized
            echo 'Authentication failed';
            exit;
        }
    } elseif (isset($data['email'])) {
        if (ObmAuth::LostPw($data['email'])) {
            $notice = str_message_sent;
            $state = "success";
        } else {
            $notice = str_no_such_mail_address;
            $state = "error";
        }
        header('Content-Type: application/json');
        echo json_encode(['state' => $state, 'notice' => $notice]);
        exit;
    } else {
        // Invalid request
        http_response_code(400); // Bad Request
        echo 'Invalid request';
        exit;
    }
}


// Load login page
$strings = [
    'str_newactivation', 'str_email', 'str_submit', 'str_password',
    'str_login', 'str_remember_me', 'str_lostpasswd', 'str_registration',

];

$project_props = projectProperties();

$translations = array_combine($strings, array_map('t', $strings));
$translations['title'] = $project_props['short']; //t(constant('PROJECTTABLE'));
$openid_buttons = '';
if (defined('OPENID_CONNECT')) {
    $openid = constant('OPENID_CONNECT');
    foreach (array_keys(constant('OPENID_CONNECT')) as $service) {
        $openid_buttons .= OidcAuth::generate_openid_button($service, true);
    }
}
render_view('pages/weblogin', [
    'css_rev' => rev('css/weblogin.css'),
    'page_title' => $project_props['short'] . " " . t('str_login'),
    't' => $translations,
    'openid_buttons' => $openid_buttons,
    'client_id' => $openid['google']['client_id'] ?? '',
    'callback_address' => $callback_address,
    'url' => $protocol . "://" . constant('URL'),
    'get_client_id' => $get_client_id,
    'get_client_secret' => $get_client_secret,
    'cookie_path' => $cookie_path,
    'scope' => $scope,
    'get_auth_key' => $auth_key
]);
exit;
