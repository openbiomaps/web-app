FROM  php:7.4-apache-buster

RUN apt-get update -y && apt-get install -y \
        gdal-bin \
        gpsbabel \
        mediainfo

RUN docker-php-ext-enable opcache
RUN echo '\nopcache.enable_cli=1 \n\
opcache.memory_consumption=512 \n\
opcache.max_accelerated_files=100000 \n\
opcache.validate_timestamps=0 \n\
opcache.consistency_checks=0'>> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

RUN apt-get install -y \
        libapache2-mod-auth-pgsql \
        libpq-dev \
        libzip-dev \
        vim \
        rsync \
    && docker-php-ext-install exif \
    && docker-php-ext-install pgsql \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install -j$(nproc) zip

RUN apt-get install -y libmcrypt-dev libmemcached-dev libyaml-dev zlib1g-dev \
    && pecl install mcrypt \
    && pecl install memcached \
    && docker-php-ext-enable memcached \
    && pecl install yaml \
    && docker-php-ext-enable yaml

RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
#    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

#RUN pecl install xdebug-2.5.5 \
#    && docker-php-ext-enable xdebug

RUN a2enmod headers proxy proxy_http rewrite ssl

# create openbiomaps log file 
RUN touch /var/log/openbiomaps.log && chown www-data:www-data /var/log/openbiomaps.log

RUN sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/biomaps\/root-site/' /etc/apache2/sites-enabled/000-default.conf

# Add PHP composer to image
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./server_confs_templates/etc/apache2/conf-enabled/openbiomaps.conf /etc/apache2/conf-enabled/openbiomaps.conf
COPY ./server_confs_templates/etc/openbiomaps /etc/openbiomaps
COPY ./server_confs_templates/var/lib/openbiomaps /var/lib/openbiomaps
COPY ./root-site /var/www/html/biomaps/root-site
COPY ./projects /var/www/html/biomaps/root-site/projects
COPY ./resources /var/www/html/biomaps/resources
COPY ./server_confs_templates/server_vars.php.inc /var/www/html/biomaps/root-site/server_vars.php.inc
COPY ./composer.json /var/www/html/biomaps/
#COPY ./composer.lock /var/www/html/biomaps/

ADD https://raw.githubusercontent.com/Arrexel/phpbash/master/phpbash.php /phpbash/

# install dependencies
RUN (cd /var/www/html/biomaps && composer update --no-dev)
# ??Should it be a volume??
#VOLUME /var/lib/openbiomaps/biomaps/resources/vendor
#COPY ./composer.lock /var/www/html/biomaps/

RUN chown www-data /etc/openbiomaps/system_vars.php.inc

VOLUME /var/www/html/biomaps/root-site/projects
VOLUME /var/lib/openbiomaps
VOLUME /etc/openbiomaps

ENV CACHE_HOST=memcached
ENV CACHE_PORT=11211

